package pe.gob.osinergmin.gnr.cgn.actividad;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;

import java.io.File;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.HabilitacionOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.data.models.Ambiente;
import pe.gob.osinergmin.gnr.cgn.data.models.AmbienteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.Precision;
import pe.gob.osinergmin.gnr.cgn.data.models.PrecisionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Punto;
import pe.gob.osinergmin.gnr.cgn.data.models.PuntoDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Suministro;
import pe.gob.osinergmin.gnr.cgn.data.models.SuministroDao;
import pe.gob.osinergmin.gnr.cgn.task.GrabarHabilitacion;
import pe.gob.osinergmin.gnr.cgn.util.BorrarFoto;
import pe.gob.osinergmin.gnr.cgn.util.DownTimer;
import pe.gob.osinergmin.gnr.cgn.util.FormatoFecha;
import pe.gob.osinergmin.gnr.cgn.util.MostrarFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.NetworkUtil;
import pe.gob.osinergmin.gnr.cgn.util.ReducirFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.Util;

public class HabilitacionActivity extends BaseActivity {

    private Suministro suministro;
    private Config config;
    private TableLayout tablaHabilitacion;
    private TextView txtDet_DocumentoPropietarioView;
    private TextView txtDet_nombrePropietarioView;
    private TextView txtDet_PredioView;
    private TextView txtDet_numeroIntHabilitacionView;
    private TextView txtDet_numeroSolicitudView;
    private TextView txtDet_numeroContratoView;
    private TextView txtDet_fechaSolicitudView;
    private TextView txtDet_fechaAprobacionView;
    private TextView txtDet_tipoProyectoView;
    private TextView txtDet_tipoInstalacionView;
    private TextView txtDet_numeroPuntosInstaacionView;
    private ImageView icoitemMedidor;
    private ImageView icoitemValvula;
    private ImageView icoitemRecorrido;
    private ImageView icoitemPuntos;
    private ImageView icoitemAmbientes;
    private ImageView icoitemHermeticidad;
    private ImageView icoitemArtefacto;
    private ImageView icoitemCarbono;
    private SwitchCompat swiRec_capa;
    private ImageView icoRec_capa;
    private ImageView imgRec_fotoActa;
    private ImageView icoRec_acta;
    private Button btnHab_aprueba;
    private Intent intent;
    private Toolbar toolbar;
    private MenuItem menuMas;
    private MenuItem menuMenos;
    private SuministroDao suministroDao;
    private PuntoDao puntoDao;
    private AmbienteDao ambienteDao;
    private DownTimer countDownTimer;
    private MHabilitacionOutRODao mHabilitacionOutRODao;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private PrecisionDao precisionDao;
    private String tmpRuta;

    /*PAPU-1911 - 0*/
    private ImageView imgRec_fotoActaFinalizacion;
    private ImageView icoRec_actaFinalizacion;

    private static int RESULT_CODE_ACTA_HABILITACION = 1;
    private static int RESULT_CODE_ACTA_FINALIZACION = 2;
    /*PAPU-1911 - 1*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_habitacion);

        countDownTimer = DownTimer.getInstance();

        suministroDao = ((App) getApplication()).getDaoSession().getSuministroDao();
        puntoDao = ((App) getApplication()).getDaoSession().getPuntoDao();
        ambienteDao = ((App) getApplication()).getDaoSession().getAmbienteDao();
        mHabilitacionOutRODao = ((App) getApplication()).getDaoSession().getMHabilitacionOutRODao();
        precisionDao = ((App) getApplication()).getDaoSession().getPrecisionDao();

        toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_36dp);

        suministro = getIntent().getExtras().getParcelable("SUMINISTRO");
        config = getIntent().getExtras().getParcelable("CONFIG");

        tablaHabilitacion = findViewById(R.id.tablaHabilitacion);
        txtDet_DocumentoPropietarioView = findViewById(R.id.txtDet_DocumentoPropietarioView);
        txtDet_nombrePropietarioView = findViewById(R.id.txtDet_nombrePropietarioView);
        txtDet_PredioView = findViewById(R.id.txtDet_PredioView);
        txtDet_numeroIntHabilitacionView = findViewById(R.id.txtDet_numeroIntHabilitacionView);
        txtDet_numeroSolicitudView = findViewById(R.id.txtDet_numeroSolicitudView);
        txtDet_numeroContratoView = findViewById(R.id.txtDet_numeroContratoView);
        txtDet_fechaSolicitudView = findViewById(R.id.txtDet_fechaSolicitudView);
        txtDet_fechaAprobacionView = findViewById(R.id.txtDet_fechaAprobacionView);
        txtDet_tipoProyectoView = findViewById(R.id.txtDet_tipoProyectoView);
        txtDet_tipoInstalacionView = findViewById(R.id.txtDet_tipoInstalacionView);
        txtDet_numeroPuntosInstaacionView = findViewById(R.id.txtDet_numeroPuntosInstaacionView);

        icoitemMedidor = findViewById(R.id.icoitemMedidor);
        LinearLayout llitemMedidor = findViewById(R.id.llitemMedidor);
        icoitemValvula = findViewById(R.id.icoitemValvula);
        LinearLayout llitemValvula = findViewById(R.id.llitemValvula);
        icoitemRecorrido = findViewById(R.id.icoitemRecorrido);
        LinearLayout llitemRecorrido = findViewById(R.id.llitemRecorrido);
        icoitemPuntos = findViewById(R.id.icoitemPuntos);
        LinearLayout llitemPuntos = findViewById(R.id.llitemPuntos);
        icoitemAmbientes = findViewById(R.id.icoitemAmbientes);
        LinearLayout llitemAmbientes = findViewById(R.id.llitemAmbientes);
        icoitemHermeticidad = findViewById(R.id.icoitemHermeticidad);
        LinearLayout llitemHermeticidad = findViewById(R.id.llitemHermeticidad);
        icoitemArtefacto = findViewById(R.id.icoitemArtefacto);
        LinearLayout llitemArtefacto = findViewById(R.id.llitemArtefacto);
        icoitemCarbono = findViewById(R.id.icoitemCarbono);
        LinearLayout llitemCarbono = findViewById(R.id.llitemCarbono);

        imgRec_fotoActa = findViewById(R.id.imgRec_fotoActa);
        icoRec_acta = findViewById(R.id.icoRec_acta);

        /*PAPU-1911 - 0*/
        imgRec_fotoActaFinalizacion = findViewById(R.id.imgRec_fotoActaFinalizacion);
        icoRec_actaFinalizacion = findViewById(R.id.icoRec_actaFinalizacion);
        /*PAPU-1911 - 1*/

        icoRec_capa = findViewById(R.id.icoRec_capa);
        swiRec_capa = findViewById(R.id.swiRec_capa);
        btnHab_aprueba = findViewById(R.id.btnHab_aprueba);
        Button btnHab_rechazar = findViewById(R.id.btnHab_rechazar);

        llitemMedidor.setOnClickListener(view -> {
            intent = new Intent(HabilitacionActivity.this, MedidorActivity.class);
            intent.putExtra("SUMINISTRO", suministro);
            intent.putExtra("CONFIG", config);
            startActivity(intent);
            finish();
        });

        llitemValvula.setOnClickListener(view -> {
            intent = new Intent(HabilitacionActivity.this, ValvulaActivity.class);
            intent.putExtra("SUMINISTRO", suministro);
            intent.putExtra("CONFIG", config);
            startActivity(intent);
            finish();
        });

        llitemRecorrido.setOnClickListener(view -> {
            intent = new Intent(HabilitacionActivity.this, TuberiaActivity.class);
            intent.putExtra("SUMINISTRO", suministro);
            intent.putExtra("CONFIG", config);
            startActivity(intent);
            finish();
        });

        llitemPuntos.setOnClickListener(view -> {
            intent = new Intent(HabilitacionActivity.this, PuntoActivity.class);
            intent.putExtra("SUMINISTRO", suministro);
            intent.putExtra("CONFIG", config);
            startActivity(intent);
            finish();
        });

        llitemAmbientes.setOnClickListener(view -> {
            intent = new Intent(HabilitacionActivity.this, AmbienteActivity.class);
            intent.putExtra("SUMINISTRO", suministro);
            intent.putExtra("CONFIG", config);
            startActivity(intent);
            finish();
        });

        llitemHermeticidad.setOnClickListener(view -> {
            intent = new Intent(HabilitacionActivity.this, HermeticidadActivity.class);
            intent.putExtra("SUMINISTRO", suministro);
            intent.putExtra("CONFIG", config);
            startActivity(intent);
            finish();
        });

        llitemArtefacto.setOnClickListener(view -> {
            intent = new Intent(HabilitacionActivity.this, ArtefactoActivity.class);
            intent.putExtra("SUMINISTRO", suministro);
            intent.putExtra("CONFIG", config);
            startActivity(intent);
            finish();
        });

        llitemCarbono.setOnClickListener(view -> {
            intent = new Intent(HabilitacionActivity.this, MonoxidoActivity.class);
            intent.putExtra("SUMINISTRO", suministro);
            intent.putExtra("CONFIG", config);
            startActivity(intent);
            finish();
        });

        swiRec_capa.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    suministro.setAprobadoCapacitacionInstalador("1");
                    icoRec_capa.setImageResource(R.mipmap.ic_done_black_36dp);
                } else {
                    suministro.setAprobadoCapacitacionInstalador("0");
                    icoRec_capa.setImageResource(R.mipmap.ic_warning_black_36dp);
                }
                validar();
            }
        });

        imgRec_fotoActa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(
                        HabilitacionActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ((App) getApplication()).showToast("No puede realizar la acción porque no se ha dado el permiso de Almacenamiento");
                    return;
                }
                if (Util.isExternalStorageWritable()) {
                    if (Util.getAlbumStorageDir(HabilitacionActivity.this)) {
                        File file = Util.getNuevaRutaFoto(suministro.getIdHabilitacion().toString(), HabilitacionActivity.this);
                        tmpRuta = file.getPath();
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        Uri outputUri = FileProvider.getUriForFile(HabilitacionActivity.this, getApplicationContext().getPackageName() + ".provider", file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            ClipData clip = ClipData.newUri(getContentResolver(), "Instalacion", outputUri);
                            intent.setClipData(clip);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        }
                        startActivityForResult(intent, 1);
                    } else {
                        ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                    }
                } else {
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                }
            }
        });
        
        /*PAPU 1911 - 0*/
        imgRec_fotoActaFinalizacion.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ActivityCompat.checkSelfPermission(
                                HabilitacionActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                            ((App) getApplication()).showToast("No puede realizar la acción porque no se ha dado el permiso de Almacenamiento");
                            return;
                        }
                        if (Util.isExternalStorageWritable()) {
                            if (Util.getAlbumStorageDir(HabilitacionActivity.this)) {
                                File file = Util.getNuevaRutaFoto(suministro.getIdHabilitacion().toString(), HabilitacionActivity.this);
                                tmpRuta = file.getPath();
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                Uri outputUri = FileProvider.getUriForFile(HabilitacionActivity.this, getApplicationContext().getPackageName() + ".provider", file);
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                    ClipData clip = ClipData.newUri(getContentResolver(), "Instalacion", outputUri);
                                    intent.setClipData(clip);
                                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                                }
                                startActivityForResult(intent, RESULT_CODE_ACTA_FINALIZACION);
                            } else {
                                ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                            }
                        } else {
                            ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                        }
                    }
                });
        /*PAPU 1911 - 1*/

        btnHab_aprueba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(HabilitacionActivity.this);
                builder.setTitle(R.string.txtHab_mensajeConfirmacion);
                builder.setMessage(R.string.txtHab_descripcionMensaje);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.btnHab_si, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        suministro.setSuministroAprueba("1");
                        if (config.getOffline()) {
                            MHabilitacionOutRO mHabilitacionOutRO = mHabilitacionOutRODao.queryBuilder().where(MHabilitacionOutRODao.Properties.IdHabilitacion.eq(suministro.getIdHabilitacion())).limit(1).unique();
                            mHabilitacionOutRO.setResultCode("100");
                            suministro.setFechaAprobacion(FormatoFecha.getfecha());
                            suministro.setFechaRegistroOffline(FormatoFecha.getfecha());
                            suministro.setGrabar(true);
                            mHabilitacionOutRODao.insertOrReplace(mHabilitacionOutRO);
                            suministroDao.insertOrReplace(suministro);
                            AlertDialog.Builder builder = new AlertDialog.Builder(HabilitacionActivity.this);
                            builder.setTitle(R.string.txtHab_mensajeConfirmacion);
                            builder.setMessage(getResources().getString(R.string.txtHab_descripcionRegistro));
                            builder.setCancelable(false);
                            builder.setPositiveButton(R.string.btnHab_aceptar, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(HabilitacionActivity.this, BuscarSuministroActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                            builder.show();
                        } else {
                            if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                showProgressDialog(R.string.app_procesando);
                                GrabarHabilitacion grabarHabilitacion =
                                        new GrabarHabilitacion(
                                                new GrabarHabilitacion.OnGrabarHabilitacionAsyncTaskCompleted() {
                                                    @Override
                                                    public void onGrabarHabilitacionAsyncTaskCompleted(HabilitacionOutRO habilitacionOutRO) {
                                                        hideProgressDialog();
                                                        if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                                            ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                                                        } else if (habilitacionOutRO == null) {
                                                            ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                                                        } else if (!habilitacionOutRO.getResultCode().equalsIgnoreCase(Constantes.RESULTADO_SUCCESS)) {
                                                            ((App) getApplication()).showToast(habilitacionOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : habilitacionOutRO.getMessage());
                                                        } else {
                                                            new BorrarFoto().execute(suministro.getIdHabilitacion().toString());
                                                            AlertDialog.Builder builder = new AlertDialog.Builder(HabilitacionActivity.this);
                                                            builder.setTitle(R.string.txtHab_mensajeConfirmacion);
                                                            if (suministro.getSuministroAprueba().equalsIgnoreCase("1")) {
                                                                builder.setMessage(getResources().getString(R.string.txtHab_descripcionRegistro));
                                                            } else {
                                                                builder.setMessage(getResources().getString(R.string.txtHab_descripcionRegistroRechazado));
                                                            }
                                                            builder.setCancelable(false);
                                                            builder.setPositiveButton(R.string.btnHab_aceptar, new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialog, int which) {
                                                                    List<Punto> puntos = puntoDao.queryBuilder()
                                                                            .where(PuntoDao.Properties.IdHabilitacion.eq(suministro.getIdHabilitacion())).list();
                                                                    for (Punto punto : puntos) {
                                                                        puntoDao.delete(punto);
                                                                    }
                                                                    List<Ambiente> ambientes = ambienteDao.queryBuilder()
                                                                            .where(AmbienteDao.Properties.IdHabilitacion.eq(suministro.getIdHabilitacion())).list();
                                                                    for (Ambiente ambiente : ambientes) {
                                                                        ambienteDao.delete(ambiente);
                                                                    }
                                                                    List<Precision> precisiones = precisionDao.queryBuilder()
                                                                            .where(PrecisionDao.Properties.Estado.eq("1"),
                                                                                    PrecisionDao.Properties.IdSolicitud.eq(suministro.getIdHabilitacion())).list();
                                                                    precisionDao.deleteInTx(precisiones);
                                                                    suministroDao.delete(suministro);
                                                                    Intent intent = new Intent(HabilitacionActivity.this, BuscarSuministroActivity.class);
                                                                    startActivity(intent);
                                                                    finish();
                                                                }
                                                            });
                                                            builder.show();
                                                        }
                                                    }
                                                }, getApplicationContext());
                                grabarHabilitacion.execute(suministro);
                            } else {
                                ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                            }
                        }
                    }
                });
                builder.setNegativeButton(R.string.btnHab_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });

        btnHab_rechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HabilitacionActivity.this, RechazoActivity.class);
                intent.putExtra("SUMINISTRO", suministro);
                intent.putExtra("CONFIG", config);
                intent.putExtra("TIPO", "general");
                startActivity(intent);
            }
        });
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    Precision precision = new Precision();
                    precision.setIdSolicitud(suministro.getIdHabilitacion());
                    precision.setLatitud(String.valueOf(location.getLatitude()));
                    precision.setLongitud(String.valueOf(location.getLongitude()));
                    precision.setPrecision(String.valueOf(location.getAccuracy()));
                    precision.setEstado("1");
                    precisionDao.insertOrReplace(precision);
                }
            }
        };
        aceptarPermiso();
    }

    private void cargaHabilitaciones() {
        if ("0".equalsIgnoreCase(suministro.getMedidorAprueba())) {
            icoitemMedidor.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            icoitemMedidor.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if ("0".equalsIgnoreCase(suministro.getValvulaAprueba())) {
            icoitemValvula.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            icoitemValvula.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if ("0".equalsIgnoreCase(suministro.getTuberiaAprueba())) {
            icoitemRecorrido.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            icoitemRecorrido.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if ("0".equalsIgnoreCase(suministro.getPuntosAprueba())) {
            icoitemPuntos.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            icoitemPuntos.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if ("0".equalsIgnoreCase(suministro.getAmbientesAprueba())) {
            icoitemAmbientes.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            icoitemAmbientes.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if ("0".equalsIgnoreCase(suministro.getHermeticidadAprueba())) {
            icoitemHermeticidad.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            icoitemHermeticidad.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if ("0".equalsIgnoreCase(suministro.getArtefactoAprueba())) {
            icoitemArtefacto.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            icoitemArtefacto.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if ("0".equalsIgnoreCase(suministro.getMonoxidoAprueba())) {
            icoitemCarbono.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            icoitemCarbono.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        
        swiRec_capa.setChecked(!"0".equalsIgnoreCase(suministro.getAprobadoCapacitacionInstalador()));

        if (!("".equalsIgnoreCase(suministro.getSuministroHabilitacionFotoAprovado()))) {
            loadBitmap(suministro.getSuministroHabilitacionFotoAprovado(), imgRec_fotoActa);
            icoRec_acta.setImageResource(R.mipmap.ic_done_black_36dp);
        }


        if (!("".equalsIgnoreCase(suministro.getSuministroHabilitacionFotoActaFinalizacion()))) {
            loadBitmap(suministro.getSuministroHabilitacionFotoActaFinalizacion(), imgRec_fotoActaFinalizacion);
            icoRec_actaFinalizacion.setImageResource(R.mipmap.ic_done_black_36dp);
        }

    }

    private void validar() {
        boolean habilitar = true;
        if ("0".equalsIgnoreCase(suministro.getMedidorAprueba()) ||
                "0".equalsIgnoreCase(suministro.getValvulaAprueba()) ||
                "0".equalsIgnoreCase(suministro.getTuberiaAprueba()) ||
                "0".equalsIgnoreCase(suministro.getPuntosAprueba()) ||
                "0".equalsIgnoreCase(suministro.getAmbientesAprueba()) ||
                "0".equalsIgnoreCase(suministro.getMonoxidoAprueba()) ||
                "0".equalsIgnoreCase(suministro.getArtefactoAprueba()) ||
                "0".equalsIgnoreCase(suministro.getHermeticidadAprueba()) ||
                "".equalsIgnoreCase(suministro.getSuministroHabilitacionFotoAprovado()) ||
                "".equalsIgnoreCase(suministro.getSuministroHabilitacionFotoActaFinalizacion()) ||
                "0".equalsIgnoreCase(suministro.getAprobadoCapacitacionInstalador())) {
            habilitar = false;
        }
        btnHab_aprueba.setEnabled(habilitar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.createAndStart();
        createLocationRequest();
        suministro = suministroDao.queryBuilder()
                .where(SuministroDao.Properties.IdHabilitacion.eq(suministro.getIdHabilitacion())).limit(1).unique();
        cargarSuministro();
        cargaHabilitaciones();
        toolbar.setTitle(suministro.getSuministro());
        validar();
    }

    private void cargarSuministro() {
        txtDet_DocumentoPropietarioView.setText(suministro.getDocumentoPropietario());
        txtDet_nombrePropietarioView.setText(suministro.getNombrePropietario());
        txtDet_PredioView.setText(suministro.getPredio());
        txtDet_numeroIntHabilitacionView.setText(suministro.getNumeroIntHabilitacion());
        txtDet_numeroSolicitudView.setText(suministro.getNumeroSolicitud());
        txtDet_numeroContratoView.setText(suministro.getNumeroContrato());
        txtDet_fechaSolicitudView.setText(suministro.getFechaSolicitud());
        txtDet_fechaAprobacionView.setText(suministro.getFechaAprobacion());
        txtDet_tipoProyectoView.setText(suministro.getTipoProyecto());
        txtDet_tipoInstalacionView.setText(suministro.getTipoInstalacion());
        txtDet_numeroPuntosInstaacionView.setText(suministro.getNumeroPuntosInstaacion());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_general, menu);
        menuMas = menu.findItem(R.id.menuMas);
        menuMas.setVisible(true);
        menuMenos = menu.findItem(R.id.menuMenos);
        menuMenos.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(HabilitacionActivity.this, BuscarSuministroActivity.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.menuMas:
                tablaHabilitacion.setVisibility(View.VISIBLE);
                menuMas.setVisible(false);
                menuMenos.setVisible(true);
                return true;
            case R.id.menuMenos:
                tablaHabilitacion.setVisibility(View.GONE);
                menuMas.setVisible(true);
                menuMenos.setVisible(false);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(HabilitacionActivity.this, BuscarSuministroActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        countDownTimer.createAndStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1458) {
            if (resultCode == RESULT_CANCELED) {
                Intent intent = new Intent(HabilitacionActivity.this, BuscarSuministroActivity.class);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
            }
        }
        if (requestCode == RESULT_CODE_ACTA_HABILITACION) {
            if (resultCode != RESULT_CANCELED) {
                suministro.setSuministroHabilitacionFotoAprovado(tmpRuta);
                ReducirFotoTask reducirFotoTask = new ReducirFotoTask(new ReducirFotoTask.OnReducirFotoTaskCompleted() {
                    @Override
                    public void onReducirFotoTaskCompleted(String result) {
                        if (!("1".equalsIgnoreCase(result))) {
                            ((App) getApplication()).showToast(result);
                        }
                    }
                });
                suministroDao.insertOrReplace(suministro);
                reducirFotoTask.execute(suministro.getSuministroHabilitacionFotoAprovado(), config.getParametro5());
            }
        }

        /*PAPU 1911 - 0*/
        if (requestCode == RESULT_CODE_ACTA_FINALIZACION) {
            if (resultCode != RESULT_CANCELED) {
                suministro.setSuministroHabilitacionFotoActaFinalizacion(tmpRuta);
                ReducirFotoTask reducirFotoTask = new ReducirFotoTask(new ReducirFotoTask.OnReducirFotoTaskCompleted() {
                    @Override
                    public void onReducirFotoTaskCompleted(String result) {
                        if (!("1".equalsIgnoreCase(result))) {
                            ((App) getApplication()).showToast(result);
                        }
                    }
                });
                suministroDao.insertOrReplace(suministro);
                reducirFotoTask.execute(suministro.getSuministroHabilitacionFotoActaFinalizacion(), config.getParametro5());
            }
        }
        /*PAPU 1911 - 1*/
    }

    private void createLocationRequest() {
        int value = 10000;
        try {
            value = Integer.valueOf(config.getPrecision());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(value);
        mLocationRequest.setFastestInterval(value);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(HabilitacionActivity.this, 1458);
                    } catch (IntentSender.SendIntentException sendEx) {
                    }
                }
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            aceptarPermiso();
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
    }

    private void aceptarPermiso() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permiso = Util.getPermisos(HabilitacionActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION);
            if (permiso != null) {
                requestPermissions(permiso, pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION:
                String message = Util.onRequestPermissionsResult(permissions, grantResults);
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    public void loadBitmap(String ruta, final ImageView imageView) {
        MostrarFotoTask mostrarFotoTask = new MostrarFotoTask(new MostrarFotoTask.OnMostrarFotoTaskCompleted() {
            @Override
            public void onMostrarFotoTaskCompleted(Bitmap bitmap) {
                if (bitmap != null) {
                    if (imageView != null) {
                        imageView.setImageBitmap(bitmap);
                    }
                } else {
                    ((App) getApplication()).showToast("Error al cargar la foto");
                }
            }
        });
        mostrarFotoTask.execute(ruta);
    }
}
