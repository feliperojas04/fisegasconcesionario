package pe.gob.osinergmin.gnr.cgn.actividad;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.widget.Toolbar;

import gob.osinergmin.gnr.domain.dto.rest.out.BaseOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.task.RecuperarContrasenia;
import pe.gob.osinergmin.gnr.cgn.util.NetworkUtil;

public class RecuperarContrasenaActivity extends BaseActivity {

    private EditText ediRec_IngresarUsuario;
    private Toolbar toolbar;
    private Button btnRec_enviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperar_contrasena);

        toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_36dp);

        btnRec_enviar = findViewById(R.id.btnRec_enviar);
        ediRec_IngresarUsuario = findViewById(R.id.ediRec_IngresarUsuario);
        btnRec_enviar = findViewById(R.id.btnRec_enviar);

        ediRec_IngresarUsuario.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    btnRec_enviar.setEnabled(true);
                } else {
                    btnRec_enviar.setEnabled(false);
                }
            }
        });

        btnRec_enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    showProgressDialog(R.string.app_cargando);
                    RecuperarContrasenia recuperarContrasenia = new RecuperarContrasenia(new RecuperarContrasenia.OnRecuperarContraseniaCompleted() {
                        @Override
                        public void onRecuperarContraseniaCompleted(BaseOutRO baseOutRO) {
                            hideProgressDialog();
                            if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                            } else if (baseOutRO == null) {
                                ((App) getApplication()).showToast(getResources().getString(R.string.txtRec_error));
                            } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(baseOutRO.getResultCode())) {
                                ((App) getApplication()).showToast(baseOutRO.getMessage() == null ? getResources().getString(R.string.txtRec_error) : baseOutRO.getMessage());
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(RecuperarContrasenaActivity.this);
                                builder.setTitle(R.string.txtRec_tituloMens);
                                builder.setMessage(R.string.txtRec_descripcionMens);
                                builder.setCancelable(false);
                                builder.setPositiveButton(R.string.btnRec_aceptar, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(RecuperarContrasenaActivity.this, IniciarActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                                builder.show();
                            }
                        }
                    });
                    recuperarContrasenia.execute(ediRec_IngresarUsuario.getText().toString());
                } else {
                    ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        toolbar.setTitle(R.string.txtRec_titulo);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(RecuperarContrasenaActivity.this, IniciarActivity.class);
                startActivity(intent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RecuperarContrasenaActivity.this, IniciarActivity.class);
        startActivity(intent);
        finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
}
