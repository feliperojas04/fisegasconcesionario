package pe.gob.osinergmin.gnr.cgn.adaptador;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;

import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.BaseInspectorOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.GabineteProyectoInstalacionOutRo;
import gob.osinergmin.gnr.domain.dto.rest.out.GenericBeanOutRO;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.data.models.AcometidaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.AcometidaNueva;
import pe.gob.osinergmin.gnr.cgn.data.models.AcometidaNuevaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Observacion;
import pe.gob.osinergmin.gnr.cgn.data.models.ObservacionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Tuberias;
import pe.gob.osinergmin.gnr.cgn.util.MostrarFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.SearchableSpinner;
import pe.gob.osinergmin.gnr.cgn.util.Util;

import static gob.osinergmin.gnr.util.Constantes.RESULTADO_INSTALACION_ACOMETIDA_CONCLUIDA;
import static gob.osinergmin.gnr.util.Constantes.RESULTADO_INSTALACION_ACOMETIDA_PENDIENTE;
import static gob.osinergmin.gnr.util.Constantes.RESULTADO_INSTALACION_ACOMETIDA_RECHAZADA;

public class AcometidaNewAdaptador extends BaseAdapter{

    private Context context;
    private List<AcometidaNueva> acometidaNuevas;
    private List<GabineteProyectoInstalacionOutRo> listaProyectoInstalacion = new ArrayList<>();
    private List<GenericBeanOutRO> listaTipoGabinete = new ArrayList<>();
    private ListView listPuntos;
    private SwitchCompat aprueba;
    private AcometidaDao acometidaDao;
    private AcometidaNuevaDao acometidaNuevaDao;
    private Button btnIns1_grabar;
    private Callback callback;
    private boolean mini = false;
    private int Id;

    public AcometidaNewAdaptador(Context context, List<AcometidaNueva> acometidaNuevas) {
        this.context = context;
        this.acometidaNuevas = acometidaNuevas;
    }

    public void setListatipoGabinete(List<GenericBeanOutRO> listaTipoGabinete) {
        this.listaTipoGabinete = listaTipoGabinete;
    }

    public void setListaProyectoInstalacion(List<GabineteProyectoInstalacionOutRo> listaProyectoInstalacion) {
        this.listaProyectoInstalacion = listaProyectoInstalacion;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void setAcometidaDao(AcometidaDao acometidaDao) {
        this.acometidaDao = acometidaDao;
    }

    public void setAcometidas(ListView listPuntos) {
        this.listPuntos = listPuntos;
    }

    public void setAprueba(SwitchCompat aprueba) {
        this.aprueba = aprueba;
    }

    public void setBtnVis1_grabar(Button btnIns1_grabar) {
        this.btnIns1_grabar = btnIns1_grabar;
    }

    public void setAcometidaNuevaDao(AcometidaNuevaDao acometidaNuevaDao) {
        this.acometidaNuevaDao = acometidaNuevaDao;
    }

    public void setId(int Id){
        this.Id = Id;
    }

    @Override
    public int getCount() {
        if (acometidaNuevas.size() == 0) {
            aprueba.setClickable(false);
        }
        return this.acometidaNuevas.size();
    }

    public AcometidaNueva getAcometidanueva(int position) {
        for (AcometidaNueva acometidaNueva : acometidaNuevas) {
            if (acometidaNueva.getPosicion() == position) {
                return acometidaNueva;
            }
        }
        return null;
    }

    @Override
    public Object getItem(int position) {
        return this.acometidaNuevas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {
        View rowView = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = layoutInflater.inflate(R.layout.item_acometida_nueva, parent, false);

            holder.spiAco_tipogabinete = rowView.findViewById(R.id.spiAco_tipogabinete);
            holder.imgAco_gabinete = rowView.findViewById(R.id.imgAco_gabinete);
            holder.icoAco_gabinete = rowView.findViewById(R.id.icoAco_gabinete);
            holder.imgMinimizar = rowView.findViewById(R.id.imgMinimizar);
            holder.txtAcom_gabinete = rowView.findViewById(R.id.txtAcom_gabinete);
            holder.txtGab_numero = rowView.findViewById(R.id.txtGab_numero);
            holder.txtAcom_gab_nro = rowView.findViewById(R.id.txtAcom_gab_nro);
            holder.llGabinete = rowView.findViewById(R.id.llGabinete);
            holder.icoAco_tipogabinete = rowView.findViewById(R.id.icoAco_tipogabinete);

            rowView.setTag(holder);

        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        holder.ref = position;
        acometidaNuevas.get(position).setPosicion(position);

        /*holder.remPunI_punto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getCount() > 1) {
                    puntoDao.delete(puntos.get(holder.ref));
                    puntos.remove(holder.ref);
                    notifyDataSetChanged();
                    Util.setListViewHeightBasedOnChildren(listPuntos);
                    check();
                } else {
                    ((App) context.getApplicationContext()).showToast("Se debe registrar un punto como mínimo");
                }
            }
        });*/

        String recurso = rowView.getResources().getString(R.string.txtTuberia_gabinetenro);
        String formateada = String.format(recurso, String.valueOf(position+1));
        holder.txtGab_numero.setText(formateada);
        holder.txtAcom_gabinete.setText(listaProyectoInstalacion.get(holder.ref).getClasificacionGabinete().getNombreClasificacionGabinete());
        holder.txtAcom_gab_nro.setText("("+listaProyectoInstalacion.get(holder.ref).getClasificacionGabinete().getValorClasificacionGabinete()+")");

        //Gabinete
        holder.spiAco_tipogabinete.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //System.out.println("rest: " +listaTipoResultado.get(position).getLabel());
                holder.check++;
                if (holder.check > 1) {
                    if (position != 0) {
                        acometidaNuevas.get(holder.ref).setTipoGabinete(setTipoGabinete(parent.getSelectedItem().toString()));
                    } else {
                        acometidaNuevas.get(holder.ref).setTipoGabinete("");
                    }
                    acometidaNuevaDao.insertOrReplace(acometidaNuevas.get(holder.ref));
                    validarGabinete(holder);
                    check();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ArrayList<String> listresultado = new ArrayList<>();
        listresultado.add("Seleccionar resultado");
        for (GenericBeanOutRO genericBeanOutRO : listaTipoGabinete) {
            listresultado.add(genericBeanOutRO.getLabel());
        }

        ArrayAdapter<String> adapterresultado = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, listresultado);
        adapterresultado.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        holder.spiAco_tipogabinete.setAdapter(adapterresultado);

        if (!"".equalsIgnoreCase(acometidaNuevas.get(holder.ref).getTipoGabinete())) {
            holder.spiAco_tipogabinete.setSelection(getTipoGabinete(acometidaNuevas.get(holder.ref).getTipoGabinete()));
            validarGabinete(holder);
        }


        holder.imgAco_gabinete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("position: " + position);
                callback.clicFoto2(position);
            }
        });

        if (!"".equalsIgnoreCase(acometidaNuevas.get(holder.ref).getFotoGabinete())) {
            loadBitmap(acometidaNuevas.get(holder.ref).getFotoGabinete(), holder.imgAco_gabinete);
            validarFotoGabinete(holder);
            check();
        } else {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(65, 65);
            layoutParams.setMargins(10, 10, 30, 10);
            holder.imgAco_gabinete.setLayoutParams(layoutParams);
            holder.imgAco_gabinete.setScaleType(ImageView.ScaleType.FIT_CENTER);
            holder.imgAco_gabinete.setImageResource(R.mipmap.ic_add_a_photo_black_36dp);
        }

        holder.imgMinimizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mini == false){
                    holder.llGabinete.setVisibility(View.GONE);
                    holder.imgMinimizar.setImageResource(R.mipmap.ic_add_black_36dp);
                    mini = true;
                }else{
                    holder.llGabinete.setVisibility(View.VISIBLE);
                    holder.imgMinimizar.setImageResource(R.mipmap.ic_remove_black_36dp);
                    mini = false;
                }

            }
        });

        validar(holder);
        check();
        return rowView;
    }

    private void validar(ViewHolder holder) {
        if ("".equalsIgnoreCase(acometidaNuevas.get(holder.ref).getTipoGabinete()) ||
                "".equalsIgnoreCase(acometidaNuevas.get(holder.ref).getFotoGabinete())) {
            btnIns1_grabar.setEnabled(false);
            btnIns1_grabar.setClickable(false);
        } else {
            btnIns1_grabar.setEnabled(true);
            btnIns1_grabar.setClickable(true);
        }
    }

    private void validarGabinete(ViewHolder holder) {
        if ("".equalsIgnoreCase(acometidaNuevas.get(holder.ref).getTipoGabinete())) {
            holder.icoAco_tipogabinete.setImageResource(R.mipmap.ic_warning_black_36dp);
            //aprueba.setChecked(false);
        } else {
            holder.icoAco_tipogabinete.setImageResource(R.mipmap.ic_done_black_36dp);
        }
    }

    private void validarFotoGabinete(ViewHolder holder) {
        if ("".equalsIgnoreCase(acometidaNuevas.get(holder.ref).getFotoGabinete())) {
            holder.icoAco_gabinete.setImageResource(R.mipmap.ic_warning_black_36dp);
            //aprueba.setChecked(false);
        } else {
            holder.icoAco_gabinete.setImageResource(R.mipmap.ic_done_black_36dp);
        }
    }

    private void check() {
        int i = 0;
        boolean boton = true;
        while (i < acometidaNuevas.size()) {
            AcometidaNueva acometidaNueva = acometidaNuevas.get(i);

            if ("".equalsIgnoreCase(acometidaNueva.getTipoGabinete()) ||
                    "".equalsIgnoreCase(acometidaNueva.getFotoGabinete())
            ) {
                boton = false;
                i = acometidaNuevas.size();
            }
            i++;
        }
        btnIns1_grabar.setEnabled(boton);
        btnIns1_grabar.setClickable(boton);
    }

    /*private String setTipoFusionista(String tipoFusionista) {
        for (BaseInspectorOutRO genericBeanOutRO : listaTipoFusionista) {
            if (listaTipoFusionista.equalsIgnoreCase(genericBeanOutRO.getNombreInspector())) {
                return genericBeanOutRO.getNombreInspector();
            }
        }
        return null;
    }

    private int getTipoRFusionista(String tipoFusionista) {
        int i = 0;
        boolean entro = false;
        for (BaseInspectorOutRO genericBeanOutRO : listaTipoFusionista) {
            if (tipoFusionista.equalsIgnoreCase(genericBeanOutRO.getNombreInspector())) {
                entro = true;
                break;
            }
            i++;
        }
        if (entro) {
            return i + 1;
        } else {
            return 0;
        }
    }*/

    private String setTipoGabinete(String tipoGabinete) {
        for (GenericBeanOutRO genericBeanOutRO : listaTipoGabinete) {
            if (tipoGabinete.equalsIgnoreCase(genericBeanOutRO.getLabel())) {
                return genericBeanOutRO.getValue();
            }
        }
        return null;
    }

    private int getTipoGabinete(String tipoGabinete) {
        int i = 0;
        boolean entro = false;
        for (GenericBeanOutRO genericBeanOutRO : listaTipoGabinete) {
            if (tipoGabinete.equalsIgnoreCase(genericBeanOutRO.getValue())) {
                entro = true;
                break;
            }
            i++;
        }
        if (entro) {
            return i + 1;
        } else {
            return 0;
        }
    }

    public interface Callback {
        void clicFoto2(int position);
    }

    private class ViewHolder {
        int ref;
        Spinner spiAco_tipogabinete;
        ImageView imgAco_gabinete,icoAco_gabinete,icoAco_tipogabinete,imgMinimizar;
        TextView txtAcom_gabinete,txtGab_numero,txtAcom_gab_nro;
        LinearLayout llGabinete;
        int check;
    }

    public void loadBitmap(String ruta, final ImageView imageView) {
        MostrarFotoTask mostrarFotoTask = new MostrarFotoTask(new MostrarFotoTask.OnMostrarFotoTaskCompleted() {
            @Override
            public void onMostrarFotoTaskCompleted(Bitmap bitmap) {
                if (bitmap != null) {
                    if (imageView != null) {
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(120, 120);
                        layoutParams.setMargins(10, 10, 10, 10);
                        imageView.setLayoutParams(layoutParams);
                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        imageView.setImageBitmap(bitmap);
                    }
                } else {
                    ((App) context.getApplicationContext()).showToast("Error al cargar la foto");
                }
            }
        });
        mostrarFotoTask.execute(ruta);
    }

}
