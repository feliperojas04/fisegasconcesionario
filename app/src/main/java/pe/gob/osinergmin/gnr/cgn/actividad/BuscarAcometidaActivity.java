package pe.gob.osinergmin.gnr.cgn.actividad;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.BaseInstalacionAcometidaOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.BaseOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.InstalacionAcometidaOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.ParametroOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.SolicitudOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListBaseInstalacionAcometidaOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListParametroOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.adaptador.AcometidaAdaptador;
import pe.gob.osinergmin.gnr.cgn.adaptador.VacioAdaptador;
import pe.gob.osinergmin.gnr.cgn.data.models.Acometida;
import pe.gob.osinergmin.gnr.cgn.data.models.AcometidaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.ConfigDao;
import pe.gob.osinergmin.gnr.cgn.data.models.MInstalacionAcometidaOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MInstalacionAcometidaOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MSolicitudOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MSolicitudOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MontanteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.SuministroDao;
import pe.gob.osinergmin.gnr.cgn.task.AcometidaTask;
import pe.gob.osinergmin.gnr.cgn.task.ListaBaseAcometida;
import pe.gob.osinergmin.gnr.cgn.task.Logout;
import pe.gob.osinergmin.gnr.cgn.task.ParametroAll;
import pe.gob.osinergmin.gnr.cgn.util.DownTimer;
import pe.gob.osinergmin.gnr.cgn.util.NetworkUtil;
import pe.gob.osinergmin.gnr.cgn.util.Parse;
import pe.gob.osinergmin.gnr.cgn.util.SimpleDividerItemDecoration;

public class BuscarAcometidaActivity extends BaseActivity implements AcometidaAdaptador.CallBack {

    private static final int PAGE_SIZE = 20;
    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private String idtipoacametida;
    private RecyclerView listaAcometida;
    private List<MInstalacionAcometidaOutRO> mInstalacionAcometidaOutROS = new ArrayList<>();

    private Toolbar toolbar;
    private Config config;
    private DownTimer countDownTimer;
    private boolean abierto = true, isLoading = false, isLastPage = false;

    private ConfigDao configDao;
    private SuministroDao suministroDao;
    private AcometidaDao acometidaDao;
    private MontanteDao montanteDao;
    private MParametroOutRODao mParametroOutRODao;
    private MInstalacionAcometidaOutRODao mInstalacionAcometidaOutRODao;
    private MSolicitudOutRODao mSolicitudOutRODao;

    private TextView user, preVisitas, numPre, tv_acometida, numAco, tv_montante, numMon, sincronizacion, cerrar, configModo;
    private ImageView desConfig, configVacio, imageSync;
    private SwitchCompat offline;
    private AcometidaAdaptador acometidaAdaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_acometida_nav);

        countDownTimer = DownTimer.getInstance();
        acometidaAdaptador = new AcometidaAdaptador();
        configDao = ((App) getApplication()).getDaoSession().getConfigDao();
        suministroDao = ((App) getApplication()).getDaoSession().getSuministroDao();
        acometidaDao = ((App) getApplication()).getDaoSession().getAcometidaDao();
        montanteDao = ((App) getApplication()).getDaoSession().getMontanteDao();
        mParametroOutRODao = ((App) getApplication()).getDaoSession().getMParametroOutRODao();
        mInstalacionAcometidaOutRODao = ((App) getApplication()).getDaoSession().getMInstalacionAcometidaOutRODao();
        mSolicitudOutRODao = ((App) getApplication()).getDaoSession().getMSolicitudOutRODao();
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.nav_open, R.string.nav_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        user = header.findViewById(R.id.nav_usuario);
        preVisitas = header.findViewById(R.id.preVisitas);
        numPre = header.findViewById(R.id.numPre);
        tv_acometida = header.findViewById(R.id.tv_acometida);
        numAco = header.findViewById(R.id.numAco);
        tv_montante = header.findViewById(R.id.tv_montante);
        numMon = header.findViewById(R.id.numMon);
        sincronizacion = header.findViewById(R.id.sincronizacion);
        imageSync = header.findViewById(R.id.imageSync);
        desConfig = header.findViewById(R.id.desConfig);
        configVacio = header.findViewById(R.id.configVacio);
        configModo = header.findViewById(R.id.configModo);
        offline = header.findViewById(R.id.offline);
        cerrar = header.findViewById(R.id.cerrar);

        listaAcometida = findViewById(R.id.listaSuministros);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        listaAcometida.setLayoutManager(mLayoutManager);
        listaAcometida.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext(), R.drawable.line_divider_black));
        listaAcometida.setItemAnimator(new DefaultItemAnimator());
        ImageView recargaAcometida = findViewById(R.id.recargaSuministro);

        assert recargaAcometida != null;
        recargaAcometida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (config.getOffline()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(BuscarAcometidaActivity.this);
                    builder.setTitle("Aviso");
                    builder.setMessage("Para obtener datos actualizados debe activar el modo ONLINE, lo cual puede hacer por medio de la opción \"Configuracion\".");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Entendido", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.show();
                } else {
                    if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        initRecycler();
                        CallListaBaseAcometida(currentPage);
                    } else {
                        ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                    }
                }
            }
        });

        listaAcometida.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!config.getOffline()) {
                    if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        int visibleItemCount = mLayoutManager.getChildCount();
                        int totalItemCount = mLayoutManager.getItemCount();
                        int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                        if (!isLoading && !isLastPage) {
                            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                                    && firstVisibleItemPosition >= 0
                                    && totalItemCount >= PAGE_SIZE) {
                                isLoading = true;
                                CallListaBaseAcometida(currentPage);
                            }
                        }
                    } else {
                        ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                    }
                }
            }
        });

        System.out.println("Aqui-11");
    }


    private void cargarAcometida(int posicion) {
        System.out.println("Aqui-0");
        InstalacionAcometidaOutRO instalacionAcometidaOutRO = Parse.getInstalacionAcometidaOutRO(mInstalacionAcometidaOutROS.get(posicion));
        MSolicitudOutRO mSolicitudOutRO = mSolicitudOutRODao.queryBuilder().where(MSolicitudOutRODao.Properties.IdSolicitud.eq(instalacionAcometidaOutRO.getIdSolicitud())).limit(1).unique();
        SolicitudOutRO solicitudOutRO = Parse.getSolicitudOutRO(mSolicitudOutRO);
        instalacionAcometidaOutRO.setSolicitud(solicitudOutRO);

        System.out.println("Id: " + solicitudOutRO.getNumeroInstalacionConcesionaria());
        System.out.println("Aqui0");

        List acometidaList = acometidaDao.queryBuilder().where(AcometidaDao.Properties.IdInstalacionAcometida.eq(instalacionAcometidaOutRO.getIdInstalacionAcometida())).list();
        if (acometidaList.size() > 0) {
            Acometida acometida = (Acometida) acometidaList.get(0);

            if(idtipoacametida.equals("T")){
                Intent intent = new Intent(BuscarAcometidaActivity.this, Acometida2Activity.class);
                intent.putExtra("ACOMETIDA", acometida);
                intent.putExtra("CONFIG", config);
                intent.putExtra("NumTub", String.valueOf(instalacionAcometidaOutRO.getSolicitud().getNumeroTuberiaConexionProyectoInstalacion()));
                intent.putExtra("idInstalacion", String.valueOf(instalacionAcometidaOutRO.getSolicitud().getIdProyectoInstalacion()));
                intent.putExtra("tipo", "T");
                startActivity(intent);
                finish();
            }else{
                Intent intent = new Intent(BuscarAcometidaActivity.this, Acometida2Activity.class);
                intent.putExtra("ACOMETIDA", acometida);
                intent.putExtra("CONFIG", config);
                intent.putExtra("NumTub", String.valueOf(instalacionAcometidaOutRO.getSolicitud().getNumeroTuberiaConexionProyectoInstalacion()));
                intent.putExtra("idInstalacion", String.valueOf(instalacionAcometidaOutRO.getSolicitud().getIdProyectoInstalacion()));
                intent.putExtra("tipo", "A");
                startActivity(intent);
                finish();
            }

        } else {
            Acometida acometida = new Acometida(null,
                    instalacionAcometidaOutRO.getIdInstalacionAcometida() != null ? instalacionAcometidaOutRO.getIdInstalacionAcometida() : 0,
                    instalacionAcometidaOutRO.getDocumentoIdentificacionSolicitante() != null ? instalacionAcometidaOutRO.getDocumentoIdentificacionSolicitante() : "",
                    instalacionAcometidaOutRO.getNombreSolicitante() != null ? instalacionAcometidaOutRO.getNombreSolicitante() : "",
                    instalacionAcometidaOutRO.getDireccionUbigeoPredio() != null ? instalacionAcometidaOutRO.getDireccionUbigeoPredio() : "",
                    instalacionAcometidaOutRO.getNumeroIntentoInstalacionAcometida() != null ? instalacionAcometidaOutRO.getNumeroIntentoInstalacionAcometida().toString() : "",
                    instalacionAcometidaOutRO.getCodigoSolicitud() != null ? instalacionAcometidaOutRO.getCodigoSolicitud().toString() : "",
                    instalacionAcometidaOutRO.getNumeroContratoConcesionariaPredio() != null ? instalacionAcometidaOutRO.getNumeroContratoConcesionariaPredio() : "",
                    instalacionAcometidaOutRO.getSolicitud().getFechaSolicitud() != null ? instalacionAcometidaOutRO.getSolicitud().getFechaSolicitud() : "",
                    instalacionAcometidaOutRO.getSolicitud().getFechaAprobacionSolicitud() != null ? instalacionAcometidaOutRO.getSolicitud().getFechaAprobacionSolicitud() : "",
                    "",
                    instalacionAcometidaOutRO.getSolicitud().getNombreEstadoTuberiaConexionAcometida() != null ? instalacionAcometidaOutRO.getSolicitud().getNombreEstadoTuberiaConexionAcometida() : "",
                    instalacionAcometidaOutRO.getSolicitud().getNombreTipoGabinete() != null ? instalacionAcometidaOutRO.getSolicitud().getNombreTipoGabinete() : "",
                    instalacionAcometidaOutRO.getSolicitud().getNombreTipoAcometida() != null ? instalacionAcometidaOutRO.getSolicitud().getNombreTipoAcometida() : "",
                    "",
                    instalacionAcometidaOutRO.getNumeroSuministroPredio() != null ? instalacionAcometidaOutRO.getNumeroSuministroPredio() : "",
                    "", "", "", "", "", "", "", "", "", false,
                    instalacionAcometidaOutRO.getNombreTipoInstalacionAcometida(),
                    instalacionAcometidaOutRO.getTipoInstalacionAcometida(), "",false);
            acometidaDao.insertOrReplace(acometida);

            if(idtipoacametida.equals("T")){
                Intent intent = new Intent(BuscarAcometidaActivity.this, Acometida2Activity.class);
                intent.putExtra("ACOMETIDA", acometida);
                intent.putExtra("CONFIG", config);
                intent.putExtra("NumTub", String.valueOf(instalacionAcometidaOutRO.getSolicitud().getNumeroTuberiaConexionProyectoInstalacion()));
                intent.putExtra("idInstalacion", String.valueOf(instalacionAcometidaOutRO.getSolicitud().getIdProyectoInstalacion()));
                intent.putExtra("tipo", "T");
                startActivity(intent);
                finish();
            }else{
                Intent intent = new Intent(BuscarAcometidaActivity.this, Acometida2Activity.class);
                intent.putExtra("ACOMETIDA", acometida);
                intent.putExtra("CONFIG", config);
                intent.putExtra("NumTub", String.valueOf(instalacionAcometidaOutRO.getSolicitud().getNumeroTuberiaConexionProyectoInstalacion()));
                intent.putExtra("idInstalacion", String.valueOf(instalacionAcometidaOutRO.getSolicitud().getIdProyectoInstalacion()));
                intent.putExtra("tipo", "A");
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.createAndStart();
        toolbar.setTitle("Acometidas y TCs");

        config = configDao.queryBuilder().limit(1).unique();
        config.setTexto("");
        user.setText(config.getUsername());
        setupMenu();
        setMenuCounter();
        initRecycler();
        if (config.getOffline()) {
            mInstalacionAcometidaOutROS = mInstalacionAcometidaOutRODao.queryBuilder().orderDesc(MInstalacionAcometidaOutRODao.Properties.ResultCode).list();
            listaAcometida(mInstalacionAcometidaOutROS);
        } else {
            if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                CallListaBaseAcometida(currentPage);
            } else {
                ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
            }
        }
    }

    public void setupMenu() {
        preVisitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuscarAcometidaActivity.this, BuscarSuministroActivity.class);
                startActivity(intent);
                finish();
            }
        });
        tv_acometida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        tv_montante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuscarAcometidaActivity.this, BuscarMontanteActivity.class);
                startActivity(intent);
                finish();
            }
        });
        sincronizacion.setVisibility(config.getOffline() ? View.VISIBLE : View.GONE);
        imageSync.setVisibility(config.getOffline() ? View.VISIBLE : View.GONE);
        sincronizacion.setText(config.getSync() ? "Sincronizando..." : "Sincronización");
        sincronizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuscarAcometidaActivity.this, SyncActivity.class);
                startActivity(intent);
            }
        });
        offline.setChecked(config.getOffline());
        desConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (abierto) {
                    configVacio.setVisibility(View.VISIBLE);
                    configModo.setVisibility(View.VISIBLE);
                    offline.setVisibility(View.VISIBLE);
                    abierto = false;
                } else {
                    configVacio.setVisibility(View.GONE);
                    configModo.setVisibility(View.GONE);
                    offline.setVisibility(View.GONE);
                    abierto = true;
                }
            }
        });
        offline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuscarAcometidaActivity.this, ConfiguracionActivity.class);
                startActivity(intent);
                finish();
            }
        });
        cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cerrarSesion();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_busqueda_suministro, menu);
        MenuItem searchItem = menu.findItem(R.id.menuBusS_buscar);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        String buscar = getResources().getString(R.string.txtBus_filtro);
        searchView.setQueryHint(buscar);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                config.setTexto(query);
                if (config.getOffline()) {
                    acometidaAdaptador.clear();
                    mInstalacionAcometidaOutROS = mInstalacionAcometidaOutRODao.queryBuilder().whereOr(
                            MInstalacionAcometidaOutRODao.Properties.CodigoSolicitud.like("%" + query + "%"),
                            MInstalacionAcometidaOutRODao.Properties.DireccionUbigeoPredio.like("%" + query + "%"),
                            MInstalacionAcometidaOutRODao.Properties.DocumentoIdentificacionSolicitante.like("%" + query + "%"),
                            MInstalacionAcometidaOutRODao.Properties.NombreSolicitante.like("%" + query + "%"),
                            MInstalacionAcometidaOutRODao.Properties.NumeroSuministroPredio.like("%" + query + "%"),
                            MInstalacionAcometidaOutRODao.Properties.NumeroContratoConcesionariaPredio.like("%" + query + "%")).list();
                    listaAcometida(mInstalacionAcometidaOutROS);
                } else {
                    if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        initRecycler();
                        CallListaBaseAcometida(currentPage);
                    } else {
                        ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                    }
                }
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                config.setTexto("");
                if (config.getOffline()) {
                    acometidaAdaptador.clear();
                    mInstalacionAcometidaOutROS = mInstalacionAcometidaOutRODao.queryBuilder().orderDesc(MInstalacionAcometidaOutRODao.Properties.ResultCode).list();
                    listaAcometida(mInstalacionAcometidaOutROS);
                } else {
                    if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        initRecycler();
                        CallListaBaseAcometida(currentPage);
                    } else {
                        ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                    }
                }
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    public void listaAcometida(List<MInstalacionAcometidaOutRO> mInstalacionAcometidaOutROS) {
        initRecycler();
        List<BaseInstalacionAcometidaOutRO> baseInstalacionAcometidaOutROS = new ArrayList<>();
        for (MInstalacionAcometidaOutRO mInstalacionAcometidaOutRO : mInstalacionAcometidaOutROS) {
            baseInstalacionAcometidaOutROS.add(Parse.getBaseInstalacionAcometidaOutRO(mInstalacionAcometidaOutRO));
        }
        if (baseInstalacionAcometidaOutROS.size() > 0) {
            mostrarDatos(baseInstalacionAcometidaOutROS);
            baseInstalacionAcometidaOutROS.get(0).getNombreTipoInstalacionAcometida();
        } else {
            mostrarSin();
        }
    }

    public void initRecycler() {
        acometidaAdaptador.clear();
        acometidaAdaptador.setCallback(this);
        listaAcometida.setAdapter(acometidaAdaptador);
        currentPage = PAGE_START;
        isLastPage = false;
    }

    public void mostrarDatos(List<BaseInstalacionAcometidaOutRO> baseInstalacionAcometidaOutROS) {
        acometidaAdaptador.add(baseInstalacionAcometidaOutROS);
    }

    public void mostrarSin() {
        listaAcometida.setAdapter(new VacioAdaptador(getResources().getString(R.string.txtAco_sinAcometida)));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent intent = new Intent(BuscarAcometidaActivity.this, PrincipalActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        countDownTimer.createAndStart();
    }

    private void setMenuCounter() {
        long previsita = suministroDao
                .queryBuilder()
                .where(SuministroDao.Properties.Grabar.eq(true)).count();
        long acometida = acometidaDao
                .queryBuilder()
                .where(AcometidaDao.Properties.Grabar.eq(true)).count();
        long montante = montanteDao
                .queryBuilder()
                .where(MontanteDao.Properties.Grabar.eq(true)).count();
        if (previsita > 0) {
            numPre.setText(String.valueOf(previsita));
        } else {
            numPre.setText("");
        }
        if (acometida > 0) {
            numAco.setText(String.valueOf(acometida));
        } else {
            numAco.setText("");
        }
        if (montante > 0) {
            numMon.setText(String.valueOf(montante));
        } else {
            numMon.setText("");
        }
    }

    private void cerrarSesion() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(BuscarAcometidaActivity.this);
        builder.setTitle(R.string.app_msjTitulo);
        builder.setMessage(R.string.app_msjCuerpo);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.app_msjSi, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (config.getOffline()) {
                    config.setLogin(false);
                    configDao.insertOrReplace(config);
                    Intent intent = new Intent(BuscarAcometidaActivity.this, IniciarActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        showProgressDialog(R.string.app_cargando);
                        Logout logout = new Logout(new Logout.OnLogoutCompleted() {
                            @Override
                            public void onLogoutCompled(BaseOutRO baseOutRO) {
                                hideProgressDialog();
                                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                                } else if (baseOutRO == null) {
                                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                                } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(baseOutRO.getResultCode())) {
                                    ((App) getApplication()).showToast(baseOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : baseOutRO.getMessage());
                                } else {
                                    countDownTimer.stop();
                                    config.setLogin(false);
                                    configDao.insertOrReplace(config);
                                    Intent intent = new Intent(BuscarAcometidaActivity.this, IniciarActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        });
                        logout.execute(config);
                    } else {
                        ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                    }
                }
            }
        });
        builder.setNegativeButton(R.string.app_msjNo, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    @Override
    public void onInstalacionEliminar(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(BuscarAcometidaActivity.this);
        builder.setTitle(R.string.app_msjTitulo);
        builder.setMessage("¿Está seguro que desea eliminar la Acometida?");
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.app_msjSi, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                BaseInstalacionAcometidaOutRO baseInstalacionAcometidaOutRO = acometidaAdaptador.getItem(position);
                Acometida acometida = acometidaDao.queryBuilder().where(AcometidaDao.Properties.IdInstalacionAcometida.eq(baseInstalacionAcometidaOutRO.getIdInstalacionAcometida())).limit(1).unique();
                acometida.setGrabar(false);
                acometidaDao.insertOrReplace(acometida);
                MInstalacionAcometidaOutRO mInstalacionAcometidaOutRO = mInstalacionAcometidaOutRODao.queryBuilder().where(MInstalacionAcometidaOutRODao.Properties
                        .IdInstalacionAcometida.eq(baseInstalacionAcometidaOutRO.getIdInstalacionAcometida())).limit(1).unique();
                mInstalacionAcometidaOutRO.setResultCode("101");
                mInstalacionAcometidaOutRODao.insertOrReplace(mInstalacionAcometidaOutRO);
                acometidaAdaptador.getItem(position).setResultCode("101");
                acometidaAdaptador.notifyItemChanged(position);
                setMenuCounter();
            }
        });
        builder.setNegativeButton(R.string.app_msjNo, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    @Override
    public void onSelect(BaseInstalacionAcometidaOutRO baseInstalacionAcometidaOutRO, int position) {
        config.setIdInstalacionAcometida(baseInstalacionAcometidaOutRO.getIdInstalacionAcometida().toString());
        if (config.getOffline()) {
            System.out.println("Aqui12");
            System.out.println("Aqui1: "+ baseInstalacionAcometidaOutRO.getNombreTipoInstalacionAcometida());
            System.out.println("Aqui2: "+ baseInstalacionAcometidaOutRO.getTipoInstalacionAcometida());
            idtipoacametida = baseInstalacionAcometidaOutRO.getTipoInstalacionAcometida();
            cargarAcometida(position);

        } else {
            if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                System.out.println("Aqui13");
                System.out.println("Aqui1.1: "+ baseInstalacionAcometidaOutRO.getNombreTipoInstalacionAcometida());
                System.out.println("Aqui2.1: "+ baseInstalacionAcometidaOutRO.getTipoInstalacionAcometida());
                idtipoacametida = baseInstalacionAcometidaOutRO.getTipoInstalacionAcometida();
                CallParametro();
            } else {
                System.out.println("Aqui14");
                ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
            }
        }
    }

    private void CallListaBaseAcometida(final int actualPage) {
        showProgressDialog(R.string.app_cargando);
        ListaBaseAcometida listaBaseAcometida = new ListaBaseAcometida(new ListaBaseAcometida.OnListBaseInstalacionAcometidaCompleted() {
            @Override
            public void onListBaseInstalacionAcometidaCompled(ListBaseInstalacionAcometidaOutRO listBaseInstalacionAcometidaOutRO) {
                hideProgressDialog();
                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                } else if (listBaseInstalacionAcometidaOutRO == null) {
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listBaseInstalacionAcometidaOutRO.getResultCode())) {
                    ((App) getApplication()).showToast(listBaseInstalacionAcometidaOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listBaseInstalacionAcometidaOutRO.getMessage());
                } else {
                    if (listBaseInstalacionAcometidaOutRO.getInstalacionAcometida() != null && listBaseInstalacionAcometidaOutRO.getInstalacionAcometida().size() > 0) {
                        mostrarDatos(listBaseInstalacionAcometidaOutRO.getInstalacionAcometida());
                    } else {
                        isLastPage = true;
                        if (actualPage == PAGE_START) {
                            mostrarSin();
                        }
                    }
                    currentPage += 1;
                    isLoading = false;
                }
            }
        });
        Config config1 = config;
        config1.setErrorPrevisita(actualPage); // pagina
        config1.setErrorVisita(PAGE_SIZE); // elementos por pagina
        listaBaseAcometida.execute(config1);
    }

    private void CallParametro() {
        showProgressDialog(R.string.app_cargando);
        ParametroAll parametroAll = new ParametroAll(new ParametroAll.OnParametroAllCompleted() {
            @Override
            public void onParametroAllCompled(ListParametroOutRO listParametroOutRO) {
                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                } else if (listParametroOutRO == null) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listParametroOutRO.getResultCode())) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast(listParametroOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listParametroOutRO.getMessage());
                } else {
                    List<ParametroOutRO> parametroOutROList = listParametroOutRO.getParametro();
                    for (ParametroOutRO parametroOutRO : parametroOutROList) {
                        switch (parametroOutRO.getNombreParametro()) {
                            case Constantes.NOMBRE_PARAMETRO_VALIDACION_ACTIVA_DISTANCIA_INSPECTOR_PREDIO:
                                config.setParametro1(parametroOutRO.getValorParametro());
                                break;
                            case Constantes.NOMBRE_PARAMETRO_DISTANCIA_MAXIMA_INSPECTOR_PREDIO:
                                config.setParametro2(parametroOutRO.getValorParametro());
                                break;
                            case Constantes.NOMBRE_PARAMETRO_SOLICITUD_ACTIVA_CODIGO_ESPECIAL_ACCESO:
                                config.setParametro3(parametroOutRO.getValorParametro());
                                break;
                            case Constantes.NOMBRE_PARAMETRO_TELEFONO_SOLICITUD_CODIGO_ESPECIAL_ACCESO:
                                config.setParametro4(parametroOutRO.getValorParametro());
                                break;
                            case Constantes.NOMBRE_PARAMETRO_CONFIGURACION_COMPRESION_FOTOS_APLICATIVO_MOVIL:
                                config.setParametro5(parametroOutRO.getValorParametro());
                                break;
                            case Constantes.NOMBRE_PARAMETRO_FRECUENCIA_TOMA_COORDENADAS_AUDITORIA:
                                config.setPrecision(parametroOutRO.getValorParametro());
                                break;
                        }
                    }
                    CallAcometida();
                }
            }
        });
        parametroAll.execute(config);
    }

    private void CallAcometida() {
        AcometidaTask acometidaTask = new AcometidaTask(new AcometidaTask.OnInstalacionAcometidaCompleted() {
            @Override
            public void onInstalacionAcometidaCompled(InstalacionAcometidaOutRO instalacionAcometidaOutRO) {
                hideProgressDialog();
                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                } else if (instalacionAcometidaOutRO == null) {
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(instalacionAcometidaOutRO.getResultCode())) {
                    ((App) getApplication()).showToast(instalacionAcometidaOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : instalacionAcometidaOutRO.getMessage());
                } else if (instalacionAcometidaOutRO.getIdInstalacionAcometida() == null) {
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                } else {

                    List acometidaList = acometidaDao.queryBuilder().where(AcometidaDao.Properties.IdInstalacionAcometida.eq(instalacionAcometidaOutRO.getIdInstalacionAcometida())).list();
                    if (acometidaList.size() > 0) {
                        Acometida acometida = (Acometida) acometidaList.get(0);

                        if(idtipoacametida.equals("T")){
                            Intent intent = new Intent(BuscarAcometidaActivity.this, Acometida2Activity.class);
                            intent.putExtra("ACOMETIDA", acometida);
                            intent.putExtra("CONFIG", config);
                            intent.putExtra("NumTub", String.valueOf(instalacionAcometidaOutRO.getSolicitud().getNumeroTuberiaConexionProyectoInstalacion()));
                            intent.putExtra("idInstalacion", String.valueOf(instalacionAcometidaOutRO.getSolicitud().getIdProyectoInstalacion()));
                            config.setIdProyectoInstalacion(String.valueOf(instalacionAcometidaOutRO.getSolicitud().getIdProyectoInstalacion()));
                            System.out.println(String.valueOf(instalacionAcometidaOutRO.getSolicitud().getIdProyectoInstalacion()));
                            intent.putExtra("tipo", "T");
                            startActivity(intent);
                            finish();
                        }else{
                            Intent intent = new Intent(BuscarAcometidaActivity.this, Acometida2Activity.class);
                            intent.putExtra("ACOMETIDA", acometida);
                            intent.putExtra("CONFIG", config);
                            intent.putExtra("NumTub", String.valueOf(instalacionAcometidaOutRO.getSolicitud().getNumeroTuberiaConexionProyectoInstalacion()));
                            intent.putExtra("idInstalacion", String.valueOf(instalacionAcometidaOutRO.getSolicitud().getIdProyectoInstalacion()));
                            config.setIdProyectoInstalacion(String.valueOf(instalacionAcometidaOutRO.getSolicitud().getIdProyectoInstalacion()));
                            System.out.println(String.valueOf(instalacionAcometidaOutRO.getSolicitud().getIdProyectoInstalacion()));
                            intent.putExtra("tipo", "A");
                            startActivity(intent);
                            finish();
                        }

                    } else {
                        Acometida acometida = new Acometida(null,
                                instalacionAcometidaOutRO.getIdInstalacionAcometida() != null ? instalacionAcometidaOutRO.getIdInstalacionAcometida() : 0,
                                instalacionAcometidaOutRO.getDocumentoIdentificacionSolicitante() != null ? instalacionAcometidaOutRO.getDocumentoIdentificacionSolicitante() : "",
                                instalacionAcometidaOutRO.getNombreSolicitante() != null ? instalacionAcometidaOutRO.getNombreSolicitante() : "",
                                instalacionAcometidaOutRO.getDireccionUbigeoPredio() != null ? instalacionAcometidaOutRO.getDireccionUbigeoPredio() : "",
                                instalacionAcometidaOutRO.getNumeroIntentoInstalacionAcometida() != null ? instalacionAcometidaOutRO.getNumeroIntentoInstalacionAcometida().toString() : "",
                                instalacionAcometidaOutRO.getCodigoSolicitud() != null ? instalacionAcometidaOutRO.getCodigoSolicitud().toString() : "",
                                instalacionAcometidaOutRO.getNumeroContratoConcesionariaPredio() != null ? instalacionAcometidaOutRO.getNumeroContratoConcesionariaPredio() : "",
                                instalacionAcometidaOutRO.getSolicitud().getFechaSolicitud() != null ? instalacionAcometidaOutRO.getSolicitud().getFechaSolicitud() : "",
                                instalacionAcometidaOutRO.getSolicitud().getFechaAprobacionSolicitud() != null ? instalacionAcometidaOutRO.getSolicitud().getFechaAprobacionSolicitud() : "",
                                "",
                                instalacionAcometidaOutRO.getSolicitud().getNombreEstadoTuberiaConexionAcometida() != null ? instalacionAcometidaOutRO.getSolicitud().getNombreEstadoTuberiaConexionAcometida() : "",
                                instalacionAcometidaOutRO.getSolicitud().getNombreTipoGabinete() != null ? instalacionAcometidaOutRO.getSolicitud().getNombreTipoGabinete() : "",
                                instalacionAcometidaOutRO.getSolicitud().getNombreTipoAcometida() != null ? instalacionAcometidaOutRO.getSolicitud().getNombreTipoAcometida() : "",
                                "",
                                instalacionAcometidaOutRO.getNumeroSuministroPredio() != null ? instalacionAcometidaOutRO.getNumeroSuministroPredio() : "",
                                "", "", "", "", "", "", "", "", "", false,
                                instalacionAcometidaOutRO.getNombreTipoInstalacionAcometida(),
                                instalacionAcometidaOutRO.getTipoInstalacionAcometida(), "",false);
                        acometidaDao.insertOrReplace(acometida);

                        if(idtipoacametida.equals("T")){
                            Intent intent = new Intent(BuscarAcometidaActivity.this, Acometida2Activity.class);
                            intent.putExtra("ACOMETIDA", acometida);
                            intent.putExtra("CONFIG", config);
                            intent.putExtra("NumTub", String.valueOf(instalacionAcometidaOutRO.getSolicitud().getNumeroTuberiaConexionProyectoInstalacion()));
                            intent.putExtra("idInstalacion", String.valueOf(instalacionAcometidaOutRO.getSolicitud().getIdProyectoInstalacion()));
                            config.setIdProyectoInstalacion(String.valueOf(instalacionAcometidaOutRO.getSolicitud().getIdProyectoInstalacion()));
                            System.out.println(String.valueOf(instalacionAcometidaOutRO.getSolicitud().getIdProyectoInstalacion()));
                            intent.putExtra("tipo", "T");
                            startActivity(intent);
                            finish();
                        }else{
                            Intent intent = new Intent(BuscarAcometidaActivity.this, Acometida2Activity.class);
                            intent.putExtra("ACOMETIDA", acometida);
                            intent.putExtra("CONFIG", config);
                            intent.putExtra("NumTub", String.valueOf(instalacionAcometidaOutRO.getSolicitud().getNumeroTuberiaConexionProyectoInstalacion()));
                            intent.putExtra("idInstalacion", String.valueOf(instalacionAcometidaOutRO.getSolicitud().getIdProyectoInstalacion()));
                            config.setIdProyectoInstalacion(String.valueOf(instalacionAcometidaOutRO.getSolicitud().getIdProyectoInstalacion()));
                            System.out.println(String.valueOf(instalacionAcometidaOutRO.getSolicitud().getIdProyectoInstalacion()));
                            intent.putExtra("tipo", "A");
                            startActivity(intent);
                            finish();
                        }
                    }
                }
            }
        });
        acometidaTask.execute(config);
    }

}
