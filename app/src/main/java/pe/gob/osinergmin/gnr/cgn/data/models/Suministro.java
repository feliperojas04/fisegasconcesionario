package pe.gob.osinergmin.gnr.cgn.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class Suministro implements Parcelable {

    @Id
    private Long id;
    private Integer idHabilitacion;
    private String DocumentoPropietario;
    private String nombrePropietario;
    private String Predio;
    private String numeroIntHabilitacion;
    private String numeroSolicitud;
    private String numeroContrato;
    private String fechaSolicitud;
    private String fechaAprobacion;
    private String usoInstalacion;
    private String tipoProyecto;
    private String tipoInstalacion;
    private String numeroPuntosInstaacion;
    private String tipoAcometida;
    private String suministro;
    private String medidorFotoUbicacion;
    private String medidorTipoAcomedida;
    private String medidorAprueba;
    private String valvulaFotoUbicacion;
    private String valvulaDiametro;
    private String valvulaAprueba;
    private String tuberiaTipoInstalacion;
    private String tuberiaMaterialInstalacion;
    private String tuberiaFotoRecorrido1;
    private String tuberiaFotoRecorrido2;
    private String tuberiaFotoRecorrido3;
    private String tuberiaFotoRecorrido4;
    private String tuberiaLongitudTotal;
    private String tuberiaAprueba;
    private String puntosAprueba;
    private String ambientesAprueba;
    private String monoxidoAprueba;
    private String artefactoMedicionFoto;
    private String artefactoPresion;
    private String artefactoAprueba;
    private String hermeticidadNanometroFoto;
    private String hermeticidadOperacion;
    private String hermeticidadPruebaInicial;
    private String hermeticidadPruebaFinal;
    private String hermeticidadPruebaTiempo;
    private String hermeticidadAprueba;
    private String suministroHabilitacionFoto;
    private String suministroPruebaFoto;
    private String suministroAprueba;
    private String fechaRegistroOffline;
    private Boolean grabar;
    private String suministroHabilitacionFotoAprovado;
    private String aprobadoCapacitacionInstalador;
    private String idMarcaAccesorio;
    private String idMarcaTuberia;
    private String isometricoFoto;

    /*PAPU 1911 - 0*/
    private String suministroHabilitacionFotoActaFinalizacion;
    /*PAPU 1911 - 1*/

    @Generated(hash = 814683293)
    public Suministro() {
    }

    public Suministro(Long id) {
        this.id = id;
    }

    @Generated(hash = 1010918779)
    public Suministro(Long id, Integer idHabilitacion, String DocumentoPropietario, String nombrePropietario, String Predio,
            String numeroIntHabilitacion, String numeroSolicitud, String numeroContrato, String fechaSolicitud,
            String fechaAprobacion, String usoInstalacion, String tipoProyecto, String tipoInstalacion,
            String numeroPuntosInstaacion, String tipoAcometida, String suministro, String medidorFotoUbicacion,
            String medidorTipoAcomedida, String medidorAprueba, String valvulaFotoUbicacion, String valvulaDiametro,
            String valvulaAprueba, String tuberiaTipoInstalacion, String tuberiaMaterialInstalacion,
            String tuberiaFotoRecorrido1, String tuberiaFotoRecorrido2, String tuberiaFotoRecorrido3,
            String tuberiaFotoRecorrido4, String tuberiaLongitudTotal, String tuberiaAprueba, String puntosAprueba,
            String ambientesAprueba, String monoxidoAprueba, String artefactoMedicionFoto, String artefactoPresion,
            String artefactoAprueba, String hermeticidadNanometroFoto, String hermeticidadOperacion,
            String hermeticidadPruebaInicial, String hermeticidadPruebaFinal, String hermeticidadPruebaTiempo,
            String hermeticidadAprueba, String suministroHabilitacionFoto, String suministroPruebaFoto,
            String suministroAprueba, String fechaRegistroOffline, Boolean grabar,
            String suministroHabilitacionFotoAprovado, String aprobadoCapacitacionInstalador, String idMarcaAccesorio,
            String idMarcaTuberia, String isometricoFoto, String suministroHabilitacionFotoActaFinalizacion) {
        this.id = id;
        this.idHabilitacion = idHabilitacion;
        this.DocumentoPropietario = DocumentoPropietario;
        this.nombrePropietario = nombrePropietario;
        this.Predio = Predio;
        this.numeroIntHabilitacion = numeroIntHabilitacion;
        this.numeroSolicitud = numeroSolicitud;
        this.numeroContrato = numeroContrato;
        this.fechaSolicitud = fechaSolicitud;
        this.fechaAprobacion = fechaAprobacion;
        this.usoInstalacion = usoInstalacion;
        this.tipoProyecto = tipoProyecto;
        this.tipoInstalacion = tipoInstalacion;
        this.numeroPuntosInstaacion = numeroPuntosInstaacion;
        this.tipoAcometida = tipoAcometida;
        this.suministro = suministro;
        this.medidorFotoUbicacion = medidorFotoUbicacion;
        this.medidorTipoAcomedida = medidorTipoAcomedida;
        this.medidorAprueba = medidorAprueba;
        this.valvulaFotoUbicacion = valvulaFotoUbicacion;
        this.valvulaDiametro = valvulaDiametro;
        this.valvulaAprueba = valvulaAprueba;
        this.tuberiaTipoInstalacion = tuberiaTipoInstalacion;
        this.tuberiaMaterialInstalacion = tuberiaMaterialInstalacion;
        this.tuberiaFotoRecorrido1 = tuberiaFotoRecorrido1;
        this.tuberiaFotoRecorrido2 = tuberiaFotoRecorrido2;
        this.tuberiaFotoRecorrido3 = tuberiaFotoRecorrido3;
        this.tuberiaFotoRecorrido4 = tuberiaFotoRecorrido4;
        this.tuberiaLongitudTotal = tuberiaLongitudTotal;
        this.tuberiaAprueba = tuberiaAprueba;
        this.puntosAprueba = puntosAprueba;
        this.ambientesAprueba = ambientesAprueba;
        this.monoxidoAprueba = monoxidoAprueba;
        this.artefactoMedicionFoto = artefactoMedicionFoto;
        this.artefactoPresion = artefactoPresion;
        this.artefactoAprueba = artefactoAprueba;
        this.hermeticidadNanometroFoto = hermeticidadNanometroFoto;
        this.hermeticidadOperacion = hermeticidadOperacion;
        this.hermeticidadPruebaInicial = hermeticidadPruebaInicial;
        this.hermeticidadPruebaFinal = hermeticidadPruebaFinal;
        this.hermeticidadPruebaTiempo = hermeticidadPruebaTiempo;
        this.hermeticidadAprueba = hermeticidadAprueba;
        this.suministroHabilitacionFoto = suministroHabilitacionFoto;
        this.suministroPruebaFoto = suministroPruebaFoto;
        this.suministroAprueba = suministroAprueba;
        this.fechaRegistroOffline = fechaRegistroOffline;
        this.grabar = grabar;
        this.suministroHabilitacionFotoAprovado = suministroHabilitacionFotoAprovado;
        this.aprobadoCapacitacionInstalador = aprobadoCapacitacionInstalador;
        this.idMarcaAccesorio = idMarcaAccesorio;
        this.idMarcaTuberia = idMarcaTuberia;
        this.isometricoFoto = isometricoFoto;
        this.suministroHabilitacionFotoActaFinalizacion = suministroHabilitacionFotoActaFinalizacion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdHabilitacion() {
        return idHabilitacion;
    }

    public void setIdHabilitacion(Integer idHabilitacion) {
        this.idHabilitacion = idHabilitacion;
    }

    public String getDocumentoPropietario() {
        return DocumentoPropietario;
    }

    public void setDocumentoPropietario(String documentoPropietario) {
        DocumentoPropietario = documentoPropietario;
    }

    public String getNombrePropietario() {
        return nombrePropietario;
    }

    public void setNombrePropietario(String nombrePropietario) {
        this.nombrePropietario = nombrePropietario;
    }

    public String getPredio() {
        return Predio;
    }

    public void setPredio(String predio) {
        Predio = predio;
    }

    public String getNumeroIntHabilitacion() {
        return numeroIntHabilitacion;
    }

    public void setNumeroIntHabilitacion(String numeroIntHabilitacion) {
        this.numeroIntHabilitacion = numeroIntHabilitacion;
    }

    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    public String getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(String fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getFechaAprobacion() {
        return fechaAprobacion;
    }

    public void setFechaAprobacion(String fechaAprobacion) {
        this.fechaAprobacion = fechaAprobacion;
    }

    public String getUsoInstalacion() {
        return usoInstalacion;
    }

    public void setUsoInstalacion(String usoInstalacion) {
        this.usoInstalacion = usoInstalacion;
    }

    public String getTipoProyecto() {
        return tipoProyecto;
    }

    public void setTipoProyecto(String tipoProyecto) {
        this.tipoProyecto = tipoProyecto;
    }

    public String getTipoInstalacion() {
        return tipoInstalacion;
    }

    public void setTipoInstalacion(String tipoInstalacion) {
        this.tipoInstalacion = tipoInstalacion;
    }

    public String getNumeroPuntosInstaacion() {
        return numeroPuntosInstaacion;
    }

    public void setNumeroPuntosInstaacion(String numeroPuntosInstaacion) {
        this.numeroPuntosInstaacion = numeroPuntosInstaacion;
    }

    public String getTipoAcometida() {
        return tipoAcometida;
    }

    public void setTipoAcometida(String tipoAcometida) {
        this.tipoAcometida = tipoAcometida;
    }

    public String getSuministro() {
        return suministro;
    }

    public void setSuministro(String suministro) {
        this.suministro = suministro;
    }

    public String getMedidorFotoUbicacion() {
        return medidorFotoUbicacion;
    }

    public void setMedidorFotoUbicacion(String medidorFotoUbicacion) {
        this.medidorFotoUbicacion = medidorFotoUbicacion;
    }

    public String getMedidorTipoAcomedida() {
        return medidorTipoAcomedida;
    }

    public void setMedidorTipoAcomedida(String medidorTipoAcomedida) {
        this.medidorTipoAcomedida = medidorTipoAcomedida;
    }

    public String getMedidorAprueba() {
        return medidorAprueba;
    }

    public void setMedidorAprueba(String medidorAprueba) {
        this.medidorAprueba = medidorAprueba;
    }

    public String getValvulaFotoUbicacion() {
        return valvulaFotoUbicacion;
    }

    public void setValvulaFotoUbicacion(String valvulaFotoUbicacion) {
        this.valvulaFotoUbicacion = valvulaFotoUbicacion;
    }

    public String getValvulaDiametro() {
        return valvulaDiametro;
    }

    public void setValvulaDiametro(String valvulaDiametro) {
        this.valvulaDiametro = valvulaDiametro;
    }

    public String getValvulaAprueba() {
        return valvulaAprueba;
    }

    public void setValvulaAprueba(String valvulaAprueba) {
        this.valvulaAprueba = valvulaAprueba;
    }

    public String getTuberiaTipoInstalacion() {
        return tuberiaTipoInstalacion;
    }

    public void setTuberiaTipoInstalacion(String tuberiaTipoInstalacion) {
        this.tuberiaTipoInstalacion = tuberiaTipoInstalacion;
    }

    public String getTuberiaMaterialInstalacion() {
        return tuberiaMaterialInstalacion;
    }

    public void setTuberiaMaterialInstalacion(String tuberiaMaterialInstalacion) {
        this.tuberiaMaterialInstalacion = tuberiaMaterialInstalacion;
    }

    public String getTuberiaFotoRecorrido1() {
        return tuberiaFotoRecorrido1;
    }

    public void setTuberiaFotoRecorrido1(String tuberiaFotoRecorrido1) {
        this.tuberiaFotoRecorrido1 = tuberiaFotoRecorrido1;
    }

    public String getTuberiaFotoRecorrido2() {
        return tuberiaFotoRecorrido2;
    }

    public void setTuberiaFotoRecorrido2(String tuberiaFotoRecorrido2) {
        this.tuberiaFotoRecorrido2 = tuberiaFotoRecorrido2;
    }

    public String getTuberiaFotoRecorrido3() {
        return tuberiaFotoRecorrido3;
    }

    public void setTuberiaFotoRecorrido3(String tuberiaFotoRecorrido3) {
        this.tuberiaFotoRecorrido3 = tuberiaFotoRecorrido3;
    }

    public String getTuberiaFotoRecorrido4() {
        return tuberiaFotoRecorrido4;
    }

    public void setTuberiaFotoRecorrido4(String tuberiaFotoRecorrido4) {
        this.tuberiaFotoRecorrido4 = tuberiaFotoRecorrido4;
    }

    public String getTuberiaLongitudTotal() {
        return tuberiaLongitudTotal;
    }

    public void setTuberiaLongitudTotal(String tuberiaLongitudTotal) {
        this.tuberiaLongitudTotal = tuberiaLongitudTotal;
    }

    public String getTuberiaAprueba() {
        return tuberiaAprueba;
    }

    public void setTuberiaAprueba(String tuberiaAprueba) {
        this.tuberiaAprueba = tuberiaAprueba;
    }

    public String getPuntosAprueba() {
        return puntosAprueba;
    }

    public void setPuntosAprueba(String puntosAprueba) {
        this.puntosAprueba = puntosAprueba;
    }

    public String getAmbientesAprueba() {
        return ambientesAprueba;
    }

    public void setAmbientesAprueba(String ambientesAprueba) {
        this.ambientesAprueba = ambientesAprueba;
    }

    public String getMonoxidoAprueba() {
        return monoxidoAprueba;
    }

    public void setMonoxidoAprueba(String monoxidoAprueba) {
        this.monoxidoAprueba = monoxidoAprueba;
    }

    public String getArtefactoMedicionFoto() {
        return artefactoMedicionFoto;
    }

    public void setArtefactoMedicionFoto(String artefactoMedicionFoto) {
        this.artefactoMedicionFoto = artefactoMedicionFoto;
    }

    public String getArtefactoPresion() {
        return artefactoPresion;
    }

    public void setArtefactoPresion(String artefactoPresion) {
        this.artefactoPresion = artefactoPresion;
    }

    public String getArtefactoAprueba() {
        return artefactoAprueba;
    }

    public void setArtefactoAprueba(String artefactoAprueba) {
        this.artefactoAprueba = artefactoAprueba;
    }

    public String getHermeticidadNanometroFoto() {
        return hermeticidadNanometroFoto;
    }

    public void setHermeticidadNanometroFoto(String hermeticidadNanometroFoto) {
        this.hermeticidadNanometroFoto = hermeticidadNanometroFoto;
    }

    public String getHermeticidadOperacion() {
        return hermeticidadOperacion;
    }

    public void setHermeticidadOperacion(String hermeticidadOperacion) {
        this.hermeticidadOperacion = hermeticidadOperacion;
    }

    public String getHermeticidadPruebaInicial() {
        return hermeticidadPruebaInicial;
    }

    public void setHermeticidadPruebaInicial(String hermeticidadPruebaInicial) {
        this.hermeticidadPruebaInicial = hermeticidadPruebaInicial;
    }

    public String getHermeticidadPruebaFinal() {
        return hermeticidadPruebaFinal;
    }

    public void setHermeticidadPruebaFinal(String hermeticidadPruebaFinal) {
        this.hermeticidadPruebaFinal = hermeticidadPruebaFinal;
    }

    public String getHermeticidadPruebaTiempo() {
        return hermeticidadPruebaTiempo;
    }

    public void setHermeticidadPruebaTiempo(String hermeticidadPruebaTiempo) {
        this.hermeticidadPruebaTiempo = hermeticidadPruebaTiempo;
    }

    public String getHermeticidadAprueba() {
        return hermeticidadAprueba;
    }

    public void setHermeticidadAprueba(String hermeticidadAprueba) {
        this.hermeticidadAprueba = hermeticidadAprueba;
    }

    public String getSuministroHabilitacionFoto() {
        return suministroHabilitacionFoto;
    }

    public void setSuministroHabilitacionFoto(String suministroHabilitacionFoto) {
        this.suministroHabilitacionFoto = suministroHabilitacionFoto;
    }

    public String getSuministroPruebaFoto() {
        return suministroPruebaFoto;
    }

    public void setSuministroPruebaFoto(String suministroPruebaFoto) {
        this.suministroPruebaFoto = suministroPruebaFoto;
    }

    public String getSuministroAprueba() {
        return suministroAprueba;
    }

    public void setSuministroAprueba(String suministroAprueba) {
        this.suministroAprueba = suministroAprueba;
    }

    public String getFechaRegistroOffline() {
        return fechaRegistroOffline;
    }

    public void setFechaRegistroOffline(String fechaRegistroOffline) {
        this.fechaRegistroOffline = fechaRegistroOffline;
    }

    public Boolean getGrabar() {
        return grabar;
    }

    public void setGrabar(Boolean grabar) {
        this.grabar = grabar;
    }

    public String getSuministroHabilitacionFotoAprovado() {
        return suministroHabilitacionFotoAprovado;
    }

    public void setSuministroHabilitacionFotoAprovado(String suministroHabilitacionFotoAprovado) {
        this.suministroHabilitacionFotoAprovado = suministroHabilitacionFotoAprovado;
    }

    public String getSuministroHabilitacionFotoActaFinalizacion() {
        return suministroHabilitacionFotoActaFinalizacion;
    }

    public void setSuministroHabilitacionFotoActaFinalizacion(String suministroHabilitacionFotoActaFinalizacion) {
        this.suministroHabilitacionFotoActaFinalizacion = suministroHabilitacionFotoActaFinalizacion;
    }

    public String getAprobadoCapacitacionInstalador() {
        return aprobadoCapacitacionInstalador;
    }

    public void setAprobadoCapacitacionInstalador(String aprobadoCapacitacionInstalador) {
        this.aprobadoCapacitacionInstalador = aprobadoCapacitacionInstalador;
    }

    public String getIdMarcaAccesorio() {
        return idMarcaAccesorio;
    }

    public void setIdMarcaAccesorio(String idMarcaAccesorio) {
        this.idMarcaAccesorio = idMarcaAccesorio;
    }

    public String getIdMarcaTuberia() {
        return idMarcaTuberia;
    }

    public void setIdMarcaTuberia(String idMarcaTuberia) {
        this.idMarcaTuberia = idMarcaTuberia;
    }

    public String getIsometricoFoto() {
        return isometricoFoto;
    }

    public void setIsometricoFoto(String isometricoFoto) {
        this.isometricoFoto = isometricoFoto;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.idHabilitacion);
        dest.writeString(this.DocumentoPropietario);
        dest.writeString(this.nombrePropietario);
        dest.writeString(this.Predio);
        dest.writeString(this.numeroIntHabilitacion);
        dest.writeString(this.numeroSolicitud);
        dest.writeString(this.numeroContrato);
        dest.writeString(this.fechaSolicitud);
        dest.writeString(this.fechaAprobacion);
        dest.writeString(this.usoInstalacion);
        dest.writeString(this.tipoProyecto);
        dest.writeString(this.tipoInstalacion);
        dest.writeString(this.numeroPuntosInstaacion);
        dest.writeString(this.tipoAcometida);
        dest.writeString(this.suministro);
        dest.writeString(this.medidorFotoUbicacion);
        dest.writeString(this.medidorTipoAcomedida);
        dest.writeString(this.medidorAprueba);
        dest.writeString(this.valvulaFotoUbicacion);
        dest.writeString(this.valvulaDiametro);
        dest.writeString(this.valvulaAprueba);
        dest.writeString(this.tuberiaTipoInstalacion);
        dest.writeString(this.tuberiaMaterialInstalacion);
        dest.writeString(this.tuberiaFotoRecorrido1);
        dest.writeString(this.tuberiaFotoRecorrido2);
        dest.writeString(this.tuberiaFotoRecorrido3);
        dest.writeString(this.tuberiaFotoRecorrido4);
        dest.writeString(this.tuberiaLongitudTotal);
        dest.writeString(this.tuberiaAprueba);
        dest.writeString(this.puntosAprueba);
        dest.writeString(this.ambientesAprueba);
        dest.writeString(this.monoxidoAprueba);
        dest.writeString(this.artefactoMedicionFoto);
        dest.writeString(this.artefactoPresion);
        dest.writeString(this.artefactoAprueba);
        dest.writeString(this.hermeticidadNanometroFoto);
        dest.writeString(this.hermeticidadOperacion);
        dest.writeString(this.hermeticidadPruebaInicial);
        dest.writeString(this.hermeticidadPruebaFinal);
        dest.writeString(this.hermeticidadPruebaTiempo);
        dest.writeString(this.hermeticidadAprueba);
        dest.writeString(this.suministroHabilitacionFoto);
        dest.writeString(this.suministroPruebaFoto);
        dest.writeString(this.suministroAprueba);
        dest.writeString(this.fechaRegistroOffline);
        dest.writeValue(this.grabar);
        dest.writeString(this.suministroHabilitacionFotoAprovado);
        dest.writeString(this.aprobadoCapacitacionInstalador);
        dest.writeString(this.idMarcaAccesorio);
        dest.writeString(this.idMarcaTuberia);
        dest.writeString(this.isometricoFoto);
        /*PAPU 1911 - 0*/
        dest.writeString(this.suministroHabilitacionFotoActaFinalizacion);
        /*PAPU 1911 - 1*/
    }

    protected Suministro(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.idHabilitacion = (Integer) in.readValue(Integer.class.getClassLoader());
        this.DocumentoPropietario = in.readString();
        this.nombrePropietario = in.readString();
        this.Predio = in.readString();
        this.numeroIntHabilitacion = in.readString();
        this.numeroSolicitud = in.readString();
        this.numeroContrato = in.readString();
        this.fechaSolicitud = in.readString();
        this.fechaAprobacion = in.readString();
        this.usoInstalacion = in.readString();
        this.tipoProyecto = in.readString();
        this.tipoInstalacion = in.readString();
        this.numeroPuntosInstaacion = in.readString();
        this.tipoAcometida = in.readString();
        this.suministro = in.readString();
        this.medidorFotoUbicacion = in.readString();
        this.medidorTipoAcomedida = in.readString();
        this.medidorAprueba = in.readString();
        this.valvulaFotoUbicacion = in.readString();
        this.valvulaDiametro = in.readString();
        this.valvulaAprueba = in.readString();
        this.tuberiaTipoInstalacion = in.readString();
        this.tuberiaMaterialInstalacion = in.readString();
        this.tuberiaFotoRecorrido1 = in.readString();
        this.tuberiaFotoRecorrido2 = in.readString();
        this.tuberiaFotoRecorrido3 = in.readString();
        this.tuberiaFotoRecorrido4 = in.readString();
        this.tuberiaLongitudTotal = in.readString();
        this.tuberiaAprueba = in.readString();
        this.puntosAprueba = in.readString();
        this.ambientesAprueba = in.readString();
        this.monoxidoAprueba = in.readString();
        this.artefactoMedicionFoto = in.readString();
        this.artefactoPresion = in.readString();
        this.artefactoAprueba = in.readString();
        this.hermeticidadNanometroFoto = in.readString();
        this.hermeticidadOperacion = in.readString();
        this.hermeticidadPruebaInicial = in.readString();
        this.hermeticidadPruebaFinal = in.readString();
        this.hermeticidadPruebaTiempo = in.readString();
        this.hermeticidadAprueba = in.readString();
        this.suministroHabilitacionFoto = in.readString();
        this.suministroPruebaFoto = in.readString();
        this.suministroAprueba = in.readString();
        this.fechaRegistroOffline = in.readString();
        this.grabar = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.suministroHabilitacionFotoAprovado = in.readString();
        this.aprobadoCapacitacionInstalador = in.readString();
        this.idMarcaAccesorio = in.readString();
        this.idMarcaTuberia = in.readString();
        this.isometricoFoto = in.readString();

        /*PAPU 1911 - 0*/
        this.suministroHabilitacionFotoActaFinalizacion = in.readString();
        /*PAPU 1911 - 1*/
    }

    public static final Creator<Suministro> CREATOR = new Creator<Suministro>() {
        @Override
        public Suministro createFromParcel(Parcel source) {
            return new Suministro(source);
        }

        @Override
        public Suministro[] newArray(int size) {
            return new Suministro[size];
        }
    };
}
