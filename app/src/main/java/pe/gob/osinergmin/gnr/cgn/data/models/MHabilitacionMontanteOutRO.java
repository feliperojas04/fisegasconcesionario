package pe.gob.osinergmin.gnr.cgn.data.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class MHabilitacionMontanteOutRO {

    @Id
    private Long idHabilitacionMontante;
    private Integer idProyectoInstalacion;
    private String codigoObjetoConexionProyectoInstalacion;
    private String codigoUnidadPredialProyectoInstalacion;
    private String nombreProyectoInstalacion;
    private String direccionUbigeoProyectoInstalacion;
    private String estadoHabilitacionMontante;
    private String nombreEstadoHabilitacionMontante;
    private String fechaUltimaModificacionHabilitacionMontante;
    private Integer idInspector;
    private Integer numeroIntentoHabilitacionMontante;
    private String fechaProgramadaHabilitacionMontante;
    private String observacionProgramacionHabilitacionMontante;
    private String fechaRegistroHabilitacionMontante;
    private String aprobadoHabilitacionMontante;
    private String nombreAprobadoHabilitacionMontante;
    private String aprobadaAcometida;
    private String nombreAprobadaAcometida;
    private String nombreFotoAcometida;
    private String aprobadoRecorridoTuberia;
    private String nombreAprobadoRecorridoTuberia;
    private String nombreFoto1RecorridoTuberia;
    private String nombreFoto2RecorridoTuberia;
    private String nombreFoto3RecorridoTuberia;
    private String aprobadoHermeticidad;
    private String nombreAprobadoHermeticidad;
    private String nombreFotoMedicionHermeticidadManometro;
    private Double presionOperacionHermeticidad;
    private Double presionTuberiaHermeticidad;
    private String aprobadaInstalacion;
    private String nombreAprobadaInstalacion;
    private String tipoInstalacion;
    private String nombreTipoInstalacion;
    private Integer idMaterialInstalacion;
    private String nombreMaterialInstalacion;
    private Double longitudTotalInstalacion;
    private Double diametroTuberia;
    private String resultCode;
    private String message;
    private String errorCode;
    private String nombreMontanteProyectoInstalacion;

    @Generated(hash = 1948361575)
    public MHabilitacionMontanteOutRO() {
    }

    public MHabilitacionMontanteOutRO(Long idHabilitacionMontante) {
        this.idHabilitacionMontante = idHabilitacionMontante;
    }

    @Generated(hash = 903745802)
    public MHabilitacionMontanteOutRO(Long idHabilitacionMontante, Integer idProyectoInstalacion, String codigoObjetoConexionProyectoInstalacion, String codigoUnidadPredialProyectoInstalacion, String nombreProyectoInstalacion, String direccionUbigeoProyectoInstalacion, String estadoHabilitacionMontante, String nombreEstadoHabilitacionMontante, String fechaUltimaModificacionHabilitacionMontante, Integer idInspector, Integer numeroIntentoHabilitacionMontante, String fechaProgramadaHabilitacionMontante, String observacionProgramacionHabilitacionMontante, String fechaRegistroHabilitacionMontante, String aprobadoHabilitacionMontante, String nombreAprobadoHabilitacionMontante, String aprobadaAcometida, String nombreAprobadaAcometida, String nombreFotoAcometida, String aprobadoRecorridoTuberia, String nombreAprobadoRecorridoTuberia, String nombreFoto1RecorridoTuberia, String nombreFoto2RecorridoTuberia, String nombreFoto3RecorridoTuberia, String aprobadoHermeticidad, String nombreAprobadoHermeticidad, String nombreFotoMedicionHermeticidadManometro, Double presionOperacionHermeticidad, Double presionTuberiaHermeticidad, String aprobadaInstalacion, String nombreAprobadaInstalacion, String tipoInstalacion, String nombreTipoInstalacion, Integer idMaterialInstalacion, String nombreMaterialInstalacion, Double longitudTotalInstalacion, Double diametroTuberia, String resultCode, String message, String errorCode, String nombreMontanteProyectoInstalacion) {
        this.idHabilitacionMontante = idHabilitacionMontante;
        this.idProyectoInstalacion = idProyectoInstalacion;
        this.codigoObjetoConexionProyectoInstalacion = codigoObjetoConexionProyectoInstalacion;
        this.codigoUnidadPredialProyectoInstalacion = codigoUnidadPredialProyectoInstalacion;
        this.nombreProyectoInstalacion = nombreProyectoInstalacion;
        this.direccionUbigeoProyectoInstalacion = direccionUbigeoProyectoInstalacion;
        this.estadoHabilitacionMontante = estadoHabilitacionMontante;
        this.nombreEstadoHabilitacionMontante = nombreEstadoHabilitacionMontante;
        this.fechaUltimaModificacionHabilitacionMontante = fechaUltimaModificacionHabilitacionMontante;
        this.idInspector = idInspector;
        this.numeroIntentoHabilitacionMontante = numeroIntentoHabilitacionMontante;
        this.fechaProgramadaHabilitacionMontante = fechaProgramadaHabilitacionMontante;
        this.observacionProgramacionHabilitacionMontante = observacionProgramacionHabilitacionMontante;
        this.fechaRegistroHabilitacionMontante = fechaRegistroHabilitacionMontante;
        this.aprobadoHabilitacionMontante = aprobadoHabilitacionMontante;
        this.nombreAprobadoHabilitacionMontante = nombreAprobadoHabilitacionMontante;
        this.aprobadaAcometida = aprobadaAcometida;
        this.nombreAprobadaAcometida = nombreAprobadaAcometida;
        this.nombreFotoAcometida = nombreFotoAcometida;
        this.aprobadoRecorridoTuberia = aprobadoRecorridoTuberia;
        this.nombreAprobadoRecorridoTuberia = nombreAprobadoRecorridoTuberia;
        this.nombreFoto1RecorridoTuberia = nombreFoto1RecorridoTuberia;
        this.nombreFoto2RecorridoTuberia = nombreFoto2RecorridoTuberia;
        this.nombreFoto3RecorridoTuberia = nombreFoto3RecorridoTuberia;
        this.aprobadoHermeticidad = aprobadoHermeticidad;
        this.nombreAprobadoHermeticidad = nombreAprobadoHermeticidad;
        this.nombreFotoMedicionHermeticidadManometro = nombreFotoMedicionHermeticidadManometro;
        this.presionOperacionHermeticidad = presionOperacionHermeticidad;
        this.presionTuberiaHermeticidad = presionTuberiaHermeticidad;
        this.aprobadaInstalacion = aprobadaInstalacion;
        this.nombreAprobadaInstalacion = nombreAprobadaInstalacion;
        this.tipoInstalacion = tipoInstalacion;
        this.nombreTipoInstalacion = nombreTipoInstalacion;
        this.idMaterialInstalacion = idMaterialInstalacion;
        this.nombreMaterialInstalacion = nombreMaterialInstalacion;
        this.longitudTotalInstalacion = longitudTotalInstalacion;
        this.diametroTuberia = diametroTuberia;
        this.resultCode = resultCode;
        this.message = message;
        this.errorCode = errorCode;
        this.nombreMontanteProyectoInstalacion = nombreMontanteProyectoInstalacion;
    }

    public Long getIdHabilitacionMontante() {
        return idHabilitacionMontante;
    }

    public void setIdHabilitacionMontante(Long idHabilitacionMontante) {
        this.idHabilitacionMontante = idHabilitacionMontante;
    }

    public Integer getIdProyectoInstalacion() {
        return idProyectoInstalacion;
    }

    public void setIdProyectoInstalacion(Integer idProyectoInstalacion) {
        this.idProyectoInstalacion = idProyectoInstalacion;
    }

    public String getCodigoObjetoConexionProyectoInstalacion() {
        return codigoObjetoConexionProyectoInstalacion;
    }

    public void setCodigoObjetoConexionProyectoInstalacion(String codigoObjetoConexionProyectoInstalacion) {
        this.codigoObjetoConexionProyectoInstalacion = codigoObjetoConexionProyectoInstalacion;
    }

    public String getCodigoUnidadPredialProyectoInstalacion() {
        return codigoUnidadPredialProyectoInstalacion;
    }

    public void setCodigoUnidadPredialProyectoInstalacion(String codigoUnidadPredialProyectoInstalacion) {
        this.codigoUnidadPredialProyectoInstalacion = codigoUnidadPredialProyectoInstalacion;
    }

    public String getNombreProyectoInstalacion() {
        return nombreProyectoInstalacion;
    }

    public void setNombreProyectoInstalacion(String nombreProyectoInstalacion) {
        this.nombreProyectoInstalacion = nombreProyectoInstalacion;
    }

    public String getDireccionUbigeoProyectoInstalacion() {
        return direccionUbigeoProyectoInstalacion;
    }

    public void setDireccionUbigeoProyectoInstalacion(String direccionUbigeoProyectoInstalacion) {
        this.direccionUbigeoProyectoInstalacion = direccionUbigeoProyectoInstalacion;
    }

    public String getEstadoHabilitacionMontante() {
        return estadoHabilitacionMontante;
    }

    public void setEstadoHabilitacionMontante(String estadoHabilitacionMontante) {
        this.estadoHabilitacionMontante = estadoHabilitacionMontante;
    }

    public String getNombreEstadoHabilitacionMontante() {
        return nombreEstadoHabilitacionMontante;
    }

    public void setNombreEstadoHabilitacionMontante(String nombreEstadoHabilitacionMontante) {
        this.nombreEstadoHabilitacionMontante = nombreEstadoHabilitacionMontante;
    }

    public String getFechaUltimaModificacionHabilitacionMontante() {
        return fechaUltimaModificacionHabilitacionMontante;
    }

    public void setFechaUltimaModificacionHabilitacionMontante(String fechaUltimaModificacionHabilitacionMontante) {
        this.fechaUltimaModificacionHabilitacionMontante = fechaUltimaModificacionHabilitacionMontante;
    }

    public Integer getIdInspector() {
        return idInspector;
    }

    public void setIdInspector(Integer idInspector) {
        this.idInspector = idInspector;
    }

    public Integer getNumeroIntentoHabilitacionMontante() {
        return numeroIntentoHabilitacionMontante;
    }

    public void setNumeroIntentoHabilitacionMontante(Integer numeroIntentoHabilitacionMontante) {
        this.numeroIntentoHabilitacionMontante = numeroIntentoHabilitacionMontante;
    }

    public String getFechaProgramadaHabilitacionMontante() {
        return fechaProgramadaHabilitacionMontante;
    }

    public void setFechaProgramadaHabilitacionMontante(String fechaProgramadaHabilitacionMontante) {
        this.fechaProgramadaHabilitacionMontante = fechaProgramadaHabilitacionMontante;
    }

    public String getObservacionProgramacionHabilitacionMontante() {
        return observacionProgramacionHabilitacionMontante;
    }

    public void setObservacionProgramacionHabilitacionMontante(String observacionProgramacionHabilitacionMontante) {
        this.observacionProgramacionHabilitacionMontante = observacionProgramacionHabilitacionMontante;
    }

    public String getFechaRegistroHabilitacionMontante() {
        return fechaRegistroHabilitacionMontante;
    }

    public void setFechaRegistroHabilitacionMontante(String fechaRegistroHabilitacionMontante) {
        this.fechaRegistroHabilitacionMontante = fechaRegistroHabilitacionMontante;
    }

    public String getAprobadoHabilitacionMontante() {
        return aprobadoHabilitacionMontante;
    }

    public void setAprobadoHabilitacionMontante(String aprobadoHabilitacionMontante) {
        this.aprobadoHabilitacionMontante = aprobadoHabilitacionMontante;
    }

    public String getNombreAprobadoHabilitacionMontante() {
        return nombreAprobadoHabilitacionMontante;
    }

    public void setNombreAprobadoHabilitacionMontante(String nombreAprobadoHabilitacionMontante) {
        this.nombreAprobadoHabilitacionMontante = nombreAprobadoHabilitacionMontante;
    }

    public String getAprobadaAcometida() {
        return aprobadaAcometida;
    }

    public void setAprobadaAcometida(String aprobadaAcometida) {
        this.aprobadaAcometida = aprobadaAcometida;
    }

    public String getNombreAprobadaAcometida() {
        return nombreAprobadaAcometida;
    }

    public void setNombreAprobadaAcometida(String nombreAprobadaAcometida) {
        this.nombreAprobadaAcometida = nombreAprobadaAcometida;
    }

    public String getNombreFotoAcometida() {
        return nombreFotoAcometida;
    }

    public void setNombreFotoAcometida(String nombreFotoAcometida) {
        this.nombreFotoAcometida = nombreFotoAcometida;
    }

    public String getAprobadoRecorridoTuberia() {
        return aprobadoRecorridoTuberia;
    }

    public void setAprobadoRecorridoTuberia(String aprobadoRecorridoTuberia) {
        this.aprobadoRecorridoTuberia = aprobadoRecorridoTuberia;
    }

    public String getNombreAprobadoRecorridoTuberia() {
        return nombreAprobadoRecorridoTuberia;
    }

    public void setNombreAprobadoRecorridoTuberia(String nombreAprobadoRecorridoTuberia) {
        this.nombreAprobadoRecorridoTuberia = nombreAprobadoRecorridoTuberia;
    }

    public String getNombreFoto1RecorridoTuberia() {
        return nombreFoto1RecorridoTuberia;
    }

    public void setNombreFoto1RecorridoTuberia(String nombreFoto1RecorridoTuberia) {
        this.nombreFoto1RecorridoTuberia = nombreFoto1RecorridoTuberia;
    }

    public String getNombreFoto2RecorridoTuberia() {
        return nombreFoto2RecorridoTuberia;
    }

    public void setNombreFoto2RecorridoTuberia(String nombreFoto2RecorridoTuberia) {
        this.nombreFoto2RecorridoTuberia = nombreFoto2RecorridoTuberia;
    }

    public String getNombreFoto3RecorridoTuberia() {
        return nombreFoto3RecorridoTuberia;
    }

    public void setNombreFoto3RecorridoTuberia(String nombreFoto3RecorridoTuberia) {
        this.nombreFoto3RecorridoTuberia = nombreFoto3RecorridoTuberia;
    }

    public String getAprobadoHermeticidad() {
        return aprobadoHermeticidad;
    }

    public void setAprobadoHermeticidad(String aprobadoHermeticidad) {
        this.aprobadoHermeticidad = aprobadoHermeticidad;
    }

    public String getNombreAprobadoHermeticidad() {
        return nombreAprobadoHermeticidad;
    }

    public void setNombreAprobadoHermeticidad(String nombreAprobadoHermeticidad) {
        this.nombreAprobadoHermeticidad = nombreAprobadoHermeticidad;
    }

    public String getNombreFotoMedicionHermeticidadManometro() {
        return nombreFotoMedicionHermeticidadManometro;
    }

    public void setNombreFotoMedicionHermeticidadManometro(String nombreFotoMedicionHermeticidadManometro) {
        this.nombreFotoMedicionHermeticidadManometro = nombreFotoMedicionHermeticidadManometro;
    }

    public Double getPresionOperacionHermeticidad() {
        return presionOperacionHermeticidad;
    }

    public void setPresionOperacionHermeticidad(Double presionOperacionHermeticidad) {
        this.presionOperacionHermeticidad = presionOperacionHermeticidad;
    }

    public Double getPresionTuberiaHermeticidad() {
        return presionTuberiaHermeticidad;
    }

    public void setPresionTuberiaHermeticidad(Double presionTuberiaHermeticidad) {
        this.presionTuberiaHermeticidad = presionTuberiaHermeticidad;
    }

    public String getAprobadaInstalacion() {
        return aprobadaInstalacion;
    }

    public void setAprobadaInstalacion(String aprobadaInstalacion) {
        this.aprobadaInstalacion = aprobadaInstalacion;
    }

    public String getNombreAprobadaInstalacion() {
        return nombreAprobadaInstalacion;
    }

    public void setNombreAprobadaInstalacion(String nombreAprobadaInstalacion) {
        this.nombreAprobadaInstalacion = nombreAprobadaInstalacion;
    }

    public String getTipoInstalacion() {
        return tipoInstalacion;
    }

    public void setTipoInstalacion(String tipoInstalacion) {
        this.tipoInstalacion = tipoInstalacion;
    }

    public String getNombreTipoInstalacion() {
        return nombreTipoInstalacion;
    }

    public void setNombreTipoInstalacion(String nombreTipoInstalacion) {
        this.nombreTipoInstalacion = nombreTipoInstalacion;
    }

    public Integer getIdMaterialInstalacion() {
        return idMaterialInstalacion;
    }

    public void setIdMaterialInstalacion(Integer idMaterialInstalacion) {
        this.idMaterialInstalacion = idMaterialInstalacion;
    }

    public String getNombreMaterialInstalacion() {
        return nombreMaterialInstalacion;
    }

    public void setNombreMaterialInstalacion(String nombreMaterialInstalacion) {
        this.nombreMaterialInstalacion = nombreMaterialInstalacion;
    }

    public Double getLongitudTotalInstalacion() {
        return longitudTotalInstalacion;
    }

    public void setLongitudTotalInstalacion(Double longitudTotalInstalacion) {
        this.longitudTotalInstalacion = longitudTotalInstalacion;
    }

    public Double getDiametroTuberia() {
        return diametroTuberia;
    }

    public void setDiametroTuberia(Double diametroTuberia) {
        this.diametroTuberia = diametroTuberia;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getNombreMontanteProyectoInstalacion() {
        return nombreMontanteProyectoInstalacion;
    }

    public void setNombreMontanteProyectoInstalacion(String nombreMontanteProyectoInstalacion) {
        this.nombreMontanteProyectoInstalacion = nombreMontanteProyectoInstalacion;
    }
}
