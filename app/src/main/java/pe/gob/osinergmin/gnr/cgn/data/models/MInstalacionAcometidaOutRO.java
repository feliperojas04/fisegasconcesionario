package pe.gob.osinergmin.gnr.cgn.data.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class MInstalacionAcometidaOutRO {

    @Id
    private Long idInstalacionAcometida;
    private Integer idSolicitud;
    private Integer codigoSolicitud;
    private String documentoIdentificacionSolicitante;
    private String nombreSolicitante;
    private String direccionUbigeoPredio;
    private String numeroSuministroPredio;
    private String numeroContratoConcesionariaPredio;
    private String estadoInstalacionAcometida;
    private String nombreEstadoInstalacionAcometida;
    private String fechaUltimaModificacionInstalacionAcometida;
    private Integer idInspector;
    private Integer numeroIntentoInstalacionAcometida;
    private String fechaProgramadaInstalacionAcometida;
    private String fechaRegistroInstalacionAcometida;
    private String fechaFinalizacionInstalacionAcometida;
    private Integer idFusionistaRegistro;
    private String documentoIdentificacionFusionistaRegistro;
    private String nombreFusionistaRegistro;
    private String resultadoInstalacionAcometida;
    private String nombreResultadoInstalacionAcometida;
    private String tipoGabinete;
    private String nombreTipoGabinete;
    private String diametroTuberiaConexionInstalacionAcometida;
    private String longitudTuberiaConexionInstalacionAcometida;
    private String nombreMaterialInstalacion;
    private String nombreFotoActaInstalacionTuberiaConexionAcometida;
    private String nombreFotoTuberiaConexionTerminada;
    private String nombreFotoGabineteManifoldTerminado;
    private String nombreTipoInstalacionAcometida;
    private String tipoInstalacionAcometida;
    private String resultCode;
    private String message;
    private String errorCode;

    @Generated(hash = 1266768811)
    public MInstalacionAcometidaOutRO() {
    }

    public MInstalacionAcometidaOutRO(Long idInstalacionAcometida) {
        this.idInstalacionAcometida = idInstalacionAcometida;
    }

    @Generated(hash = 1444104156)
    public MInstalacionAcometidaOutRO(Long idInstalacionAcometida, Integer idSolicitud, Integer codigoSolicitud, String documentoIdentificacionSolicitante, String nombreSolicitante, String direccionUbigeoPredio, String numeroSuministroPredio, String numeroContratoConcesionariaPredio, String estadoInstalacionAcometida, String nombreEstadoInstalacionAcometida, String fechaUltimaModificacionInstalacionAcometida, Integer idInspector, Integer numeroIntentoInstalacionAcometida, String fechaProgramadaInstalacionAcometida, String fechaRegistroInstalacionAcometida, String fechaFinalizacionInstalacionAcometida, Integer idFusionistaRegistro, String documentoIdentificacionFusionistaRegistro, String nombreFusionistaRegistro, String resultadoInstalacionAcometida, String nombreResultadoInstalacionAcometida, String tipoGabinete, String nombreTipoGabinete, String diametroTuberiaConexionInstalacionAcometida, String longitudTuberiaConexionInstalacionAcometida, String nombreMaterialInstalacion, String nombreFotoActaInstalacionTuberiaConexionAcometida, String nombreFotoTuberiaConexionTerminada, String nombreFotoGabineteManifoldTerminado, String nombreTipoInstalacionAcometida, String tipoInstalacionAcometida, String resultCode, String message, String errorCode) {
        this.idInstalacionAcometida = idInstalacionAcometida;
        this.idSolicitud = idSolicitud;
        this.codigoSolicitud = codigoSolicitud;
        this.documentoIdentificacionSolicitante = documentoIdentificacionSolicitante;
        this.nombreSolicitante = nombreSolicitante;
        this.direccionUbigeoPredio = direccionUbigeoPredio;
        this.numeroSuministroPredio = numeroSuministroPredio;
        this.numeroContratoConcesionariaPredio = numeroContratoConcesionariaPredio;
        this.estadoInstalacionAcometida = estadoInstalacionAcometida;
        this.nombreEstadoInstalacionAcometida = nombreEstadoInstalacionAcometida;
        this.fechaUltimaModificacionInstalacionAcometida = fechaUltimaModificacionInstalacionAcometida;
        this.idInspector = idInspector;
        this.numeroIntentoInstalacionAcometida = numeroIntentoInstalacionAcometida;
        this.fechaProgramadaInstalacionAcometida = fechaProgramadaInstalacionAcometida;
        this.fechaRegistroInstalacionAcometida = fechaRegistroInstalacionAcometida;
        this.fechaFinalizacionInstalacionAcometida = fechaFinalizacionInstalacionAcometida;
        this.idFusionistaRegistro = idFusionistaRegistro;
        this.documentoIdentificacionFusionistaRegistro = documentoIdentificacionFusionistaRegistro;
        this.nombreFusionistaRegistro = nombreFusionistaRegistro;
        this.resultadoInstalacionAcometida = resultadoInstalacionAcometida;
        this.nombreResultadoInstalacionAcometida = nombreResultadoInstalacionAcometida;
        this.tipoGabinete = tipoGabinete;
        this.nombreTipoGabinete = nombreTipoGabinete;
        this.diametroTuberiaConexionInstalacionAcometida = diametroTuberiaConexionInstalacionAcometida;
        this.longitudTuberiaConexionInstalacionAcometida = longitudTuberiaConexionInstalacionAcometida;
        this.nombreMaterialInstalacion = nombreMaterialInstalacion;
        this.nombreFotoActaInstalacionTuberiaConexionAcometida = nombreFotoActaInstalacionTuberiaConexionAcometida;
        this.nombreFotoTuberiaConexionTerminada = nombreFotoTuberiaConexionTerminada;
        this.nombreFotoGabineteManifoldTerminado = nombreFotoGabineteManifoldTerminado;
        this.nombreTipoInstalacionAcometida = nombreTipoInstalacionAcometida;
        this.tipoInstalacionAcometida = tipoInstalacionAcometida;
        this.resultCode = resultCode;
        this.message = message;
        this.errorCode = errorCode;
    }

    public Long getIdInstalacionAcometida() {
        return idInstalacionAcometida;
    }

    public void setIdInstalacionAcometida(Long idInstalacionAcometida) {
        this.idInstalacionAcometida = idInstalacionAcometida;
    }

    public Integer getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(Integer idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public Integer getCodigoSolicitud() {
        return codigoSolicitud;
    }

    public void setCodigoSolicitud(Integer codigoSolicitud) {
        this.codigoSolicitud = codigoSolicitud;
    }

    public String getDocumentoIdentificacionSolicitante() {
        return documentoIdentificacionSolicitante;
    }

    public void setDocumentoIdentificacionSolicitante(String documentoIdentificacionSolicitante) {
        this.documentoIdentificacionSolicitante = documentoIdentificacionSolicitante;
    }

    public String getNombreSolicitante() {
        return nombreSolicitante;
    }

    public void setNombreSolicitante(String nombreSolicitante) {
        this.nombreSolicitante = nombreSolicitante;
    }

    public String getDireccionUbigeoPredio() {
        return direccionUbigeoPredio;
    }

    public void setDireccionUbigeoPredio(String direccionUbigeoPredio) {
        this.direccionUbigeoPredio = direccionUbigeoPredio;
    }

    public String getNumeroSuministroPredio() {
        return numeroSuministroPredio;
    }

    public void setNumeroSuministroPredio(String numeroSuministroPredio) {
        this.numeroSuministroPredio = numeroSuministroPredio;
    }

    public String getNumeroContratoConcesionariaPredio() {
        return numeroContratoConcesionariaPredio;
    }

    public void setNumeroContratoConcesionariaPredio(String numeroContratoConcesionariaPredio) {
        this.numeroContratoConcesionariaPredio = numeroContratoConcesionariaPredio;
    }

    public String getEstadoInstalacionAcometida() {
        return estadoInstalacionAcometida;
    }

    public void setEstadoInstalacionAcometida(String estadoInstalacionAcometida) {
        this.estadoInstalacionAcometida = estadoInstalacionAcometida;
    }

    public String getNombreEstadoInstalacionAcometida() {
        return nombreEstadoInstalacionAcometida;
    }

    public void setNombreEstadoInstalacionAcometida(String nombreEstadoInstalacionAcometida) {
        this.nombreEstadoInstalacionAcometida = nombreEstadoInstalacionAcometida;
    }

    public String getFechaUltimaModificacionInstalacionAcometida() {
        return fechaUltimaModificacionInstalacionAcometida;
    }

    public void setFechaUltimaModificacionInstalacionAcometida(String fechaUltimaModificacionInstalacionAcometida) {
        this.fechaUltimaModificacionInstalacionAcometida = fechaUltimaModificacionInstalacionAcometida;
    }

    public Integer getIdInspector() {
        return idInspector;
    }

    public void setIdInspector(Integer idInspector) {
        this.idInspector = idInspector;
    }

    public Integer getNumeroIntentoInstalacionAcometida() {
        return numeroIntentoInstalacionAcometida;
    }

    public void setNumeroIntentoInstalacionAcometida(Integer numeroIntentoInstalacionAcometida) {
        this.numeroIntentoInstalacionAcometida = numeroIntentoInstalacionAcometida;
    }

    public String getFechaProgramadaInstalacionAcometida() {
        return fechaProgramadaInstalacionAcometida;
    }

    public void setFechaProgramadaInstalacionAcometida(String fechaProgramadaInstalacionAcometida) {
        this.fechaProgramadaInstalacionAcometida = fechaProgramadaInstalacionAcometida;
    }

    public String getFechaRegistroInstalacionAcometida() {
        return fechaRegistroInstalacionAcometida;
    }

    public void setFechaRegistroInstalacionAcometida(String fechaRegistroInstalacionAcometida) {
        this.fechaRegistroInstalacionAcometida = fechaRegistroInstalacionAcometida;
    }

    public String getFechaFinalizacionInstalacionAcometida() {
        return fechaFinalizacionInstalacionAcometida;
    }

    public void setFechaFinalizacionInstalacionAcometida(String fechaFinalizacionInstalacionAcometida) {
        this.fechaFinalizacionInstalacionAcometida = fechaFinalizacionInstalacionAcometida;
    }

    public Integer getIdFusionistaRegistro() {
        return idFusionistaRegistro;
    }

    public void setIdFusionistaRegistro(Integer idFusionistaRegistro) {
        this.idFusionistaRegistro = idFusionistaRegistro;
    }

    public String getDocumentoIdentificacionFusionistaRegistro() {
        return documentoIdentificacionFusionistaRegistro;
    }

    public void setDocumentoIdentificacionFusionistaRegistro(String documentoIdentificacionFusionistaRegistro) {
        this.documentoIdentificacionFusionistaRegistro = documentoIdentificacionFusionistaRegistro;
    }

    public String getNombreFusionistaRegistro() {
        return nombreFusionistaRegistro;
    }

    public void setNombreFusionistaRegistro(String nombreFusionistaRegistro) {
        this.nombreFusionistaRegistro = nombreFusionistaRegistro;
    }

    public String getResultadoInstalacionAcometida() {
        return resultadoInstalacionAcometida;
    }

    public void setResultadoInstalacionAcometida(String resultadoInstalacionAcometida) {
        this.resultadoInstalacionAcometida = resultadoInstalacionAcometida;
    }

    public String getNombreResultadoInstalacionAcometida() {
        return nombreResultadoInstalacionAcometida;
    }

    public void setNombreResultadoInstalacionAcometida(String nombreResultadoInstalacionAcometida) {
        this.nombreResultadoInstalacionAcometida = nombreResultadoInstalacionAcometida;
    }

    public String getTipoGabinete() {
        return tipoGabinete;
    }

    public void setTipoGabinete(String tipoGabinete) {
        this.tipoGabinete = tipoGabinete;
    }

    public String getNombreTipoGabinete() {
        return nombreTipoGabinete;
    }

    public void setNombreTipoGabinete(String nombreTipoGabinete) {
        this.nombreTipoGabinete = nombreTipoGabinete;
    }

    public String getDiametroTuberiaConexionInstalacionAcometida() {
        return diametroTuberiaConexionInstalacionAcometida;
    }

    public void setDiametroTuberiaConexionInstalacionAcometida(String diametroTuberiaConexionInstalacionAcometida) {
        this.diametroTuberiaConexionInstalacionAcometida = diametroTuberiaConexionInstalacionAcometida;
    }

    public String getLongitudTuberiaConexionInstalacionAcometida() {
        return longitudTuberiaConexionInstalacionAcometida;
    }

    public void setLongitudTuberiaConexionInstalacionAcometida(String longitudTuberiaConexionInstalacionAcometida) {
        this.longitudTuberiaConexionInstalacionAcometida = longitudTuberiaConexionInstalacionAcometida;
    }

    public String getNombreMaterialInstalacion() {
        return nombreMaterialInstalacion;
    }

    public void setNombreMaterialInstalacion(String nombreMaterialInstalacion) {
        this.nombreMaterialInstalacion = nombreMaterialInstalacion;
    }

    public String getNombreFotoActaInstalacionTuberiaConexionAcometida() {
        return nombreFotoActaInstalacionTuberiaConexionAcometida;
    }

    public void setNombreFotoActaInstalacionTuberiaConexionAcometida(String nombreFotoActaInstalacionTuberiaConexionAcometida) {
        this.nombreFotoActaInstalacionTuberiaConexionAcometida = nombreFotoActaInstalacionTuberiaConexionAcometida;
    }

    public String getNombreFotoTuberiaConexionTerminada() {
        return nombreFotoTuberiaConexionTerminada;
    }

    public void setNombreFotoTuberiaConexionTerminada(String nombreFotoTuberiaConexionTerminada) {
        this.nombreFotoTuberiaConexionTerminada = nombreFotoTuberiaConexionTerminada;
    }

    public String getNombreFotoGabineteManifoldTerminado() {
        return nombreFotoGabineteManifoldTerminado;
    }

    public void setNombreFotoGabineteManifoldTerminado(String nombreFotoGabineteManifoldTerminado) {
        this.nombreFotoGabineteManifoldTerminado = nombreFotoGabineteManifoldTerminado;
    }

    public String getNombreTipoInstalacionAcometida() {
        return nombreTipoInstalacionAcometida;
    }

    public void setNombreTipoInstalacionAcometida(String nombreTipoInstalacionAcometida) {
        this.nombreTipoInstalacionAcometida = nombreTipoInstalacionAcometida;
    }

    public String getTipoInstalacionAcometida() {
        return tipoInstalacionAcometida;
    }

    public void setTipoInstalacionAcometida(String tipoInstalacionAcometida) {
        this.tipoInstalacionAcometida = tipoInstalacionAcometida;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
