package pe.gob.osinergmin.gnr.cgn.actividad;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

//import com.crashlytics.android.Crashlytics;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;
import java.util.Locale;

import gob.osinergmin.gnr.domain.dto.rest.out.AccessResponseOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.ParametroOutRO;
//import io.fabric.sdk.android.Fabric;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.BuildConfig;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.ConfigDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Usuario;
import pe.gob.osinergmin.gnr.cgn.data.models.UsuarioDao;
import pe.gob.osinergmin.gnr.cgn.data.pojos.Download;
import pe.gob.osinergmin.gnr.cgn.service.AlarmService;
import pe.gob.osinergmin.gnr.cgn.service.DownloadService;
import pe.gob.osinergmin.gnr.cgn.task.Login;
import pe.gob.osinergmin.gnr.cgn.task.Mapa;
import pe.gob.osinergmin.gnr.cgn.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.util.NetworkUtil;
import pe.gob.osinergmin.gnr.cgn.util.ResponseReceiver;
import pe.gob.osinergmin.gnr.cgn.util.Urls;
import pe.gob.osinergmin.gnr.cgn.util.Util;

public class IniciarActivity extends AppCompatActivity {

    public static final String MESSAGE_PROGRESS = "message_progress";
    private TextInputLayout mUsuarioView;
    private CheckBox mRecordarUsuario;
    private TextInputLayout mClaveView;
    private CompoundButton offline;
    private LinearLayout ingresarDNI;
    private LinearLayout ingresarClave;
    private LinearLayout btn_offline;
    private TextView accederOff;
    private Button recuperarContrasena;
    private Spinner sesionDni;
    private EditText documento;
    private View mSesionProgresoView;
    private View mInicioFormularioView;
    private ProgressBar mProgressBar;
    private TextView mProgressText;
    private Button reintentar;
    private UsuarioDao usuarioDao;
    private Usuario usuario;
    private ConfigDao configDao;
    private Config config;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (MESSAGE_PROGRESS.equalsIgnoreCase(intent.getAction())) {
                Download download = intent.getParcelableExtra("download");
                mProgressBar.setProgress(download.getProgress());
                if (download.getProgress() == 100) {
                    mProgressText.setText("Se terminó la descarga del mapa");
                    mProgressBar.setVisibility(View.GONE);
                    mProgressText.setVisibility(View.GONE);
                    mInicioFormularioView.setVisibility(View.VISIBLE);
                } else if (download.getProgress() == 111) {
                    mProgressText.setText("Error al descargar el mapa, vuelva a intentarlo.");
                    reintentar.setVisibility(View.VISIBLE);
                    Util.eliminarMap(IniciarActivity.this);
                } else {
                    mProgressText.setText(String.format(Locale.US, "Descargando el mapa (%d/%d) MB", download.getCurrentFileSize(), download.getTotalFileSize()));
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!BuildConfig.DEBUG) {
            //Fabric.with(getApplicationContext(), new Crashlytics());
        }
        setContentView(R.layout.activity_iniciar);
        IntentFilter intentFilter = new IntentFilter(Constantes.ACTION_EXIT_SERVICE);
        intentFilter.addAction(Constantes.ACTION_OFFLINE_ON);
        intentFilter.addAction(Constantes.ACTION_OFFLINE_OFF);
        ResponseReceiver responseReceiver = new ResponseReceiver();

        LocalBroadcastManager mBroadcastMgr = LocalBroadcastManager.getInstance(getApplicationContext());
        mBroadcastMgr.unregisterReceiver(responseReceiver);
        mBroadcastMgr.registerReceiver(responseReceiver, intentFilter);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        usuarioDao = ((App) getApplication()).getDaoSession().getUsuarioDao();
        configDao = ((App) getApplication()).getDaoSession().getConfigDao();

        mUsuarioView = findViewById(R.id.usuario);
        mRecordarUsuario = findViewById(R.id.recordarUsuario);
        mClaveView = findViewById(R.id.clave);
        accederOff = findViewById(R.id.accederOff);
        mInicioFormularioView = findViewById(R.id.scrollSesion);
        mSesionProgresoView = findViewById(R.id.progressSesion);
        mProgressBar = findViewById(R.id.progress);
        mProgressText = findViewById(R.id.progress_text);
        mClaveView.setTypeface(Typeface.DEFAULT);
        offline = findViewById(R.id.offline);
        ingresarDNI = findViewById(R.id.ingresarDNI);
        ingresarClave = findViewById(R.id.ingresarClave);
        Button mSesionIngresarButton = findViewById(R.id.sesionIngresar);
        recuperarContrasena = findViewById(R.id.sesion_recuperar);
        sesionDni = findViewById(R.id.sesionDni);
        documento = findViewById(R.id.documento);
        btn_offline = findViewById(R.id.btn_offline);
        reintentar = findViewById(R.id.reintentar);

        if (mSesionIngresarButton != null) {
            mSesionIngresarButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
                    if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                        Toast.makeText(IniciarActivity.this, "Activar el GPS para iniciar sesión", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    InputMethodManager tecladoVirtual = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    if (tecladoVirtual != null) {
                        tecladoVirtual.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                    verificarSesion();
                }
            });
        }

        if (recuperarContrasena != null) {
            recuperarContrasena.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(IniciarActivity.this, RecuperarContrasenaActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        }

        offline.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ingresarDNI.setVisibility(View.VISIBLE);
                    ingresarClave.setVisibility(View.GONE);
                    recuperarContrasena.setVisibility(View.GONE);
                } else {
                    ingresarDNI.setVisibility(View.GONE);
                    ingresarClave.setVisibility(View.VISIBLE);
                    recuperarContrasena.setVisibility(View.VISIBLE);
                }
            }
        });

        sesionDni.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    documento.setHint(R.string.sesion_dni);
                    if (documento.getText().length() > 8) {
                        documento.setText(documento.getText().toString().substring(0, 8));
                        documento.setSelection(documento.getText().length());
                    }
                } else if (position == 1) {
                    documento.setHint(R.string.sesion_ce);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        documento.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    if (sesionDni.getSelectedItemPosition() == 0) {
                        if (s.length() < 9) {

                        } else {
                            ((App) getApplication()).showToast(getResources().getString(R.string.app_error_dni));
                            documento.setText(s.subSequence(0, s.length() - 1));
                            documento.setSelection(documento.getText().length());
                        }
                    } else {
                        if (s.length() < 21) {

                        } else {
                            ((App) getApplication()).showToast(String.format(getResources().getString(R.string.app_error_campo), 20));
                            documento.setText(s.subSequence(0, s.length() - 1));
                            documento.setSelection(documento.getText().length());
                        }
                    }
                }
            }
        });

        reintentar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProgressText.setText("Volviendo a intentar la descarga del mapa.");
                startDownload();
            }
        });

        if (Urls.getSSL()) {
            Util.enableSSL();
        }
        aceptarPermiso();
        registerReceiver();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //((App) getApplication()).isRooted(IniciarActivity.this);
        List<Usuario> usuarios = usuarioDao.queryBuilder().list();
        if (!(usuarios.size() > 0)) {
            accederOff.setVisibility(View.GONE);
            offline.setVisibility(View.GONE);
        }
        addUsuarioToAutoComplete();
        createAlerta();
    }

    private void createAlerta() {
        if (config.getOffline()) {
            SharedPreferences mPreferences = getSharedPreferences("gnrconcesionario", Context.MODE_PRIVATE);
            long fecha = mPreferences.getLong("fecha", 0);
            if (fecha != 0) {
                AlarmService.startActionAlarm(getApplicationContext(), fecha);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION:
                String message = Util.onRequestPermissionsResult(permissions, grantResults);
                startDownload();
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void startDownload() {
        reintentar.setVisibility(View.GONE);
        Util.getAlbumStorageDir(IniciarActivity.this);
        String[] permiso = Util.getPermisos(IniciarActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permiso == null) {
            if (Util.isOkMapa(IniciarActivity.this)) {
                if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    Mapa mapa = new Mapa(new Mapa.OnMapaCompleted() {
                        @Override
                        public void onMapaCompleted(ParametroOutRO parametroOutRO) {
                            if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                mProgressBar.setVisibility(View.VISIBLE);
                                mProgressText.setVisibility(View.VISIBLE);
                                mInicioFormularioView.setVisibility(View.GONE);
                                mProgressText.setText("Error al descargar el mapa, vuelva a intentarlo.");
                                reintentar.setVisibility(View.VISIBLE);
                                System.out.println("1");
                                ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                            } else if (parametroOutRO == null) {
                                mProgressBar.setVisibility(View.VISIBLE);
                                mProgressText.setVisibility(View.VISIBLE);
                                mInicioFormularioView.setVisibility(View.GONE);
                                mProgressText.setText("Error al descargar el mapa, vuelva a intentarlo.");
                                reintentar.setVisibility(View.VISIBLE);
                                System.out.println("2");
                                ((App) getApplication()).showToast(getResources().getString(R.string.sesion_error_mapa));
                            } else if (!parametroOutRO.getResultCode().equalsIgnoreCase(gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS)) {
                                mProgressBar.setVisibility(View.VISIBLE);
                                mProgressText.setVisibility(View.VISIBLE);
                                mInicioFormularioView.setVisibility(View.GONE);
                                mProgressText.setText("Error al descargar el mapa, vuelva a intentarlo.");
                                reintentar.setVisibility(View.VISIBLE);
                                System.out.println("3");
                                ((App) getApplication()).showToast(parametroOutRO.getMessage() == null ? getResources().getString(R.string.sesion_error_mapa) : parametroOutRO.getMessage());
                            } else {
                                mProgressBar.setVisibility(View.VISIBLE);
                                mProgressText.setVisibility(View.VISIBLE);
                                mInicioFormularioView.setVisibility(View.GONE);
                                System.out.println("4");
                                Intent intent = new Intent(IniciarActivity.this, DownloadService.class);
                                intent.putExtra("URL", parametroOutRO.getValorParametro());
                                startService(intent);
                            }
                        }
                    });
                    mapa.execute("");
                } else {
                    mProgressBar.setVisibility(View.VISIBLE);
                    mProgressText.setVisibility(View.VISIBLE);
                    mInicioFormularioView.setVisibility(View.GONE);
                    mProgressText.setText("Error al descargar el mapa, vuelva a intentarlo.");
                    reintentar.setVisibility(View.VISIBLE);
                    ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                }
            }
        } else {
            mProgressBar.setVisibility(View.VISIBLE);
            mProgressText.setVisibility(View.VISIBLE);
            mInicioFormularioView.setVisibility(View.GONE);
            mProgressText.setText("La aplicación no tiene permiso de almacenamiento.");
            reintentar.setVisibility(View.GONE);
        }
    }

    private void registerReceiver() {
        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MESSAGE_PROGRESS);
        bManager.registerReceiver(broadcastReceiver, intentFilter);
    }

    private void verificarSesion() {
        usuario = new Usuario(null, "", "", "", "", "", "", "", "", "", "", "", "", "", "");
        if (offline.isChecked()) {
            mUsuarioView.setError(null);
            documento.setError(null);
            usuario.setUsername(mUsuarioView.getEditText().getText().toString().trim());
            usuario.setDni(documento.getText().toString());
            usuario.setTipo(sesionDni.getSelectedItemPosition() == 0 ? gob.osinergmin.gnr.util.Constantes.TIPO_DOCUMENTO_IDENTIFICACION_DNI : gob.osinergmin.gnr.util.Constantes.TIPO_DOCUMENTO_IDENTIFICACION_CE);
            boolean cancel = false;
            View focusView = null;
            if (TextUtils.isEmpty(usuario.getUsername())) {
                mUsuarioView.setError(getString(R.string.sesion_requerido));
                focusView = mUsuarioView;
                cancel = true;
            }
            if (TextUtils.isEmpty(usuario.getDni())) {
                documento.setError(getString(R.string.sesion_requerido));
                focusView = documento;
                cancel = true;
            }

            if (cancel) {
                focusView.requestFocus();
            } else {
                loginLocal();
            }
        } else {
            mUsuarioView.setError(null);
            mClaveView.setError(null);

            usuario.setUsername(mUsuarioView.getEditText().getText().toString().trim());
            usuario.setPassword(mClaveView.getEditText().getText().toString().trim());

            boolean cancel = false;
            View focusView = null;

            if (TextUtils.isEmpty(usuario.getUsername())) {
                mUsuarioView.setError(getString(R.string.sesion_requerido));
                focusView = mUsuarioView;
                cancel = true;
            }

            if (TextUtils.isEmpty(usuario.getPassword())) {
                mClaveView.setError(getString(R.string.sesion_requerido));
                focusView = mClaveView;
                cancel = true;
            }

            if (cancel) {
                focusView.requestFocus();
            } else {
                if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    showProgress(true);
                    Login login = new Login(new Login.OnLoginCompleted() {
                        @Override
                        public void onLoginCompled(AccessResponseOutRO accessResponseOutRO) {
                            if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                showProgress(false);
                                ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                            } else if (accessResponseOutRO == null) {
                                showProgress(false);
                                ((App) getApplication()).showToast("Usuario o Contraseña incorrecto");
                            } else if (!gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(accessResponseOutRO.getResultCode())) {
                                showProgress(false);
                                ((App) getApplication()).showToast(accessResponseOutRO.getMessage() == null ? "Usuario o Contraseña incorrecto" : accessResponseOutRO.getMessage());
                            } else {
                                config.setToken(accessResponseOutRO.getToken());
                                System.out.println("Token: " +accessResponseOutRO.getToken());
                                usuario.setTipo(accessResponseOutRO.getTipoIdentificacionUsuario());
                                usuario.setDni(accessResponseOutRO.getNumeroIdentificacionUsuario());
                                usuario.setTexto(accessResponseOutRO.getNombreUsuario());
                                if (mRecordarUsuario.isChecked()) {
                                    config.setRecordarUsuario("1");
                                } else {
                                    config.setRecordarUsuario("");
                                }
                                config.setUsername(usuario.getUsername());
                                config.setNombreUsuario(usuario.getTexto());
                                config.setLogin(true);
                                usuarioDao.queryBuilder().where(UsuarioDao.Properties.Username.eq(usuario.getUsername())).buildDelete().executeDeleteWithoutDetachingEntities();
                                usuarioDao.insertOrReplace(usuario);
                                configDao.insertOrReplace(config);

                                Intent intent = new Intent(IniciarActivity.this, PrincipalActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                    });
                    login.execute(usuario);
                } else {
                    ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                }
            }
        }
    }

    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mInicioFormularioView.setVisibility(show ? View.GONE : View.VISIBLE);
        mInicioFormularioView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mInicioFormularioView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mSesionProgresoView.setVisibility(show ? View.VISIBLE : View.GONE);
        mSesionProgresoView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mSesionProgresoView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    private void addUsuarioToAutoComplete() {
        List configs = configDao.queryBuilder().list();
        if (configs.size() > 0) {
            config = (Config) configs.get(0);
            if ("1".equalsIgnoreCase(config.getRecordarUsuario())) {
                mUsuarioView.getEditText().setText(config.getUsername());
                mRecordarUsuario.setChecked(true);
            }
            if (config.getOffline()) {
                btn_offline.setVisibility(View.GONE);
            }
            offline.setChecked(config.getOffline());
        } else {
            config = new Config(null, "", "", false, "",
                    "", "", "", "", "", "",
                    "", "", "", 0, 0,
                    0, false, false, false,
                    false, "", false, false, false,
                    "10000", "",false,"","");
            offline.setChecked(false);
        }
    }

    private void loginLocal() {
        List<Usuario> tmps = usuarioDao.queryBuilder()
                .where(UsuarioDao.Properties.Username.eq(usuario.getUsername()),
                        UsuarioDao.Properties.Tipo.eq(usuario.getTipo()),
                        UsuarioDao.Properties.Dni.eq(usuario.getDni()))
                .list();
        if (tmps.size() > 0) {
            config.setNombreUsuario(tmps.get(0).getTexto());
            config.setLogin(true);
            if (mRecordarUsuario.isChecked()) {
                config.setRecordarUsuario("1");
            } else {
                config.setRecordarUsuario("");
            }
            configDao.insertOrReplace(config);
            Intent intent = new Intent(IniciarActivity.this, PrincipalActivity.class);
            startActivity(intent);
            finish();
        } else {
            ((App) getApplication()).showToast("Usuario o DNI incorrecto");
        }
    }

    private void aceptarPermiso() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permiso = Util.getPermisos(IniciarActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CALL_PHONE);
            if (permiso == null) {
                startDownload();
            } else {
                requestPermissions(permiso, pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION);
            }
        } else {
            startDownload();
        }
    }
}
