package pe.gob.osinergmin.gnr.cgn.adaptador;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;

import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.BaseInspectorOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.GenericBeanOutRO;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.actividad.Acometida2Activity;
import pe.gob.osinergmin.gnr.cgn.data.models.Acometida;
import pe.gob.osinergmin.gnr.cgn.data.models.AcometidaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.BaseOutroResultado;
import pe.gob.osinergmin.gnr.cgn.data.models.Observacion;
import pe.gob.osinergmin.gnr.cgn.data.models.ObservacionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Punto;
import pe.gob.osinergmin.gnr.cgn.data.models.TipoPendiente;
import pe.gob.osinergmin.gnr.cgn.data.models.TipoRechazada;
import pe.gob.osinergmin.gnr.cgn.data.models.Tuberias;
import pe.gob.osinergmin.gnr.cgn.data.models.TuberiasDao;
import pe.gob.osinergmin.gnr.cgn.util.MostrarFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.Parse;
import pe.gob.osinergmin.gnr.cgn.util.SearchableSpinner;
import pe.gob.osinergmin.gnr.cgn.util.Util;

import static gob.osinergmin.gnr.util.Constantes.RESULTADO_INSTALACION_ACOMETIDA_CONCLUIDA;
import static gob.osinergmin.gnr.util.Constantes.RESULTADO_INSTALACION_ACOMETIDA_PENDIENTE;
import static gob.osinergmin.gnr.util.Constantes.RESULTADO_INSTALACION_ACOMETIDA_RECHAZADA;
import static gob.osinergmin.gnr.util.Constantes.TIPO_INSTALACION_ACOMETIDA_ACOMETIDA;
import static gob.osinergmin.gnr.util.Constantes.TIPO_INSTALACION_ACOMETIDA_TC;

public class TubyTcAdaptador extends BaseAdapter{

    private Context context;
    private List<Tuberias> tuberias;

    private List<BaseInspectorOutRO> listaTipoFusionista = new ArrayList<>();
    private List<GenericBeanOutRO> listaTipoResultado = new ArrayList<>();
    private List<GenericBeanOutRO> listaTipoMaterial = new ArrayList<>();
    private List<GenericBeanOutRO> listatipoObsePendiente = new ArrayList<>();
    private List<GenericBeanOutRO> listatipoObseRechazado = new ArrayList<>();

    private ListView listPuntos;
    private SwitchCompat aprueba;
    private AcometidaDao acometidaDao;
    private Callback callback;
    private Button btnIns1_grabar;
    private ObservacionAdaptador observacionAdaptador;
    private ObservacionDao observacionDao;
    private TuberiasDao tuberiasDao;
    private List<Observacion> observaciones = new ArrayList<>();
    private int Id;
    private boolean bandera=false;

    public TubyTcAdaptador(Context context, List<Tuberias> tuberias) {
        this.context = context;
        this.tuberias = tuberias;
    }

    public void setListatipoObsePendiente(List<GenericBeanOutRO> listatipoObsePendiente) {
        this.listatipoObsePendiente = listatipoObsePendiente;
    }

    public void setListatipoObseRechazado(List<GenericBeanOutRO> listatipoObseRechazado) {
        this.listatipoObseRechazado = listatipoObseRechazado;
    }

    public void setListatipoFusionista(List<BaseInspectorOutRO> listaTipoFusionista) {
        this.listaTipoFusionista = listaTipoFusionista;
    }

    public void setListatipoResultado(List<GenericBeanOutRO> listaTipoResultado) {
        this.listaTipoResultado = listaTipoResultado;
    }

    public void setListatipoMaterial(List<GenericBeanOutRO> listaTipoMaterial) {
        this.listaTipoMaterial = listaTipoMaterial;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void setAcometidaDao(AcometidaDao acometidaDao) {
        this.acometidaDao = acometidaDao;
    }

    public void setBtnVis1_grabar(Button btnIns1_grabar) {
        this.btnIns1_grabar = btnIns1_grabar;
    }

    public void setObservacionDao(ObservacionDao observacionDao) {
        this.observacionDao = observacionDao;
    }

    public void setTuberiasDao(TuberiasDao tuberiasDao) {
        this.tuberiasDao = tuberiasDao;
    }

    public void setAcometidas(ListView listPuntos) {
        this.listPuntos = listPuntos;
    }

    public void setAprueba(SwitchCompat aprueba) {
        this.aprueba = aprueba;
    }

    public void setId(int Id){
        this.Id = Id;
    }

    @Override
    public int getCount() {
        if (tuberias.size() == 0) {
            aprueba.setClickable(false);
        }
        return this.tuberias.size();
    }

    public Tuberias getAcometida(int position) {
        for (Tuberias tuberia : tuberias) {
            if (tuberia.getPosicion() == position) {
                return tuberia;
            }
        }
        return null;
    }

    @Override
    public Object getItem(int position) {
        return this.tuberias.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {
        View rowView = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = layoutInflater.inflate(R.layout.item_tuberia, parent, false);

            holder.txtTub_tuberia = rowView.findViewById(R.id.txtTub_tuberia);
            holder.spiAco_resultado = rowView.findViewById(R.id.spiAco_resultado);
            holder.spiAco_fusionista = rowView.findViewById(R.id.spiAco_fusionista);
            holder.spiAco_material = rowView.findViewById(R.id.spiAco_material);
            holder.ediAco_diametro = rowView.findViewById(R.id.ediAco_diametro);
            holder.ediAco_longitud = rowView.findViewById(R.id.ediAco_longitud);
            holder.imgAco_tc = rowView.findViewById(R.id.imgAco_tc);
            holder.icoAco_tc = rowView.findViewById(R.id.icoAco_tc);
            holder.icoAco_material = rowView.findViewById(R.id.icoAco_material);
            holder.icoAco_longitud = rowView.findViewById(R.id.icoAco_longitud);
            holder.icoAco_diametro = rowView.findViewById(R.id.icoAco_diametro);
            holder.icoAco_resultado = rowView.findViewById(R.id.icoAco_resultado);
            holder.icoAco_fusionista = rowView.findViewById(R.id.icoAco_fusionista);
            holder.concluido = rowView.findViewById(R.id.concluido);
            holder.ll_observaciones = rowView.findViewById(R.id.observaciones);
            holder.addObservacion = rowView.findViewById(R.id.addObservacion);
            holder.listaObservacionAcometida = rowView.findViewById(R.id.listaObservacionAcometida);
            rowView.setTag(holder);

        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        holder.ref = position;
        tuberias.get(position).setPosicion(position);

        System.out.println("Resultado:" +tuberias.get(holder.ref).getResultado());
        if(tuberias.get(holder.ref).getResultado().equals("C")){
            holder.concluido.setVisibility(View.VISIBLE);
            holder.ll_observaciones.setVisibility(View.GONE);
        }else if(tuberias.get(holder.ref).getResultado().equals("P")){
            holder.concluido.setVisibility(View.GONE);
            holder.ll_observaciones.setVisibility(View.VISIBLE);
        }else if(tuberias.get(holder.ref).getResultado().equals("R")){
            holder.concluido.setVisibility(View.GONE);
            holder.ll_observaciones.setVisibility(View.VISIBLE);
        }else{
            holder.concluido.setVisibility(View.GONE);
            holder.ll_observaciones.setVisibility(View.GONE);
        }
        /*holder.remPunI_punto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getCount() > 1) {
                    puntoDao.delete(puntos.get(holder.ref));
                    puntos.remove(holder.ref);
                    notifyDataSetChanged();
                    Util.setListViewHeightBasedOnChildren(listPuntos);
                    check();
                } else {
                    ((App) context.getApplicationContext()).showToast("Se debe registrar un punto como mínimo");
                }
            }
        });*/

        String recurso = rowView.getResources().getString(R.string.txtTuberia_conexion);
        String formateada = String.format(recurso, String.valueOf(position+1));
        holder.txtTub_tuberia.setText(formateada);

        //Fusionista
        //int i = getFusionista(tuberias.get(holder.ref).getFusionista());
        ArrayList<String> listfusionista = new ArrayList<>();
        listfusionista.add("Seleccione fusionista/técnico instalador");
        for (BaseInspectorOutRO genericBeanOutRO : listaTipoFusionista)   {
            listfusionista.add(genericBeanOutRO.getNombreInspector());
            System.out.println("Aqui cero");
        }
        System.out.println("listaTipoFusionista0: "+listaTipoFusionista.get(0).getNombreInspector());

        ArrayAdapter<String> adapterfusionista = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, listfusionista);
        adapterfusionista.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        holder.spiAco_fusionista.setAdapter(adapterfusionista);
        holder.spiAco_fusionista.setTitle("Ingrese datos del fusionista/técnico instalador");
        holder.spiAco_fusionista.setPositiveButton("Cerrar");
        holder.spiAco_fusionista.setTag(0);
        System.out.println("Aqui uno");
        holder.spiAco_fusionista.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
               // System.out.println("Aqui dos");
                if (i == (int) holder.spiAco_fusionista.getTag()) {
                    System.out.println("Aqui tres");
                    return;
                }else{
                    System.out.println("Aqui cuatro");
                    holder.spiAco_fusionista.setTag(i);
                    if (i != 0) {
                        System.out.println("Aqui cinco");
                        tuberias.get(holder.ref).setFusionista(listaTipoFusionista.get(i-1).getIdInspector().toString());
                    } else {
                        System.out.println("Aqui seis");
                        tuberias.get(holder.ref).setFusionista(" ");
                    }
                    System.out.println("Aqui siete");
                    tuberiasDao.insertOrReplace(tuberias.get(holder.ref));
                    validarFusionista(holder);
                    check();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                System.out.println("Aqui ocho");
            }
        });

        if (!"".equalsIgnoreCase(tuberias.get(holder.ref).getFusionista())) {
            holder.spiAco_fusionista.setSelection(getFusionista(tuberias.get(holder.ref).getFusionista()));
        }

        //resultado
        holder.spiAco_resultado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //System.out.println("rest: " +listaTipoResultado.get(position).getLabel());
                holder.check++;
                if (holder.check > 1) {
                    if (position != 0) {
                        tuberias.get(holder.ref).setResultado(setTipoResultado(parent.getSelectedItem().toString()));
                    } else {
                        tuberias.get(holder.ref).setResultado("");
                    }

                    switch (tuberias.get(holder.ref).getResultado()) {
                        case RESULTADO_INSTALACION_ACOMETIDA_CONCLUIDA:
                            holder.concluido.setVisibility(View.VISIBLE);
                            holder.ll_observaciones.setVisibility(View.GONE);
                            /*switch (acometida.getTipoInstalacionAcometida()) {
                                case TIPO_INSTALACION_ACOMETIDA_ACOMETIDA:
                                    llAcometida.setVisibility(View.VISIBLE);
                                    llTc.setVisibility(View.GONE);
                                    break;
                                case TIPO_INSTALACION_ACOMETIDA_TC:
                                    llAcometida.setVisibility(View.GONE);
                                    llTc.setVisibility(View.VISIBLE);
                                    break;
                                default:
                                    llAcometida.setVisibility(View.GONE);
                                    llTc.setVisibility(View.GONE);
                            }*/
                            tuberiasDao.insertOrReplace(tuberias.get(holder.ref));
                            validarResultado(holder);
                            check();
                            break;
                        case RESULTADO_INSTALACION_ACOMETIDA_PENDIENTE:
                        case RESULTADO_INSTALACION_ACOMETIDA_RECHAZADA:
                            holder.concluido.setVisibility(View.GONE);
                            holder.ll_observaciones.setVisibility(View.VISIBLE);
                            System.out.println("position: " + position);
                            bandera = true;
                            tuberiasDao.insertOrReplace(tuberias.get(holder.ref));
                            callback.cambiarVista(holder.ref);
                            if (RESULTADO_INSTALACION_ACOMETIDA_PENDIENTE.equalsIgnoreCase(tuberias.get(holder.ref).getResultado())) {
                                System.out.println("Click");
                                Observacion observacion = new Observacion(null, Id, 0, "", "");
                                observaciones.add(observacion);
                                observacionAdaptador = new ObservacionAdaptador(context, observaciones);
                                holder.listaObservacionAcometida.setAdapter(observacionAdaptador);
                                observacionAdaptador.setBtnVis1_grabar(btnIns1_grabar);
                                observacionAdaptador.setListaObservacionInstalacion(holder.listaObservacionAcometida);
                                observacionAdaptador.setObservacionDao(observacionDao);
                                observacionAdaptador.setListatipoObse(listatipoObsePendiente);
                                observacionAdaptador.setCallback((ObservacionAdaptador.CallBack) callback);
                                observacionAdaptador.notifyDataSetChanged();
                            } else {
                                System.out.println("Click");
                                Observacion observacion = new Observacion(null, Id, 0, "", "");
                                observaciones.add(observacion);
                                observacionAdaptador = new ObservacionAdaptador(context, observaciones);
                                holder.listaObservacionAcometida.setAdapter(observacionAdaptador);
                                observacionAdaptador.setBtnVis1_grabar(btnIns1_grabar);
                                observacionAdaptador.setListaObservacionInstalacion(holder.listaObservacionAcometida);
                                observacionAdaptador.setObservacionDao(observacionDao);
                                observacionAdaptador.setListatipoObse(listatipoObseRechazado);
                                observacionAdaptador.setCallback((ObservacionAdaptador.CallBack) callback);
                                observacionAdaptador.notifyDataSetChanged();
                            }
                            /*List<Observacion> a = observacionDao.queryBuilder()
                                    .where(ObservacionDao.Properties.IdInstalacionMontante.eq(acometida.getIdInstalacionAcometida())).list();
                            observacionDao.deleteInTx(a);
                            if (config.getOffline()) {
                                if (RESULTADO_INSTALACION_ACOMETIDA_PENDIENTE.equalsIgnoreCase(acometida.getResultado())) {
                                    listatipoObse.clear();
                                    List<TipoPendiente> tipoPendientes = tipoPendienteDao.queryBuilder().list();
                                    for (TipoPendiente tipoPendiente : tipoPendientes) {
                                        listatipoObse.add(Parse.getGenericBeanOutRO(tipoPendiente));
                                    }
                                    configurarObs();
                                } else {
                                    listatipoObse.clear();
                                    List<TipoRechazada> tipoRechazadas = tipoRechazadaDao.queryBuilder().list();
                                    for (TipoRechazada tipoRechazada : tipoRechazadas) {
                                        listatipoObse.add(Parse.getGenericBeanOutRO(tipoRechazada));
                                    }
                                    configurarObs();
                                }
                            } else {
                                callTiposObservacion();
                            }*/
                            tuberiasDao.insertOrReplace(tuberias.get(holder.ref));
                            validarResultado(holder);
                            check();
                            break;
                        default:
                            holder.concluido.setVisibility(View.GONE);
                            holder.ll_observaciones.setVisibility(View.GONE);
                    }
                    tuberiasDao.insertOrReplace(tuberias.get(holder.ref));
                    validarResultado(holder);
                    check();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ArrayList<String> listresultado = new ArrayList<>();
        listresultado.add("Seleccionar resultado");
        for (GenericBeanOutRO genericBeanOutRO : listaTipoResultado) {
            listresultado.add(genericBeanOutRO.getLabel());
        }

        ArrayAdapter<String> adapterresultado = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, listresultado);
        adapterresultado.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        holder.spiAco_resultado.setAdapter(adapterresultado);

        if (!"".equalsIgnoreCase(tuberias.get(holder.ref).getResultado())) {
            holder.spiAco_resultado.setSelection(getTipoResultado(tuberias.get(holder.ref).getResultado()));
            validarResultado(holder);
        }

        //material
        holder.spiAco_material.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //System.out.println("rest: " +listaTipoResultado.get(position).getLabel());
                holder.check++;
                if (holder.check > 1) {
                    if (position != 0) {
                        tuberias.get(holder.ref).setMaterial(setTipoMaterial(parent.getSelectedItem().toString()));
                    } else {
                        tuberias.get(holder.ref).setMaterial("");
                    }
                    tuberiasDao.insertOrReplace(tuberias.get(holder.ref));
                    validarMaterial(holder);
                    check();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ArrayList<String> listmaterial = new ArrayList<>();
        listmaterial.add("Seleccionar material");
        for (GenericBeanOutRO genericBeanOutRO : listaTipoMaterial) {
            listmaterial.add(genericBeanOutRO.getLabel());
        }

        ArrayAdapter<String> adaptermaterial = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, listmaterial);
        adaptermaterial.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        holder.spiAco_material.setAdapter(adaptermaterial);

        if (!"".equalsIgnoreCase(tuberias.get(holder.ref).getMaterial())) {
            holder.spiAco_material.setSelection(getTipoMaterial(tuberias.get(holder.ref).getMaterial()));
        }

        holder.ediAco_diametro.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Util.validarScaleAndPrecision(s.toString(), 2, 5)) {
                    tuberias.get(holder.ref).setDiametro(s.toString());
                } else {
                    ((App) context.getApplicationContext()).showToast(String.format(context.getResources().getString(R.string.app_error_numero), 5, 2));
                    holder.ediAco_diametro.getEditText().setText(tuberias.get(holder.ref).getDiametro());
                    holder.ediAco_diametro.getEditText().setSelection(holder.ediAco_diametro.getEditText().getText().length());
                }
                tuberiasDao.insertOrReplace(tuberias.get(holder.ref));
                validarDiametro(holder);
                check();
            }
        });

        holder.ediAco_longitud.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Util.validarScaleAndPrecision(s.toString(), 2, 5)) {
                    tuberias.get(holder.ref).setLongitud(s.toString());
                } else {
                    ((App) context.getApplicationContext()).showToast(String.format(context.getResources().getString(R.string.app_error_numero), 5, 2));
                    holder.ediAco_longitud.getEditText().setText(tuberias.get(holder.ref).getLongitud());
                    holder.ediAco_longitud.getEditText().setSelection(holder.ediAco_longitud.getEditText().getText().length());
                }
                tuberiasDao.insertOrReplace(tuberias.get(holder.ref));
                validarLongitud(holder);
                check();
            }
        });

        holder.imgAco_tc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("position: " + position);
                callback.clicFoto(position);
            }
        });

        if (!"".equalsIgnoreCase(tuberias.get(holder.ref).getFotoTC())) {
            loadBitmap(tuberias.get(holder.ref).getFotoTC(), holder.imgAco_tc);
            validarFotoTC(holder);
            check();
        } else {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(65, 65);
            layoutParams.setMargins(10, 10, 30, 10);
            holder.imgAco_tc.setLayoutParams(layoutParams);
            holder.imgAco_tc.setScaleType(ImageView.ScaleType.FIT_CENTER);
            holder.imgAco_tc.setImageResource(R.mipmap.ic_add_a_photo_black_36dp);
        }

        if (!tuberias.get(holder.ref).getDiametro().equalsIgnoreCase("")) {
            holder.ediAco_diametro.getEditText().setText(tuberias.get(holder.ref).getDiametro());
            holder.ediAco_diametro.getEditText().setSelection(holder.ediAco_diametro.getEditText().getText().length());
        } else {
            holder.ediAco_diametro.getEditText().setText("");
        }

        if (!tuberias.get(holder.ref).getLongitud().equalsIgnoreCase("")) {
            holder.ediAco_longitud.getEditText().setText(tuberias.get(holder.ref).getLongitud());
            holder.ediAco_longitud.getEditText().setSelection(holder.ediAco_longitud.getEditText().getText().length());
        } else {
            holder.ediAco_longitud.getEditText().setText("");
        }

        holder.addObservacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (RESULTADO_INSTALACION_ACOMETIDA_PENDIENTE.equalsIgnoreCase(tuberias.get(holder.ref).getResultado())) {
                    System.out.println("Click");
                    Observacion observacion = new Observacion(null, Id, 0, "", "");
                    observaciones.add(observacion);
                    observacionAdaptador = new ObservacionAdaptador(context, observaciones);
                    holder.listaObservacionAcometida.setAdapter(observacionAdaptador);
                    observacionAdaptador.setBtnVis1_grabar(btnIns1_grabar);
                    observacionAdaptador.setListaObservacionInstalacion(holder.listaObservacionAcometida);
                    observacionAdaptador.setObservacionDao(observacionDao);
                    observacionAdaptador.setListatipoObse(listatipoObsePendiente);
                    observacionAdaptador.setCallback((ObservacionAdaptador.CallBack) callback);
                    observacionAdaptador.notifyDataSetChanged();
                } else {
                    System.out.println("Click");
                    Observacion observacion = new Observacion(null, Id, 0, "", "");
                    observaciones.add(observacion);
                    observacionAdaptador = new ObservacionAdaptador(context, observaciones);
                    holder.listaObservacionAcometida.setAdapter(observacionAdaptador);
                    observacionAdaptador.setBtnVis1_grabar(btnIns1_grabar);
                    observacionAdaptador.setListaObservacionInstalacion(holder.listaObservacionAcometida);
                    observacionAdaptador.setObservacionDao(observacionDao);
                    observacionAdaptador.setListatipoObse(listatipoObseRechazado);
                    observacionAdaptador.setCallback((ObservacionAdaptador.CallBack) callback);
                    observacionAdaptador.notifyDataSetChanged();
                }

                //Observacion observacion = new Observacion();
                //observacion.setIdInstalacionMontante(Id);
                //observacion.setDescripcionObservacion("");
                //observacion.setTipoObservacion("0");
                //observaciones.add(observacion);
                //observacionAdaptador.notifyDataSetChanged();
                //holder.listaObservacionAcometida.setAdapter(observacionAdaptador);
                //Util.setListViewHeightBasedOnChildren(holder.listaObservacionAcometida);
            }
        });
        validar(holder);
        check();
        return rowView;
    }

    private void validar(ViewHolder holder) {
        if ("".equalsIgnoreCase(tuberias.get(holder.ref).getFusionista()) ||
                "".equalsIgnoreCase(tuberias.get(holder.ref).getResultado()) ||
                "".equalsIgnoreCase(tuberias.get(holder.ref).getDiametro()) ||
                "".equalsIgnoreCase(tuberias.get(holder.ref).getLongitud()) ||
                "".equalsIgnoreCase(tuberias.get(holder.ref).getMaterial()) ||
                "".equalsIgnoreCase(tuberias.get(holder.ref).getFotoTC())) {
            btnIns1_grabar.setEnabled(false);
            btnIns1_grabar.setClickable(false);
        } else {
            btnIns1_grabar.setEnabled(true);
            btnIns1_grabar.setClickable(true);
        }
    }

    private void validarFusionista(ViewHolder holder) {
        if ("".equalsIgnoreCase(tuberias.get(holder.ref).getFusionista())) {
            holder.icoAco_fusionista.setImageResource(R.mipmap.ic_warning_black_36dp);
            //aprueba.setChecked(false);
        } else {
            holder.icoAco_fusionista.setImageResource(R.mipmap.ic_done_black_36dp);
        }
    }

    private void validarResultado(ViewHolder holder) {
        if ("".equalsIgnoreCase(tuberias.get(holder.ref).getResultado())) {
            holder.icoAco_resultado.setImageResource(R.mipmap.ic_warning_black_36dp);
            //aprueba.setChecked(false);
        } else {
            holder.icoAco_resultado.setImageResource(R.mipmap.ic_done_black_36dp);
        }
    }

    private void validarDiametro(ViewHolder holder) {
        if ("".equalsIgnoreCase(tuberias.get(holder.ref).getDiametro())) {
            holder.icoAco_diametro.setImageResource(R.mipmap.ic_warning_black_36dp);
            //aprueba.setChecked(false);
        } else {
            holder.icoAco_diametro.setImageResource(R.mipmap.ic_done_black_36dp);
        }
    }

    private void validarLongitud(ViewHolder holder) {
        if ("".equalsIgnoreCase(tuberias.get(holder.ref).getLongitud())) {
            holder.icoAco_longitud.setImageResource(R.mipmap.ic_warning_black_36dp);
            //aprueba.setChecked(false);
        } else {
            holder.icoAco_longitud.setImageResource(R.mipmap.ic_done_black_36dp);
        }
    }

    private void validarMaterial(ViewHolder holder) {
        if ("".equalsIgnoreCase(tuberias.get(holder.ref).getMaterial())) {
            holder.icoAco_material.setImageResource(R.mipmap.ic_warning_black_36dp);
            //aprueba.setChecked(false);
        } else {
            holder.icoAco_material.setImageResource(R.mipmap.ic_done_black_36dp);
        }
    }

    private void validarFotoTC(ViewHolder holder) {
        if ("".equalsIgnoreCase(tuberias.get(holder.ref).getFotoTC())) {
            holder.icoAco_tc.setImageResource(R.mipmap.ic_warning_black_36dp);
            //aprueba.setChecked(false);
        } else {
            holder.icoAco_tc.setImageResource(R.mipmap.ic_done_black_36dp);
        }
    }

    private void check() {
        int i = 0;
        boolean boton = true;
        while (i < tuberias.size()) {
            Tuberias tuberia = tuberias.get(i);

            if ("".equalsIgnoreCase(tuberia.getFusionista()) ||
                "".equalsIgnoreCase(tuberia.getResultado()) ||
                    "".equalsIgnoreCase(tuberia.getDiametro()) ||
                    "".equalsIgnoreCase(tuberia.getLongitud()) ||
                    "".equalsIgnoreCase(tuberia.getMaterial()) ||
                    "".equalsIgnoreCase(tuberia.getFotoTC())) {
                boton = false;
                i = tuberias.size();
            }
            i++;
        }
        btnIns1_grabar.setEnabled(boton);
        btnIns1_grabar.setClickable(boton);
    }

    /*private String setTipoFusionista(String tipoFusionista) {
        for (BaseInspectorOutRO genericBeanOutRO : listaTipoFusionista) {
            if (listaTipoFusionista.equalsIgnoreCase(genericBeanOutRO.getNombreInspector())) {
                return genericBeanOutRO.getNombreInspector();
            }
        }
        return null;
    }

    private int getTipoRFusionista(String tipoFusionista) {
        int i = 0;
        boolean entro = false;
        for (BaseInspectorOutRO genericBeanOutRO : listaTipoFusionista) {
            if (tipoFusionista.equalsIgnoreCase(genericBeanOutRO.getNombreInspector())) {
                entro = true;
                break;
            }
            i++;
        }
        if (entro) {
            return i + 1;
        } else {
            return 0;
        }
    }*/

    private int getFusionista(String fusionista) {
        int i = 0;
        boolean entro = false;
        for (BaseInspectorOutRO baseInspectorOutRO : listaTipoFusionista) {
            if (fusionista.equalsIgnoreCase(baseInspectorOutRO.getIdInspector().toString())) {
                entro = true;
                break;
            }
            i++;
        }
        if (entro) {
            return i + 1;
        } else {
            return 0;
        }
    }

    private String setTipoResultado(String tipoResultado) {
        for (GenericBeanOutRO genericBeanOutRO : listaTipoResultado) {
            if (tipoResultado.equalsIgnoreCase(genericBeanOutRO.getLabel())) {
                return genericBeanOutRO.getValue();
            }
        }
        return null;
    }

    private int getTipoResultado(String tipoResultado) {
        int i = 0;
        boolean entro = false;
        for (GenericBeanOutRO genericBeanOutRO : listaTipoResultado) {
            if (tipoResultado.equalsIgnoreCase(genericBeanOutRO.getValue())) {
                entro = true;
                break;
            }
            i++;
        }
        if (entro) {
            return i + 1;
        } else {
            return 0;
        }
    }

    private String setTipoMaterial(String tipoMaterial) {
        for (GenericBeanOutRO genericBeanOutRO : listaTipoMaterial) {
            if (tipoMaterial.equalsIgnoreCase(genericBeanOutRO.getLabel())) {
                return genericBeanOutRO.getValue();
            }
        }
        return null;
    }

    private int getTipoMaterial(String tipoMaterial) {
        int i = 0;
        boolean entro = false;
        for (GenericBeanOutRO genericBeanOutRO : listaTipoMaterial) {
            if (tipoMaterial.equalsIgnoreCase(genericBeanOutRO.getValue())) {
                entro = true;
                break;
            }
            i++;
        }
        if (entro) {
            return i + 1;
        } else {
            return 0;
        }
    }

    public interface Callback {
        void clicFoto(int position);
        void cambiarVista(int position);
    }

    private class ViewHolder {
        int ref;
        Spinner spiAco_resultado,spiAco_material;
        SearchableSpinner spiAco_fusionista;
        TextInputLayout ediAco_longitud,ediAco_diametro;
        ImageView imgAco_tc,icoAco_tc,icoAco_material,icoAco_longitud,icoAco_diametro,icoAco_resultado,icoAco_fusionista,addObservacion;
        TextView txtTub_tuberia;
        LinearLayout concluido,ll_observaciones;
        ListView listaObservacionAcometida;
        int check;
    }

    public void loadBitmap(String ruta, final ImageView imageView) {
        MostrarFotoTask mostrarFotoTask = new MostrarFotoTask(new MostrarFotoTask.OnMostrarFotoTaskCompleted() {
            @Override
            public void onMostrarFotoTaskCompleted(Bitmap bitmap) {
                if (bitmap != null) {
                    if (imageView != null) {
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(120, 120);
                        layoutParams.setMargins(10, 10, 10, 10);
                        imageView.setLayoutParams(layoutParams);
                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        imageView.setImageBitmap(bitmap);
                    }
                } else {
                    ((App) context.getApplicationContext()).showToast("Error al cargar la foto");
                }
            }
        });
        mostrarFotoTask.execute(ruta);
    }

}
