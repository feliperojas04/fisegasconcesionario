package pe.gob.osinergmin.gnr.cgn.data.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class MMotivoRechazoHabilitacionOutRO {

    @Id
    private Long idMotivoRechazoHabilitacion;
    private Integer idMotivoRechazoHabilitacionPadre;
    private String nombreMotivoRechazoHabilitacion;

    @Generated(hash = 2050298045)
    public MMotivoRechazoHabilitacionOutRO() {
    }

    public MMotivoRechazoHabilitacionOutRO(Long idMotivoRechazoHabilitacion) {
        this.idMotivoRechazoHabilitacion = idMotivoRechazoHabilitacion;
    }

    @Generated(hash = 1166680522)
    public MMotivoRechazoHabilitacionOutRO(Long idMotivoRechazoHabilitacion, Integer idMotivoRechazoHabilitacionPadre, String nombreMotivoRechazoHabilitacion) {
        this.idMotivoRechazoHabilitacion = idMotivoRechazoHabilitacion;
        this.idMotivoRechazoHabilitacionPadre = idMotivoRechazoHabilitacionPadre;
        this.nombreMotivoRechazoHabilitacion = nombreMotivoRechazoHabilitacion;
    }

    public Long getIdMotivoRechazoHabilitacion() {
        return idMotivoRechazoHabilitacion;
    }

    public void setIdMotivoRechazoHabilitacion(Long idMotivoRechazoHabilitacion) {
        this.idMotivoRechazoHabilitacion = idMotivoRechazoHabilitacion;
    }

    public Integer getIdMotivoRechazoHabilitacionPadre() {
        return idMotivoRechazoHabilitacionPadre;
    }

    public void setIdMotivoRechazoHabilitacionPadre(Integer idMotivoRechazoHabilitacionPadre) {
        this.idMotivoRechazoHabilitacionPadre = idMotivoRechazoHabilitacionPadre;
    }

    public String getNombreMotivoRechazoHabilitacion() {
        return nombreMotivoRechazoHabilitacion;
    }

    public void setNombreMotivoRechazoHabilitacion(String nombreMotivoRechazoHabilitacion) {
        this.nombreMotivoRechazoHabilitacion = nombreMotivoRechazoHabilitacion;
    }

}
