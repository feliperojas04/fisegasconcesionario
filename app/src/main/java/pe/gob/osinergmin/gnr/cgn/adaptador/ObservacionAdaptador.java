package pe.gob.osinergmin.gnr.cgn.adaptador;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.GenericBeanOutRO;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.data.models.ObservacionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Observacion;
import pe.gob.osinergmin.gnr.cgn.util.Util;

public class ObservacionAdaptador extends BaseAdapter {

    private CallBack mCallback;
    private Context context;
    private List<Observacion> observaciones;
    private ImageView icoActaNoInstalacion;
    private Button btnIns1_grabar;
    private ListView listaObservacionInstalacion;
    private Boolean validado = false;
    private ObservacionDao observacionDao;
    private List<GenericBeanOutRO> listatipoObse;

    public ObservacionAdaptador(Context context, List<Observacion> observaciones) {
        this.context = context;
        this.observaciones = observaciones;
    }

    public void setCallback(CallBack callback) {
        mCallback = callback;
    }

    public void setListatipoObse(List<GenericBeanOutRO> listatipoObse) {
        this.listatipoObse = listatipoObse;
        notifyDataSetChanged();
    }

    public void setObservacionDao(ObservacionDao observacionDao) {
        this.observacionDao = observacionDao;
    }

    public void setIcoActaNoInstalacion(ImageView icoActaNoInstalacion) {
        this.icoActaNoInstalacion = icoActaNoInstalacion;
    }

    public void setBtnVis1_grabar(Button btnIns1_grabar) {
        this.btnIns1_grabar = btnIns1_grabar;
    }

    public void setListaObservacionInstalacion(ListView listaObservacionInstalacion) {
        this.listaObservacionInstalacion = listaObservacionInstalacion;
    }

    public Boolean getValidado() {
        return this.validado;
    }

    @Override
    public int getCount() {
        if (observaciones.size() == 0) {
            btnIns1_grabar.setEnabled(false);
        }
        return this.observaciones.size();
    }

    @Override
    public Object getItem(int position) {
        return this.observaciones.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        final ViewHolder holder;
        if (convertView == null) {

            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = layoutInflater.inflate(R.layout.item_observacion, parent, false);

            holder = new ViewHolder();
            holder.icoVis4_observacion = rowView.findViewById(R.id.ico_observacion);
            holder.txtVis4_tipoObservacion = rowView.findViewById(R.id.txt_tipoObservacion);
            holder.ediVis4_descripcion = rowView.findViewById(R.id.edi_descripcion);
            holder.txtVis4_observacion = rowView.findViewById(R.id.txt_observacion);
            holder.icoVis4_check = rowView.findViewById(R.id.ico_check);

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        holder.ref = position;
        holder.check = 0;
        ArrayList<String> list = new ArrayList<>();
        list.add("Seleccione tipo");
        for (GenericBeanOutRO genericBeanOutRO : listatipoObse) {
            list.add(genericBeanOutRO.getLabel());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        holder.txtVis4_tipoObservacion.setAdapter(adapter);

        holder.icoVis4_observacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getCount() > 1) {
                    observacionDao.delete(observaciones.get(holder.ref));
                    observaciones.remove(holder.ref);
                    notifyDataSetChanged();
                    Util.setListViewHeightBasedOnChildren(listaObservacionInstalacion);
                } else {
                    ((App) context.getApplicationContext()).showToast("Se debe registrar una observación como mínimo");
                }
            }
        });

        String recurso = "Observación N° %1$s";
        String formateada = String.format(recurso, String.valueOf(position + 1));
        holder.txtVis4_observacion.setText(formateada);

        holder.txtVis4_tipoObservacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                holder.check++;
                if (holder.check > 1) {
                    if (position != 0) {
                        observaciones.get(holder.ref).setTipoObservacion(setTipoObservacion(parent.getSelectedItem().toString()));
                    } else {
                        observaciones.get(holder.ref).setTipoObservacion("0");
                    }
                    observacionDao.insertOrReplace(observaciones.get(holder.ref));
                    validar(holder);
                    check();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (!observaciones.get(holder.ref).getTipoObservacion().equalsIgnoreCase("")) {
            holder.txtVis4_tipoObservacion.setSelection(getTipoObservacion(observaciones.get(holder.ref).getTipoObservacion()));
        }

        holder.ediVis4_descripcion.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    if (s.length() < 1000) {
                        observaciones.get(holder.ref).setDescripcionObservacion(s.toString());
                    } else {
                        ((App) context.getApplicationContext()).showToast(String.format(context.getResources().getString(R.string.app_error_campo), 1000));
                    }
                } else {
                    observaciones.get(holder.ref).setDescripcionObservacion("");
                }
                observacionDao.insertOrReplace(observaciones.get(holder.ref));
                validar(holder);
                check();
            }
        });

        holder.ediVis4_descripcion.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    try {
                        holder.ediVis4_descripcion.getEditText().setText(observaciones.get(holder.ref).getDescripcionObservacion().trim());
                    } catch (IndexOutOfBoundsException e) {

                    }
                }
            }
        });

        holder.ediVis4_descripcion.getEditText().setText(observaciones.get(holder.ref).getDescripcionObservacion());

        System.out.println("Aqui en el adaptador de la observacio");
        notifyDataSetChanged();
        return rowView;
    }

    private void validar(ViewHolder holder) {
        if (observaciones.get(holder.ref).getTipoObservacion().equalsIgnoreCase("0") || observaciones.get(holder.ref).getDescripcionObservacion().equalsIgnoreCase("")) {
            holder.icoVis4_check.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            holder.icoVis4_check.setImageResource(R.mipmap.ic_done_black_36dp);
            btnIns1_grabar.setEnabled(true);
        }
    }

    private void check() {
        int i = 0;
        validado = true;
        while (i < observaciones.size()) {
            Observacion observacion = observaciones.get(i);
            if (observacion.getTipoObservacion().equalsIgnoreCase("0") || observacion.getDescripcionObservacion().equalsIgnoreCase("")) {
                validado = false;
                i = observaciones.size();
            }
            i++;
        }
        mCallback.onValidar();
    }

    private String setTipoObservacion(String tipoGaso) {
        for (GenericBeanOutRO genericBeanOutRO : listatipoObse) {
            if (tipoGaso.equalsIgnoreCase(genericBeanOutRO.getLabel())) {
                return genericBeanOutRO.getValue();
            }
        }
        return null;
    }

    private int getTipoObservacion(String tipoGaso) {
        int i = 0;
        boolean entro = false;
        for (GenericBeanOutRO genericBeanOutRO : listatipoObse) {
            if (tipoGaso.equalsIgnoreCase(genericBeanOutRO.getValue())) {
                entro = true;
                break;
            }
            i++;
        }
        if (entro) {
            return i + 1;
        } else {
            return 0;
        }
    }

    public interface CallBack {
        void onValidar();
    }

    private class ViewHolder {
        int ref;
        ImageView icoVis4_observacion;
        Spinner txtVis4_tipoObservacion;
        TextInputLayout ediVis4_descripcion;
        TextView txtVis4_observacion;
        ImageView icoVis4_check;
        int check;
    }

}
