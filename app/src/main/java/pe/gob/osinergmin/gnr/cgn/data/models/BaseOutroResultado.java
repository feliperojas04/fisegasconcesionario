package pe.gob.osinergmin.gnr.cgn.data.models;

public class BaseOutroResultado extends  BaseOutRO{
    private static final long serialVersionUID = 1L;
    private String value;
    private String label;

    public BaseOutroResultado() {
    }

    public BaseOutroResultado(String value, String label) {
        this.value = value;
        this.label = label;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}

