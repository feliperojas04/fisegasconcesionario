package pe.gob.osinergmin.gnr.cgn.util;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Location;
import android.media.ExifInterface;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import org.mapsforge.map.datastore.MapDataStore;
import org.mapsforge.map.reader.MapFile;
import org.mapsforge.map.reader.header.MapFileException;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import pe.gob.osinergmin.gnr.cgn.R;

public class Util {
    public static final String share_tag = "AppFisegasPreferences";
    public static final String activo = "";
    public static final String idacometida = "";
    private static final String RUTA = "/.CONCESIONARIO/";
    private static final String MAP_FILE = "peru.map";

    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public static Boolean getAlbumStorageDir(Context context) {
        //ContextWrapper contextWrapper = new ContextWrapper(context.getApplicationContext());
        //File file = contextWrapper.getExternalFilesDir(Environment.DIRECTORY_PICTURES + RUTA);
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), RUTA);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                Log.e("ALBUM", "Album no creado");
                return false;
            }
        }
        return true;
    }

    public static File getNuevaRutaFoto(String idSolicitud, Context context) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
        String timeStamp = dateFormat.format(new Date());
        //ContextWrapper contextWrapper = new ContextWrapper(context.getApplicationContext());
        //File mapDirectory = contextWrapper.getExternalFilesDir(Environment.DIRECTORY_PICTURES + RUTA);
        //File file = new File(mapDirectory + idSolicitud + "_" + timeStamp + ".JPEG");
        //return file;
        return new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), RUTA + idSolicitud + "_" + timeStamp + ".JPEG");
    }

    public static boolean verificarMap() {
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), RUTA + MAP_FILE);
        return file.exists();
    }

    public static void eliminarMap(Context context) {
        ContextWrapper contextWrapper = new ContextWrapper(context.getApplicationContext());
        //File mapDirectory = contextWrapper.getExternalFilesDir(Environment.DIRECTORY_PICTURES + RUTA);
        //File file = new File(mapDirectory,MAP_FILE);

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), RUTA + MAP_FILE);
        if (file.exists()) {
            file.delete();
        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public static Bitmap rotateBitmap(Bitmap bitmap, int orientation) {
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                return bitmap;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return bitmap;
        }
        try {
            Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
            return bmRotated;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getRuta() {
        return RUTA;
    }

    public static void enableSSL() {
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                }
        };

        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
        }
    }

    public static boolean validarScaleAndPrecision(String numero, int escala, int precision) {
        boolean valor;
        int punto = numero.indexOf(".");
        if (punto == -1) {
            if (numero.length() > precision - escala) {
                valor = false;
            } else {
                valor = true;
            }
        } else {
            int decimal = numero.length() - punto - 1;
            int entero = numero.length() - decimal - 1;
            if (entero > precision - escala) {
                valor = false;
            } else {
                if (decimal > escala) {
                    valor = false;
                } else {
                    valor = true;
                }
            }
        }
        return valor;
    }

    public static boolean isOkMapa(Context context) {
        ContextWrapper contextWrapper = new ContextWrapper(context.getApplicationContext());
        File mapDirectory = contextWrapper.getExternalFilesDir(Environment.DIRECTORY_PICTURES + RUTA);
        try {
            MapDataStore mapDataStore = new MapFile(new File(mapDirectory,MAP_FILE));
            //MapDataStore mapDataStore = new MapFile(new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), RUTA + MAP_FILE));
            return false;
        } catch (MapFileException e) {
            return true;
        }
    }

    public static String[] getPermisos(Context context, String... permisos) {
        if (context == null) {
            return null;
        }
        List<String> permissionList = new ArrayList<>();
        for (String permission : permisos) {
            if (ActivityCompat.checkSelfPermission(context, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                permissionList.add(permission);
            }
        }
        if (!permissionList.isEmpty()) {
            return permissionList.toArray(new String[permissionList.size()]);
        } else {
            return null;
        }
    }

    public static String onRequestPermissionsResult(@NonNull String[] permissions, @NonNull int[] grantResults) {
        int i = 0;
        String message = "No puede realizar la acción porque no se ha dado el permiso de ";
        boolean entro = false;
        for (String permiso : permissions) {
            switch (permiso) {
                case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        message += "Almacenamiento";
                        entro = true;
                    }
                    break;
                case Manifest.permission.CALL_PHONE:
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        message += "Llamadas";
                        entro = true;
                    }
                    break;
                case Manifest.permission.ACCESS_FINE_LOCATION:
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        message += "Ubicación";
                        entro = true;
                    }
                    break;
                default:
            }
            i++;
        }
        if (entro) {
            return message;
        } else {
            return "";
        }
    }
    public static final String KEY_REQUESTING_LOCATION_UPDATES = "requesting_location_updates";
    public static final String KEY_CLOSING_SERVICE = "closing_sesion_service";

    /**
     * Returns true if requesting location updates, otherwise returns false.
     *
     * @param context The {@link Context}.
     */
    public static boolean requestingLocationUpdates(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(KEY_REQUESTING_LOCATION_UPDATES, false);
    }

    /**
     * Stores the location updates state in SharedPreferences.
     * @param requestingLocationUpdates The location updates state.
     */
    public static void setRequestingLocationUpdates(Context context, boolean requestingLocationUpdates) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(KEY_REQUESTING_LOCATION_UPDATES, requestingLocationUpdates)
                .apply();
    }

    public static boolean requestingCloseSesion(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(KEY_CLOSING_SERVICE, false);
    }

    /**
     * Stores the location updates state in SharedPreferences.
     * @param cerrarSesionEnvios The closing sesion
     */
    public static void setRequestingCloseSesion(Context context, boolean cerrarSesionEnvios) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(KEY_CLOSING_SERVICE, cerrarSesionEnvios)
                .apply();
    }

    /**
     * Returns the {@code location} object as a human readable string.
     * @param location  The {@link Location}.
     */
    public static String getLocationText(Location location) {
        return location == null ? "Unknown location" :
                "(" + location.getLatitude() + ", " + location.getLongitude() + ")";
    }

    public static File generarRutaParaAcometidasOGabinetes(Integer posicion) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
        String timeStamp = dateFormat.format(new Date());
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), RUTA + timeStamp);
        if(!file.exists()) {
            file.mkdir();
        }
        return new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), RUTA + timeStamp + "/" + posicion + ".JPEG");
    }
}

