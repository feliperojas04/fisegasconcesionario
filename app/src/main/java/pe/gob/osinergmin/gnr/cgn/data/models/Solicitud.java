package pe.gob.osinergmin.gnr.cgn.data.models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "resultCode",
        "message",
        "errorCode",
        "idSolicitud",
        "codigoSolicitud",
        "fechaSolicitud",
        "documentoIdentificacionSolicitante",
        "nombreSolicitante",
        "direccionUbigeoPredio",
        "numeroSuministroPredio",
        "numeroContratoConcesionariaPredio",
        "idEstadoSolicitud",
        "nombreEstadoSolicitud",
        "idEmpresaInstaladora",
        "documentoIdentificacionEmpresaInstaladora",
        "nombreCortoEmpresaInstaladora",
        "nombreEmpresaInstaladora",
        "nombreDocumentoIdentificacionEmpresaInstaladora",
        "numeroRegistroGnEmpresaInstaladora",
        "telefonoSolicitante",
        "celularSolicitante",
        "emailSolicitante",
        "coordenadasPredio",
        "codigoUnidadPredialPredio",
        "codigoManzanaPredio",
        "nombreEsUsuarioFiseSolicitud",
        "nombreAplicaMecanismoPromocionSolicitud",
        "nombreTipoProyectoInstalacion",
        "nombreArchivoDocumentacionProyectoNoTipicoSolicitud",
        "nombreTipoInstalacion",
        "nombreMaterialInstalacion",
        "numeroPuntosInstalacion",
        "nombreEstratoPersona",
        "fechaAprobacionSolicitud",
        "fechaContratoConcesionariaPredio",
        "codigoInternoContratoConcesionaria",
        "numeroInstalacionConcesionaria",
        "nombreZonaAdjudicacion",
        "nombreEstadoTuberiaConexionAcometida",
        "nombreTipoGabinete",
        "nombreTipoAcometida",
        "nombreInstalacionAcometidaConcluidaPredio",
        "numeroTuberiaConexionProyectoInstalacion",
        "idProyectoInstalacion"
})
public class Solicitud {

    @JsonProperty("resultCode")
    private Object resultCode;
    @JsonProperty("message")
    private Object message;
    @JsonProperty("errorCode")
    private Object errorCode;
    @JsonProperty("idSolicitud")
    private int idSolicitud;
    @JsonProperty("codigoSolicitud")
    private int codigoSolicitud;
    @JsonProperty("fechaSolicitud")
    private String fechaSolicitud;
    @JsonProperty("documentoIdentificacionSolicitante")
    private String documentoIdentificacionSolicitante;
    @JsonProperty("nombreSolicitante")
    private String nombreSolicitante;
    @JsonProperty("direccionUbigeoPredio")
    private String direccionUbigeoPredio;
    @JsonProperty("numeroSuministroPredio")
    private String numeroSuministroPredio;
    @JsonProperty("numeroContratoConcesionariaPredio")
    private String numeroContratoConcesionariaPredio;
    @JsonProperty("idEstadoSolicitud")
    private int idEstadoSolicitud;
    @JsonProperty("nombreEstadoSolicitud")
    private String nombreEstadoSolicitud;
    @JsonProperty("idEmpresaInstaladora")
    private int idEmpresaInstaladora;
    @JsonProperty("documentoIdentificacionEmpresaInstaladora")
    private String documentoIdentificacionEmpresaInstaladora;
    @JsonProperty("nombreCortoEmpresaInstaladora")
    private String nombreCortoEmpresaInstaladora;
    @JsonProperty("nombreEmpresaInstaladora")
    private String nombreEmpresaInstaladora;
    @JsonProperty("nombreDocumentoIdentificacionEmpresaInstaladora")
    private String nombreDocumentoIdentificacionEmpresaInstaladora;
    @JsonProperty("numeroRegistroGnEmpresaInstaladora")
    private String numeroRegistroGnEmpresaInstaladora;
    @JsonProperty("telefonoSolicitante")
    private String telefonoSolicitante;
    @JsonProperty("celularSolicitante")
    private String celularSolicitante;
    @JsonProperty("emailSolicitante")
    private String emailSolicitante;
    @JsonProperty("coordenadasPredio")
    private String coordenadasPredio;
    @JsonProperty("codigoUnidadPredialPredio")
    private String codigoUnidadPredialPredio;
    @JsonProperty("codigoManzanaPredio")
    private String codigoManzanaPredio;
    @JsonProperty("nombreEsUsuarioFiseSolicitud")
    private String nombreEsUsuarioFiseSolicitud;
    @JsonProperty("nombreAplicaMecanismoPromocionSolicitud")
    private String nombreAplicaMecanismoPromocionSolicitud;
    @JsonProperty("nombreTipoProyectoInstalacion")
    private String nombreTipoProyectoInstalacion;
    @JsonProperty("nombreArchivoDocumentacionProyectoNoTipicoSolicitud")
    private Object nombreArchivoDocumentacionProyectoNoTipicoSolicitud;
    @JsonProperty("nombreTipoInstalacion")
    private String nombreTipoInstalacion;
    @JsonProperty("nombreMaterialInstalacion")
    private String nombreMaterialInstalacion;
    @JsonProperty("numeroPuntosInstalacion")
    private int numeroPuntosInstalacion;
    @JsonProperty("nombreEstratoPersona")
    private String nombreEstratoPersona;
    @JsonProperty("fechaAprobacionSolicitud")
    private Object fechaAprobacionSolicitud;
    @JsonProperty("fechaContratoConcesionariaPredio")
    private String fechaContratoConcesionariaPredio;
    @JsonProperty("codigoInternoContratoConcesionaria")
    private String codigoInternoContratoConcesionaria;
    @JsonProperty("numeroInstalacionConcesionaria")
    private String numeroInstalacionConcesionaria;
    @JsonProperty("nombreZonaAdjudicacion")
    private String nombreZonaAdjudicacion;
    @JsonProperty("nombreEstadoTuberiaConexionAcometida")
    private String nombreEstadoTuberiaConexionAcometida;
    @JsonProperty("nombreTipoGabinete")
    private String nombreTipoGabinete;
    @JsonProperty("nombreTipoAcometida")
    private String nombreTipoAcometida;
    @JsonProperty("nombreInstalacionAcometidaConcluidaPredio")
    private Object nombreInstalacionAcometidaConcluidaPredio;
    @JsonProperty("numeroTuberiaConexionProyectoInstalacion")
    private int numeroTuberiaConexionProyectoInstalacion;
    @JsonProperty("idProyectoInstalacion")
    private int idProyectoInstalacion;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("resultCode")
    public Object getResultCode() {
        return resultCode;
    }

    @JsonProperty("resultCode")
    public void setResultCode(Object resultCode) {
        this.resultCode = resultCode;
    }

    @JsonProperty("message")
    public Object getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(Object message) {
        this.message = message;
    }

    @JsonProperty("errorCode")
    public Object getErrorCode() {
        return errorCode;
    }

    @JsonProperty("errorCode")
    public void setErrorCode(Object errorCode) {
        this.errorCode = errorCode;
    }

    @JsonProperty("idSolicitud")
    public int getIdSolicitud() {
        return idSolicitud;
    }

    @JsonProperty("idSolicitud")
    public void setIdSolicitud(int idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    @JsonProperty("codigoSolicitud")
    public int getCodigoSolicitud() {
        return codigoSolicitud;
    }

    @JsonProperty("codigoSolicitud")
    public void setCodigoSolicitud(int codigoSolicitud) {
        this.codigoSolicitud = codigoSolicitud;
    }

    @JsonProperty("fechaSolicitud")
    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    @JsonProperty("fechaSolicitud")
    public void setFechaSolicitud(String fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    @JsonProperty("documentoIdentificacionSolicitante")
    public String getDocumentoIdentificacionSolicitante() {
        return documentoIdentificacionSolicitante;
    }

    @JsonProperty("documentoIdentificacionSolicitante")
    public void setDocumentoIdentificacionSolicitante(String documentoIdentificacionSolicitante) {
        this.documentoIdentificacionSolicitante = documentoIdentificacionSolicitante;
    }

    @JsonProperty("nombreSolicitante")
    public String getNombreSolicitante() {
        return nombreSolicitante;
    }

    @JsonProperty("nombreSolicitante")
    public void setNombreSolicitante(String nombreSolicitante) {
        this.nombreSolicitante = nombreSolicitante;
    }

    @JsonProperty("direccionUbigeoPredio")
    public String getDireccionUbigeoPredio() {
        return direccionUbigeoPredio;
    }

    @JsonProperty("direccionUbigeoPredio")
    public void setDireccionUbigeoPredio(String direccionUbigeoPredio) {
        this.direccionUbigeoPredio = direccionUbigeoPredio;
    }

    @JsonProperty("numeroSuministroPredio")
    public String getNumeroSuministroPredio() {
        return numeroSuministroPredio;
    }

    @JsonProperty("numeroSuministroPredio")
    public void setNumeroSuministroPredio(String numeroSuministroPredio) {
        this.numeroSuministroPredio = numeroSuministroPredio;
    }

    @JsonProperty("numeroContratoConcesionariaPredio")
    public String getNumeroContratoConcesionariaPredio() {
        return numeroContratoConcesionariaPredio;
    }

    @JsonProperty("numeroContratoConcesionariaPredio")
    public void setNumeroContratoConcesionariaPredio(String numeroContratoConcesionariaPredio) {
        this.numeroContratoConcesionariaPredio = numeroContratoConcesionariaPredio;
    }

    @JsonProperty("idEstadoSolicitud")
    public int getIdEstadoSolicitud() {
        return idEstadoSolicitud;
    }

    @JsonProperty("idEstadoSolicitud")
    public void setIdEstadoSolicitud(int idEstadoSolicitud) {
        this.idEstadoSolicitud = idEstadoSolicitud;
    }

    @JsonProperty("nombreEstadoSolicitud")
    public String getNombreEstadoSolicitud() {
        return nombreEstadoSolicitud;
    }

    @JsonProperty("nombreEstadoSolicitud")
    public void setNombreEstadoSolicitud(String nombreEstadoSolicitud) {
        this.nombreEstadoSolicitud = nombreEstadoSolicitud;
    }

    @JsonProperty("idEmpresaInstaladora")
    public int getIdEmpresaInstaladora() {
        return idEmpresaInstaladora;
    }

    @JsonProperty("idEmpresaInstaladora")
    public void setIdEmpresaInstaladora(int idEmpresaInstaladora) {
        this.idEmpresaInstaladora = idEmpresaInstaladora;
    }

    @JsonProperty("documentoIdentificacionEmpresaInstaladora")
    public String getDocumentoIdentificacionEmpresaInstaladora() {
        return documentoIdentificacionEmpresaInstaladora;
    }

    @JsonProperty("documentoIdentificacionEmpresaInstaladora")
    public void setDocumentoIdentificacionEmpresaInstaladora(String documentoIdentificacionEmpresaInstaladora) {
        this.documentoIdentificacionEmpresaInstaladora = documentoIdentificacionEmpresaInstaladora;
    }

    @JsonProperty("nombreCortoEmpresaInstaladora")
    public String getNombreCortoEmpresaInstaladora() {
        return nombreCortoEmpresaInstaladora;
    }

    @JsonProperty("nombreCortoEmpresaInstaladora")
    public void setNombreCortoEmpresaInstaladora(String nombreCortoEmpresaInstaladora) {
        this.nombreCortoEmpresaInstaladora = nombreCortoEmpresaInstaladora;
    }

    @JsonProperty("nombreEmpresaInstaladora")
    public String getNombreEmpresaInstaladora() {
        return nombreEmpresaInstaladora;
    }

    @JsonProperty("nombreEmpresaInstaladora")
    public void setNombreEmpresaInstaladora(String nombreEmpresaInstaladora) {
        this.nombreEmpresaInstaladora = nombreEmpresaInstaladora;
    }

    @JsonProperty("nombreDocumentoIdentificacionEmpresaInstaladora")
    public String getNombreDocumentoIdentificacionEmpresaInstaladora() {
        return nombreDocumentoIdentificacionEmpresaInstaladora;
    }

    @JsonProperty("nombreDocumentoIdentificacionEmpresaInstaladora")
    public void setNombreDocumentoIdentificacionEmpresaInstaladora(String nombreDocumentoIdentificacionEmpresaInstaladora) {
        this.nombreDocumentoIdentificacionEmpresaInstaladora = nombreDocumentoIdentificacionEmpresaInstaladora;
    }

    @JsonProperty("numeroRegistroGnEmpresaInstaladora")
    public String getNumeroRegistroGnEmpresaInstaladora() {
        return numeroRegistroGnEmpresaInstaladora;
    }

    @JsonProperty("numeroRegistroGnEmpresaInstaladora")
    public void setNumeroRegistroGnEmpresaInstaladora(String numeroRegistroGnEmpresaInstaladora) {
        this.numeroRegistroGnEmpresaInstaladora = numeroRegistroGnEmpresaInstaladora;
    }

    @JsonProperty("telefonoSolicitante")
    public String getTelefonoSolicitante() {
        return telefonoSolicitante;
    }

    @JsonProperty("telefonoSolicitante")
    public void setTelefonoSolicitante(String telefonoSolicitante) {
        this.telefonoSolicitante = telefonoSolicitante;
    }

    @JsonProperty("celularSolicitante")
    public String getCelularSolicitante() {
        return celularSolicitante;
    }

    @JsonProperty("celularSolicitante")
    public void setCelularSolicitante(String celularSolicitante) {
        this.celularSolicitante = celularSolicitante;
    }

    @JsonProperty("emailSolicitante")
    public String getEmailSolicitante() {
        return emailSolicitante;
    }

    @JsonProperty("emailSolicitante")
    public void setEmailSolicitante(String emailSolicitante) {
        this.emailSolicitante = emailSolicitante;
    }

    @JsonProperty("coordenadasPredio")
    public String getCoordenadasPredio() {
        return coordenadasPredio;
    }

    @JsonProperty("coordenadasPredio")
    public void setCoordenadasPredio(String coordenadasPredio) {
        this.coordenadasPredio = coordenadasPredio;
    }

    @JsonProperty("codigoUnidadPredialPredio")
    public String getCodigoUnidadPredialPredio() {
        return codigoUnidadPredialPredio;
    }

    @JsonProperty("codigoUnidadPredialPredio")
    public void setCodigoUnidadPredialPredio(String codigoUnidadPredialPredio) {
        this.codigoUnidadPredialPredio = codigoUnidadPredialPredio;
    }

    @JsonProperty("codigoManzanaPredio")
    public String getCodigoManzanaPredio() {
        return codigoManzanaPredio;
    }

    @JsonProperty("codigoManzanaPredio")
    public void setCodigoManzanaPredio(String codigoManzanaPredio) {
        this.codigoManzanaPredio = codigoManzanaPredio;
    }

    @JsonProperty("nombreEsUsuarioFiseSolicitud")
    public String getNombreEsUsuarioFiseSolicitud() {
        return nombreEsUsuarioFiseSolicitud;
    }

    @JsonProperty("nombreEsUsuarioFiseSolicitud")
    public void setNombreEsUsuarioFiseSolicitud(String nombreEsUsuarioFiseSolicitud) {
        this.nombreEsUsuarioFiseSolicitud = nombreEsUsuarioFiseSolicitud;
    }

    @JsonProperty("nombreAplicaMecanismoPromocionSolicitud")
    public String getNombreAplicaMecanismoPromocionSolicitud() {
        return nombreAplicaMecanismoPromocionSolicitud;
    }

    @JsonProperty("nombreAplicaMecanismoPromocionSolicitud")
    public void setNombreAplicaMecanismoPromocionSolicitud(String nombreAplicaMecanismoPromocionSolicitud) {
        this.nombreAplicaMecanismoPromocionSolicitud = nombreAplicaMecanismoPromocionSolicitud;
    }

    @JsonProperty("nombreTipoProyectoInstalacion")
    public String getNombreTipoProyectoInstalacion() {
        return nombreTipoProyectoInstalacion;
    }

    @JsonProperty("nombreTipoProyectoInstalacion")
    public void setNombreTipoProyectoInstalacion(String nombreTipoProyectoInstalacion) {
        this.nombreTipoProyectoInstalacion = nombreTipoProyectoInstalacion;
    }

    @JsonProperty("nombreArchivoDocumentacionProyectoNoTipicoSolicitud")
    public Object getNombreArchivoDocumentacionProyectoNoTipicoSolicitud() {
        return nombreArchivoDocumentacionProyectoNoTipicoSolicitud;
    }

    @JsonProperty("nombreArchivoDocumentacionProyectoNoTipicoSolicitud")
    public void setNombreArchivoDocumentacionProyectoNoTipicoSolicitud(Object nombreArchivoDocumentacionProyectoNoTipicoSolicitud) {
        this.nombreArchivoDocumentacionProyectoNoTipicoSolicitud = nombreArchivoDocumentacionProyectoNoTipicoSolicitud;
    }

    @JsonProperty("nombreTipoInstalacion")
    public String getNombreTipoInstalacion() {
        return nombreTipoInstalacion;
    }

    @JsonProperty("nombreTipoInstalacion")
    public void setNombreTipoInstalacion(String nombreTipoInstalacion) {
        this.nombreTipoInstalacion = nombreTipoInstalacion;
    }

    @JsonProperty("nombreMaterialInstalacion")
    public String getNombreMaterialInstalacion() {
        return nombreMaterialInstalacion;
    }

    @JsonProperty("nombreMaterialInstalacion")
    public void setNombreMaterialInstalacion(String nombreMaterialInstalacion) {
        this.nombreMaterialInstalacion = nombreMaterialInstalacion;
    }

    @JsonProperty("numeroPuntosInstalacion")
    public int getNumeroPuntosInstalacion() {
        return numeroPuntosInstalacion;
    }

    @JsonProperty("numeroPuntosInstalacion")
    public void setNumeroPuntosInstalacion(int numeroPuntosInstalacion) {
        this.numeroPuntosInstalacion = numeroPuntosInstalacion;
    }

    @JsonProperty("nombreEstratoPersona")
    public String getNombreEstratoPersona() {
        return nombreEstratoPersona;
    }

    @JsonProperty("nombreEstratoPersona")
    public void setNombreEstratoPersona(String nombreEstratoPersona) {
        this.nombreEstratoPersona = nombreEstratoPersona;
    }

    @JsonProperty("fechaAprobacionSolicitud")
    public Object getFechaAprobacionSolicitud() {
        return fechaAprobacionSolicitud;
    }

    @JsonProperty("fechaAprobacionSolicitud")
    public void setFechaAprobacionSolicitud(Object fechaAprobacionSolicitud) {
        this.fechaAprobacionSolicitud = fechaAprobacionSolicitud;
    }

    @JsonProperty("fechaContratoConcesionariaPredio")
    public String getFechaContratoConcesionariaPredio() {
        return fechaContratoConcesionariaPredio;
    }

    @JsonProperty("fechaContratoConcesionariaPredio")
    public void setFechaContratoConcesionariaPredio(String fechaContratoConcesionariaPredio) {
        this.fechaContratoConcesionariaPredio = fechaContratoConcesionariaPredio;
    }

    @JsonProperty("codigoInternoContratoConcesionaria")
    public String getCodigoInternoContratoConcesionaria() {
        return codigoInternoContratoConcesionaria;
    }

    @JsonProperty("codigoInternoContratoConcesionaria")
    public void setCodigoInternoContratoConcesionaria(String codigoInternoContratoConcesionaria) {
        this.codigoInternoContratoConcesionaria = codigoInternoContratoConcesionaria;
    }

    @JsonProperty("numeroInstalacionConcesionaria")
    public String getNumeroInstalacionConcesionaria() {
        return numeroInstalacionConcesionaria;
    }

    @JsonProperty("numeroInstalacionConcesionaria")
    public void setNumeroInstalacionConcesionaria(String numeroInstalacionConcesionaria) {
        this.numeroInstalacionConcesionaria = numeroInstalacionConcesionaria;
    }

    @JsonProperty("nombreZonaAdjudicacion")
    public String getNombreZonaAdjudicacion() {
        return nombreZonaAdjudicacion;
    }

    @JsonProperty("nombreZonaAdjudicacion")
    public void setNombreZonaAdjudicacion(String nombreZonaAdjudicacion) {
        this.nombreZonaAdjudicacion = nombreZonaAdjudicacion;
    }

    @JsonProperty("nombreEstadoTuberiaConexionAcometida")
    public String getNombreEstadoTuberiaConexionAcometida() {
        return nombreEstadoTuberiaConexionAcometida;
    }

    @JsonProperty("nombreEstadoTuberiaConexionAcometida")
    public void setNombreEstadoTuberiaConexionAcometida(String nombreEstadoTuberiaConexionAcometida) {
        this.nombreEstadoTuberiaConexionAcometida = nombreEstadoTuberiaConexionAcometida;
    }

    @JsonProperty("nombreTipoGabinete")
    public String getNombreTipoGabinete() {
        return nombreTipoGabinete;
    }

    @JsonProperty("nombreTipoGabinete")
    public void setNombreTipoGabinete(String nombreTipoGabinete) {
        this.nombreTipoGabinete = nombreTipoGabinete;
    }

    @JsonProperty("nombreTipoAcometida")
    public String getNombreTipoAcometida() {
        return nombreTipoAcometida;
    }

    @JsonProperty("nombreTipoAcometida")
    public void setNombreTipoAcometida(String nombreTipoAcometida) {
        this.nombreTipoAcometida = nombreTipoAcometida;
    }

    @JsonProperty("nombreInstalacionAcometidaConcluidaPredio")
    public Object getNombreInstalacionAcometidaConcluidaPredio() {
        return nombreInstalacionAcometidaConcluidaPredio;
    }

    @JsonProperty("nombreInstalacionAcometidaConcluidaPredio")
    public void setNombreInstalacionAcometidaConcluidaPredio(Object nombreInstalacionAcometidaConcluidaPredio) {
        this.nombreInstalacionAcometidaConcluidaPredio = nombreInstalacionAcometidaConcluidaPredio;
    }

    @JsonProperty("numeroTuberiaConexionProyectoInstalacion")
    public int getNumeroTuberiaConexionProyectoInstalacion() {
        return numeroTuberiaConexionProyectoInstalacion;
    }

    @JsonProperty("numeroTuberiaConexionProyectoInstalacion")
    public void setNumeroTuberiaConexionProyectoInstalacion(int numeroTuberiaConexionProyectoInstalacion) {
        this.numeroTuberiaConexionProyectoInstalacion = numeroTuberiaConexionProyectoInstalacion;
    }

    @JsonProperty("idProyectoInstalacion")
    public int getIdProyectoInstalacion() {
        return idProyectoInstalacion;
    }

    @JsonProperty("idProyectoInstalacion")
    public void setIdProyectoInstalacion(int idProyectoInstalacion) {
        this.idProyectoInstalacion = idProyectoInstalacion;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}