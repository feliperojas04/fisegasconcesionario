package pe.gob.osinergmin.gnr.cgn.actividad;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.BaseInspectorOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.GabineteProyectoInstalacionOutRo;
import gob.osinergmin.gnr.domain.dto.rest.out.GenericBeanOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.InstalacionAcometidaOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListBaseInspectorOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListGabineteProyectoInstalacionOutRo;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListGenericBeanOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.adaptador.AcometidaNewAdaptador;
import pe.gob.osinergmin.gnr.cgn.adaptador.ObservacionAdaptador;
import pe.gob.osinergmin.gnr.cgn.adaptador.PuntoAdaptador;
import pe.gob.osinergmin.gnr.cgn.adaptador.TuberiaAdaptador;
import pe.gob.osinergmin.gnr.cgn.adaptador.TubyTcAdaptador;
import pe.gob.osinergmin.gnr.cgn.data.models.Acometida;
import pe.gob.osinergmin.gnr.cgn.data.models.AcometidaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.AcometidaNueva;
import pe.gob.osinergmin.gnr.cgn.data.models.AcometidaNuevaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.BaseOutroResultado;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.Fusionista;
import pe.gob.osinergmin.gnr.cgn.data.models.FusionistaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.MBaseInspectorOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MInstalacionAcometidaOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MInstalacionAcometidaOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MaterialInstalacion;
import pe.gob.osinergmin.gnr.cgn.data.models.MaterialInstalacionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Observacion;
import pe.gob.osinergmin.gnr.cgn.data.models.ObservacionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Precision;
import pe.gob.osinergmin.gnr.cgn.data.models.PrecisionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.ResponseInstalacionAcometida;
import pe.gob.osinergmin.gnr.cgn.data.models.TipoGabinete;
import pe.gob.osinergmin.gnr.cgn.data.models.TipoGabineteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.TipoPendiente;
import pe.gob.osinergmin.gnr.cgn.data.models.TipoPendienteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.TipoRechazada;
import pe.gob.osinergmin.gnr.cgn.data.models.TipoRechazadaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.TipoResultado;
import pe.gob.osinergmin.gnr.cgn.data.models.TipoResultadoDao;
import pe.gob.osinergmin.gnr.cgn.data.models.TiposResultadoAcometidaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Tuberias;
import pe.gob.osinergmin.gnr.cgn.data.models.TuberiasDao;
import pe.gob.osinergmin.gnr.cgn.task.GrabarAcometida;
import pe.gob.osinergmin.gnr.cgn.task.GrabarGabinete;
import pe.gob.osinergmin.gnr.cgn.task.GrabarTuberias;
import pe.gob.osinergmin.gnr.cgn.task.TipoFusionista;
import pe.gob.osinergmin.gnr.cgn.task.TipoGabineteAcometida;
import pe.gob.osinergmin.gnr.cgn.task.TipoGabineteAcometida2;
import pe.gob.osinergmin.gnr.cgn.task.TipoMaterialInstalacion;
import pe.gob.osinergmin.gnr.cgn.task.TipoObservacionPendiente;
import pe.gob.osinergmin.gnr.cgn.task.TipoObservacionRechazada;
import pe.gob.osinergmin.gnr.cgn.task.TipoResultadoAcometida;
import pe.gob.osinergmin.gnr.cgn.util.DownTimer;
import pe.gob.osinergmin.gnr.cgn.util.FormatoFecha;
import pe.gob.osinergmin.gnr.cgn.util.MostrarFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.NetworkUtil;
import pe.gob.osinergmin.gnr.cgn.util.Parse;
import pe.gob.osinergmin.gnr.cgn.util.ReducirFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.SearchableSpinner;
import pe.gob.osinergmin.gnr.cgn.util.Util;

import static gob.osinergmin.gnr.util.Constantes.RESULTADO_INSTALACION_ACOMETIDA_CONCLUIDA;
import static gob.osinergmin.gnr.util.Constantes.RESULTADO_INSTALACION_ACOMETIDA_PENDIENTE;
import static gob.osinergmin.gnr.util.Constantes.RESULTADO_INSTALACION_ACOMETIDA_RECHAZADA;
import static gob.osinergmin.gnr.util.Constantes.TIPO_INSTALACION_ACOMETIDA_ACOMETIDA;
import static gob.osinergmin.gnr.util.Constantes.TIPO_INSTALACION_ACOMETIDA_TC;

public class Acometida2Activity extends BaseActivity implements TubyTcAdaptador.Callback, ObservacionAdaptador.CallBack, AcometidaNewAdaptador.Callback{

    private Acometida acometida;
    private SharedPreferences pref;
    private Config config;
    String NumTub,tipo,idinstala;
    int numtabsint,idinstalacion;
    private TableLayout tablaTuberia;
    private TextView txtTub_tuberia;
    private TextView txtDet_DocumentoPropietarioView;
    private TextView txtDet_nombrePropietarioView;
    private TextView txtDet_PredioView;
    private TextView txtDetInstalacionView;
    private TextView txtDet_numeroIntHabilitacionView;
    private TextView txtDet_numeroSolicitudView;
    private TextView txtDet_numeroContratoView;
    private TextView txtDet_fechaSolicitudView;
    private TextView txtDet_fechaAprobacionView;
    private TextView txtDet_tipoProyectoView;
    private TextView txtDet_tipoInstalacionView;
    private TextView txtDet_numeroPuntosInstaacionView;
    private Toolbar toolbar;
    private MenuItem menuMas;
    private MenuItem menuMenos;
    private ImageView icoAco_fusionista, icoAco_resultado, icoAco_diametro, icoAco_longitud, icoAco_material,icoAco_acta, icoAco_tc, icoAco_gabinete, imgAco_acta, imgAco_tc, imgAco_gabinete, icoAco_tipogabinete,icoAco_actagabinate;
    /*PAPU 2211 - 0*/
    private MInstalacionAcometidaOutRODao mInstalacionAcometidaOutRODao;
    private AcometidaDao acometidaDao;
    private LinearLayout concluido, ll_observaciones, llAcometida, llTc, lynuevo,lyantiguo,lygabinetes,ll_observacionesgabinete;
    private List<Observacion> observaciones;
    private List<BaseInspectorOutRO> listaTipoFusionista = new ArrayList<>();
    private List<GenericBeanOutRO> listaTipoResultado = new ArrayList<>();
    private List<GenericBeanOutRO> listaTipoGabinete = new ArrayList<>();/*PAPU 2011 - 0*/
    private List<GenericBeanOutRO> listaTipoMaterial = new ArrayList<>();
    private List<GenericBeanOutRO> listatipoObse = new ArrayList<>();
    private List<GenericBeanOutRO> listatipoObsePendiente = new ArrayList<>();
    private List<GenericBeanOutRO> listatipoObseRechazado = new ArrayList<>();
    private Button btnAco_grabar;
    private ListView listaObservacionAcometida,listaTuberias,listaObservacionAcometidagabinete,recy_acometidas;
    private Spinner spiAco_resultado, spiAco_material /*PAPU 2211 - 0*/ ,spiAco_tipogabinete;
    /*PAPU 2211 - 1*/
    private TextInputLayout ediAco_diametro, ediAco_longitud;
    private ObservacionAdaptador observacionAdaptador;
    private TuberiaAdaptador tuberiaAdaptador;
    private SearchableSpinner spiAco_fusionista;
    private String tmpRuta;
    private DownTimer countDownTimer;
    private ObservacionDao observacionDao;
    private FusionistaDao fusionistaDao;
    private TipoResultadoDao tipoResultadoDao;
    private TipoPendienteDao tipoPendienteDao;
    private TipoRechazadaDao tipoRechazadaDao;
    private MBaseInspectorOutRODao mBaseInspectorOutRODao;
    private TiposResultadoAcometidaDao tiposResultadoAcometidaDao;
    private MaterialInstalacionDao materialInstalacionDao;
    private MParametroOutRODao mParametroOutRODao;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private PrecisionDao precisionDao;
    private TubyTcAdaptador tubyTcAdaptador;
    private AcometidaNewAdaptador acometidaNewAdaptador;
    private List<Tuberias> tuberias = new ArrayList<>();
    private List<AcometidaNueva> acometidaNuevas = new ArrayList<>();
    /*PAPU 2211 - 0*/
    private TipoGabineteDao tipoGabineteDao;
    private TextView txtAco_tipogabinete;

    private List<BaseInspectorOutRO> listaTipoFusionista1 = new ArrayList<>();
    private List<GenericBeanOutRO> listaTipoResultado1 = new ArrayList<>();
    private List<GabineteProyectoInstalacionOutRo> listGabineteProyectoInstalacion = new ArrayList<>();
    private List<GenericBeanOutRO> listaTipoGabinete1 = new ArrayList<>();
    private List<GenericBeanOutRO> listaTipoMaterial1 = new ArrayList<>();
    private int position;

    private LinearLayout llAcomDatosG;
    private TextView txtTub_tuberiagabinete;
    private ImageView icoAco_fusionistagabinete;
    private TextView txtAco_fusionista;
    private SearchableSpinner spiAco_fusionistagabinete;
    private ImageView icoAco_resultadogabinete;
    private TextView txtAco_resultado;
    private Spinner spiAco_resultadogabinete;
    private LinearLayout llAcomUbicacion;
    private TextView txtUbiAcom;
    private ImageView icoAco_actagabinete;
    private TextView txtAco_acta;
    private ImageView imgAco_actagabinete;
    private List<BaseOutroResultado> listaTipoResultado12 = new ArrayList<>();
    TuberiasDao tuberiasDao;
    AcometidaNuevaDao acometidaNuevaDao;
    private boolean ban = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acometida2);

        countDownTimer = DownTimer.getInstance();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        acometidaDao = ((App) getApplication()).getDaoSession().getAcometidaDao();
        mParametroOutRODao = ((App) getApplication()).getDaoSession().getMParametroOutRODao();
        materialInstalacionDao = ((App) getApplication()).getDaoSession().getMaterialInstalacionDao();
        precisionDao = ((App) getApplication()).getDaoSession().getPrecisionDao();
        mInstalacionAcometidaOutRODao = ((App) getApplication()).getDaoSession().getMInstalacionAcometidaOutRODao();
        observacionDao = ((App) getApplication()).getDaoSession().getObservacionDao();
        tipoPendienteDao = ((App) getApplication()).getDaoSession().getTipoPendienteDao();
        tipoRechazadaDao = ((App) getApplication()).getDaoSession().getTipoRechazadaDao();
        fusionistaDao = ((App) getApplication()).getDaoSession().getFusionistaDao();
        tipoResultadoDao = ((App) getApplication()).getDaoSession().getTipoResultadoDao();
        tipoGabineteDao = ((App) getApplication()).getDaoSession().getTipoGabineteDao();
        tuberiasDao = ((App) getApplicationContext()).getDaoSession().getTuberiasDao();
        acometidaNuevaDao = ((App) getApplicationContext()).getDaoSession().getAcometidaNuevaDao();
        pref = getSharedPreferences(Util.share_tag, Context.MODE_PRIVATE);
        toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_36dp);


        //SharedPreferences.Editor editor = pref.edit();
        //editor.putString(Util.activo,"false");
        //editor.apply();

        acometida = getIntent().getExtras().getParcelable("ACOMETIDA");
        config = getIntent().getExtras().getParcelable("CONFIG");
        NumTub = getIntent().getStringExtra("NumTub");
        tipo = getIntent().getStringExtra("tipo");
        idinstala = getIntent().getStringExtra("idInstalacion");
        //idinstalacion = Integer.parseInt(idinstala);
        System.out.println("NumTub: " + NumTub);
        System.out.println("idInstalacion: " + idinstala);
        lynuevo = findViewById(R.id.lynuevo);
        lyantiguo = findViewById(R.id.lyantiguo);
        lygabinetes = findViewById(R.id.lygabinetes);
        config.setIdProyectoInstalacion(idinstala);
        tablaTuberia = findViewById(R.id.tablaTuberia);
        txtTub_tuberia = findViewById(R.id.txtTub_tuberia);
        txtDet_DocumentoPropietarioView = findViewById(R.id.txtDet_DocumentoPropietarioView);
        txtDet_nombrePropietarioView = findViewById(R.id.txtDet_nombrePropietarioView);
        txtDet_PredioView = findViewById(R.id.txtDet_PredioView);
        txtDetInstalacionView = findViewById(R.id.txtDetInstalacionView);
        txtDet_numeroIntHabilitacionView = findViewById(R.id.txtDet_numeroIntHabilitacionView);
        txtDet_numeroSolicitudView = findViewById(R.id.txtDet_numeroSolicitudView);
        txtDet_numeroContratoView = findViewById(R.id.txtDet_numeroContratoView);
        txtDet_fechaSolicitudView = findViewById(R.id.txtDet_fechaSolicitudView);
        txtDet_fechaAprobacionView = findViewById(R.id.txtDet_fechaAprobacionView);
        txtDet_tipoProyectoView = findViewById(R.id.txtDet_tipoProyectoView);
        txtDet_tipoInstalacionView = findViewById(R.id.txtDet_tipoInstalacionView);
        txtDet_numeroPuntosInstaacionView = findViewById(R.id.txtDet_numeroPuntosInstaacionView);
        spiAco_fusionista = findViewById(R.id.spiAco_fusionista);
        icoAco_fusionista = findViewById(R.id.icoAco_fusionista);
        spiAco_resultado = findViewById(R.id.spiAco_resultado);
        icoAco_resultado = findViewById(R.id.icoAco_resultado);
        icoAco_longitud = findViewById(R.id.icoAco_longitud);
        icoAco_material = findViewById(R.id.icoAco_material);
        icoAco_gabinete = findViewById(R.id.icoAco_gabinete);
        icoAco_acta = findViewById(R.id.icoAco_acta);
        icoAco_tc = findViewById(R.id.icoAco_tc);
        btnAco_grabar = findViewById(R.id.btnAco_grabar);
        ediAco_diametro = findViewById(R.id.ediAco_diametro);
        listaObservacionAcometida = findViewById(R.id.listaObservacionAcometida);
        listaObservacionAcometidagabinete = findViewById(R.id.listaObservacionAcometidagabinete);
        ediAco_longitud = findViewById(R.id.ediAco_longitud);
        icoAco_diametro = findViewById(R.id.icoAco_diametro);
        imgAco_acta = findViewById(R.id.imgAco_acta);
        imgAco_gabinete = findViewById(R.id.imgAco_gabinete);
        spiAco_material = findViewById(R.id.spiAco_material);
        imgAco_tc = findViewById(R.id.imgAco_tc);
        ll_observaciones = findViewById(R.id.observaciones);
        ll_observacionesgabinete = findViewById(R.id.observacionesgabinete);
        concluido = findViewById(R.id.concluido);
        llAcometida = findViewById(R.id.llAcometida);
        llTc = findViewById(R.id.llTc);
        listaTuberias = findViewById(R.id.listaTuberias);
        ImageView addObservacion = findViewById(R.id.addObservacion);
        ImageView addObservaciongabinete = findViewById(R.id.addObservaciongabinete);
        llAcomDatosG = findViewById(R.id.llAcomDatosG);
        txtTub_tuberiagabinete = findViewById(R.id.txtTub_tuberiagabinete);
        icoAco_fusionistagabinete = findViewById(R.id.icoAco_fusionistagabinete);
        txtAco_fusionista = findViewById(R.id.txtAco_fusionistagabinete);
        spiAco_fusionistagabinete = findViewById(R.id.spiAco_fusionistagabinete);
        icoAco_resultadogabinete = findViewById(R.id.icoAco_resultadogabinete);
        txtAco_resultado = findViewById(R.id.txtAco_resultadogabinete);
        spiAco_resultadogabinete = findViewById(R.id.spiAco_resultadogabinete);
        recy_acometidas = findViewById(R.id.recy_acometidas);
        llAcomUbicacion = findViewById(R.id.llAcomUbicacion);
        txtUbiAcom = findViewById(R.id.txtUbiAcom);
        icoAco_actagabinete = findViewById(R.id.icoAco_actagabinate);
        txtAco_acta = findViewById(R.id.txtAco_actagabinete);
        imgAco_actagabinete = findViewById(R.id.imgAco_actagabinete);
        icoAco_actagabinate = findViewById(R.id.icoAco_actagabinate);
        //System.out.println("8: " + listaTipoResultado1.get(0).getLabel());
        /*PAPU 2011 - 0*/
        spiAco_tipogabinete = findViewById(R.id.spiAco_tipogabinete);
        icoAco_tipogabinete = findViewById(R.id.icoAco_tipogabinete);
        txtAco_tipogabinete = findViewById(R.id.txtAco_tipogabinete);
        spiAco_tipogabinete.setTag(0);
        spiAco_tipogabinete.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == (int) spiAco_tipogabinete.getTag()) {
                    return;
                } else {
                    spiAco_tipogabinete.setTag(position);
                    if (position != 0) {
                        acometida.setTipoGabinete(setTipoGabinete(parent.getSelectedItem().toString()));
                    } else {
                        acometida.setTipoGabinete("");
                    }

                    validarCheck();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spiAco_resultadogabinete.setTag(0);
        spiAco_resultadogabinete.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()  {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("restAc: " +acometida.getResultado());
                if (position == (int) spiAco_resultadogabinete.getTag()) {
                    return;
                } else {
                    spiAco_resultadogabinete.setTag(position);
                    if (position != 0) {
                        acometida.setResultado(setTipoResultado(parent.getSelectedItem().toString()));
                    } else {
                        acometida.setResultado("");
                    }

                    switch (acometida.getResultado()) {
                        case RESULTADO_INSTALACION_ACOMETIDA_CONCLUIDA:
                            recy_acometidas.setVisibility(View.VISIBLE);
                            ll_observacionesgabinete.setVisibility(View.GONE);
                            switch (acometida.getTipoInstalacionAcometida()) {
                                case TIPO_INSTALACION_ACOMETIDA_ACOMETIDA:
                                    llAcometida.setVisibility(View.VISIBLE);
                                    llTc.setVisibility(View.GONE);
                                    break;
                                case TIPO_INSTALACION_ACOMETIDA_TC:
                                    llAcometida.setVisibility(View.GONE);
                                    llTc.setVisibility(View.VISIBLE);
                                    break;
                                default:
                                    llAcometida.setVisibility(View.GONE);
                                    llTc.setVisibility(View.GONE);
                            }
                            validarCheck();
                            break;
                        case RESULTADO_INSTALACION_ACOMETIDA_PENDIENTE:
                        case RESULTADO_INSTALACION_ACOMETIDA_RECHAZADA:
                            System.out.println("validando");
                            recy_acometidas.setVisibility(View.GONE);
                            ll_observacionesgabinete.setVisibility(View.VISIBLE);
                            List<Observacion> a = observacionDao.queryBuilder().where(ObservacionDao.Properties.IdInstalacionMontante.eq(acometida.getIdInstalacionAcometida())).list();
                            observacionDao.deleteInTx(a);
                            if (config.getOffline()) {
                                if (RESULTADO_INSTALACION_ACOMETIDA_PENDIENTE.equalsIgnoreCase(acometida.getResultado())) {
                                    listatipoObse.clear();
                                    List<TipoPendiente> tipoPendientes = tipoPendienteDao.queryBuilder().list();
                                    for (TipoPendiente tipoPendiente : tipoPendientes) {
                                        listatipoObse.add(Parse.getGenericBeanOutRO(tipoPendiente));
                                    }
                                    configurarObsgabinete();
                                } else {
                                    listatipoObse.clear();
                                    List<TipoRechazada> tipoRechazadas = tipoRechazadaDao.queryBuilder().list();
                                    for (TipoRechazada tipoRechazada : tipoRechazadas) {
                                        listatipoObse.add(Parse.getGenericBeanOutRO(tipoRechazada));
                                    }
                                    configurarObsgabinete();
                                }
                            } else {
                                callTiposObservacion();
                            }
                            break;
                        default:
                            concluido.setVisibility(View.GONE);
                            ll_observaciones.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spiAco_resultado.setTag(0);
        spiAco_resultado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("es este");
                System.out.println("restAc1: " +acometida.getResultado());
                if (position == (int) spiAco_resultado.getTag()) {
                    return;
                } else {
                    spiAco_resultado.setTag(position);
                    if (position != 0) {
                        acometida.setResultado(setTipoResultado(parent.getSelectedItem().toString()));
                    } else {
                        acometida.setResultado("");
                    }

                    switch (acometida.getResultado()) {
                        case RESULTADO_INSTALACION_ACOMETIDA_CONCLUIDA:
                            System.out.println("es este1");
                            List<Acometida> ac = acometidaDao.queryBuilder().where(AcometidaDao.Properties.IdInstalacionAcometida.eq(acometida.getIdInstalacionAcometida())).list();
                            if(ac.get(0).getActivo() == true){
                                lynuevo.setVisibility(View.VISIBLE);
                                lyantiguo.setVisibility(View.GONE);
                                acometida.setActivo(false);
                                acometidaDao.insertOrReplace(acometida);
                                mostrarLista();
                                //fordeTuberia();
                                System.out.println("Estoy cambiando de vista concluida");
                            }else{
                                concluido.setVisibility(View.VISIBLE);
                                ll_observaciones.setVisibility(View.GONE);
                                switch (acometida.getTipoInstalacionAcometida()) {
                                    case TIPO_INSTALACION_ACOMETIDA_ACOMETIDA:
                                        System.out.println("es este 2");
                                        llAcometida.setVisibility(View.VISIBLE);
                                        llTc.setVisibility(View.GONE);
                                        break;
                                    case TIPO_INSTALACION_ACOMETIDA_TC:
                                        System.out.println("es este 3");
                                        llAcometida.setVisibility(View.GONE);
                                        llTc.setVisibility(View.VISIBLE);
                                        break;
                                    default:
                                        System.out.println("es este 4");
                                        llAcometida.setVisibility(View.GONE);
                                        llTc.setVisibility(View.GONE);
                                }
                                validarCheck();
                            }
                            break;
                        case RESULTADO_INSTALACION_ACOMETIDA_PENDIENTE:
                        case RESULTADO_INSTALACION_ACOMETIDA_RECHAZADA:
                            System.out.println("es este 5");
                            concluido.setVisibility(View.GONE);
                            ll_observaciones.setVisibility(View.VISIBLE);
                            List<Observacion> a = observacionDao.queryBuilder().where(ObservacionDao.Properties.IdInstalacionMontante.eq(acometida.getIdInstalacionAcometida())).list();
                            //observacionDao.deleteInTx(a);
                            if (config.getOffline()) {
                                if (RESULTADO_INSTALACION_ACOMETIDA_PENDIENTE.equalsIgnoreCase(acometida.getResultado())) {
                                    listatipoObse.clear();
                                    List<TipoPendiente> tipoPendientes = tipoPendienteDao.queryBuilder().list();
                                    for (TipoPendiente tipoPendiente : tipoPendientes) {
                                        listatipoObse.add(Parse.getGenericBeanOutRO(tipoPendiente));
                                    }
                                    configurarObs();
                                } else {
                                    listatipoObse.clear();
                                    List<TipoRechazada> tipoRechazadas = tipoRechazadaDao.queryBuilder().list();
                                    for (TipoRechazada tipoRechazada : tipoRechazadas) {
                                        listatipoObse.add(Parse.getGenericBeanOutRO(tipoRechazada));
                                    }
                                    configurarObs();
                                }
                            } else {
                                callTiposObservacion();
                            }
                            break;
                        default:
                            System.out.println("es este 6");
                            concluido.setVisibility(View.GONE);
                            ll_observaciones.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        ediAco_diametro.setTag(1);
        ediAco_diametro.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (0 == (int) ediAco_diametro.getTag()) {
                    ediAco_diametro.setTag(1);
                    return;
                }
                if (s.length() > 0) {
                    if (Util.validarScaleAndPrecision(s.toString(), 2, 5)) {
                        acometida.setDiametro(s.toString());
                    } else {
                        ((App) getApplication()).showToast(String.format(getResources().getString(R.string.app_error_numero), 5, 2));
                        ediAco_diametro.setTag(0);
                        ediAco_diametro.getEditText().setText(acometida.getDiametro());
                        ediAco_diametro.getEditText().setSelection(ediAco_diametro.getEditText().getText().length());
                    }
                } else {
                    acometida.setDiametro("");
                }
                validarCheck();
            }
        });
        ediAco_longitud.setTag(1);
        ediAco_longitud.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (0 == (int) ediAco_longitud.getTag()) {
                    ediAco_longitud.setTag(1);
                    return;
                }
                if (s.length() > 0) {
                    if (Util.validarScaleAndPrecision(s.toString(), 2, 5)) {
                        acometida.setLongitud(s.toString());
                    } else {
                        ((App) getApplication()).showToast(String.format(getResources().getString(R.string.app_error_numero), 5, 2));
                        ediAco_longitud.setTag(0);
                        ediAco_longitud.getEditText().setText(acometida.getLongitud());
                        ediAco_longitud.getEditText().setSelection(ediAco_longitud.getEditText().getText().length());
                    }
                } else {
                    acometida.setLongitud("");
                }
                validarCheck();
            }
        });
        spiAco_material.setTag(0);
        spiAco_material.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == (int) spiAco_material.getTag()) {
                    return;
                } else {
                    spiAco_material.setTag(position);
                    if (position != 0) {
                        acometida.setMaterial(setTipoMaterial(parent.getSelectedItem().toString()));
                    } else {
                        acometida.setMaterial("");
                    }
                    validarCheck();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        imgAco_acta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(
                        Acometida2Activity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ((App) getApplication()).showToast("No puede realizar la acción porque no se ha dado el permiso de Almacenamiento");
                    return;
                }
                if (Util.isExternalStorageWritable()) {
                    if (Util.getAlbumStorageDir(Acometida2Activity.this)) {
                        File file = Util.getNuevaRutaFoto(acometida.getIdInstalacionAcometida().toString(), Acometida2Activity.this);
                        tmpRuta = file.getPath();
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        Uri outputUri = FileProvider.getUriForFile(Acometida2Activity.this, getApplicationContext().getPackageName() + ".provider", file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            ClipData clip = ClipData.newUri(getContentResolver(), "Concesionario", outputUri);
                            intent.setClipData(clip);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        }
                        startActivityForResult(intent, 2);
                    } else {
                        ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                    }
                } else {
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                }
            }
        });
        imgAco_tc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(
                        Acometida2Activity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ((App) getApplication()).showToast("No puede realizar la acción porque no se ha dado el permiso de Almacenamiento");
                    return;
                }
                if (Util.isExternalStorageWritable()) {
                    if (Util.getAlbumStorageDir(Acometida2Activity.this)) {
                        File file = Util.getNuevaRutaFoto(acometida.getIdInstalacionAcometida().toString(), Acometida2Activity.this);
                        tmpRuta = file.getPath();
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        Uri outputUri = FileProvider.getUriForFile(Acometida2Activity.this, getApplicationContext().getPackageName() + ".provider", file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            ClipData clip = ClipData.newUri(getContentResolver(), "Concesionario", outputUri);
                            intent.setClipData(clip);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        }
                        startActivityForResult(intent, 3);
                    } else {
                        ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                    }
                } else {
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                }
            }
        });
        imgAco_gabinete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(
                        Acometida2Activity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ((App) getApplication()).showToast("No puede realizar la acción porque no se ha dado el permiso de Almacenamiento");
                    return;
                }
                if (Util.isExternalStorageWritable()) {
                    if (Util.getAlbumStorageDir(Acometida2Activity.this)) {
                        File file = Util.getNuevaRutaFoto(acometida.getIdInstalacionAcometida().toString(), Acometida2Activity.this);
                        tmpRuta = file.getPath();
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        Uri outputUri = FileProvider.getUriForFile(Acometida2Activity.this, getApplicationContext().getPackageName() + ".provider", file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            ClipData clip = ClipData.newUri(getContentResolver(), "Concesionario", outputUri);
                            intent.setClipData(clip);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        }
                        startActivityForResult(intent, 4);
                    } else {
                        ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                    }
                } else {
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                }
            }
        });

        imgAco_actagabinete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(
                        Acometida2Activity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ((App) getApplication()).showToast("No puede realizar la acción porque no se ha dado el permiso de Almacenamiento");
                    return;
                }
                if (Util.isExternalStorageWritable()) {
                    if (Util.getAlbumStorageDir(Acometida2Activity.this)) {
                        File file = Util.getNuevaRutaFoto(acometida.getIdInstalacionAcometida().toString(), Acometida2Activity.this);
                        tmpRuta = file.getPath();
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        Uri outputUri = FileProvider.getUriForFile(Acometida2Activity.this, getApplicationContext().getPackageName() + ".provider", file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            ClipData clip = ClipData.newUri(getContentResolver(), "Concesionario", outputUri);
                            intent.setClipData(clip);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        }
                        startActivityForResult(intent, 6);
                    } else {
                        ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                    }
                } else {
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                }
            }
        });

        addObservacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Add observacion");
                Observacion observacion = new Observacion();
                observacion.setIdInstalacionMontante(acometida.getIdInstalacionAcometida());
                observacion.setDescripcionObservacion("");
                observacion.setTipoObservacion("0");
                observaciones.add(observacion);
                observacionDao.insertOrReplace(observacion);
                observacionAdaptador.notifyDataSetChanged();
                Util.setListViewHeightBasedOnChildren(listaObservacionAcometida);
            }
        });

        addObservaciongabinete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Observacion observacion = new Observacion();
                System.out.println("Add observacion Gabinete");
                observacion.setIdInstalacionMontante(acometida.getIdInstalacionAcometida());
                observacion.setDescripcionObservacion("");
                observacion.setTipoObservacion("0");
                observaciones.add(observacion);
                observacionDao.insertOrReplace(observacion);
                observacionAdaptador.notifyDataSetChanged();
                Util.setListViewHeightBasedOnChildren(listaObservacionAcometidagabinete);
            }
        });

        btnAco_grabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (RESULTADO_INSTALACION_ACOMETIDA_CONCLUIDA.equalsIgnoreCase(acometida.getResultado())) {
                    //grabarAcometida();
                    grabarAcometida();
                } else {
                    //if (quitarEspacios()) {
                    grabarAcometida();
                        //grabarAcometida();
                    //}
                }
            }
        });
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    Precision precision = new Precision();
                    precision.setIdSolicitud(acometida.getIdInstalacionAcometida());
                    precision.setLatitud(String.valueOf(location.getLatitude()));
                    precision.setLongitud(String.valueOf(location.getLongitude()));
                    precision.setPrecision(String.valueOf(location.getAccuracy()));
                    precision.setEstado("3");
                    precisionDao.insertOrReplace(precision);
                }
            }
        };

        boolean esAcometida = acometida.getNombreTipoInstalacionAcometida().equalsIgnoreCase("acometida");
        int visibleAcometida = esAcometida ? View.VISIBLE : View.GONE;
        spiAco_tipogabinete.setVisibility( visibleAcometida);
        icoAco_tipogabinete.setVisibility( visibleAcometida);
        txtAco_tipogabinete.setVisibility( visibleAcometida);
        System.out.println("NumTub: "+NumTub);
        System.out.println("Tipo: " +tipo);

        if(tipo.equals("T")){
            System.out.println("Tuberia");
            if(NumTub.equals("null") || NumTub.equals("O")){
                lynuevo.setVisibility(View.GONE);
                lyantiguo.setVisibility(View.VISIBLE);
                System.out.println("Fijo");
                //btnAco_grabar.setEnabled(false);
            }else{
                List<Acometida> ac = acometidaDao.queryBuilder().where(AcometidaDao.Properties.IdInstalacionAcometida.eq(acometida.getIdInstalacionAcometida())).list();
                System.out.println("AC: "+ac.get(0).getActivo() + " ID: "+ac.get(0).getIdInstalacionAcometida() + " IDACOM: " +acometida.getIdInstalacionAcometida());
                if(ac.get(0).getActivo() == true){
                //if(pref.getString(Util.activo,"").equals("true")){
                    lynuevo.setVisibility(View.GONE);
                    lyantiguo.setVisibility(View.VISIBLE);
                }else{
                    numtabsint = Integer.parseInt(NumTub);
                    lynuevo.setVisibility(View.VISIBLE);
                    lyantiguo.setVisibility(View.GONE);
                    System.out.println("variable");
                    System.out.println("numero de tabs: "+numtabsint);
                    btnAco_grabar.setEnabled(false);
                    mostrarLista();
                }
            }
        }else{
            if(NumTub.equals("null") || NumTub.equals("O")){
                System.out.println("NumTub: "+NumTub);
                System.out.println("Gabinete antiguo");
                lyantiguo.setVisibility(View.VISIBLE);
                lynuevo.setVisibility(View.GONE);
                lygabinetes.setVisibility(View.GONE);
                concluido.setVisibility(View.VISIBLE);
                llAcometida.setVisibility(View.VISIBLE);
                btnAco_grabar.setEnabled(false);

            }else{
                System.out.println("NumTub: "+NumTub);
                System.out.println("Gabinete nuevo");
                lyantiguo.setVisibility(View.GONE);
                lynuevo.setVisibility(View.GONE);
                lygabinetes.setVisibility(View.VISIBLE);
                mostrarGabinete();
                btnAco_grabar.setEnabled(false);
            }
        }

        aceptarPermiso();
    }

    private void mostrarGabinete(){
        TipoGabineteAcometida2 tipoGabineteAcometida2 = new TipoGabineteAcometida2(new TipoGabineteAcometida2.OnTipoGabineteAcometida2() {
            @Override
            public void OnTipoGabineteAcometida2(ListGabineteProyectoInstalacionOutRo listGabineteProyectoInstalacionOutRo) {
                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                } else if (listGabineteProyectoInstalacionOutRo == null) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGabineteProyectoInstalacionOutRo.getResultCode())) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast(listGabineteProyectoInstalacionOutRo.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listGabineteProyectoInstalacionOutRo.getMessage());
                } else {
                    listGabineteProyectoInstalacion = listGabineteProyectoInstalacionOutRo.getListaGabineteProyectoInstalacionOutRo();
                    if(listGabineteProyectoInstalacion.get(0).getClasificacionGabinete().getIdClasificacionGabinete() == null){
                         System.out.println("Version antigiua");
                    }else{
                        System.out.println("000: " + listGabineteProyectoInstalacion.get(0).getClasificacionGabinete().getIdClasificacionGabinete());
                        System.out.println("001: " + listGabineteProyectoInstalacion.get(0).getClasificacionGabinete().getNombreClasificacionGabinete());
                        System.out.println("002: " + listGabineteProyectoInstalacion.get(0).getClasificacionGabinete().getValorClasificacionGabinete());
                        tipoGabinete2();
                    }


                }
            }
        });
        tipoGabineteAcometida2.execute(config);
    }

    private void tipoGabinete2(){
        TipoGabineteAcometida tipoGabinete = new TipoGabineteAcometida(new TipoGabineteAcometida.OnTipoGabineteCompleted() {
            @Override
            public void OnTipoGabineteCompleted(ListGenericBeanOutRO listGenericBeanOutRO) {
                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                } else if (listGenericBeanOutRO == null) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast(listGenericBeanOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listGenericBeanOutRO.getMessage());
                } else {

                    listaTipoGabinete1 = listGenericBeanOutRO.getGenericBean();
                    System.out.println("2: " + listaTipoGabinete1.get(0).getLabel());
                    fordeGabinete();
                }
            }
        });
        tipoGabinete.execute(config);
    }

    private void mostrarLista() {
        TipoFusionista tipoFusionista = new TipoFusionista(new TipoFusionista.OnTipoFusionistaCompleted() {
            @Override
            public void onTipoFusionistaCompled(ListBaseInspectorOutRO listBaseInspectorOutRO) {
                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                } else if (listBaseInspectorOutRO == null) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listBaseInspectorOutRO.getResultCode())) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast(listBaseInspectorOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listBaseInspectorOutRO.getMessage());
                } else {

                    listaTipoFusionista1 = listBaseInspectorOutRO.getInspector();
                    System.out.println("1: " + listaTipoFusionista1.get(0).getNombreInspector());
                    tipoGabinete();
                }
            }
        });
        tipoFusionista.execute(config);
    }

    private void tipoGabinete(){
        TipoGabineteAcometida tipoGabinete = new TipoGabineteAcometida(new TipoGabineteAcometida.OnTipoGabineteCompleted() {
            @Override
            public void OnTipoGabineteCompleted(ListGenericBeanOutRO listGenericBeanOutRO) {
                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                } else if (listGenericBeanOutRO == null) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast(listGenericBeanOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listGenericBeanOutRO.getMessage());
                } else {

                    listaTipoGabinete1 = listGenericBeanOutRO.getGenericBean();
                    System.out.println("2: " + listaTipoGabinete1.get(0).getLabel());
                    tipoResultado();
                }
            }
        });
        tipoGabinete.execute(config);
    }

    private void tipoResultado(){
        TipoResultadoAcometida tipoResultadoAcometida = new TipoResultadoAcometida(new TipoResultadoAcometida.OnTipoResultadoCompleted() {
            @Override
            public void onTipoResultadoCompled(ListGenericBeanOutRO listGenericBeanOutRO) {
                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                } else if (listGenericBeanOutRO == null) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast(listGenericBeanOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listGenericBeanOutRO.getMessage());
                } else {

                    listaTipoResultado1 = listGenericBeanOutRO.getGenericBean();
                    System.out.println("3: " + listaTipoResultado1.get(0).getLabel());

                    tipoMaterial();
                }
            }
        });
        tipoResultadoAcometida.execute(config);
    }

    private void tipoMaterial(){
        TipoMaterialInstalacion tipoMaterialInstalacion = new TipoMaterialInstalacion(new TipoMaterialInstalacion.OnTipoMaterialInstalacionCompleted() {
            @Override
            public void onTipoMaterialInstalacionCompled(ListGenericBeanOutRO listGenericBeanOutRO) {
                hideProgressDialog();
                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                } else if (listGenericBeanOutRO == null) {
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                    ((App) getApplication()).showToast(listGenericBeanOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listGenericBeanOutRO.getMessage());
                } else {

                    listaTipoMaterial1 = listGenericBeanOutRO.getGenericBean();

                    System.out.println("4: " + listaTipoMaterial1.get(0).getLabel());

                    if ("".equalsIgnoreCase(config.getParametro5())) {
                        config.setParametro5("800x600");
                    }
                    if ("".equalsIgnoreCase(config.getPrecision())) {
                        config.setPrecision("10000");
                    }

                    tipoObservacionPendiente();
                }
            }
        });
        tipoMaterialInstalacion.execute(config);
    }

    private void tipoObservacionPendiente() {

        TipoObservacionPendiente tipoObservacionPendiente = new TipoObservacionPendiente(new TipoObservacionPendiente.OnTipoObservacionCompleted() {
            @Override
            public void onTipoObservacionCompleted(ListGenericBeanOutRO listGenericBeanOutRO) {
                hideProgressDialog();
                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                } else if (listGenericBeanOutRO == null) {
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                } else if (!listGenericBeanOutRO.getResultCode().equalsIgnoreCase(Constantes.RESULTADO_SUCCESS)) {
                    ((App) getApplication()).showToast(listGenericBeanOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listGenericBeanOutRO.getMessage());
                } else {
                    listatipoObsePendiente = listGenericBeanOutRO.getGenericBean();
                    tipoObservacionRechazado();
                }
            }
        });
        tipoObservacionPendiente.execute(config);
        showProgressDialog(R.string.app_procesando);

    }

    private void  tipoObservacionRechazado(){
        TipoObservacionRechazada tipoObservacionRechazada = new TipoObservacionRechazada(new TipoObservacionRechazada.OnTipoObservacionCompleted() {
            @Override
            public void onTipoObservacionCompleted(ListGenericBeanOutRO listGenericBeanOutRO) {
                hideProgressDialog();
                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                } else if (listGenericBeanOutRO == null) {
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                } else if (!listGenericBeanOutRO.getResultCode().equalsIgnoreCase(Constantes.RESULTADO_SUCCESS)) {
                    ((App) getApplication()).showToast(listGenericBeanOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listGenericBeanOutRO.getMessage());
                } else {
                    listatipoObseRechazado = listGenericBeanOutRO.getGenericBean();
                    fordeTuberia();
                }
            }
        });
        tipoObservacionRechazada.execute(config);
    }

    private void fordeTuberia(){
        //tuberiasDao.deleteAll();
        //List<Tuberias> tuberiasDaos = tuberiasDao.queryBuilder().list();
        List<Tuberias> tuberiasDaos = tuberiasDao.queryBuilder().where(TuberiasDao.Properties.IdInstalacionAcometida.eq(acometida.getIdInstalacionAcometida())).list();
        System.out.println("acometidaDao:" + tuberiasDaos.size() + " getIdInstalacionAcometida: "+acometida.getIdInstalacionAcometida());
        System.out.println("Numero de registros2:" + tuberiasDaos.size());

        if(tuberiasDaos.size() > 0){
            System.out.println("Son Iguales " + tuberiasDaos.size());
            for(int i=0; i < tuberiasDaos.size(); i++){
                System.out.println("1: "+acometida.getIdInstalacionAcometida() + " 2: "+tuberiasDaos.get(i).getIdInstalacionAcometida());
                //if(acometida.getIdInstalacionAcometida() == tuberiasDaos.get(i).getIdInstalacionAcometida()){
                    System.out.println("For del dao");
                    Tuberias tuberia = new Tuberias(tuberiasDaos.get(i).getId(), acometida.getIdInstalacionAcometida(),tuberiasDaos.get(i).getFusionista(),tuberiasDaos.get(i).getResultado(),tuberiasDaos.get(i).getDiametro(),tuberiasDaos.get(i).getLongitud(),tuberiasDaos.get(i).getMaterial(),tuberiasDaos.get(i).getFotoActa(),tuberiasDaos.get(i).getFotoTC(),tuberiasDaos.get(i).getFotoGabinete(),i);
                    tuberias.add(tuberia);
                    tubyTcAdaptador = new TubyTcAdaptador(Acometida2Activity.this, tuberias);
                    tubyTcAdaptador.setCallback(this);
                    tubyTcAdaptador.setListatipoFusionista(listaTipoFusionista1);
                    tubyTcAdaptador.setListatipoResultado(listaTipoResultado1);
                    tubyTcAdaptador.setListatipoMaterial(listaTipoMaterial1);
                    tubyTcAdaptador.setListatipoObsePendiente(listatipoObsePendiente);
                    tubyTcAdaptador.setListatipoObseRechazado(listatipoObseRechazado);
                    tubyTcAdaptador.setBtnVis1_grabar(btnAco_grabar);
                    tubyTcAdaptador.setObservacionDao(observacionDao);
                    tubyTcAdaptador.setTuberiasDao(tuberiasDao);
                    tubyTcAdaptador.setId(acometida.getIdInstalacionAcometida());
                    tubyTcAdaptador.notifyDataSetChanged();
                    listaTuberias.setAdapter(tubyTcAdaptador);
                    ban = false;
                    System.out.println("ff: "+ i);
            }
           /* if(ban==true){
                System.out.println("Son diferentes11: " + numtabsint);
                for(int j=0; j < numtabsint; j++) {
                    System.out.println("111: "+acometida.getIdInstalacionAcometida());
                    Tuberias tuberia = new Tuberias(null, acometida.getIdInstalacionAcometida(),"","","","","","","","",j);
                    tuberias.add(tuberia);
                    tubyTcAdaptador = new TubyTcAdaptador(Acometida2Activity.this, tuberias);
                    tubyTcAdaptador.setCallback(this);
                    tubyTcAdaptador.setListatipoFusionista(listaTipoFusionista1);
                    System.out.println("fus: " +listaTipoFusionista1.get(0).getNombreInspector() + " id: " + listaTipoFusionista1.get(0).getIdInspector());
                    System.out.println("mat" + listaTipoMaterial.get(0).getValue());
                    tubyTcAdaptador.setListatipoResultado(listaTipoResultado1);
                    tubyTcAdaptador.setListatipoMaterial(listaTipoMaterial1);
                    tubyTcAdaptador.setListatipoObsePendiente(listatipoObsePendiente);
                    tubyTcAdaptador.setListatipoObseRechazado(listatipoObseRechazado);
                    tubyTcAdaptador.setBtnVis1_grabar(btnAco_grabar);
                    tubyTcAdaptador.setObservacionDao(observacionDao);
                    tubyTcAdaptador.setTuberiasDao(tuberiasDao);
                    tubyTcAdaptador.setId(acometida.getIdInstalacionAcometida());
                    tubyTcAdaptador.notifyDataSetChanged();
                    listaTuberias.setAdapter(tubyTcAdaptador);
                    ban=false;
                }
            }*/
        }else{
            System.out.println("Son else dao " + numtabsint);
            for(int i=0; i < numtabsint; i++) {
                System.out.println("222: "+acometida.getIdInstalacionAcometida());
                Tuberias tuberia = new Tuberias(null, acometida.getIdInstalacionAcometida(),"","","","","","","","",i);
                tuberias.add(tuberia);
                tubyTcAdaptador = new TubyTcAdaptador(Acometida2Activity.this, tuberias);
                tubyTcAdaptador.setCallback(this);
                tubyTcAdaptador.setListatipoFusionista(listaTipoFusionista1);
                System.out.println("fus: " +listaTipoFusionista1.get(0).getNombreInspector() + " id: " + listaTipoFusionista1.get(0).getIdInspector());
                System.out.println("mat" + listaTipoMaterial.get(0).getValue());
                tubyTcAdaptador.setListatipoResultado(listaTipoResultado1);
                tubyTcAdaptador.setListatipoMaterial(listaTipoMaterial1);
                tubyTcAdaptador.setListatipoObsePendiente(listatipoObsePendiente);
                tubyTcAdaptador.setListatipoObseRechazado(listatipoObseRechazado);
                tubyTcAdaptador.setBtnVis1_grabar(btnAco_grabar);
                tubyTcAdaptador.setObservacionDao(observacionDao);
                tubyTcAdaptador.setTuberiasDao(tuberiasDao);
                tubyTcAdaptador.setId(acometida.getIdInstalacionAcometida());
                tubyTcAdaptador.notifyDataSetChanged();
                listaTuberias.setAdapter(tubyTcAdaptador);
            }
        }
    }

    private void fordeGabinete(){
        //acometidaNuevaDao.deleteAll();
        //List<AcometidaNueva> gabineteDao = acometidaNuevaDao.queryBuilder().list();
        List<AcometidaNueva> gabineteDao = acometidaNuevaDao.queryBuilder().where(AcometidaNuevaDao.Properties.IdInstalacionAcometida.eq(acometida.getIdInstalacionAcometida())).list();
        System.out.println("acometidaDao:" + gabineteDao.size() + " getIdInstalacionAcometida: "+acometida.getIdInstalacionAcometida());
        System.out.println(" listGabineteProyectoInstalacion: "+listGabineteProyectoInstalacion.get(0).getIdGabineteProyectoInstalacion());

        if(gabineteDao.size() > 0){
            for(int i=0; i < gabineteDao.size(); i++) {
                System.out.println("1: "+acometida.getIdInstalacionAcometida() + " 2: "+gabineteDao.get(i).getIdInstalacionAcometida());
                //if(acometida.getIdInstalacionAcometida() == gabineteDao.get(i).getIdInstalacionAcometida()){
                    System.out.println("if del for del dao");
                    AcometidaNueva acometidaNueva = new AcometidaNueva(gabineteDao.get(i).getId(),acometida.getIdInstalacionAcometida(),gabineteDao.get(i).getIdClasificacionGabinete(), gabineteDao.get(i).getNombreClasificacionGabinete(), gabineteDao.get(i).getValorClasificacionGabinete() , gabineteDao.get(i).getNumerodeOrden(), gabineteDao.get(i).getNumerodeGabinete(),gabineteDao.get(i).getFotoGabinete(),gabineteDao.get(i).getTipoGabinete(),i,listGabineteProyectoInstalacion.get(i).getIdGabineteProyectoInstalacion());
                    acometidaNuevas.add(acometidaNueva);
                    acometidaNewAdaptador = new AcometidaNewAdaptador(Acometida2Activity.this, acometidaNuevas);
                    acometidaNewAdaptador.setCallback(this);
                    acometidaNewAdaptador.setListaProyectoInstalacion(listGabineteProyectoInstalacion);
                    acometidaNewAdaptador.setListatipoGabinete(listaTipoGabinete1);
                    acometidaNewAdaptador.setAcometidaNuevaDao(acometidaNuevaDao);
                    acometidaNewAdaptador.setBtnVis1_grabar(btnAco_grabar);
                    acometidaNewAdaptador.notifyDataSetChanged();
                    recy_acometidas.setAdapter(acometidaNewAdaptador);

                    if ("".equalsIgnoreCase(acometida.getFotoGabinete())) {
                        icoAco_actagabinate.setImageResource(R.mipmap.ic_warning_black_36dp);
                    } else {
                        icoAco_actagabinate.setImageResource(R.mipmap.ic_done_black_36dp);
                    }

                    if (!"".equalsIgnoreCase(acometida.getFotoGabinete())) {
                        loadBitmap(acometida.getFotoGabinete(), imgAco_actagabinete);
                    }

                    System.out.println("resttT: "+acometida.getResultado());
                    switch (acometida.getResultado()) {
                    case RESULTADO_INSTALACION_ACOMETIDA_CONCLUIDA:
                        recy_acometidas.setVisibility(View.VISIBLE);
                        ll_observacionesgabinete.setVisibility(View.GONE);
                        switch (acometida.getTipoInstalacionAcometida()) {
                            case TIPO_INSTALACION_ACOMETIDA_ACOMETIDA:
                                llAcometida.setVisibility(View.VISIBLE);
                                llTc.setVisibility(View.GONE);
                                break;
                            case TIPO_INSTALACION_ACOMETIDA_TC:
                                llAcometida.setVisibility(View.GONE);
                                llTc.setVisibility(View.VISIBLE);
                                break;
                            default:
                                llAcometida.setVisibility(View.GONE);
                                llTc.setVisibility(View.GONE);
                        }
                        validarCheck();
                        break;
                    case RESULTADO_INSTALACION_ACOMETIDA_PENDIENTE:
                    case RESULTADO_INSTALACION_ACOMETIDA_RECHAZADA:
                        System.out.println("validando");
                        recy_acometidas.setVisibility(View.GONE);
                        ll_observacionesgabinete.setVisibility(View.VISIBLE);
                        List<Observacion> a = observacionDao.queryBuilder().where(ObservacionDao.Properties.IdInstalacionMontante.eq(acometida.getIdInstalacionAcometida())).list();
                        //observacionDao.deleteInTx(a);
                        if (config.getOffline()) {
                            if (RESULTADO_INSTALACION_ACOMETIDA_PENDIENTE.equalsIgnoreCase(acometida.getResultado())) {
                                listatipoObse.clear();
                                List<TipoPendiente> tipoPendientes = tipoPendienteDao.queryBuilder().list();
                                for (TipoPendiente tipoPendiente : tipoPendientes) {
                                    listatipoObse.add(Parse.getGenericBeanOutRO(tipoPendiente));
                                }
                                configurarObsgabinete();
                            } else {
                                listatipoObse.clear();
                                List<TipoRechazada> tipoRechazadas = tipoRechazadaDao.queryBuilder().list();
                                for (TipoRechazada tipoRechazada : tipoRechazadas) {
                                    listatipoObse.add(Parse.getGenericBeanOutRO(tipoRechazada));
                                }
                                configurarObsgabinete();
                            }
                        } else {
                            callTiposObservacion();
                        }
                        break;
                    default:
                        concluido.setVisibility(View.GONE);
                        ll_observaciones.setVisibility(View.GONE);
                }

                /*}else{
                    for(int j=0; i < listGabineteProyectoInstalacion.size(); j++) {
                        System.out.println("for del numero de gabinetes");
                        AcometidaNueva acometidaNueva = new AcometidaNueva(null,acometida.getIdInstalacionAcometida(),0, "", 0 , 0, 0,"","",j,listGabineteProyectoInstalacion.get(i).getIdGabineteProyectoInstalacion());
                        acometidaNuevas.add(acometidaNueva);
                        acometidaNewAdaptador = new AcometidaNewAdaptador(Acometida2Activity.this, acometidaNuevas);
                        acometidaNewAdaptador.setCallback(this);
                        acometidaNewAdaptador.setListaProyectoInstalacion(listGabineteProyectoInstalacion);
                        acometidaNewAdaptador.setListatipoGabinete(listaTipoGabinete1);
                        acometidaNewAdaptador.setAcometidaNuevaDao(acometidaNuevaDao);
                        acometidaNewAdaptador.setBtnVis1_grabar(btnAco_grabar);
                        acometidaNewAdaptador.notifyDataSetChanged();
                        recy_acometidas.setAdapter(acometidaNewAdaptador);

                        System.out.println("111: "+acometida.getIdInstalacionAcometida());
                    }
                }*/
            }
        }else{
            for(int i=0; i < listGabineteProyectoInstalacion.size(); i++) {
                //AcometidaNueva acometidaNueva2 = new AcometidaNueva(listGabineteProyectoInstalacion.get(i).getClasificacionGabinete().getIdClasificacionGabinete(), listGabineteProyectoInstalacion.get(i).getClasificacionGabinete().getNombreClasificacionGabinete(), listGabineteProyectoInstalacion.get(i).getClasificacionGabinete().getValorClasificacionGabinete(), listGabineteProyectoInstalacion.get(i).getNumeroOrden(), listGabineteProyectoInstalacion.get(i).getNumeroGabinete(),"","",i);
                AcometidaNueva acometidaNueva = new AcometidaNueva(null,acometida.getIdInstalacionAcometida(),0, "", 0 , 0, 0,"","",i,listGabineteProyectoInstalacion.get(i).getIdGabineteProyectoInstalacion());
                acometidaNuevas.add(acometidaNueva);
                acometidaNewAdaptador = new AcometidaNewAdaptador(Acometida2Activity.this, acometidaNuevas);
                acometidaNewAdaptador.setCallback(this);
                acometidaNewAdaptador.setListaProyectoInstalacion(listGabineteProyectoInstalacion);
                acometidaNewAdaptador.setListatipoGabinete(listaTipoGabinete1);
                acometidaNewAdaptador.setAcometidaNuevaDao(acometidaNuevaDao);
                acometidaNewAdaptador.setBtnVis1_grabar(btnAco_grabar);
                acometidaNewAdaptador.notifyDataSetChanged();
                recy_acometidas.setAdapter(acometidaNewAdaptador);
                System.out.println("aqui dentro del fot");
                System.out.println("Numero: " + i);
                System.out.println("222: "+acometida.getIdInstalacionAcometida());
            }
        }

        System.out.println("aqui en el for");
    }

    private void grabarAcometida() {
        List<Acometida> ac = acometidaDao.queryBuilder().where(AcometidaDao.Properties.IdInstalacionAcometida.eq(acometida.getIdInstalacionAcometida())).list();
        if(ac.get(0).getActivo() == true){
            AlertDialog.Builder builder = new AlertDialog.Builder(Acometida2Activity.this);
            builder.setTitle(R.string.txtHab_mensajeConfirmacion);
            builder.setMessage(String.format(getString(R.string.txtAco_descripcionMensaje), acometida.getNombreTipoInstalacionAcometida()));
            builder.setCancelable(false);
            builder.setPositiveButton(R.string.btnHab_si, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    System.out.println("Grabar tuberia antigua 1");
                    grabarAcometidaOld();
                }
            });
            builder.setNegativeButton(R.string.btnHab_no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.show();
        }else{
            if (tipo.equals("T")) {
                if (NumTub.equals("null") || NumTub.equals("O")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Acometida2Activity.this);
                    builder.setTitle(R.string.txtHab_mensajeConfirmacion);
                    builder.setMessage(String.format(getString(R.string.txtAco_descripcionMensaje), acometida.getNombreTipoInstalacionAcometida()));
                    builder.setCancelable(false);
                    builder.setPositiveButton(R.string.btnHab_si, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            System.out.println("Grabar tuberia antigua 2");
                            grabarAcometidaOld();
                        }
                    });
                    builder.setNegativeButton(R.string.btnHab_no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.show();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Acometida2Activity.this);
                    builder.setTitle(R.string.txtHab_mensajeConfirmacion);
                    builder.setMessage(String.format(getString(R.string.txtAco_descripcionMensaje), acometida.getNombreTipoInstalacionAcometida()));
                    builder.setCancelable(false);
                    builder.setPositiveButton(R.string.btnHab_si, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            System.out.println("Grabar tuberia");
                            grabarTuberia();
                        }
                    });
                    builder.setNegativeButton(R.string.btnHab_no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.show();
                }
            } else {
                if(NumTub.equals("null") || NumTub.equals("O")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(Acometida2Activity.this);
                    builder.setTitle(R.string.txtHab_mensajeConfirmacion);
                    builder.setMessage(String.format(getString(R.string.txtAco_descripcionMensaje), acometida.getNombreTipoInstalacionAcometida()));
                    builder.setCancelable(false);
                    builder.setPositiveButton(R.string.btnHab_si, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            System.out.println("Grabar tuberia antigua 2");
                            grabarAcometidaOld();
                        }
                    });
                    builder.setNegativeButton(R.string.btnHab_no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.show();
                }else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(Acometida2Activity.this);
                    builder.setTitle(R.string.txtHab_mensajeConfirmacion);
                    builder.setMessage(String.format(getString(R.string.txtAco_descripcionMensaje), acometida.getNombreTipoInstalacionAcometida()));
                    builder.setCancelable(false);
                    builder.setPositiveButton(R.string.btnHab_si, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            System.out.println("Grabar gabinete");
                            grabarGabinete();
                        }
                    });
                    builder.setNegativeButton(R.string.btnHab_no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    builder.show();
                }

            }
        }

    }

    private void grabarAcometidaOld(){
        acometida.setGrabar(true);
        if (config.getOffline()) {
            MInstalacionAcometidaOutRO mInstalacionAcometidaOutRO = mInstalacionAcometidaOutRODao.queryBuilder().where(MInstalacionAcometidaOutRODao.Properties.IdInstalacionAcometida.eq(acometida.getIdInstalacionAcometida())).limit(1).unique();
            mInstalacionAcometidaOutRO.setResultCode("100");
            acometida.setFechaAprobacion(FormatoFecha.getfecha());
            acometida.setFechaRegistroOffline(FormatoFecha.getfecha());
            mInstalacionAcometidaOutRODao.insertOrReplace(mInstalacionAcometidaOutRO);
            acometidaDao.insertOrReplace(acometida);
            AlertDialog.Builder builder = new AlertDialog.Builder(Acometida2Activity.this);
            builder.setTitle(R.string.txtHab_mensajeConfirmacion);
            builder.setMessage(String.format(getResources().getString(R.string.txtAco_descripcionRegistro), acometida.getNombreTipoInstalacionAcometida()));
            builder.setCancelable(false);
            builder.setPositiveButton(R.string.btnHab_aceptar, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    System.out.println("Old");
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString(Util.activo,"false");
                    editor.apply();
                    acometida.setActivo(false);
                    acometidaDao.insertOrReplace(acometida);
                    Intent intent = new Intent(Acometida2Activity.this, BuscarAcometidaActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            builder.show();
        } else {
            if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                showProgressDialog(R.string.app_procesando);
                GrabarAcometida grabarAcometida = new GrabarAcometida(new GrabarAcometida.OnGrabarAcometidaAsyncTaskCompleted() {
                    @Override
                    public void onGrabarAcometidaAsyncTaskCompleted(InstalacionAcometidaOutRO instalacionAcometidaOutRO) {
                        hideProgressDialog();
                        if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                            ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                        } else if (instalacionAcometidaOutRO == null) {
                            ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                        } else if (!instalacionAcometidaOutRO.getResultCode().equalsIgnoreCase(Constantes.RESULTADO_SUCCESS)) {
                            ((App) getApplication()).showToast(instalacionAcometidaOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : instalacionAcometidaOutRO.getMessage());
                        } else {
//                                                            new BorrarFoto().execute(suministro.getIdHabilitacion().toString());
                            AlertDialog.Builder builder = new AlertDialog.Builder(Acometida2Activity.this);
                            builder.setTitle(R.string.txtHab_mensajeConfirmacion);
                            builder.setMessage(String.format(getResources().getString(R.string.txtAco_descripcionRegistro), acometida.getNombreTipoInstalacionAcometida()));
                            builder.setCancelable(false);
                            builder.setPositiveButton(R.string.btnHab_aceptar, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    List<Observacion> observacions = observacionDao.queryBuilder()
                                            .where(ObservacionDao.Properties.IdInstalacionMontante.eq(acometida.getIdInstalacionAcometida())).list();
                                    observacionDao.deleteInTx(observacions);
                                    List<Precision> precisiones = precisionDao.queryBuilder()
                                            .where(PrecisionDao.Properties.Estado.eq("3"),
                                                    PrecisionDao.Properties.IdSolicitud.eq(acometida.getIdInstalacionAcometida())).list();
                                    precisionDao.deleteInTx(precisiones);
                                    acometidaDao.delete(acometida);
                                    System.out.println("Old");
                                    SharedPreferences.Editor editor = pref.edit();
                                    editor.putString(Util.activo,"false");
                                    editor.apply();
                                    acometida.setActivo(false);
                                    acometidaDao.insertOrReplace(acometida);
                                    Intent intent = new Intent(Acometida2Activity.this, BuscarAcometidaActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                            builder.show();
                        }
                    }
                }, getApplicationContext());
                grabarAcometida.execute(acometida);
            } else {
                ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
            }
        }
    }

    private void grabarTuberia() {
        acometida.setGrabar(true);
        if (config.getOffline()) {
            System.out.println("Off");
            MInstalacionAcometidaOutRO mInstalacionAcometidaOutRO = mInstalacionAcometidaOutRODao.queryBuilder().where(MInstalacionAcometidaOutRODao.Properties.IdInstalacionAcometida.eq(acometida.getIdInstalacionAcometida())).limit(1).unique();
            mInstalacionAcometidaOutRO.setResultCode("100");
            acometida.setFechaAprobacion(FormatoFecha.getfecha());
            acometida.setFechaRegistroOffline(FormatoFecha.getfecha());
            mInstalacionAcometidaOutRODao.insertOrReplace(mInstalacionAcometidaOutRO);
            acometidaDao.insertOrReplace(acometida);
            AlertDialog.Builder builder = new AlertDialog.Builder(Acometida2Activity.this);
            builder.setTitle(R.string.txtHab_mensajeConfirmacion);
            builder.setMessage(String.format(getResources().getString(R.string.txtAco_descripcionRegistro), acometida.getNombreTipoInstalacionAcometida()));
            builder.setCancelable(false);
            builder.setPositiveButton(R.string.btnHab_aceptar, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    System.out.println("Old");
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString(Util.activo,"false");
                    editor.apply();
                    acometida.setActivo(false);
                    acometidaDao.insertOrReplace(acometida);
                    Intent intent = new Intent(Acometida2Activity.this, BuscarAcometidaActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            builder.show();
        } else {
            if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                System.out.println("On");
                showProgressDialog(R.string.app_procesando);
                GrabarTuberias grabarTuberias = new GrabarTuberias(new GrabarTuberias.OnGrabarAcometidaAsyncTaskCompleted() {
                    @Override
                    public void onGrabarAcometidaAsyncTaskCompleted(ResponseInstalacionAcometida instalacionAcometidaOutRO) {
                        hideProgressDialog();
                        //tuberiasDao.deleteAll();
                        System.out.println("1w");
                        if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                            ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                            System.out.println("2w");
                        } else if (instalacionAcometidaOutRO == null) {
                            ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                            System.out.println("3w");
                        } else if (!instalacionAcometidaOutRO.getResultCode().equalsIgnoreCase(Constantes.RESULTADO_SUCCESS)) {
                            System.out.println("4w");
                            System.out.println("Restcode: " + instalacionAcometidaOutRO.getResultCode());
                            System.out.println("message: " + instalacionAcometidaOutRO.getMessage());
                            System.out.println("Errorcode: " + instalacionAcometidaOutRO.getErrorCode());
                            ((App) getApplication()).showToast(instalacionAcometidaOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : instalacionAcometidaOutRO.getMessage());
                        } else {
                            System.out.println("5w");
//                          new BorrarFoto().execute(suministro.getIdHabilitacion().toString());
                            AlertDialog.Builder builder = new AlertDialog.Builder(Acometida2Activity.this);
                            builder.setTitle(R.string.txtHab_mensajeConfirmacion);
                            builder.setMessage(String.format(getResources().getString(R.string.txtAco_descripcionRegistro), acometida.getNombreTipoInstalacionAcometida()));
                            builder.setCancelable(false);
                            builder.setPositiveButton(R.string.btnHab_aceptar, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    List<Observacion> observacions = observacionDao.queryBuilder().where(ObservacionDao.Properties.IdInstalacionMontante.eq(acometida.getIdInstalacionAcometida())).list();
                                    observacionDao.deleteInTx(observacions);
                                    List<Precision> precisiones = precisionDao.queryBuilder().where(PrecisionDao.Properties.Estado.eq("3"), PrecisionDao.Properties.IdSolicitud.eq(acometida.getIdInstalacionAcometida())).list();
                                    precisionDao.deleteInTx(precisiones);
                                    acometidaDao.delete(acometida);
                                    tuberiasDao.deleteAll();
                                    System.out.println("Old");
                                    SharedPreferences.Editor editor = pref.edit();
                                    editor.putString(Util.activo,"false");
                                    editor.apply();
                                    acometida.setActivo(false);
                                    acometidaDao.insertOrReplace(acometida);
                                    Intent intent = new Intent(Acometida2Activity.this, BuscarAcometidaActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                            builder.show();
                        }

                    }
                }, getApplicationContext(),tuberias);
                grabarTuberias.execute(acometida);
            } else {
                ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
            }
        }
    }

    private void grabarGabinete() {
        if (RESULTADO_INSTALACION_ACOMETIDA_CONCLUIDA.equalsIgnoreCase(acometida.getResultado())) {
            if (!"".equalsIgnoreCase(acometida.getFusionista())|| !"".equalsIgnoreCase(acometida.getFotoGabinete())) {
                System.out.println("1g: "+acometida.getFusionista() + " "+acometida.getFotoGabinete());
                btnAco_grabar.setEnabled(true);
                btnAco_grabar.setClickable(true);
                acometida.setGrabar(true);
                if (config.getOffline()) {

                    MInstalacionAcometidaOutRO mInstalacionAcometidaOutRO = mInstalacionAcometidaOutRODao.queryBuilder().where(MInstalacionAcometidaOutRODao.Properties.IdInstalacionAcometida.eq(acometida.getIdInstalacionAcometida())).limit(1).unique();
                    mInstalacionAcometidaOutRO.setResultCode("100");
                    acometida.setFechaAprobacion(FormatoFecha.getfecha());
                    acometida.setFechaRegistroOffline(FormatoFecha.getfecha());
                    mInstalacionAcometidaOutRODao.insertOrReplace(mInstalacionAcometidaOutRO);
                    acometidaDao.insertOrReplace(acometida);
                    AlertDialog.Builder builder = new AlertDialog.Builder(Acometida2Activity.this);
                    builder.setTitle(R.string.txtHab_mensajeConfirmacion);
                    builder.setMessage(String.format(getResources().getString(R.string.txtAco_descripcionRegistro), acometida.getNombreTipoInstalacionAcometida()));
                    builder.setCancelable(false);
                    builder.setPositiveButton(R.string.btnHab_aceptar, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            System.out.println("Old");
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString(Util.activo,"false");
                            editor.apply();
                            acometida.setActivo(false);
                            acometidaDao.insertOrReplace(acometida);
                            Intent intent = new Intent(Acometida2Activity.this, BuscarAcometidaActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });
                    builder.show();
                } else {
                    if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        showProgressDialog(R.string.app_procesando);
                        GrabarGabinete grabarTuberias = new GrabarGabinete(new GrabarGabinete.onGrabarGabineteAsyncTaskCompleted() {
                            @Override
                            public void onGrabarGabineteAsyncTaskCompleted(ResponseInstalacionAcometida instalacionAcometidaOutRO) {
                                hideProgressDialog();
                                //System.out.println("Restcode: " + instalacionAcometidaOutRO.getResultCode());
                                //System.out.println("message: " + instalacionAcometidaOutRO.getMessage());
                                //System.out.println("Errorcode: " + instalacionAcometidaOutRO.getErrorCode());
                                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                                } else if (instalacionAcometidaOutRO == null) {
                                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                                } else if (!instalacionAcometidaOutRO.getResultCode().equalsIgnoreCase(Constantes.RESULTADO_SUCCESS)) {
                                    System.out.println("Restcode: " + instalacionAcometidaOutRO.getResultCode());
                                    System.out.println("message: " + instalacionAcometidaOutRO.getMessage());
                                    System.out.println("Errorcode: " + instalacionAcometidaOutRO.getErrorCode());
                                    ((App) getApplication()).showToast(instalacionAcometidaOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : instalacionAcometidaOutRO.getMessage());
                                } else {
//                                      new BorrarFoto().execute(suministro.getIdHabilitacion().toString());
                                    AlertDialog.Builder builder = new AlertDialog.Builder(Acometida2Activity.this);
                                    builder.setTitle(R.string.txtHab_mensajeConfirmacion);
                                    builder.setMessage(String.format(getResources().getString(R.string.txtAco_descripcionRegistro), acometida.getNombreTipoInstalacionAcometida()));
                                    builder.setCancelable(false);
                                    builder.setPositiveButton(R.string.btnHab_aceptar, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            List<Observacion> observacions = observacionDao.queryBuilder()
                                                    .where(ObservacionDao.Properties.IdInstalacionMontante.eq(acometida.getIdInstalacionAcometida())).list();
                                            observacionDao.deleteInTx(observacions);
                                            List<Precision> precisiones = precisionDao.queryBuilder()
                                                    .where(PrecisionDao.Properties.Estado.eq("3"),
                                                            PrecisionDao.Properties.IdSolicitud.eq(acometida.getIdInstalacionAcometida())).list();
                                            precisionDao.deleteInTx(precisiones);
                                            acometidaDao.delete(acometida);
                                            acometidaNuevaDao.deleteAll();
                                            Intent intent = new Intent(Acometida2Activity.this, BuscarAcometidaActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                    });
                                    builder.show();
                                }

                            }
                        }, getApplicationContext(), acometidaNuevas);
                        grabarTuberias.execute(acometida);
                    } else {
                        ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                    }
                }

            } else {
                System.out.println("2g: ");
                btnAco_grabar.setEnabled(false);
                btnAco_grabar.setClickable(false);
            }
        }else{
            //acometida.setGrabar(true);
            if (config.getOffline()) {
                ///
            } else {
                if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    showProgressDialog(R.string.app_procesando);
                    GrabarGabinete grabarTuberias = new GrabarGabinete(new GrabarGabinete.onGrabarGabineteAsyncTaskCompleted() {
                        @Override
                        public void onGrabarGabineteAsyncTaskCompleted(ResponseInstalacionAcometida instalacionAcometidaOutRO) {
                            hideProgressDialog();
                            //System.out.println("Restcode: " + instalacionAcometidaOutRO.getResultCode());
                            //System.out.println("message: " + instalacionAcometidaOutRO.getMessage());
                            //System.out.println("Errorcode: " + instalacionAcometidaOutRO.getErrorCode());
                            if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                            } else if (instalacionAcometidaOutRO == null) {
                                ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                            } else if (!instalacionAcometidaOutRO.getResultCode().equalsIgnoreCase(Constantes.RESULTADO_SUCCESS)) {
                                System.out.println("Restcode: " + instalacionAcometidaOutRO.getResultCode());
                                System.out.println("message: " + instalacionAcometidaOutRO.getMessage());
                                System.out.println("Errorcode: " + instalacionAcometidaOutRO.getErrorCode());
                                ((App) getApplication()).showToast(instalacionAcometidaOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : instalacionAcometidaOutRO.getMessage());
                            } else {
//                                      new BorrarFoto().execute(suministro.getIdHabilitacion().toString());
                                AlertDialog.Builder builder = new AlertDialog.Builder(Acometida2Activity.this);
                                builder.setTitle(R.string.txtHab_mensajeConfirmacion);
                                builder.setMessage(String.format(getResources().getString(R.string.txtAco_descripcionRegistro), acometida.getNombreTipoInstalacionAcometida()));
                                builder.setCancelable(false);
                                builder.setPositiveButton(R.string.btnHab_aceptar, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        List<Observacion> observacions = observacionDao.queryBuilder()
                                                .where(ObservacionDao.Properties.IdInstalacionMontante.eq(acometida.getIdInstalacionAcometida())).list();
                                        observacionDao.deleteInTx(observacions);
                                        List<Precision> precisiones = precisionDao.queryBuilder()
                                                .where(PrecisionDao.Properties.Estado.eq("3"),
                                                        PrecisionDao.Properties.IdSolicitud.eq(acometida.getIdInstalacionAcometida())).list();
                                        precisionDao.deleteInTx(precisiones);
                                        acometidaDao.delete(acometida);
                                        Intent intent = new Intent(Acometida2Activity.this, BuscarAcometidaActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                                builder.show();
                            }

                        }
                    }, getApplicationContext(), acometidaNuevas);
                    grabarTuberias.execute(acometida);
                } else {
                    ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                }
            }
        }

    }

    private void grabarAcometida2() {
        List<Tuberias> tub = tuberiasDao.queryBuilder().where(TuberiasDao.Properties.Id.eq(acometida.getId())).list();
        for(int i=0; i < tub.size(); i++) {
            System.out.println(tub.get(i).getLongitud());
            System.out.println(tub.get(i).getResultado());
            System.out.println(tub.get(i).getFotoTC());
        }
    }

    private void validarCheck() {
        boolean grabar = true;
        if ("".equalsIgnoreCase(acometida.getFusionista())) {
            icoAco_fusionista.setImageResource(R.mipmap.ic_warning_black_36dp);
            icoAco_fusionistagabinete.setImageResource(R.mipmap.ic_warning_black_36dp);

            grabar = false;
        } else {
            icoAco_fusionista.setImageResource(R.mipmap.ic_done_black_36dp);
            icoAco_fusionistagabinete.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        /*PAPU 2011 - 0 */
        /*if (acometida.getNombreTipoInstalacionAcometida().equalsIgnoreCase("acometida")){
            if ("".equalsIgnoreCase(acometida.getTipoGabinete())) {
                icoAco_tipogabinete.setImageResource(R.mipmap.ic_warning_black_36dp);
                grabar = false;
            } else {
                icoAco_tipogabinete.setImageResource(R.mipmap.ic_done_black_36dp);
            }
        }*/
        /*PAPU 2011 - 1*/

        if ("".equalsIgnoreCase(acometida.getResultado())) {
            icoAco_resultado.setImageResource(R.mipmap.ic_warning_black_36dp);
            icoAco_resultadogabinete.setImageResource(R.mipmap.ic_warning_black_36dp);
            grabar = false;
        } else {
            icoAco_resultado.setImageResource(R.mipmap.ic_done_black_36dp);
            icoAco_resultadogabinete.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if (RESULTADO_INSTALACION_ACOMETIDA_CONCLUIDA.equalsIgnoreCase(acometida.getResultado())) {
            if (TIPO_INSTALACION_ACOMETIDA_ACOMETIDA.equalsIgnoreCase(acometida.getTipoInstalacionAcometida())) {
                if ("".equalsIgnoreCase(acometida.getTipoGabinete())) {
                    icoAco_tipogabinete.setImageResource(R.mipmap.ic_warning_black_36dp);
                    grabar = false;
                } else {
                    icoAco_tipogabinete.setImageResource(R.mipmap.ic_done_black_36dp);
                }
                if ("".equalsIgnoreCase(acometida.getFotoActa())) {
                    icoAco_acta.setImageResource(R.mipmap.ic_warning_black_36dp);
                    grabar = false;
                } else {
                    icoAco_acta.setImageResource(R.mipmap.ic_done_black_36dp);
                }
                if ("".equalsIgnoreCase(acometida.getFotoGabinete())) {
                    icoAco_gabinete.setImageResource(R.mipmap.ic_warning_black_36dp);
                    grabar = false;
                } else {
                    icoAco_gabinete.setImageResource(R.mipmap.ic_done_black_36dp);
                }
            } else if (TIPO_INSTALACION_ACOMETIDA_TC.equalsIgnoreCase(acometida.getTipoInstalacionAcometida())) {
                if ("".equalsIgnoreCase(acometida.getDiametro())) {
                    icoAco_diametro.setImageResource(R.mipmap.ic_warning_black_36dp);
                    grabar = false;
                } else {
                    icoAco_diametro.setImageResource(R.mipmap.ic_done_black_36dp);
                }
                if ("".equalsIgnoreCase(acometida.getLongitud())) {
                    icoAco_longitud.setImageResource(R.mipmap.ic_warning_black_36dp);
                    grabar = false;
                } else {
                    icoAco_longitud.setImageResource(R.mipmap.ic_done_black_36dp);
                }
                if ("".equalsIgnoreCase(acometida.getMaterial())) {
                    icoAco_material.setImageResource(R.mipmap.ic_warning_black_36dp);
                    grabar = false;
                } else {
                    icoAco_material.setImageResource(R.mipmap.ic_done_black_36dp);
                }
                if ("".equalsIgnoreCase(acometida.getFotoTC())) {
                    icoAco_tc.setImageResource(R.mipmap.ic_warning_black_36dp);
                    grabar = false;
                } else {
                    icoAco_tc.setImageResource(R.mipmap.ic_done_black_36dp);
                }
            }
        } else {
            if (observacionAdaptador == null || !observacionAdaptador.getValidado()) {
                grabar = false;
            }
        }
        btnAco_grabar.setEnabled(grabar);
    }

    @Override
    public void clicFoto(int position) {
        System.out.println("click foto: " + position);
        if (ActivityCompat.checkSelfPermission(Acometida2Activity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ((App) getApplication()).showToast("No puede realizar la acción porque no se ha dado el permiso de Almacenamiento");
            return;
        }
        if (Util.isExternalStorageWritable()) {
            if (Util.getAlbumStorageDir(Acometida2Activity.this)) {
                File file = Util.getNuevaRutaFoto("0",Acometida2Activity.this);
                tmpRuta = file.getPath();
                this.position = position;
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                Uri outputUri = FileProvider.getUriForFile(Acometida2Activity.this, getApplicationContext().getPackageName() + ".provider", file);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    ClipData clip = ClipData.newUri(getContentResolver(), "Instalacion", outputUri);
                    intent.setClipData(clip);
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                }
                startActivityForResult(intent, 1);
            } else {
                ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
            }
        } else {
            ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
        }
    }

    @Override
    public void clicFoto2(int position) {
        System.out.println("click foto2: " + position);
        if (ActivityCompat.checkSelfPermission(Acometida2Activity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ((App) getApplication()).showToast("No puede realizar la acción porque no se ha dado el permiso de Almacenamiento");
            return;
        }
        if (Util.isExternalStorageWritable()) {
            if (Util.getAlbumStorageDir(Acometida2Activity.this)) {
                File file = Util.generarRutaParaAcometidasOGabinetes(position);
                tmpRuta = file.getPath();
                this.position = position;
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                Uri outputUri = FileProvider.getUriForFile(Acometida2Activity.this, getApplicationContext().getPackageName() + ".provider", file);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    ClipData clip = ClipData.newUri(getContentResolver(), "Instalacion", outputUri);
                    intent.setClipData(clip);
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                }
                startActivityForResult(intent, 5);
            } else {
                ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
            }
        } else {
            ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
        }
    }

    @Override
    public void cambiarVista(int position) {
        int pos = position-1;
        System.out.println("Estoy cambiando de vista  rechazada/pendiente: "+pos);
        List<Tuberias> tub = tuberiasDao.queryBuilder().list();
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Util.activo,"true");
        editor.apply();
        acometida.setActivo(true);
        acometida.setFusionista(tub.get(pos).getFusionista());
        acometida.setResultado(tub.get(pos).getResultado());
        acometidaDao.update(acometida);
        btnAco_grabar.setEnabled(true);
        switch (acometida.getResultado()) {
            /*case RESULTADO_INSTALACION_ACOMETIDA_CONCLUIDA:
                if(pref.getString(Util.activo,"").equals("true")){
                    lynuevo.setVisibility(View.VISIBLE);
                    lyantiguo.setVisibility(View.GONE);
                    mostrarLista();
                    SharedPreferences.Editor editor2 = pref.edit();
                    editor2.putString(Util.activo,"false");
                    editor2.apply();
                    System.out.println("Estoy cambiando de vista concluida");
                }else{
                    concluido.setVisibility(View.VISIBLE);
                    ll_observaciones.setVisibility(View.GONE);
                    switch (acometida.getTipoInstalacionAcometida()) {
                        case TIPO_INSTALACION_ACOMETIDA_ACOMETIDA:
                            llAcometida.setVisibility(View.VISIBLE);
                            llTc.setVisibility(View.GONE);
                            break;
                        case TIPO_INSTALACION_ACOMETIDA_TC:
                            llAcometida.setVisibility(View.GONE);
                            llTc.setVisibility(View.VISIBLE);
                            break;
                        default:
                            llAcometida.setVisibility(View.GONE);
                            llTc.setVisibility(View.GONE);
                    }
                    validarCheck();
                }
                break;*/
            case RESULTADO_INSTALACION_ACOMETIDA_PENDIENTE:
            case RESULTADO_INSTALACION_ACOMETIDA_RECHAZADA:
                concluido.setVisibility(View.GONE);
                ll_observaciones.setVisibility(View.VISIBLE);
                List<Observacion> a = observacionDao.queryBuilder().where(ObservacionDao.Properties.IdInstalacionMontante.eq(acometida.getIdInstalacionAcometida())).list();
                observacionDao.deleteInTx(a);
                if (config.getOffline()) {
                    if (RESULTADO_INSTALACION_ACOMETIDA_PENDIENTE.equalsIgnoreCase(acometida.getResultado())) {
                        listatipoObse.clear();
                        List<TipoPendiente> tipoPendientes = tipoPendienteDao.queryBuilder().list();
                        for (TipoPendiente tipoPendiente : tipoPendientes) {
                            listatipoObse.add(Parse.getGenericBeanOutRO(tipoPendiente));
                        }
                        configurarObs();
                    } else {
                        listatipoObse.clear();
                        List<TipoRechazada> tipoRechazadas = tipoRechazadaDao.queryBuilder().list();
                        for (TipoRechazada tipoRechazada : tipoRechazadas) {
                            listatipoObse.add(Parse.getGenericBeanOutRO(tipoRechazada));
                        }
                        configurarObs();
                    }
                } else {
                    callTiposObservacion();
                }
                break;
            default:
                concluido.setVisibility(View.GONE);
                ll_observaciones.setVisibility(View.GONE);
        }
        List<Acometida> ac = acometidaDao.queryBuilder().where(AcometidaDao.Properties.IdInstalacionAcometida.eq(acometida.getIdInstalacionAcometida())).list();
        System.out.println("AC2: "+ac.get(0).getActivo() + " ID2: "+ac.get(0).getIdInstalacionAcometida());

        lynuevo.setVisibility(View.GONE);
        lyantiguo.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1458) {
            if (resultCode == RESULT_CANCELED) {
                Intent intent = new Intent(Acometida2Activity.this, BuscarAcometidaActivity.class);
                startActivity(intent);
                finish();
            }
        }

        if (requestCode == 1) {
            if (resultCode != RESULT_CANCELED) {
                ((TubyTcAdaptador) listaTuberias.getAdapter()).getAcometida(position).setFotoTC(tmpRuta);
                ReducirFotoTask reducirFotoTask = new ReducirFotoTask(new ReducirFotoTask.OnReducirFotoTaskCompleted() {
                    @Override
                    public void onReducirFotoTaskCompleted(String result) {
                        if (!("1".equalsIgnoreCase(result))) {
                            ((App) getApplication()).showToast(result);
                        }
                    }
                });
                tuberiasDao.insertOrReplace(((TubyTcAdaptador) listaTuberias.getAdapter()).getAcometida(position));
                reducirFotoTask.execute(((TubyTcAdaptador) listaTuberias.getAdapter()).getAcometida(position).getFotoTC(), config.getParametro5());
                ((TubyTcAdaptador) listaTuberias.getAdapter()).notifyDataSetChanged();
            }
        }
        if (requestCode == 2) {
            if (resultCode != RESULT_CANCELED) {
                acometida.setFotoActa(tmpRuta);
                ReducirFotoTask reducirFotoTask = new ReducirFotoTask(new ReducirFotoTask.OnReducirFotoTaskCompleted() {
                    @Override
                    public void onReducirFotoTaskCompleted(String result) {
                        if (!("1".equalsIgnoreCase(result))) {
                            ((App) getApplication()).showToast(result);
                        }
                    }
                });
                acometidaDao.insertOrReplace(acometida);
                reducirFotoTask.execute(acometida.getFotoActa(), config.getParametro5());
            }
        }
        if (requestCode == 3) {
            if (resultCode != RESULT_CANCELED) {
                acometida.setFotoTC(tmpRuta);
                ReducirFotoTask reducirFotoTask = new ReducirFotoTask(new ReducirFotoTask.OnReducirFotoTaskCompleted() {
                    @Override
                    public void onReducirFotoTaskCompleted(String result) {
                        if (!("1".equalsIgnoreCase(result))) {
                            ((App) getApplication()).showToast(result);
                        }
                    }
                });
                acometidaDao.insertOrReplace(acometida);
                reducirFotoTask.execute(acometida.getFotoTC(), config.getParametro5());
            }
        }
        if (requestCode == 4) {
            if (resultCode != RESULT_CANCELED) {
                acometida.setFotoGabinete(tmpRuta);
                ReducirFotoTask reducirFotoTask = new ReducirFotoTask(new ReducirFotoTask.OnReducirFotoTaskCompleted() {
                    @Override
                    public void onReducirFotoTaskCompleted(String result) {
                        if (!("1".equalsIgnoreCase(result))) {
                            ((App) getApplication()).showToast(result);
                        }
                    }
                });
                acometidaDao.insertOrReplace(acometida);
                reducirFotoTask.execute(acometida.getFotoGabinete(), config.getParametro5());
            }
        }
        if (requestCode == 5) {
            if (resultCode != RESULT_CANCELED) {
                ((AcometidaNewAdaptador) recy_acometidas.getAdapter()).getAcometidanueva(position).setFotoGabinete(tmpRuta);
                ReducirFotoTask reducirFotoTask = new ReducirFotoTask(new ReducirFotoTask.OnReducirFotoTaskCompleted() {
                    @Override
                    public void onReducirFotoTaskCompleted(String result) {
                        if (!("1".equalsIgnoreCase(result))) {
                            ((App) getApplication()).showToast(result);
                        }
                    }
                });
                System.out.println("RUTA: " + tmpRuta);
                acometidaNuevaDao.insertOrReplace(((AcometidaNewAdaptador) recy_acometidas.getAdapter()).getAcometidanueva(position));
                reducirFotoTask.execute(((AcometidaNewAdaptador) recy_acometidas.getAdapter()).getAcometidanueva(position).getFotoGabinete(), config.getParametro5());
                ((AcometidaNewAdaptador) recy_acometidas.getAdapter()).notifyDataSetChanged();
            }
        }
        if (requestCode == 6) {
            if (resultCode != RESULT_CANCELED) {
                acometida.setFotoGabinete(tmpRuta);
                ReducirFotoTask reducirFotoTask = new ReducirFotoTask(new ReducirFotoTask.OnReducirFotoTaskCompleted() {
                    @Override
                    public void onReducirFotoTaskCompleted(String result) {
                        if (!("1".equalsIgnoreCase(result))) {
                            ((App) getApplication()).showToast(result);
                        }
                    }
                });
                acometidaDao.insertOrReplace(acometida);
                reducirFotoTask.execute(acometida.getFotoGabinete(), config.getParametro5());
                if (!"".equalsIgnoreCase(acometida.getFotoGabinete())) {
                    loadBitmap(acometida.getFotoGabinete(), imgAco_actagabinete);
                }
                if ("".equalsIgnoreCase(acometida.getFotoGabinete())) {
                    icoAco_actagabinate.setImageResource(R.mipmap.ic_warning_black_36dp);
                } else {
                    icoAco_actagabinate.setImageResource(R.mipmap.ic_done_black_36dp);
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.createAndStart();
        createLocationRequest();
        toolbar.setTitle(acometida.getSuministro());
        cargarAcometida();
        validarCheck();
        txtTub_tuberia.setText(acometida.getNombreTipoInstalacionAcometida());
        if (config.getOffline()) {
            listaTipoFusionista.clear();
            List<Fusionista> tipoFusionistas = fusionistaDao.queryBuilder().list();
            for (Fusionista fusionista : tipoFusionistas) {
                listaTipoFusionista.add(Parse.getBaseInspectorOutRO(fusionista));
            }
            ArrayList<String> list = new ArrayList<>();
            list.add("Seleccione fusionista / técnico instalador");
            for (BaseInspectorOutRO baseInspectorOutRO : listaTipoFusionista) {
                list.add(baseInspectorOutRO.getNombreInspector());
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<>(Acometida2Activity.this, android.R.layout.simple_spinner_item, list);
            adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            spiAco_fusionista.setAdapter(adapter);
            spiAco_fusionista.setTitle("Ingrese datos del fusionista / técnico instalador");
            spiAco_fusionista.setPositiveButton("Cerrar");
            spiAco_fusionista.setTag(0);
            spiAco_fusionista.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i == (int) spiAco_fusionista.getTag()) {
                        return;
                    }else{
                        spiAco_fusionista.setTag(i);
                        if (i != 0) {
                            acometida.setFusionista(listaTipoFusionista.get(i - 1).getIdInspector().toString());
                        } else {
                            acometida.setFusionista("");
                        }
                        validarCheck();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });


            spiAco_fusionistagabinete.setAdapter(adapter);
            spiAco_fusionistagabinete.setTitle("Ingrese datos del fusionista / técnico instalador");
            spiAco_fusionistagabinete.setPositiveButton("Cerrar");
            spiAco_fusionistagabinete.setTag(0);
            spiAco_fusionistagabinete.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i == (int) spiAco_fusionistagabinete.getTag()) {
                        return;
                    } else {
                        spiAco_fusionistagabinete.setTag(i);
                        if (i != 0) {
                            acometida.setFusionista(listaTipoFusionista.get(i - 1).getIdInspector().toString());
                        } else {
                            acometida.setFusionista("");
                        }
                        validarCheck();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            listaTipoResultado.clear();
            List<TipoResultado> resultados = tipoResultadoDao.queryBuilder().list();
            for (TipoResultado tipoResultado : resultados) {
                listaTipoResultado.add(Parse.getGenericBeanOutRO(tipoResultado));
            }
            ArrayList<String> list2 = new ArrayList<>();
            list2.add("Seleccione resultado");
            for (GenericBeanOutRO genericBeanOutRO : listaTipoResultado) {
                Log.i("TAG RES",genericBeanOutRO.getLabel());
                list2.add(genericBeanOutRO.getLabel());
            }
            ArrayAdapter<String> adapter2 = new ArrayAdapter<>(Acometida2Activity.this, android.R.layout.simple_spinner_item, list2);
            adapter2.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            spiAco_resultado.setAdapter(adapter2);
            spiAco_resultadogabinete.setAdapter(adapter2);

            /*PAPU 2011 - 0*/
            listaTipoGabinete.clear();
            List<TipoGabinete> tipoGabinetes = tipoGabineteDao.queryBuilder().list();
            for (TipoGabinete tipoGabinete : tipoGabinetes) {
                listaTipoGabinete.add(Parse.getGenericBeanOutRO(tipoGabinete));
            }
            ArrayList<String> list21 = new ArrayList<>();
            list2.add("Seleccione tipo de gabinete");
            for (GenericBeanOutRO genericBeanOutRO : listaTipoGabinete) {
                Log.i("TAG TGAB",genericBeanOutRO.getLabel());
                list21.add(genericBeanOutRO.getLabel());
            }
            ArrayAdapter<String> adapter21 = new ArrayAdapter<>(Acometida2Activity.this, android.R.layout.simple_spinner_item, list21);
            adapter21.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            spiAco_tipogabinete.setAdapter(adapter21);
            /*PAPU 2011 - 1*/

            listaTipoMaterial.clear();
            List<MaterialInstalacion> materialInstalacions = materialInstalacionDao.queryBuilder().list();
            for (MaterialInstalacion materialInstalacion : materialInstalacions) {
                listaTipoMaterial.add(Parse.getGenericBeanOutRO(materialInstalacion));
            }
            ArrayList<String> list3 = new ArrayList<>();
            list3.add("Seleccione material");
            for (GenericBeanOutRO genericBeanOutRO : listaTipoMaterial) {
                list3.add(genericBeanOutRO.getLabel());
            }
            ArrayAdapter<String> adapter3 = new ArrayAdapter<>(Acometida2Activity.this, android.R.layout.simple_spinner_item, list3);
            adapter3.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            spiAco_material.setAdapter(adapter3);
            MParametroOutRO mParametroOutRO = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_CONFIGURACION_COMPRESION_FOTOS_APLICATIVO_MOVIL))
                    .limit(1)
                    .unique();
            config.setParametro5(mParametroOutRO == null ? "800x600" : mParametroOutRO.getValorParametro());
            MParametroOutRO mParametroOutPrecision = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_FRECUENCIA_TOMA_COORDENADAS_AUDITORIA))
                    .limit(1).unique();
            config.setPrecision(mParametroOutPrecision == null ? "10000" : mParametroOutPrecision.getValorParametro());
            cargaDatos();
        }
        else {
            if (listaTipoFusionista.size() < 1) {
                if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    showProgressDialog(R.string.app_cargando);
                    TipoFusionista tipoFusionista = new TipoFusionista(new TipoFusionista.OnTipoFusionistaCompleted() {
                        @Override
                        public void onTipoFusionistaCompled(ListBaseInspectorOutRO listBaseInspectorOutRO) {
                            if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                hideProgressDialog();
                                ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                            } else if (listBaseInspectorOutRO == null) {
                                hideProgressDialog();
                                ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                            } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listBaseInspectorOutRO.getResultCode())) {
                                hideProgressDialog();
                                ((App) getApplication()).showToast(listBaseInspectorOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listBaseInspectorOutRO.getMessage());
                            } else {
                                listaTipoFusionista = listBaseInspectorOutRO.getInspector();
                                ArrayList<String> list = new ArrayList<>();
                                list.add("Seleccione fusionista / técnico instalador");
                                for (BaseInspectorOutRO genericBeanOutRO : listaTipoFusionista) {
                                    list.add(genericBeanOutRO.getNombreInspector());
                                }

                                ArrayAdapter<String> adapter = new ArrayAdapter<>(Acometida2Activity.this, android.R.layout.simple_spinner_item, list);
                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                                spiAco_fusionista.setAdapter(adapter);
                                spiAco_fusionista.setTitle("Ingrese datos del fusionista / técnico instalador");
                                spiAco_fusionista.setPositiveButton("Cerrar");
                                spiAco_fusionista.setTag(0);
                                spiAco_fusionista.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        if (i == (int) spiAco_fusionista.getTag()) {
                                            return;
                                        } else {
                                            spiAco_fusionista.setTag(i);
                                            if (i != 0) {
                                                acometida.setFusionista(listaTipoFusionista.get(i - 1).getIdInspector().toString());
                                            } else {
                                                acometida.setFusionista("");
                                            }
                                            validarCheck();
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });


                                spiAco_fusionistagabinete.setAdapter(adapter);
                                spiAco_fusionistagabinete.setTitle("Ingrese datos del fusionista / técnico instalador");
                                spiAco_fusionistagabinete.setPositiveButton("Cerrar");
                                spiAco_fusionistagabinete.setTag(0);
                                spiAco_fusionistagabinete.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        if (i == (int) spiAco_fusionistagabinete.getTag()) {
                                            return;
                                        } else {
                                            spiAco_fusionistagabinete.setTag(i);
                                            if (i != 0) {
                                                acometida.setFusionista(listaTipoFusionista.get(i - 1).getIdInspector().toString());
                                            } else {
                                                acometida.setFusionista("");
                                            }
                                            validarCheck();
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });
                                /*PAPU 2011 - 0*/
                                if (listaTipoGabinete.size() < 1) {
                                    TipoGabineteAcometida tipoGabinete = new TipoGabineteAcometida(new TipoGabineteAcometida.OnTipoGabineteCompleted() {
                                        @Override
                                        public void OnTipoGabineteCompleted(ListGenericBeanOutRO listGenericBeanOutRO) {
                                            if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                                hideProgressDialog();
                                                ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                                            } else if (listGenericBeanOutRO == null) {
                                                hideProgressDialog();
                                                ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                                            } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                                                hideProgressDialog();
                                                ((App) getApplication()).showToast(listGenericBeanOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listGenericBeanOutRO.getMessage());
                                            } else {
                                                listaTipoGabinete = listGenericBeanOutRO.getGenericBean();
                                                ArrayList<String> list = new ArrayList<>();
                                                list.add("Seleccione tipo de gabinete");
                                                for (GenericBeanOutRO genericBeanOutRO : listaTipoGabinete) {
                                                    list.add(genericBeanOutRO.getLabel());
                                                }

                                                ArrayAdapter<String> adapter = new ArrayAdapter<>(Acometida2Activity.this, android.R.layout.simple_spinner_item, list);
                                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                                                spiAco_tipogabinete.setAdapter(adapter);
                                            }
                                        }
                                    });
                                    tipoGabinete.execute(config);
                                }
                                /*PAPU 2011 - 1*/

                                if (listaTipoResultado.size() < 1) {
                                    TipoResultadoAcometida tipoResultadoAcometida = new TipoResultadoAcometida(new TipoResultadoAcometida.OnTipoResultadoCompleted() {
                                        @Override
                                        public void onTipoResultadoCompled(ListGenericBeanOutRO listGenericBeanOutRO) {
                                            if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                                hideProgressDialog();
                                                ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                                            } else if (listGenericBeanOutRO == null) {
                                                hideProgressDialog();
                                                ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                                            } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                                                hideProgressDialog();
                                                ((App) getApplication()).showToast(listGenericBeanOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listGenericBeanOutRO.getMessage());
                                            } else {
                                                listaTipoResultado = listGenericBeanOutRO.getGenericBean();
                                                ArrayList<String> list = new ArrayList<>();
                                                list.add("Seleccione resultadoo");

                                                for (GenericBeanOutRO genericBeanOutRO : listaTipoResultado) {
                                                    list.add(genericBeanOutRO.getLabel());
                                                }

                                                ArrayAdapter<String> adapter = new ArrayAdapter<>(Acometida2Activity.this, android.R.layout.simple_spinner_item, list);
                                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                                                spiAco_resultado.setAdapter(adapter);
                                                spiAco_resultadogabinete.setAdapter(adapter);

                                                if (listaTipoMaterial.size() < 1) {
                                                    if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                                        TipoMaterialInstalacion tipoMaterialInstalacion = new TipoMaterialInstalacion(new TipoMaterialInstalacion.OnTipoMaterialInstalacionCompleted() {
                                                            @Override
                                                            public void onTipoMaterialInstalacionCompled(ListGenericBeanOutRO listGenericBeanOutRO) {
                                                                hideProgressDialog();
                                                                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                                                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                                                                } else if (listGenericBeanOutRO == null) {
                                                                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                                                                } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                                                                    ((App) getApplication()).showToast(listGenericBeanOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listGenericBeanOutRO.getMessage());
                                                                } else {
                                                                    listaTipoMaterial = listGenericBeanOutRO.getGenericBean();
                                                                    ArrayList<String> list = new ArrayList<>();
                                                                    list.add("Seleccione material");
                                                                    for (GenericBeanOutRO genericBeanOutRO : listaTipoMaterial) {
                                                                        list.add(genericBeanOutRO.getLabel());
                                                                    }
                                                                    ArrayAdapter<String> adapter = new ArrayAdapter<>(Acometida2Activity.this, android.R.layout.simple_spinner_item, list);
                                                                    adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                                                                    spiAco_material.setAdapter(adapter);
                                                                    if ("".equalsIgnoreCase(config.getParametro5())) {
                                                                        config.setParametro5("800x600");
                                                                    }
                                                                    if ("".equalsIgnoreCase(config.getPrecision())) {
                                                                        config.setPrecision("10000");
                                                                    }
                                                                    cargaDatos();
                                                                }
                                                            }
                                                        });
                                                        tipoMaterialInstalacion.execute(config);
                                                    } else {
                                                        ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                                                    }
                                                }
                                            }
                                        }
                                    });
                                    tipoResultadoAcometida.execute(config);
                                }

                                List<Acometida> ac = acometidaDao.queryBuilder().where(AcometidaDao.Properties.IdInstalacionAcometida.eq(acometida.getIdInstalacionAcometida())).list();
                                System.out.println("AC: "+ac.get(0).getActivo() + " ID: "+ac.get(0).getIdInstalacionAcometida() + " IDACOM: " +acometida.getIdInstalacionAcometida());

                                if(ac.get(0).getActivo() == true){
                                    if (!"".equalsIgnoreCase(ac.get(0).getResultado())) {
                                        spiAco_resultado.setSelection(getTipoResultado(ac.get(0).getResultado()));
                                    }

                                    if (!"".equalsIgnoreCase(ac.get(0).getFusionista())) {
                                        spiAco_fusionista.setSelection(getFusionista(ac.get(0).getFusionista()));
                                    }
                                }
                            }
                        }
                    });
                    tipoFusionista.execute(config);
                } else {
                    ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                }
            } else {
                cargaDatos();
            }
        }
    }

    @Override
    public void onValidar() {
        validarCheck();
    }

    private void cargaDatos() {
        if (!"".equalsIgnoreCase(acometida.getFusionista())) {
            int i = getFusionista(acometida.getFusionista());
            spiAco_fusionista.setTag(i);
            spiAco_fusionista.setSelection(i);

            spiAco_fusionistagabinete.setTag(i);
            spiAco_fusionistagabinete.setSelection(i);

        }
        if (!"".equalsIgnoreCase(acometida.getResultado())) {
            int i = getTipoResultado(acometida.getResultado());
            spiAco_resultado.setTag(i);
            spiAco_resultado.setSelection(i);
            System.out.println("TAGREST:"+i);
            spiAco_resultadogabinete.setTag(i);
            spiAco_resultadogabinete.setSelection(i);

            switch (acometida.getResultado()) {
                case RESULTADO_INSTALACION_ACOMETIDA_CONCLUIDA:
                    concluido.setVisibility(View.VISIBLE);
                    ll_observaciones.setVisibility(View.GONE);
                    switch (acometida.getTipoInstalacionAcometida()) {
                        case TIPO_INSTALACION_ACOMETIDA_ACOMETIDA:
                            llAcometida.setVisibility(View.VISIBLE);
                            llTc.setVisibility(View.GONE);
                            break;
                        case TIPO_INSTALACION_ACOMETIDA_TC:
                            llAcometida.setVisibility(View.GONE);
                            llTc.setVisibility(View.VISIBLE);
                            break;
                        default:
                            llAcometida.setVisibility(View.GONE);
                            llTc.setVisibility(View.GONE);
                    }
                    break;
                case RESULTADO_INSTALACION_ACOMETIDA_PENDIENTE:
                case RESULTADO_INSTALACION_ACOMETIDA_RECHAZADA:
                    concluido.setVisibility(View.GONE);
                    ll_observaciones.setVisibility(View.VISIBLE);
                    if (config.getOffline()) {
                        if (RESULTADO_INSTALACION_ACOMETIDA_PENDIENTE.equalsIgnoreCase(acometida.getResultado())) {
                            listatipoObse.clear();
                            List<TipoPendiente> tipoPendientes = tipoPendienteDao.queryBuilder().list();
                            for (TipoPendiente tipoPendiente : tipoPendientes) {
                                listatipoObse.add(Parse.getGenericBeanOutRO(tipoPendiente));
                            }
                            configurarObs();
                        } else {
                            listatipoObse.clear();
                            List<TipoRechazada> tipoRechazadas = tipoRechazadaDao.queryBuilder().list();
                            for (TipoRechazada tipoRechazada : tipoRechazadas) {
                                listatipoObse.add(Parse.getGenericBeanOutRO(tipoRechazada));
                            }
                            configurarObs();
                        }
                    } else {
                        callTiposObservacion();
                    }
                    break;
                default:
                    concluido.setVisibility(View.GONE);
                    ll_observaciones.setVisibility(View.GONE);
            }
        }
        if (!"".equalsIgnoreCase(acometida.getDiametro())) {
            ediAco_diametro.getEditText().setText(acometida.getDiametro());
            ediAco_diametro.getEditText().setSelection(ediAco_diametro.getEditText().getText().length());
        }
        if (!"".equalsIgnoreCase(acometida.getLongitud())) {
            ediAco_longitud.getEditText().setText(acometida.getLongitud());
            ediAco_longitud.getEditText().setSelection(ediAco_longitud.getEditText().getText().length());
        }
        if (!"".equalsIgnoreCase(acometida.getMaterial())) {
            int i = getTipoMaterial(acometida.getMaterial());
            spiAco_material.setTag(i);
            spiAco_material.setSelection(i);
        }
        if (!"".equalsIgnoreCase(acometida.getFotoActa())) {
            loadBitmap(acometida.getFotoActa(), imgAco_acta);
        }
        if (!"".equalsIgnoreCase(acometida.getFotoTC())) {
            loadBitmap(acometida.getFotoTC(), imgAco_tc);
        }
        if (!"".equalsIgnoreCase(acometida.getFotoGabinete())) {
            loadBitmap(acometida.getFotoGabinete(), imgAco_gabinete);
        }

        /*PAPU 2011 - 0*/
        boolean esAcometida = acometida.getNombreTipoInstalacionAcometida().equalsIgnoreCase("acometida");
        if (esAcometida){
            spiAco_tipogabinete.setVisibility( View.VISIBLE);
            icoAco_tipogabinete.setVisibility( View.VISIBLE);
            txtAco_tipogabinete.setVisibility( View.VISIBLE);

            if (!"".equalsIgnoreCase(acometida.getTipoGabinete())) {
                int i = getTipoGabinete(acometida.getTipoGabinete());
                spiAco_tipogabinete.setTag(i);
                spiAco_tipogabinete.setSelection(i);
            }
        } else {
            spiAco_tipogabinete.setVisibility( View.GONE);
            icoAco_tipogabinete.setVisibility( View.GONE);
            txtAco_tipogabinete.setVisibility( View.GONE);
        }
        /*PAPU 2011 - 1*/
    }

    private void cargarAcometida() {
        txtDet_DocumentoPropietarioView.setText(acometida.getDocumentoPropietario());
        txtDet_nombrePropietarioView.setText(acometida.getNombrePropietario());
        txtDet_PredioView.setText(acometida.getPredio());
        txtDetInstalacionView.setText(acometida.getNombreTipoInstalacionAcometida());
        txtDet_numeroIntHabilitacionView.setText(acometida.getNumeroIntHabilitacion());
        txtDet_numeroSolicitudView.setText(acometida.getNumeroSolicitud());
        txtDet_numeroContratoView.setText(acometida.getNumeroContrato());
        txtDet_fechaSolicitudView.setText(acometida.getFechaSolicitud());
        txtDet_fechaAprobacionView.setText(acometida.getFechaAprobacion());
        txtDet_tipoProyectoView.setText(acometida.getTipoProyecto());
        txtDet_tipoInstalacionView.setText(acometida.getTipoInstalacion());
        txtDet_numeroPuntosInstaacionView.setText(acometida.getNumeroPuntosInstaacion());
    }

    private boolean quitarEspacios() {
        int i = 0;
        boolean entro = true;
        for (Observacion observacion : observaciones) {
            observaciones.get(i).setDescripcionObservacion(observacion.getDescripcionObservacion().trim());
            if (observaciones.get(i).getDescripcionObservacion().equalsIgnoreCase("")) {
                entro = false;
            }
            i++;
        }
        observacionAdaptador.notifyDataSetChanged();
        return entro;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_general, menu);
        menuMas = menu.findItem(R.id.menuMas);
        menuMas.setVisible(true);
        menuMenos = menu.findItem(R.id.menuMenos);
        menuMenos.setVisible(false);
        return true;
    }

    private String setTipoResultado(String tipoResultado) {
        for (GenericBeanOutRO genericBeanOutRO : listaTipoResultado) {
            if (tipoResultado.equalsIgnoreCase(genericBeanOutRO.getLabel())) {
                return genericBeanOutRO.getValue();
            }
        }
        return null;
    }

    private int getTipoResultado(String tipoResultado) {
        int i = 0;
        boolean entro = false;
        for (GenericBeanOutRO genericBeanOutRO : listaTipoResultado) {
            if (tipoResultado.equalsIgnoreCase(genericBeanOutRO.getValue())) {
                entro = true;
                break;
            }
            i++;
        }
        if (entro) {
            return i + 1;
        } else {
            return 0;
        }
    }

    private String setTipoGabinete(String tipoGabinete) {
        for (GenericBeanOutRO genericBeanOutRO : listaTipoGabinete) {
            if (tipoGabinete.equalsIgnoreCase(genericBeanOutRO.getLabel())) {
                return genericBeanOutRO.getValue();
            }
        }
        return null;
    }

    private int getTipoGabinete(String tipoGabinete) {
        int i = 0;
        boolean entro = false;
        for (GenericBeanOutRO genericBeanOutRO : listaTipoGabinete) {
            if (tipoGabinete.equalsIgnoreCase(genericBeanOutRO.getValue())) {
                entro = true;
                break;
            }
            i++;
        }
        if (entro) {
            return i + 1;
        } else {
            return 0;
        }
    }

    private String setTipoMaterial(String tipoMaterial) {
        for (GenericBeanOutRO genericBeanOutRO : listaTipoMaterial) {
            if (tipoMaterial.equalsIgnoreCase(genericBeanOutRO.getLabel())) {
                return genericBeanOutRO.getValue();
            }
        }
        return null;
    }

    private int getTipoMaterial(String tipoMaterial) {
        int i = 0;
        boolean entro = false;
        for (GenericBeanOutRO genericBeanOutRO : listaTipoMaterial) {
            if (tipoMaterial.equalsIgnoreCase(genericBeanOutRO.getValue())) {
                entro = true;
                break;
            }
            i++;
        }
        if (entro) {
            return i + 1;
        } else {
            return 0;
        }
    }

    private int getFusionista(String fusionista) {
        int i = 0;
        boolean entro = false;
        for (BaseInspectorOutRO baseInspectorOutRO : listaTipoFusionista) {
            if (fusionista.equalsIgnoreCase(baseInspectorOutRO.getIdInspector().toString())) {
                entro = true;
                break;
            }
            i++;
        }
        if (entro) {
            return i + 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                acometidaDao.insertOrReplace(acometida);
                Intent intent = new Intent(Acometida2Activity.this, BuscarAcometidaActivity.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.menuMas:
                tablaTuberia.setVisibility(View.VISIBLE);
                menuMas.setVisible(false);
                menuMenos.setVisible(true);
                return true;

            case R.id.menuMenos:
                tablaTuberia.setVisibility(View.GONE);
                menuMas.setVisible(true);
                menuMenos.setVisible(false);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        acometidaDao.insertOrReplace(acometida);
        Intent intent = new Intent(Acometida2Activity.this, BuscarAcometidaActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        countDownTimer.createAndStart();
    }

    private void aceptarPermiso() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permiso = Util.getPermisos(Acometida2Activity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION);
            if (permiso != null) {
                requestPermissions(permiso, pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION);
            }
        }
    }

    private void createLocationRequest() {
        int value = 10000;
        try {
            value = Integer.valueOf(config.getPrecision());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(value);
        mLocationRequest.setFastestInterval(value);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(Acometida2Activity.this, 1458);
                    } catch (IntentSender.SendIntentException sendEx) {
                    }
                }
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            aceptarPermiso();
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION:
                String message = Util.onRequestPermissionsResult(permissions, grantResults);
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        spiAco_fusionista.dismiss();
        spiAco_fusionistagabinete.dismiss();
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    private void callTiposObservacion() {
        showProgressDialog(R.string.app_procesando);
        System.out.println("OBSA");
        if (RESULTADO_INSTALACION_ACOMETIDA_PENDIENTE.equalsIgnoreCase(acometida.getResultado())) {
            System.out.println("OBSB");
            TipoObservacionPendiente tipoObservacionPendiente = new TipoObservacionPendiente(new TipoObservacionPendiente.OnTipoObservacionCompleted() {
                @Override
                public void onTipoObservacionCompleted(ListGenericBeanOutRO listGenericBeanOutRO) {
                    hideProgressDialog();
                    if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                    } else if (listGenericBeanOutRO == null) {
                        ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                    } else if (!listGenericBeanOutRO.getResultCode().equalsIgnoreCase(Constantes.RESULTADO_SUCCESS)) {
                        ((App) getApplication()).showToast(listGenericBeanOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listGenericBeanOutRO.getMessage());
                    } else {
                        listatipoObse = listGenericBeanOutRO.getGenericBean();
                        if(tipo.equals("T")){
                            configurarObs();
                        }else{
                            configurarObsgabinete();
                        }
                    }
                }
            });
            tipoObservacionPendiente.execute(config);
        } else {
            System.out.println("OBSC");
            TipoObservacionRechazada tipoObservacionRechazada = new TipoObservacionRechazada(new TipoObservacionRechazada.OnTipoObservacionCompleted() {
                @Override
                public void onTipoObservacionCompleted(ListGenericBeanOutRO listGenericBeanOutRO) {
                    hideProgressDialog();
                    if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                    } else if (listGenericBeanOutRO == null) {
                        ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                    } else if (!listGenericBeanOutRO.getResultCode().equalsIgnoreCase(Constantes.RESULTADO_SUCCESS)) {
                        ((App) getApplication()).showToast(listGenericBeanOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listGenericBeanOutRO.getMessage());
                    } else {
                        listatipoObse = listGenericBeanOutRO.getGenericBean();

                        if(tipo.equals("T")){
                            configurarObs();
                        }else{
                            configurarObsgabinete();
                        }

                    }
                }
            });
            tipoObservacionRechazada.execute(config);
        }
    }

    private void configurarObs() {
        observaciones = observacionDao.queryBuilder().where(ObservacionDao.Properties.IdInstalacionMontante.eq(acometida.getIdInstalacionAcometida())).list();
        if (observaciones.size() < 1) {
            System.out.println("OBS1");
            observaciones = new ArrayList<>();
            Observacion observacion = new Observacion();
            observacion.setIdInstalacionMontante(acometida.getIdInstalacionAcometida());
            observacion.setDescripcionObservacion("");
            observacion.setTipoObservacion("0");
            observaciones.add(observacion);
            observacionDao.insertOrReplace(observacion);
        }
        System.out.println("OBS0");
        observacionAdaptador = new ObservacionAdaptador(this, observaciones);
        observacionAdaptador.setBtnVis1_grabar(btnAco_grabar);
        observacionAdaptador.setListaObservacionInstalacion(listaObservacionAcometida);
        observacionAdaptador.setObservacionDao(observacionDao);
        observacionAdaptador.setListatipoObse(listatipoObse);
        observacionAdaptador.setCallback(this);
        listaObservacionAcometida.setAdapter(observacionAdaptador);
        Util.setListViewHeightBasedOnChildren(listaObservacionAcometida);
        validarCheck();
    }

    private void configurarObsgabinete() {
        observaciones = observacionDao.queryBuilder().where(ObservacionDao.Properties.IdInstalacionMontante.eq(acometida.getIdInstalacionAcometida())).list();
        if (observaciones.size() < 1) {
            observaciones = new ArrayList<>();
            Observacion observacion = new Observacion();
            observacion.setIdInstalacionMontante(acometida.getIdInstalacionAcometida());
            observacion.setDescripcionObservacion("");
            observacion.setTipoObservacion("0");
            observaciones.add(observacion);
            observacionDao.insertOrReplace(observacion);
        }

        observacionAdaptador = new ObservacionAdaptador(this, observaciones);
        observacionAdaptador.setBtnVis1_grabar(btnAco_grabar);
        observacionAdaptador.setListaObservacionInstalacion(listaObservacionAcometidagabinete);
        observacionAdaptador.setObservacionDao(observacionDao);
        observacionAdaptador.setListatipoObse(listatipoObse);
        observacionAdaptador.setCallback(this);
        listaObservacionAcometidagabinete.setAdapter(observacionAdaptador);
        Util.setListViewHeightBasedOnChildren(listaObservacionAcometidagabinete);
        validarCheck();
    }

    public void loadBitmap(String ruta, final ImageView imageView) {
        MostrarFotoTask mostrarFotoTask = new MostrarFotoTask(new MostrarFotoTask.OnMostrarFotoTaskCompleted() {
            @Override
            public void onMostrarFotoTaskCompleted(Bitmap bitmap) {
                if (bitmap != null) {
                    if (imageView != null) {
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(120, 120);
                        layoutParams.setMargins(10, 10, 10, 10);
                        imageView.setLayoutParams(layoutParams);
                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        imageView.setImageBitmap(bitmap);
                    }
                } else {
                    ((App) getApplication()).showToast("Error al cargar la foto");
                }
            }
        });
        mostrarFotoTask.execute(ruta);
    }

}
