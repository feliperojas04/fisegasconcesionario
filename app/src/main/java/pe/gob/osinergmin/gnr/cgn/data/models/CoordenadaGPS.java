package pe.gob.osinergmin.gnr.ign.data.models;

public class CoordenadaGPS {
    private String latitud;
    private String longitud;

    public CoordenadaGPS(String latitud, String longitud){
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }
}
