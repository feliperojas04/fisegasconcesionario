package pe.gob.osinergmin.gnr.cgn.util;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import pe.gob.osinergmin.gnr.cgn.actividad.PrincipalActivity;
import pe.gob.osinergmin.gnr.cgn.service.AlarmService;
import pe.gob.osinergmin.gnr.cgn.service.DescaService;

public class ResponseReceiver extends BroadcastReceiver {

    NotificationManager manager;
    NotificationCompat.Builder builder;

    public ResponseReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("ResponseReceiver: ", intent.getAction());
        SharedPreferences mPreferences = context.getSharedPreferences("gnrconcesionario", Context.MODE_PRIVATE);
        switch (intent.getAction()) {
            case Constantes.ACTION_RUN_SERVICE:
                break;
            case Constantes.ACTION_EXIT_SERVICE:
                String tipo = intent.getStringExtra("tipo");
                boolean sinWifi = intent.getBooleanExtra("sinWifi", false);
                Intent intent2 = new Intent(context, PrincipalActivity.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent2, 0);
                if (sinWifi) {
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        builder = new NotificationCompat.Builder(context, Constantes.CHANNEL_ID)
                                .setSmallIcon(android.R.drawable.stat_sys_download_done)
                                .setContentTitle("CGN - Concesionaria")
                                .setContentText("Se interrumpió la conexión a internet.\nPor favor vuelva a intentarlo.")
                                .setContentIntent(pendingIntent)
                                .addAction(android.R.drawable.stat_sys_download_done, "Ir a la aplicación", pendingIntent);
                    } else {
                        builder = new NotificationCompat.Builder(context)
                                .setSmallIcon(android.R.drawable.stat_sys_download_done)
                                .setContentTitle("CGN - Concesionaria")
                                .setContentText("Se interrumpió la conexión a internet.\nPor favor vuelva a intentarlo.")
                                .setContentIntent(pendingIntent)
                                .addAction(android.R.drawable.stat_sys_download_done, "Ir a la aplicación", pendingIntent);
                    }
                    manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    manager.notify(1, builder.build());
                } else {
                    if ("sync".equalsIgnoreCase(tipo)) {
                        Intent intentMemoryService = new Intent(context, DescaService.class);
                        intentMemoryService.putExtra("EXTRA", true);
                        context.startService(intentMemoryService);
                    } else if ("descarga".equalsIgnoreCase(tipo)) {
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                            builder = new NotificationCompat.Builder(context, Constantes.CHANNEL_ID)
                                    .setSmallIcon(android.R.drawable.stat_sys_download_done)
                                    .setContentTitle("CGN - Concesionaria")
                                    .setContentText("Se terminó la descarga de datos")
                                    .setContentIntent(pendingIntent)
                                    .addAction(android.R.drawable.stat_sys_download_done, "Ir a la aplicación", pendingIntent);
                        } else {
                            builder = new NotificationCompat.Builder(context)
                                    .setSmallIcon(android.R.drawable.stat_sys_download_done)
                                    .setContentTitle("CGN - Concesionaria")
                                    .setContentText("Se terminó la descarga de datos")
                                    .setContentIntent(pendingIntent)
                                    .addAction(android.R.drawable.stat_sys_download_done, "Ir a la aplicación", pendingIntent);
                        }
                        manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                        manager.notify(1, builder.build());
                    } else {
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                            builder = new NotificationCompat.Builder(context, Constantes.CHANNEL_ID)
                                    .setContentTitle("CGN - Concesionaria")
                                    .setSmallIcon(android.R.drawable.stat_sys_upload_done)
                                    .setContentText("Se terminó la sincronización de datos")
                                    .setContentIntent(pendingIntent)
                                    .addAction(android.R.drawable.stat_sys_upload_done, "Ir a la aplicación", pendingIntent);
                        } else {
                            builder = new NotificationCompat.Builder(context)
                                    .setContentTitle("CGN - Concesionaria")
                                    .setSmallIcon(android.R.drawable.stat_sys_upload_done)
                                    .setContentText("Se terminó la sincronización de datos")
                                    .setContentIntent(pendingIntent)
                                    .addAction(android.R.drawable.stat_sys_upload_done, "Ir a la aplicación", pendingIntent);
                        }
                        manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                        manager.notify(1, builder.build());
                    }
                }
                break;
            case Constantes.ACTION_OFFLINE_ON:
                boolean pendientes = mPreferences.getBoolean("pendientes", false);
                if (pendientes) {
                    NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
                    bigTextStyle.setBigContentTitle("CGN - Concesionaria");
                    bigTextStyle.bigText("Ha pasado más de 1 día sin que haya sincronizado los datos, se recomienda hacerlo para que cuente con datos actualizados y reduzca el riesgo de problemas en la sincronización");
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        builder = new NotificationCompat.Builder(context, Constantes.CHANNEL_ID)
                                .setSmallIcon(android.R.drawable.stat_notify_sync)
                                .setStyle(bigTextStyle)
                                .setContentTitle("CGN - Concesionaria")
                                .setContentText("Tiene datos pendientes por sincronizar")
                                .setAutoCancel(false)
                                .setOngoing(true);
                    } else {
                        builder = new NotificationCompat.Builder(context)
                                .setSmallIcon(android.R.drawable.stat_notify_sync)
                                .setStyle(bigTextStyle)
                                .setContentTitle("CGN - Concesionaria")
                                .setContentText("Tiene datos pendientes por sincronizar")
                                .setAutoCancel(false)
                                .setOngoing(true);
                    }
                    manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    manager.notify(2, builder.build());
                } else {
                    SharedPreferences.Editor editor = mPreferences.edit();
                    editor.putBoolean("pendientes", false);
                    editor.putLong("fecha", 0);
                    editor.apply();
                }
                break;
            case Constantes.ACTION_OFFLINE_OFF:
                manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                manager.cancel(2);
                break;
            case "android.intent.action.BOOT_COMPLETED":
                long fecha = mPreferences.getLong("fecha", 0);
                if (fecha != 0) {
                    AlarmService.startActionAlarm(context, fecha);
                }
                break;
        }
    }
}
