package pe.gob.osinergmin.gnr.cgn;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.StrictMode;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.multidex.MultiDexApplication;

//import com.crashlytics.android.Crashlytics;
import com.scottyab.rootbeer.RootBeer;

import org.greenrobot.greendao.database.Database;

//import io.fabric.sdk.android.Fabric;
import pe.gob.osinergmin.gnr.cgn.data.DatabaseUpgradeHelper;
import pe.gob.osinergmin.gnr.cgn.data.models.DaoMaster;
import pe.gob.osinergmin.gnr.cgn.data.models.DaoSession;
import pe.gob.osinergmin.gnr.cgn.util.Constantes;

public class App extends MultiDexApplication {

    public static final boolean ENCRYPTED = true;
    private static Toast toast;
    private DaoSession daoSession;
    private RootBeer rootBeer;

    @Override
    public void onCreate() {
        super.onCreate();

        DatabaseUpgradeHelper helper = new DatabaseUpgradeHelper(this, ENCRYPTED ? "cgn" : "BDFiseGasConcesionario");
        Database db = ENCRYPTED ? helper.getEncryptedWritableDb(BuildConfig.DB) : helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();

        if (BuildConfig.DEBUG) {
//            setStrictMode();
        } else {
            //Fabric.with(this, new Crashlytics());
        }

        rootBeer = new RootBeer(this);
        toast = Toast.makeText(this, "message_blank", Toast.LENGTH_SHORT);
        createNotificationChannel();
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }

    public void showToast(String message) {
        if (toast != null) {
            toast.setText(message);
            toast.show();
        } else {
            toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void isRooted(Context context) {
        if (!BuildConfig.DEBUG) {
            rootBeer.isRootedWithoutBusyBoxCheck();
            if (rootBeer.isRooted()) {
                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.setTitle("Alerta");
                alertDialog.setMessage("Este dispositivo esta rooteado. No puede usar la aplicación");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Intent intent = new Intent(Intent.ACTION_MAIN);
                                intent.addCategory(Intent.CATEGORY_HOME);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.show();
            }
        }
    }

    private void setStrictMode() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build());

        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectLeakedSqlLiteObjects()
                .penaltyLog()
                .penaltyDeath()
                .build());
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = Constantes.CHANNEL_NAME;
            String description = Constantes.CHANNEL_DESCRIPTION;
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(Constantes.CHANNEL_ID, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
