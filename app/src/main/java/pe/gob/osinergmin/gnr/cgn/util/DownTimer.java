package pe.gob.osinergmin.gnr.cgn.util;

import android.os.CountDownTimer;

public class DownTimer {

    private static DownTimer mInstance = null;
    private CountDownTimer countdownTimer;
    private long startTime;
    private long interval;

    private DownTimer() {
        startTime = 30 * 60 * 1000;
        interval = 1000;
    }

    public static DownTimer getInstance() {
        if (mInstance == null) {
            mInstance = new DownTimer();
        }
        return mInstance;
    }

    public void createAndStart() {
        stop();
        countdownTimer = new CountDownTimer(startTime, interval) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        };
        countdownTimer.start();
    }

    public void stop() {
        if (countdownTimer != null) {
            countdownTimer.cancel();
            countdownTimer = null;
        }
    }
}
