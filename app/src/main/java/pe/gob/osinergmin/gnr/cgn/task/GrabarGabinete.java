package pe.gob.osinergmin.gnr.cgn.task;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.in.InstalacionAcometidaProyectoInstalacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.InstalacionAcometidaTuberiaConexionProyectoInstalacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.InstalacionTuberiaConexionProyectoInstalacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.ObservacionInstalacionAcometidaInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.list.ListInstalacionAcometidaProyectoInstalacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.list.ListInstalacionTuberiaConexionProyectoInstalacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.list.ListObservacionInstalacionAcometidaInRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.data.models.Acometida;
import pe.gob.osinergmin.gnr.cgn.data.models.AcometidaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.AcometidaNueva;
import pe.gob.osinergmin.gnr.cgn.data.models.AcometidaNuevaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.ConfigDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Observacion;
import pe.gob.osinergmin.gnr.cgn.data.models.ObservacionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Precision;
import pe.gob.osinergmin.gnr.cgn.data.models.PrecisionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.ResponseInstalacionAcometida;
import pe.gob.osinergmin.gnr.cgn.data.models.Tuberias;
import pe.gob.osinergmin.gnr.cgn.data.models.TuberiasDao;
import pe.gob.osinergmin.gnr.cgn.util.Urls;

import static gob.osinergmin.gnr.util.Constantes.RESULTADO_INSTALACION_ACOMETIDA_CONCLUIDA;

public class GrabarGabinete extends AsyncTask<Acometida, Void, ResponseInstalacionAcometida> {

    private final WeakReference<Context> context;
    private onGrabarGabineteAsyncTaskCompleted listener = null;
    private Config config;
    private ConfigDao configDao;
    private ObservacionDao observacionDao;
    private AcometidaDao acometidaDao;
    private PrecisionDao precisionDao;
    private AcometidaNuevaDao acometidaNuevaDao;
    private List<AcometidaNueva> acometidaNuevaaa = new ArrayList<>();

    public GrabarGabinete(onGrabarGabineteAsyncTaskCompleted listener, Context context, List<AcometidaNueva> acometidaNuevaaa) {
        this.listener = listener;
        this.context = new WeakReference<>(context);
        this.acometidaNuevaaa = acometidaNuevaaa;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Context context = this.context.get();
        if (context != null) {
            observacionDao = ((App) context.getApplicationContext()).getDaoSession().getObservacionDao();
            acometidaNuevaDao = ((App) context.getApplicationContext()).getDaoSession().getAcometidaNuevaDao();
            acometidaDao = ((App) context.getApplicationContext()).getDaoSession().getAcometidaDao();
            configDao = ((App) context.getApplicationContext()).getDaoSession().getConfigDao();
            config = configDao.queryBuilder().limit(1).unique();
            precisionDao = ((App) context.getApplicationContext()).getDaoSession().getPrecisionDao();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected ResponseInstalacionAcometida doInBackground(Acometida... acometidas) {
        try {
            Acometida acometida = acometidas[0];
            Precision precision = precisionDao.queryBuilder().where(PrecisionDao.Properties.Estado.eq("3"), PrecisionDao.Properties.IdSolicitud.eq(acometida.getIdInstalacionAcometida())).orderAsc(PrecisionDao.Properties.Precision).limit(1).unique();
            String url = Urls.getBase() + Urls.getTuberiaRegistrar();

            InstalacionAcometidaTuberiaConexionProyectoInstalacionInRO instalacionInRO = new InstalacionAcometidaTuberiaConexionProyectoInstalacionInRO();
            instalacionInRO.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);
            instalacionInRO.setIdInstalacionAcometida(acometida.getIdInstalacionAcometida());
            instalacionInRO.setIdProyectoInstalacion(Integer.parseInt(config.getIdProyectoInstalacion()));
            instalacionInRO.setResultadoInstalacionAcometida("C");
            instalacionInRO.setTipoGabinete("C");
            System.out.println("Gabinete 0: " +acometidaNuevaaa.get(0).getTipoGabinete() +" "+acometidaNuevaaa.get(0).getIdClasificacionGabinete()+" "+acometidaNuevaaa.get(0).getFotoGabinete());
            System.out.println("Gabinete 1: " +acometidaNuevaaa.get(1).getTipoGabinete() +" "+acometidaNuevaaa.get(1).getIdClasificacionGabinete()+" "+acometidaNuevaaa.get(1).getFotoGabinete());
            System.out.println("Numero de registros:" + acometidaNuevaaa.size());
            List<AcometidaNueva> acometidaNuevas = acometidaNuevaDao.queryBuilder().where(AcometidaNuevaDao.Properties.IdInstalacionAcometida.eq(acometida.getIdInstalacionAcometida())).list();
            //List<AcometidaNueva> acometidaNuevas = acometidaNuevaDao.queryBuilder().list();
            System.out.println("Numero de registros2:" + acometidaNuevas.size());
            ListInstalacionAcometidaProyectoInstalacionInRO listPuntoInstalacionHabilitacionInRO = new ListInstalacionAcometidaProyectoInstalacionInRO();
            List<InstalacionAcometidaProyectoInstalacionInRO> puntosInstalacion = new ArrayList<>();
            MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
            if (acometidaNuevas.size() > 0) {
                int i = 1;
                        if (RESULTADO_INSTALACION_ACOMETIDA_CONCLUIDA.equalsIgnoreCase(acometida.getResultado())) {
                            //for (int i=0; i < acometidaNuevas.size(); i++)
                            for (AcometidaNueva acometidaNueva : acometidaNuevas) {
                                InstalacionAcometidaProyectoInstalacionInRO puntoTMP = new InstalacionAcometidaProyectoInstalacionInRO();
                                puntoTMP.setIdGabineteProyectoInstalacion(acometidaNueva.getIdGabineteProyectoInstalacion());
                                puntoTMP.setTipoGabinete("".equalsIgnoreCase(acometidaNueva.getTipoGabinete()) ? null : (acometidaNueva.getTipoGabinete()));
                                puntoTMP.setNumeroOrdenGabineteInstalacion(i);
                                puntosInstalacion.add(puntoTMP);
                                parts.add("fotoGabineteManifoldTerminado", new FileSystemResource(acometidaNueva.getFotoGabinete()));
                                //i++;
                            }
                            listPuntoInstalacionHabilitacionInRO.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);
                            listPuntoInstalacionHabilitacionInRO.setIdfusionistaRegistro(Integer.parseInt("".equalsIgnoreCase(acometida.getFusionista()) ? null : acometida.getFusionista()));
                            listPuntoInstalacionHabilitacionInRO.setGabinetes(puntosInstalacion);
                            listPuntoInstalacionHabilitacionInRO.setNombreFotoUbicacionAcometida(acometida.getFotoGabinete());
                            parts.add("fotoGabineteManifoldTerminado", new FileSystemResource(acometida.getFotoGabinete()));
                            instalacionInRO.setInstalacionAcometidaProyectoInstalacionInRO(listPuntoInstalacionHabilitacionInRO);
                        } else {
                            List<Observacion> observacions = observacionDao.queryBuilder().where(ObservacionDao.Properties.IdInstalacionMontante.eq(acometida.getIdInstalacionAcometida())).list();
                            ListObservacionInstalacionAcometidaInRO listObservacionInstalacionAcometidaInRO = new ListObservacionInstalacionAcometidaInRO();
                            List<ObservacionInstalacionAcometidaInRO> observacionInstalacionAcometidaInROS = new ArrayList<>();
                            int k = 1;
                            for (Observacion observacion : observacions) {
                                ObservacionInstalacionAcometidaInRO tmp = new ObservacionInstalacionAcometidaInRO();
                                tmp.setOrdenObservacionInstalacionAcometida(k);
                                //tmp.setIdObservacionInstalacionAcometida(observacion.getIdInstalacionMontante());
                                tmp.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);
                                tmp.setIdTipoObservacion(Integer.parseInt(observacion.getTipoObservacion()));
                                tmp.setDescripcionObservacionInstalacionAcometida(observacion.getDescripcionObservacion());
                                observacionInstalacionAcometidaInROS.add(tmp);
                                k++;
                            }

                            listObservacionInstalacionAcometidaInRO.setObservacionInstalacionAcometida(observacionInstalacionAcometidaInROS);
                            listObservacionInstalacionAcometidaInRO.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);
                            instalacionInRO.setObservaciones(listObservacionInstalacionAcometidaInRO);
                        }
                }

            instalacionInRO.setInstalacionTuberiaConexionProyectoInstalacion(null);
            instalacionInRO.setFechaRegistroOfflineInstalacionAcometida(acometida.getFechaRegistroOffline());
            instalacionInRO.setCoordenadasAuditoriaInstalacionAcometida(String.format("%s,%s", precision.getLatitud(), precision.getLongitud()));
            instalacionInRO.setPrecisionCoordenadasAuditoriaInstalacionAcometida(BigDecimal.valueOf(Double.valueOf(precision.getPrecision())));

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
            httpHeaders.add("Authorization", "Bearer " + config.getToken());
            parts.add(Constantes.PARAM_NAME_INSTALACION_ACOMETIDA, instalacionInRO);

            ObjectMapper mapper = new ObjectMapper();String jsonString = mapper.writeValueAsString(instalacionInRO);
            System.out.println("Json: "+jsonString);

            HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(parts, httpHeaders);
            RestTemplate restTemplate = new RestTemplate();
            return restTemplate.postForObject(url, httpEntity, ResponseInstalacionAcometida.class);
        } catch (RestClientException | NullPointerException | IllegalArgumentException | JsonProcessingException e) {
            Log.e("GRABAR", e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(ResponseInstalacionAcometida instalacionAcometidaOutRO) {
        super.onPostExecute(instalacionAcometidaOutRO);
        listener.onGrabarGabineteAsyncTaskCompleted(instalacionAcometidaOutRO);
    }

    public interface onGrabarGabineteAsyncTaskCompleted {
        void onGrabarGabineteAsyncTaskCompleted(ResponseInstalacionAcometida instalacionAcometidaOutRO);
    }
}
