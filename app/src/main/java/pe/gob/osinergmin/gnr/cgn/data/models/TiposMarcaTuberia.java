package pe.gob.osinergmin.gnr.cgn.data.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class TiposMarcaTuberia {

    @Id
    private Long id;
    private String value;
    private String label;

    @Generated(hash = 1433928803)
    public TiposMarcaTuberia() {
    }

    public TiposMarcaTuberia(Long id) {
        this.id = id;
    }

    @Generated(hash = 939177187)
    public TiposMarcaTuberia(Long id, String value, String label) {
        this.id = id;
        this.value = value;
        this.label = label;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
