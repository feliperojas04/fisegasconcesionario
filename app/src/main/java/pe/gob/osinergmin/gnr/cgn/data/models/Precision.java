package pe.gob.osinergmin.gnr.cgn.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class Precision implements Parcelable {

    @Id
    private Long id;
    private Integer idSolicitud;
    private String latitud;
    private String longitud;
    private String precision;
    private String estado;

    @Generated(hash = 749059640)
    public Precision() {
    }

    public Precision(Long id) {
        this.id = id;
    }

    @Generated(hash = 2012484535)
    public Precision(Long id, Integer idSolicitud, String latitud, String longitud, String precision, String estado) {
        this.id = id;
        this.idSolicitud = idSolicitud;
        this.latitud = latitud;
        this.longitud = longitud;
        this.precision = precision;
        this.estado = estado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(Integer idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getPrecision() {
        return precision;
    }

    public void setPrecision(String precision) {
        this.precision = precision;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.idSolicitud);
        dest.writeString(this.latitud);
        dest.writeString(this.longitud);
        dest.writeString(this.precision);
        dest.writeString(this.estado);
    }

    protected Precision(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.idSolicitud = (Integer) in.readValue(Integer.class.getClassLoader());
        this.latitud = in.readString();
        this.longitud = in.readString();
        this.precision = in.readString();
        this.estado = in.readString();
    }

    public static final Creator<Precision> CREATOR = new Creator<Precision>() {
        @Override
        public Precision createFromParcel(Parcel source) {
            return new Precision(source);
        }

        @Override
        public Precision[] newArray(int size) {
            return new Precision[size];
        }
    };
}
