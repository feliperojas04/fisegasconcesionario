package pe.gob.osinergmin.gnr.cgn.data.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class MParametroOutRO {

    @Id
    private Long id;
    private String nombreParametro;
    private String descripcionParametro;
    private String valorParametro;

    @Generated(hash = 2067421443)
    public MParametroOutRO() {
    }

    public MParametroOutRO(Long id) {
        this.id = id;
    }

    @Generated(hash = 1012078098)
    public MParametroOutRO(Long id, String nombreParametro, String descripcionParametro, String valorParametro) {
        this.id = id;
        this.nombreParametro = nombreParametro;
        this.descripcionParametro = descripcionParametro;
        this.valorParametro = valorParametro;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreParametro() {
        return nombreParametro;
    }

    public void setNombreParametro(String nombreParametro) {
        this.nombreParametro = nombreParametro;
    }

    public String getDescripcionParametro() {
        return descripcionParametro;
    }

    public void setDescripcionParametro(String descripcionParametro) {
        this.descripcionParametro = descripcionParametro;
    }

    public String getValorParametro() {
        return valorParametro;
    }

    public void setValorParametro(String valorParametro) {
        this.valorParametro = valorParametro;
    }

}
