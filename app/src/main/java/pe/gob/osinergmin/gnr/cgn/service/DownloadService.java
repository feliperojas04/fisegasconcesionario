package pe.gob.osinergmin.gnr.cgn.service;

import android.app.IntentService;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Environment;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import pe.gob.osinergmin.gnr.cgn.actividad.IniciarActivity;
import pe.gob.osinergmin.gnr.cgn.data.pojos.Download;
import retrofit2.Call;
import retrofit2.Retrofit;

public class DownloadService extends IntentService {

    private static String ruta = "/.CONCESIONARIO/";
    private int totalFileSize;

    public DownloadService() {
        super("Download Service");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        try {
            initDownload(intent.getExtras().getString("URL"));
        } catch (NullPointerException e) {
            e.printStackTrace();
            onDownloadError();
        }
    }

    private void initDownload(String url) {

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://download.mapsforge.org/")
                .client(client)
                .build();

        RetrofitInterface retrofitInterface = retrofit.create(RetrofitInterface.class);

        Call<ResponseBody> request = retrofitInterface.downloadFile(url);
        try {
            downloadFile(request.execute().body());
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
            onDownloadError();
        }
    }

    private String getFilePath(){
        ContextWrapper contextWrapper = new ContextWrapper(getApplicationContext());
        File mapDirectory = contextWrapper.getExternalFilesDir(Environment.DIRECTORY_PICTURES+ruta);
        File file = new File(mapDirectory,"peru.map");
        System.out.println(file.getPath());
        return file.getPath();
    }

    private void downloadFile(ResponseBody body) throws IOException, NullPointerException {
        OutputStream output = null;
        InputStream bis = null;
        try {
            int count;
            byte data[] = new byte[1024 * 4];
            long fileSize = body.contentLength();
            bis = new BufferedInputStream(body.byteStream(), 1024 * 8);
            //File outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), ruta + "peru.map");
            output = new FileOutputStream(getFilePath());
            long total = 0;
            long startTime = System.currentTimeMillis();
            int timeCount = 1;

            while ((count = bis.read(data)) != -1) {

                total += count;
                totalFileSize = (int) (fileSize / (Math.pow(1024, 2)));
                double current = Math.round(total / (Math.pow(1024, 2)));

                int progress = (int) ((total * 100) / fileSize);

                long currentTime = System.currentTimeMillis() - startTime;

                Download download = new Download();
                download.setTotalFileSize(totalFileSize);

                if (currentTime > 1000 * timeCount) {
                    download.setCurrentFileSize((int) current);
                    download.setProgress(progress);
                    sendIntent(download);
                    timeCount++;
                }
                output.write(data, 0, count);
            }
            onDownloadComplete();

        } catch (SocketTimeoutException | FileNotFoundException | NullPointerException e) {
            e.printStackTrace();
            onDownloadError();
        }
        if (output != null) {
            output.flush();
            output.close();
        }
        bis.close();
    }

    private void sendIntent(Download download) {
        Intent intent = new Intent(IniciarActivity.MESSAGE_PROGRESS);
        intent.putExtra("download", download);
        LocalBroadcastManager.getInstance(DownloadService.this).sendBroadcast(intent);
    }

    private void onDownloadComplete() {
        Download download = new Download();
        download.setProgress(100);
        sendIntent(download);
    }

    private void onDownloadError() {
        Download download = new Download();
        download.setProgress(111);
        sendIntent(download);
    }
}
