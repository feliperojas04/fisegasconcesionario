package pe.gob.osinergmin.gnr.cgn.actividad;

import android.Manifest;
import android.content.ClipData;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.GenericBeanOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListGenericBeanOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.adaptador.PuntoAdaptador;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.Gasodomestico;
import pe.gob.osinergmin.gnr.cgn.data.models.GasodomesticoDao;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.Precision;
import pe.gob.osinergmin.gnr.cgn.data.models.PrecisionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Punto;
import pe.gob.osinergmin.gnr.cgn.data.models.PuntoDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Suministro;
import pe.gob.osinergmin.gnr.cgn.data.models.SuministroDao;
import pe.gob.osinergmin.gnr.cgn.task.TipoGasoDomestico;
import pe.gob.osinergmin.gnr.cgn.util.DownTimer;
import pe.gob.osinergmin.gnr.cgn.util.NetworkUtil;
import pe.gob.osinergmin.gnr.cgn.util.Parse;
import pe.gob.osinergmin.gnr.cgn.util.ReducirFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.Util;

public class PuntoActivity extends BaseActivity implements PuntoAdaptador.Callback {

    private Suministro suministro;
    private Config config;
    private TableLayout tablaPunto;
    private TextView txtDet_DocumentoPropietarioView;
    private TextView txtDet_nombrePropietarioView;
    private TextView txtDet_PredioView;
    private TextView txtDet_numeroIntHabilitacionView;
    private TextView txtDet_numeroSolicitudView;
    private TextView txtDet_numeroContratoView;
    private TextView txtDet_fechaSolicitudView;
    private TextView txtDet_fechaAprobacionView;
    private TextView txtDet_tipoProyectoView;
    private TextView txtDet_tipoInstalacionView;
    private TextView txtDet_numeroPuntosInstaacionView;
    private ListView listPuntos;
    private List<Punto> puntos;
    private SwitchCompat swiPunI_aprueba;
    private Button btnPunI_rechazar;
    private Toolbar toolbar;
    private MenuItem menuMas;
    private MenuItem menuMenos;
    private PuntoAdaptador puntoAdaptador;
    private SuministroDao suministroDao;
    private PuntoDao puntoDao;
    private List<GenericBeanOutRO> listaTipoGaso = new ArrayList<>();
    private DownTimer countDownTimer;
    private MParametroOutRODao mParametroOutRODao;
    private GasodomesticoDao gasodomesticoDao;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private PrecisionDao precisionDao;
    private String tmpRuta;
    private int position;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_punto);

        countDownTimer = DownTimer.getInstance();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        suministroDao = ((App) getApplication()).getDaoSession().getSuministroDao();
        puntoDao = ((App) getApplication()).getDaoSession().getPuntoDao();
        mParametroOutRODao = ((App) getApplication()).getDaoSession().getMParametroOutRODao();
        gasodomesticoDao = ((App) getApplication()).getDaoSession().getGasodomesticoDao();
        precisionDao = ((App) getApplication()).getDaoSession().getPrecisionDao();

        toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_36dp);

        suministro = getIntent().getExtras().getParcelable("SUMINISTRO");
        config = getIntent().getExtras().getParcelable("CONFIG");

        tablaPunto = findViewById(R.id.tablaPunto);
        txtDet_DocumentoPropietarioView = findViewById(R.id.txtDet_DocumentoPropietarioView);
        txtDet_nombrePropietarioView = findViewById(R.id.txtDet_nombrePropietarioView);
        txtDet_PredioView = findViewById(R.id.txtDet_PredioView);
        txtDet_numeroIntHabilitacionView = findViewById(R.id.txtDet_numeroIntHabilitacionView);
        txtDet_numeroSolicitudView = findViewById(R.id.txtDet_numeroSolicitudView);
        txtDet_numeroContratoView = findViewById(R.id.txtDet_numeroContratoView);
        txtDet_fechaSolicitudView = findViewById(R.id.txtDet_fechaSolicitudView);
        txtDet_fechaAprobacionView = findViewById(R.id.txtDet_fechaAprobacionView);
        txtDet_tipoProyectoView = findViewById(R.id.txtDet_tipoProyectoView);
        txtDet_tipoInstalacionView = findViewById(R.id.txtDet_tipoInstalacionView);
        txtDet_numeroPuntosInstaacionView = findViewById(R.id.txtDet_numeroPuntosInstaacionView);
        ImageView addPunI_puntos = findViewById(R.id.addPunI_puntos);
        listPuntos = findViewById(R.id.listPuntos);
        swiPunI_aprueba = findViewById(R.id.swiPunI_aprueba);
        Button btnPunI_evaluando = findViewById(R.id.btnPunI_evaluando);
        btnPunI_rechazar = findViewById(R.id.btnPunI_rechazar);

        addPunI_puntos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Punto punto = new Punto(null, suministro.getIdHabilitacion(), "", "", "","",-1);
                if (puntos != null) {
                    puntos.add(punto);
                }
                puntoDao.insertOrReplace(punto);
                if (puntoAdaptador != null) {
                    puntoAdaptador.notifyDataSetChanged();
                }
                Util.setListViewHeightBasedOnChildren(listPuntos);
                suministro.setPuntosAprueba("0");
                swiPunI_aprueba.setChecked(false);
                swiPunI_aprueba.setClickable(false);
            }
        });

        swiPunI_aprueba.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    suministro.setPuntosAprueba("1");
                    btnPunI_rechazar.setVisibility(View.GONE);
                } else {
                    suministro.setPuntosAprueba("0");
                    btnPunI_rechazar.setVisibility(View.VISIBLE);
                }
            }
        });

        btnPunI_evaluando.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suministroDao.insertOrReplace(suministro);
                Intent intent = new Intent(PuntoActivity.this, AmbienteActivity.class);
                intent.putExtra("SUMINISTRO", suministro);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
            }
        });

        btnPunI_rechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PuntoActivity.this, RechazoActivity.class);
                intent.putExtra("SUMINISTRO", suministro);
                intent.putExtra("CONFIG", config);
                intent.putExtra("TIPO", "");
                startActivity(intent);
            }
        });

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    Precision precision = new Precision();
                    precision.setIdSolicitud(suministro.getIdHabilitacion());
                    precision.setLatitud(String.valueOf(location.getLatitude()));
                    precision.setLongitud(String.valueOf(location.getLongitude()));
                    precision.setPrecision(String.valueOf(location.getAccuracy()));
                    precision.setEstado("1");
                    precisionDao.insertOrReplace(precision);
                }
            }
        };

        aceptarPermiso();
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.createAndStart();
        createLocationRequest();
        toolbar.setTitle(suministro.getSuministro() + " - Puntos de instalación");
        cargarSuministro();
        if (config.getOffline()) {
            listaTipoGaso.clear();
            List<Gasodomestico> gasodomesticos = gasodomesticoDao.queryBuilder().list();
            for (Gasodomestico gasodomestico : gasodomesticos) {
                listaTipoGaso.add(Parse.getGenericBeanOutRO(gasodomestico));
            }
            MParametroOutRO mParametroOutRO = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_CONFIGURACION_COMPRESION_FOTOS_APLICATIVO_MOVIL))
                    .limit(1).unique();
            config.setParametro5(mParametroOutRO == null ? "800x600" : mParametroOutRO.getValorParametro());
            MParametroOutRO mParametroOutPrecision = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_FRECUENCIA_TOMA_COORDENADAS_AUDITORIA))
                    .limit(1).unique();
            config.setPrecision(mParametroOutPrecision == null ? "10000" : mParametroOutPrecision.getValorParametro());
            cargaDatos();
        } else {
            if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                showProgressDialog(R.string.app_cargando);

                TipoGasoDomestico tipoGasoDomestico = new TipoGasoDomestico(new TipoGasoDomestico.OnGasoDomesticoCompleted() {
                    @Override
                    public void onGasoDomesticoCompled(ListGenericBeanOutRO listGenericBeanOutRO) {
                        hideProgressDialog();
                        if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                            ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                        } else if (listGenericBeanOutRO == null) {
                            ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                        } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                            ((App) getApplication()).showToast(listGenericBeanOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listGenericBeanOutRO.getMessage());
                        } else {
                            listaTipoGaso = listGenericBeanOutRO.getGenericBean();
                            if ("".equalsIgnoreCase(config.getParametro5())) {
                                config.setParametro5("800x600");
                            }
                            if ("".equalsIgnoreCase(config.getPrecision())) {
                                config.setPrecision("10000");
                            }
                            cargaDatos();
                        }
                    }
                });
                tipoGasoDomestico.execute(config);
            } else {
                ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
            }
        }
    }

    @Override
    public void clicFoto(int position) {
        System.out.println("click de la foto: " + position);
        if (ActivityCompat.checkSelfPermission(
                PuntoActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ((App) getApplication()).showToast("No puede realizar la acción porque no se ha dado el permiso de Almacenamiento");
            return;
        }
        if (Util.isExternalStorageWritable()) {
            if (Util.getAlbumStorageDir(PuntoActivity.this)) {
                File file = Util.getNuevaRutaFoto(suministro.getIdHabilitacion().toString(),PuntoActivity.this);
                tmpRuta = file.getPath();
                this.position = position;
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                Uri outputUri = FileProvider.getUriForFile(PuntoActivity.this, getApplicationContext().getPackageName() + ".provider", file);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    ClipData clip = ClipData.newUri(getContentResolver(), "Instalacion", outputUri);
                    intent.setClipData(clip);
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                }
                startActivityForResult(intent, 1);
            } else {
                ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
            }
        } else {
            ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
        }

    }

    private void cargaDatos() {
        puntos = puntoDao.queryBuilder().where(PuntoDao.Properties.IdHabilitacion.eq(suministro.getIdHabilitacion())).list();
        if (puntos.size() > 0) {
            puntoAdaptador = new PuntoAdaptador(this, puntos);
            puntoAdaptador.setCallback(this);
            puntoAdaptador.setPuntos(listPuntos);
            puntoAdaptador.setAprueba(swiPunI_aprueba);
            puntoAdaptador.setPuntoDao(puntoDao);
            puntoAdaptador.setListatipoObse(listaTipoGaso);
            listPuntos.setAdapter(puntoAdaptador);
            Util.setListViewHeightBasedOnChildren(listPuntos);
        } else {
            puntos = new ArrayList<>();
            Punto punto = new Punto(null, suministro.getIdHabilitacion(), "", "", "", "", -1);
            puntoDao.insertOrReplace(punto);
            puntos.add(punto);
            puntoAdaptador = new PuntoAdaptador(this, puntos);
            puntoAdaptador.setCallback(this);
            puntoAdaptador.setPuntos(listPuntos);
            puntoAdaptador.setAprueba(swiPunI_aprueba);
            puntoAdaptador.setPuntoDao(puntoDao);
            puntoAdaptador.setListatipoObse(listaTipoGaso);
            listPuntos.setAdapter(puntoAdaptador);
            Util.setListViewHeightBasedOnChildren(listPuntos);
        }
        if (!("0".equalsIgnoreCase(suministro.getPuntosAprueba()))) {
            swiPunI_aprueba.setChecked(true);
        }
    }

    private void cargarSuministro() {
        txtDet_DocumentoPropietarioView.setText(suministro.getDocumentoPropietario());
        txtDet_nombrePropietarioView.setText(suministro.getNombrePropietario());
        txtDet_PredioView.setText(suministro.getPredio());
        txtDet_numeroIntHabilitacionView.setText(suministro.getNumeroIntHabilitacion());
        txtDet_numeroSolicitudView.setText(suministro.getNumeroSolicitud());
        txtDet_numeroContratoView.setText(suministro.getNumeroContrato());
        txtDet_fechaSolicitudView.setText(suministro.getFechaSolicitud());
        txtDet_fechaAprobacionView.setText(suministro.getFechaAprobacion());
        txtDet_tipoProyectoView.setText(suministro.getTipoProyecto());
        txtDet_tipoInstalacionView.setText(suministro.getTipoInstalacion());
        txtDet_numeroPuntosInstaacionView.setText(suministro.getNumeroPuntosInstaacion());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_general, menu);
        menuMas = menu.findItem(R.id.menuMas);
        menuMas.setVisible(true);
        menuMenos = menu.findItem(R.id.menuMenos);
        menuMenos.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                suministroDao.insertOrReplace(suministro);
                Intent intent = new Intent(PuntoActivity.this, HabilitacionActivity.class);
                intent.putExtra("SUMINISTRO", suministro);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
                return true;
            case R.id.menuMas:
                tablaPunto.setVisibility(View.VISIBLE);
                menuMas.setVisible(false);
                menuMenos.setVisible(true);
                return true;

            case R.id.menuMenos:
                tablaPunto.setVisibility(View.GONE);
                menuMas.setVisible(true);
                menuMenos.setVisible(false);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        suministroDao.insertOrReplace(suministro);
        Intent intent = new Intent(PuntoActivity.this, HabilitacionActivity.class);
        intent.putExtra("SUMINISTRO", suministro);
        intent.putExtra("CONFIG", config);
        startActivity(intent);
        finish();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        countDownTimer.createAndStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1458) {
            if (resultCode == RESULT_CANCELED) {
                Intent intent = new Intent(PuntoActivity.this, BuscarSuministroActivity.class);
                startActivity(intent);
                finish();
            }
        }
        if (requestCode == 1) {
            if (resultCode != RESULT_CANCELED) {
                ((PuntoAdaptador) listPuntos.getAdapter()).getPunto(position).setPuntoFoto(tmpRuta);
                ReducirFotoTask reducirFotoTask = new ReducirFotoTask(new ReducirFotoTask.OnReducirFotoTaskCompleted() {
                    @Override
                    public void onReducirFotoTaskCompleted(String result) {
                        if (!("1".equalsIgnoreCase(result))) {
                            ((App) getApplication()).showToast(result);
                        }
                    }
                });

                puntoDao.insertOrReplace(((PuntoAdaptador) listPuntos.getAdapter()).getPunto(position));
                reducirFotoTask.execute(((PuntoAdaptador) listPuntos.getAdapter()).getPunto(position).getPuntoFoto(), config.getParametro5());
                ((PuntoAdaptador) listPuntos.getAdapter()).notifyDataSetChanged();
            }
        }
    }

    private void createLocationRequest() {
        int value = 10000;
        try {
            value = Integer.valueOf(config.getPrecision());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(value);
        mLocationRequest.setFastestInterval(value);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(PuntoActivity.this, 1458);
                    } catch (IntentSender.SendIntentException sendEx) {
                    }
                }
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            aceptarPermiso();
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
    }

    private void aceptarPermiso() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permiso = Util.getPermisos(PuntoActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION);
            if (permiso != null) {
                requestPermissions(permiso, pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION:
                String message = Util.onRequestPermissionsResult(permissions, grantResults);
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }
}
