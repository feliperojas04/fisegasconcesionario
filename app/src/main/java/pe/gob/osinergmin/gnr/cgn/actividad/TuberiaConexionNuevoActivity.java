package pe.gob.osinergmin.gnr.cgn.actividad;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ScrollView;
import android.widget.Toolbar;

import pe.gob.osinergmin.gnr.cgn.R;

public class TuberiaConexionNuevoActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView rvTuberia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tuberia_conexion_nuevo);

        toolbar = findViewById(R.id.toolbar);
        rvTuberia = findViewById(R.id.rvTuberia);

    }
}