package pe.gob.osinergmin.gnr.cgn.adaptador;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.GenericBeanOutRO;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.data.models.Acometida;
import pe.gob.osinergmin.gnr.cgn.data.models.AcometidaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Observacion;
import pe.gob.osinergmin.gnr.cgn.data.models.ObservacionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Rechazo;
import pe.gob.osinergmin.gnr.cgn.util.SearchableSpinner;
import pe.gob.osinergmin.gnr.cgn.util.Util;

public class TuberiaAdaptador extends BaseAdapter {

    private CallBack mCallback;
    private Context context;
    private List<Acometida> acometidas;
    private ImageView icoActaNoInstalacion;
    private Button btnIns1_grabar;
    private ListView listaAcometidas;
    private Boolean validado = false;
    private AcometidaDao acometidaDao;
    private List<GenericBeanOutRO> listatipoObse;

    public void TuberiaAdaptador(Context context, List<Acometida> acometidas) {
        this.context = context;
        this.acometidas = acometidas;
        notifyDataSetChanged();
    }


    //public void setTuberiass(List<Acometida> acometidas) {
    //    this.rechazos = new ArrayList<>();
    //    this.rechazos = rechazos;
    //    notifyDataSetChanged();
    //}

    public void setCallback(CallBack callback) {
        mCallback = callback;
    }

    public void setListatipoObse(List<GenericBeanOutRO> listatipoObse) {
        this.listatipoObse = listatipoObse;
    }

    public void setObservacionDao(AcometidaDao acometidaDao) {
        this.acometidaDao = acometidaDao;
    }

    public void setIcoActaNoInstalacion(ImageView icoActaNoInstalacion) {
        this.icoActaNoInstalacion = icoActaNoInstalacion;
    }

    public void setBtnVis1_grabar(Button btnIns1_grabar) {
        this.btnIns1_grabar = btnIns1_grabar;
    }

    public void setListaObservacionInstalacion(ListView listaAcometidas) {
        this.listaAcometidas = listaAcometidas;
    }

    public Boolean getValidado() {
        return this.validado;
    }

    @Override
    public int getCount() {
        if (acometidas.size() == 0) {
            btnIns1_grabar.setEnabled(false);
        }
        return this.acometidas.size();
    }

    @Override
    public Object getItem(int position) {
        return this.acometidas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        final ViewHolder holder;
        if (convertView == null) {

            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = layoutInflater.inflate(R.layout.item_tuberia, parent, false);

            holder = new ViewHolder();
            holder.txt_observacion = rowView.findViewById(R.id.txt_observacion);
            holder.txtTub_tuberia = rowView.findViewById(R.id.txtTub_tuberia);
            holder.txtTub_numero  = rowView.findViewById(R.id.txtTub_numero);
            holder.txtAco_fusionista = rowView.findViewById(R.id.txtAco_fusionista);
            holder.txtAco_resultado = rowView.findViewById(R.id.txtAco_resultado);
            holder.txtAco_material = rowView.findViewById(R.id.txtAco_material);
            holder.txtAco_tc = rowView.findViewById(R.id.txtAco_tc);
            holder.imgAco_tc = rowView.findViewById(R.id.imgAco_tc);
            holder.textView32 = rowView.findViewById(R.id.textView32);
            holder.icoAco_fusionista = rowView.findViewById(R.id.icoAco_fusionista);
            holder.icoAco_resultado = rowView.findViewById(R.id.icoAco_resultado);
            holder.icoAco_diametro = rowView.findViewById(R.id.icoAco_diametro);
            holder.icoAco_longitud = rowView.findViewById(R.id.icoAco_longitud);
            holder.icoAco_material = rowView.findViewById(R.id.icoAco_material);
            holder.icoAco_tc = rowView.findViewById(R.id.icoAco_tc);
            holder.addObservacion = rowView.findViewById(R.id.addObservacion);
            holder.spiAco_fusionista = rowView.findViewById(R.id.spiAco_fusionista);
            holder.spiAco_resultado = rowView.findViewById(R.id.spiAco_resultado);
            holder.spiAco_material = rowView.findViewById(R.id.spiAco_material);
            holder.ediAco_diametro = rowView.findViewById(R.id.ediAco_diametro);
            holder.ediAco_longitud = rowView.findViewById(R.id.ediAco_longitud);
            holder.listaObservacionAcometida = rowView.findViewById(R.id.listaObservacionAcometida);

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        holder.ref = position;
        holder.check = 0;
        ArrayList<String> list = new ArrayList<>();
        list.add("Seleccione tipo");
        for (GenericBeanOutRO genericBeanOutRO : listatipoObse) {
            list.add(genericBeanOutRO.getLabel());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        holder.spiAco_resultado.setAdapter(adapter);

        String recurso = "Tuberia de conxión Nro. %1$s";
        String formateada = String.format(recurso, String.valueOf(position + 1));
        holder.txt_observacion.setText(formateada);

        holder.spiAco_fusionista.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                holder.check++;
                if (holder.check > 1) {
                    if (position != 0) {
                        acometidas.get(holder.ref).setFusionista(setTipoResultado(parent.getSelectedItem().toString()));
                    } else {
                        acometidas.get(holder.ref).setFusionista("0");
                    }
                    acometidaDao.insertOrReplace(acometidas.get(holder.ref));
                    validar(holder);
                    check();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (!acometidas.get(holder.ref).getFusionista().equalsIgnoreCase("")) {
            holder.spiAco_fusionista.setSelection(getTipoObservacion(acometidas.get(holder.ref).getFusionista()));
        }

        holder.spiAco_resultado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                holder.check++;
                if (holder.check > 1) {
                    if (position != 0) {
                        acometidas.get(holder.ref).setResultado(setTipoResultado(parent.getSelectedItem().toString()));
                    } else {
                        acometidas.get(holder.ref).setResultado("0");
                    }
                    acometidaDao.insertOrReplace(acometidas.get(holder.ref));
                    validar(holder);
                    check();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (!acometidas.get(holder.ref).getResultado().equalsIgnoreCase("")) {
            holder.spiAco_resultado.setSelection(getTipoObservacion(acometidas.get(holder.ref).getResultado()));
        }

        holder.spiAco_material.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                holder.check++;
                if (holder.check > 1) {
                    if (position != 0) {
                        acometidas.get(holder.ref).setMaterial(setTipoResultado(parent.getSelectedItem().toString()));
                    } else {
                        acometidas.get(holder.ref).setMaterial("0");
                    }
                    acometidaDao.insertOrReplace(acometidas.get(holder.ref));
                    validar(holder);
                    check();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (!acometidas.get(holder.ref).getMaterial().equalsIgnoreCase("")) {
            holder.spiAco_material.setSelection(getTipoObservacion(acometidas.get(holder.ref).getMaterial()));
        }

//
        holder.ediAco_diametro.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    if (s.length() < 1000) {
                        acometidas.get(holder.ref).setDiametro(s.toString());
                    } else {
                        ((App) context.getApplicationContext()).showToast(String.format(context.getResources().getString(R.string.app_error_campo), 1000));
                    }
                } else {
                    acometidas.get(holder.ref).setDiametro("");
                }
                acometidaDao.insertOrReplace(acometidas.get(holder.ref));
                validar(holder);
                check();
            }
        });

        holder.ediAco_diametro.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    try {
                        holder.ediAco_diametro.getEditText().setText(acometidas.get(holder.ref).getDiametro().trim());
                    } catch (IndexOutOfBoundsException e) {

                    }
                }
            }
        });

        holder.ediAco_diametro.getEditText().setText(acometidas.get(holder.ref).getDiametro());

//
        holder.ediAco_longitud.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    if (s.length() < 1000) {
                        acometidas.get(holder.ref).setLongitud(s.toString());
                    } else {
                        ((App) context.getApplicationContext()).showToast(String.format(context.getResources().getString(R.string.app_error_campo), 1000));
                    }
                } else {
                    acometidas.get(holder.ref).setLongitud("");
                }
                acometidaDao.insertOrReplace(acometidas.get(holder.ref));
                validar(holder);
                check();
            }
        });

        holder.ediAco_longitud.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    try {
                        holder.ediAco_longitud.getEditText().setText(acometidas.get(holder.ref).getLongitud().trim());
                    } catch (IndexOutOfBoundsException e) {

                    }
                }
            }
        });

        holder.ediAco_longitud.getEditText().setText(acometidas.get(holder.ref).getLongitud());

        return rowView;
    }

    private void validar(ViewHolder holder) {

        if (acometidas.get(holder.ref).getFusionista().equalsIgnoreCase("0") || acometidas.get(holder.ref).getFusionista().equalsIgnoreCase("")) {
            holder.icoAco_fusionista.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            holder.icoAco_fusionista.setImageResource(R.mipmap.ic_done_black_36dp);
        }

        if (acometidas.get(holder.ref).getResultado().equalsIgnoreCase("0") || acometidas.get(holder.ref).getResultado().equalsIgnoreCase("")) {
            holder.icoAco_resultado.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            holder.icoAco_resultado.setImageResource(R.mipmap.ic_done_black_36dp);
        }

        if (acometidas.get(holder.ref).getMaterial().equalsIgnoreCase("0") || acometidas.get(holder.ref).getMaterial().equalsIgnoreCase("")) {
            holder.icoAco_material.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            holder.icoAco_material.setImageResource(R.mipmap.ic_done_black_36dp);
        }

        if (acometidas.get(holder.ref).getDiametro().equalsIgnoreCase("0") || acometidas.get(holder.ref).getDiametro().equalsIgnoreCase("")) {
            holder.icoAco_diametro.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            holder.icoAco_diametro.setImageResource(R.mipmap.ic_done_black_36dp);
        }

        if (acometidas.get(holder.ref).getLongitud().equalsIgnoreCase("0") || acometidas.get(holder.ref).getLongitud().equalsIgnoreCase("")) {
            holder.icoAco_longitud.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            holder.icoAco_longitud.setImageResource(R.mipmap.ic_done_black_36dp);
        }
    }

    private void check() {
        //int i = 0;
        //validado = true;
        //while (i < acometidas.size()) {
        //    Acometida acometida = acometidas.get(i);
        //    if (acometida.getTipoObservacion().equalsIgnoreCase("0") || acometida.getDescripcionObservacion().equalsIgnoreCase("")) {
        //        validado = false;
        //        i = acometidas.size();
        //    }
        //    i++;
        //}
        //mCallback.onValidar();
    }

    private String setTipoResultado(String tipoResultado) {
        for (GenericBeanOutRO genericBeanOutRO : listatipoObse) {
            if (tipoResultado.equalsIgnoreCase(genericBeanOutRO.getLabel())) {
                return genericBeanOutRO.getValue();
            }
        }
        return null;
    }

    private int getTipoObservacion(String tipoGaso) {
        int i = 0;
        boolean entro = false;
        for (GenericBeanOutRO genericBeanOutRO : listatipoObse) {
            if (tipoGaso.equalsIgnoreCase(genericBeanOutRO.getValue())) {
                entro = true;
                break;
            }
            i++;
        }
        if (entro) {
            return i + 1;
        } else {
            return 0;
        }
    }

    public interface CallBack {
        void onValidar();
    }

    private class ViewHolder {
        int ref;
        TextView txtTub_tuberia,txtTub_numero,txtAco_fusionista,txtAco_resultado,txtAco_material,txtAco_tc,imgAco_tc,textView32,txt_observacion;
        ImageView icoAco_fusionista,icoAco_resultado,icoAco_diametro,icoAco_longitud,icoAco_material,icoAco_tc,addObservacion;
        SearchableSpinner spiAco_fusionista;
        Spinner spiAco_resultado,spiAco_material;
        TextInputLayout ediAco_diametro,ediAco_longitud;
        ListView listaObservacionAcometida;
        int check;
    }
}
