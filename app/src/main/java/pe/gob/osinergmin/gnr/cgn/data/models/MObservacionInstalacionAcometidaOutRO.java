package pe.gob.osinergmin.gnr.cgn.data.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class MObservacionInstalacionAcometidaOutRO {
    @Id
    private Long idObservacionInstalacionAcometida;
    private Integer idInstalacionAcometida;
    private Integer ordenObservacionInstalacionAcometida;
    private String nombreTipoObservacion;
    private String descripcionObservacionInstalacionAcometida;
    private String estadoObservacionInstalacionAcometida;
    private String nombreEstadoObservacionInstalacionAcometida;
    private String resultCode;
    private String message;
    private String errorCode;

    @Generated(hash = 1618865602)
    public MObservacionInstalacionAcometidaOutRO() {
    }

    public MObservacionInstalacionAcometidaOutRO(Long idObservacionInstalacionAcometida) {
        this.idObservacionInstalacionAcometida = idObservacionInstalacionAcometida;
    }

    @Generated(hash = 1281138065)
    public MObservacionInstalacionAcometidaOutRO(Long idObservacionInstalacionAcometida, Integer idInstalacionAcometida, Integer ordenObservacionInstalacionAcometida, String nombreTipoObservacion, String descripcionObservacionInstalacionAcometida, String estadoObservacionInstalacionAcometida, String nombreEstadoObservacionInstalacionAcometida, String resultCode, String message, String errorCode) {
        this.idObservacionInstalacionAcometida = idObservacionInstalacionAcometida;
        this.idInstalacionAcometida = idInstalacionAcometida;
        this.ordenObservacionInstalacionAcometida = ordenObservacionInstalacionAcometida;
        this.nombreTipoObservacion = nombreTipoObservacion;
        this.descripcionObservacionInstalacionAcometida = descripcionObservacionInstalacionAcometida;
        this.estadoObservacionInstalacionAcometida = estadoObservacionInstalacionAcometida;
        this.nombreEstadoObservacionInstalacionAcometida = nombreEstadoObservacionInstalacionAcometida;
        this.resultCode = resultCode;
        this.message = message;
        this.errorCode = errorCode;
    }

    public Long getIdObservacionInstalacionAcometida() {
        return idObservacionInstalacionAcometida;
    }

    public void setIdObservacionInstalacionAcometida(Long idObservacionInstalacionAcometida) {
        this.idObservacionInstalacionAcometida = idObservacionInstalacionAcometida;
    }

    public Integer getIdInstalacionAcometida() {
        return idInstalacionAcometida;
    }

    public void setIdInstalacionAcometida(Integer idInstalacionAcometida) {
        this.idInstalacionAcometida = idInstalacionAcometida;
    }

    public Integer getOrdenObservacionInstalacionAcometida() {
        return ordenObservacionInstalacionAcometida;
    }

    public void setOrdenObservacionInstalacionAcometida(Integer ordenObservacionInstalacionAcometida) {
        this.ordenObservacionInstalacionAcometida = ordenObservacionInstalacionAcometida;
    }

    public String getNombreTipoObservacion() {
        return nombreTipoObservacion;
    }

    public void setNombreTipoObservacion(String nombreTipoObservacion) {
        this.nombreTipoObservacion = nombreTipoObservacion;
    }

    public String getDescripcionObservacionInstalacionAcometida() {
        return descripcionObservacionInstalacionAcometida;
    }

    public void setDescripcionObservacionInstalacionAcometida(String descripcionObservacionInstalacionAcometida) {
        this.descripcionObservacionInstalacionAcometida = descripcionObservacionInstalacionAcometida;
    }

    public String getEstadoObservacionInstalacionAcometida() {
        return estadoObservacionInstalacionAcometida;
    }

    public void setEstadoObservacionInstalacionAcometida(String estadoObservacionInstalacionAcometida) {
        this.estadoObservacionInstalacionAcometida = estadoObservacionInstalacionAcometida;
    }

    public String getNombreEstadoObservacionInstalacionAcometida() {
        return nombreEstadoObservacionInstalacionAcometida;
    }

    public void setNombreEstadoObservacionInstalacionAcometida(String nombreEstadoObservacionInstalacionAcometida) {
        this.nombreEstadoObservacionInstalacionAcometida = nombreEstadoObservacionInstalacionAcometida;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
