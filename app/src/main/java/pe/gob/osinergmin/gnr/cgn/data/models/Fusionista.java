package pe.gob.osinergmin.gnr.cgn.data.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class Fusionista {

    @Id
    private Long id;
    private Integer idInspector;
    private String nombreInspector;
    private String nombreDocumentoIdentificacionInspector;

    @Generated(hash = 567202182)
    public Fusionista() {
    }

    public Fusionista(Long id) {
        this.id = id;
    }

    @Generated(hash = 911167122)
    public Fusionista(Long id, Integer idInspector, String nombreInspector, String nombreDocumentoIdentificacionInspector) {
        this.id = id;
        this.idInspector = idInspector;
        this.nombreInspector = nombreInspector;
        this.nombreDocumentoIdentificacionInspector = nombreDocumentoIdentificacionInspector;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdInspector() {
        return idInspector;
    }

    public void setIdInspector(Integer idInspector) {
        this.idInspector = idInspector;
    }

    public String getNombreInspector() {
        return nombreInspector;
    }

    public void setNombreInspector(String nombreInspector) {
        this.nombreInspector = nombreInspector;
    }

    public String getNombreDocumentoIdentificacionInspector() {
        return nombreDocumentoIdentificacionInspector;
    }

    public void setNombreDocumentoIdentificacionInspector(String nombreDocumentoIdentificacionInspector) {
        this.nombreDocumentoIdentificacionInspector = nombreDocumentoIdentificacionInspector;
    }
}
