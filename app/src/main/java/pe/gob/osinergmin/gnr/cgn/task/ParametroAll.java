package pe.gob.osinergmin.gnr.cgn.task;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import gob.osinergmin.gnr.domain.dto.rest.in.BaseInRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListParametroOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.util.Urls;

public class ParametroAll extends AsyncTask<Config, Void, ListParametroOutRO> {

    private OnParametroAllCompleted listener = null;

    public ParametroAll(OnParametroAllCompleted listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected ListParametroOutRO doInBackground(Config... configs) {
        try {

            Config config = configs[0];

            String url = Urls.getBase() + Urls.getParametroAll();

            BaseInRO baseInRO = new BaseInRO();
            baseInRO.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Authorization", "Bearer " + config.getToken());

            HttpEntity<BaseInRO> httpEntity = new HttpEntity<>(baseInRO, httpHeaders);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            return restTemplate.postForObject(url, httpEntity, ListParametroOutRO.class);

        } catch (RestClientException e) {
            Log.e("PARAMETROS", e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(ListParametroOutRO listParametroOutRO) {
        super.onPostExecute(listParametroOutRO);
        listener.onParametroAllCompled(listParametroOutRO);
    }

    public interface OnParametroAllCompleted {
        void onParametroAllCompled(ListParametroOutRO listParametroOutRO);
    }
}
