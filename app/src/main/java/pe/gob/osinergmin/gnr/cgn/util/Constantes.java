package pe.gob.osinergmin.gnr.cgn.util;

public class Constantes {


    public static final String ACTION_RUN_SERVICE = "pe.gob.osinergmin.gnr.cgn.service.action.RUN_SERVICE";
    public static final String ACTION_EXIT_SERVICE = "pe.gob.osinergmin.gnr.cgn.service.action.EXIT_SERVICE";
    public static final String ACTION_SERVICE = "pe.gob.osinergmin.gnr.cgn.service.action.SERVICE";
    public static final String ACTION_OFFLINE_ON = "pe.gob.osinergmin.gnr.cgn.service.action.OFFLINE_ON";
    public static final String ACTION_OFFLINE_OFF = "pe.gob.osinergmin.gnr.cgn.service.action.OFFLINE_OFF";
    public static final String ACTION_FOO = "pe.gob.osinergmin.gnr.cgn.service.action.FOO";
    public static final String ACTION_BAZ = "pe.gob.osinergmin.gnr.cgn.service.action.BAZ";

    public static final String EXTRA_PARAM1 = "pe.gob.osinergmin.gnr.cgn.service.extra.PARAM1";
    public static final String EXTRA_PARAM2 = "pe.gob.osinergmin.gnr.cgn.service.extra.PARAM2";

    public static final int INTENT_PERMISSION = 1;

    public static final String CHANNEL_ID = "my_channel_gnr_cgn";
    public static final String CHANNEL_NAME = "GNR CGN";
    public static final String CHANNEL_DESCRIPTION = "Canal de notificación GNR CGN";
}
