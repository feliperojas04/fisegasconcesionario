package pe.gob.osinergmin.gnr.cgn.actividad;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.google.android.material.textfield.TextInputLayout;

import gob.osinergmin.gnr.domain.dto.rest.out.EspecialAccessResponseOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.Suministro;
import pe.gob.osinergmin.gnr.cgn.task.LoginEspecial;
import pe.gob.osinergmin.gnr.cgn.util.NetworkUtil;
import pe.gob.osinergmin.gnr.cgn.util.Util;

public class IniciarEspecialActivity extends BaseActivity {

    private Suministro suministro;
    private Toolbar toolbar;
    private Config config;

    private TextInputLayout ediEsp_IngresarCodigo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iniciar_especial);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_36dp);

        suministro = getIntent().getExtras().getParcelable("SUMINISTRO");
        config = getIntent().getExtras().getParcelable("CONFIG");

        Button btnEsp_SolicitarCodigo = findViewById(R.id.btnEsp_SolicitarCodigo);
        ediEsp_IngresarCodigo = findViewById(R.id.ediEsp_IngresarCodigo);
        ediEsp_IngresarCodigo.setTypeface(Typeface.DEFAULT);
        Button btnEsp_Ingresar = findViewById(R.id.btnEsp_Ingresar);

        assert btnEsp_SolicitarCodigo != null;
        btnEsp_SolicitarCodigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + config.getParametro4()));
                if (ActivityCompat.checkSelfPermission(IniciarEspecialActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivityForResult(intent, 1);
            }
        });

        assert btnEsp_Ingresar != null;
        btnEsp_Ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verificarSesion();
            }
        });
        aceptarPermiso();
    }

    private void verificarSesion() {
        ediEsp_IngresarCodigo.setError(null);

        config.setTexto(ediEsp_IngresarCodigo.getEditText().getText().toString());

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(config.getTexto())) {
            ediEsp_IngresarCodigo.setError(getString(R.string.sesion_requerido));
            focusView = ediEsp_IngresarCodigo;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                showProgressDialog(R.string.app_cargando);
                LoginEspecial loginEspecial = new LoginEspecial(new LoginEspecial.OnLoginEspecialCompleted() {
                    @Override
                    public void onLoginEspecialCompleted(EspecialAccessResponseOutRO especialAccessResponseOutRO) {
                        hideProgressDialog();
                        if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                            ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                        } else if (especialAccessResponseOutRO == null) {
                            ((App) getApplication()).showToast(getResources().getString(R.string.sesion_error_clave));
                        } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(especialAccessResponseOutRO.getResultCode())) {
                            ((App) getApplication()).showToast(especialAccessResponseOutRO.getMessage() == null ? getResources().getString(R.string.sesion_error_clave) : especialAccessResponseOutRO.getMessage());
                        } else {
                            if (Constantes.VALOR_SI.equalsIgnoreCase(especialAccessResponseOutRO.getAccesoPermitido())) {
                                Intent intent = new Intent(IniciarEspecialActivity.this, HabilitacionActivity.class);
                                intent.putExtra("SUMINISTRO", suministro);
                                intent.putExtra("CONFIG", config);
                                startActivity(intent);
                                finish();
                            } else {
                                ((App) getApplication()).showToast("No se permite acceso");
                            }
                        }
                    }
                });
                loginEspecial.execute(config);
            } else {
                ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        toolbar.setTitle(R.string.txtEsp_titulo);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(IniciarEspecialActivity.this, MapasActivity.class);
                intent.putExtra("SUMINISTRO", suministro);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(IniciarEspecialActivity.this, MapasActivity.class);
        intent.putExtra("SUMINISTRO", suministro);
        intent.putExtra("CONFIG", config);
        startActivity(intent);
        finish();
    }

    private void aceptarPermiso() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permiso = Util.getPermisos(IniciarEspecialActivity.this,
                    Manifest.permission.CALL_PHONE);
            if (permiso != null) {
                requestPermissions(permiso, pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION:
                String message = Util.onRequestPermissionsResult(permissions, grantResults);
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
