package pe.gob.osinergmin.gnr.cgn.actividad;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import org.mapsforge.core.graphics.Bitmap;
import org.mapsforge.core.graphics.GraphicFactory;
import org.mapsforge.core.graphics.Paint;
import org.mapsforge.core.graphics.Style;
import org.mapsforge.core.model.LatLong;
import org.mapsforge.map.android.graphics.AndroidGraphicFactory;
import org.mapsforge.map.android.util.AndroidUtil;
import org.mapsforge.map.android.view.MapView;
import org.mapsforge.map.datastore.MapDataStore;
import org.mapsforge.map.layer.cache.TileCache;
import org.mapsforge.map.layer.overlay.Circle;
import org.mapsforge.map.layer.overlay.Marker;
import org.mapsforge.map.layer.renderer.TileRendererLayer;
import org.mapsforge.map.reader.MapFile;
import org.mapsforge.map.reader.header.MapFileException;
import org.mapsforge.map.rendertheme.InternalRenderTheme;

import java.io.File;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.HabilitacionOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.Suministro;
import pe.gob.osinergmin.gnr.cgn.data.models.SuministroDao;
import pe.gob.osinergmin.gnr.cgn.util.DownTimer;
import pe.gob.osinergmin.gnr.cgn.util.Util;

public class MapViewerActivity extends AppCompatActivity implements LocationListener {

    private static final String MAP_FILE = "peru.map";
    private static final GraphicFactory GRAPHIC_FACTORY = AndroidGraphicFactory.INSTANCE;
    private static String ruta = "/.CONCESIONARIO/";
    Location mlocation;
    LocationManager locationManager;
    private HabilitacionOutRO habilitacionOutRO;
    private Config config;
    private ProgressDialog dialog;
    private MenuItem menuItem;
    private Button btnMap_siguiente;
    private Boolean activo = true;
    private Toolbar toolbar;
    private SuministroDao suministroDao;
    private MParametroOutRODao mParametroOutRODao;
    private DownTimer countDownTimer;
    private MapView mapView;

    private static Paint getDefaultCircleFill() {
        return getPaint(GRAPHIC_FACTORY.createColor(48, 0, 0, 255), 0, Style.FILL);
    }

    private static Paint getDefaultCircleStroke() {
        return getPaint(GRAPHIC_FACTORY.createColor(160, 0, 0, 255), 2, Style.STROKE);
    }

    private static Paint getPaint(int color, int strokeWidth, Style style) {
        Paint paint = GRAPHIC_FACTORY.createPaint();
        paint.setColor(color);
        paint.setStrokeWidth(strokeWidth);
        paint.setStyle(style);
        return paint;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_viewer);

        AndroidGraphicFactory.createInstance(this.getApplication());
        countDownTimer = DownTimer.getInstance();

        suministroDao = ((App) getApplication()).getDaoSession().getSuministroDao();
        mParametroOutRODao = ((App) getApplication()).getDaoSession().getMParametroOutRODao();

        this.mapView = findViewById(R.id.mapView);
        toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_36dp);

        //this.mapView = new MapView(this);
        // setContentView(this.mapView);

        btnMap_siguiente = findViewById(R.id.btnMap_siguiente);
        btnMap_siguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                siguiente();
            }
        });
        locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        habilitacionOutRO = (HabilitacionOutRO) getIntent().getExtras().getSerializable("HABILITACION");
        config = getIntent().getExtras().getParcelable("CONFIG");

        String recurso = getResources().getString(R.string.txtMap_descripcion);
        String formateada = String.format(recurso, habilitacionOutRO.getDireccionUbigeoPredio(), habilitacionOutRO.getNumeroSuministroPredio());
        TextView texto = findViewById(R.id.txtMap_descripcionView);
        texto.setText(formateada);
        toolbar.setTitle(habilitacionOutRO.getNumeroSuministroPredio());

        this.mapView.setClickable(true);
        this.mapView.getMapScaleBar().setVisible(true);
        this.mapView.setBuiltInZoomControls(true);
        this.mapView.setZoomLevelMin((byte) 10);
        this.mapView.setZoomLevelMax((byte) 20);

        // create a tile cache of suitable size
        TileCache tileCache = AndroidUtil.createTileCache(this, "mapcache",
                mapView.getModel().displayModel.getTileSize(), 1f,
                this.mapView.getModel().frameBufferModel.getOverdrawFactor());
        try {
            // tile renderer layer using internal render theme
            MapDataStore mapDataStore = new MapFile(new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), ruta + MAP_FILE));
            TileRendererLayer tileRendererLayer = new TileRendererLayer(tileCache, mapDataStore,
                    this.mapView.getModel().mapViewPosition, AndroidGraphicFactory.INSTANCE);
            tileRendererLayer.setXmlRenderTheme(InternalRenderTheme.OSMARENDER);

            // only once a layer is associated with a mapView the rendering starts
            this.mapView.getLayerManager().getLayers().add(tileRendererLayer);
        } catch (MapFileException e) {
            ((App) getApplication()).showToast("Error al cargar el mapa, reiniciar la aplicación");
            Util.eliminarMap(MapViewerActivity.this);
            onBackPressed();
        }
        aceptarPermiso();
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.createAndStart();
        dialog = new ProgressDialog(this);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            activarGPS();
        } else {
            mapCircle();
        }
    }

    private void mapCircle() {
        Circle circle = new Circle(null, 0, getDefaultCircleFill(), getDefaultCircleStroke());
        String[] coordenadasPredio = habilitacionOutRO.getSolicitud().getCoordenadasPredio().split(",");
        LatLong latLong = new LatLong(Double.valueOf(coordenadasPredio[0]), Double.valueOf(coordenadasPredio[1]));
        circle.setLatLong(latLong);
        circle.setRadius(Float.parseFloat(config.getParametro2()));
        this.mapView.getLayerManager().getLayers().add(circle);
        this.mapView.setCenter(latLong);
        this.mapView.setZoomLevel((byte) 17);
    }

    private void mapMarker(LatLong latLong) {
        Drawable drawable = ContextCompat.getDrawable(this, R.mipmap.map_marker);
        Bitmap bitmap = AndroidGraphicFactory.convertToBitmap(drawable);
        if (this.mapView.getLayerManager() != null) {
            if (this.mapView.getLayerManager().getLayers().size() > 2) {
                this.mapView.getLayerManager().getLayers().remove(2);
            }
            Marker marker = new Marker(latLong, bitmap, 0, 0);
            this.mapView.getLayerManager().getLayers().add(marker);
        }
    }

    @Override
    protected void onDestroy() {
        this.mapView.destroyAll();
        AndroidGraphicFactory.clearResourceMemoryCache();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(MapViewerActivity.this);
    }

    private void isLocal() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }
        mlocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (mlocation != null) {
            mapMarker(new LatLong(mlocation.getLatitude(), mlocation.getLongitude()));
            String[] coordenadasTaller = habilitacionOutRO.getSolicitud().getCoordenadasPredio().split(",");
            Double latTaller = Math.abs(Double.valueOf(coordenadasTaller[0]));
            Double latActual = Math.abs(mlocation.getLatitude());
            Double lngTaller = Math.abs(Double.valueOf(coordenadasTaller[1]));
            Double lngActual = Math.abs(mlocation.getLongitude());
            if (Math.abs(latTaller - latActual) < Double.parseDouble(config.getParametro2()) / 125000 && Math.abs(lngTaller - lngActual) < Double.parseDouble(config.getParametro2()) / 125000) {
                btnMap_siguiente.setVisibility(View.VISIBLE);
            } else {
                btnMap_siguiente.setVisibility(View.GONE);
            }
        } else {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                btnMap_siguiente.setVisibility(View.GONE);
                activarEspecial();
            }
        }
    }

    private void siguiente() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.removeUpdates(MapViewerActivity.this);
        List suministroList = suministroDao.queryBuilder()
                .where(SuministroDao.Properties.IdHabilitacion.eq(habilitacionOutRO.getIdHabilitacion())).list();
        if (suministroList.size() > 0) {
            Suministro suministro = (Suministro) suministroList.get(0);
            Intent intent = new Intent(MapViewerActivity.this, HabilitacionActivity.class);
            intent.putExtra("SUMINISTRO", suministro);
            intent.putExtra("CONFIG", config);
            startActivity(intent);
            finish();
        } else {
            Suministro suministro = new Suministro(null,
                    habilitacionOutRO.getIdHabilitacion() != null ? habilitacionOutRO.getIdHabilitacion() : 0,
                    habilitacionOutRO.getDocumentoIdentificacionSolicitante() != null ? habilitacionOutRO.getDocumentoIdentificacionSolicitante() : "",
                    habilitacionOutRO.getNombreSolicitante() != null ? habilitacionOutRO.getNombreSolicitante() : "",
                    habilitacionOutRO.getDireccionUbigeoPredio() != null ? habilitacionOutRO.getDireccionUbigeoPredio() : "",
                    habilitacionOutRO.getNumeroIntentoHabilitacion() != null ? habilitacionOutRO.getNumeroIntentoHabilitacion().toString() : "",
                    habilitacionOutRO.getCodigoSolicitud() != null ? habilitacionOutRO.getCodigoSolicitud().toString() : "",
                    habilitacionOutRO.getNumeroContratoConcesionariaPredio() != null ? habilitacionOutRO.getNumeroContratoConcesionariaPredio() : "",
                    habilitacionOutRO.getSolicitud().getFechaSolicitud() != null ? habilitacionOutRO.getSolicitud().getFechaSolicitud() : "",
                    habilitacionOutRO.getSolicitud().getFechaAprobacionSolicitud() != null ? habilitacionOutRO.getSolicitud().getFechaAprobacionSolicitud() : "",
                    "",
                    habilitacionOutRO.getSolicitud().getNombreTipoProyectoInstalacion() != null ? habilitacionOutRO.getSolicitud().getNombreTipoProyectoInstalacion() : "",
                    habilitacionOutRO.getSolicitud().getNombreTipoInstalacion() != null ? habilitacionOutRO.getSolicitud().getNombreTipoInstalacion() : "",
                    habilitacionOutRO.getSolicitud().getNumeroPuntosInstalacion() != null ? habilitacionOutRO.getSolicitud().getNumeroPuntosInstalacion().toString() : "",
                    "",
                    habilitacionOutRO.getNumeroSuministroPredio() != null ? habilitacionOutRO.getNumeroSuministroPredio() : "",
                    "",
                    "",
                    "0",
                    "",
                    "",
                    "0",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "0",
                    "0",
                    "0",
                    "0",
                    "",
                    "",
                    "0",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "0",
                    "",
                    "",
                    "0",
                    "",
                    false,
                    "",
                    "0",
                    "",
                    "",
                    "",
                    "");
            suministroDao.insertOrReplace(suministro);

            Intent intent = new Intent(MapViewerActivity.this, HabilitacionActivity.class);
            intent.putExtra("SUMINISTRO", suministro);
            intent.putExtra("CONFIG", config);
            startActivity(intent);
            finish();
        }
    }

    private void activarGPS() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.txtMap_aviso);
        builder.setMessage(R.string.txtMap_desactivada);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.btnMap_si, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        });
        builder.setNegativeButton(R.string.btnMap_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    private void activarEspecial() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.txtMap_aviso);
        builder.setMessage(R.string.txtMap_especial);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.btnMap_si, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                menuItem.setVisible(false);
            }
        });
        builder.setNegativeButton(R.string.btnMap_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (Constantes.VALOR_SI.equalsIgnoreCase(config.getParametro3())) {
                    menuItem.setVisible(true);
                }
            }
        });
        builder.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        isLocal();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        btnMap_siguiente.setVisibility(View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_mapas, menu);
        menuItem = menu.findItem(R.id.menuMap_especial);
        menuItem.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                locationManager.removeUpdates(MapViewerActivity.this);
                Intent intent = new Intent(MapViewerActivity.this, BuscarSuministroActivity.class);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
                return true;
            case R.id.menuMap_especial:
                if (config.getOffline()) {
                    MParametroOutRO mParametroOutRO4 = mParametroOutRODao.
                            queryBuilder().
                            where(MParametroOutRODao.
                                    Properties.NombreParametro.
                                    eq("TELEFONO_SOLICITUD_CODIGO_ESPECIAL_ACCESO"))
                            .limit(1)
                            .unique();
                    config.setParametro4(mParametroOutRO4 == null ? "012193400" : mParametroOutRO4.getValorParametro());
                    locationManager.removeUpdates(MapViewerActivity.this);
                    List suministroList = suministroDao.queryBuilder()
                            .where(SuministroDao.Properties.IdHabilitacion.eq(habilitacionOutRO.getIdHabilitacion())).list();
                    if (suministroList.size() > 0) {
                        Suministro suministro = (Suministro) suministroList.get(0);
                        Intent intent2 = new Intent(MapViewerActivity.this, IniciarEspecialActivity.class);
                        intent2.putExtra("SUMINISTRO", suministro);
                        intent2.putExtra("CONFIG", config);
                        startActivity(intent2);
                        finish();
                    } else {
                        Suministro suministro = new Suministro(null,
                                habilitacionOutRO.getIdHabilitacion() != null ? habilitacionOutRO.getIdHabilitacion() : 0,
                                habilitacionOutRO.getDocumentoIdentificacionSolicitante() != null ? habilitacionOutRO.getDocumentoIdentificacionSolicitante() : "",
                                habilitacionOutRO.getNombreSolicitante() != null ? habilitacionOutRO.getNombreSolicitante() : "",
                                habilitacionOutRO.getDireccionUbigeoPredio() != null ? habilitacionOutRO.getDireccionUbigeoPredio() : "",
                                habilitacionOutRO.getNumeroIntentoHabilitacion() != null ? habilitacionOutRO.getNumeroIntentoHabilitacion().toString() : "",
                                habilitacionOutRO.getCodigoSolicitud() != null ? habilitacionOutRO.getCodigoSolicitud().toString() : "",
                                habilitacionOutRO.getNumeroContratoConcesionariaPredio() != null ? habilitacionOutRO.getNumeroContratoConcesionariaPredio() : "",
                                habilitacionOutRO.getSolicitud().getFechaSolicitud() != null ? habilitacionOutRO.getSolicitud().getFechaSolicitud() : "",
                                habilitacionOutRO.getSolicitud().getFechaAprobacionSolicitud() != null ? habilitacionOutRO.getSolicitud().getFechaAprobacionSolicitud() : "",
                                "",
                                habilitacionOutRO.getSolicitud().getNombreTipoProyectoInstalacion() != null ? habilitacionOutRO.getSolicitud().getNombreTipoProyectoInstalacion() : "",
                                habilitacionOutRO.getSolicitud().getNombreTipoInstalacion() != null ? habilitacionOutRO.getSolicitud().getNombreTipoInstalacion() : "",
                                habilitacionOutRO.getSolicitud().getNumeroPuntosInstalacion() != null ? habilitacionOutRO.getSolicitud().getNumeroPuntosInstalacion().toString() : "",
                                "",
                                habilitacionOutRO.getNumeroSuministroPredio() != null ? habilitacionOutRO.getNumeroSuministroPredio() : "",
                                "",
                                "",
                                "0",
                                "",
                                "",
                                "0",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "0",
                                "0",
                                "0",
                                "0",
                                "",
                                "",
                                "0",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "0",
                                "",
                                "",
                                "0",
                                "",
                                false,
                                "",
                                "0",
                                "",
                                "",
                                "",
                                "");
                        suministroDao.insertOrReplace(suministro);

                        Intent intent3 = new Intent(MapViewerActivity.this, IniciarEspecialActivity.class);
                        intent3.putExtra("SUMINISTRO", suministro);
                        intent3.putExtra("CONFIG", config);
                        startActivity(intent3);
                        finish();
                    }
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        countDownTimer.createAndStart();
    }

    @Override
    public void onBackPressed() {
        locationManager.removeUpdates(MapViewerActivity.this);
        Intent intent = new Intent(MapViewerActivity.this, BuscarSuministroActivity.class);
        intent.putExtra("CONFIG", config);
        startActivity(intent);
        finish();
    }

    private void aceptarPermiso() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permiso = Util.getPermisos(MapViewerActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION);
            if (permiso != null) {
                requestPermissions(permiso, pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION:
                String message = Util.onRequestPermissionsResult(permissions, grantResults);
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
