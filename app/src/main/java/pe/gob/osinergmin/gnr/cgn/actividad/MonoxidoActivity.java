package pe.gob.osinergmin.gnr.cgn.actividad;

import android.Manifest;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;

import java.io.File;
import java.util.List;

import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.adaptador.MonoxidoAdaptador;
import pe.gob.osinergmin.gnr.cgn.data.models.Ambiente;
import pe.gob.osinergmin.gnr.cgn.data.models.AmbienteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.Precision;
import pe.gob.osinergmin.gnr.cgn.data.models.PrecisionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Suministro;
import pe.gob.osinergmin.gnr.cgn.data.models.SuministroDao;
import pe.gob.osinergmin.gnr.cgn.util.DownTimer;
import pe.gob.osinergmin.gnr.cgn.util.ReducirFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.Util;

public class MonoxidoActivity extends AppCompatActivity implements MonoxidoAdaptador.Callback {

    private Suministro suministro;
    private Config config;
    private TableLayout tablaMonoxido;
    private TextView txtDet_DocumentoPropietarioView;
    private TextView txtDet_nombrePropietarioView;
    private TextView txtDet_PredioView;
    private TextView txtDet_numeroIntHabilitacionView;
    private TextView txtDet_numeroSolicitudView;
    private TextView txtDet_numeroContratoView;
    private TextView txtDet_fechaSolicitudView;
    private TextView txtDet_fechaAprobacionView;
    private TextView txtDet_tipoProyectoView;
    private TextView txtDet_tipoInstalacionView;
    private TextView txtDet_numeroPuntosInstaacionView;
    private ListView listaMonoxido;
    private SwitchCompat swiPrueM_aprueba;
    private Button btnPrueM_rechazar;
    private Toolbar toolbar;
    private MenuItem menuMas;
    private MenuItem menuMenos;
    private SuministroDao suministroDao;
    private AmbienteDao ambienteDao;
    private DownTimer countDownTimer;
    private MParametroOutRODao mParametroOutRODao;
    private String tmpRuta;
    private int position;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private PrecisionDao precisionDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monoxido);

        countDownTimer = DownTimer.getInstance();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        suministroDao = ((App) getApplication()).getDaoSession().getSuministroDao();
        ambienteDao = ((App) getApplication()).getDaoSession().getAmbienteDao();
        mParametroOutRODao = ((App) getApplication()).getDaoSession().getMParametroOutRODao();
        precisionDao = ((App) getApplication()).getDaoSession().getPrecisionDao();

        toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_36dp);

        suministro = getIntent().getExtras().getParcelable("SUMINISTRO");
        config = getIntent().getExtras().getParcelable("CONFIG");

        tablaMonoxido = findViewById(R.id.tablaMonoxido);
        txtDet_DocumentoPropietarioView = findViewById(R.id.txtDet_DocumentoPropietarioView);
        txtDet_nombrePropietarioView = findViewById(R.id.txtDet_nombrePropietarioView);
        txtDet_PredioView = findViewById(R.id.txtDet_PredioView);
        txtDet_numeroIntHabilitacionView = findViewById(R.id.txtDet_numeroIntHabilitacionView);
        txtDet_numeroSolicitudView = findViewById(R.id.txtDet_numeroSolicitudView);
        txtDet_numeroContratoView = findViewById(R.id.txtDet_numeroContratoView);
        txtDet_fechaSolicitudView = findViewById(R.id.txtDet_fechaSolicitudView);
        txtDet_fechaAprobacionView = findViewById(R.id.txtDet_fechaAprobacionView);
        txtDet_tipoProyectoView = findViewById(R.id.txtDet_tipoProyectoView);
        txtDet_tipoInstalacionView = findViewById(R.id.txtDet_tipoInstalacionView);
        txtDet_numeroPuntosInstaacionView = findViewById(R.id.txtDet_numeroPuntosInstaacionView);
        listaMonoxido = findViewById(R.id.listaMonoxido);
        swiPrueM_aprueba = findViewById(R.id.swiPrueM_aprueba);
        Button btnPrueM_evaluando = findViewById(R.id.btnPrueM_evaluando);
        btnPrueM_rechazar = findViewById(R.id.btnPrueM_rechazar);

        swiPrueM_aprueba.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    suministro.setMonoxidoAprueba("1");
                    btnPrueM_rechazar.setVisibility(View.GONE);
                } else {
                    suministro.setMonoxidoAprueba("0");
                    btnPrueM_rechazar.setVisibility(View.VISIBLE);
                }
            }
        });

        btnPrueM_evaluando.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suministroDao.insertOrReplace(suministro);
                Intent intent = new Intent(MonoxidoActivity.this, HabilitacionActivity.class);
                intent.putExtra("SUMINISTRO", suministro);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
            }
        });

        btnPrueM_rechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MonoxidoActivity.this, RechazoActivity.class);
                intent.putExtra("SUMINISTRO", suministro);
                intent.putExtra("CONFIG", config);
                intent.putExtra("TIPO", "");
                startActivity(intent);
            }
        });
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    Precision precision = new Precision();
                    precision.setIdSolicitud(suministro.getIdHabilitacion());
                    precision.setLatitud(String.valueOf(location.getLatitude()));
                    precision.setLongitud(String.valueOf(location.getLongitude()));
                    precision.setPrecision(String.valueOf(location.getAccuracy()));
                    precision.setEstado("1");
                    precisionDao.insertOrReplace(precision);
                }
            }
        };
        aceptarPermiso();
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.createAndStart();
        createLocationRequest();
        toolbar.setTitle(suministro.getSuministro() + " - Prueba de monóxido");
        cargarSuministro();
        swiPrueM_aprueba.setClickable(false);
        cargaDatos();
        if (config.getOffline()) {
            MParametroOutRO mParametroOutRO = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_CONFIGURACION_COMPRESION_FOTOS_APLICATIVO_MOVIL))
                    .limit(1).unique();
            config.setParametro5(mParametroOutRO == null ? "800x600" : mParametroOutRO.getValorParametro());
            MParametroOutRO mParametroOutPrecision = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_FRECUENCIA_TOMA_COORDENADAS_AUDITORIA))
                    .limit(1).unique();
            config.setPrecision(mParametroOutPrecision == null ? "10000" : mParametroOutPrecision.getValorParametro());
        } else {
            if ("".equalsIgnoreCase(config.getParametro5())) {
                config.setParametro5("800x600");
            }
            if ("".equalsIgnoreCase(config.getPrecision())) {
                config.setPrecision("10000");
            }
        }
    }

    @Override
    public void clicFoto(int position) {
        Log.d("clicFoto", String.valueOf(position));
        if (ActivityCompat.checkSelfPermission(
                MonoxidoActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ((App) getApplication()).showToast("No puede realizar la acción porque no se ha dado el permiso de Almacenamiento");
            return;
        }
        if (Util.isExternalStorageWritable()) {
            if (Util.getAlbumStorageDir(MonoxidoActivity.this)) {
                File file = Util.getNuevaRutaFoto(suministro.getIdHabilitacion().toString(), MonoxidoActivity.this);
                tmpRuta = file.getPath();
                this.position = position;
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                Uri outputUri = FileProvider.getUriForFile(MonoxidoActivity.this, getApplicationContext().getPackageName() + ".provider", file);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    ClipData clip = ClipData.newUri(getContentResolver(), "Instalacion", outputUri);
                    intent.setClipData(clip);
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                }
                startActivityForResult(intent, 1);
            } else {
                ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
            }
        } else {
            ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
        }
    }

    private void cargaDatos() {
        List<Ambiente> ambientes = ambienteDao.queryBuilder()
                .where(AmbienteDao.Properties.IdHabilitacion.eq(suministro.getIdHabilitacion())).list();
        if (ambientes.size() > 0) {
            MonoxidoAdaptador monoxidoAdaptador = new MonoxidoAdaptador(this, ambientes);
            monoxidoAdaptador.setCallback(this);
            monoxidoAdaptador.setAprueba(swiPrueM_aprueba);
            monoxidoAdaptador.setAmbienteDao(ambienteDao);
            listaMonoxido.setAdapter(monoxidoAdaptador);
            Util.setListViewHeightBasedOnChildren(listaMonoxido);
        } else {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MonoxidoActivity.this);
            alertDialogBuilder.setTitle("Alerta");
            alertDialogBuilder.setMessage("Primero debe ingresar datos de Ambientes").setCancelable(false);
            alertDialogBuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(MonoxidoActivity.this, AmbienteActivity.class);
                    intent.putExtra("SUMINISTRO", suministro);
                    intent.putExtra("CONFIG", config);
                    startActivity(intent);
                    finish();
                }
            });
            alertDialogBuilder.show();
        }
        if ("1".equalsIgnoreCase(suministro.getMonoxidoAprueba())) {
            swiPrueM_aprueba.setChecked(true);
        }
    }

    private void cargarSuministro() {
        txtDet_DocumentoPropietarioView.setText(suministro.getDocumentoPropietario());
        txtDet_nombrePropietarioView.setText(suministro.getNombrePropietario());
        txtDet_PredioView.setText(suministro.getPredio());
        txtDet_numeroIntHabilitacionView.setText(suministro.getNumeroIntHabilitacion());
        txtDet_numeroSolicitudView.setText(suministro.getNumeroSolicitud());
        txtDet_numeroContratoView.setText(suministro.getNumeroContrato());
        txtDet_fechaSolicitudView.setText(suministro.getFechaSolicitud());
        txtDet_fechaAprobacionView.setText(suministro.getFechaAprobacion());
        txtDet_tipoProyectoView.setText(suministro.getTipoProyecto());
        txtDet_tipoInstalacionView.setText(suministro.getTipoInstalacion());
        txtDet_numeroPuntosInstaacionView.setText(suministro.getNumeroPuntosInstaacion());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_general, menu);
        menuMas = menu.findItem(R.id.menuMas);
        menuMas.setVisible(true);
        menuMenos = menu.findItem(R.id.menuMenos);
        menuMenos.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                suministroDao.insertOrReplace(suministro);
                Intent intent = new Intent(MonoxidoActivity.this, HabilitacionActivity.class);
                intent.putExtra("SUMINISTRO", suministro);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
                return true;
            case R.id.menuMas:
                tablaMonoxido.setVisibility(View.VISIBLE);
                menuMas.setVisible(false);
                menuMenos.setVisible(true);
                return true;

            case R.id.menuMenos:
                tablaMonoxido.setVisibility(View.GONE);
                menuMas.setVisible(true);
                menuMenos.setVisible(false);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1458) {
            if (resultCode == RESULT_CANCELED) {
                Intent intent = new Intent(MonoxidoActivity.this, BuscarSuministroActivity.class);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
            }
        }
        if (requestCode == 1) {
            if (resultCode != RESULT_CANCELED) {
                ((MonoxidoAdaptador) listaMonoxido.getAdapter()).getAmbiente(position).setMonoxidoFoto(tmpRuta);
                ReducirFotoTask reducirFotoTask = new ReducirFotoTask(new ReducirFotoTask.OnReducirFotoTaskCompleted() {
                    @Override
                    public void onReducirFotoTaskCompleted(String result) {
                        if (!("1".equalsIgnoreCase(result))) {
                            ((App) getApplication()).showToast(result);
                        }
                    }
                });
                ambienteDao.insertOrReplace(((MonoxidoAdaptador) listaMonoxido.getAdapter()).getAmbiente(position));
                reducirFotoTask.execute(((MonoxidoAdaptador) listaMonoxido.getAdapter()).getAmbiente(position).getMonoxidoFoto()
                        , config.getParametro5());
                ((MonoxidoAdaptador) listaMonoxido.getAdapter()).notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onBackPressed() {
        suministroDao.insertOrReplace(suministro);
        Intent intent = new Intent(MonoxidoActivity.this, HabilitacionActivity.class);
        intent.putExtra("SUMINISTRO", suministro);
        intent.putExtra("CONFIG", config);
        startActivity(intent);
        finish();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        countDownTimer.createAndStart();
    }

    private void createLocationRequest() {
        int value = 10000;
        try {
            value = Integer.valueOf(config.getPrecision());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(value);
        mLocationRequest.setFastestInterval(value);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(MonoxidoActivity.this, 1458);
                    } catch (IntentSender.SendIntentException sendEx) {
                    }
                }
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            aceptarPermiso();
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
    }

    private void aceptarPermiso() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permiso = Util.getPermisos(MonoxidoActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION);
            if (permiso != null) {
                requestPermissions(permiso, pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION:
                String message = Util.onRequestPermissionsResult(permissions, grantResults);
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }
}
