package pe.gob.osinergmin.gnr.cgn.actividad;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toolbar;

import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.util.SearchableSpinner;

public class AcometidaNuevaActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private LinearLayout llAcomDatosG;
    private TextView txtTub_tuberia;
    private ImageView icoAco_fusionista;
    private TextView txtAco_fusionista;
    private SearchableSpinner spiAco_fusionista;
    private ImageView icoAco_resultado;
    private TextView txtAco_resultado;
    private Spinner spiAco_resultado;
    private RecyclerView recy_acometidas;
    private LinearLayout llAcomUbicacion;
    private TextView txtUbiAcom;
    private ImageView icoAco_acta;
    private TextView txtAco_acta;
    private ImageView imgAco_acta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acometida_nueva);

        toolbar = findViewById(R.id.toolbar);
        llAcomDatosG = findViewById(R.id.llAcomDatosG);
        txtTub_tuberia = findViewById(R.id.txtTub_tuberia);
        icoAco_fusionista = findViewById(R.id.icoAco_fusionista);
        txtAco_fusionista = findViewById(R.id.txtAco_fusionista);
        spiAco_fusionista = findViewById(R.id.spiAco_fusionista);
        icoAco_resultado = findViewById(R.id.icoAco_resultado);
        txtAco_resultado = findViewById(R.id.txtAco_resultado);
        spiAco_resultado = findViewById(R.id.spiAco_resultado);
        recy_acometidas = findViewById(R.id.recy_acometidas);
        llAcomUbicacion = findViewById(R.id.llAcomUbicacion);
        txtUbiAcom = findViewById(R.id.txtUbiAcom);
        icoAco_acta = findViewById(R.id.icoAco_acta);
        txtAco_acta = findViewById(R.id.txtAco_acta);
        imgAco_acta = findViewById(R.id.imgAco_acta);

    }
}