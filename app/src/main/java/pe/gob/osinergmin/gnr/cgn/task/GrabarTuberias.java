package pe.gob.osinergmin.gnr.cgn.task;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import gob.osinergmin.gnr.domain.dto.rest.in.InstalacionAcometidaInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.InstalacionAcometidaTuberiaConexionProyectoInstalacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.InstalacionTuberiaConexionProyectoInstalacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.ObservacionInstalacionAcometidaInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.PuntoInstalacionHabilitacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.list.ListInstalacionTuberiaConexionProyectoInstalacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.list.ListObservacionInstalacionAcometidaInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.list.ListPuntoInstalacionHabilitacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.out.InstalacionAcometidaOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.data.models.Acometida;
import pe.gob.osinergmin.gnr.cgn.data.models.AcometidaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.ConfigDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Observacion;
import pe.gob.osinergmin.gnr.cgn.data.models.ObservacionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Precision;
import pe.gob.osinergmin.gnr.cgn.data.models.PrecisionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Punto;
import pe.gob.osinergmin.gnr.cgn.data.models.ResponseInstalacionAcometida;
import pe.gob.osinergmin.gnr.cgn.data.models.Tuberias;
import pe.gob.osinergmin.gnr.cgn.data.models.TuberiasDao;
import pe.gob.osinergmin.gnr.cgn.util.Urls;

import static gob.osinergmin.gnr.util.Constantes.RESULTADO_INSTALACION_ACOMETIDA_CONCLUIDA;

public class GrabarTuberias extends AsyncTask<Acometida, Void, ResponseInstalacionAcometida> {

    private final WeakReference<Context> context;
    private OnGrabarAcometidaAsyncTaskCompleted listener = null;
    private Config config;
    private ConfigDao configDao;
    private ObservacionDao observacionDao;
    private AcometidaDao acometidaDao;
    private PrecisionDao precisionDao;
    private TuberiasDao tuberiasDao;
    private List<Tuberias> tuberiasList = new ArrayList<>();

    public GrabarTuberias(OnGrabarAcometidaAsyncTaskCompleted listener, Context context,List<Tuberias> tuberiasList) {
        this.listener = listener;
        this.context = new WeakReference<>(context);
        this.tuberiasList = tuberiasList;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Context context = this.context.get();
        if (context != null) {
            observacionDao = ((App) context.getApplicationContext()).getDaoSession().getObservacionDao();
            tuberiasDao = ((App) context.getApplicationContext()).getDaoSession().getTuberiasDao();
            acometidaDao = ((App) context.getApplicationContext()).getDaoSession().getAcometidaDao();
            configDao = ((App) context.getApplicationContext()).getDaoSession().getConfigDao();
            config = configDao.queryBuilder().limit(1).unique();
            precisionDao = ((App) context.getApplicationContext()).getDaoSession().getPrecisionDao();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected ResponseInstalacionAcometida doInBackground(Acometida... acometidas) {
        try {
            Acometida acometida = acometidas[0];
            Precision precision = precisionDao.queryBuilder().where(PrecisionDao.Properties.Estado.eq("3"), PrecisionDao.Properties.IdSolicitud.eq(acometida.getIdInstalacionAcometida())).orderAsc(PrecisionDao.Properties.Precision).limit(1).unique();
            String url = Urls.getBase() + Urls.getTuberiaRegistrar();

            System.out.println("Tuberias 0: " +tuberiasList.get(0).getResultado()+" "+tuberiasList.get(0).getFusionista() +" "+tuberiasList.get(0).getLongitud() +" "+tuberiasList.get(0).getFotoTC());
            System.out.println("Tuberias 1: " +tuberiasList.get(1).getResultado()+" "+tuberiasList.get(1).getFusionista() +" "+tuberiasList.get(1).getLongitud() +" "+tuberiasList.get(1).getFotoTC());
            System.out.println("Numero de registros Tuberias: " + tuberiasList.size());

            InstalacionAcometidaTuberiaConexionProyectoInstalacionInRO instalacionInRO = new InstalacionAcometidaTuberiaConexionProyectoInstalacionInRO();
            instalacionInRO.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);
            instalacionInRO.setIdInstalacionAcometida(acometida.getIdInstalacionAcometida());
            instalacionInRO.setIdProyectoInstalacion(Integer.parseInt(config.getIdProyectoInstalacion()));
            instalacionInRO.setResultadoInstalacionAcometida("C");
            instalacionInRO.setTipoGabinete("C");
            List<Tuberias> tuberias = tuberiasDao.queryBuilder().where(TuberiasDao.Properties.IdInstalacionAcometida.eq(acometida.getIdInstalacionAcometida())).list();
            //List<Tuberias> tuberias = tuberiasDao.queryBuilder().list();
            System.out.println("Numero de registros2: " + tuberias.size());
            MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
            if (tuberias.size() > 0) {
                //if (tuberiasList.size() > 0) {
                    ListInstalacionTuberiaConexionProyectoInstalacionInRO listPuntoInstalacionHabilitacionInRO = new ListInstalacionTuberiaConexionProyectoInstalacionInRO();
                    List<InstalacionTuberiaConexionProyectoInstalacionInRO> puntosInstalacion = new ArrayList<>();
                    int i = 1;
                    //for (int i = 0; i <= tuberiasList.size(); i++) {
                    for (Tuberias tuberia : tuberias){
                        if (RESULTADO_INSTALACION_ACOMETIDA_CONCLUIDA.equalsIgnoreCase(tuberia.getResultado())) {
                            InstalacionTuberiaConexionProyectoInstalacionInRO puntoTMP = new InstalacionTuberiaConexionProyectoInstalacionInRO();
                            puntoTMP.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);
                            puntoTMP.setOrdenTuberiaConexionInstalacionAcometida(i);
                            puntoTMP.setIdfusionistaRegistro(Integer.parseInt("".equalsIgnoreCase(tuberia.getFusionista()) ? null : tuberia.getFusionista()));
                            //puntoTMP.setIdfusionistaRegistro(5445);
                            puntoTMP.setIdMaterialInstalacion("".equalsIgnoreCase(tuberia.getMaterial()) ? null : Integer.parseInt(tuberia.getMaterial()));
                            System.out.println("material: " + tuberia.getMaterial());
                            //puntoTMP.setIdMaterialInstalacion(1);
                            puntoTMP.setDiametroTuberiaConexionInstalacionAcometida("".equalsIgnoreCase(tuberia.getDiametro()) ? null : BigDecimal.valueOf(Float.parseFloat(tuberia.getDiametro())));
                            puntoTMP.setLongitudTuberiaConexionInstalacionAcometida("".equalsIgnoreCase(tuberia.getLongitud()) ? null : BigDecimal.valueOf(Float.parseFloat(tuberia.getLongitud())));
                            puntosInstalacion.add(puntoTMP);
                            parts.add("fotoTuberiaConexionTerminada", new FileSystemResource(tuberia.getFotoTC()));
                        } else {
                            List<Observacion> observacions = observacionDao.queryBuilder().where(ObservacionDao.Properties.IdInstalacionMontante.eq(acometida.getIdInstalacionAcometida())).list();
                            ListObservacionInstalacionAcometidaInRO listObservacionInstalacionAcometidaInRO = new ListObservacionInstalacionAcometidaInRO();
                            List<ObservacionInstalacionAcometidaInRO> observacionInstalacionAcometidaInROS = new ArrayList<>();
                            int k = 1;
                            for (Observacion observacion : observacions) {
                                ObservacionInstalacionAcometidaInRO tmp = new ObservacionInstalacionAcometidaInRO();
                                tmp.setOrdenObservacionInstalacionAcometida(k);
                                tmp.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);
                                tmp.setIdTipoObservacion(Integer.parseInt(observacion.getTipoObservacion()));
                                tmp.setDescripcionObservacionInstalacionAcometida(observacion.getDescripcionObservacion());
                                observacionInstalacionAcometidaInROS.add(tmp);
                                k++;
                            }
                            listObservacionInstalacionAcometidaInRO.setObservacionInstalacionAcometida(observacionInstalacionAcometidaInROS);
                            listObservacionInstalacionAcometidaInRO.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);
                            instalacionInRO.setObservaciones(listObservacionInstalacionAcometidaInRO);
                        }
                        listPuntoInstalacionHabilitacionInRO.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);
                        listPuntoInstalacionHabilitacionInRO.setInstalacionTuberiaConexionProyectoInstalacionInRO(puntosInstalacion);
                        instalacionInRO.setInstalacionTuberiaConexionProyectoInstalacion(listPuntoInstalacionHabilitacionInRO);
                    }
                }

            instalacionInRO.setFechaRegistroOfflineInstalacionAcometida(acometida.getFechaRegistroOffline());
            instalacionInRO.setCoordenadasAuditoriaInstalacionAcometida(String.format("%s,%s", precision.getLatitud(), precision.getLongitud()));
            instalacionInRO.setPrecisionCoordenadasAuditoriaInstalacionAcometida(BigDecimal.valueOf(Double.valueOf(precision.getPrecision())));
            instalacionInRO.setInstalacionAcometidaProyectoInstalacionInRO(null);

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
            httpHeaders.add("Authorization", "Bearer " + config.getToken());
            parts.add(Constantes.PARAM_NAME_INSTALACION_ACOMETIDA, instalacionInRO);

            ObjectMapper mapper = new ObjectMapper();String jsonString = mapper.writeValueAsString(instalacionInRO);
            System.out.println("Json: "+jsonString);
            ObjectMapper mapper2 = new ObjectMapper();String jsonString2 = mapper.writeValueAsString(parts);
            System.out.println("fotos: "+jsonString2);

            HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(parts, httpHeaders);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<ResponseInstalacionAcometida> otro = restTemplate.postForEntity(url, httpEntity, ResponseInstalacionAcometida.class);
            return restTemplate.postForObject(url, httpEntity, ResponseInstalacionAcometida.class);
        } catch (RestClientException | NullPointerException | IllegalArgumentException | JsonProcessingException e) {
            Log.e("GRABAR", e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(ResponseInstalacionAcometida instalacionAcometidaOutRO) {
        super.onPostExecute(instalacionAcometidaOutRO);
        listener.onGrabarAcometidaAsyncTaskCompleted(instalacionAcometidaOutRO);
    }

    public interface OnGrabarAcometidaAsyncTaskCompleted {
        void onGrabarAcometidaAsyncTaskCompleted(ResponseInstalacionAcometida instalacionAcometidaOutRO);
    }

}
