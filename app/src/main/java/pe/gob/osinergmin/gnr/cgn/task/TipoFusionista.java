package pe.gob.osinergmin.gnr.cgn.task;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import gob.osinergmin.gnr.domain.dto.rest.in.BaseInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.filter.FilterGenericInRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListBaseInspectorOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.util.Urls;

public class TipoFusionista extends AsyncTask<Config, Void, ListBaseInspectorOutRO> {

    private OnTipoFusionistaCompleted listener = null;

    public TipoFusionista(OnTipoFusionistaCompleted listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected ListBaseInspectorOutRO doInBackground(Config... configs) {
        try {

            Config config = configs[0];

            String url = Urls.getBase() + Urls.getListaFusionista();

            FilterGenericInRO filter = new FilterGenericInRO();
            filter.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);
            filter.setTexto("");

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Authorization", "Bearer " + config.getToken());

            HttpEntity<BaseInRO> httpEntity = new HttpEntity<>(filter, httpHeaders);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            return restTemplate.postForObject(url, httpEntity, ListBaseInspectorOutRO.class);

        } catch (RestClientException e) {
            Log.e("TIPOFUSIONISTA", e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(ListBaseInspectorOutRO listBaseInspectorOutRO) {
        super.onPostExecute(listBaseInspectorOutRO);
        listener.onTipoFusionistaCompled(listBaseInspectorOutRO);
    }

    public interface OnTipoFusionistaCompleted {
        void onTipoFusionistaCompled(ListBaseInspectorOutRO listBaseInspectorOutRO);
    }
}
