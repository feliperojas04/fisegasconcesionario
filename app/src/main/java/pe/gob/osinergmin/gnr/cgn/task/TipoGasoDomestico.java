package pe.gob.osinergmin.gnr.cgn.task;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import gob.osinergmin.gnr.domain.dto.rest.in.BaseInRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListGenericBeanOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.util.Urls;

public class TipoGasoDomestico extends AsyncTask<Config, Void, ListGenericBeanOutRO> {

    private OnGasoDomesticoCompleted listener = null;

    public TipoGasoDomestico(OnGasoDomesticoCompleted listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected ListGenericBeanOutRO doInBackground(Config... configs) {
        try {

            Config config = configs[0];

            String url = Urls.getBase() + Urls.getListaGasodomestico();

            BaseInRO baseInRO = new BaseInRO();
            baseInRO.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Authorization", "Bearer " + config.getToken());

            HttpEntity<BaseInRO> httpEntity = new HttpEntity<>(baseInRO, httpHeaders);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            return restTemplate.postForObject(url, httpEntity, ListGenericBeanOutRO.class);

        } catch (RestClientException e) {
            Log.e("GASODOMESTICO", e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(ListGenericBeanOutRO listGenericBeanOutRO) {
        super.onPostExecute(listGenericBeanOutRO);
        listener.onGasoDomesticoCompled(listGenericBeanOutRO);
    }

    public interface OnGasoDomesticoCompleted {
        void onGasoDomesticoCompled(ListGenericBeanOutRO listGenericBeanOutRO);
    }
}
