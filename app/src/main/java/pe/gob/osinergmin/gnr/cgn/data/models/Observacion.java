package pe.gob.osinergmin.gnr.cgn.data.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class Observacion {

    @Id
    private Long id;
    private Integer idHabilitacion;
    private Integer idInstalacionMontante;
    private String tipoObservacion;
    private String descripcionObservacion;

    @Generated(hash = 932284500)
    public Observacion() {
    }

    public Observacion(Long id) {
        this.id = id;
    }

    @Generated(hash = 1964511184)
    public Observacion(Long id, Integer idHabilitacion, Integer idInstalacionMontante, String tipoObservacion, String descripcionObservacion) {
        this.id = id;
        this.idHabilitacion = idHabilitacion;
        this.idInstalacionMontante = idInstalacionMontante;
        this.tipoObservacion = tipoObservacion;
        this.descripcionObservacion = descripcionObservacion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdHabilitacion() {
        return idHabilitacion;
    }

    public void setIdHabilitacion(Integer idHabilitacion) {
        this.idHabilitacion = idHabilitacion;
    }

    public Integer getIdInstalacionMontante() {
        return idInstalacionMontante;
    }

    public void setIdInstalacionMontante(Integer idInstalacionMontante) {
        this.idInstalacionMontante = idInstalacionMontante;
    }

    public String getTipoObservacion() {
        return tipoObservacion;
    }

    public void setTipoObservacion(String tipoObservacion) {
        this.tipoObservacion = tipoObservacion;
    }

    public String getDescripcionObservacion() {
        return descripcionObservacion;
    }

    public void setDescripcionObservacion(String descripcionObservacion) {
        this.descripcionObservacion = descripcionObservacion;
    }
}
