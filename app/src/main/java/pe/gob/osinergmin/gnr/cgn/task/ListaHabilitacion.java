package pe.gob.osinergmin.gnr.cgn.task;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import gob.osinergmin.gnr.domain.dto.rest.in.filter.FilterHabilitacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListHabilitacionOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.util.Urls;

public class ListaHabilitacion extends AsyncTask<Config, Void, ListHabilitacionOutRO> {

    private OnListaHabilitacionCompleted listener = null;

    public ListaHabilitacion(OnListaHabilitacionCompleted listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected ListHabilitacionOutRO doInBackground(Config... configs) {
        try {

            Config config = configs[0];

            String url = Urls.getBase() + Urls.getListaHabilitacionAll();

            FilterHabilitacionInRO filterHabilitacionInRO = new FilterHabilitacionInRO();
            filterHabilitacionInRO.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Authorization", "Bearer " + config.getToken());

            HttpEntity<FilterHabilitacionInRO> httpEntity = new HttpEntity<>(filterHabilitacionInRO, httpHeaders);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            return restTemplate.postForObject(url, httpEntity, ListHabilitacionOutRO.class);

        } catch (RestClientException e) {
            Log.e("LISTAHABILITACION", e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(ListHabilitacionOutRO listHabilitacionOutRO) {
        super.onPostExecute(listHabilitacionOutRO);
        listener.onListaHabilitacionCompled(listHabilitacionOutRO);
    }

    public interface OnListaHabilitacionCompleted {
        void onListaHabilitacionCompled(ListHabilitacionOutRO listHabilitacionOutRO);
    }
}
