package pe.gob.osinergmin.gnr.cgn.data.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class Rechazo {

    @Id
    private Long id;
    private Integer idHabilitacion;
    private Integer idMotivoRechazo;
    private Integer idMotivoSubRechazo;
    private String observacion;

    @Generated(hash = 701437498)
    public Rechazo() {
    }

    public Rechazo(Long id) {
        this.id = id;
    }

    @Generated(hash = 855306081)
    public Rechazo(Long id, Integer idHabilitacion, Integer idMotivoRechazo, Integer idMotivoSubRechazo, String observacion) {
        this.id = id;
        this.idHabilitacion = idHabilitacion;
        this.idMotivoRechazo = idMotivoRechazo;
        this.idMotivoSubRechazo = idMotivoSubRechazo;
        this.observacion = observacion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdHabilitacion() {
        return idHabilitacion;
    }

    public void setIdHabilitacion(Integer idHabilitacion) {
        this.idHabilitacion = idHabilitacion;
    }

    public Integer getIdMotivoRechazo() {
        return idMotivoRechazo;
    }

    public void setIdMotivoRechazo(Integer idMotivoRechazo) {
        this.idMotivoRechazo = idMotivoRechazo;
    }

    public Integer getIdMotivoSubRechazo() {
        return idMotivoSubRechazo;
    }

    public void setIdMotivoSubRechazo(Integer idMotivoSubRechazo) {
        this.idMotivoSubRechazo = idMotivoSubRechazo;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

}
