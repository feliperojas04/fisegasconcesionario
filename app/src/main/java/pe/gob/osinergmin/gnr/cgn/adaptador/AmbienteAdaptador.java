package pe.gob.osinergmin.gnr.cgn.adaptador;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;

import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.GenericBeanOutRO;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.data.models.Ambiente;
import pe.gob.osinergmin.gnr.cgn.data.models.AmbienteDao;
import pe.gob.osinergmin.gnr.cgn.util.MostrarFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.Util;

public class AmbienteAdaptador extends BaseAdapter {

    private Context context;
    private List<Ambiente> ambientes;
    private ListView listAmbientes;
    private SwitchCompat aprueba;
    private List<GenericBeanOutRO> listatipoObse;
    private AmbienteDao ambienteDao;
    private Callback callback;

    public AmbienteAdaptador(Context context, List<Ambiente> ambientes) {
        this.context = context;
        this.ambientes = ambientes;
    }

    public void setListatipoObse(List<GenericBeanOutRO> listatipoObse) {
        this.listatipoObse = listatipoObse;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void setAmbienteDao(AmbienteDao ambienteDao) {
        this.ambienteDao = ambienteDao;
    }

    public void setAmbientes(ListView listAmbientes) {
        this.listAmbientes = listAmbientes;
    }

    public void setAprueba(SwitchCompat aprueba) {
        this.aprueba = aprueba;
    }

    @Override
    public int getCount() {
        if (ambientes.size() == 0) {
            aprueba.setClickable(false);
        }
        return this.ambientes.size();
    }

    @Override
    public Object getItem(int position) {
        return this.ambientes.get(position);
    }

    public Ambiente getAmbiente(int position) {
        for (Ambiente ambiente : ambientes) {
            if (ambiente.getPosicion() == position) {
                return ambiente;
            }
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, final View convertView, final ViewGroup parent) {
        View rowView = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = layoutInflater.inflate(R.layout.item_ambiente, parent, false);

            holder.icoAmb_ambiente = rowView.findViewById(R.id.icoAmb_ambiente);
            holder.txtAmb_ambiente = rowView.findViewById(R.id.txtAmb_ambiente);
            holder.remAmb_ambiente = rowView.findViewById(R.id.remAmb_ambiente);
            holder.spiAmb_ambiente = rowView.findViewById(R.id.spiAmb_ambiente);
            holder.imgAmb_foto1Ambiente = rowView.findViewById(R.id.imgAmb_foto1Ambiente);
            holder.imgAmb_foto2Ambiente = rowView.findViewById(R.id.imgAmb_foto2Ambiente);
            holder.swiAmb_confinado = rowView.findViewById(R.id.swiAmb_confinado);

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        holder.ref = position;

        ambientes.get(position).setPosicion(position + 1);

        String recurso = rowView.getResources().getString(R.string.txtAmb_ambiente);
        String formateada = String.format(recurso, String.valueOf(ambientes.get(holder.ref).getPosicion()));
        holder.txtAmb_ambiente.setText(formateada);

        holder.swiAmb_confinado.setChecked("1".equalsIgnoreCase(ambientes.get(holder.ref).getAmbienteConfinado()));

        holder.remAmb_ambiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getCount() > 1) {
                    ambienteDao.delete(ambientes.get(holder.ref));
                    ambientes.remove(holder.ref);
                    notifyDataSetChanged();
                    Util.setListViewHeightBasedOnChildren(listAmbientes);
                    check();
                } else {
                    ((App) context.getApplicationContext()).showToast("Se debe registrar un ambiente como mínimo");
                }
            }
        });

        holder.spiAmb_ambiente.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                holder.check++;
                if (holder.check > 1) {
                    if (position != 0) {
                        ambientes.get(holder.ref).setAmbienteTipo(setTipoAmbiente(parent.getSelectedItem().toString()));
                    } else {
                        ambientes.get(holder.ref).setAmbienteTipo("");
                    }
                    ambienteDao.insertOrReplace(ambientes.get(holder.ref));
                    validar(holder);
                    check();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayList<String> list = new ArrayList<>();
        list.add("Seleccione tipo de ambiente");
        for (GenericBeanOutRO genericBeanOutRO : listatipoObse) {
            list.add(genericBeanOutRO.getLabel());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        holder.spiAmb_ambiente.setAdapter(adapter);

        holder.swiAmb_confinado.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ambientes.get(holder.ref).setAmbienteConfinado("1");
                } else {
                    ambientes.get(holder.ref).setAmbienteConfinado("0");
                }
                ambienteDao.insertOrReplace(ambientes.get(holder.ref));
                check();
            }
        });
        holder.imgAmb_foto1Ambiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.clicFoto(1, ambientes.get(holder.ref).getPosicion());
            }
        });

        holder.imgAmb_foto2Ambiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.clicFoto(2, ambientes.get(holder.ref).getPosicion());
            }
        });

        if (!"".equalsIgnoreCase(ambientes.get(holder.ref).getAmbienteUnoFoto())) {
            loadBitmap(ambientes.get(holder.ref).getAmbienteUnoFoto(), holder.imgAmb_foto1Ambiente);
        } else {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(65, 65);
            layoutParams.setMargins(10, 10, 30, 10);
            holder.imgAmb_foto1Ambiente.setLayoutParams(layoutParams);
            holder.imgAmb_foto1Ambiente.setScaleType(ImageView.ScaleType.FIT_CENTER);
            holder.imgAmb_foto1Ambiente.setImageResource(R.mipmap.ic_add_a_photo_black_36dp);
        }
        if (!"".equalsIgnoreCase(ambientes.get(holder.ref).getAmbienteDosFoto())) {
            loadBitmap(ambientes.get(holder.ref).getAmbienteDosFoto(), holder.imgAmb_foto2Ambiente);
        } else {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(65, 65);
            layoutParams.setMargins(10, 10, 30, 10);
            holder.imgAmb_foto2Ambiente.setLayoutParams(layoutParams);
            holder.imgAmb_foto2Ambiente.setScaleType(ImageView.ScaleType.FIT_CENTER);
            holder.imgAmb_foto2Ambiente.setImageResource(R.mipmap.ic_add_a_photo_black_36dp);
        }
        if (!"".equalsIgnoreCase(ambientes.get(holder.ref).getAmbienteTipo())) {
            holder.spiAmb_ambiente.setSelection(getTipoAmbiente(ambientes.get(holder.ref).getAmbienteTipo()));
        } else {
            holder.spiAmb_ambiente.setSelection(0);
        }
        validar(holder);

        return rowView;
    }

    private void validar(ViewHolder holder) {
        if ("".equalsIgnoreCase(ambientes.get(holder.ref).getAmbienteTipo()) ||
                "".equalsIgnoreCase(ambientes.get(holder.ref).getAmbienteUnoFoto()) ||
                "".equalsIgnoreCase(ambientes.get(holder.ref).getAmbienteDosFoto())) {
            holder.icoAmb_ambiente.setImageResource(R.mipmap.ic_warning_black_36dp);
            aprueba.setChecked(false);
        } else {
            holder.icoAmb_ambiente.setImageResource(R.mipmap.ic_done_black_36dp);
        }
    }

    private void check() {
        int i = 0;
        boolean boton = true;
        while (i < ambientes.size()) {
            Ambiente ambiente = ambientes.get(i);
            if ("".equalsIgnoreCase(ambiente.getAmbienteTipo()) ||
                    "".equalsIgnoreCase(ambiente.getAmbienteUnoFoto()) ||
                    "".equalsIgnoreCase(ambiente.getAmbienteDosFoto())) {
                boton = false;
                i = ambientes.size();
            }
            i++;
        }
        aprueba.setClickable(boton);
    }

    private String setTipoAmbiente(String tipoAmbiente) {
        for (GenericBeanOutRO genericBeanOutRO : listatipoObse) {
            if (tipoAmbiente.equalsIgnoreCase(genericBeanOutRO.getLabel())) {
                return genericBeanOutRO.getValue();
            }
        }
        return null;
    }

    private int getTipoAmbiente(String tipoAmbiente) {
        int i = 0;
        boolean entro = false;
        for (GenericBeanOutRO genericBeanOutRO : listatipoObse) {
            if (tipoAmbiente.equalsIgnoreCase(genericBeanOutRO.getValue())) {
                entro = true;
                break;
            }
            i++;
        }
        if (entro) {
            return i + 1;
        } else {
            return 0;
        }
    }

    public interface Callback {
        void clicFoto(int tipo, int position);
    }

    private class ViewHolder {
        int ref;
        ImageView icoAmb_ambiente;
        TextView txtAmb_ambiente;
        ImageView remAmb_ambiente;
        ImageView imgAmb_foto1Ambiente;
        ImageView imgAmb_foto2Ambiente;
        Spinner spiAmb_ambiente;
        SwitchCompat swiAmb_confinado;
        int check;
    }

    public void loadBitmap(String ruta, final ImageView imageView) {
        MostrarFotoTask mostrarFotoTask = new MostrarFotoTask(new MostrarFotoTask.OnMostrarFotoTaskCompleted() {
            @Override
            public void onMostrarFotoTaskCompleted(Bitmap bitmap) {
                if (bitmap != null) {
                    if (imageView != null) {
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(120, 120);
                        layoutParams.setMargins(10, 10, 10, 10);
                        imageView.setLayoutParams(layoutParams);
                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        imageView.setImageBitmap(bitmap);
                    }
                } else {
                    ((App) context.getApplicationContext()).showToast("Error al cargar la foto");
                }
            }
        });
        mostrarFotoTask.execute(ruta);
    }
}
