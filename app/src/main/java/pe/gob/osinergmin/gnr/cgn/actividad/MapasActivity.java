package pe.gob.osinergmin.gnr.cgn.actividad;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.HabilitacionOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.Suministro;
import pe.gob.osinergmin.gnr.cgn.data.models.SuministroDao;
import pe.gob.osinergmin.gnr.cgn.util.DownTimer;
import pe.gob.osinergmin.gnr.cgn.util.Util;

public class MapasActivity extends AppCompatActivity implements LocationListener, OnMapReadyCallback {

    Location mlocation;
    LocationManager locationManager;
    private HabilitacionOutRO habilitacionOutRO;
    private Config config;
    private MenuItem menuItem;
    private GoogleMap mMap;
    private Button btnMap_siguiente;
    private SuministroDao suministroDao;
    private MParametroOutRODao mParametroOutRODao;
    private DownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapas);

        countDownTimer = DownTimer.getInstance();

        suministroDao = ((App) getApplication()).getDaoSession().getSuministroDao();
        mParametroOutRODao = ((App) getApplication()).getDaoSession().getMParametroOutRODao();

        Toolbar toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_36dp);

        btnMap_siguiente = findViewById(R.id.btnMap_siguiente);
        btnMap_siguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                siguiente();
            }
        });
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        habilitacionOutRO = (HabilitacionOutRO) getIntent().getExtras().getSerializable("HABILITACION");
        config = getIntent().getExtras().getParcelable("CONFIG");

        String recurso = getResources().getString(R.string.txtMap_descripcion);
        String formateada = String.format(recurso, habilitacionOutRO.getDireccionUbigeoPredio(), habilitacionOutRO.getNumeroSuministroPredio());
        TextView texto = findViewById(R.id.txtMap_descripcionView);
        texto.setText(formateada);
        toolbar.setTitle(habilitacionOutRO.getNumeroSuministroPredio());
        GoogleApiClient client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        aceptarPermiso();
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.createAndStart();
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            activarGPS();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        } else if (mMap != null) {
            mMap.setMyLocationEnabled(true);
        }
        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    activarGPS();
                } else {
                    isLocal();
                }
                return false;
            }
        });
        String[] coordenadasPredio = habilitacionOutRO.getSolicitud().getCoordenadasPredio().split(",");
        LatLng latLngPredio = new LatLng(Double.valueOf(coordenadasPredio[0]), Double.valueOf(coordenadasPredio[1]));
        mMap.addMarker(new MarkerOptions()
                .title(habilitacionOutRO.getSolicitud().getNumeroSuministroPredio())
                .snippet(habilitacionOutRO.getSolicitud().getDireccionUbigeoPredio())
                .position(latLngPredio));
        drawMarkerWithCircle(latLngPredio);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngPredio, 17));
    }

    private void drawMarkerWithCircle(LatLng position) {
        double radiusInMeters = Double.parseDouble(config.getParametro2());
        int strokeColor = 0xffff0000; //red outline
        int shadeColor = 0x44ff0000; //opaque red fill

        CircleOptions circleOptions = new CircleOptions().center(position).radius(radiusInMeters).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(8);
        mMap.addCircle(circleOptions);
    }

    private void isLocal() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }
        mlocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (mlocation != null) {
            String[] coordenadasTaller = habilitacionOutRO.getSolicitud().getCoordenadasPredio().split(",");
            Double latTaller = Math.abs(Double.valueOf(coordenadasTaller[0]));
            Double latActual = Math.abs(mlocation.getLatitude());
            Double lngTaller = Math.abs(Double.valueOf(coordenadasTaller[1]));
            Double lngActual = Math.abs(mlocation.getLongitude());
            if (Math.abs(latTaller - latActual) < Double.parseDouble(config.getParametro2()) / 125000 && Math.abs(lngTaller - lngActual) < Double.parseDouble(config.getParametro2()) / 125000) {
                btnMap_siguiente.setVisibility(View.VISIBLE);
            } else {
                btnMap_siguiente.setVisibility(View.GONE);
            }
        } else {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                btnMap_siguiente.setVisibility(View.GONE);
                activarEspecial();
            }
        }
    }

    private void siguiente() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.removeUpdates(MapasActivity.this);
        List suministroList = suministroDao.queryBuilder()
                .where(SuministroDao.Properties.IdHabilitacion.eq(habilitacionOutRO.getIdHabilitacion())).list();
        if (suministroList.size() > 0) {
            Suministro suministro = (Suministro) suministroList.get(0);
            Intent intent = new Intent(MapasActivity.this, HabilitacionActivity.class);
            intent.putExtra("SUMINISTRO", suministro);
            intent.putExtra("CONFIG", config);
            startActivity(intent);
            finish();
        } else {
            Suministro suministro = new Suministro(null,
                    habilitacionOutRO.getIdHabilitacion() != null ? habilitacionOutRO.getIdHabilitacion() : 0,
                    habilitacionOutRO.getDocumentoIdentificacionSolicitante() != null ? habilitacionOutRO.getDocumentoIdentificacionSolicitante() : "",
                    habilitacionOutRO.getNombreSolicitante() != null ? habilitacionOutRO.getNombreSolicitante() : "",
                    habilitacionOutRO.getDireccionUbigeoPredio() != null ? habilitacionOutRO.getDireccionUbigeoPredio() : "",
                    habilitacionOutRO.getNumeroIntentoHabilitacion() != null ? habilitacionOutRO.getNumeroIntentoHabilitacion().toString() : "",
                    habilitacionOutRO.getCodigoSolicitud() != null ? habilitacionOutRO.getCodigoSolicitud().toString() : "",
                    habilitacionOutRO.getNumeroContratoConcesionariaPredio() != null ? habilitacionOutRO.getNumeroContratoConcesionariaPredio() : "",
                    habilitacionOutRO.getSolicitud().getFechaSolicitud() != null ? habilitacionOutRO.getSolicitud().getFechaSolicitud() : "",
                    habilitacionOutRO.getSolicitud().getFechaAprobacionSolicitud() != null ? habilitacionOutRO.getSolicitud().getFechaAprobacionSolicitud() : "",
                    "",
                    habilitacionOutRO.getSolicitud().getNombreTipoProyectoInstalacion() != null ? habilitacionOutRO.getSolicitud().getNombreTipoProyectoInstalacion() : "",
                    habilitacionOutRO.getSolicitud().getNombreTipoInstalacion() != null ? habilitacionOutRO.getSolicitud().getNombreTipoInstalacion() : "",
                    habilitacionOutRO.getSolicitud().getNumeroPuntosInstalacion() != null ? habilitacionOutRO.getSolicitud().getNumeroPuntosInstalacion().toString() : "",
                    "",
                    habilitacionOutRO.getNumeroSuministroPredio() != null ? habilitacionOutRO.getNumeroSuministroPredio() : "",
                    "",
                    "",
                    "0",
                    "",
                    "",
                    "0",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "0",
                    "0",
                    "0",
                    "0",
                    "",
                    "",
                    "0",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "0",
                    "",
                    "",
                    "0",
                    "",
                    false,
                    "",
                    "0",
                    "",
                    "",
                    "",
                    "");
            suministroDao.insertOrReplace(suministro);

            Intent intent = new Intent(MapasActivity.this, HabilitacionActivity.class);
            intent.putExtra("SUMINISTRO", suministro);
            intent.putExtra("CONFIG", config);
            startActivity(intent);
            finish();
        }
    }

    private void activarGPS() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.txtMap_aviso);
        builder.setMessage(R.string.txtMap_desactivada);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.btnMap_si, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        });
        builder.setNegativeButton(R.string.btnMap_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    private void activarEspecial() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.txtMap_aviso);
        builder.setMessage(R.string.txtMap_especial);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.btnMap_si, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                menuItem.setVisible(false);
            }
        });
        builder.setNegativeButton(R.string.btnMap_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (Constantes.VALOR_SI.equalsIgnoreCase(config.getParametro3())) {
                    menuItem.setVisible(true);
                }
            }
        });
        builder.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        isLocal();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        btnMap_siguiente.setVisibility(View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_mapas, menu);
        menuItem = menu.findItem(R.id.menuMap_especial);
        menuItem.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                locationManager.removeUpdates(MapasActivity.this);
                Intent intent = new Intent(MapasActivity.this, BuscarSuministroActivity.class);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
                return true;
            case R.id.menuMap_especial:
                if (config.getOffline()) {
                    MParametroOutRO mParametroOutRO4 = mParametroOutRODao.
                            queryBuilder().
                            where(MParametroOutRODao.
                                    Properties.NombreParametro.
                                    eq(Constantes.NOMBRE_PARAMETRO_TELEFONO_SOLICITUD_CODIGO_ESPECIAL_ACCESO))
                            .limit(1).unique();
                    config.setParametro4(mParametroOutRO4 == null ? "012193400" : mParametroOutRO4.getValorParametro());
                    locationManager.removeUpdates(MapasActivity.this);
                    List suministroList = suministroDao.queryBuilder()
                            .where(SuministroDao.Properties.IdHabilitacion.eq(habilitacionOutRO.getIdHabilitacion())).list();
                    if (suministroList.size() > 0) {
                        Suministro suministro = (Suministro) suministroList.get(0);
                        Intent intent2 = new Intent(MapasActivity.this, IniciarEspecialActivity.class);
                        intent2.putExtra("SUMINISTRO", suministro);
                        intent2.putExtra("CONFIG", config);
                        startActivity(intent2);
                        finish();
                    } else {
                        Suministro suministro = new Suministro(null,
                                habilitacionOutRO.getIdHabilitacion() != null ? habilitacionOutRO.getIdHabilitacion() : 0,
                                habilitacionOutRO.getDocumentoIdentificacionSolicitante() != null ? habilitacionOutRO.getDocumentoIdentificacionSolicitante() : "",
                                habilitacionOutRO.getNombreSolicitante() != null ? habilitacionOutRO.getNombreSolicitante() : "",
                                habilitacionOutRO.getDireccionUbigeoPredio() != null ? habilitacionOutRO.getDireccionUbigeoPredio() : "",
                                habilitacionOutRO.getNumeroIntentoHabilitacion() != null ? habilitacionOutRO.getNumeroIntentoHabilitacion().toString() : "",
                                habilitacionOutRO.getCodigoSolicitud() != null ? habilitacionOutRO.getCodigoSolicitud().toString() : "",
                                habilitacionOutRO.getNumeroContratoConcesionariaPredio() != null ? habilitacionOutRO.getNumeroContratoConcesionariaPredio() : "",
                                habilitacionOutRO.getSolicitud().getFechaSolicitud() != null ? habilitacionOutRO.getSolicitud().getFechaSolicitud() : "",
                                habilitacionOutRO.getSolicitud().getFechaAprobacionSolicitud() != null ? habilitacionOutRO.getSolicitud().getFechaAprobacionSolicitud() : "",
                                "",
                                habilitacionOutRO.getSolicitud().getNombreTipoProyectoInstalacion() != null ? habilitacionOutRO.getSolicitud().getNombreTipoProyectoInstalacion() : "",
                                habilitacionOutRO.getSolicitud().getNombreTipoInstalacion() != null ? habilitacionOutRO.getSolicitud().getNombreTipoInstalacion() : "",
                                habilitacionOutRO.getSolicitud().getNumeroPuntosInstalacion() != null ? habilitacionOutRO.getSolicitud().getNumeroPuntosInstalacion().toString() : "",
                                "",
                                habilitacionOutRO.getNumeroSuministroPredio() != null ? habilitacionOutRO.getNumeroSuministroPredio() : "",
                                "",
                                "",
                                "0",
                                "",
                                "",
                                "0",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "0",
                                "0",
                                "0",
                                "0",
                                "",
                                "",
                                "0",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "0",
                                "",
                                "",
                                "0",
                                "",
                                false,
                                "",
                                "0",
                                "",
                                "",
                                "",
                                "");
                        suministroDao.insertOrReplace(suministro);

                        Intent intent3 = new Intent(MapasActivity.this, IniciarEspecialActivity.class);
                        intent3.putExtra("SUMINISTRO", suministro);
                        intent3.putExtra("CONFIG", config);
                        startActivity(intent3);
                        finish();
                    }
                } else {
                    locationManager.removeUpdates(MapasActivity.this);
                    List suministroList = suministroDao.queryBuilder()
                            .where(SuministroDao.Properties.IdHabilitacion.eq(habilitacionOutRO.getIdHabilitacion())).list();
                    if (suministroList.size() > 0) {
                        Suministro suministro = (Suministro) suministroList.get(0);
                        Intent intent4 = new Intent(MapasActivity.this, IniciarEspecialActivity.class);
                        intent4.putExtra("SUMINISTRO", suministro);
                        intent4.putExtra("CONFIG", config);
                        startActivity(intent4);
                        finish();
                    } else {
                        Suministro suministro = new Suministro(null,
                                habilitacionOutRO.getIdHabilitacion() != null ? habilitacionOutRO.getIdHabilitacion() : 0,
                                habilitacionOutRO.getDocumentoIdentificacionSolicitante() != null ? habilitacionOutRO.getDocumentoIdentificacionSolicitante() : "",
                                habilitacionOutRO.getNombreSolicitante() != null ? habilitacionOutRO.getNombreSolicitante() : "",
                                habilitacionOutRO.getDireccionUbigeoPredio() != null ? habilitacionOutRO.getDireccionUbigeoPredio() : "",
                                habilitacionOutRO.getNumeroIntentoHabilitacion() != null ? habilitacionOutRO.getNumeroIntentoHabilitacion().toString() : "",
                                habilitacionOutRO.getCodigoSolicitud() != null ? habilitacionOutRO.getCodigoSolicitud().toString() : "",
                                habilitacionOutRO.getNumeroContratoConcesionariaPredio() != null ? habilitacionOutRO.getNumeroContratoConcesionariaPredio() : "",
                                habilitacionOutRO.getSolicitud().getFechaSolicitud() != null ? habilitacionOutRO.getSolicitud().getFechaSolicitud() : "",
                                habilitacionOutRO.getSolicitud().getFechaAprobacionSolicitud() != null ? habilitacionOutRO.getSolicitud().getFechaAprobacionSolicitud() : "",
                                "",
                                habilitacionOutRO.getSolicitud().getNombreTipoProyectoInstalacion() != null ? habilitacionOutRO.getSolicitud().getNombreTipoProyectoInstalacion() : "",
                                habilitacionOutRO.getSolicitud().getNombreTipoInstalacion() != null ? habilitacionOutRO.getSolicitud().getNombreTipoInstalacion() : "",
                                habilitacionOutRO.getSolicitud().getNumeroPuntosInstalacion() != null ? habilitacionOutRO.getSolicitud().getNumeroPuntosInstalacion().toString() : "",
                                "",
                                habilitacionOutRO.getNumeroSuministroPredio() != null ? habilitacionOutRO.getNumeroSuministroPredio() : "",
                                "",
                                "",
                                "0",
                                "",
                                "",
                                "0",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "0",
                                "0",
                                "0",
                                "0",
                                "",
                                "",
                                "0",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "0",
                                "",
                                "",
                                "0",
                                "",
                                false,
                                "",
                                "0",
                                "",
                                "",
                                "",
                                "");
                        suministroDao.insertOrReplace(suministro);

                        Intent intent5 = new Intent(MapasActivity.this, IniciarEspecialActivity.class);
                        intent5.putExtra("SUMINISTRO", suministro);
                        intent5.putExtra("CONFIG", config);
                        startActivity(intent5);
                        finish();
                    }
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        countDownTimer.createAndStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(MapasActivity.this);
    }

    @Override
    public void onBackPressed() {
        locationManager.removeUpdates(MapasActivity.this);
        Intent intent = new Intent(MapasActivity.this, BuscarSuministroActivity.class);
        intent.putExtra("CONFIG", config);
        startActivity(intent);
        finish();
    }

    private void aceptarPermiso() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permiso = Util.getPermisos(MapasActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION);
            if (permiso != null) {
                requestPermissions(permiso, pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION:
                String message = Util.onRequestPermissionsResult(permissions, grantResults);
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
