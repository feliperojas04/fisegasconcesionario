package pe.gob.osinergmin.gnr.cgn.adaptador;

import android.content.Context;

import java.util.List;

public class SimpleFilterableAdapter<ObjectType> extends FilterableAdapter<ObjectType, String> {

    public SimpleFilterableAdapter(Context context, int resourceId, List<ObjectType> objects) {
        super(context, resourceId, objects);
    }

    @Override
    protected String prepareFilter(CharSequence seq) {

        /* The object we return here will be passed to passesFilter() as constraint.
         ** This method is called only once per filter run. The same constraint is
         ** then used to decide upon all objects in the data set.
         */

        return seq.toString().toUpperCase();
    }

    @Override
    protected boolean passesFilter(ObjectType object, String constraint) {
        String repr = object.toString().toUpperCase();
        String carac = "Á,A;É,E;Í,I;Ó,O;Ú,U;Ä,A;Ë,E;Ï,I;Ö,O;Ü,U";
        String ca[] = carac.split(";");
        for (String b : ca) {
            String y[] = b.split(",");
            repr = repr.replace(y[0], y[1]);
            constraint = constraint.replace(y[0], y[1]);
        }
        if (repr.contains(constraint)) {
            return true;
        }

//        if (repr.startsWith(constraint))
//            return true;
//
//        else {
//            final String[] words = repr.split("");
//            final int wordCount = words.length;
//
//            for (int i = 0; i < wordCount; i++) {
//                if (words[i].startsWith(constraint))
//                    return true;
//            }
//        }

        return false;
    }
}
