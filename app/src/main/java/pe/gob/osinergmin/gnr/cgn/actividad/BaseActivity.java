package pe.gob.osinergmin.gnr.cgn.actividad;

import android.app.ProgressDialog;
import android.os.Build;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by cwramirezg on 26/12/2017.
 */

public class BaseActivity extends AppCompatActivity {

    protected ProgressDialog progressDialog;

    protected void showProgressDialog(int messageResourceId) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        int style;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            style = android.R.style.Theme_Material_Light_Dialog;
        } else {
            style = ProgressDialog.THEME_HOLO_LIGHT;
        }
        progressDialog = new ProgressDialog(this, style);
        progressDialog.setMessage(getResources().getString(messageResourceId));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    protected void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        // hide progress dialog to prevent leaks
        hideProgressDialog();
    }
}
