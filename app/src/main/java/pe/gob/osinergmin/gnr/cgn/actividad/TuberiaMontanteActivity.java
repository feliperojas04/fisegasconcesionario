package pe.gob.osinergmin.gnr.cgn.actividad;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.util.Constantes;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.adaptador.FotoMontanteAdaptador;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionMontanteOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MontanteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.PrecisionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionMontanteOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.Montante;
import pe.gob.osinergmin.gnr.cgn.data.models.Precision;
import pe.gob.osinergmin.gnr.cgn.task.MontanteRx;
import pe.gob.osinergmin.gnr.cgn.util.DownTimer;
import pe.gob.osinergmin.gnr.cgn.util.FormatoFecha;
import pe.gob.osinergmin.gnr.cgn.util.NetworkUtil;
import pe.gob.osinergmin.gnr.cgn.util.ReducirFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.Util;

public class TuberiaMontanteActivity extends BaseActivity {

    private Montante montante;
    private Config config;
    private TableLayout tablaTuberia;
    private TextView txtDet_DocumentoPropietarioView;
    private TextView txtDet_nombrePropietarioView;
    private TextView txtDetProyectoView;
    private TextView txtDetMontanteView;
    private TextView txtDet_numeroIntHabilitacionView;
    private TextView txtDet_numeroSolicitudView;
    private TextView txtDet_numeroContratoView;
    private TextView txtDet_fechaSolicitudView;
    private TextView txtDet_fechaAprobacionView;
    private TextView txtDet_tipoProyectoView;
    private TextView txtDet_tipoInstalacionView;
    private TextView txtDet_numeroPuntosInstaacionView;
    private TextView txtDet_memoriaView;
    private ImageView icoTub_recorrido;
    private ListView listaTub_recorrido;
    private List<String> listaTub_fotosRecorrido;
    private SwitchCompat swiTub_aprueba;
    private Button btnTub_rechazar;
    private Toolbar toolbar;
    private MenuItem menuMas;
    private MenuItem menuMenos;
    private FotoMontanteAdaptador fotoAdaptador;
    private MontanteDao montanteDao;
    private DownTimer countDownTimer;
    private MParametroOutRODao mParametroOutRODao;
    private MHabilitacionMontanteOutRODao mHabilitacionMontanteOutRODao;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private PrecisionDao precisionDao;
    private final CompositeDisposable disposables = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tuberia_montante);

        countDownTimer = DownTimer.getInstance();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        montanteDao = ((App) getApplication()).getDaoSession().getMontanteDao();
        mParametroOutRODao = ((App) getApplication()).getDaoSession().getMParametroOutRODao();
        mHabilitacionMontanteOutRODao = ((App) getApplication()).getDaoSession().getMHabilitacionMontanteOutRODao();
        precisionDao = ((App) getApplication()).getDaoSession().getPrecisionDao();

        toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_36dp);

        montante = getIntent().getExtras().getParcelable("MONTANTE");
        config = getIntent().getExtras().getParcelable("CONFIG");

        tablaTuberia = findViewById(R.id.tablaTuberia);
        txtDet_DocumentoPropietarioView = findViewById(R.id.txtDet_DocumentoPropietarioView);
        txtDet_nombrePropietarioView = findViewById(R.id.txtDet_nombrePropietarioView);
        txtDetProyectoView = findViewById(R.id.txtDetProyectoView);
        txtDetMontanteView = findViewById(R.id.txtDetMontanteView);
        txtDet_numeroIntHabilitacionView = findViewById(R.id.txtDet_numeroIntHabilitacionView);
        txtDet_numeroSolicitudView = findViewById(R.id.txtDet_numeroSolicitudView);
        txtDet_numeroContratoView = findViewById(R.id.txtDet_numeroContratoView);
        txtDet_fechaSolicitudView = findViewById(R.id.txtDet_fechaSolicitudView);
        txtDet_fechaAprobacionView = findViewById(R.id.txtDet_fechaAprobacionView);
        txtDet_tipoProyectoView = findViewById(R.id.txtDet_tipoProyectoView);
        txtDet_tipoInstalacionView = findViewById(R.id.txtDet_tipoInstalacionView);
        txtDet_numeroPuntosInstaacionView = findViewById(R.id.txtDet_numeroPuntosInstaacionView);
        txtDet_memoriaView = findViewById(R.id.txtDet_memoriaView);
        icoTub_recorrido = findViewById(R.id.icoTub_recorrido);
        ImageView imgTub_recorrido = findViewById(R.id.imgTub_recorrido);
        listaTub_recorrido = findViewById(R.id.listaTub_recorrido);
        swiTub_aprueba = findViewById(R.id.swiTub_aprueba);
        Button btnTub_evaluando = findViewById(R.id.btnTub_evaluando);
        btnTub_rechazar = findViewById(R.id.btnTub_rechazar);

        imgTub_recorrido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listaTub_fotosRecorrido.size() < 3) {
                    if (ActivityCompat.checkSelfPermission(
                            TuberiaMontanteActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ((App) getApplication()).showToast("No puede realizar la acción porque no se ha dado el permiso de Almacenamiento");
                        return;
                    }
                    if (Util.isExternalStorageWritable()) {
                        if (Util.getAlbumStorageDir(TuberiaMontanteActivity.this)) {
                            File file = Util.getNuevaRutaFoto(montante.getIdHabilitacionMontante().toString(), TuberiaMontanteActivity.this);
                            listaTub_fotosRecorrido.add(file.getPath());
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            Uri outputUri = FileProvider.getUriForFile(TuberiaMontanteActivity.this, getApplicationContext().getPackageName() + ".provider", file);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                ClipData clip = ClipData.newUri(getContentResolver(), "Concesionario", outputUri);
                                intent.setClipData(clip);
                                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            }
                            startActivityForResult(intent, 2);
                        } else {
                            ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                        }
                    } else {
                        ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                    }
                } else {
                    ((App) getApplication()).showToast("Solo se admite 3 fotos");
                }
            }
        });

        swiTub_aprueba.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    montante.setApruebaTuberia("1");
                    btnTub_rechazar.setVisibility(View.GONE);
                } else {
                    montante.setApruebaTuberia("0");
                    btnTub_rechazar.setVisibility(View.VISIBLE);
                }
            }
        });

        assert btnTub_evaluando != null;
        btnTub_evaluando.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                montanteDao.insertOrReplace(montante);
                Intent intent = new Intent(TuberiaMontanteActivity.this, HermeticidadMontanteActivity.class);
                intent.putExtra("MONTANTE", montante);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
            }
        });

        btnTub_rechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TuberiaMontanteActivity.this, RechazoMontanteActivity.class);
                intent.putExtra("MONTANTE", montante);
                intent.putExtra("CONFIG", config);
                intent.putExtra("TIPO", "");
                startActivity(intent);
            }
        });
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    Precision precision = new Precision();
                    precision.setIdSolicitud(montante.getIdHabilitacionMontante());
                    precision.setLatitud(String.valueOf(location.getLatitude()));
                    precision.setLongitud(String.valueOf(location.getLongitude()));
                    precision.setPrecision(String.valueOf(location.getAccuracy()));
                    precision.setEstado("2");
                    precisionDao.insertOrReplace(precision);
                }
            }
        };
        aceptarPermiso();
    }

    private void validarCheck() {
        if (listaTub_fotosRecorrido != null && listaTub_fotosRecorrido.size() < 3) {
            icoTub_recorrido.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            icoTub_recorrido.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        validarSelector();
    }

    private void validarSelector() {
        if (listaTub_fotosRecorrido != null) {
            if (listaTub_fotosRecorrido.size() < 3) {
                swiTub_aprueba.setClickable(false);
                swiTub_aprueba.setChecked(false);
            } else {
                swiTub_aprueba.setClickable(true);
            }
        } else {
            swiTub_aprueba.setClickable(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1458) {
            if (resultCode == RESULT_CANCELED) {
                Intent intent = new Intent(TuberiaMontanteActivity.this, BuscarMontanteActivity.class);
                startActivity(intent);
                finish();
            }
        }
        if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                if (listaTub_fotosRecorrido.size() > 0) {
                    ReducirFotoTask reducirFotoTask = new ReducirFotoTask(new ReducirFotoTask.OnReducirFotoTaskCompleted() {
                        @Override
                        public void onReducirFotoTaskCompleted(String result) {
                            if (!("1".equalsIgnoreCase(result))) {
                                ((App) getApplication()).showToast(result);
                            }
                        }
                    });
                    reducirFotoTask.execute(listaTub_fotosRecorrido.get(listaTub_fotosRecorrido.size() - 1), config.getParametro5());
                    switch (listaTub_fotosRecorrido.size()) {
                        case 1:
                            montante.setFoto1Tuberia(listaTub_fotosRecorrido.get(0));
                            montante.setFoto2Tuberia("");
                            montante.setFoto3Tuberia("");
                            break;
                        case 2:
                            montante.setFoto1Tuberia(listaTub_fotosRecorrido.get(0));
                            montante.setFoto2Tuberia(listaTub_fotosRecorrido.get(1));
                            montante.setFoto3Tuberia("");
                            break;
                        case 3:
                            montante.setFoto1Tuberia(listaTub_fotosRecorrido.get(0));
                            montante.setFoto2Tuberia(listaTub_fotosRecorrido.get(1));
                            montante.setFoto3Tuberia(listaTub_fotosRecorrido.get(2));
                            break;
                    }
                    fotoAdaptador.notifyDataSetChanged();
                    validarCheck();
                }
            } else {
                if (listaTub_fotosRecorrido.size() > 0) {
                    listaTub_fotosRecorrido.remove(listaTub_fotosRecorrido.size() - 1);
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.createAndStart();
        createLocationRequest();
        toolbar.setTitle(montante.getObjetoConexion() + " - Recorrido de la tubería");
        cargarSuministro();
        cargaDatos();
        validarCheck();
        if (config.getOffline()) {
            MParametroOutRO mParametroOutRO = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_CONFIGURACION_COMPRESION_FOTOS_APLICATIVO_MOVIL))
                    .limit(1)
                    .unique();
            config.setParametro5(mParametroOutRO == null ? "800x600" : mParametroOutRO.getValorParametro());
            MParametroOutRO mParametroOutPrecision = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_FRECUENCIA_TOMA_COORDENADAS_AUDITORIA))
                    .limit(1).unique();
            config.setPrecision(mParametroOutPrecision == null ? "10000" : mParametroOutPrecision.getValorParametro());
        } else {
            if ("".equalsIgnoreCase(config.getParametro5())) {
                config.setParametro5("800x600");
            }
            if ("".equalsIgnoreCase(config.getPrecision())) {
                config.setPrecision("10000");
            }
        }
    }

    private void cargaDatos() {
        listaTub_fotosRecorrido = new ArrayList<>();
        if (!montante.getFoto1Tuberia().equalsIgnoreCase("")) {
            listaTub_fotosRecorrido.add(montante.getFoto1Tuberia());
        }
        if (!montante.getFoto2Tuberia().equalsIgnoreCase("")) {
            listaTub_fotosRecorrido.add(montante.getFoto2Tuberia());
        }
        if (!montante.getFoto3Tuberia().equalsIgnoreCase("")) {
            listaTub_fotosRecorrido.add(montante.getFoto3Tuberia());
        }
        fotoAdaptador = new FotoMontanteAdaptador(this, listaTub_fotosRecorrido);
        fotoAdaptador.setFotos(listaTub_recorrido);
        fotoAdaptador.setTipo(100);
        fotoAdaptador.setIco(icoTub_recorrido);
        fotoAdaptador.setAprueba(swiTub_aprueba);
        listaTub_recorrido.setAdapter(fotoAdaptador);
        Util.setListViewHeightBasedOnChildren(listaTub_recorrido);
        if (!(montante.getApruebaTuberia().equalsIgnoreCase("0"))) {
            swiTub_aprueba.setChecked(true);
        }
    }

    private void cargarSuministro() {
        txtDet_DocumentoPropietarioView.setText(montante.getObjetoConexion());
        txtDet_nombrePropietarioView.setText(montante.getCup());
        txtDetProyectoView.setText(montante.getNombre());
        txtDetMontanteView.setText(montante.getNombreMontanteProyectoInstalacion());
        txtDet_numeroIntHabilitacionView.setText(montante.getDireccion());
        txtDet_numeroSolicitudView.setText(montante.getTipoProyecto());
        txtDet_numeroContratoView.setText(montante.getFechaRegistro());
        txtDet_fechaSolicitudView.setText(montante.getEsProyectoFise());
        txtDet_fechaAprobacionView.setText(montante.getTienePromocion());
        txtDet_tipoProyectoView.setText(montante.getEsZonaGasificada());
        txtDet_tipoInstalacionView.setText(montante.getTienePlanoInstalacion());
        txtDet_numeroPuntosInstaacionView.setText(montante.getTieneEspecificacion());
        txtDet_memoriaView.setText(montante.getTieneMemoria());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_general, menu);
        menuMas = menu.findItem(R.id.menuMas);
        menuMas.setVisible(true);
        menuMenos = menu.findItem(R.id.menuMenos);
        menuMenos.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                return true;
            case R.id.menuMas:
                tablaTuberia.setVisibility(View.VISIBLE);
                menuMas.setVisible(false);
                menuMenos.setVisible(true);
                return true;

            case R.id.menuMenos:
                tablaTuberia.setVisibility(View.GONE);
                menuMas.setVisible(true);
                menuMenos.setVisible(false);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        montanteDao.insertOrReplace(montante);
        Intent intent = new Intent(TuberiaMontanteActivity.this, HabilitacionMontanteActivity.class);
        intent.putExtra("MONTANTE", montante);
        intent.putExtra("CONFIG", config);
        startActivity(intent);
        finish();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        countDownTimer.createAndStart();
    }

    private void aceptarPermiso() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permiso = Util.getPermisos(TuberiaMontanteActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION);
            if (permiso != null) {
                requestPermissions(permiso, pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION);
            }
        }
    }

    private void createLocationRequest() {
        int value = 10000;
        try {
            value = Integer.valueOf(config.getPrecision());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(value);
        mLocationRequest.setFastestInterval(value);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(TuberiaMontanteActivity.this, 1458);
                    } catch (IntentSender.SendIntentException sendEx) {
                    }
                }
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            aceptarPermiso();
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION:
                String message = Util.onRequestPermissionsResult(permissions, grantResults);
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    @Override
    protected void onDestroy() {
        disposables.clear();
        super.onDestroy();
    }
}
