package pe.gob.osinergmin.gnr.cgn.task;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import gob.osinergmin.gnr.domain.dto.rest.in.filter.FilterHabilitacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.out.HabilitacionOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.util.Urls;

public class Habilitacion extends AsyncTask<Config, Void, HabilitacionOutRO> {

    private OnHabilitacionCompleted listener = null;

    public Habilitacion(OnHabilitacionCompleted listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected HabilitacionOutRO doInBackground(Config... configs) {
        try {

            Config config = configs[0];

            String url = Urls.getBase() + Urls.getHabilitacion();

            FilterHabilitacionInRO filterHabilitacionInRO = new FilterHabilitacionInRO();
            filterHabilitacionInRO.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);
            filterHabilitacionInRO.setIdHabilitacion(Integer.parseInt(config.getIdHabilitacion()));

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Authorization", "Bearer " + config.getToken());

            HttpEntity<FilterHabilitacionInRO> httpEntity = new HttpEntity<>(filterHabilitacionInRO, httpHeaders);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            return restTemplate.postForObject(url, httpEntity, HabilitacionOutRO.class);

        } catch (RestClientException e) {
            Log.e("HABILITACION", e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(HabilitacionOutRO habilitacionOutRO) {
        super.onPostExecute(habilitacionOutRO);
        listener.onHabilitacionCompled(habilitacionOutRO);
    }

    public interface OnHabilitacionCompleted {
        void onHabilitacionCompled(HabilitacionOutRO habilitacionOutRO);
    }
}
