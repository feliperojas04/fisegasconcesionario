package pe.gob.osinergmin.gnr.cgn.actividad;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputLayout;

import gob.osinergmin.gnr.domain.dto.rest.out.AccessResponseOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.BaseOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.data.models.AcometidaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.ConfigDao;
import pe.gob.osinergmin.gnr.cgn.data.models.MontanteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.SuministroDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Usuario;
import pe.gob.osinergmin.gnr.cgn.service.AlarmService;
import pe.gob.osinergmin.gnr.cgn.service.DescaService;
import pe.gob.osinergmin.gnr.cgn.task.Login;
import pe.gob.osinergmin.gnr.cgn.task.Logout;
import pe.gob.osinergmin.gnr.cgn.util.DownTimer;
import pe.gob.osinergmin.gnr.cgn.util.NetworkUtil;

public class ConfiguracionActivity extends BaseActivity {

    private Config config;
    private Toolbar toolbar;
    private DownTimer countDownTimer;
    private boolean abierto = true;

    private ConstraintLayout error, consOffline, consOnline;
    private TextInputLayout usuario;
    private TextInputLayout clave;
    private CheckBox checkMaestros, checkPre, checkAco, checkMon;

    private ConfigDao configDao;
    private SuministroDao suministroDao;
    private AcometidaDao acometidaDao;
    private MontanteDao montanteDao;

    private TextView user, preVisitas, numPre, tv_acometida, numAco, tv_montante, numMon, sincronizacion, cerrar, configModo;
    private ImageView desConfig, configVacio, imageSync;
    private SwitchCompat offline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion_nav);

        countDownTimer = DownTimer.getInstance();

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        configDao = ((App) getApplication()).getDaoSession().getConfigDao();
        suministroDao = ((App) getApplication()).getDaoSession().getSuministroDao();
        montanteDao = ((App) getApplication()).getDaoSession().getMontanteDao();
        acometidaDao = ((App) getApplication()).getDaoSession().getAcometidaDao();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.nav_open, R.string.nav_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        user = header.findViewById(R.id.nav_usuario);
        preVisitas = header.findViewById(R.id.preVisitas);
        numPre = header.findViewById(R.id.numPre);
        tv_acometida = header.findViewById(R.id.tv_acometida);
        numAco = header.findViewById(R.id.numAco);
        tv_montante = header.findViewById(R.id.tv_montante);
        numMon = header.findViewById(R.id.numMon);
        sincronizacion = header.findViewById(R.id.sincronizacion);
        imageSync = header.findViewById(R.id.imageSync);
        desConfig = header.findViewById(R.id.desConfig);
        configVacio = header.findViewById(R.id.configVacio);
        configModo = header.findViewById(R.id.configModo);
        offline = header.findViewById(R.id.offline);
        cerrar = header.findViewById(R.id.cerrar);

        error = findViewById(R.id.error);
        Button entendidoError = findViewById(R.id.entendidoError);
        consOnline = findViewById(R.id.ConsOnline);
        usuario = findViewById(R.id.usuario);
        clave = findViewById(R.id.clave);
        Button activarOnline = findViewById(R.id.activarOnline);
        consOffline = findViewById(R.id.ConsOffline);
        checkMaestros = findViewById(R.id.checkMaestros);
        checkPre = findViewById(R.id.checkPre);
        checkAco = findViewById(R.id.checkAco);
        checkMon = findViewById(R.id.checkMon);
        Button activarOffline = findViewById(R.id.activarOffline);

        activarOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clave.setError(null);
                boolean cancel = false;
                View focusView = null;

                if (TextUtils.isEmpty(clave.getEditText().getText())) {
                    clave.setError(getString(R.string.sesion_requerido));
                    focusView = clave;
                    cancel = true;
                }

                if (cancel) {
                    focusView.requestFocus();
                } else {
                    if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        showProgressDialog(R.string.app_cargando);
                        Login login = new Login(new Login.OnLoginCompleted() {
                            @Override
                            public void onLoginCompled(final AccessResponseOutRO accessResponseOutRO) {
                                hideProgressDialog();
                                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                                } else if (accessResponseOutRO == null) {
                                    ((App) getApplication()).showToast("Contraseña incorrecto");
                                } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(accessResponseOutRO.getResultCode())) {
                                    ((App) getApplication()).showToast(accessResponseOutRO.getMessage() == null ? "Contraseña incorrecto" : accessResponseOutRO.getMessage());
                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(ConfiguracionActivity.this);
                                    builder.setTitle("Modo ONLINE Activado");
                                    builder.setMessage("El modo ONLINE ha sido activado, asegúrese de contar con una conexión a internet estable para poder trabajar adecuadamente.");
                                    builder.setCancelable(false);
                                    builder.setPositiveButton("Entendido", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            config.setToken(accessResponseOutRO.getToken());
                                            config.setOffline(false);
                                            configDao.insertOrReplace(config);
                                            AlarmService.cancelActionAlarm(getApplicationContext());
                                            retorno();
                                        }
                                    });
                                    builder.show();
                                }
                            }
                        });
                        Usuario usuario = new Usuario();
                        usuario.setUsername(config.getUsername());
                        usuario.setPassword(clave.getEditText().getText().toString());
                        login.execute(usuario);
                    } else {
                        ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                    }
                }
            }
        });

        activarOffline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtil.isNetworkConnected(getBaseContext())) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ConfiguracionActivity.this);
                    builder.setTitle("Modo OFFLINE Activado");
                    builder.setMessage("El modo OFFLINE ha sido activado, a partir de ahora puede trabajar sin necesidad de contar con una conexión a internet.");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Entendido", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            config.setDesMaestras(checkMaestros.isChecked());
                            config.setDesPreVisitas(checkPre.isChecked());
                            config.setDesVisitas(checkMon.isChecked());
                            config.setDesAcometidas(checkAco.isChecked());
                            config.setOffline(true);
                            config.setDesca(true);
                            configDao.insertOrReplace(config);
                            Intent intentMemoryService = new Intent(getApplicationContext(), DescaService.class);
                            startService(intentMemoryService);
                            retorno();
                        }
                    });
                    builder.show();
                } else {
                    ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                }
            }
        });

        entendidoError.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.createAndStart();

        config = configDao.queryBuilder().limit(1).unique();
        user.setText(config.getUsername());
        setupMenu();
        setMenuCounter();
        setOffline();
    }

    public void setOffline() {
        if (config.getOffline()) {
            toolbar.setTitle("Activar modo ONLINE");
            long previsita = suministroDao
                    .queryBuilder()
                    .where(SuministroDao.Properties.Grabar.eq(true)).count();
            long acometida = acometidaDao
                    .queryBuilder()
                    .where(AcometidaDao.Properties.Grabar.eq(true)).count();
            long montante = montanteDao
                    .queryBuilder()
                    .where(MontanteDao.Properties.Grabar.eq(true)).count();
            if (previsita > 0 || acometida > 0 || montante > 0) {
                error.setVisibility(View.VISIBLE);
                consOnline.setVisibility(View.GONE);
            } else {
                error.setVisibility(View.GONE);
                consOnline.setVisibility(View.VISIBLE);
                usuario.getEditText().setText(config.getUsername());
            }
            consOffline.setVisibility(View.GONE);
        } else {
            toolbar.setTitle("Activar modo OFFLINE");
            error.setVisibility(View.GONE);
            consOnline.setVisibility(View.GONE);
            consOffline.setVisibility(View.VISIBLE);
        }
    }

    private void setMenuCounter() {
        long previsita = suministroDao
                .queryBuilder()
                .where(SuministroDao.Properties.Grabar.eq(true)).count();
        long acometida = acometidaDao
                .queryBuilder()
                .where(AcometidaDao.Properties.Grabar.eq(true)).count();
        long montante = montanteDao
                .queryBuilder()
                .where(MontanteDao.Properties.Grabar.eq(true)).count();
        if (previsita > 0) {
            numPre.setText(String.valueOf(previsita));
        } else {
            numPre.setText("");
        }
        if (acometida > 0) {
            numAco.setText(String.valueOf(acometida));
        } else {
            numAco.setText("");
        }
        if (montante > 0) {
            numMon.setText(String.valueOf(montante));
        } else {
            numMon.setText("");
        }
    }

    public void setupMenu() {
        ColorStateList oldColors = preVisitas.getTextColors();
        if (!config.getDesca() && !config.getSync()) {
            preVisitas.setTextColor(oldColors);
            preVisitas.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ConfiguracionActivity.this, BuscarSuministroActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            tv_acometida.setTextColor(oldColors);
            tv_acometida.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ConfiguracionActivity.this, BuscarAcometidaActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            tv_montante.setTextColor(oldColors);
            tv_montante.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ConfiguracionActivity.this, BuscarMontanteActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        } else {
            preVisitas.setTextColor(getResources().getColor(R.color.divider));
            tv_acometida.setTextColor(getResources().getColor(R.color.divider));
            tv_montante.setTextColor(getResources().getColor(R.color.divider));
        }
        sincronizacion.setVisibility(config.getOffline() ? View.VISIBLE : View.GONE);
        imageSync.setVisibility(config.getOffline() ? View.VISIBLE : View.GONE);
        sincronizacion.setText(config.getSync() ? "Sincronizando..." : "Sincronización");
        sincronizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfiguracionActivity.this, SyncActivity.class);
                startActivity(intent);
            }
        });
        offline.setChecked(config.getOffline());
        desConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (abierto) {
                    configVacio.setVisibility(View.VISIBLE);
                    configModo.setVisibility(View.VISIBLE);
                    offline.setVisibility(View.VISIBLE);
                    abierto = false;
                } else {
                    configVacio.setVisibility(View.GONE);
                    configModo.setVisibility(View.GONE);
                    offline.setVisibility(View.GONE);
                    abierto = true;
                }
            }
        });
        offline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cerrarSesion();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (config.getOffline()) {
            getMenuInflater().inflate(R.menu.menu_config_on, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_config_off, menu);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            configDao.insertOrReplace(config);
            retorno();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void retorno() {
        Intent intent = new Intent(ConfiguracionActivity.this, PrincipalActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        countDownTimer.createAndStart();
    }

    private void cerrarSesion() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ConfiguracionActivity.this);
        builder.setTitle(R.string.app_msjTitulo);
        builder.setMessage(R.string.app_msjCuerpo);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.app_msjSi, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (config.getOffline()) {
                    config.setLogin(false);
                    configDao.insertOrReplace(config);
                    Intent intent = new Intent(ConfiguracionActivity.this, IniciarActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        showProgressDialog(R.string.app_cargando);
                        Logout logout = new Logout(new Logout.OnLogoutCompleted() {
                            @Override
                            public void onLogoutCompled(BaseOutRO baseOutRO) {
                                hideProgressDialog();
                                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                                } else if (baseOutRO == null) {
                                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                                } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(baseOutRO.getResultCode())) {
                                    ((App) getApplication()).showToast(baseOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : baseOutRO.getMessage());
                                } else {
                                    countDownTimer.stop();
                                    config.setLogin(false);
                                    configDao.insertOrReplace(config);
                                    Intent intent = new Intent(ConfiguracionActivity.this, IniciarActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        });
                        logout.execute(config);
                    } else {
                        ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                    }
                }
            }
        });
        builder.setNegativeButton(R.string.app_msjNo, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

}
