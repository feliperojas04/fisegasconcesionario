package pe.gob.osinergmin.gnr.cgn.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.BaseInspectorOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.GenericBeanOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.HabilitacionMontanteOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.HabilitacionOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.InstalacionAcometidaOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.MotivoRechazoHabilitacionMontanteOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.MotivoRechazoHabilitacionOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.ParametroOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListBaseInspectorOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListGenericBeanOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListHabilitacionOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListMotivoRechazoHabilitacionMontanteOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListMotivoRechazoHabilitacionOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListParametroOutRO;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.data.models.AmbientePredioDao;
import pe.gob.osinergmin.gnr.cgn.data.models.ConfigDao;
import pe.gob.osinergmin.gnr.cgn.data.models.FusionistaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.GasodomesticoDao;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionMontanteOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MInstalacionAcometidaOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MMotivoRechazoHabilitacionMontanteOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MMotivoRechazoHabilitacionOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MProyectoInstalacionOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MSolicitudOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MaterialInstalacionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.TipoAcometidaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.TipoPendienteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.TipoRechazadaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.TipoResultadoDao;
import pe.gob.osinergmin.gnr.cgn.data.models.TiposInstalacionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.TiposInstalacionMontanteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionMontanteOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MInstalacionAcometidaOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.TiposMarcaAccesorioDao;
import pe.gob.osinergmin.gnr.cgn.data.models.TiposMarcaTuberiaDao;
import pe.gob.osinergmin.gnr.cgn.task.AcometidaRx;
import pe.gob.osinergmin.gnr.cgn.task.ListaHabilitacion;
import pe.gob.osinergmin.gnr.cgn.task.MontanteRx;
import pe.gob.osinergmin.gnr.cgn.task.ParametroAll;
import pe.gob.osinergmin.gnr.cgn.task.TipoAcometida;
import pe.gob.osinergmin.gnr.cgn.task.TipoAmbientePredio;
import pe.gob.osinergmin.gnr.cgn.task.TipoFusionista;
import pe.gob.osinergmin.gnr.cgn.task.TipoGasoDomestico;
import pe.gob.osinergmin.gnr.cgn.task.TipoInstalacion;
import pe.gob.osinergmin.gnr.cgn.task.TipoMarcaAccesorio;
import pe.gob.osinergmin.gnr.cgn.task.TipoMarcaTuberia;
import pe.gob.osinergmin.gnr.cgn.task.TipoMaterialInstalacion;
import pe.gob.osinergmin.gnr.cgn.task.TipoMotivoRechazo;
import pe.gob.osinergmin.gnr.cgn.task.TipoMotivoRechazoMontante;
import pe.gob.osinergmin.gnr.cgn.task.TipoObservacionPendiente;
import pe.gob.osinergmin.gnr.cgn.task.TipoObservacionRechazada;
import pe.gob.osinergmin.gnr.cgn.task.TipoResultadoAcometida;
import pe.gob.osinergmin.gnr.cgn.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.util.FormatoFecha;
import pe.gob.osinergmin.gnr.cgn.util.NetworkUtil;
import pe.gob.osinergmin.gnr.cgn.util.Parse;

public class DescaService extends Service {

    private static final String TAG = DescaService.class.getSimpleName();
    private MParametroOutRODao mParametroOutRODao;
    private ConfigDao configDao;
    private Config config;
    private MHabilitacionOutRODao mHabilitacionOutRODao;
    private MHabilitacionMontanteOutRODao mHabilitacionMontanteOutRODao;
    private MSolicitudOutRODao mSolicitudOutRODao;
    private MProyectoInstalacionOutRODao mProyectoInstalacionOutRODao;
    private MMotivoRechazoHabilitacionOutRODao mMotivoRechazoHabilitacionOutRODao;
    private GasodomesticoDao gasodomesticoDao;
    private MaterialInstalacionDao materialInstalacionDao;
    private TipoAcometidaDao tipoAcometidaDao;
    private TiposInstalacionDao tiposInstalacionDao;
    private TiposMarcaAccesorioDao tiposMarcaAccesorioDao;
    private TiposMarcaTuberiaDao tiposMarcaTuberiaDao;
    private TipoResultadoDao tipoResultadoDao;
    private TipoPendienteDao tipoPendienteDao;
    private TipoRechazadaDao tipoRechazadaDao;
    private FusionistaDao fusionistaDao;
    private TiposInstalacionMontanteDao tiposInstalacionMontanteDao;
    private AmbientePredioDao ambientePredioDao;
    private MInstalacionAcometidaOutRODao mInstalacionAcometidaOutRODao;
    private int cont = 0, porcentaje = 0;
    private boolean sync = true, extra, sinWifi = false;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private MMotivoRechazoHabilitacionMontanteOutRODao mMotivoRechazoHabilitacionMontanteOutRODao;

    public DescaService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Servicio creado...");

        configDao = ((App) getApplication()).getDaoSession().getConfigDao();
        mHabilitacionOutRODao = ((App) getApplication()).getDaoSession().getMHabilitacionOutRODao();
        mHabilitacionMontanteOutRODao = ((App) getApplication()).getDaoSession().getMHabilitacionMontanteOutRODao();
        mSolicitudOutRODao = ((App) getApplication()).getDaoSession().getMSolicitudOutRODao();
        mProyectoInstalacionOutRODao = ((App) getApplication()).getDaoSession().getMProyectoInstalacionOutRODao();
        mParametroOutRODao = ((App) getApplication()).getDaoSession().getMParametroOutRODao();
        gasodomesticoDao = ((App) getApplication()).getDaoSession().getGasodomesticoDao();
        materialInstalacionDao = ((App) getApplication()).getDaoSession().getMaterialInstalacionDao();
        tipoAcometidaDao = ((App) getApplication()).getDaoSession().getTipoAcometidaDao();
        tiposInstalacionDao = ((App) getApplication()).getDaoSession().getTiposInstalacionDao();
        tiposMarcaTuberiaDao = ((App) getApplication()).getDaoSession().getTiposMarcaTuberiaDao();
        tiposMarcaAccesorioDao = ((App) getApplication()).getDaoSession().getTiposMarcaAccesorioDao();
        tiposInstalacionMontanteDao = ((App) getApplication()).getDaoSession().getTiposInstalacionMontanteDao();
        ambientePredioDao = ((App) getApplication()).getDaoSession().getAmbientePredioDao();
        mMotivoRechazoHabilitacionOutRODao = ((App) getApplication()).getDaoSession().getMMotivoRechazoHabilitacionOutRODao();
        mInstalacionAcometidaOutRODao = ((App) getApplication()).getDaoSession().getMInstalacionAcometidaOutRODao();
        tipoResultadoDao = ((App) getApplication()).getDaoSession().getTipoResultadoDao();
        tipoPendienteDao = ((App) getApplication()).getDaoSession().getTipoPendienteDao();
        tipoRechazadaDao = ((App) getApplication()).getDaoSession().getTipoRechazadaDao();
        fusionistaDao = ((App) getApplication()).getDaoSession().getFusionistaDao();
        config = configDao.queryBuilder().limit(1).unique();
        mMotivoRechazoHabilitacionMontanteOutRODao = ((App) getApplication()).getDaoSession().getMMotivoRechazoHabilitacionMontanteOutRODao();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Servicio iniciado...");

        extra = intent.getBooleanExtra("EXTRA", false);
        final NotificationCompat.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = new NotificationCompat.Builder(this, Constantes.CHANNEL_ID)
                    .setSmallIcon(android.R.drawable.stat_sys_download_done)
                    .setContentTitle("CGN - Concesionaria")
                    .setContentText("Descargando datos...");
        } else {
            builder = new NotificationCompat.Builder(this)
                    .setSmallIcon(android.R.drawable.stat_sys_download_done)
                    .setContentTitle("CGN - Concesionaria")
                    .setContentText("Descargando datos...");
        }
        builder.setProgress(18, porcentaje, false);
        startForeground(1, builder.build());

        if (config.getDesMaestras()) {
            sync = false;
            mParametroOutRODao.deleteAll();
            cont++;
            ParametroAll parametroAll = new ParametroAll(new ParametroAll.OnParametroAllCompleted() {
                @Override
                public void onParametroAllCompled(ListParametroOutRO listParametroOutRO) {
                    if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        sinWifi = true;
                        stopSelf();
                    } else if (listParametroOutRO == null || !listParametroOutRO.getResultCode().equalsIgnoreCase(gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS)) {
                        stopSelf();
                    } else {
                        for (ParametroOutRO parametroOutRO : listParametroOutRO.getParametro()) {
                            MParametroOutRO mParametroOutRO = new MParametroOutRO();
                            mParametroOutRO.setNombreParametro(parametroOutRO.getNombreParametro());
                            mParametroOutRO.setDescripcionParametro(parametroOutRO.getDescripcionParametro());
                            mParametroOutRO.setValorParametro(parametroOutRO.getValorParametro());
                            mParametroOutRODao.insertOrReplace(mParametroOutRO);
                        }
                        porcentaje++;
                        builder.setProgress(18, porcentaje, false);
                        startForeground(1, builder.build());
                        cont--;
                        if (cont == 0) {
                            stopSelf();
                        }
                    }
                }
            });
            parametroAll.execute(config);
            cont++;

            TipoAcometida tipoAcometida = new TipoAcometida(new TipoAcometida.OnTipoAcometidaCompleted() {
                @Override
                public void onTipoAcometidaCompled(ListGenericBeanOutRO listGenericBeanOutRO) {
                    if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        sinWifi = true;
                        stopSelf();
                    } else if (listGenericBeanOutRO == null || !gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                        stopSelf();
                    } else {
                        tipoAcometidaDao.deleteAll();
                        for (GenericBeanOutRO genericBeanOutRO : listGenericBeanOutRO.getGenericBean()) {
                            tipoAcometidaDao.insertOrReplace(Parse.getTipoAcometida(genericBeanOutRO));
                        }
                        porcentaje++;
                        builder.setProgress(18, porcentaje, false);
                        startForeground(1, builder.build());
                        cont--;
                        if (cont == 0) {
                            stopSelf();
                        }
                    }
                }
            });
            tipoAcometida.execute(config);
            cont++;

            TipoInstalacion tipoInstalacion = new TipoInstalacion(new TipoInstalacion.OnTipoInstalacionCompleted() {
                @Override
                public void onTipoInstalacionCompled(ListGenericBeanOutRO listGenericBeanOutRO) {
                    if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        sinWifi = true;
                        stopSelf();
                    } else if (listGenericBeanOutRO == null || !gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                        stopSelf();
                    } else {
                        tiposInstalacionDao.deleteAll();
                        for (GenericBeanOutRO genericBeanOutRO : listGenericBeanOutRO.getGenericBean()) {
                            tiposInstalacionDao.insertOrReplace(Parse.getTiposInstalacion(genericBeanOutRO));
                        }
                        porcentaje++;
                        builder.setProgress(18, porcentaje, false);
                        startForeground(1, builder.build());
                        cont--;
                        if (cont == 0) {
                            stopSelf();
                        }
                    }
                }
            });
            tipoInstalacion.execute(config);
            cont++;

            TipoMarcaAccesorio tipoMarcaAccesorio = new TipoMarcaAccesorio(new TipoMarcaAccesorio.OnTipoMarcaAccesorioCompleted() {
                @Override
                public void onTipoMarcaAccesorioCompled(ListGenericBeanOutRO listGenericBeanOutRO) {
                    if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        sinWifi = true;
                        stopSelf();
                    } else if (listGenericBeanOutRO == null || !gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                        stopSelf();
                    } else {
                        tiposMarcaAccesorioDao.deleteAll();
                        for (GenericBeanOutRO genericBeanOutRO : listGenericBeanOutRO.getGenericBean()) {
                            tiposMarcaAccesorioDao.insertOrReplace(Parse.getTiposMarcaAccesorio(genericBeanOutRO));
                        }
                        porcentaje++;
                        builder.setProgress(18, porcentaje, false);
                        startForeground(1, builder.build());
                        cont--;
                        if (cont == 0) {
                            stopSelf();
                        }
                    }
                }
            });
            tipoMarcaAccesorio.execute(config);
            cont++;

            TipoMarcaTuberia tipoMarcaTuberia = new TipoMarcaTuberia(new TipoMarcaTuberia.OnTipoMarcaTuberiaCompleted() {
                @Override
                public void onTipoMarcaTuberiaCompled(ListGenericBeanOutRO listGenericBeanOutRO) {
                    if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        sinWifi = true;
                        stopSelf();
                    } else if (listGenericBeanOutRO == null || !gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                        stopSelf();
                    } else {
                        tiposMarcaTuberiaDao.deleteAll();
                        for (GenericBeanOutRO genericBeanOutRO : listGenericBeanOutRO.getGenericBean()) {
                            tiposMarcaTuberiaDao.insertOrReplace(Parse.getTiposMarcaTuberia(genericBeanOutRO));
                        }
                        porcentaje++;
                        builder.setProgress(18, porcentaje, false);
                        startForeground(1, builder.build());
                        cont--;
                        if (cont == 0) {
                            stopSelf();
                        }
                    }
                }
            });
            tipoMarcaTuberia.execute(config);
            cont++;

            disposables.add(MontanteRx.getTiposInstalacion(config.getToken())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(listGenericBeanOutRO -> {
                        if (listGenericBeanOutRO == null || !gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                            stopSelf();
                        } else {
                            tiposInstalacionMontanteDao.deleteAll();
                            for (GenericBeanOutRO genericBeanOutRO : listGenericBeanOutRO.getGenericBean()) {
                                tiposInstalacionMontanteDao.insertOrReplace(Parse.getTiposInstalacionMontante(genericBeanOutRO));
                            }
                            porcentaje++;
                            builder.setProgress(18, porcentaje, false);
                            startForeground(1, builder.build());
                            cont--;
                            if (cont == 0) {
                                stopSelf();
                            }
                        }
                    }, throwable -> {
                        if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                            sinWifi = true;
                        }
                        stopSelf();
                    }));
            cont++;

            TipoMaterialInstalacion tipoMaterialInstalacion = new TipoMaterialInstalacion(new TipoMaterialInstalacion.OnTipoMaterialInstalacionCompleted() {
                @Override
                public void onTipoMaterialInstalacionCompled(ListGenericBeanOutRO listGenericBeanOutRO) {
                    if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        sinWifi = true;
                        stopSelf();
                    } else if (listGenericBeanOutRO == null || !gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                        stopSelf();
                    } else {
                        materialInstalacionDao.deleteAll();
                        for (GenericBeanOutRO genericBeanOutRO : listGenericBeanOutRO.getGenericBean()) {
                            materialInstalacionDao.insertOrReplace(Parse.getMaterialInstalacion(genericBeanOutRO));
                        }
                        porcentaje++;
                        builder.setProgress(18, porcentaje, false);
                        startForeground(1, builder.build());
                        cont--;
                        if (cont == 0) {
                            stopSelf();
                        }
                    }
                }
            });
            tipoMaterialInstalacion.execute(config);
            cont++;

            TipoGasoDomestico tipoGasoDomestico = new TipoGasoDomestico(new TipoGasoDomestico.OnGasoDomesticoCompleted() {
                @Override
                public void onGasoDomesticoCompled(ListGenericBeanOutRO listGenericBeanOutRO) {
                    if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        sinWifi = true;
                        stopSelf();
                    } else if (listGenericBeanOutRO == null || !gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                        stopSelf();
                    } else {
                        gasodomesticoDao.deleteAll();
                        for (GenericBeanOutRO genericBeanOutRO : listGenericBeanOutRO.getGenericBean()) {
                            gasodomesticoDao.insertOrReplace(Parse.getGasodomestico(genericBeanOutRO));
                        }
                        porcentaje++;
                        builder.setProgress(18, porcentaje, false);
                        startForeground(1, builder.build());
                        cont--;
                        if (cont == 0) {
                            stopSelf();
                        }
                    }
                }
            });
            tipoGasoDomestico.execute(config);
            cont++;

            TipoAmbientePredio tipoAmbientePredio = new TipoAmbientePredio(new TipoAmbientePredio.OnAmbientePredioCompleted() {
                @Override
                public void onAmbientePredioCompled(ListGenericBeanOutRO listGenericBeanOutRO) {
                    if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        sinWifi = true;
                        stopSelf();
                    } else if (listGenericBeanOutRO == null || !gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                        stopSelf();
                    } else {
                        ambientePredioDao.deleteAll();
                        for (GenericBeanOutRO genericBeanOutRO : listGenericBeanOutRO.getGenericBean()) {
                            ambientePredioDao.insertOrReplace(Parse.getAmbientePredio(genericBeanOutRO));
                        }
                        porcentaje++;
                        builder.setProgress(18, porcentaje, false);
                        startForeground(1, builder.build());
                        cont--;
                        if (cont == 0) {
                            stopSelf();
                        }
                    }
                }
            });
            tipoAmbientePredio.execute(config);
            cont++;

            TipoMotivoRechazo tipoMotivoRechazo = new TipoMotivoRechazo(new TipoMotivoRechazo.OnTipoMotivoRechazoCompleted() {
                @Override
                public void onTipoMotivoRechazoCompled(ListMotivoRechazoHabilitacionOutRO listMotivoRechazoHabilitacionOutRO) {
                    if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        sinWifi = true;
                        stopSelf();
                    } else if (listMotivoRechazoHabilitacionOutRO == null || !gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listMotivoRechazoHabilitacionOutRO.getResultCode())) {
                        stopSelf();
                    } else {
                        mMotivoRechazoHabilitacionOutRODao.deleteAll();
                        for (MotivoRechazoHabilitacionOutRO motivoRechazoHabilitacion : listMotivoRechazoHabilitacionOutRO.getMotivoRechazoHabilitacion()) {
                            mMotivoRechazoHabilitacionOutRODao.insertOrReplace(Parse.getMMotivoRechazoHabilitacionOutRO(motivoRechazoHabilitacion));
                        }
                        porcentaje++;
                        builder.setProgress(18, porcentaje, false);
                        startForeground(1, builder.build());
                        cont--;
                        if (cont == 0) {
                            stopSelf();
                        }
                    }
                }
            });
            tipoMotivoRechazo.execute(config);
            cont++;

            TipoResultadoAcometida tipoResultado = new TipoResultadoAcometida(new TipoResultadoAcometida.OnTipoResultadoCompleted() {
                @Override
                public void onTipoResultadoCompled(ListGenericBeanOutRO listGenericBeanOutRO) {
                    if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        sinWifi = true;
                        stopSelf();
                    } else if (listGenericBeanOutRO == null || !gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                        stopSelf();
                    } else {
                        tipoResultadoDao.deleteAll();
                        for (GenericBeanOutRO genericBeanOutRO : listGenericBeanOutRO.getGenericBean()) {
                            tipoResultadoDao.insertOrReplace(Parse.getTipoResultado(genericBeanOutRO));
                        }
                        porcentaje++;
                        builder.setProgress(18, porcentaje, false);
                        startForeground(1, builder.build());
                        cont--;
                        if (cont == 0) {
                            stopSelf();
                        }
                    }
                }
            });
            tipoResultado.execute(config);
            cont++;

            TipoObservacionPendiente tipoObservacionPendiente = new TipoObservacionPendiente(new TipoObservacionPendiente.OnTipoObservacionCompleted() {
                @Override
                public void onTipoObservacionCompleted(ListGenericBeanOutRO listGenericBeanOutRO) {
                    if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        sinWifi = true;
                        stopSelf();
                    } else if (listGenericBeanOutRO == null || !gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                        stopSelf();
                    } else {
                        tipoPendienteDao.deleteAll();
                        for (GenericBeanOutRO genericBeanOutRO : listGenericBeanOutRO.getGenericBean()) {
                            tipoPendienteDao.insertOrReplace(Parse.getTipoPendiente(genericBeanOutRO));
                        }
                        porcentaje++;
                        builder.setProgress(18, porcentaje, false);
                        startForeground(1, builder.build());
                        cont--;
                        if (cont == 0) {
                            stopSelf();
                        }
                    }
                }
            });
            tipoObservacionPendiente.execute(config);
            cont++;

            TipoObservacionRechazada tipoObservacionRechazada = new TipoObservacionRechazada(new TipoObservacionRechazada.OnTipoObservacionCompleted() {
                @Override
                public void onTipoObservacionCompleted(ListGenericBeanOutRO listGenericBeanOutRO) {
                    if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        sinWifi = true;
                        stopSelf();
                    } else if (listGenericBeanOutRO == null || !gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                        stopSelf();
                    } else {
                        tipoRechazadaDao.deleteAll();
                        for (GenericBeanOutRO genericBeanOutRO : listGenericBeanOutRO.getGenericBean()) {
                            tipoRechazadaDao.insertOrReplace(Parse.getTipoRechazada(genericBeanOutRO));
                        }
                        porcentaje++;
                        builder.setProgress(18, porcentaje, false);
                        startForeground(1, builder.build());
                        cont--;
                        if (cont == 0) {
                            stopSelf();
                        }
                    }
                }
            });
            tipoObservacionRechazada.execute(config);
            cont++;

            TipoFusionista tipoFusionista = new TipoFusionista(new TipoFusionista.OnTipoFusionistaCompleted() {
                @Override
                public void onTipoFusionistaCompled(ListBaseInspectorOutRO listBaseInspectorOutRO) {
                    if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        sinWifi = true;
                        stopSelf();
                    } else if (listBaseInspectorOutRO == null || !gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listBaseInspectorOutRO.getResultCode())) {
                        stopSelf();
                    } else {
                        fusionistaDao.deleteAll();
                        for (BaseInspectorOutRO baseInspectorOutRO : listBaseInspectorOutRO.getInspector()) {
                            fusionistaDao.insertOrReplace(Parse.getFusionista(baseInspectorOutRO));
                        }
                        porcentaje++;
                        builder.setProgress(18, porcentaje, false);
                        startForeground(1, builder.build());
                        cont--;
                        if (cont == 0) {
                            stopSelf();
                        }
                    }
                }
            });
            tipoFusionista.execute(config);
            cont++;

            TipoMotivoRechazoMontante tipoMotivoRechazoMontante = new TipoMotivoRechazoMontante(new TipoMotivoRechazoMontante.OnTipoMotivoRechazoCompleted() {
                @Override
                public void onTipoMotivoRechazoCompled(ListMotivoRechazoHabilitacionMontanteOutRO listMotivoRechazoHabilitacionMontanteOutRO) {
                    if(!NetworkUtil.isNetworkConnected(getApplicationContext())){
                        sinWifi = true;
                        stopSelf();
                    }else if (listMotivoRechazoHabilitacionMontanteOutRO ==null || !gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listMotivoRechazoHabilitacionMontanteOutRO.getResultCode())){
                        stopSelf();
                    }else {
                        mMotivoRechazoHabilitacionMontanteOutRODao.deleteAll();
                        for(MotivoRechazoHabilitacionMontanteOutRO motivoRechazoHabilitacionMontanteOutRO: listMotivoRechazoHabilitacionMontanteOutRO.getMotivoRechazoHabilitacionMontante()){
                            mMotivoRechazoHabilitacionMontanteOutRODao.insertOrReplace(Parse.getMMotivoRechazoHabilitacionMontanteOutRO(motivoRechazoHabilitacionMontanteOutRO));
                        }
                        porcentaje++;
                        builder.setProgress(18, porcentaje, false);
                        startForeground(1, builder.build());
                        cont--;
                        if(cont == 0){
                            stopSelf();
                        }
                    }
                }
            });
            tipoMotivoRechazoMontante.execute(config);
        }

        if (config.getDesPreVisitas()) {
            sync = false;
            cont++;
            List<MHabilitacionOutRO> mHabilitacionOutROS = mHabilitacionOutRODao.queryBuilder().where(MHabilitacionOutRODao.Properties.ResultCode.isNull()).list();
            mHabilitacionOutRODao.deleteInTx(mHabilitacionOutROS);
            ListaHabilitacion listaHabilitacion = new ListaHabilitacion(new ListaHabilitacion.OnListaHabilitacionCompleted() {
                @Override
                public void onListaHabilitacionCompled(ListHabilitacionOutRO listHabilitacionOutRO) {
                    if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        sinWifi = true;
                        stopSelf();
                    } else if (listHabilitacionOutRO == null || !gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listHabilitacionOutRO.getResultCode())) {
                        stopSelf();
                    } else {
                        for (HabilitacionOutRO habilitacionOutRO : listHabilitacionOutRO.getHabilitacion()) {
                            mSolicitudOutRODao.insertOrReplace(Parse.getMSolicitudOutRO(habilitacionOutRO.getSolicitud()));
                            try {
                                mHabilitacionOutRODao.insert(Parse.getMHabilitacionOutRO(habilitacionOutRO));
                            } catch (Exception e) {
                                Log.e("SYNC", e.getMessage(), e);
                            }
                        }
                        porcentaje++;
                        builder.setProgress(18, porcentaje, false);
                        startForeground(1, builder.build());
                        cont--;
                        if (cont == 0) {
                            stopSelf();
                        }
                    }
                }
            });
            listaHabilitacion.execute(config);
        }

        if (config.getDesVisitas()) {
            sync = false;
            cont++;
            List<MHabilitacionMontanteOutRO> mHabilitacionMontanteOutROS = mHabilitacionMontanteOutRODao.queryBuilder().where(MHabilitacionMontanteOutRODao.Properties.ResultCode.isNull()).list();
            mHabilitacionMontanteOutRODao.deleteInTx(mHabilitacionMontanteOutROS);
            disposables.add(MontanteRx.getMontanteListar(config.getToken()).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(listHabilitacionMontanteOutRO -> {
                        if (listHabilitacionMontanteOutRO == null || !gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listHabilitacionMontanteOutRO.getResultCode())) {
                            stopSelf();
                        } else {
                            for (HabilitacionMontanteOutRO habilitacionMontanteOutRO : listHabilitacionMontanteOutRO.getHabilitacionMontante()) {
                                mProyectoInstalacionOutRODao.insertOrReplace(Parse.getMProyectoInstalacionOutRO(habilitacionMontanteOutRO.getProyectoInstalacion()));
                                try {
                                    mHabilitacionMontanteOutRODao.insert(Parse.getMHabilitacionMontanteOutRO(habilitacionMontanteOutRO));
                                } catch (Exception e) {
                                    Log.e("SYNC", e.getMessage(), e);
                                }
                            }
                            porcentaje++;
                            builder.setProgress(18, porcentaje, false);
                            startForeground(1, builder.build());
                            cont--;
                            if (cont == 0) {
                                stopSelf();
                            }
                        }
                    }, throwable -> {
                        if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                            sinWifi = true;
                        }
                        stopSelf();
                    }));
        }

        if (config.getDesAcometidas()) {
            sync = false;
            cont++;
            List<MInstalacionAcometidaOutRO> mInstalacionAcometidaOutROS = mInstalacionAcometidaOutRODao.queryBuilder().where(MInstalacionAcometidaOutRODao.Properties.ResultCode.isNull()).list();
            mInstalacionAcometidaOutRODao.deleteInTx(mInstalacionAcometidaOutROS);
            disposables.add(AcometidaRx.getAcometidaListar(config.getToken()).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(listInstalacionAcometidaOutRO -> {
                        if (listInstalacionAcometidaOutRO == null || !gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listInstalacionAcometidaOutRO.getResultCode())) {
                            stopSelf();
                        } else {
                            for (InstalacionAcometidaOutRO instalacionAcometidaOutRO : listInstalacionAcometidaOutRO.getInstalacionAcometida()) {
                                mSolicitudOutRODao.insertOrReplace(Parse.getMSolicitudOutRO(instalacionAcometidaOutRO.getSolicitud()));
                                try {
                                    mInstalacionAcometidaOutRODao.insert(Parse.getMInstalacionAcometidaOutRO(instalacionAcometidaOutRO));
                                } catch (Exception e) {
                                    Log.e("SYNC", e.getMessage(), e);
                                }
                            }
                            porcentaje++;
                            builder.setProgress(18, porcentaje, false);
                            startForeground(1, builder.build());
                            cont--;
                            if (cont == 0) {
                                stopSelf();
                            }
                        }
                    }, throwable -> {
                        if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                            sinWifi = true;
                        }
                        stopSelf();
                    }));
        }

        if (sync) {
            stopSelf();
        }

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        config.setDesca(false);
        if (sinWifi) {
            config.setOffline(false);
        }
        configDao.insertOrReplace(config);
        Intent localIntent = new Intent(Constantes.ACTION_EXIT_SERVICE);
        if (extra) {
            localIntent.putExtra("tipo", "extra");
            localIntent.putExtra("sinWifi", sinWifi);
        } else {
            SharedPreferences mPreferences = getSharedPreferences("gnrconcesionario", Context.MODE_PRIVATE);
            Long fecha = FormatoFecha.getFechaMoreOneDay();
            SharedPreferences.Editor editor = mPreferences.edit();
            editor.putLong("fecha", fecha);
            editor.apply();
            AlarmService.startActionAlarm(getApplicationContext(), fecha);
            localIntent.putExtra("tipo", "descarga");
            localIntent.putExtra("sinWifi", sinWifi);
        }
        LocalBroadcastManager.getInstance(DescaService.this).sendBroadcast(localIntent);
        localIntent = new Intent(Constantes.ACTION_SERVICE);
        localIntent.putExtra("sinWifi", sinWifi);
        LocalBroadcastManager.getInstance(DescaService.this).sendBroadcast(localIntent);
        stopForeground(true);
        disposables.clear();
        Log.d(TAG, "Servicio destruido...");
    }


}
