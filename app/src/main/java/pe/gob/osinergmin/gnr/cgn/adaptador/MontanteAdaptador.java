package pe.gob.osinergmin.gnr.cgn.adaptador;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.BaseHabilitacionMontanteOutRO;
import pe.gob.osinergmin.gnr.cgn.R;

public class MontanteAdaptador extends RecyclerView.Adapter<MontanteAdaptador.ViewHolder> {

    private CallBack mCallback;
    private List<BaseHabilitacionMontanteOutRO> base;

    public MontanteAdaptador() {
        this.base = new ArrayList<>();
    }

    public void add(List<BaseHabilitacionMontanteOutRO> mc) {
        base.addAll(mc);
        notifyItemInserted(base.size() - 1);
    }

    public void remove(BaseHabilitacionMontanteOutRO city) {
        int position = base.indexOf(city);
        if (position > -1) {
            base.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public void setCallback(CallBack callback) {
        mCallback = callback;
    }

    public List<BaseHabilitacionMontanteOutRO> getItems() {
        return base;
    }

    public BaseHabilitacionMontanteOutRO getItem(int position) {
        return this.base.get(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_montante, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final BaseHabilitacionMontanteOutRO obj = this.base.get(position);
        String suministro = "<b>Objeto de Conexión:</b> %1$s";
        String suministroF = String.format(suministro, obj.getCodigoObjetoConexionProyectoInstalacion());
        holder.txtBusI_propietario.setText(Html.fromHtml(suministroF));
        String visitado = "<b>CUP:</b> %1$s";
        String visitadoF = String.format(visitado, obj.getCodigoUnidadPredialProyectoInstalacion());
        holder.txtBusI_predio.setText(Html.fromHtml(visitadoF));
        String predio = "<b>Proyecto:</b> %1$s";
        String predioF = String.format(predio, obj.getNombreProyectoInstalacion());
        holder.txtBusI_suministros.setText(Html.fromHtml(predioF));
        String montante = "<b>Montante:</b> %1$s";
        String montanteF = String.format(montante, obj.getNombreMontanteProyectoInstalacion());
        holder.txtBusIMontante.setText(Html.fromHtml(montanteF));
        String visitante = "<b>Dirección:</b> %1$s";
        String visitanteF = String.format(visitante, obj.getDireccionUbigeoProyectoInstalacion());
        holder.txtBusI_contrato.setText(Html.fromHtml(visitanteF));

        if (obj.getErrorCode() == null || "".equalsIgnoreCase(obj.getErrorCode())) {
            holder.btnBusI_error.setVisibility(View.GONE);
            holder.btnBusI_eliminar.setVisibility(View.GONE);
            if (obj.getResultCode() == null || "".equalsIgnoreCase(obj.getResultCode())) {
                holder.contenedor.setBackgroundResource(R.color.blanco);
            } else {
                holder.contenedor.setBackgroundResource(R.color.mod);
            }
        } else {
            holder.btnBusI_error.setVisibility(View.VISIBLE);
            holder.btnBusI_eliminar.setVisibility(obj.getResultCode().equalsIgnoreCase("101") ? View.GONE : View.VISIBLE);
            holder.contenedor.setBackgroundResource(R.color.error);
            holder.btnBusI_error.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setTitle("Causa del error en la sincronización");
                    builder.setMessage(obj.getMessage());
                    builder.setCancelable(false);
                    builder.setPositiveButton("Entendido", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.show();
                }
            });
            holder.btnBusI_eliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onInstalacionEliminar(position);
                }
            });
        }
        holder.contenedor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onSelect(obj, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return base.size();
    }

    public interface CallBack {
        void onInstalacionEliminar(int position);

        void onSelect(BaseHabilitacionMontanteOutRO base, int position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtBusI_propietario, txtBusI_predio, txtBusI_suministros, txtBusI_contrato,txtBusIMontante;
        Button btnBusI_error, btnBusI_eliminar;
        RelativeLayout contenedor;

        ViewHolder(View v) {
            super(v);
            txtBusI_propietario = v.findViewById(R.id.txtBusI_propietario);
            txtBusI_predio = v.findViewById(R.id.txtBusI_predio);
            txtBusI_suministros = v.findViewById(R.id.txtBusI_suministros);
            txtBusIMontante = v.findViewById(R.id.txtBusIMontante);
            txtBusI_contrato = v.findViewById(R.id.txtBusI_contrato);
            btnBusI_error = v.findViewById(R.id.btnBusI_error);
            btnBusI_eliminar = v.findViewById(R.id.btnBusI_eliminar);
            contenedor = v.findViewById(R.id.contenedor);
        }
    }
}
