package pe.gob.osinergmin.gnr.cgn.util;

import java.math.BigDecimal;

import gob.osinergmin.gnr.domain.dto.rest.out.BaseHabilitacionMontanteOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.BaseHabilitacionOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.BaseInspectorOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.BaseInstalacionAcometidaOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.GenericBeanOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.HabilitacionMontanteOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.HabilitacionOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.InstalacionAcometidaOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.MotivoRechazoHabilitacionMontanteOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.MotivoRechazoHabilitacionOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.ProyectoInstalacionOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.SolicitudOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.AmbientePredio;
import pe.gob.osinergmin.gnr.cgn.data.models.Fusionista;
import pe.gob.osinergmin.gnr.cgn.data.models.Gasodomestico;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionMontanteOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MInstalacionAcometidaOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MMotivoRechazoHabilitacionMontanteOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MMotivoRechazoHabilitacionOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MProyectoInstalacionOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MSolicitudOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MaterialInstalacion;
import pe.gob.osinergmin.gnr.cgn.data.models.TipoAcometida;
import pe.gob.osinergmin.gnr.cgn.data.models.TipoGabinete;
import pe.gob.osinergmin.gnr.cgn.data.models.TipoPendiente;
import pe.gob.osinergmin.gnr.cgn.data.models.TipoRechazada;
import pe.gob.osinergmin.gnr.cgn.data.models.TipoResultado;
import pe.gob.osinergmin.gnr.cgn.data.models.TiposInstalacion;
import pe.gob.osinergmin.gnr.cgn.data.models.TiposInstalacionMontante;
import pe.gob.osinergmin.gnr.cgn.data.models.TiposMarcaAccesorio;
import pe.gob.osinergmin.gnr.cgn.data.models.TiposMarcaTuberia;

public class Parse {

    public static TipoAcometida getTipoAcometida(GenericBeanOutRO genericBeanOutRO) {
        return new TipoAcometida(null, genericBeanOutRO.getValue(), genericBeanOutRO.getLabel());
    }

    public static GenericBeanOutRO getGenericBeanOutRO(TipoAcometida tipoAcometida) {
        return new GenericBeanOutRO(tipoAcometida.getValue(), tipoAcometida.getLabel());
    }

    public static TiposInstalacion getTiposInstalacion(GenericBeanOutRO genericBeanOutRO) {
        return new TiposInstalacion(null, genericBeanOutRO.getValue(), genericBeanOutRO.getLabel());
    }

    public static TiposMarcaAccesorio getTiposMarcaAccesorio(GenericBeanOutRO genericBeanOutRO) {
        return new TiposMarcaAccesorio(null, genericBeanOutRO.getValue(), genericBeanOutRO.getLabel());
    }

    public static TiposMarcaTuberia getTiposMarcaTuberia(GenericBeanOutRO genericBeanOutRO) {
        return new TiposMarcaTuberia(null, genericBeanOutRO.getValue(), genericBeanOutRO.getLabel());
    }

    public static TiposInstalacionMontante getTiposInstalacionMontante(GenericBeanOutRO genericBeanOutRO) {
        return new TiposInstalacionMontante(null, genericBeanOutRO.getValue(), genericBeanOutRO.getLabel());
    }

    public static GenericBeanOutRO getGenericBeanOutRO(TiposInstalacion tiposInstalacion) {
        return new GenericBeanOutRO(tiposInstalacion.getValue(), tiposInstalacion.getLabel());
    }

    public static GenericBeanOutRO getGenericBeanOutRO(TiposMarcaAccesorio tiposMarcaAccesorio) {
        return new GenericBeanOutRO(tiposMarcaAccesorio.getValue(), tiposMarcaAccesorio.getLabel());
    }

    public static GenericBeanOutRO getGenericBeanOutRO(TiposMarcaTuberia tiposMarcaTuberia) {
        return new GenericBeanOutRO(tiposMarcaTuberia.getValue(), tiposMarcaTuberia.getLabel());
    }

    public static GenericBeanOutRO getGenericBeanOutRO(TiposInstalacionMontante tiposInstalacionMontante) {
        return new GenericBeanOutRO(tiposInstalacionMontante.getValue(), tiposInstalacionMontante.getLabel());
    }

    public static MaterialInstalacion getMaterialInstalacion(GenericBeanOutRO genericBeanOutRO) {
        return new MaterialInstalacion(null, genericBeanOutRO.getValue(), genericBeanOutRO.getLabel());
    }

    public static GenericBeanOutRO getGenericBeanOutRO(MaterialInstalacion materialInstalacion) {
        return new GenericBeanOutRO(materialInstalacion.getValue(), materialInstalacion.getLabel());
    }

    /* PAPU 2011 - 0*/
    public static GenericBeanOutRO getGenericBeanOutRO(TipoGabinete tipoGabinete) {
        return new GenericBeanOutRO(tipoGabinete.getValue(), tipoGabinete.getLabel());
    }
    /* PAPU 2011 - 1*/

    public static Gasodomestico getGasodomestico(GenericBeanOutRO genericBeanOutRO) {
        return new Gasodomestico(null, genericBeanOutRO.getValue(), genericBeanOutRO.getLabel());
    }

    public static GenericBeanOutRO getGenericBeanOutRO(Gasodomestico gasodomestico) {
        return new GenericBeanOutRO(gasodomestico.getValue(), gasodomestico.getLabel());
    }

    public static AmbientePredio getAmbientePredio(GenericBeanOutRO genericBeanOutRO) {
        return new AmbientePredio(null, genericBeanOutRO.getValue(), genericBeanOutRO.getLabel());
    }

    public static TipoResultado getTipoResultado(GenericBeanOutRO genericBeanOutRO) {
        return new TipoResultado(null, genericBeanOutRO.getValue(), genericBeanOutRO.getLabel());
    }

    public static TipoPendiente getTipoPendiente(GenericBeanOutRO genericBeanOutRO) {
        return new TipoPendiente(null, genericBeanOutRO.getValue(), genericBeanOutRO.getLabel());
    }

    public static TipoRechazada getTipoRechazada(GenericBeanOutRO genericBeanOutRO) {
        return new TipoRechazada(null, genericBeanOutRO.getValue(), genericBeanOutRO.getLabel());
    }

    public static GenericBeanOutRO getGenericBeanOutRO(AmbientePredio ambientePredio) {
        return new GenericBeanOutRO(ambientePredio.getValue(), ambientePredio.getLabel());
    }

    public static MHabilitacionOutRO getMHabilitacionOutRO(HabilitacionOutRO habilitacionOutRO) {
        MHabilitacionOutRO mHabilitacionOutRO = new MHabilitacionOutRO(
                habilitacionOutRO.getIdHabilitacion().longValue(),
                habilitacionOutRO.getIdSolicitud(),
                habilitacionOutRO.getCodigoSolicitud(),
                habilitacionOutRO.getDocumentoIdentificacionSolicitante(),
                habilitacionOutRO.getNombreSolicitante(),
                habilitacionOutRO.getDireccionUbigeoPredio(),
                habilitacionOutRO.getNumeroSuministroPredio(),
                habilitacionOutRO.getNumeroContratoConcesionariaPredio(),
                habilitacionOutRO.getEstadoHabilitacion(),
                habilitacionOutRO.getNombreEstadoHabilitacion(),
                habilitacionOutRO.getFechaUltimaModificacionHabilitacion(),
                habilitacionOutRO.getIdInspector(),
                habilitacionOutRO.getNumeroIntentoHabilitacion(),
                habilitacionOutRO.getFechaProgramadaHabilitacion(),
                habilitacionOutRO.getObservacionProgramacionHabilitacion(),
                habilitacionOutRO.getFechaRegistroHabilitacion(),
                habilitacionOutRO.getAprobadoHabilitacion(),
                habilitacionOutRO.getNombreAprobadoHabilitacion(),
                habilitacionOutRO.getAprobadoMedidor(),
                habilitacionOutRO.getNombreAprobadoMedidor(),
                habilitacionOutRO.getNombreFotoUbicacionMedidor(),
                habilitacionOutRO.getIdTipoAcometida(),
                habilitacionOutRO.getNombreTipoAcometida(),
                habilitacionOutRO.getAprobadoValvulaCorteGeneral(),
                habilitacionOutRO.getNombreAprobadoValvulaCorteGeneral(),
                habilitacionOutRO.getNombreFotoUbicacionValvulaCorteGeneral(),
                habilitacionOutRO.getDiametroValvulaCorteGeneral() == null ? "" : habilitacionOutRO.getDiametroValvulaCorteGeneral().toString(),
                habilitacionOutRO.getAprobadoTuberia(),
                habilitacionOutRO.getNombreAprobadoTuberia(),
                habilitacionOutRO.getNombreFoto1RecorridoTuberia(),
                habilitacionOutRO.getNombreFoto2RecorridoTuberia(),
                habilitacionOutRO.getNombreFoto3RecorridoTuberia(),
                habilitacionOutRO.getNombreFoto4RecorridoTuberia(),
                habilitacionOutRO.getTipoInstalacion(),
                habilitacionOutRO.getNombreTipoInstalacion(),
                habilitacionOutRO.getIdMaterialInstalacion(),
                habilitacionOutRO.getNombreMaterialInstalacion(),
                habilitacionOutRO.getLongitudTotalInstalacion() == null ? "" : habilitacionOutRO.getLongitudTotalInstalacion().toString(),
                habilitacionOutRO.getAprobadoPuntosInstalacion(),
                habilitacionOutRO.getNombreAprobadoPuntosInstalacion(),
                habilitacionOutRO.getNumeroPuntosInstalacion(),
                habilitacionOutRO.getAprobadoAmbientes(),
                habilitacionOutRO.getNombreAprobadoAmbientes(),
                habilitacionOutRO.getNumeroAmbientes(),
                habilitacionOutRO.getAprobadoPruebaMonoxido(),
                habilitacionOutRO.getNombreAprobadoPruebaMonoxido(),
                habilitacionOutRO.getAprobadoPresionArtefacto(),
                habilitacionOutRO.getNombreAprobadoPresionArtefacto(),
                habilitacionOutRO.getNombreFotoMedicionPresionManometro(),
                habilitacionOutRO.getValorPresionArtefacto() == null ? "" : habilitacionOutRO.getValorPresionArtefacto().toString(),
                habilitacionOutRO.getAprobadoHermeticidad(),
                habilitacionOutRO.getNombreAprobadoHermeticidad(),
                habilitacionOutRO.getNombreFotoMedicionHermeticidadManometro(),
                "",
                habilitacionOutRO.getPresionInicialPruebaHermeticidad() == null ? "" : habilitacionOutRO.getPresionInicialPruebaHermeticidad().toString(),
                habilitacionOutRO.getPresionFinalPruebaHermeticidad() == null ? "" : habilitacionOutRO.getPresionFinalPruebaHermeticidad().toString(),
                habilitacionOutRO.getTiempoPruebaHermeticidad() == null ? "" : habilitacionOutRO.getTiempoPruebaHermeticidad().toString(),
                habilitacionOutRO.getResultCode(),
                habilitacionOutRO.getMessage(),
                habilitacionOutRO.getErrorCode()

        );
        return mHabilitacionOutRO;
    }

    public static MSolicitudOutRO getMSolicitudOutRO(SolicitudOutRO solicitudOutRO) {
        MSolicitudOutRO mSolicitudOutRO = new MSolicitudOutRO(
                solicitudOutRO.getIdSolicitud().longValue(),
                solicitudOutRO.getCodigoSolicitud(),
                solicitudOutRO.getFechaSolicitud(),
                solicitudOutRO.getDocumentoIdentificacionSolicitante(),
                solicitudOutRO.getNombreSolicitante(),
                solicitudOutRO.getDireccionUbigeoPredio(),
                solicitudOutRO.getNumeroSuministroPredio(),
                solicitudOutRO.getNumeroContratoConcesionariaPredio(),
                solicitudOutRO.getIdEstadoSolicitud(),
                solicitudOutRO.getNombreEstadoSolicitud(),
                solicitudOutRO.getIdEmpresaInstaladora(),
                solicitudOutRO.getDocumentoIdentificacionEmpresaInstaladora(),
                solicitudOutRO.getNombreCortoEmpresaInstaladora(),
                solicitudOutRO.getNombreEmpresaInstaladora(),
                solicitudOutRO.getNombreDocumentoIdentificacionEmpresaInstaladora(),
                solicitudOutRO.getNumeroRegistroGnEmpresaInstaladora(),
                solicitudOutRO.getTelefonoSolicitante(),
                solicitudOutRO.getCelularSolicitante(),
                solicitudOutRO.getEmailSolicitante(),
                solicitudOutRO.getCoordenadasPredio(),
                solicitudOutRO.getCodigoUnidadPredialPredio(),
                solicitudOutRO.getCodigoManzanaPredio(),
                solicitudOutRO.getNombreEsUsuarioFiseSolicitud(),
                solicitudOutRO.getNombreAplicaMecanismoPromocionSolicitud(),
                solicitudOutRO.getNombreTipoProyectoInstalacion(),
                solicitudOutRO.getNombreArchivoDocumentacionProyectoNoTipicoSolicitud(),
                solicitudOutRO.getNombreTipoInstalacion(),
                solicitudOutRO.getNombreMaterialInstalacion(),
                solicitudOutRO.getNumeroPuntosInstalacion(),
                solicitudOutRO.getNombreEstratoPersona(),
                solicitudOutRO.getFechaAprobacionSolicitud(),
                solicitudOutRO.getFechaContratoConcesionariaPredio(),
                solicitudOutRO.getCodigoInternoContratoConcesionaria(),
                solicitudOutRO.getNumeroInstalacionConcesionaria(),
                solicitudOutRO.getNombreZonaAdjudicacion(),
                solicitudOutRO.getNombreEstadoTuberiaConexionAcometida(),
                solicitudOutRO.getNombreTipoGabinete(),
                solicitudOutRO.getNombreTipoAcometida(),
                solicitudOutRO.getNombreInstalacionAcometidaConcluidaPredio()
        );
        return mSolicitudOutRO;
    }


    public static BaseHabilitacionOutRO getBaseHabilitacionOutRO(MHabilitacionOutRO mHabilitacionOutRO) {
        BaseHabilitacionOutRO baseHabilitacionOutRO = new BaseHabilitacionOutRO();
        baseHabilitacionOutRO.setIdHabilitacion(mHabilitacionOutRO.getIdHabilitacion().intValue());
        baseHabilitacionOutRO.setIdSolicitud(mHabilitacionOutRO.getIdSolicitud());
        baseHabilitacionOutRO.setCodigoSolicitud(mHabilitacionOutRO.getCodigoSolicitud());
        baseHabilitacionOutRO.setDocumentoIdentificacionSolicitante(mHabilitacionOutRO.getDocumentoIdentificacionSolicitante());
        baseHabilitacionOutRO.setNombreSolicitante(mHabilitacionOutRO.getNombreSolicitante());
        baseHabilitacionOutRO.setDireccionUbigeoPredio(mHabilitacionOutRO.getDireccionUbigeoPredio());
        baseHabilitacionOutRO.setNumeroSuministroPredio(mHabilitacionOutRO.getNumeroSuministroPredio());
        baseHabilitacionOutRO.setNumeroContratoConcesionariaPredio(mHabilitacionOutRO.getNumeroContratoConcesionariaPredio());
        baseHabilitacionOutRO.setEstadoHabilitacion(mHabilitacionOutRO.getEstadoHabilitacion());
        baseHabilitacionOutRO.setNombreEstadoHabilitacion(mHabilitacionOutRO.getNombreEstadoHabilitacion());
        baseHabilitacionOutRO.setFechaUltimaModificacionHabilitacion(mHabilitacionOutRO.getFechaUltimaModificacionHabilitacion());
        baseHabilitacionOutRO.setResultCode(mHabilitacionOutRO.getResultCode() == null ? "" : mHabilitacionOutRO.getResultCode());
        baseHabilitacionOutRO.setMessage(mHabilitacionOutRO.getMessage() == null ? "" : mHabilitacionOutRO.getMessage());
        baseHabilitacionOutRO.setErrorCode(mHabilitacionOutRO.getErrorCode() == null ? "" : mHabilitacionOutRO.getErrorCode());
        return baseHabilitacionOutRO;
    }

    public static HabilitacionOutRO getHabilitacionOutRO(MHabilitacionOutRO mHabilitacionOutRO) {
        HabilitacionOutRO habilitacionOutRO = new HabilitacionOutRO();
        habilitacionOutRO.setIdHabilitacion(mHabilitacionOutRO.getIdHabilitacion().intValue());
        habilitacionOutRO.setIdSolicitud(mHabilitacionOutRO.getIdSolicitud());
        habilitacionOutRO.setCodigoSolicitud(mHabilitacionOutRO.getCodigoSolicitud());
        habilitacionOutRO.setDocumentoIdentificacionSolicitante(mHabilitacionOutRO.getDocumentoIdentificacionSolicitante());
        habilitacionOutRO.setNombreSolicitante(mHabilitacionOutRO.getNombreSolicitante());
        habilitacionOutRO.setDireccionUbigeoPredio(mHabilitacionOutRO.getDireccionUbigeoPredio());
        habilitacionOutRO.setNumeroSuministroPredio(mHabilitacionOutRO.getNumeroSuministroPredio());
        habilitacionOutRO.setNumeroContratoConcesionariaPredio(mHabilitacionOutRO.getNumeroContratoConcesionariaPredio());
        habilitacionOutRO.setEstadoHabilitacion(mHabilitacionOutRO.getEstadoHabilitacion());
        habilitacionOutRO.setNombreEstadoHabilitacion(mHabilitacionOutRO.getNombreEstadoHabilitacion());
        habilitacionOutRO.setFechaUltimaModificacionHabilitacion(mHabilitacionOutRO.getFechaUltimaModificacionHabilitacion());
        habilitacionOutRO.setIdInspector(mHabilitacionOutRO.getIdInspector());
        habilitacionOutRO.setNumeroIntentoHabilitacion(mHabilitacionOutRO.getNumeroIntentoHabilitacion());
        habilitacionOutRO.setFechaProgramadaHabilitacion(mHabilitacionOutRO.getFechaProgramadaHabilitacion());
        habilitacionOutRO.setObservacionProgramacionHabilitacion(mHabilitacionOutRO.getObservacionProgramacionHabilitacion());
        habilitacionOutRO.setFechaRegistroHabilitacion(mHabilitacionOutRO.getFechaRegistroHabilitacion());
        habilitacionOutRO.setAprobadoHabilitacion(mHabilitacionOutRO.getAprobadoHabilitacion());
        habilitacionOutRO.setNombreAprobadoHabilitacion(mHabilitacionOutRO.getNombreAprobadoHabilitacion());
        habilitacionOutRO.setAprobadoMedidor(mHabilitacionOutRO.getAprobadoMedidor());
        habilitacionOutRO.setNombreAprobadoMedidor(mHabilitacionOutRO.getNombreAprobadoMedidor());
        habilitacionOutRO.setNombreFotoUbicacionMedidor(mHabilitacionOutRO.getNombreFotoUbicacionMedidor());
        habilitacionOutRO.setIdTipoAcometida(mHabilitacionOutRO.getIdTipoAcometida());
        habilitacionOutRO.setNombreTipoAcometida(mHabilitacionOutRO.getNombreTipoAcometida());
        habilitacionOutRO.setAprobadoValvulaCorteGeneral(mHabilitacionOutRO.getAprobadoValvulaCorteGeneral());
        habilitacionOutRO.setNombreAprobadoValvulaCorteGeneral(mHabilitacionOutRO.getNombreAprobadoValvulaCorteGeneral());
        habilitacionOutRO.setNombreFotoUbicacionValvulaCorteGeneral(mHabilitacionOutRO.getNombreFotoUbicacionValvulaCorteGeneral());
        habilitacionOutRO.setDiametroValvulaCorteGeneral(mHabilitacionOutRO.getDiametroValvulaCorteGeneral().equalsIgnoreCase("") ? BigDecimal.ONE : BigDecimal.ONE);
        habilitacionOutRO.setAprobadoTuberia(mHabilitacionOutRO.getAprobadoTuberia());
        habilitacionOutRO.setNombreAprobadoTuberia(mHabilitacionOutRO.getNombreAprobadoTuberia());
        habilitacionOutRO.setNombreFoto1RecorridoTuberia(mHabilitacionOutRO.getNombreFoto1RecorridoTuberia());
        habilitacionOutRO.setNombreFoto2RecorridoTuberia(mHabilitacionOutRO.getNombreFoto2RecorridoTuberia());
        habilitacionOutRO.setNombreFoto3RecorridoTuberia(mHabilitacionOutRO.getNombreFoto3RecorridoTuberia());
        habilitacionOutRO.setNombreFoto4RecorridoTuberia(mHabilitacionOutRO.getNombreFoto4RecorridoTuberia());
        habilitacionOutRO.setTipoInstalacion(mHabilitacionOutRO.getTipoInstalacion());
        habilitacionOutRO.setNombreTipoInstalacion(mHabilitacionOutRO.getNombreTipoInstalacion());
        habilitacionOutRO.setIdMaterialInstalacion(mHabilitacionOutRO.getIdMaterialInstalacion());
        habilitacionOutRO.setNombreMaterialInstalacion(mHabilitacionOutRO.getNombreMaterialInstalacion());
        habilitacionOutRO.setLongitudTotalInstalacion(mHabilitacionOutRO.getLongitudTotalInstalacion().equalsIgnoreCase("") ? BigDecimal.ONE : BigDecimal.ONE);
        habilitacionOutRO.setAprobadoPuntosInstalacion(mHabilitacionOutRO.getAprobadoPuntosInstalacion());
        habilitacionOutRO.setNombreAprobadoPuntosInstalacion(mHabilitacionOutRO.getNombreAprobadoPuntosInstalacion());
        habilitacionOutRO.setNumeroPuntosInstalacion(mHabilitacionOutRO.getNumeroPuntosInstalacion());
        habilitacionOutRO.setAprobadoAmbientes(mHabilitacionOutRO.getAprobadoAmbientes());
        habilitacionOutRO.setNombreAprobadoAmbientes(mHabilitacionOutRO.getNombreAprobadoAmbientes());
        habilitacionOutRO.setNumeroAmbientes(mHabilitacionOutRO.getNumeroAmbientes());
        habilitacionOutRO.setAprobadoPruebaMonoxido(mHabilitacionOutRO.getAprobadoPruebaMonoxido());
        habilitacionOutRO.setNombreAprobadoPruebaMonoxido(mHabilitacionOutRO.getNombreAprobadoPruebaMonoxido());
        habilitacionOutRO.setAprobadoPresionArtefacto(mHabilitacionOutRO.getAprobadoPresionArtefacto());
        habilitacionOutRO.setNombreAprobadoPresionArtefacto(mHabilitacionOutRO.getNombreAprobadoPresionArtefacto());
        habilitacionOutRO.setNombreFotoMedicionPresionManometro(mHabilitacionOutRO.getNombreFotoMedicionPresionManometro());
        habilitacionOutRO.setValorPresionArtefacto(mHabilitacionOutRO.getValorPresionArtefacto().equalsIgnoreCase("") ? BigDecimal.ONE : BigDecimal.ONE);
        habilitacionOutRO.setAprobadoHermeticidad(mHabilitacionOutRO.getAprobadoHermeticidad());
        habilitacionOutRO.setNombreAprobadoHermeticidad(mHabilitacionOutRO.getNombreAprobadoHermeticidad());
        habilitacionOutRO.setNombreFotoMedicionHermeticidadManometro(mHabilitacionOutRO.getNombreFotoMedicionHermeticidadManometro());
        habilitacionOutRO.setPresionInicialPruebaHermeticidad(mHabilitacionOutRO.getPresionInicialPruebaHermeticidad().equalsIgnoreCase("") ? BigDecimal.ONE : BigDecimal.ONE);
        habilitacionOutRO.setPresionFinalPruebaHermeticidad(mHabilitacionOutRO.getPresionFinalPruebaHermeticidad().equalsIgnoreCase("") ? BigDecimal.ONE : BigDecimal.ONE);
        habilitacionOutRO.setTiempoPruebaHermeticidad(mHabilitacionOutRO.getTiempoPruebaHermeticidad().equalsIgnoreCase("") ? BigDecimal.ONE : BigDecimal.ONE);
        return habilitacionOutRO;
    }

    public static BaseInstalacionAcometidaOutRO getBaseInstalacionAcometidaOutRO(MInstalacionAcometidaOutRO mInstalacionAcometidaOutRO) {
        BaseInstalacionAcometidaOutRO baseInstalacionAcometidaOutRO = new BaseInstalacionAcometidaOutRO();
        baseInstalacionAcometidaOutRO.setIdInstalacionAcometida(mInstalacionAcometidaOutRO.getIdInstalacionAcometida().intValue());
        baseInstalacionAcometidaOutRO.setIdSolicitud(mInstalacionAcometidaOutRO.getIdSolicitud());
        baseInstalacionAcometidaOutRO.setCodigoSolicitud(mInstalacionAcometidaOutRO.getCodigoSolicitud());
        baseInstalacionAcometidaOutRO.setDocumentoIdentificacionSolicitante(mInstalacionAcometidaOutRO.getDocumentoIdentificacionSolicitante());
        baseInstalacionAcometidaOutRO.setNombreSolicitante(mInstalacionAcometidaOutRO.getNombreSolicitante());
        baseInstalacionAcometidaOutRO.setDireccionUbigeoPredio(mInstalacionAcometidaOutRO.getDireccionUbigeoPredio());
        baseInstalacionAcometidaOutRO.setNumeroSuministroPredio(mInstalacionAcometidaOutRO.getNumeroSuministroPredio());
        baseInstalacionAcometidaOutRO.setNumeroContratoConcesionariaPredio(mInstalacionAcometidaOutRO.getNumeroContratoConcesionariaPredio());
        baseInstalacionAcometidaOutRO.setEstadoInstalacionAcometida(mInstalacionAcometidaOutRO.getEstadoInstalacionAcometida());
        baseInstalacionAcometidaOutRO.setNombreEstadoInstalacionAcometida(mInstalacionAcometidaOutRO.getNombreEstadoInstalacionAcometida());
        baseInstalacionAcometidaOutRO.setFechaUltimaModificacionInstalacionAcometida(mInstalacionAcometidaOutRO.getFechaUltimaModificacionInstalacionAcometida());
        baseInstalacionAcometidaOutRO.setNombreTipoInstalacionAcometida(mInstalacionAcometidaOutRO.getNombreTipoInstalacionAcometida());
        baseInstalacionAcometidaOutRO.setTipoInstalacionAcometida(mInstalacionAcometidaOutRO.getTipoInstalacionAcometida());
        baseInstalacionAcometidaOutRO.setResultCode(mInstalacionAcometidaOutRO.getResultCode() == null ? "" : mInstalacionAcometidaOutRO.getResultCode());
        baseInstalacionAcometidaOutRO.setMessage(mInstalacionAcometidaOutRO.getMessage() == null ? "" : mInstalacionAcometidaOutRO.getMessage());
        baseInstalacionAcometidaOutRO.setErrorCode(mInstalacionAcometidaOutRO.getErrorCode() == null ? "" : mInstalacionAcometidaOutRO.getErrorCode());
        return baseInstalacionAcometidaOutRO;
    }

    public static InstalacionAcometidaOutRO getInstalacionAcometidaOutRO(MInstalacionAcometidaOutRO mInstalacionAcometidaOutRO) {
        InstalacionAcometidaOutRO instalacionAcometidaOutRO = new InstalacionAcometidaOutRO();
        instalacionAcometidaOutRO.setIdInstalacionAcometida(mInstalacionAcometidaOutRO.getIdInstalacionAcometida().intValue());
        instalacionAcometidaOutRO.setIdSolicitud(mInstalacionAcometidaOutRO.getIdSolicitud());
        instalacionAcometidaOutRO.setCodigoSolicitud(mInstalacionAcometidaOutRO.getCodigoSolicitud());
        instalacionAcometidaOutRO.setDocumentoIdentificacionSolicitante(mInstalacionAcometidaOutRO.getDocumentoIdentificacionSolicitante());
        instalacionAcometidaOutRO.setNombreSolicitante(mInstalacionAcometidaOutRO.getNombreSolicitante());
        instalacionAcometidaOutRO.setDireccionUbigeoPredio(mInstalacionAcometidaOutRO.getDireccionUbigeoPredio());
        instalacionAcometidaOutRO.setNumeroSuministroPredio(mInstalacionAcometidaOutRO.getNumeroSuministroPredio());
        instalacionAcometidaOutRO.setNumeroContratoConcesionariaPredio(mInstalacionAcometidaOutRO.getNumeroContratoConcesionariaPredio());
        instalacionAcometidaOutRO.setEstadoInstalacionAcometida(mInstalacionAcometidaOutRO.getEstadoInstalacionAcometida());
        instalacionAcometidaOutRO.setNombreEstadoInstalacionAcometida(mInstalacionAcometidaOutRO.getNombreEstadoInstalacionAcometida());
        instalacionAcometidaOutRO.setFechaUltimaModificacionInstalacionAcometida(mInstalacionAcometidaOutRO.getFechaUltimaModificacionInstalacionAcometida());
        instalacionAcometidaOutRO.setIdInspector(mInstalacionAcometidaOutRO.getIdInspector());
        instalacionAcometidaOutRO.setNumeroIntentoInstalacionAcometida(mInstalacionAcometidaOutRO.getNumeroIntentoInstalacionAcometida());
        instalacionAcometidaOutRO.setFechaProgramadaInstalacionAcometida(mInstalacionAcometidaOutRO.getFechaProgramadaInstalacionAcometida());
        instalacionAcometidaOutRO.setFechaRegistroInstalacionAcometida(mInstalacionAcometidaOutRO.getFechaRegistroInstalacionAcometida());
        instalacionAcometidaOutRO.setFechaFinalizacionInstalacionAcometida(mInstalacionAcometidaOutRO.getFechaFinalizacionInstalacionAcometida());
        instalacionAcometidaOutRO.setIdFusionistaRegistro(mInstalacionAcometidaOutRO.getIdFusionistaRegistro());
        instalacionAcometidaOutRO.setDocumentoIdentificacionFusionistaRegistro(mInstalacionAcometidaOutRO.getDocumentoIdentificacionFusionistaRegistro());
        instalacionAcometidaOutRO.setNombreFusionistaRegistro(mInstalacionAcometidaOutRO.getNombreFusionistaRegistro());
        instalacionAcometidaOutRO.setResultadoInstalacionAcometida(mInstalacionAcometidaOutRO.getResultadoInstalacionAcometida());
        instalacionAcometidaOutRO.setNombreResultadoInstalacionAcometida(mInstalacionAcometidaOutRO.getNombreResultadoInstalacionAcometida());
        instalacionAcometidaOutRO.setDiametroTuberiaConexionInstalacionAcometida(mInstalacionAcometidaOutRO.getDiametroTuberiaConexionInstalacionAcometida() == null ? BigDecimal.ZERO : BigDecimal.valueOf(Double.valueOf(mInstalacionAcometidaOutRO.getDiametroTuberiaConexionInstalacionAcometida())));
        instalacionAcometidaOutRO.setLongitudTuberiaConexionInstalacionAcometida(mInstalacionAcometidaOutRO.getLongitudTuberiaConexionInstalacionAcometida() == null ? BigDecimal.ZERO : BigDecimal.valueOf(Double.valueOf(mInstalacionAcometidaOutRO.getLongitudTuberiaConexionInstalacionAcometida())));
        instalacionAcometidaOutRO.setNombreMaterialInstalacion(mInstalacionAcometidaOutRO.getNombreMaterialInstalacion());
        instalacionAcometidaOutRO.setNombreTipoGabinete(mInstalacionAcometidaOutRO.getNombreTipoGabinete());
        instalacionAcometidaOutRO.setNombreFotoActaInstalacionTuberiaConexionAcometida(mInstalacionAcometidaOutRO.getNombreFotoActaInstalacionTuberiaConexionAcometida());
        instalacionAcometidaOutRO.setNombreFotoTuberiaConexionTerminada(mInstalacionAcometidaOutRO.getNombreFotoTuberiaConexionTerminada());
        instalacionAcometidaOutRO.setNombreTipoInstalacionAcometida(mInstalacionAcometidaOutRO.getNombreTipoInstalacionAcometida());
        instalacionAcometidaOutRO.setTipoInstalacionAcometida(mInstalacionAcometidaOutRO.getTipoInstalacionAcometida());
        return instalacionAcometidaOutRO;
    }

    public static SolicitudOutRO getSolicitudOutRO(MSolicitudOutRO mSolicitudOutRO) {
        SolicitudOutRO solicitudOutRO = new SolicitudOutRO();
        solicitudOutRO.setIdEmpresaInstaladora(mSolicitudOutRO.getIdEmpresaInstaladora());
        solicitudOutRO.setDocumentoIdentificacionEmpresaInstaladora(mSolicitudOutRO.getDocumentoIdentificacionEmpresaInstaladora());
        solicitudOutRO.setNombreCortoEmpresaInstaladora(mSolicitudOutRO.getNombreCortoEmpresaInstaladora());
        solicitudOutRO.setNombreEmpresaInstaladora(mSolicitudOutRO.getNombreEmpresaInstaladora());
        solicitudOutRO.setNombreDocumentoIdentificacionEmpresaInstaladora(mSolicitudOutRO.getNombreDocumentoIdentificacionEmpresaInstaladora());
        solicitudOutRO.setNumeroRegistroGnEmpresaInstaladora(mSolicitudOutRO.getNumeroRegistroGnEmpresaInstaladora());
        solicitudOutRO.setTelefonoSolicitante(mSolicitudOutRO.getTelefonoSolicitante());
        solicitudOutRO.setCelularSolicitante(mSolicitudOutRO.getCelularSolicitante());
        solicitudOutRO.setEmailSolicitante(mSolicitudOutRO.getEmailSolicitante());
        solicitudOutRO.setCoordenadasPredio(mSolicitudOutRO.getCoordenadasPredio());
        solicitudOutRO.setCodigoUnidadPredialPredio(mSolicitudOutRO.getCodigoUnidadPredialPredio());
        solicitudOutRO.setCodigoManzanaPredio(mSolicitudOutRO.getCodigoManzanaPredio());
        solicitudOutRO.setNombreEsUsuarioFiseSolicitud(mSolicitudOutRO.getNombreEsUsuarioFiseSolicitud());
        solicitudOutRO.setNombreAplicaMecanismoPromocionSolicitud(mSolicitudOutRO.getNombreAplicaMecanismoPromocionSolicitud());
        solicitudOutRO.setNombreTipoProyectoInstalacion(mSolicitudOutRO.getNombreTipoProyectoInstalacion());
        solicitudOutRO.setNombreArchivoDocumentacionProyectoNoTipicoSolicitud(mSolicitudOutRO.getNombreArchivoDocumentacionProyectoNoTipicoSolici());
        solicitudOutRO.setNombreTipoInstalacion(mSolicitudOutRO.getNombreTipoInstalacion());
        solicitudOutRO.setNombreMaterialInstalacion(mSolicitudOutRO.getNombreMaterialInstalacion());
        solicitudOutRO.setNumeroPuntosInstalacion(mSolicitudOutRO.getNumeroPuntosInstalacion());
        solicitudOutRO.setNombreEstratoPersona(mSolicitudOutRO.getNombreEstratoPersona());
        solicitudOutRO.setFechaAprobacionSolicitud(mSolicitudOutRO.getFechaAprobacionSolicitud());
        solicitudOutRO.setFechaContratoConcesionariaPredio(mSolicitudOutRO.getFechaContratoConcesionariaPredio());
        solicitudOutRO.setCodigoInternoContratoConcesionaria(mSolicitudOutRO.getCodigoInternoContratoConcesionaria());
        solicitudOutRO.setNumeroInstalacionConcesionaria(mSolicitudOutRO.getNumeroInstalacionConcesionaria());
        solicitudOutRO.setNombreZonaAdjudicacion(mSolicitudOutRO.getNombreZonaAdjudicacion());
        solicitudOutRO.setNombreEstadoTuberiaConexionAcometida(mSolicitudOutRO.getNombreEstadoTuberiaConexionAcometida());
        solicitudOutRO.setNombreTipoGabinete(mSolicitudOutRO.getNombreTipoGabinete());
        solicitudOutRO.setNombreTipoAcometida(mSolicitudOutRO.getNombreTipoAcometida());
        solicitudOutRO.setNombreInstalacionAcometidaConcluidaPredio(mSolicitudOutRO.getNombreInstalacionAcometidaConcluidaPredio());
        return solicitudOutRO;
    }

    public static MMotivoRechazoHabilitacionOutRO getMMotivoRechazoHabilitacionOutRO(MotivoRechazoHabilitacionOutRO motivoRechazoHabilitacion) {
        MMotivoRechazoHabilitacionOutRO mMotivoRechazoHabilitacionOutRO = new MMotivoRechazoHabilitacionOutRO();
        mMotivoRechazoHabilitacionOutRO.setIdMotivoRechazoHabilitacion(motivoRechazoHabilitacion.getIdMotivoRechazoHabilitacion().longValue());
        mMotivoRechazoHabilitacionOutRO.setIdMotivoRechazoHabilitacionPadre(motivoRechazoHabilitacion.getIdMotivoRechazoHabilitacionPadre());
        mMotivoRechazoHabilitacionOutRO.setNombreMotivoRechazoHabilitacion(motivoRechazoHabilitacion.getNombreMotivoRechazoHabilitacion());
        return mMotivoRechazoHabilitacionOutRO;
    }

    public static MotivoRechazoHabilitacionOutRO getMotivoRechazoHabilitacionOutRO(MMotivoRechazoHabilitacionOutRO mMotivoRechazoHabilitacionOutRO) {
        MotivoRechazoHabilitacionOutRO motivoRechazoHabilitacionOutRO = new MotivoRechazoHabilitacionOutRO();
        motivoRechazoHabilitacionOutRO.setIdMotivoRechazoHabilitacion(mMotivoRechazoHabilitacionOutRO.getIdMotivoRechazoHabilitacion().intValue());
        motivoRechazoHabilitacionOutRO.setIdMotivoRechazoHabilitacionPadre(mMotivoRechazoHabilitacionOutRO.getIdMotivoRechazoHabilitacionPadre());
        motivoRechazoHabilitacionOutRO.setNombreMotivoRechazoHabilitacion(mMotivoRechazoHabilitacionOutRO.getNombreMotivoRechazoHabilitacion());
        return motivoRechazoHabilitacionOutRO;
    }

    public static MMotivoRechazoHabilitacionMontanteOutRO getMMotivoRechazoHabilitacionMontanteOutRO(MotivoRechazoHabilitacionMontanteOutRO motivoRechazoHabilitacionMontanteOutRO) {
        MMotivoRechazoHabilitacionMontanteOutRO mMotivoRechazoHabilitacionMontanteOutRO = new MMotivoRechazoHabilitacionMontanteOutRO();
        mMotivoRechazoHabilitacionMontanteOutRO.setIdMotivoRechazoHabilitacionMontante(motivoRechazoHabilitacionMontanteOutRO.getIdMotivoRechazoHabilitacionMontante().longValue());
        mMotivoRechazoHabilitacionMontanteOutRO.setIdMotivoRechazoHabilitacionMontantePadre(motivoRechazoHabilitacionMontanteOutRO.getIdMotivoRechazoHabilitacionMontantePadre());
        mMotivoRechazoHabilitacionMontanteOutRO.setNombreMotivoRechazoHabilitacionMontante(motivoRechazoHabilitacionMontanteOutRO.getNombreMotivoRechazoHabilitacionMontante());
        return mMotivoRechazoHabilitacionMontanteOutRO;
    }

    public static MotivoRechazoHabilitacionMontanteOutRO getMotivoRechazoHabilitacionMontanteOutRO(MMotivoRechazoHabilitacionMontanteOutRO mMotivoRechazoHabilitacionMontanteOutRO) {
        MotivoRechazoHabilitacionMontanteOutRO motivoRechazoHabilitacionMontanteOutRO = new MotivoRechazoHabilitacionMontanteOutRO();
        motivoRechazoHabilitacionMontanteOutRO.setIdMotivoRechazoHabilitacionMontante(mMotivoRechazoHabilitacionMontanteOutRO.getIdMotivoRechazoHabilitacionMontante().intValue());
        motivoRechazoHabilitacionMontanteOutRO.setIdMotivoRechazoHabilitacionMontantePadre(mMotivoRechazoHabilitacionMontanteOutRO.getIdMotivoRechazoHabilitacionMontantePadre());
        motivoRechazoHabilitacionMontanteOutRO.setNombreMotivoRechazoHabilitacionMontante(mMotivoRechazoHabilitacionMontanteOutRO.getNombreMotivoRechazoHabilitacionMontante());
        return motivoRechazoHabilitacionMontanteOutRO;
    }

    public static HabilitacionMontanteOutRO getHabilitacionMontanteOutRO(MHabilitacionMontanteOutRO obj) {
        HabilitacionMontanteOutRO newObj = new HabilitacionMontanteOutRO();
        newObj.setIdHabilitacionMontante(obj.getIdHabilitacionMontante().intValue());
        newObj.setIdProyectoInstalacion(obj.getIdProyectoInstalacion());
        newObj.setCodigoObjetoConexionProyectoInstalacion(obj.getCodigoObjetoConexionProyectoInstalacion());
        newObj.setCodigoUnidadPredialProyectoInstalacion(obj.getCodigoUnidadPredialProyectoInstalacion());
        newObj.setNombreProyectoInstalacion(obj.getNombreProyectoInstalacion());
        newObj.setNombreMontanteProyectoInstalacion(obj.getNombreMontanteProyectoInstalacion());
        newObj.setDireccionUbigeoProyectoInstalacion(obj.getDireccionUbigeoProyectoInstalacion());
        newObj.setEstadoHabilitacionMontante(obj.getEstadoHabilitacionMontante());
        newObj.setNombreEstadoHabilitacionMontante(obj.getNombreEstadoHabilitacionMontante());
        newObj.setFechaUltimaModificacionHabilitacionMontante(obj.getFechaUltimaModificacionHabilitacionMontante());
        newObj.setIdInspector(obj.getIdInspector());
        newObj.setNumeroIntentoHabilitacionMontante(obj.getNumeroIntentoHabilitacionMontante());
        newObj.setFechaProgramadaHabilitacionMontante(obj.getFechaProgramadaHabilitacionMontante());
        newObj.setObservacionProgramacionHabilitacionMontante(obj.getObservacionProgramacionHabilitacionMontante());
        newObj.setFechaRegistroHabilitacionMontante(obj.getFechaRegistroHabilitacionMontante());
        newObj.setAprobadoHabilitacionMontante(obj.getAprobadoHabilitacionMontante());
        newObj.setNombreAprobadoHabilitacionMontante(obj.getNombreAprobadoHabilitacionMontante());
        newObj.setAprobadaAcometida(obj.getAprobadaAcometida());
        newObj.setNombreAprobadaAcometida(obj.getNombreAprobadaAcometida());
        newObj.setNombreFotoAcometida(obj.getNombreFotoAcometida());
        newObj.setAprobadoRecorridoTuberia(obj.getAprobadoRecorridoTuberia());
        newObj.setNombreAprobadoRecorridoTuberia(obj.getNombreAprobadoRecorridoTuberia());
        newObj.setNombreFoto1RecorridoTuberia(obj.getNombreFoto1RecorridoTuberia());
        newObj.setNombreFoto2RecorridoTuberia(obj.getNombreFoto2RecorridoTuberia());
        newObj.setNombreFoto3RecorridoTuberia(obj.getNombreFoto3RecorridoTuberia());
        newObj.setAprobadoHermeticidad(obj.getAprobadoHermeticidad());
        newObj.setNombreAprobadoHermeticidad(obj.getNombreAprobadoHermeticidad());
        newObj.setNombreFotoMedicionHermeticidadManometro(obj.getNombreFotoMedicionHermeticidadManometro());
        newObj.setPresionOperacionHermeticidad(BigDecimal.valueOf(obj.getPresionOperacionHermeticidad()));
        newObj.setPresionTuberiaHermeticidad(BigDecimal.valueOf(obj.getPresionTuberiaHermeticidad()));
        newObj.setAprobadaInstalacion(obj.getAprobadaInstalacion());
        newObj.setNombreAprobadaInstalacion(obj.getNombreAprobadaInstalacion());
        newObj.setTipoInstalacion(obj.getTipoInstalacion());
        newObj.setNombreTipoInstalacion(obj.getNombreTipoInstalacion());
        newObj.setIdMaterialInstalacion(obj.getIdMaterialInstalacion());
        newObj.setNombreMaterialInstalacion(obj.getNombreMaterialInstalacion());
        newObj.setLongitudTotalInstalacion(BigDecimal.valueOf(obj.getLongitudTotalInstalacion()));
        newObj.setDiametroTuberia(BigDecimal.valueOf(obj.getDiametroTuberia()));
        newObj.setIdHabilitacionMontante(obj.getIdHabilitacionMontante().intValue());
        newObj.setIdProyectoInstalacion(obj.getIdProyectoInstalacion());
        newObj.setCodigoObjetoConexionProyectoInstalacion(obj.getCodigoObjetoConexionProyectoInstalacion());
        newObj.setCodigoUnidadPredialProyectoInstalacion(obj.getCodigoUnidadPredialProyectoInstalacion());
        newObj.setDireccionUbigeoProyectoInstalacion(obj.getDireccionUbigeoProyectoInstalacion());
        newObj.setEstadoHabilitacionMontante(obj.getEstadoHabilitacionMontante());
        newObj.setNombreEstadoHabilitacionMontante(obj.getNombreEstadoHabilitacionMontante());
        newObj.setFechaUltimaModificacionHabilitacionMontante(obj.getFechaUltimaModificacionHabilitacionMontante());
        newObj.setResultCode(obj.getResultCode());
        newObj.setMessage(obj.getMessage());
        newObj.setErrorCode(obj.getErrorCode());
        return newObj;
    }

    public static ProyectoInstalacionOutRO getProyectoInstalacionOutRO(MProyectoInstalacionOutRO obj) {
        ProyectoInstalacionOutRO newObj = new ProyectoInstalacionOutRO();
        newObj.setIdProyectoInstalacion(obj.getIdProyectoInstalacion().intValue());
        newObj.setCodigoObjetoConexionProyectoInstalacion(obj.getCodigoObjetoConexionProyectoInstalacion());
        newObj.setCodigoUnidadPredialProyectoInstalacion(obj.getCodigoUnidadPredialProyectoInstalacion());
        newObj.setNombreProyectoInstalacion(obj.getNombreProyectoInstalacion());
        newObj.setFechaRegistroPortalProyectoInstalacion(obj.getFechaRegistroPortalProyectoInstalacion());
        newObj.setDireccionUbigeoProyectoInstalacion(obj.getDireccionUbigeoProyectoInstalacion());
        newObj.setIdEstadoProyectoInstalacion(obj.getIdEstadoProyectoInstalacion());
        newObj.setNombreEstadoProyectoInstalacion(obj.getNombreEstadoProyectoInstalacion());
        newObj.setIdEmpresaInstaladora(obj.getIdEmpresaInstaladora());
        newObj.setDocumentoIdentificacionEmpresaInstaladora(obj.getDocumentoIdentificacionEmpresaInstaladora());
        newObj.setNombreCortoEmpresaInstaladora(obj.getNombreCortoEmpresaInstaladora());
        newObj.setNombreEmpresaInstaladora(obj.getNombreEmpresaInstaladora());
        newObj.setNombreDocumentoIdentificacionEmpresaInstaladora(obj.getNombreDocumentoIdentificacionEmpresaInstaladora());
        newObj.setNumeroRegistroGnEmpresaInstaladora(obj.getNumeroRegistroGnEmpresaInstaladora());
        newObj.setCoordenadasProyectoInstalacion(obj.getCoordenadasProyectoInstalacion());
        newObj.setNombreTipoProyectoInstalacion(obj.getNombreTipoProyectoInstalacion());
        newObj.setNombreEsProyectoFiseProyectoInstalacion(obj.getNombreEsProyectoFiseProyectoInstalacion());
        newObj.setNombreAplicaMecanismoPromocionProyectoInstalacion(obj.getNombreAplicaMecanismoPromocionProyectoInstalacion());
        newObj.setNombreEsZonaGasificadaProyectoInstalacion(obj.getNombreEsZonaGasificadaProyectoInstalacion());
        newObj.setNombreTienePlanoInstalacionInternaProyectoInstalacion(obj.getNombreTienePlanoInstalacionInternaProyectoInstalacion());
        newObj.setNombreTieneEspecificacionTecnicaProyectoInstalacion(obj.getNombreTieneEspecificacionTecnicaProyectoInstalacion());
        newObj.setNombreTieneMemoriaCalculosProyectoInstalacion(obj.getNombreTieneMemoriaCalculosProyectoInstalacion());
        newObj.setResultCode(obj.getResultCode());
        newObj.setMessage(obj.getMessage());
        newObj.setErrorCode(obj.getErrorCode());
        return newObj;
    }

    public static BaseHabilitacionMontanteOutRO getBaseHabilitacionMontanteOutRO(MHabilitacionMontanteOutRO obj) {
        BaseHabilitacionMontanteOutRO newObj = new BaseHabilitacionMontanteOutRO();
        newObj.setIdHabilitacionMontante(obj.getIdHabilitacionMontante().intValue());
        newObj.setIdProyectoInstalacion(obj.getIdProyectoInstalacion());
        newObj.setCodigoObjetoConexionProyectoInstalacion(obj.getCodigoObjetoConexionProyectoInstalacion());
        newObj.setCodigoUnidadPredialProyectoInstalacion(obj.getCodigoUnidadPredialProyectoInstalacion());
        newObj.setNombreProyectoInstalacion(obj.getNombreProyectoInstalacion());
        newObj.setNombreMontanteProyectoInstalacion(obj.getNombreMontanteProyectoInstalacion());
        newObj.setDireccionUbigeoProyectoInstalacion(obj.getDireccionUbigeoProyectoInstalacion());
        newObj.setEstadoHabilitacionMontante(obj.getEstadoHabilitacionMontante());
        newObj.setNombreEstadoHabilitacionMontante(obj.getNombreEstadoHabilitacionMontante());
        newObj.setFechaUltimaModificacionHabilitacionMontante(obj.getFechaUltimaModificacionHabilitacionMontante());
        newObj.setResultCode(obj.getResultCode());
        newObj.setMessage(obj.getMessage());
        newObj.setErrorCode(obj.getErrorCode());
        return newObj;
    }

    public static MProyectoInstalacionOutRO getMProyectoInstalacionOutRO(ProyectoInstalacionOutRO obj) {
        MProyectoInstalacionOutRO newObj = new MProyectoInstalacionOutRO();
        newObj.setIdProyectoInstalacion(obj.getIdProyectoInstalacion().longValue());
        newObj.setCodigoObjetoConexionProyectoInstalacion(obj.getCodigoObjetoConexionProyectoInstalacion());
        newObj.setCodigoUnidadPredialProyectoInstalacion(obj.getCodigoUnidadPredialProyectoInstalacion());
        newObj.setNombreProyectoInstalacion(obj.getNombreProyectoInstalacion());
        newObj.setFechaRegistroPortalProyectoInstalacion(obj.getFechaRegistroPortalProyectoInstalacion());
        newObj.setDireccionUbigeoProyectoInstalacion(obj.getDireccionUbigeoProyectoInstalacion());
        newObj.setIdEstadoProyectoInstalacion(obj.getIdEstadoProyectoInstalacion());
        newObj.setNombreEstadoProyectoInstalacion(obj.getNombreEstadoProyectoInstalacion());
        newObj.setIdEmpresaInstaladora(obj.getIdEmpresaInstaladora());
        newObj.setDocumentoIdentificacionEmpresaInstaladora(obj.getDocumentoIdentificacionEmpresaInstaladora());
        newObj.setNombreCortoEmpresaInstaladora(obj.getNombreCortoEmpresaInstaladora());
        newObj.setNombreEmpresaInstaladora(obj.getNombreEmpresaInstaladora());
        newObj.setNombreDocumentoIdentificacionEmpresaInstaladora(obj.getNombreDocumentoIdentificacionEmpresaInstaladora());
        newObj.setNumeroRegistroGnEmpresaInstaladora(obj.getNumeroRegistroGnEmpresaInstaladora());
        newObj.setCoordenadasProyectoInstalacion(obj.getCoordenadasProyectoInstalacion());
        newObj.setNombreTipoProyectoInstalacion(obj.getNombreTipoProyectoInstalacion());
        newObj.setNombreEsProyectoFiseProyectoInstalacion(obj.getNombreEsProyectoFiseProyectoInstalacion());
        newObj.setNombreAplicaMecanismoPromocionProyectoInstalacion(obj.getNombreAplicaMecanismoPromocionProyectoInstalacion());
        newObj.setNombreEsZonaGasificadaProyectoInstalacion(obj.getNombreEsZonaGasificadaProyectoInstalacion());
        newObj.setNombreTienePlanoInstalacionInternaProyectoInstalacion(obj.getNombreTienePlanoInstalacionInternaProyectoInstalacion());
        newObj.setNombreTieneEspecificacionTecnicaProyectoInstalacion(obj.getNombreTieneEspecificacionTecnicaProyectoInstalacion());
        newObj.setNombreTieneMemoriaCalculosProyectoInstalacion(obj.getNombreTieneMemoriaCalculosProyectoInstalacion());
        newObj.setResultCode(obj.getResultCode());
        newObj.setMessage(obj.getMessage());
        newObj.setErrorCode(obj.getErrorCode());
        return newObj;
    }

    public static MHabilitacionMontanteOutRO getMHabilitacionMontanteOutRO(HabilitacionMontanteOutRO obj) {
        MHabilitacionMontanteOutRO newObj = new MHabilitacionMontanteOutRO();
        newObj.setIdHabilitacionMontante(obj.getIdHabilitacionMontante().longValue());
        newObj.setIdProyectoInstalacion(obj.getIdProyectoInstalacion());
        newObj.setCodigoObjetoConexionProyectoInstalacion(obj.getCodigoObjetoConexionProyectoInstalacion());
        newObj.setCodigoUnidadPredialProyectoInstalacion(obj.getCodigoUnidadPredialProyectoInstalacion());
        newObj.setNombreProyectoInstalacion(obj.getNombreProyectoInstalacion());
        newObj.setNombreMontanteProyectoInstalacion(obj.getNombreMontanteProyectoInstalacion());
        newObj.setDireccionUbigeoProyectoInstalacion(obj.getDireccionUbigeoProyectoInstalacion());
        newObj.setEstadoHabilitacionMontante(obj.getEstadoHabilitacionMontante());
        newObj.setNombreEstadoHabilitacionMontante(obj.getNombreEstadoHabilitacionMontante());
        newObj.setFechaUltimaModificacionHabilitacionMontante(obj.getFechaUltimaModificacionHabilitacionMontante());
        newObj.setIdInspector(obj.getIdInspector());
        newObj.setNumeroIntentoHabilitacionMontante(obj.getNumeroIntentoHabilitacionMontante());
        newObj.setFechaProgramadaHabilitacionMontante(obj.getFechaProgramadaHabilitacionMontante());
        newObj.setObservacionProgramacionHabilitacionMontante(obj.getObservacionProgramacionHabilitacionMontante());
        newObj.setFechaRegistroHabilitacionMontante(obj.getFechaRegistroHabilitacionMontante());
        newObj.setAprobadoHabilitacionMontante(obj.getAprobadoHabilitacionMontante());
        newObj.setNombreAprobadoHabilitacionMontante(obj.getNombreAprobadoHabilitacionMontante());
        newObj.setAprobadaAcometida(obj.getAprobadaAcometida());
        newObj.setNombreAprobadaAcometida(obj.getNombreAprobadaAcometida());
        newObj.setNombreFotoAcometida(obj.getNombreFotoAcometida());
        newObj.setAprobadoRecorridoTuberia(obj.getAprobadoRecorridoTuberia());
        newObj.setNombreAprobadoRecorridoTuberia(obj.getNombreAprobadoRecorridoTuberia());
        newObj.setNombreFoto1RecorridoTuberia(obj.getNombreFoto1RecorridoTuberia());
        newObj.setNombreFoto2RecorridoTuberia(obj.getNombreFoto2RecorridoTuberia());
        newObj.setNombreFoto3RecorridoTuberia(obj.getNombreFoto3RecorridoTuberia());
        newObj.setAprobadoHermeticidad(obj.getAprobadoHermeticidad());
        newObj.setNombreAprobadoHermeticidad(obj.getNombreAprobadoHermeticidad());
        newObj.setNombreFotoMedicionHermeticidadManometro(obj.getNombreFotoMedicionHermeticidadManometro());
        newObj.setPresionOperacionHermeticidad(obj.getPresionOperacionHermeticidad() == null ? 0.0 : obj.getPresionOperacionHermeticidad().doubleValue());
        newObj.setPresionTuberiaHermeticidad(obj.getPresionTuberiaHermeticidad() == null ? 0.0 : obj.getPresionTuberiaHermeticidad().doubleValue());
        newObj.setAprobadaInstalacion(obj.getAprobadaInstalacion());
        newObj.setNombreAprobadaInstalacion(obj.getNombreAprobadaInstalacion());
        newObj.setTipoInstalacion(obj.getTipoInstalacion());
        newObj.setNombreTipoInstalacion(obj.getNombreTipoInstalacion());
        newObj.setIdMaterialInstalacion(obj.getIdMaterialInstalacion());
        newObj.setNombreMaterialInstalacion(obj.getNombreMaterialInstalacion());
        newObj.setLongitudTotalInstalacion(obj.getLongitudTotalInstalacion() == null ? 0.0 : obj.getLongitudTotalInstalacion().doubleValue());
        newObj.setDiametroTuberia(obj.getDiametroTuberia() == null ? 0.0 : obj.getDiametroTuberia().doubleValue());
        newObj.setResultCode(obj.getResultCode());
        newObj.setMessage(obj.getMessage());
        newObj.setErrorCode(obj.getErrorCode());
        return newObj;
    }

    public static MInstalacionAcometidaOutRO getMInstalacionAcometidaOutRO(InstalacionAcometidaOutRO obj) {
        return new MInstalacionAcometidaOutRO(
                obj.getIdInstalacionAcometida().longValue(),
                obj.getIdSolicitud(),
                obj.getCodigoSolicitud(),
                obj.getDocumentoIdentificacionSolicitante(),
                obj.getNombreSolicitante(),
                obj.getDireccionUbigeoPredio(),
                obj.getNumeroSuministroPredio(),
                obj.getNumeroContratoConcesionariaPredio(),
                obj.getEstadoInstalacionAcometida(),
                obj.getNombreEstadoInstalacionAcometida(),
                obj.getFechaUltimaModificacionInstalacionAcometida(),
                obj.getIdInspector(),
                obj.getNumeroIntentoInstalacionAcometida(),
                obj.getFechaProgramadaInstalacionAcometida(),
                obj.getFechaRegistroInstalacionAcometida(),
                obj.getFechaFinalizacionInstalacionAcometida(),
                obj.getIdFusionistaRegistro(),
                obj.getDocumentoIdentificacionFusionistaRegistro(),
                obj.getNombreFusionistaRegistro(),
                obj.getResultadoInstalacionAcometida(),
                obj.getNombreResultadoInstalacionAcometida(),
                obj.getNombreTipoGabinete(),
                obj.getNombreTipoGabinete(),
                obj.getDiametroTuberiaConexionInstalacionAcometida() == null ? null : obj.getDiametroTuberiaConexionInstalacionAcometida().toString(),
                obj.getLongitudTuberiaConexionInstalacionAcometida() == null ? null : obj.getLongitudTuberiaConexionInstalacionAcometida().toString(),
                obj.getNombreMaterialInstalacion(),
                obj.getNombreFotoActaInstalacionTuberiaConexionAcometida(),
                obj.getNombreFotoTuberiaConexionTerminada(),
                obj.getNombreFotoGabineteManifoldTerminado(),
                obj.getNombreTipoInstalacionAcometida(),
                obj.getTipoInstalacionAcometida(),
                obj.getResultCode(),
                obj.getMessage(),
                obj.getErrorCode()
        );
    }

    public static Fusionista getFusionista(BaseInspectorOutRO baseInspectorOutRO) {
        return new Fusionista(null,
                baseInspectorOutRO.getIdInspector(),
                baseInspectorOutRO.getNombreInspector(),
                baseInspectorOutRO.getNombreDocumentoIdentificacionInspector());
    }

    public static BaseInspectorOutRO getBaseInspectorOutRO(Fusionista fusionista) {
        BaseInspectorOutRO baseInspectorOutRO = new BaseInspectorOutRO();
        baseInspectorOutRO.setIdInspector(fusionista.getIdInspector());
        baseInspectorOutRO.setNombreInspector(fusionista.getNombreInspector());
        baseInspectorOutRO.setNombreDocumentoIdentificacionInspector(fusionista.getNombreDocumentoIdentificacionInspector());
        return baseInspectorOutRO;
    }

    public static GenericBeanOutRO getGenericBeanOutRO(TipoResultado tipoResultado) {
        return new GenericBeanOutRO(tipoResultado.getValue(), tipoResultado.getLabel());
    }

    public static GenericBeanOutRO getGenericBeanOutRO(TipoPendiente tipoPendiente) {
        return new GenericBeanOutRO(tipoPendiente.getValue(), tipoPendiente.getLabel());
    }

    public static GenericBeanOutRO getGenericBeanOutRO(TipoRechazada tipoRechazada) {
        return new GenericBeanOutRO(tipoRechazada.getValue(), tipoRechazada.getLabel());
    }
}
