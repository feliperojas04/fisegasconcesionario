package pe.gob.osinergmin.gnr.cgn.task;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import gob.osinergmin.gnr.domain.dto.rest.in.BaseInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.filter.FilterGabineteProyectoInstalacionByProyInsInRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListGabineteProyectoInstalacionOutRo;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.util.Urls;

public class TipoGabineteAcometida2 extends AsyncTask<Config, Void, ListGabineteProyectoInstalacionOutRo> {

    private OnTipoGabineteAcometida2 listener = null;

    public TipoGabineteAcometida2(OnTipoGabineteAcometida2 listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected ListGabineteProyectoInstalacionOutRo doInBackground(Config... configs) {
        try {
            Config config = configs[0];

            String url = Urls.getBase() + Urls.getListaTipoGabineteporIdProyectoInstalacion();

            FilterGabineteProyectoInstalacionByProyInsInRO baseInRO = new FilterGabineteProyectoInstalacionByProyInsInRO();
            baseInRO.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);
            baseInRO.setIdProyectoInstalacion(Integer.parseInt(config.getIdProyectoInstalacion()));

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Authorization", "Bearer " + config.getToken());

            HttpEntity<BaseInRO> httpEntity = new HttpEntity<>(baseInRO, httpHeaders);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            return restTemplate.postForObject(url, httpEntity, ListGabineteProyectoInstalacionOutRo.class);

        } catch (RestClientException e) {
            Log.e("TIPOGABINETE", e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(ListGabineteProyectoInstalacionOutRo listGabineteProyectoInstalacionOutRo) {
        super.onPostExecute(listGabineteProyectoInstalacionOutRo);
        listener.OnTipoGabineteAcometida2(listGabineteProyectoInstalacionOutRo);
    }

    public interface OnTipoGabineteAcometida2 {
        void OnTipoGabineteAcometida2(ListGabineteProyectoInstalacionOutRo listGabineteProyectoInstalacionOutRo);
    }
}
