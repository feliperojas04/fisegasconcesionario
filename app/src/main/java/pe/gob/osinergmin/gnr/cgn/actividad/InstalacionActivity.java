package pe.gob.osinergmin.gnr.cgn.actividad;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.GenericBeanOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListGenericBeanOutRO;
import gob.osinergmin.gnr.util.Constantes;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionMontanteOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionMontanteOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MaterialInstalacion;
import pe.gob.osinergmin.gnr.cgn.data.models.MaterialInstalacionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Montante;
import pe.gob.osinergmin.gnr.cgn.data.models.MontanteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Precision;
import pe.gob.osinergmin.gnr.cgn.data.models.PrecisionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.TiposInstalacionMontante;
import pe.gob.osinergmin.gnr.cgn.data.models.TiposInstalacionMontanteDao;
import pe.gob.osinergmin.gnr.cgn.task.MontanteRx;
import pe.gob.osinergmin.gnr.cgn.task.TipoMaterialInstalacion;
import pe.gob.osinergmin.gnr.cgn.util.DownTimer;
import pe.gob.osinergmin.gnr.cgn.util.FormatoFecha;
import pe.gob.osinergmin.gnr.cgn.util.NetworkUtil;
import pe.gob.osinergmin.gnr.cgn.util.Parse;
import pe.gob.osinergmin.gnr.cgn.util.Util;

public class InstalacionActivity extends BaseActivity {

    private Montante montante;
    private Config config;
    private TableLayout tablaTuberia;
    private TextView txtDet_DocumentoPropietarioView;
    private TextView txtDet_nombrePropietarioView;
    private TextView txtDetProyectoView;
    private TextView txtDetMontanteView;
    private TextView txtDet_numeroIntHabilitacionView;
    private TextView txtDet_numeroSolicitudView;
    private TextView txtDet_numeroContratoView;
    private TextView txtDet_fechaSolicitudView;
    private TextView txtDet_fechaAprobacionView;
    private TextView txtDet_tipoProyectoView;
    private TextView txtDet_tipoInstalacionView;
    private TextView txtDet_numeroPuntosInstaacionView;
    private TextView txtDet_memoriaView;
    private ImageView icoTub_tipoInstalacion;
    private Spinner spiTub_tipoInstalacion;
    private ImageView icoTub_materialInstalacion;
    private Spinner spiTub_materialInstalacion;
    private TextInputLayout ediTub_longitudTotal;
    private TextInputLayout ediTub_diametroTotal;
    private ImageView icoTub_longitudTotal;
    private ImageView icoTub_diametroTotal;
    private SwitchCompat swiTub_aprueba;
    private Button btnTub_rechazar;
    private Toolbar toolbar;
    private MenuItem menuMas;
    private MenuItem menuMenos;
    private MontanteDao montanteDao;
    private List<GenericBeanOutRO> listaTipoInslacion = new ArrayList<>();
    private List<GenericBeanOutRO> listaTipoMaterial = new ArrayList<>();
    private int check1 = 0, check2 = 0;
    private DownTimer countDownTimer;
    private TiposInstalacionMontanteDao tiposInstalacionMontanteDao;
    private MParametroOutRODao mParametroOutRODao;
    private MaterialInstalacionDao materialInstalacionDao;
    private MHabilitacionMontanteOutRODao mHabilitacionMontanteOutRODao;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private PrecisionDao precisionDao;
    private final CompositeDisposable disposables = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instalacion);

        countDownTimer = DownTimer.getInstance();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        montanteDao = ((App) getApplication()).getDaoSession().getMontanteDao();
        tiposInstalacionMontanteDao = ((App) getApplication()).getDaoSession().getTiposInstalacionMontanteDao();
        mHabilitacionMontanteOutRODao = ((App) getApplication()).getDaoSession().getMHabilitacionMontanteOutRODao();
        mParametroOutRODao = ((App) getApplication()).getDaoSession().getMParametroOutRODao();
        materialInstalacionDao = ((App) getApplication()).getDaoSession().getMaterialInstalacionDao();
        precisionDao = ((App) getApplication()).getDaoSession().getPrecisionDao();

        toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_36dp);

        montante = getIntent().getExtras().getParcelable("MONTANTE");
        config = getIntent().getExtras().getParcelable("CONFIG");

        tablaTuberia = findViewById(R.id.tablaTuberia);
        txtDet_DocumentoPropietarioView = findViewById(R.id.txtDet_DocumentoPropietarioView);
        txtDet_nombrePropietarioView = findViewById(R.id.txtDet_nombrePropietarioView);
        txtDetProyectoView = findViewById(R.id.txtDetProyectoView);
        txtDetMontanteView = findViewById(R.id.txtDetMontanteView);
        txtDet_numeroIntHabilitacionView = findViewById(R.id.txtDet_numeroIntHabilitacionView);
        txtDet_numeroSolicitudView = findViewById(R.id.txtDet_numeroSolicitudView);
        txtDet_numeroContratoView = findViewById(R.id.txtDet_numeroContratoView);
        txtDet_fechaSolicitudView = findViewById(R.id.txtDet_fechaSolicitudView);
        txtDet_fechaAprobacionView = findViewById(R.id.txtDet_fechaAprobacionView);
        txtDet_tipoProyectoView = findViewById(R.id.txtDet_tipoProyectoView);
        txtDet_tipoInstalacionView = findViewById(R.id.txtDet_tipoInstalacionView);
        txtDet_numeroPuntosInstaacionView = findViewById(R.id.txtDet_numeroPuntosInstaacionView);
        txtDet_memoriaView = findViewById(R.id.txtDet_memoriaView);
        icoTub_tipoInstalacion = findViewById(R.id.icoTub_tipoInstalacion);
        spiTub_tipoInstalacion = findViewById(R.id.spiTub_tipoInstalacion);
        icoTub_materialInstalacion = findViewById(R.id.icoTub_materialInstalacion);
        spiTub_materialInstalacion = findViewById(R.id.spiTub_materialInstalacion);
        icoTub_longitudTotal = findViewById(R.id.icoTub_longitudTotal);
        ediTub_longitudTotal = findViewById(R.id.ediTub_longitudTotal);
        icoTub_diametroTotal = findViewById(R.id.icoTub_diametroTotal);
        ediTub_diametroTotal = findViewById(R.id.ediTub_diametroTotal);
        swiTub_aprueba = findViewById(R.id.swiTub_aprueba);
        Button btnTub_evaluando = findViewById(R.id.btnTub_evaluando);
        btnTub_rechazar = findViewById(R.id.btnTub_rechazar);

        spiTub_tipoInstalacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                check1++;
                if (check1 > 1) {
                    if (position != 0) {
                        montante.setTipoInstalacion(setTipoInslacion(parent.getSelectedItem().toString()));
                    } else {
                        montante.setTipoInstalacion("");
                    }
                    validarCheck();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spiTub_materialInstalacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                check2++;
                if (check2 > 1) {
                    if (position != 0) {
                        montante.setTipoMaterialInstalacion(setTipoMaterial(parent.getSelectedItem().toString()));
                        montante.setNombreMaterialInstalacion(parent.getSelectedItem().toString());
                    } else {
                        montante.setTipoMaterialInstalacion("");
                        montante.setNombreMaterialInstalacion("");
                    }
                    validarCheck();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ediTub_longitudTotal.setTag(1);
        ediTub_longitudTotal.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (0 == (int) ediTub_longitudTotal.getTag()) {
                    ediTub_longitudTotal.setTag(1);
                    return;
                }
                if (s.length() > 0) {
                    if (Util.validarScaleAndPrecision(s.toString(), 2, 5)) {
                        montante.setLongitudInstalacion(s.toString());
                    } else {
                        ((App) getApplication()).showToast(String.format(getResources().getString(R.string.app_error_numero), 5, 2));
                        ediTub_longitudTotal.setTag(0);
                        ediTub_longitudTotal.getEditText().setText(montante.getLongitudInstalacion());
                        ediTub_longitudTotal.getEditText().setSelection(ediTub_longitudTotal.getEditText().getText().length());
                    }
                } else {
                    montante.setLongitudInstalacion("");
                }
                validarCheck();
            }
        });

        ediTub_diametroTotal.setTag(1);
        ediTub_diametroTotal.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (0 == (int) ediTub_diametroTotal.getTag()) {
                    ediTub_diametroTotal.setTag(1);
                    return;
                }
                if (s.length() > 0) {
                    if (Util.validarScaleAndPrecision(s.toString(), 2, 5)) {
                        montante.setDiametroTuberiaInstalacion(s.toString());
                    } else {
                        ((App) getApplication()).showToast(String.format(getResources().getString(R.string.app_error_numero), 5, 2));
                        ediTub_diametroTotal.setTag(0);
                        ediTub_diametroTotal.getEditText().setText(montante.getDiametroTuberiaInstalacion());
                        ediTub_diametroTotal.getEditText().setSelection(ediTub_diametroTotal.getEditText().getText().length());
                    }
                } else {
                    montante.setDiametroTuberiaInstalacion("");
                }
                validarCheck();
            }
        });

        swiTub_aprueba.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    montante.setApruebaInstalacion("1");
                    btnTub_rechazar.setVisibility(View.GONE);
                } else {
                    montante.setApruebaInstalacion("0");
                    btnTub_rechazar.setVisibility(View.VISIBLE);
                }
            }
        });

        assert btnTub_evaluando != null;
        btnTub_evaluando.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                montanteDao.insertOrReplace(montante);
                Intent intent = new Intent(InstalacionActivity.this, HabilitacionMontanteActivity.class);
                intent.putExtra("MONTANTE", montante);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
            }
        });

        btnTub_rechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InstalacionActivity.this, RechazoMontanteActivity.class);
                intent.putExtra("MONTANTE", montante);
                intent.putExtra("CONFIG", config);
                intent.putExtra("TIPO", "");
                startActivity(intent);
            }
        });
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    Precision precision = new Precision();
                    precision.setIdSolicitud(montante.getIdHabilitacionMontante());
                    precision.setLatitud(String.valueOf(location.getLatitude()));
                    precision.setLongitud(String.valueOf(location.getLongitude()));
                    precision.setPrecision(String.valueOf(location.getAccuracy()));
                    precision.setEstado("2");
                    precisionDao.insertOrReplace(precision);
                }
            }
        };
        aceptarPermiso();
    }

    private void validarCheck() {
        if ("".equalsIgnoreCase(montante.getTipoInstalacion())) {
            icoTub_tipoInstalacion.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            icoTub_tipoInstalacion.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if ("".equalsIgnoreCase(montante.getTipoMaterialInstalacion())) {
            icoTub_materialInstalacion.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            icoTub_materialInstalacion.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if ("".equalsIgnoreCase(montante.getLongitudInstalacion())) {
            icoTub_longitudTotal.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            icoTub_longitudTotal.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if ("".equalsIgnoreCase(montante.getDiametroTuberiaInstalacion())) {
            icoTub_diametroTotal.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            icoTub_diametroTotal.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        validarSelector();
    }

    private void validarSelector() {
        if ("".equalsIgnoreCase(montante.getTipoInstalacion()) ||
                "".equalsIgnoreCase(montante.getTipoMaterialInstalacion()) ||
                "".equalsIgnoreCase(montante.getLongitudInstalacion()) ||
                "".equalsIgnoreCase(montante.getDiametroTuberiaInstalacion())) {
            swiTub_aprueba.setClickable(false);
            swiTub_aprueba.setChecked(false);
        } else {
            swiTub_aprueba.setClickable(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.createAndStart();
        createLocationRequest();
        toolbar.setTitle(montante.getObjetoConexion() + " - Datos de la tubería instalada");
        cargarSuministro();
        cargaDatos();
        validarCheck();
        if (config.getOffline()) {
            listaTipoInslacion.clear();
            List<TiposInstalacionMontante> tiposInstalacionMontantes = tiposInstalacionMontanteDao.queryBuilder().list();
            for (TiposInstalacionMontante tiposInstalacionMontante : tiposInstalacionMontantes) {
                listaTipoInslacion.add(Parse.getGenericBeanOutRO(tiposInstalacionMontante));
            }
            ArrayList<String> list = new ArrayList<>();
            list.add("Seleccione tipo de instalación");
            for (GenericBeanOutRO genericBeanOutRO : listaTipoInslacion) {
                list.add(genericBeanOutRO.getLabel());
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<>(InstalacionActivity.this, android.R.layout.simple_spinner_item, list);
            adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            spiTub_tipoInstalacion.setAdapter(adapter);
            if (!("".equalsIgnoreCase(montante.getTipoInstalacion()))) {
                spiTub_tipoInstalacion.setSelection(getTipoInslacion(montante.getTipoInstalacion()));
            } else {
                spiTub_tipoInstalacion.setSelection(0);
            }
            listaTipoMaterial.clear();
            List<MaterialInstalacion> materialInstalacions = materialInstalacionDao.queryBuilder().list();
            for (MaterialInstalacion materialInstalacion : materialInstalacions) {
                listaTipoMaterial.add(Parse.getGenericBeanOutRO(materialInstalacion));
            }
            ArrayList<String> list2 = new ArrayList<>();
            list2.add("Seleccione material de instalación");
            for (GenericBeanOutRO genericBeanOutRO : listaTipoMaterial) {
                list2.add(genericBeanOutRO.getLabel());
            }
            ArrayAdapter<String> adapter2 = new ArrayAdapter<>(InstalacionActivity.this, android.R.layout.simple_spinner_item, list2);
            adapter2.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            spiTub_materialInstalacion.setAdapter(adapter2);
            if (!(montante.getTipoMaterialInstalacion().equalsIgnoreCase(""))) {
                spiTub_materialInstalacion.setSelection(getTipoMaterial(montante.getTipoMaterialInstalacion()));
            } else {
                spiTub_materialInstalacion.setSelection(0);
            }
            MParametroOutRO mParametroOutPrecision = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_FRECUENCIA_TOMA_COORDENADAS_AUDITORIA))
                    .limit(1).unique();
            config.setPrecision(mParametroOutPrecision == null ? "10000" : mParametroOutPrecision.getValorParametro());
        } else {
            if (listaTipoInslacion.size() < 1) {
                if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    showProgressDialog(R.string.app_cargando);
                    disposables.add(MontanteRx.getTiposInstalacion(config.getToken())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(listGenericBeanOutRO -> {
                                if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                                    hideProgressDialog();
                                    ((App) getApplication()).showToast(listGenericBeanOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listGenericBeanOutRO.getMessage());
                                } else {
                                    listaTipoInslacion = listGenericBeanOutRO.getGenericBean();
                                    ArrayList<String> list = new ArrayList<>();
                                    list.add("Seleccione tipo de instalación");
                                    for (GenericBeanOutRO genericBeanOutRO : listaTipoInslacion) {
                                        list.add(genericBeanOutRO.getLabel());
                                    }
                                    ArrayAdapter<String> adapter = new ArrayAdapter<>(InstalacionActivity.this, android.R.layout.simple_spinner_item, list);
                                    adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                                    spiTub_tipoInstalacion.setAdapter(adapter);
                                    if (!("".equalsIgnoreCase(montante.getTipoInstalacion()))) {
                                        spiTub_tipoInstalacion.setSelection(getTipoInslacion(montante.getTipoInstalacion()));
                                    } else {
                                        spiTub_tipoInstalacion.setSelection(0);
                                    }
                                    if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                        TipoMaterialInstalacion tipoMaterialInstalacion = new TipoMaterialInstalacion(new TipoMaterialInstalacion.OnTipoMaterialInstalacionCompleted() {
                                            @Override
                                            public void onTipoMaterialInstalacionCompled(ListGenericBeanOutRO listGenericBeanOutRO) {
                                                hideProgressDialog();
                                                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                                                } else if (listGenericBeanOutRO == null) {
                                                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                                                } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                                                    ((App) getApplication()).showToast(listGenericBeanOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listGenericBeanOutRO.getMessage());
                                                } else {
                                                    listaTipoMaterial = listGenericBeanOutRO.getGenericBean();
                                                    ArrayList<String> list = new ArrayList<>();
                                                    list.add("Seleccione material de instalación");
                                                    for (GenericBeanOutRO genericBeanOutRO : listaTipoMaterial) {
                                                        list.add(genericBeanOutRO.getLabel());
                                                    }
                                                    ArrayAdapter<String> adapter = new ArrayAdapter<>(InstalacionActivity.this, android.R.layout.simple_spinner_item, list);
                                                    adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                                                    spiTub_materialInstalacion.setAdapter(adapter);
                                                    if (!(montante.getTipoMaterialInstalacion().equalsIgnoreCase(""))) {
                                                        spiTub_materialInstalacion.setSelection(getTipoMaterial(montante.getTipoMaterialInstalacion()));
                                                    } else {
                                                        spiTub_materialInstalacion.setSelection(0);
                                                    }
                                                    if ("".equalsIgnoreCase(config.getParametro5())) {
                                                        config.setParametro5("800x600");
                                                    }
                                                    if ("".equalsIgnoreCase(config.getPrecision())) {
                                                        config.setPrecision("10000");
                                                    }
                                                }
                                            }
                                        });
                                        tipoMaterialInstalacion.execute(config);
                                    } else {
                                        ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                                    }
                                }
                            }, throwable -> {
                                hideProgressDialog();
                                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                                } else {
                                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                                }
                            }));
                } else {
                    ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                }
            }
        }
    }

    private void cargaDatos() {
        if (!montante.getLongitudInstalacion().equalsIgnoreCase("")) {
            ediTub_longitudTotal.getEditText().setText(montante.getLongitudInstalacion());
            ediTub_longitudTotal.getEditText().setSelection(ediTub_longitudTotal.getEditText().getText().length());
            icoTub_longitudTotal.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if (!montante.getDiametroTuberiaInstalacion().equalsIgnoreCase("")) {
            ediTub_diametroTotal.getEditText().setText(montante.getDiametroTuberiaInstalacion());
            ediTub_diametroTotal.getEditText().setSelection(ediTub_diametroTotal.getEditText().getText().length());
            icoTub_diametroTotal.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if (!(montante.getApruebaInstalacion().equalsIgnoreCase("0"))) {
            swiTub_aprueba.setChecked(true);
        }
    }

    private void cargarSuministro() {
        txtDet_DocumentoPropietarioView.setText(montante.getObjetoConexion());
        txtDet_nombrePropietarioView.setText(montante.getCup());
        txtDetProyectoView.setText(montante.getNombre());
        txtDetMontanteView.setText(montante.getNombreMontanteProyectoInstalacion());
        txtDet_numeroIntHabilitacionView.setText(montante.getDireccion());
        txtDet_numeroSolicitudView.setText(montante.getTipoProyecto());
        txtDet_numeroContratoView.setText(montante.getFechaRegistro());
        txtDet_fechaSolicitudView.setText(montante.getEsProyectoFise());
        txtDet_fechaAprobacionView.setText(montante.getTienePromocion());
        txtDet_tipoProyectoView.setText(montante.getEsZonaGasificada());
        txtDet_tipoInstalacionView.setText(montante.getTienePlanoInstalacion());
        txtDet_numeroPuntosInstaacionView.setText(montante.getTieneEspecificacion());
        txtDet_memoriaView.setText(montante.getTieneMemoria());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_general, menu);
        menuMas = menu.findItem(R.id.menuMas);
        menuMas.setVisible(true);
        menuMenos = menu.findItem(R.id.menuMenos);
        menuMenos.setVisible(false);
        return true;
    }

    private String setTipoInslacion(String tipoInslacion) {
        for (GenericBeanOutRO genericBeanOutRO : listaTipoInslacion) {
            if (tipoInslacion.equalsIgnoreCase(genericBeanOutRO.getLabel())) {
                return genericBeanOutRO.getValue();
            }
        }
        return null;
    }

    private int getTipoInslacion(String tipoInslacion) {
        int i = 0;
        boolean entro = false;
        for (GenericBeanOutRO genericBeanOutRO : listaTipoInslacion) {
            if (tipoInslacion.equalsIgnoreCase(genericBeanOutRO.getValue())) {
                entro = true;
                break;
            }
            i++;
        }
        if (entro) {
            return i + 1;
        } else {
            return 0;
        }
    }

    private String setTipoMaterial(String tipoMaterial) {
        for (GenericBeanOutRO genericBeanOutRO : listaTipoMaterial) {
            if (tipoMaterial.equalsIgnoreCase(genericBeanOutRO.getLabel())) {
                return genericBeanOutRO.getValue();
            }
        }
        return null;
    }

    private int getTipoMaterial(String tipoMaterial) {
        int i = 0;
        boolean entro = false;
        for (GenericBeanOutRO genericBeanOutRO : listaTipoMaterial) {
            if (tipoMaterial.equalsIgnoreCase(genericBeanOutRO.getValue())) {
                entro = true;
                break;
            }
            i++;
        }
        if (entro) {
            return i + 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menuMas:
                tablaTuberia.setVisibility(View.VISIBLE);
                menuMas.setVisible(false);
                menuMenos.setVisible(true);
                return true;

            case R.id.menuMenos:
                tablaTuberia.setVisibility(View.GONE);
                menuMas.setVisible(true);
                menuMenos.setVisible(false);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        montanteDao.insertOrReplace(montante);
        Intent intent = new Intent(InstalacionActivity.this, HabilitacionMontanteActivity.class);
        intent.putExtra("MONTANTE", montante);
        intent.putExtra("CONFIG", config);
        startActivity(intent);
        finish();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        countDownTimer.createAndStart();
    }

    private void aceptarPermiso() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permiso = Util.getPermisos(InstalacionActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION);
            if (permiso != null) {
                requestPermissions(permiso, pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1458) {
            if (resultCode == RESULT_CANCELED) {
                Intent intent = new Intent(InstalacionActivity.this, BuscarMontanteActivity.class);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
            }
        }
    }

    private void createLocationRequest() {
        int value = 10000;
        try {
            value = Integer.valueOf(config.getPrecision());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(value);
        mLocationRequest.setFastestInterval(value);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(InstalacionActivity.this, 1458);
                    } catch (IntentSender.SendIntentException sendEx) {
                    }
                }
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            aceptarPermiso();
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION:
                String message = Util.onRequestPermissionsResult(permissions, grantResults);
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    @Override
    protected void onDestroy() {
        disposables.clear();
        super.onDestroy();
    }
}
