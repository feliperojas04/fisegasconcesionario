package pe.gob.osinergmin.gnr.cgn.adaptador;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;

import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.GenericBeanOutRO;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.data.models.Punto;
import pe.gob.osinergmin.gnr.cgn.data.models.PuntoDao;
import pe.gob.osinergmin.gnr.cgn.util.MostrarFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.Util;

public class PuntoAdaptador extends BaseAdapter {

    private Context context;
    private List<Punto> puntos;
    private ListView listPuntos;
    private SwitchCompat aprueba;
    private List<GenericBeanOutRO> listatipoObse;
    private PuntoDao puntoDao;
    private Callback callback;

    public PuntoAdaptador(Context context, List<Punto> puntos) {
        this.context = context;
        this.puntos = puntos;
    }

    public void setListatipoObse(List<GenericBeanOutRO> listatipoObse) {
        this.listatipoObse = listatipoObse;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void setPuntoDao(PuntoDao puntoDao) {
        this.puntoDao = puntoDao;
    }

    public void setPuntos(ListView listPuntos) {
        this.listPuntos = listPuntos;
    }

    public void setAprueba(SwitchCompat aprueba) {
        this.aprueba = aprueba;
    }

    @Override
    public int getCount() {
        if (puntos.size() == 0) {
            aprueba.setClickable(false);
        }
        return this.puntos.size();
    }

    public Punto getPunto(int position) {
        for (Punto punto : puntos) {
            if (punto.getPosicion() == position) {
                return punto;
            }
        }
        return null;
    }

    @Override
    public Object getItem(int position) {
        return this.puntos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {
        View rowView = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = layoutInflater.inflate(R.layout.item_punto, parent, false);

            holder.icoPunI_punto = rowView.findViewById(R.id.icoPunI_punto);
            holder.remPunI_punto = rowView.findViewById(R.id.remPunI_punto);
            holder.txtPunI_punto = rowView.findViewById(R.id.txtPunI_punto);
            holder.spiPunI_punto = rowView.findViewById(R.id.spiPunI_punto);
            holder.imgPunI_fotoGasodomestico = rowView.findViewById(R.id.imgPunI_fotoGasodomestico);
            holder.ediPunI_diametro = rowView.findViewById(R.id.ediPunI_diametro);

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        holder.ref = position;

        //Todo se tiene que probar si guarda la posicio correctamente
        puntos.get(position).setPosicion(position + 1);

        holder.remPunI_punto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getCount() > 1) {
                    puntoDao.delete(puntos.get(holder.ref));
                    puntos.remove(holder.ref);
                    notifyDataSetChanged();
                    Util.setListViewHeightBasedOnChildren(listPuntos);
                    check();
                } else {
                    ((App) context.getApplicationContext()).showToast("Se debe registrar un punto como mínimo");
                }
            }
        });

        String recurso = rowView.getResources().getString(R.string.txtPunI_punto);
        String formateada = String.format(recurso, String.valueOf(position + 1));
        holder.txtPunI_punto.setText(formateada);

        holder.spiPunI_punto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                holder.check++;
                if (holder.check > 1) {
                    if (position != 0) {
                        puntos.get(holder.ref).setPuntoGasodomestico(setTipoGaso(parent.getSelectedItem().toString()));
                    } else {
                        puntos.get(holder.ref).setPuntoGasodomestico("");
                    }
                    puntoDao.insertOrReplace(puntos.get(holder.ref));
                    validar(holder);
                    check();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ArrayList<String> list = new ArrayList<>();
        list.add("Seleccione gasodoméstico");
        for (GenericBeanOutRO genericBeanOutRO : listatipoObse) {
            list.add(genericBeanOutRO.getLabel());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        holder.spiPunI_punto.setAdapter(adapter);

        if (!"".equalsIgnoreCase(puntos.get(holder.ref).getPuntoGasodomestico())) {
            holder.spiPunI_punto.setSelection(getTipoGaso(puntos.get(holder.ref).getPuntoGasodomestico()));
        }

        holder.ediPunI_diametro.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Util.validarScaleAndPrecision(s.toString(), 2, 5)) {
                    puntos.get(holder.ref).setPuntoDiametroValvula(s.toString());
                } else {
                    ((App) context.getApplicationContext()).showToast(String.format(context.getResources().getString(R.string.app_error_numero), 5, 2));
                    holder.ediPunI_diametro.getEditText().setText(puntos.get(holder.ref).getPuntoDiametroValvula());
                    holder.ediPunI_diametro.getEditText().setSelection(holder.ediPunI_diametro.getEditText().getText().length());
                }
                puntoDao.insertOrReplace(puntos.get(holder.ref));
                validar(holder);
                check();
            }
        });

        holder.imgPunI_fotoGasodomestico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.clicFoto(puntos.get(holder.ref).getPosicion());
            }
        });

        if (!"".equalsIgnoreCase(puntos.get(holder.ref).getPuntoFoto())) {
            loadBitmap(puntos.get(holder.ref).getPuntoFoto(), holder.imgPunI_fotoGasodomestico);
        } else {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(65, 65);
            layoutParams.setMargins(10, 10, 30, 10);
            holder.imgPunI_fotoGasodomestico.setLayoutParams(layoutParams);
            holder.imgPunI_fotoGasodomestico.setScaleType(ImageView.ScaleType.FIT_CENTER);
            holder.imgPunI_fotoGasodomestico.setImageResource(R.mipmap.ic_add_a_photo_black_36dp);
        }

        if (!puntos.get(holder.ref).getPuntoDiametroValvula().equalsIgnoreCase("")) {
            holder.ediPunI_diametro.getEditText().setText(puntos.get(holder.ref).getPuntoDiametroValvula());
            holder.ediPunI_diametro.getEditText().setSelection(holder.ediPunI_diametro.getEditText().getText().length());
        } else {
            holder.ediPunI_diametro.getEditText().setText("");
        }

        validar(holder);
        check();
        return rowView;
    }

    private void validar(ViewHolder holder) {

        if ("".equalsIgnoreCase(puntos.get(holder.ref).getPuntoGasodomestico()) ||
                "".equalsIgnoreCase(puntos.get(holder.ref).getPuntoDiametroValvula()) ||
                "".equalsIgnoreCase(puntos.get(holder.ref).getPuntoFoto())) {
            holder.icoPunI_punto.setImageResource(R.mipmap.ic_warning_black_36dp);
            aprueba.setChecked(false);
        } else {
            holder.icoPunI_punto.setImageResource(R.mipmap.ic_done_black_36dp);
        }
    }

    private void check() {
        int i = 0;
        boolean boton = true;
        while (i < puntos.size()) {
            Punto punto = puntos.get(i);

            if ("".equalsIgnoreCase(punto.getPuntoGasodomestico()) ||
                    "".equalsIgnoreCase(punto.getPuntoDiametroValvula()) ||
                    "".equalsIgnoreCase(punto.getPuntoFoto())) {
                boton = false;
                i = puntos.size();
            }
            i++;
        }
        aprueba.setClickable(boton);
    }

    private String setTipoGaso(String tipoGaso) {
        for (GenericBeanOutRO genericBeanOutRO : listatipoObse) {
            if (tipoGaso.equalsIgnoreCase(genericBeanOutRO.getLabel())) {
                return genericBeanOutRO.getValue();
            }
        }
        return null;
    }

    private int getTipoGaso(String tipoGaso) {
        int i = 0;
        boolean entro = false;
        for (GenericBeanOutRO genericBeanOutRO : listatipoObse) {
            if (tipoGaso.equalsIgnoreCase(genericBeanOutRO.getValue())) {
                entro = true;
                break;
            }
            i++;
        }
        if (entro) {
            return i + 1;
        } else {
            return 0;
        }
    }

    public interface Callback {
        void clicFoto(int position);
    }

    private class ViewHolder {
        int ref;
        ImageView icoPunI_punto;
        TextView txtPunI_punto;
        ImageView remPunI_punto;
        Spinner spiPunI_punto;
        ImageView imgPunI_fotoGasodomestico;
        TextInputLayout ediPunI_diametro;
        int check;
    }

    public void loadBitmap(String ruta, final ImageView imageView) {
        MostrarFotoTask mostrarFotoTask = new MostrarFotoTask(new MostrarFotoTask.OnMostrarFotoTaskCompleted() {
            @Override
            public void onMostrarFotoTaskCompleted(Bitmap bitmap) {
                if (bitmap != null) {
                    if (imageView != null) {
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(120, 120);
                        layoutParams.setMargins(10, 10, 10, 10);
                        imageView.setLayoutParams(layoutParams);
                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        imageView.setImageBitmap(bitmap);
                    }
                } else {
                    ((App) context.getApplicationContext()).showToast("Error al cargar la foto");
                }
            }
        });
        mostrarFotoTask.execute(ruta);
    }
}
