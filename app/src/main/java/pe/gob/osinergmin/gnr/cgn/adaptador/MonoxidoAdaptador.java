package pe.gob.osinergmin.gnr.cgn.adaptador;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;

import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.data.models.AmbienteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Ambiente;
import pe.gob.osinergmin.gnr.cgn.util.MostrarFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.Util;

public class MonoxidoAdaptador extends BaseAdapter {

    private Context context;
    private List<Ambiente> ambientes;
    private SwitchCompat aprueba;
    private AmbienteDao ambienteDao;
    private Callback callback;

    public MonoxidoAdaptador(Context context, List<Ambiente> ambientes) {
        this.context = context;
        this.ambientes = ambientes;
    }

    public void setAmbienteDao(AmbienteDao ambienteDao) {
        this.ambienteDao = ambienteDao;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void setAprueba(SwitchCompat aprueba) {
        this.aprueba = aprueba;
    }

    @Override
    public int getCount() {
        return this.ambientes.size();
    }

    @Override
    public Object getItem(int position) {
        return this.ambientes.get(position);
    }

    public Ambiente getAmbiente(int position) {
        for (Ambiente ambiente : ambientes) {
            if (ambiente.getPosicion() == position) {
                return ambiente;
            }
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        View rowView = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = layoutInflater.inflate(R.layout.item_monoxido, parent, false);

            holder.icoPrueM_medicion = rowView.findViewById(R.id.icoPrueM_medicion);
            holder.textNroAmbiente = rowView.findViewById(R.id.textNroAmbiente);
            holder.imgPrueM_fotoMedicion = rowView.findViewById(R.id.imgPrueM_fotoMedicion);
            holder.ediPrueM_valorMedicion = rowView.findViewById(R.id.ediPrueM_valorMedicion);

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        holder.ref = position;

        String recurso = rowView.getResources().getString(R.string.txtPrueM_ambiente);
        String formateada = String.format(recurso, String.valueOf(ambientes.get(holder.ref).getPosicion()));
        holder.textNroAmbiente.setText(formateada);

        holder.ediPrueM_valorMedicion.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Util.validarScaleAndPrecision(s.toString(), 2, 5)) {
                    ambientes.get(holder.ref).setMonoxidoMedicion(s.toString());
                } else {
                    ((App) context.getApplicationContext()).showToast(String.format(context.getResources().getString(R.string.app_error_numero), 5, 2));
                    holder.ediPrueM_valorMedicion.getEditText().setText(ambientes.get(holder.ref).getMonoxidoMedicion());
                    holder.ediPrueM_valorMedicion.getEditText().setSelection(holder.ediPrueM_valorMedicion.getEditText().getText().length());
                }
                ambienteDao.insertOrReplace(ambientes.get(holder.ref));
                validar(holder);
                check();
            }
        });

        holder.imgPrueM_fotoMedicion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.clicFoto(ambientes.get(holder.ref).getPosicion());
            }
        });

        if (!"".equalsIgnoreCase(ambientes.get(holder.ref).getMonoxidoFoto())) {
            loadBitmap(ambientes.get(holder.ref).getMonoxidoFoto(), holder.imgPrueM_fotoMedicion);
        } else {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(65, 65);
            layoutParams.setMargins(10, 10, 30, 10);
            holder.imgPrueM_fotoMedicion.setLayoutParams(layoutParams);
            holder.imgPrueM_fotoMedicion.setScaleType(ImageView.ScaleType.FIT_CENTER);
            holder.imgPrueM_fotoMedicion.setImageResource(R.mipmap.ic_add_a_photo_black_36dp);
        }

        if (!"".equalsIgnoreCase(ambientes.get(holder.ref).getMonoxidoMedicion())) {
            holder.ediPrueM_valorMedicion.getEditText().setText(ambientes.get(holder.ref).getMonoxidoMedicion());
            holder.ediPrueM_valorMedicion.getEditText().setSelection(holder.ediPrueM_valorMedicion.getEditText().getText().length());
        }
        return rowView;
    }

    private void validar(ViewHolder holder) {
        if ("".equalsIgnoreCase(ambientes.get(holder.ref).getMonoxidoMedicion()) ||
                "".equalsIgnoreCase(ambientes.get(holder.ref).getMonoxidoFoto())) {
            holder.icoPrueM_medicion.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            holder.icoPrueM_medicion.setImageResource(R.mipmap.ic_done_black_36dp);
        }
    }

    private void check() {
        int i = 0;
        boolean boton = true;
        while (i < ambientes.size()) {
            Ambiente ambiente = ambientes.get(i);
            if ("".equalsIgnoreCase(ambiente.getMonoxidoMedicion()) ||
                    "".equalsIgnoreCase(ambiente.getMonoxidoFoto())) {
                boton = false;
                i = ambientes.size();
            }
            i++;
        }
        if (!boton) {
            aprueba.setChecked(boton);
        }
        aprueba.setClickable(boton);
    }

    public interface Callback {
        void clicFoto(int position);
    }

    private class ViewHolder {
        int ref;
        ImageView icoPrueM_medicion;
        TextView textNroAmbiente;
        ImageView imgPrueM_fotoMedicion;
        TextInputLayout ediPrueM_valorMedicion;
    }

    public void loadBitmap(String ruta, final ImageView imageView) {
        MostrarFotoTask mostrarFotoTask = new MostrarFotoTask(new MostrarFotoTask.OnMostrarFotoTaskCompleted() {
            @Override
            public void onMostrarFotoTaskCompleted(Bitmap bitmap) {
                if (bitmap != null) {
                    if (imageView != null) {
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(120, 120);
                        layoutParams.setMargins(10, 10, 10, 10);
                        imageView.setLayoutParams(layoutParams);
                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        imageView.setImageBitmap(bitmap);
                    }
                } else {
                    ((App) context.getApplicationContext()).showToast("Error al cargar la foto");
                }
            }
        });
        mostrarFotoTask.execute(ruta);
    }
}
