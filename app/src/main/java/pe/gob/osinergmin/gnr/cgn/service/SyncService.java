package pe.gob.osinergmin.gnr.cgn.service;

import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.HabilitacionOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.InstalacionAcometidaOutRO;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.data.models.AcometidaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.AmbienteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.ConfigDao;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionMontanteOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MInstalacionAcometidaOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MontanteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.ObservacionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.PrecisionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.PuntoDao;
import pe.gob.osinergmin.gnr.cgn.data.models.RechazoDao;
import pe.gob.osinergmin.gnr.cgn.data.models.RechazoMontante;
import pe.gob.osinergmin.gnr.cgn.data.models.RechazoMontanteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.SuministroDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Acometida;
import pe.gob.osinergmin.gnr.cgn.data.models.Ambiente;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionMontanteOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MInstalacionAcometidaOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.Montante;
import pe.gob.osinergmin.gnr.cgn.data.models.Observacion;
import pe.gob.osinergmin.gnr.cgn.data.models.Precision;
import pe.gob.osinergmin.gnr.cgn.data.models.Punto;
import pe.gob.osinergmin.gnr.cgn.data.models.Rechazo;
import pe.gob.osinergmin.gnr.cgn.data.models.Suministro;
import pe.gob.osinergmin.gnr.cgn.task.GrabarAcometida;
import pe.gob.osinergmin.gnr.cgn.task.GrabarHabilitacion;
import pe.gob.osinergmin.gnr.cgn.task.MontanteRx;
import pe.gob.osinergmin.gnr.cgn.util.BorrarFoto;
import pe.gob.osinergmin.gnr.cgn.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.util.NetworkUtil;

public class SyncService extends Service {

    private static final String TAG = SyncService.class.getSimpleName();

    private Config config;
    private ConfigDao configDao;
    private PuntoDao puntoDao;
    private AmbienteDao ambienteDao;
    private SuministroDao suministroDao;
    private MontanteDao montanteDao;
    private AcometidaDao acometidaDao;
    private MHabilitacionOutRODao mHabilitacionOutRODao;
    private MHabilitacionMontanteOutRODao mHabilitacionMontanteOutRODao;
    private MInstalacionAcometidaOutRODao mInstalacionAcometidaOutRODao;
    private RechazoDao rechazoDao;
    private PrecisionDao precisionDao;
    private ObservacionDao observacionDao;

    private int cont = 0, porcentaje = 0;
    private boolean sync = true, sinWifi = false;
    private final CompositeDisposable disposables = new CompositeDisposable();

    private RechazoMontanteDao rechazoMontanteDao;

    public SyncService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        puntoDao = ((App) getApplication()).getDaoSession().getPuntoDao();
        configDao = ((App) getApplication()).getDaoSession().getConfigDao();
        ambienteDao = ((App) getApplication()).getDaoSession().getAmbienteDao();
        config = configDao.queryBuilder().limit(1).unique();
        suministroDao = ((App) getApplication()).getDaoSession().getSuministroDao();
        montanteDao = ((App) getApplication()).getDaoSession().getMontanteDao();
        mHabilitacionOutRODao = ((App) getApplication()).getDaoSession().getMHabilitacionOutRODao();
        mHabilitacionMontanteOutRODao = ((App) getApplication()).getDaoSession().getMHabilitacionMontanteOutRODao();
        mInstalacionAcometidaOutRODao = ((App) getApplication()).getDaoSession().getMInstalacionAcometidaOutRODao();
        rechazoDao = ((App) getApplication()).getDaoSession().getRechazoDao();
        precisionDao = ((App) getApplication()).getDaoSession().getPrecisionDao();
        acometidaDao = ((App) getApplication()).getDaoSession().getAcometidaDao();
        observacionDao = ((App) getApplication()).getDaoSession().getObservacionDao();
        rechazoMontanteDao = ((App) getApplication()).getDaoSession().getRechazoMontanteDao();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final NotificationCompat.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = new NotificationCompat.Builder(this, Constantes.CHANNEL_ID)
                    .setSmallIcon(android.R.drawable.stat_sys_upload_done)
                    .setContentTitle("CGN - Concesionaria")
                    .setContentText("Sincronizando datos...");
        } else {
            builder = new NotificationCompat.Builder(this)
                    .setSmallIcon(android.R.drawable.stat_sys_upload_done)
                    .setContentTitle("CGN - Concesionaria")
                    .setContentText("Sincronizando datos...");
        }
        builder.setProgress(11, porcentaje, false);
        startForeground(1, builder.build());

        if (config.getDesPreVisitas()) {
            porcentaje++;
            builder.setProgress(11, porcentaje, false);
            startForeground(1, builder.build());
            List<Suministro> suministros = suministroDao
                    .queryBuilder()
                    .where(SuministroDao
                            .Properties
                            .Grabar
                            .eq(true))
                    .list();
            for (final Suministro suministro : suministros) {
                sync = false;
                cont++;
                final MHabilitacionOutRO mHabilitacionOutRO = mHabilitacionOutRODao
                        .queryBuilder()
                        .where(MHabilitacionOutRODao
                                .Properties
                                .IdHabilitacion
                                .eq(suministro.getIdHabilitacion()))
                        .limit(1)
                        .unique();
                GrabarHabilitacion grabarHabilitacion = new GrabarHabilitacion(
                        new GrabarHabilitacion.OnGrabarHabilitacionAsyncTaskCompleted() {
                            @Override
                            public void onGrabarHabilitacionAsyncTaskCompleted(HabilitacionOutRO habilitacionOutRO) {
                                porcentaje++;
                                builder.setProgress(11, porcentaje, false);
                                startForeground(1, builder.build());
                                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                    sinWifi = true;
                                    stopSelf();
                                } else if (habilitacionOutRO == null) {
                                    mHabilitacionOutRO.setMessage("Ocurrió un error inesperado");
                                    mHabilitacionOutRO.setErrorCode("100");
                                    mHabilitacionOutRODao.insertOrReplace(mHabilitacionOutRO);
                                } else if (!habilitacionOutRO.getResultCode().equalsIgnoreCase(gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS)) {
                                    mHabilitacionOutRO.setMessage(habilitacionOutRO.getMessage() == null ? "Ocurrió un error inesperado" : habilitacionOutRO.getMessage());
                                    mHabilitacionOutRO.setErrorCode("100");
                                    mHabilitacionOutRODao.insertOrReplace(mHabilitacionOutRO);
                                } else {
                                    new BorrarFoto().execute(suministro.getIdHabilitacion().toString());

                                    List<Punto> puntos = puntoDao.queryBuilder().where(PuntoDao.Properties.IdHabilitacion.eq(suministro.getIdHabilitacion())).list();
                                    puntoDao.deleteInTx(puntos);

                                    List<Ambiente> ambientes = ambienteDao.queryBuilder().where(AmbienteDao.Properties.IdHabilitacion.eq(suministro.getIdHabilitacion())).list();
                                    ambienteDao.deleteInTx(ambientes);

                                    List<Rechazo> rechazos = rechazoDao.queryBuilder().where(RechazoDao.Properties.IdHabilitacion.eq(suministro.getIdHabilitacion())).list();
                                    rechazoDao.deleteInTx(rechazos);

                                    List<Precision> precisiones = precisionDao.queryBuilder()
                                            .where(PrecisionDao.Properties.Estado.eq("1"),
                                                    PrecisionDao.Properties.IdSolicitud.eq(suministro.getIdHabilitacion())).list();
                                    precisionDao.deleteInTx(precisiones);

                                    suministroDao.delete(suministro);
                                    mHabilitacionOutRODao.delete(mHabilitacionOutRO);
                                }
                                cont--;
                                if (cont == 0) {
                                    stopSelf();
                                }
                            }
                        }, getApplicationContext()
                );
                grabarHabilitacion.execute(suministro);
            }
        }
        if (config.getDesVisitas()) {
            porcentaje++;
            builder.setProgress(11, porcentaje, false);
            startForeground(1, builder.build());
            List<Montante> montantes = montanteDao
                    .queryBuilder()
                    .where(MontanteDao
                            .Properties
                            .Grabar
                            .eq(true))
                    .list();
            for (final Montante montante : montantes) {
                sync = false;
                cont++;
                final MHabilitacionMontanteOutRO mHabilitacionMontanteOutRO = mHabilitacionMontanteOutRODao
                        .queryBuilder()
                        .where(MHabilitacionMontanteOutRODao
                                .Properties
                                .IdHabilitacionMontante
                                .eq(montante.getIdHabilitacionMontante()))
                        .limit(1)
                        .unique();
                disposables.add(MontanteRx.getMontanteRegistrar(precisionDao, montante, config.getToken(), rechazoMontanteDao)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(habilitacionMontanteOutRO -> {
                            porcentaje++;
                            builder.setProgress(11, porcentaje, false);
                            startForeground(1, builder.build());
                            if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                sinWifi = true;
                                stopSelf();
                            } else if (habilitacionMontanteOutRO == null) {
                                mHabilitacionMontanteOutRO.setMessage("Error interno");
                                mHabilitacionMontanteOutRO.setErrorCode("100");
                                mHabilitacionMontanteOutRODao.insertOrReplace(mHabilitacionMontanteOutRO);
                            } else if (!habilitacionMontanteOutRO.getResultCode().equalsIgnoreCase(gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS)) {
                                mHabilitacionMontanteOutRO.setMessage(habilitacionMontanteOutRO.getMessage() == null ? "Error interno" : habilitacionMontanteOutRO.getMessage());
                                mHabilitacionMontanteOutRO.setErrorCode("100");
                                mHabilitacionMontanteOutRODao.insertOrReplace(mHabilitacionMontanteOutRO);
                            } else {
                                List<Precision> precisiones = precisionDao.queryBuilder()
                                        .where(PrecisionDao.Properties.Estado.eq("2"),
                                                PrecisionDao.Properties.IdSolicitud.eq(montante.getIdHabilitacionMontante())).list();
                                precisionDao.deleteInTx(precisiones);

                                List<RechazoMontante> rechazoMontantes = rechazoMontanteDao.queryBuilder().where(RechazoMontanteDao.Properties.IdHabilitacionMontante
                                        .eq(montante.getIdHabilitacionMontante())).list();
                                rechazoMontanteDao.deleteInTx(rechazoMontantes);

                                mHabilitacionMontanteOutRODao.delete(mHabilitacionMontanteOutRO);
                                montanteDao.delete(montante);
                            }
                            cont--;
                            if (cont == 0) {
                                stopSelf();
                            }
                        }, throwable -> {
                            if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                sinWifi = true;
                            } else {
                                mHabilitacionMontanteOutRO.setMessage(throwable.getMessage());
                                mHabilitacionMontanteOutRO.setErrorCode("100");
                                mHabilitacionMontanteOutRODao.insertOrReplace(mHabilitacionMontanteOutRO);
                            }
                            throwable.printStackTrace();
                            stopSelf();
                        }));
            }
        }
        if (config.getDesAcometidas()) {
            porcentaje++;
            builder.setProgress(11, porcentaje, false);
            startForeground(1, builder.build());
            List<Acometida> acometidas = acometidaDao
                    .queryBuilder()
                    .where(AcometidaDao
                            .Properties
                            .Grabar
                            .eq(true))
                    .list();
            for (final Acometida acometida : acometidas) {
                sync = false;
                cont++;
                final MInstalacionAcometidaOutRO mInstalacionAcometidaOutRO = mInstalacionAcometidaOutRODao
                        .queryBuilder()
                        .where(MInstalacionAcometidaOutRODao
                                .Properties
                                .IdInstalacionAcometida
                                .eq(acometida.getIdInstalacionAcometida()))
                        .limit(1)
                        .unique();
                GrabarAcometida grabarAcometida = new GrabarAcometida(new GrabarAcometida.OnGrabarAcometidaAsyncTaskCompleted() {
                    @Override
                    public void onGrabarAcometidaAsyncTaskCompleted(InstalacionAcometidaOutRO instalacionAcometidaOutRO) {
                        porcentaje++;
                        builder.setProgress(11, porcentaje, false);
                        startForeground(1, builder.build());
                        if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                            sinWifi = true;
                            stopSelf();
                        } else if (instalacionAcometidaOutRO == null) {
                            //mInstalacionAcometidaOutRO.setMessage("Ocurrió un error inesperado");
                            //mInstalacionAcometidaOutRO.setErrorCode("100");
                            //mInstalacionAcometidaOutRODao.insertOrReplace(mInstalacionAcometidaOutRO);
                        } else if (!instalacionAcometidaOutRO.getResultCode().equalsIgnoreCase(gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS)) {
                            mInstalacionAcometidaOutRO.setMessage(instalacionAcometidaOutRO.getMessage() == null ? "Ocurrió un error inesperado" : instalacionAcometidaOutRO.getMessage());
                            mInstalacionAcometidaOutRO.setErrorCode("100");
                            mInstalacionAcometidaOutRODao.insertOrReplace(mInstalacionAcometidaOutRO);
                        } else {
//                            new BorrarFoto().execute(acometida.getIdInstalacionAcometida().toString());
                            List<Observacion> observacions = observacionDao.queryBuilder()
                                    .where(ObservacionDao.Properties.IdInstalacionMontante.eq(acometida.getIdInstalacionAcometida())).list();
                            observacionDao.deleteInTx(observacions);
                            List<Precision> precisiones = precisionDao.queryBuilder()
                                    .where(PrecisionDao.Properties.Estado.eq("3"),
                                            PrecisionDao.Properties.IdSolicitud.eq(acometida.getIdInstalacionAcometida())).list();
                            precisionDao.deleteInTx(precisiones);

                            acometidaDao.delete(acometida);
                            mInstalacionAcometidaOutRODao.delete(mInstalacionAcometidaOutRO);
                        }
                        cont--;
                        if (cont == 0) {
                            stopSelf();
                        }
                    }
                }, getApplicationContext()
                );
                grabarAcometida.execute(acometida);
            }
        }
        if (sync) {
            stopSelf();
        }

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        config.setSync(false);
        if (sinWifi) {
            config.setOffline(true);
        } else {
            config.setDesca(true);
            AlarmService.cancelActionAlarm(getApplicationContext());
        }
        configDao.insertOrReplace(config);
        Intent localIntent = new Intent(Constantes.ACTION_EXIT_SERVICE);
        localIntent.putExtra("tipo", "sync");
        localIntent.putExtra("sinWifi", sinWifi);
        LocalBroadcastManager.getInstance(SyncService.this).sendBroadcast(localIntent);
        if (sinWifi) {
            localIntent = new Intent(Constantes.ACTION_SERVICE);
            localIntent.putExtra("sinWifi", sinWifi);
            localIntent.putExtra("sync", true);
            LocalBroadcastManager.getInstance(SyncService.this).sendBroadcast(localIntent);
        }
        stopForeground(true);
        disposables.clear();
        Log.d(TAG, "SyncService destruido...");
    }

}
