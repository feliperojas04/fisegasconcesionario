package pe.gob.osinergmin.gnr.cgn.data.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "resultCode",
        "message",
        "errorCode",
        "idInstalacionAcometida",
        "idSolicitud",
        "codigoSolicitud",
        "documentoIdentificacionSolicitante",
        "nombreSolicitante",
        "direccionUbigeoPredio",
        "numeroSuministroPredio",
        "numeroContratoConcesionariaPredio",
        "tipoInstalacionAcometida",
        "nombreTipoInstalacionAcometida",
        "estadoInstalacionAcometida",
        "nombreEstadoInstalacionAcometida",
        "fechaUltimaModificacionInstalacionAcometida",
        "solicitud",
        "idInspector",
        "numeroIntentoInstalacionAcometida",
        "fechaProgramadaInstalacionAcometida",
        "fechaRegistroInstalacionAcometida",
        "fechaFinalizacionInstalacionAcometida",
        "idFusionistaRegistro",
        "documentoIdentificacionFusionistaRegistro",
        "nombreFusionistaRegistro",
        "resultadoInstalacionAcometida",
        "nombreResultadoInstalacionAcometida",
        "diametroTuberiaConexionInstalacionAcometida",
        "longitudTuberiaConexionInstalacionAcometida",
        "nombreMaterialInstalacion",
        "nombreFotoActaInstalacionTuberiaConexionAcometida",
        "nombreFotoTuberiaConexionTerminada",
        "nombreFotoGabineteManifoldTerminado",
        "observaciones",
        "tipoGabinete",
        "nombreTipoGabinete"
})
public class ResponseInstalacionAcometida {
    @JsonProperty("resultCode")
    private String resultCode;
    @JsonProperty("message")
    private String message;
    @JsonProperty("errorCode")
    private String errorCode;
    @JsonProperty("idInstalacionAcometida")
    private int idInstalacionAcometida;
    @JsonProperty("idSolicitud")
    private int idSolicitud;
    @JsonProperty("codigoSolicitud")
    private int codigoSolicitud;
    @JsonProperty("documentoIdentificacionSolicitante")
    private String documentoIdentificacionSolicitante;
    @JsonProperty("nombreSolicitante")
    private String nombreSolicitante;
    @JsonProperty("direccionUbigeoPredio")
    private String direccionUbigeoPredio;
    @JsonProperty("numeroSuministroPredio")
    private String numeroSuministroPredio;
    @JsonProperty("numeroContratoConcesionariaPredio")
    private String numeroContratoConcesionariaPredio;
    @JsonProperty("tipoInstalacionAcometida")
    private String tipoInstalacionAcometida;
    @JsonProperty("nombreTipoInstalacionAcometida")
    private String nombreTipoInstalacionAcometida;
    @JsonProperty("estadoInstalacionAcometida")
    private String estadoInstalacionAcometida;
    @JsonProperty("nombreEstadoInstalacionAcometida")
    private String nombreEstadoInstalacionAcometida;
    @JsonProperty("fechaUltimaModificacionInstalacionAcometida")
    private String fechaUltimaModificacionInstalacionAcometida;
    @JsonProperty("solicitud")
    private Solicitud solicitud;
    @JsonProperty("idInspector")
    private int idInspector;
    @JsonProperty("numeroIntentoInstalacionAcometida")
    private int numeroIntentoInstalacionAcometida;
    @JsonProperty("fechaProgramadaInstalacionAcometida")
    private String fechaProgramadaInstalacionAcometida;
    @JsonProperty("fechaRegistroInstalacionAcometida")
    private String fechaRegistroInstalacionAcometida;
    @JsonProperty("fechaFinalizacionInstalacionAcometida")
    private String fechaFinalizacionInstalacionAcometida;
    @JsonProperty("idFusionistaRegistro")
    private String idFusionistaRegistro;
    @JsonProperty("documentoIdentificacionFusionistaRegistro")
    private String documentoIdentificacionFusionistaRegistro;
    @JsonProperty("nombreFusionistaRegistro")
    private String nombreFusionistaRegistro;
    @JsonProperty("resultadoInstalacionAcometida")
    private String resultadoInstalacionAcometida;
    @JsonProperty("nombreResultadoInstalacionAcometida")
    private String nombreResultadoInstalacionAcometida;
    @JsonProperty("diametroTuberiaConexionInstalacionAcometida")
    private String diametroTuberiaConexionInstalacionAcometida;
    @JsonProperty("longitudTuberiaConexionInstalacionAcometida")
    private String longitudTuberiaConexionInstalacionAcometida;
    @JsonProperty("nombreMaterialInstalacion")
    private String nombreMaterialInstalacion;
    @JsonProperty("nombreFotoActaInstalacionTuberiaConexionAcometida")
    private String nombreFotoActaInstalacionTuberiaConexionAcometida;
    @JsonProperty("nombreFotoTuberiaConexionTerminada")
    private String nombreFotoTuberiaConexionTerminada;
    @JsonProperty("nombreFotoGabineteManifoldTerminado")
    private String nombreFotoGabineteManifoldTerminado;
    @JsonProperty("observaciones")
    private String observaciones;
    @JsonProperty("tipoGabinete")
    private String tipoGabinete;
    @JsonProperty("nombreTipoGabinete")
    private String nombreTipoGabinete;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("resultCode")
    public String getResultCode() {
        return resultCode;
    }

    @JsonProperty("resultCode")
    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("errorCode")
    public String getErrorCode() {
        return errorCode;
    }

    @JsonProperty("errorCode")
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    @JsonProperty("idInstalacionAcometida")
    public int getIdInstalacionAcometida() {
        return idInstalacionAcometida;
    }

    @JsonProperty("idInstalacionAcometida")
    public void setIdInstalacionAcometida(int idInstalacionAcometida) {
        this.idInstalacionAcometida = idInstalacionAcometida;
    }

    @JsonProperty("idSolicitud")
    public int getIdSolicitud() {
        return idSolicitud;
    }

    @JsonProperty("idSolicitud")
    public void setIdSolicitud(int idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    @JsonProperty("codigoSolicitud")
    public int getCodigoSolicitud() {
        return codigoSolicitud;
    }

    @JsonProperty("codigoSolicitud")
    public void setCodigoSolicitud(int codigoSolicitud) {
        this.codigoSolicitud = codigoSolicitud;
    }

    @JsonProperty("documentoIdentificacionSolicitante")
    public String getDocumentoIdentificacionSolicitante() {
        return documentoIdentificacionSolicitante;
    }

    @JsonProperty("documentoIdentificacionSolicitante")
    public void setDocumentoIdentificacionSolicitante(String documentoIdentificacionSolicitante) {
        this.documentoIdentificacionSolicitante = documentoIdentificacionSolicitante;
    }

    @JsonProperty("nombreSolicitante")
    public String getNombreSolicitante() {
        return nombreSolicitante;
    }

    @JsonProperty("nombreSolicitante")
    public void setNombreSolicitante(String nombreSolicitante) {
        this.nombreSolicitante = nombreSolicitante;
    }

    @JsonProperty("direccionUbigeoPredio")
    public String getDireccionUbigeoPredio() {
        return direccionUbigeoPredio;
    }

    @JsonProperty("direccionUbigeoPredio")
    public void setDireccionUbigeoPredio(String direccionUbigeoPredio) {
        this.direccionUbigeoPredio = direccionUbigeoPredio;
    }

    @JsonProperty("numeroSuministroPredio")
    public String getNumeroSuministroPredio() {
        return numeroSuministroPredio;
    }

    @JsonProperty("numeroSuministroPredio")
    public void setNumeroSuministroPredio(String numeroSuministroPredio) {
        this.numeroSuministroPredio = numeroSuministroPredio;
    }

    @JsonProperty("numeroContratoConcesionariaPredio")
    public String getNumeroContratoConcesionariaPredio() {
        return numeroContratoConcesionariaPredio;
    }

    @JsonProperty("numeroContratoConcesionariaPredio")
    public void setNumeroContratoConcesionariaPredio(String numeroContratoConcesionariaPredio) {
        this.numeroContratoConcesionariaPredio = numeroContratoConcesionariaPredio;
    }

    @JsonProperty("tipoInstalacionAcometida")
    public String getTipoInstalacionAcometida() {
        return tipoInstalacionAcometida;
    }

    @JsonProperty("tipoInstalacionAcometida")
    public void setTipoInstalacionAcometida(String tipoInstalacionAcometida) {
        this.tipoInstalacionAcometida = tipoInstalacionAcometida;
    }

    @JsonProperty("nombreTipoInstalacionAcometida")
    public String getNombreTipoInstalacionAcometida() {
        return nombreTipoInstalacionAcometida;
    }

    @JsonProperty("nombreTipoInstalacionAcometida")
    public void setNombreTipoInstalacionAcometida(String nombreTipoInstalacionAcometida) {
        this.nombreTipoInstalacionAcometida = nombreTipoInstalacionAcometida;
    }

    @JsonProperty("estadoInstalacionAcometida")
    public String getEstadoInstalacionAcometida() {
        return estadoInstalacionAcometida;
    }

    @JsonProperty("estadoInstalacionAcometida")
    public void setEstadoInstalacionAcometida(String estadoInstalacionAcometida) {
        this.estadoInstalacionAcometida = estadoInstalacionAcometida;
    }

    @JsonProperty("nombreEstadoInstalacionAcometida")
    public String getNombreEstadoInstalacionAcometida() {
        return nombreEstadoInstalacionAcometida;
    }

    @JsonProperty("nombreEstadoInstalacionAcometida")
    public void setNombreEstadoInstalacionAcometida(String nombreEstadoInstalacionAcometida) {
        this.nombreEstadoInstalacionAcometida = nombreEstadoInstalacionAcometida;
    }

    @JsonProperty("fechaUltimaModificacionInstalacionAcometida")
    public String getFechaUltimaModificacionInstalacionAcometida() {
        return fechaUltimaModificacionInstalacionAcometida;
    }

    @JsonProperty("fechaUltimaModificacionInstalacionAcometida")
    public void setFechaUltimaModificacionInstalacionAcometida(String fechaUltimaModificacionInstalacionAcometida) {
        this.fechaUltimaModificacionInstalacionAcometida = fechaUltimaModificacionInstalacionAcometida;
    }

    @JsonProperty("solicitud")
    public Solicitud getSolicitud() {
        return solicitud;
    }

    @JsonProperty("solicitud")
    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    @JsonProperty("idInspector")
    public int getIdInspector() {
        return idInspector;
    }

    @JsonProperty("idInspector")
    public void setIdInspector(int idInspector) {
        this.idInspector = idInspector;
    }

    @JsonProperty("numeroIntentoInstalacionAcometida")
    public int getNumeroIntentoInstalacionAcometida() {
        return numeroIntentoInstalacionAcometida;
    }

    @JsonProperty("numeroIntentoInstalacionAcometida")
    public void setNumeroIntentoInstalacionAcometida(int numeroIntentoInstalacionAcometida) {
        this.numeroIntentoInstalacionAcometida = numeroIntentoInstalacionAcometida;
    }

    @JsonProperty("fechaProgramadaInstalacionAcometida")
    public String getFechaProgramadaInstalacionAcometida() {
        return fechaProgramadaInstalacionAcometida;
    }

    @JsonProperty("fechaProgramadaInstalacionAcometida")
    public void setFechaProgramadaInstalacionAcometida(String fechaProgramadaInstalacionAcometida) {
        this.fechaProgramadaInstalacionAcometida = fechaProgramadaInstalacionAcometida;
    }

    @JsonProperty("fechaRegistroInstalacionAcometida")
    public String getFechaRegistroInstalacionAcometida() {
        return fechaRegistroInstalacionAcometida;
    }

    @JsonProperty("fechaRegistroInstalacionAcometida")
    public void setFechaRegistroInstalacionAcometida(String fechaRegistroInstalacionAcometida) {
        this.fechaRegistroInstalacionAcometida = fechaRegistroInstalacionAcometida;
    }

    @JsonProperty("fechaFinalizacionInstalacionAcometida")
    public Object getFechaFinalizacionInstalacionAcometida() {
        return fechaFinalizacionInstalacionAcometida;
    }

    @JsonProperty("fechaFinalizacionInstalacionAcometida")
    public void setFechaFinalizacionInstalacionAcometida(String fechaFinalizacionInstalacionAcometida) {
        this.fechaFinalizacionInstalacionAcometida = fechaFinalizacionInstalacionAcometida;
    }

    @JsonProperty("idFusionistaRegistro")
    public Object getIdFusionistaRegistro() {
        return idFusionistaRegistro;
    }

    @JsonProperty("idFusionistaRegistro")
    public void setIdFusionistaRegistro(String idFusionistaRegistro) {
        this.idFusionistaRegistro = idFusionistaRegistro;
    }

    @JsonProperty("documentoIdentificacionFusionistaRegistro")
    public String getDocumentoIdentificacionFusionistaRegistro() {
        return documentoIdentificacionFusionistaRegistro;
    }

    @JsonProperty("documentoIdentificacionFusionistaRegistro")
    public void setDocumentoIdentificacionFusionistaRegistro(String documentoIdentificacionFusionistaRegistro) {
        this.documentoIdentificacionFusionistaRegistro = documentoIdentificacionFusionistaRegistro;
    }

    @JsonProperty("nombreFusionistaRegistro")
    public String getNombreFusionistaRegistro() {
        return nombreFusionistaRegistro;
    }

    @JsonProperty("nombreFusionistaRegistro")
    public void setNombreFusionistaRegistro(String nombreFusionistaRegistro) {
        this.nombreFusionistaRegistro = nombreFusionistaRegistro;
    }

    @JsonProperty("resultadoInstalacionAcometida")
    public Object getResultadoInstalacionAcometida() {
        return resultadoInstalacionAcometida;
    }

    @JsonProperty("resultadoInstalacionAcometida")
    public void setResultadoInstalacionAcometida(String resultadoInstalacionAcometida) {
        this.resultadoInstalacionAcometida = resultadoInstalacionAcometida;
    }

    @JsonProperty("nombreResultadoInstalacionAcometida")
    public String getNombreResultadoInstalacionAcometida() {
        return nombreResultadoInstalacionAcometida;
    }

    @JsonProperty("nombreResultadoInstalacionAcometida")
    public void setNombreResultadoInstalacionAcometida(String nombreResultadoInstalacionAcometida) {
        this.nombreResultadoInstalacionAcometida = nombreResultadoInstalacionAcometida;
    }

    @JsonProperty("diametroTuberiaConexionInstalacionAcometida")
    public Object getDiametroTuberiaConexionInstalacionAcometida() {
        return diametroTuberiaConexionInstalacionAcometida;
    }

    @JsonProperty("diametroTuberiaConexionInstalacionAcometida")
    public void setDiametroTuberiaConexionInstalacionAcometida(String diametroTuberiaConexionInstalacionAcometida) {
        this.diametroTuberiaConexionInstalacionAcometida = diametroTuberiaConexionInstalacionAcometida;
    }

    @JsonProperty("longitudTuberiaConexionInstalacionAcometida")
    public Object getLongitudTuberiaConexionInstalacionAcometida() {
        return longitudTuberiaConexionInstalacionAcometida;
    }

    @JsonProperty("longitudTuberiaConexionInstalacionAcometida")
    public void setLongitudTuberiaConexionInstalacionAcometida(String longitudTuberiaConexionInstalacionAcometida) {
        this.longitudTuberiaConexionInstalacionAcometida = longitudTuberiaConexionInstalacionAcometida;
    }

    @JsonProperty("nombreMaterialInstalacion")
    public String getNombreMaterialInstalacion() {
        return nombreMaterialInstalacion;
    }

    @JsonProperty("nombreMaterialInstalacion")
    public void setNombreMaterialInstalacion(String nombreMaterialInstalacion) {
        this.nombreMaterialInstalacion = nombreMaterialInstalacion;
    }

    @JsonProperty("nombreFotoActaInstalacionTuberiaConexionAcometida")
    public Object getNombreFotoActaInstalacionTuberiaConexionAcometida() {
        return nombreFotoActaInstalacionTuberiaConexionAcometida;
    }

    @JsonProperty("nombreFotoActaInstalacionTuberiaConexionAcometida")
    public void setNombreFotoActaInstalacionTuberiaConexionAcometida(String nombreFotoActaInstalacionTuberiaConexionAcometida) {
        this.nombreFotoActaInstalacionTuberiaConexionAcometida = nombreFotoActaInstalacionTuberiaConexionAcometida;
    }

    @JsonProperty("nombreFotoTuberiaConexionTerminada")
    public Object getNombreFotoTuberiaConexionTerminada() {
        return nombreFotoTuberiaConexionTerminada;
    }

    @JsonProperty("nombreFotoTuberiaConexionTerminada")
    public void setNombreFotoTuberiaConexionTerminada(String nombreFotoTuberiaConexionTerminada) {
        this.nombreFotoTuberiaConexionTerminada = nombreFotoTuberiaConexionTerminada;
    }

    @JsonProperty("nombreFotoGabineteManifoldTerminado")
    public Object getNombreFotoGabineteManifoldTerminado() {
        return nombreFotoGabineteManifoldTerminado;
    }

    @JsonProperty("nombreFotoGabineteManifoldTerminado")
    public void setNombreFotoGabineteManifoldTerminado(String nombreFotoGabineteManifoldTerminado) {
        this.nombreFotoGabineteManifoldTerminado = nombreFotoGabineteManifoldTerminado;
    }

    @JsonProperty("observaciones")
    public Object getObservaciones() {
        return observaciones;
    }

    @JsonProperty("observaciones")
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @JsonProperty("tipoGabinete")
    public Object getTipoGabinete() {
        return tipoGabinete;
    }

    @JsonProperty("tipoGabinete")
    public void setTipoGabinete(String tipoGabinete) {
        this.tipoGabinete = tipoGabinete;
    }

    @JsonProperty("nombreTipoGabinete")
    public Object getNombreTipoGabinete() {
        return nombreTipoGabinete;
    }

    @JsonProperty("nombreTipoGabinete")
    public void setNombreTipoGabinete(String nombreTipoGabinete) {
        this.nombreTipoGabinete = nombreTipoGabinete;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
