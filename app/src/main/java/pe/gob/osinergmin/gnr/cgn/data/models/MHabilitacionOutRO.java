package pe.gob.osinergmin.gnr.cgn.data.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class MHabilitacionOutRO {

    @Id
    private Long idHabilitacion;
    private Integer idSolicitud;
    private Integer codigoSolicitud;
    private String documentoIdentificacionSolicitante;
    private String nombreSolicitante;
    private String direccionUbigeoPredio;
    private String numeroSuministroPredio;
    private String numeroContratoConcesionariaPredio;
    private String estadoHabilitacion;
    private String nombreEstadoHabilitacion;
    private String fechaUltimaModificacionHabilitacion;
    private Integer idInspector;
    private Integer numeroIntentoHabilitacion;
    private String fechaProgramadaHabilitacion;
    private String observacionProgramacionHabilitacion;
    private String fechaRegistroHabilitacion;
    private String aprobadoHabilitacion;
    private String nombreAprobadoHabilitacion;
    private String aprobadoMedidor;
    private String nombreAprobadoMedidor;
    private String nombreFotoUbicacionMedidor;
    private Integer idTipoAcometida;
    private String nombreTipoAcometida;
    private String aprobadoValvulaCorteGeneral;
    private String nombreAprobadoValvulaCorteGeneral;
    private String nombreFotoUbicacionValvulaCorteGeneral;
    private String diametroValvulaCorteGeneral;
    private String aprobadoTuberia;
    private String nombreAprobadoTuberia;
    private String nombreFoto1RecorridoTuberia;
    private String nombreFoto2RecorridoTuberia;
    private String nombreFoto3RecorridoTuberia;
    private String nombreFoto4RecorridoTuberia;
    private String tipoInstalacion;
    private String nombreTipoInstalacion;
    private Integer idMaterialInstalacion;
    private String nombreMaterialInstalacion;
    private String longitudTotalInstalacion;
    private String aprobadoPuntosInstalacion;
    private String nombreAprobadoPuntosInstalacion;
    private Integer numeroPuntosInstalacion;
    private String aprobadoAmbientes;
    private String nombreAprobadoAmbientes;
    private Integer numeroAmbientes;
    private String aprobadoPruebaMonoxido;
    private String nombreAprobadoPruebaMonoxido;
    private String aprobadoPresionArtefacto;
    private String nombreAprobadoPresionArtefacto;
    private String nombreFotoMedicionPresionManometro;
    private String valorPresionArtefacto;
    private String aprobadoHermeticidad;
    private String nombreAprobadoHermeticidad;
    private String nombreFotoMedicionHermeticidadManometro;
    private String presionOperacionHermeticidad;
    private String presionInicialPruebaHermeticidad;
    private String presionFinalPruebaHermeticidad;
    private String tiempoPruebaHermeticidad;
    private String resultCode;
    private String message;
    private String errorCode;

    @Generated(hash = 426716816)
    public MHabilitacionOutRO() {
    }

    public MHabilitacionOutRO(Long idHabilitacion) {
        this.idHabilitacion = idHabilitacion;
    }

    @Generated(hash = 221219941)
    public MHabilitacionOutRO(Long idHabilitacion, Integer idSolicitud, Integer codigoSolicitud, String documentoIdentificacionSolicitante, String nombreSolicitante, String direccionUbigeoPredio, String numeroSuministroPredio, String numeroContratoConcesionariaPredio, String estadoHabilitacion, String nombreEstadoHabilitacion, String fechaUltimaModificacionHabilitacion, Integer idInspector, Integer numeroIntentoHabilitacion, String fechaProgramadaHabilitacion, String observacionProgramacionHabilitacion, String fechaRegistroHabilitacion, String aprobadoHabilitacion, String nombreAprobadoHabilitacion, String aprobadoMedidor, String nombreAprobadoMedidor, String nombreFotoUbicacionMedidor, Integer idTipoAcometida, String nombreTipoAcometida, String aprobadoValvulaCorteGeneral, String nombreAprobadoValvulaCorteGeneral, String nombreFotoUbicacionValvulaCorteGeneral, String diametroValvulaCorteGeneral, String aprobadoTuberia, String nombreAprobadoTuberia, String nombreFoto1RecorridoTuberia, String nombreFoto2RecorridoTuberia, String nombreFoto3RecorridoTuberia, String nombreFoto4RecorridoTuberia, String tipoInstalacion, String nombreTipoInstalacion, Integer idMaterialInstalacion, String nombreMaterialInstalacion, String longitudTotalInstalacion, String aprobadoPuntosInstalacion, String nombreAprobadoPuntosInstalacion, Integer numeroPuntosInstalacion, String aprobadoAmbientes, String nombreAprobadoAmbientes, Integer numeroAmbientes, String aprobadoPruebaMonoxido, String nombreAprobadoPruebaMonoxido, String aprobadoPresionArtefacto, String nombreAprobadoPresionArtefacto, String nombreFotoMedicionPresionManometro, String valorPresionArtefacto, String aprobadoHermeticidad, String nombreAprobadoHermeticidad, String nombreFotoMedicionHermeticidadManometro, String presionOperacionHermeticidad, String presionInicialPruebaHermeticidad, String presionFinalPruebaHermeticidad, String tiempoPruebaHermeticidad, String resultCode, String message, String errorCode) {
        this.idHabilitacion = idHabilitacion;
        this.idSolicitud = idSolicitud;
        this.codigoSolicitud = codigoSolicitud;
        this.documentoIdentificacionSolicitante = documentoIdentificacionSolicitante;
        this.nombreSolicitante = nombreSolicitante;
        this.direccionUbigeoPredio = direccionUbigeoPredio;
        this.numeroSuministroPredio = numeroSuministroPredio;
        this.numeroContratoConcesionariaPredio = numeroContratoConcesionariaPredio;
        this.estadoHabilitacion = estadoHabilitacion;
        this.nombreEstadoHabilitacion = nombreEstadoHabilitacion;
        this.fechaUltimaModificacionHabilitacion = fechaUltimaModificacionHabilitacion;
        this.idInspector = idInspector;
        this.numeroIntentoHabilitacion = numeroIntentoHabilitacion;
        this.fechaProgramadaHabilitacion = fechaProgramadaHabilitacion;
        this.observacionProgramacionHabilitacion = observacionProgramacionHabilitacion;
        this.fechaRegistroHabilitacion = fechaRegistroHabilitacion;
        this.aprobadoHabilitacion = aprobadoHabilitacion;
        this.nombreAprobadoHabilitacion = nombreAprobadoHabilitacion;
        this.aprobadoMedidor = aprobadoMedidor;
        this.nombreAprobadoMedidor = nombreAprobadoMedidor;
        this.nombreFotoUbicacionMedidor = nombreFotoUbicacionMedidor;
        this.idTipoAcometida = idTipoAcometida;
        this.nombreTipoAcometida = nombreTipoAcometida;
        this.aprobadoValvulaCorteGeneral = aprobadoValvulaCorteGeneral;
        this.nombreAprobadoValvulaCorteGeneral = nombreAprobadoValvulaCorteGeneral;
        this.nombreFotoUbicacionValvulaCorteGeneral = nombreFotoUbicacionValvulaCorteGeneral;
        this.diametroValvulaCorteGeneral = diametroValvulaCorteGeneral;
        this.aprobadoTuberia = aprobadoTuberia;
        this.nombreAprobadoTuberia = nombreAprobadoTuberia;
        this.nombreFoto1RecorridoTuberia = nombreFoto1RecorridoTuberia;
        this.nombreFoto2RecorridoTuberia = nombreFoto2RecorridoTuberia;
        this.nombreFoto3RecorridoTuberia = nombreFoto3RecorridoTuberia;
        this.nombreFoto4RecorridoTuberia = nombreFoto4RecorridoTuberia;
        this.tipoInstalacion = tipoInstalacion;
        this.nombreTipoInstalacion = nombreTipoInstalacion;
        this.idMaterialInstalacion = idMaterialInstalacion;
        this.nombreMaterialInstalacion = nombreMaterialInstalacion;
        this.longitudTotalInstalacion = longitudTotalInstalacion;
        this.aprobadoPuntosInstalacion = aprobadoPuntosInstalacion;
        this.nombreAprobadoPuntosInstalacion = nombreAprobadoPuntosInstalacion;
        this.numeroPuntosInstalacion = numeroPuntosInstalacion;
        this.aprobadoAmbientes = aprobadoAmbientes;
        this.nombreAprobadoAmbientes = nombreAprobadoAmbientes;
        this.numeroAmbientes = numeroAmbientes;
        this.aprobadoPruebaMonoxido = aprobadoPruebaMonoxido;
        this.nombreAprobadoPruebaMonoxido = nombreAprobadoPruebaMonoxido;
        this.aprobadoPresionArtefacto = aprobadoPresionArtefacto;
        this.nombreAprobadoPresionArtefacto = nombreAprobadoPresionArtefacto;
        this.nombreFotoMedicionPresionManometro = nombreFotoMedicionPresionManometro;
        this.valorPresionArtefacto = valorPresionArtefacto;
        this.aprobadoHermeticidad = aprobadoHermeticidad;
        this.nombreAprobadoHermeticidad = nombreAprobadoHermeticidad;
        this.nombreFotoMedicionHermeticidadManometro = nombreFotoMedicionHermeticidadManometro;
        this.presionOperacionHermeticidad = presionOperacionHermeticidad;
        this.presionInicialPruebaHermeticidad = presionInicialPruebaHermeticidad;
        this.presionFinalPruebaHermeticidad = presionFinalPruebaHermeticidad;
        this.tiempoPruebaHermeticidad = tiempoPruebaHermeticidad;
        this.resultCode = resultCode;
        this.message = message;
        this.errorCode = errorCode;
    }

    public Long getIdHabilitacion() {
        return idHabilitacion;
    }

    public void setIdHabilitacion(Long idHabilitacion) {
        this.idHabilitacion = idHabilitacion;
    }

    public Integer getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(Integer idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public Integer getCodigoSolicitud() {
        return codigoSolicitud;
    }

    public void setCodigoSolicitud(Integer codigoSolicitud) {
        this.codigoSolicitud = codigoSolicitud;
    }

    public String getDocumentoIdentificacionSolicitante() {
        return documentoIdentificacionSolicitante;
    }

    public void setDocumentoIdentificacionSolicitante(String documentoIdentificacionSolicitante) {
        this.documentoIdentificacionSolicitante = documentoIdentificacionSolicitante;
    }

    public String getNombreSolicitante() {
        return nombreSolicitante;
    }

    public void setNombreSolicitante(String nombreSolicitante) {
        this.nombreSolicitante = nombreSolicitante;
    }

    public String getDireccionUbigeoPredio() {
        return direccionUbigeoPredio;
    }

    public void setDireccionUbigeoPredio(String direccionUbigeoPredio) {
        this.direccionUbigeoPredio = direccionUbigeoPredio;
    }

    public String getNumeroSuministroPredio() {
        return numeroSuministroPredio;
    }

    public void setNumeroSuministroPredio(String numeroSuministroPredio) {
        this.numeroSuministroPredio = numeroSuministroPredio;
    }

    public String getNumeroContratoConcesionariaPredio() {
        return numeroContratoConcesionariaPredio;
    }

    public void setNumeroContratoConcesionariaPredio(String numeroContratoConcesionariaPredio) {
        this.numeroContratoConcesionariaPredio = numeroContratoConcesionariaPredio;
    }

    public String getEstadoHabilitacion() {
        return estadoHabilitacion;
    }

    public void setEstadoHabilitacion(String estadoHabilitacion) {
        this.estadoHabilitacion = estadoHabilitacion;
    }

    public String getNombreEstadoHabilitacion() {
        return nombreEstadoHabilitacion;
    }

    public void setNombreEstadoHabilitacion(String nombreEstadoHabilitacion) {
        this.nombreEstadoHabilitacion = nombreEstadoHabilitacion;
    }

    public String getFechaUltimaModificacionHabilitacion() {
        return fechaUltimaModificacionHabilitacion;
    }

    public void setFechaUltimaModificacionHabilitacion(String fechaUltimaModificacionHabilitacion) {
        this.fechaUltimaModificacionHabilitacion = fechaUltimaModificacionHabilitacion;
    }

    public Integer getIdInspector() {
        return idInspector;
    }

    public void setIdInspector(Integer idInspector) {
        this.idInspector = idInspector;
    }

    public Integer getNumeroIntentoHabilitacion() {
        return numeroIntentoHabilitacion;
    }

    public void setNumeroIntentoHabilitacion(Integer numeroIntentoHabilitacion) {
        this.numeroIntentoHabilitacion = numeroIntentoHabilitacion;
    }

    public String getFechaProgramadaHabilitacion() {
        return fechaProgramadaHabilitacion;
    }

    public void setFechaProgramadaHabilitacion(String fechaProgramadaHabilitacion) {
        this.fechaProgramadaHabilitacion = fechaProgramadaHabilitacion;
    }

    public String getObservacionProgramacionHabilitacion() {
        return observacionProgramacionHabilitacion;
    }

    public void setObservacionProgramacionHabilitacion(String observacionProgramacionHabilitacion) {
        this.observacionProgramacionHabilitacion = observacionProgramacionHabilitacion;
    }

    public String getFechaRegistroHabilitacion() {
        return fechaRegistroHabilitacion;
    }

    public void setFechaRegistroHabilitacion(String fechaRegistroHabilitacion) {
        this.fechaRegistroHabilitacion = fechaRegistroHabilitacion;
    }

    public String getAprobadoHabilitacion() {
        return aprobadoHabilitacion;
    }

    public void setAprobadoHabilitacion(String aprobadoHabilitacion) {
        this.aprobadoHabilitacion = aprobadoHabilitacion;
    }

    public String getNombreAprobadoHabilitacion() {
        return nombreAprobadoHabilitacion;
    }

    public void setNombreAprobadoHabilitacion(String nombreAprobadoHabilitacion) {
        this.nombreAprobadoHabilitacion = nombreAprobadoHabilitacion;
    }

    public String getAprobadoMedidor() {
        return aprobadoMedidor;
    }

    public void setAprobadoMedidor(String aprobadoMedidor) {
        this.aprobadoMedidor = aprobadoMedidor;
    }

    public String getNombreAprobadoMedidor() {
        return nombreAprobadoMedidor;
    }

    public void setNombreAprobadoMedidor(String nombreAprobadoMedidor) {
        this.nombreAprobadoMedidor = nombreAprobadoMedidor;
    }

    public String getNombreFotoUbicacionMedidor() {
        return nombreFotoUbicacionMedidor;
    }

    public void setNombreFotoUbicacionMedidor(String nombreFotoUbicacionMedidor) {
        this.nombreFotoUbicacionMedidor = nombreFotoUbicacionMedidor;
    }

    public Integer getIdTipoAcometida() {
        return idTipoAcometida;
    }

    public void setIdTipoAcometida(Integer idTipoAcometida) {
        this.idTipoAcometida = idTipoAcometida;
    }

    public String getNombreTipoAcometida() {
        return nombreTipoAcometida;
    }

    public void setNombreTipoAcometida(String nombreTipoAcometida) {
        this.nombreTipoAcometida = nombreTipoAcometida;
    }

    public String getAprobadoValvulaCorteGeneral() {
        return aprobadoValvulaCorteGeneral;
    }

    public void setAprobadoValvulaCorteGeneral(String aprobadoValvulaCorteGeneral) {
        this.aprobadoValvulaCorteGeneral = aprobadoValvulaCorteGeneral;
    }

    public String getNombreAprobadoValvulaCorteGeneral() {
        return nombreAprobadoValvulaCorteGeneral;
    }

    public void setNombreAprobadoValvulaCorteGeneral(String nombreAprobadoValvulaCorteGeneral) {
        this.nombreAprobadoValvulaCorteGeneral = nombreAprobadoValvulaCorteGeneral;
    }

    public String getNombreFotoUbicacionValvulaCorteGeneral() {
        return nombreFotoUbicacionValvulaCorteGeneral;
    }

    public void setNombreFotoUbicacionValvulaCorteGeneral(String nombreFotoUbicacionValvulaCorteGeneral) {
        this.nombreFotoUbicacionValvulaCorteGeneral = nombreFotoUbicacionValvulaCorteGeneral;
    }

    public String getDiametroValvulaCorteGeneral() {
        return diametroValvulaCorteGeneral;
    }

    public void setDiametroValvulaCorteGeneral(String diametroValvulaCorteGeneral) {
        this.diametroValvulaCorteGeneral = diametroValvulaCorteGeneral;
    }

    public String getAprobadoTuberia() {
        return aprobadoTuberia;
    }

    public void setAprobadoTuberia(String aprobadoTuberia) {
        this.aprobadoTuberia = aprobadoTuberia;
    }

    public String getNombreAprobadoTuberia() {
        return nombreAprobadoTuberia;
    }

    public void setNombreAprobadoTuberia(String nombreAprobadoTuberia) {
        this.nombreAprobadoTuberia = nombreAprobadoTuberia;
    }

    public String getNombreFoto1RecorridoTuberia() {
        return nombreFoto1RecorridoTuberia;
    }

    public void setNombreFoto1RecorridoTuberia(String nombreFoto1RecorridoTuberia) {
        this.nombreFoto1RecorridoTuberia = nombreFoto1RecorridoTuberia;
    }

    public String getNombreFoto2RecorridoTuberia() {
        return nombreFoto2RecorridoTuberia;
    }

    public void setNombreFoto2RecorridoTuberia(String nombreFoto2RecorridoTuberia) {
        this.nombreFoto2RecorridoTuberia = nombreFoto2RecorridoTuberia;
    }

    public String getNombreFoto3RecorridoTuberia() {
        return nombreFoto3RecorridoTuberia;
    }

    public void setNombreFoto3RecorridoTuberia(String nombreFoto3RecorridoTuberia) {
        this.nombreFoto3RecorridoTuberia = nombreFoto3RecorridoTuberia;
    }

    public String getNombreFoto4RecorridoTuberia() {
        return nombreFoto4RecorridoTuberia;
    }

    public void setNombreFoto4RecorridoTuberia(String nombreFoto4RecorridoTuberia) {
        this.nombreFoto4RecorridoTuberia = nombreFoto4RecorridoTuberia;
    }

    public String getTipoInstalacion() {
        return tipoInstalacion;
    }

    public void setTipoInstalacion(String tipoInstalacion) {
        this.tipoInstalacion = tipoInstalacion;
    }

    public String getNombreTipoInstalacion() {
        return nombreTipoInstalacion;
    }

    public void setNombreTipoInstalacion(String nombreTipoInstalacion) {
        this.nombreTipoInstalacion = nombreTipoInstalacion;
    }

    public Integer getIdMaterialInstalacion() {
        return idMaterialInstalacion;
    }

    public void setIdMaterialInstalacion(Integer idMaterialInstalacion) {
        this.idMaterialInstalacion = idMaterialInstalacion;
    }

    public String getNombreMaterialInstalacion() {
        return nombreMaterialInstalacion;
    }

    public void setNombreMaterialInstalacion(String nombreMaterialInstalacion) {
        this.nombreMaterialInstalacion = nombreMaterialInstalacion;
    }

    public String getLongitudTotalInstalacion() {
        return longitudTotalInstalacion;
    }

    public void setLongitudTotalInstalacion(String longitudTotalInstalacion) {
        this.longitudTotalInstalacion = longitudTotalInstalacion;
    }

    public String getAprobadoPuntosInstalacion() {
        return aprobadoPuntosInstalacion;
    }

    public void setAprobadoPuntosInstalacion(String aprobadoPuntosInstalacion) {
        this.aprobadoPuntosInstalacion = aprobadoPuntosInstalacion;
    }

    public String getNombreAprobadoPuntosInstalacion() {
        return nombreAprobadoPuntosInstalacion;
    }

    public void setNombreAprobadoPuntosInstalacion(String nombreAprobadoPuntosInstalacion) {
        this.nombreAprobadoPuntosInstalacion = nombreAprobadoPuntosInstalacion;
    }

    public Integer getNumeroPuntosInstalacion() {
        return numeroPuntosInstalacion;
    }

    public void setNumeroPuntosInstalacion(Integer numeroPuntosInstalacion) {
        this.numeroPuntosInstalacion = numeroPuntosInstalacion;
    }

    public String getAprobadoAmbientes() {
        return aprobadoAmbientes;
    }

    public void setAprobadoAmbientes(String aprobadoAmbientes) {
        this.aprobadoAmbientes = aprobadoAmbientes;
    }

    public String getNombreAprobadoAmbientes() {
        return nombreAprobadoAmbientes;
    }

    public void setNombreAprobadoAmbientes(String nombreAprobadoAmbientes) {
        this.nombreAprobadoAmbientes = nombreAprobadoAmbientes;
    }

    public Integer getNumeroAmbientes() {
        return numeroAmbientes;
    }

    public void setNumeroAmbientes(Integer numeroAmbientes) {
        this.numeroAmbientes = numeroAmbientes;
    }

    public String getAprobadoPruebaMonoxido() {
        return aprobadoPruebaMonoxido;
    }

    public void setAprobadoPruebaMonoxido(String aprobadoPruebaMonoxido) {
        this.aprobadoPruebaMonoxido = aprobadoPruebaMonoxido;
    }

    public String getNombreAprobadoPruebaMonoxido() {
        return nombreAprobadoPruebaMonoxido;
    }

    public void setNombreAprobadoPruebaMonoxido(String nombreAprobadoPruebaMonoxido) {
        this.nombreAprobadoPruebaMonoxido = nombreAprobadoPruebaMonoxido;
    }

    public String getAprobadoPresionArtefacto() {
        return aprobadoPresionArtefacto;
    }

    public void setAprobadoPresionArtefacto(String aprobadoPresionArtefacto) {
        this.aprobadoPresionArtefacto = aprobadoPresionArtefacto;
    }

    public String getNombreAprobadoPresionArtefacto() {
        return nombreAprobadoPresionArtefacto;
    }

    public void setNombreAprobadoPresionArtefacto(String nombreAprobadoPresionArtefacto) {
        this.nombreAprobadoPresionArtefacto = nombreAprobadoPresionArtefacto;
    }

    public String getNombreFotoMedicionPresionManometro() {
        return nombreFotoMedicionPresionManometro;
    }

    public void setNombreFotoMedicionPresionManometro(String nombreFotoMedicionPresionManometro) {
        this.nombreFotoMedicionPresionManometro = nombreFotoMedicionPresionManometro;
    }

    public String getValorPresionArtefacto() {
        return valorPresionArtefacto;
    }

    public void setValorPresionArtefacto(String valorPresionArtefacto) {
        this.valorPresionArtefacto = valorPresionArtefacto;
    }

    public String getAprobadoHermeticidad() {
        return aprobadoHermeticidad;
    }

    public void setAprobadoHermeticidad(String aprobadoHermeticidad) {
        this.aprobadoHermeticidad = aprobadoHermeticidad;
    }

    public String getNombreAprobadoHermeticidad() {
        return nombreAprobadoHermeticidad;
    }

    public void setNombreAprobadoHermeticidad(String nombreAprobadoHermeticidad) {
        this.nombreAprobadoHermeticidad = nombreAprobadoHermeticidad;
    }

    public String getNombreFotoMedicionHermeticidadManometro() {
        return nombreFotoMedicionHermeticidadManometro;
    }

    public void setNombreFotoMedicionHermeticidadManometro(String nombreFotoMedicionHermeticidadManometro) {
        this.nombreFotoMedicionHermeticidadManometro = nombreFotoMedicionHermeticidadManometro;
    }

    public String getPresionOperacionHermeticidad() {
        return presionOperacionHermeticidad;
    }

    public void setPresionOperacionHermeticidad(String presionOperacionHermeticidad) {
        this.presionOperacionHermeticidad = presionOperacionHermeticidad;
    }

    public String getPresionInicialPruebaHermeticidad() {
        return presionInicialPruebaHermeticidad;
    }

    public void setPresionInicialPruebaHermeticidad(String presionInicialPruebaHermeticidad) {
        this.presionInicialPruebaHermeticidad = presionInicialPruebaHermeticidad;
    }

    public String getPresionFinalPruebaHermeticidad() {
        return presionFinalPruebaHermeticidad;
    }

    public void setPresionFinalPruebaHermeticidad(String presionFinalPruebaHermeticidad) {
        this.presionFinalPruebaHermeticidad = presionFinalPruebaHermeticidad;
    }

    public String getTiempoPruebaHermeticidad() {
        return tiempoPruebaHermeticidad;
    }

    public void setTiempoPruebaHermeticidad(String tiempoPruebaHermeticidad) {
        this.tiempoPruebaHermeticidad = tiempoPruebaHermeticidad;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

}
