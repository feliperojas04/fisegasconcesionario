package pe.gob.osinergmin.gnr.cgn.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import org.greenrobot.greendao.database.Database;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import pe.gob.osinergmin.gnr.cgn.data.models.AcometidaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Ambiente;
import pe.gob.osinergmin.gnr.cgn.data.models.AmbienteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.ConfigDao;
import pe.gob.osinergmin.gnr.cgn.data.models.DaoMaster;
import pe.gob.osinergmin.gnr.cgn.data.models.MBaseInspectorOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionMontanteOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MInstalacionAcometidaOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MObservacionInstalacionAcometidaOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MProyectoInstalacionOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MSolicitudOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MontanteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.ObservacionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.PrecisionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.PuntoDao;
import pe.gob.osinergmin.gnr.cgn.data.models.SuministroDao;
import pe.gob.osinergmin.gnr.cgn.data.models.TipoGabineteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.TiposInstalacionMontanteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.TiposMarcaAccesorioDao;
import pe.gob.osinergmin.gnr.cgn.data.models.TiposMarcaTuberiaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.TiposResultadoAcometidaDao;

public class DatabaseUpgradeHelper extends DaoMaster.OpenHelper {

    public DatabaseUpgradeHelper(Context context, String name) {
        super(context, name);
    }

    public DatabaseUpgradeHelper(Context context, String name, SQLiteDatabase.CursorFactory factory) {
        super(context, name, factory);
    }

    @Override
    public void onUpgrade(Database db, int oldVersion, int newVersion) {
        List<Migration> migrations = getMigrations();
        for (Migration migration : migrations) {
            if (oldVersion < migration.getVersion()) {
                migration.runMigration(db);
            }
        }
    }

    private List<Migration> getMigrations() {
        List<Migration> migrations = new ArrayList<>();
        migrations.add(new MigrationV2()); //Se agregar las versiones
        migrations.add(new MigrationV3());
        migrations.add(new MigrationV4());
        migrations.add(new MigrationV5());
        migrations.add(new MigrationV6());
        migrations.add(new MigrationV7());
        migrations.add(new MigrationV8());
        migrations.add(new MigrationV9());
        migrations.add(new MigrationV10());
        migrations.add(new MigrationV11());

        Comparator<Migration> migrationComparator = new Comparator<Migration>() {
            @Override
            public int compare(Migration m1, Migration m2) {
                return m1.getVersion().compareTo(m2.getVersion());
            }
        };
        Collections.sort(migrations, migrationComparator);

        return migrations;
    }

    private interface Migration {
        Integer getVersion();

        void runMigration(Database db);
    }

    private static class MigrationV2 implements Migration {

        @Override
        public Integer getVersion() {
            return 2;
        }

        @Override
        public void runMigration(Database db) {
            // Se agregara sentencias cuando se suba a la nueva version
            // UsuarioDao.createTable(db, false);
            db.execSQL("ALTER TABLE " + AmbienteDao.TABLENAME + " ADD COLUMN "
                    + AmbienteDao.Properties.MonoxidoFoto.columnName + " TEXT");
        }
    }

    private static class MigrationV3 implements Migration {

        @Override
        public Integer getVersion() {
            return 3;
        }

        @Override
        public void runMigration(Database db) {
            PrecisionDao.createTable(db, false);
            db.execSQL("ALTER TABLE " + ConfigDao.TABLENAME + " ADD COLUMN "
                    + ConfigDao.Properties.Precision.columnName + " TEXT DEFAULT 10000");
        }
    }

    private static class MigrationV4 implements Migration {

        @Override
        public Integer getVersion() {
            return 4;
        }

        @Override
        public void runMigration(Database db) {
            MHabilitacionMontanteOutRODao.createTable(db, false);
            MontanteDao.createTable(db, false);
            MProyectoInstalacionOutRODao.createTable(db, false);
            TiposInstalacionMontanteDao.createTable(db, false);
            db.execSQL("ALTER TABLE " + ConfigDao.TABLENAME + " ADD COLUMN "
                    + ConfigDao.Properties.IdHabilitacionMontante.columnName + " TEXT");
        }
    }

    private static class MigrationV5 implements Migration {

        @Override
        public Integer getVersion() {
            return 5;
        }

        @Override
        public void runMigration(Database db) {
            db.execSQL("ALTER TABLE " + ConfigDao.TABLENAME + " ADD COLUMN "
                    + ConfigDao.Properties.IdInstalacionAcometida.columnName + " TEXT");
            AcometidaDao.createTable(db, false);
            MBaseInspectorOutRODao.createTable(db, false);
            MInstalacionAcometidaOutRODao.createTable(db, false);
            MObservacionInstalacionAcometidaOutRODao.createTable(db, false);
            ObservacionDao.createTable(db, false);
            TiposResultadoAcometidaDao.createTable(db, false);
            db.execSQL("ALTER TABLE " + ConfigDao.TABLENAME + " ADD COLUMN "
                    + ConfigDao.Properties.DesAcometidas.columnName + " INTEGER");
            db.execSQL("ALTER TABLE " + MSolicitudOutRODao.TABLENAME + " ADD COLUMN "
                    + MSolicitudOutRODao.Properties.NombreEstadoTuberiaConexionAcometida.columnName + " TEXT");
            db.execSQL("ALTER TABLE " + MSolicitudOutRODao.TABLENAME + " ADD COLUMN "
                    + MSolicitudOutRODao.Properties.NombreTipoGabinete.columnName + " TEXT");
            db.execSQL("ALTER TABLE " + MSolicitudOutRODao.TABLENAME + " ADD COLUMN "
                    + MSolicitudOutRODao.Properties.NombreTipoAcometida.columnName + " TEXT");
        }
    }

    private static class MigrationV6 implements Migration {

        @Override
        public Integer getVersion() {
            return 6;
        }

        @Override
        public void runMigration(Database db) {
            db.execSQL("ALTER TABLE " + MHabilitacionMontanteOutRODao.TABLENAME + " ADD COLUMN "
                    + MHabilitacionMontanteOutRODao.Properties.NombreMontanteProyectoInstalacion.columnName + " TEXT DEFAULT ''");
        }
    }

    private static class MigrationV7 implements Migration {

        @Override
        public Integer getVersion() {
            return 7;
        }

        @Override
        public void runMigration(Database db) {
            db.execSQL("ALTER TABLE " + MontanteDao.TABLENAME + " ADD COLUMN "
                    + MontanteDao.Properties.NombreMontanteProyectoInstalacion.columnName + " TEXT DEFAULT ''");
        }
    }

    private static class MigrationV8 implements Migration {

        @Override
        public Integer getVersion() {
            return 8;
        }

        @Override
        public void runMigration(Database db) {
            db.execSQL("ALTER TABLE " + MInstalacionAcometidaOutRODao.TABLENAME + " ADD COLUMN "
                    + MInstalacionAcometidaOutRODao.Properties.NombreTipoInstalacionAcometida.columnName + " TEXT DEFAULT ''");
            db.execSQL("ALTER TABLE " + MInstalacionAcometidaOutRODao.TABLENAME + " ADD COLUMN "
                    + MInstalacionAcometidaOutRODao.Properties.TipoInstalacionAcometida.columnName + " TEXT DEFAULT ''");
            db.execSQL("ALTER TABLE " + AcometidaDao.TABLENAME + " ADD COLUMN "
                    + AcometidaDao.Properties.NombreTipoInstalacionAcometida.columnName + " TEXT DEFAULT ''");
            db.execSQL("ALTER TABLE " + AcometidaDao.TABLENAME + " ADD COLUMN "
                    + AcometidaDao.Properties.TipoInstalacionAcometida.columnName + " TEXT DEFAULT ''");
            db.execSQL("ALTER TABLE " + SuministroDao.TABLENAME + " ADD COLUMN "
                    + SuministroDao.Properties.SuministroHabilitacionFotoAprovado.columnName + " TEXT DEFAULT ''");
        }
    }

    private static class MigrationV9 implements Migration {

        @Override
        public Integer getVersion() {
            return 9;
        }

        @Override
        public void runMigration(Database db) {
            db.execSQL("ALTER TABLE " + AmbienteDao.TABLENAME + " ADD COLUMN "
                    + AmbienteDao.Properties.AmbienteUnoFoto.columnName + " TEXT DEFAULT ''");
            db.execSQL("ALTER TABLE " + AmbienteDao.TABLENAME + " ADD COLUMN "
                    + AmbienteDao.Properties.AmbienteDosFoto.columnName + " TEXT DEFAULT ''");
            db.execSQL("ALTER TABLE " + PuntoDao.TABLENAME + " ADD COLUMN "
                    + PuntoDao.Properties.PuntoFoto.columnName + " TEXT DEFAULT ''");
            db.execSQL("ALTER TABLE " + PuntoDao.TABLENAME + " ADD COLUMN "
                    + PuntoDao.Properties.Posicion.columnName + " INTEGER");
            db.execSQL("ALTER TABLE " + SuministroDao.TABLENAME + " ADD COLUMN "
                    + SuministroDao.Properties.AprobadoCapacitacionInstalador.columnName + " TEXT DEFAULT ''");
            db.execSQL("ALTER TABLE " + SuministroDao.TABLENAME + " ADD COLUMN "
                    + SuministroDao.Properties.IdMarcaAccesorio.columnName + " TEXT DEFAULT ''");
            db.execSQL("ALTER TABLE " + SuministroDao.TABLENAME + " ADD COLUMN "
                    + SuministroDao.Properties.IdMarcaTuberia.columnName + " TEXT DEFAULT ''");
            db.execSQL("ALTER TABLE " + SuministroDao.TABLENAME + " ADD COLUMN "
                    + SuministroDao.Properties.IsometricoFoto.columnName + " TEXT DEFAULT ''");
            TiposMarcaAccesorioDao.createTable(db, false);
            TiposMarcaTuberiaDao.createTable(db, false);
        }
    }

    private static class MigrationV10 implements Migration {

        @Override
        public Integer getVersion() {
            return 10;
        }

        @Override
        public void runMigration(Database db) {
            db.execSQL("ALTER TABLE " + SuministroDao.TABLENAME + " ADD COLUMN "
                    + SuministroDao.Properties.SuministroHabilitacionFotoActaFinalizacion.columnName + " TEXT DEFAULT ''");
            db.execSQL("ALTER TABLE " + AcometidaDao.TABLENAME + " ADD COLUMN "
                    + AcometidaDao.Properties.TipoGabinete.columnName + " TEXT DEFAULT ''");

        }
    }

    private static class MigrationV11 implements Migration {

        @Override
        public Integer getVersion() {
            return 11;
        }

        @Override
        public void runMigration(Database db) {
            db.execSQL("ALTER TABLE " + SuministroDao.TABLENAME + " ADD COLUMN "
                    + SuministroDao.Properties.SuministroHabilitacionFotoActaFinalizacion.columnName + " TEXT DEFAULT ''");
            db.execSQL("ALTER TABLE " + AcometidaDao.TABLENAME + " ADD COLUMN "
                    + AcometidaDao.Properties.TipoGabinete.columnName + " TEXT DEFAULT ''");

        }
    }
}
