package pe.gob.osinergmin.gnr.cgn.actividad;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.HabilitacionOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.MotivoRechazoHabilitacionOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListMotivoRechazoHabilitacionOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.adaptador.RechazoAdaptador;
import pe.gob.osinergmin.gnr.cgn.data.models.AmbienteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MMotivoRechazoHabilitacionOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.PrecisionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.PuntoDao;
import pe.gob.osinergmin.gnr.cgn.data.models.RechazoDao;
import pe.gob.osinergmin.gnr.cgn.data.models.SuministroDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Ambiente;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MMotivoRechazoHabilitacionOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.Precision;
import pe.gob.osinergmin.gnr.cgn.data.models.Punto;
import pe.gob.osinergmin.gnr.cgn.data.models.Rechazo;
import pe.gob.osinergmin.gnr.cgn.data.models.Suministro;
import pe.gob.osinergmin.gnr.cgn.task.GrabarHabilitacion;
import pe.gob.osinergmin.gnr.cgn.task.TipoMotivoRechazo;
import pe.gob.osinergmin.gnr.cgn.util.BorrarFoto;
import pe.gob.osinergmin.gnr.cgn.util.DownTimer;
import pe.gob.osinergmin.gnr.cgn.util.FormatoFecha;
import pe.gob.osinergmin.gnr.cgn.util.MostrarFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.NetworkUtil;
import pe.gob.osinergmin.gnr.cgn.util.Parse;
import pe.gob.osinergmin.gnr.cgn.util.ReducirFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.Util;

public class RechazoActivity extends BaseActivity implements RechazoAdaptador.CallBack {

    private Suministro suministro;
    private Config config;
    private LinearLayout lnlRec_finales;
    private ImageView icoRec_acta;
    private ImageView imgRec_fotoActa;
    private ImageView icoRec_prueba;
    private ImageView imgRec_fotoPrueba;
    private ListView listaRechazos;
    private Button btnRec_rechazar;
    private Button btnRec_aceptar;
    private Button btnRec_cancelar;
    private List<Rechazo> rechazos;
    private Toolbar toolbar;
    private String tipo;
    private List<MotivoRechazoHabilitacionOutRO> totalMotivos = new ArrayList<>();
    private RechazoAdaptador rechazoAdaptador;
    private SuministroDao suministroDao;
    private AmbienteDao ambienteDao;
    private PuntoDao puntoDao;
    private RechazoDao rechazoDao;

    private DownTimer countDownTimer;
    private MHabilitacionOutRODao mHabilitacionOutRODao;
    private MParametroOutRODao mParametroOutRODao;
    private MMotivoRechazoHabilitacionOutRODao mMotivoRechazoHabilitacionOutRODao;
    private String tmpRuta;
    private List<Integer> select = new ArrayList<>();
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private PrecisionDao precisionDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rechazo);

        countDownTimer = DownTimer.getInstance();

        suministroDao = ((App) getApplication()).getDaoSession().getSuministroDao();
        ambienteDao = ((App) getApplication()).getDaoSession().getAmbienteDao();
        puntoDao = ((App) getApplication()).getDaoSession().getPuntoDao();
        mHabilitacionOutRODao = ((App) getApplication()).getDaoSession().getMHabilitacionOutRODao();
        mParametroOutRODao = ((App) getApplication()).getDaoSession().getMParametroOutRODao();
        rechazoDao = ((App) getApplication()).getDaoSession().getRechazoDao();
        mMotivoRechazoHabilitacionOutRODao = ((App) getApplication()).getDaoSession().getMMotivoRechazoHabilitacionOutRODao();
        precisionDao = ((App) getApplication()).getDaoSession().getPrecisionDao();

        toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_36dp);

        suministro = getIntent().getExtras().getParcelable("SUMINISTRO");
        config = getIntent().getExtras().getParcelable("CONFIG");
        tipo = getIntent().getExtras().getString("TIPO");

        lnlRec_finales = findViewById(R.id.lnlRec_finales);
        icoRec_acta = findViewById(R.id.icoRec_acta);
        imgRec_fotoActa = findViewById(R.id.imgRec_fotoActa);
        icoRec_prueba = findViewById(R.id.icoRec_prueba);
        imgRec_fotoPrueba = findViewById(R.id.imgRec_fotoPrueba);
        ImageView addRec_rechazos = findViewById(R.id.addRec_rechazos);
        listaRechazos = findViewById(R.id.listaRechazos);
        btnRec_rechazar = findViewById(R.id.btnRec_rechazar);
        btnRec_aceptar = findViewById(R.id.btnRec_aceptar);
        btnRec_cancelar = findViewById(R.id.btnRec_cancelar);

        imgRec_fotoActa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(
                        RechazoActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ((App) getApplication()).showToast("No puede realizar la acción porque no se ha dado el permiso de Almacenamiento");
                    return;
                }
                if (Util.isExternalStorageWritable()) {
                    if (Util.getAlbumStorageDir(RechazoActivity.this)) {
                        File file = Util.getNuevaRutaFoto(suministro.getIdHabilitacion().toString(), RechazoActivity.this);
                        tmpRuta = file.getPath();
//                        suministro.setSuministroHabilitacionFoto(file.getPath());
//                        suministroDao.insertOrReplace(suministro);
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        Uri outputUri = FileProvider.getUriForFile(RechazoActivity.this, getApplicationContext().getPackageName() + ".provider", file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            ClipData clip = ClipData.newUri(getContentResolver(), "Instalacion", outputUri);
                            intent.setClipData(clip);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        }
                        guardarRechazos();
                        startActivityForResult(intent, 1);
                    } else {
                        ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                    }
                } else {
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                }
            }
        });

        imgRec_fotoPrueba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(
                        RechazoActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ((App) getApplication()).showToast("No puede realizar la acción porque no se ha dado el permiso de Almacenamiento");
                    return;
                }
                if (Util.isExternalStorageWritable()) {
                    if (Util.getAlbumStorageDir(RechazoActivity.this)) {
                        File file = Util.getNuevaRutaFoto(suministro.getIdHabilitacion().toString(), RechazoActivity.this);
                        tmpRuta = file.getPath();
//                        suministro.setSuministroPruebaFoto(file.getPath());
//                        suministroDao.insertOrReplace(suministro);
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        Uri outputUri = FileProvider.getUriForFile(RechazoActivity.this, getApplicationContext().getPackageName() + ".provider", file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            ClipData clip = ClipData.newUri(getContentResolver(), "Instalacion", outputUri);
                            intent.setClipData(clip);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        }
                        guardarRechazos();
                        startActivityForResult(intent, 2);
                    } else {
                        ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                    }
                } else {
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                }
            }
        });

        addRec_rechazos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Rechazo rechazo = new Rechazo(null, suministro.getIdHabilitacion(), 0, 0, "");
                if (rechazos != null) {
                    rechazos.add(rechazo);
                }
                rechazoAdaptador.setRechazos(rechazos);
                Util.setListViewHeightBasedOnChildren(listaRechazos);
            }
        });

        btnRec_aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarRechazos();
                finish();
            }
        });

        btnRec_cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                ((App) getApplication()).getDaoSession().clear();
            }
        });

        btnRec_rechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(RechazoActivity.this);
                builder.setTitle(R.string.txtHab_mensajeConfirmacion);
                builder.setMessage(R.string.txtHab_descripcionMensajeRechazado);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.btnHab_si, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        suministro.setSuministroAprueba("0");
                        if (config.getOffline()) {
                            MHabilitacionOutRO mHabilitacionOutRO = mHabilitacionOutRODao.queryBuilder().where(MHabilitacionOutRODao.Properties.IdHabilitacion.eq(suministro.getIdHabilitacion())).limit(1).unique();
                            mHabilitacionOutRO.setResultCode("100");
                            suministro.setFechaAprobacion(FormatoFecha.getfecha());
                            suministro.setFechaRegistroOffline(FormatoFecha.getfecha());
                            suministro.setGrabar(true);
                            for (Rechazo rechazo : rechazos) {
                                rechazoDao.insertOrReplace(rechazo);
                            }
                            mHabilitacionOutRODao.insertOrReplace(mHabilitacionOutRO);
                            suministroDao.insertOrReplace(suministro);
                            AlertDialog.Builder builder = new AlertDialog.Builder(RechazoActivity.this);
                            builder.setTitle(R.string.txtHab_mensajeConfirmacion);
                            builder.setMessage(getResources().getString(R.string.txtHab_descripcionRegistroRechazado));
                            builder.setCancelable(false);
                            builder.setPositiveButton(R.string.btnHab_aceptar, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(RechazoActivity.this, BuscarSuministroActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                            builder.show();
                        } else {
                            if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                showProgressDialog(R.string.app_procesando);
                                guardarRechazos();
                                GrabarHabilitacion grabarHabilitacion = new GrabarHabilitacion(
                                        new GrabarHabilitacion.OnGrabarHabilitacionAsyncTaskCompleted() {
                                            @Override
                                            public void onGrabarHabilitacionAsyncTaskCompleted(HabilitacionOutRO habilitacionOutRO) {
                                                hideProgressDialog();
                                                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                                                } else if (habilitacionOutRO == null) {
                                                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                                                } else if (!habilitacionOutRO.getResultCode().equalsIgnoreCase(Constantes.RESULTADO_SUCCESS)) {
                                                    ((App) getApplication()).showToast(habilitacionOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : habilitacionOutRO.getMessage());
                                                } else {
                                                    new BorrarFoto().execute(suministro.getIdHabilitacion().toString());
                                                    final AlertDialog.Builder builder = new AlertDialog.Builder(RechazoActivity.this);
                                                    builder.setTitle(R.string.txtHab_mensajeConfirmacion);
                                                    if (suministro.getSuministroAprueba().equalsIgnoreCase("1")) {
                                                        builder.setMessage(getResources().getString(R.string.txtHab_descripcionRegistro));
                                                    } else {
                                                        builder.setMessage(getResources().getString(R.string.txtHab_descripcionRegistroRechazado));
                                                    }
                                                    builder.setCancelable(false);
                                                    builder.setPositiveButton(R.string.btnHab_aceptar, new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            List<Punto> puntos = puntoDao.queryBuilder()
                                                                    .where(PuntoDao
                                                                            .Properties
                                                                            .IdHabilitacion
                                                                            .eq(suministro.getIdHabilitacion()))
                                                                    .list();
                                                            puntoDao.deleteInTx(puntos);

                                                            List<Ambiente> ambientes = ambienteDao.queryBuilder()
                                                                    .where(AmbienteDao
                                                                            .Properties
                                                                            .IdHabilitacion
                                                                            .eq(suministro.getIdHabilitacion()))
                                                                    .list();
                                                            ambienteDao.deleteInTx(ambientes);

                                                            List<Rechazo> rechazos = rechazoDao
                                                                    .queryBuilder()
                                                                    .where(RechazoDao
                                                                            .Properties
                                                                            .IdHabilitacion
                                                                            .eq(suministro.getIdHabilitacion()))
                                                                    .list();
                                                            rechazoDao.deleteInTx(rechazos);
                                                            List<Precision> precisiones = precisionDao.queryBuilder()
                                                                    .where(PrecisionDao.Properties.Estado.eq("1"),
                                                                            PrecisionDao.Properties.IdSolicitud.eq(suministro.getIdHabilitacion())).list();
                                                            precisionDao.deleteInTx(precisiones);

                                                            suministroDao.delete(suministro);
                                                            Intent intent = new Intent(RechazoActivity.this, BuscarSuministroActivity.class);
                                                            intent.putExtra("CONFIG", config);
                                                            startActivity(intent);
                                                            finish();
                                                        }
                                                    });
                                                    builder.show();
                                                }
                                            }
                                        }, getApplicationContext()
                                );
                                grabarHabilitacion.execute(suministro);
                            } else {
                                ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                            }
                        }
                    }
                });
                builder.setNegativeButton(R.string.btnHab_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    Precision precision = new Precision();
                    precision.setIdSolicitud(suministro.getIdHabilitacion());
                    precision.setLatitud(String.valueOf(location.getLatitude()));
                    precision.setLongitud(String.valueOf(location.getLongitude()));
                    precision.setPrecision(String.valueOf(location.getAccuracy()));
                    precision.setEstado("1");
                    precisionDao.insertOrReplace(precision);
                }
            }
        };
        aceptarPermiso();
    }

    private void guardarRechazos() {
        List<Rechazo> rechazosTMP = rechazoDao.queryBuilder()
                .where(RechazoDao.Properties.IdHabilitacion.eq(suministro.getIdHabilitacion())).list();
        rechazoDao.deleteInTx(rechazosTMP);
        for (Rechazo rechazo : rechazos) {
            rechazoDao.insertOrReplace(rechazo);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.createAndStart();
        createLocationRequest();
        if ("general".equalsIgnoreCase(tipo)) {
            toolbar.setTitle(suministro.getSuministro() + " - Rechazo");
            btnRec_aceptar.setVisibility(View.GONE);
            btnRec_cancelar.setVisibility(View.GONE);
            btnRec_rechazar.setVisibility(View.VISIBLE);
            lnlRec_finales.setVisibility(View.VISIBLE);
        } else {
            toolbar.setTitle("Selección de motivo de rechazo");
            btnRec_aceptar.setVisibility(View.VISIBLE);
            btnRec_cancelar.setVisibility(View.VISIBLE);
            btnRec_rechazar.setVisibility(View.GONE);
            lnlRec_finales.setVisibility(View.GONE);
        }
        if (config.getOffline()) {
            totalMotivos.clear();
            List<MMotivoRechazoHabilitacionOutRO> mMotivoRechazoHabilitacionOutROS = mMotivoRechazoHabilitacionOutRODao.queryBuilder().list();
            for (MMotivoRechazoHabilitacionOutRO mMotivoRechazoHabilitacionOutRO : mMotivoRechazoHabilitacionOutROS) {
                totalMotivos.add(Parse.getMotivoRechazoHabilitacionOutRO(mMotivoRechazoHabilitacionOutRO));
            }
            MParametroOutRO mParametroOutRO = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_CONFIGURACION_COMPRESION_FOTOS_APLICATIVO_MOVIL))
                    .limit(1).unique();
            config.setParametro5(mParametroOutRO == null ? "800x600" : mParametroOutRO.getValorParametro());
            MParametroOutRO mParametroOutPrecision = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_FRECUENCIA_TOMA_COORDENADAS_AUDITORIA))
                    .limit(1).unique();
            config.setPrecision(mParametroOutPrecision == null ? "10000" : mParametroOutPrecision.getValorParametro());
            cargaDatos();
        } else {
            if (totalMotivos.size() > 0 && !"".equalsIgnoreCase(config.getParametro5())) {
                cargaDatos();
            } else {
                if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    showProgressDialog(R.string.app_cargando);
                    TipoMotivoRechazo tipoMotivoRechazo = new TipoMotivoRechazo(new TipoMotivoRechazo.OnTipoMotivoRechazoCompleted() {
                        @Override
                        public void onTipoMotivoRechazoCompled(ListMotivoRechazoHabilitacionOutRO listMotivoRechazoHabilitacionOutRO) {
                            hideProgressDialog();
                            if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                            } else if (listMotivoRechazoHabilitacionOutRO == null) {
                                ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                            } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listMotivoRechazoHabilitacionOutRO.getResultCode())) {
                                ((App) getApplication()).showToast(listMotivoRechazoHabilitacionOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listMotivoRechazoHabilitacionOutRO.getMessage());
                            } else {
                                totalMotivos = listMotivoRechazoHabilitacionOutRO.getMotivoRechazoHabilitacion();
                                if ("".equalsIgnoreCase(config.getParametro5())) {
                                    config.setParametro5("800x600");
                                }
                                if ("".equalsIgnoreCase(config.getPrecision())) {
                                    config.setPrecision("10000");
                                }
                                cargaDatos();
                            }
                        }
                    });
                    tipoMotivoRechazo.execute(config);
                } else {
                    ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                }
            }
        }
    }

    private void cargaDatos() {
        rechazos = rechazoDao.queryBuilder()
                .where(RechazoDao.Properties.IdHabilitacion.eq(suministro.getIdHabilitacion())).list();
        if (rechazos.size() > 0) {
            rechazoAdaptador = new RechazoAdaptador(this);
            rechazoAdaptador.setRechazos(rechazos);
            rechazoAdaptador.setListaRechazos(listaRechazos);
            rechazoAdaptador.setCallback(this);
            rechazoAdaptador.setListaTotalMotivos(totalMotivos);
            listaRechazos.setAdapter(rechazoAdaptador);
            Util.setListViewHeightBasedOnChildren(listaRechazos);
        } else {
            rechazos = new ArrayList<>();
            Rechazo rechazo = new Rechazo(null, suministro.getIdHabilitacion(), 0, 0, "");
            rechazos.add(rechazo);
            rechazoAdaptador = new RechazoAdaptador(this);
            rechazoAdaptador.setRechazos(rechazos);
            rechazoAdaptador.setListaRechazos(listaRechazos);
            rechazoAdaptador.setCallback(this);
            rechazoAdaptador.setListaTotalMotivos(totalMotivos);
            listaRechazos.setAdapter(rechazoAdaptador);
            Util.setListViewHeightBasedOnChildren(listaRechazos);
        }
        if (!("".equalsIgnoreCase(suministro.getSuministroHabilitacionFoto()))) {
            loadBitmap(suministro.getSuministroHabilitacionFoto(), imgRec_fotoActa);
            icoRec_acta.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if (!("".equalsIgnoreCase(suministro.getSuministroPruebaFoto()))) {
            loadBitmap(suministro.getSuministroPruebaFoto(), imgRec_fotoPrueba);
            icoRec_prueba.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        onCheck();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_general, menu);
        MenuItem menuMas = menu.findItem(R.id.menuMas);
        menuMas.setVisible(false);
        MenuItem menuMenos = menu.findItem(R.id.menuMenos);
        menuMenos.setVisible(false);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1458) {
            if (resultCode == RESULT_CANCELED) {
                Intent intent = new Intent(RechazoActivity.this, BuscarSuministroActivity.class);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
            }
        }
        if (requestCode == 1) {
            if (resultCode != RESULT_CANCELED) {
                suministro.setSuministroHabilitacionFoto(tmpRuta);
                ReducirFotoTask reducirFotoTask = new ReducirFotoTask(new ReducirFotoTask.OnReducirFotoTaskCompleted() {
                    @Override
                    public void onReducirFotoTaskCompleted(String result) {
                        if (!("1".equalsIgnoreCase(result))) {
                            ((App) getApplication()).showToast(result);
                        }
                    }
                });
                suministroDao.insertOrReplace(suministro);
                reducirFotoTask.execute(suministro.getSuministroHabilitacionFoto(), config.getParametro5());
            }
        }
        if (requestCode == 2) {
            if (resultCode != RESULT_CANCELED) {
                suministro.setSuministroPruebaFoto(tmpRuta);
                ReducirFotoTask reducirFotoTask = new ReducirFotoTask(new ReducirFotoTask.OnReducirFotoTaskCompleted() {
                    @Override
                    public void onReducirFotoTaskCompleted(String result) {
                        if (!("1".equalsIgnoreCase(result))) {
                            ((App) getApplication()).showToast(result);
                        }
                    }
                });
                suministroDao.insertOrReplace(suministro);
                reducirFotoTask.execute(suministro.getSuministroPruebaFoto(), config.getParametro5());
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        ((App) getApplication()).getDaoSession().clear();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        countDownTimer.createAndStart();
    }

    private boolean addSelect(int id) {
        if (id != 0) {
            if (!select.contains(id)) {
                select.add(id);
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    @Override
    public void onCheck() {
        int i = 0;
        select.clear();
        boolean boton = true;
        while (i < rechazos.size()) {
            Rechazo rechazo = rechazos.get(i);
            if (addSelect(rechazo.getIdMotivoSubRechazo())) {
                ((App) getApplication()).showToast("Un submotivo de rechazo ya ha sido seleccionado más de una vez.");
                boton = false;
                i = rechazos.size();
            }
            if (0 == rechazo.getIdMotivoRechazo()) {
                boton = false;
                i = rechazos.size();
            }
            if (0 == rechazo.getIdMotivoSubRechazo()) {
                boton = false;
                i = rechazos.size();
            }
            i++;
        }
        if ("general".equalsIgnoreCase(tipo)) {
            if (!(rechazos.size() > 0)) {
                boton = false;
            }
            if ("".equalsIgnoreCase(suministro.getSuministroHabilitacionFoto())) {
                boton = false;
            }
            btnRec_rechazar.setEnabled(boton);
        } else {
            btnRec_aceptar.setEnabled(boton);
        }
    }

    public void loadBitmap(String ruta, final ImageView imageView) {
        MostrarFotoTask mostrarFotoTask = new MostrarFotoTask(new MostrarFotoTask.OnMostrarFotoTaskCompleted() {
            @Override
            public void onMostrarFotoTaskCompleted(Bitmap bitmap) {
                if (bitmap != null) {
                    if (imageView != null) {
                        imageView.setImageBitmap(bitmap);
                    }
                } else {
                    ((App) getApplication()).showToast("Error al cargar la foto");
                }
            }
        });
        mostrarFotoTask.execute(ruta);
    }

    private void createLocationRequest() {
        int value = 10000;
        try {
            value = Integer.valueOf(config.getPrecision());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(value);
        mLocationRequest.setFastestInterval(value);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(RechazoActivity.this, 1458);
                    } catch (IntentSender.SendIntentException sendEx) {
                    }
                }
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            aceptarPermiso();
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
    }

    private void aceptarPermiso() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permiso = Util.getPermisos(RechazoActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION);
            if (permiso != null) {
                requestPermissions(permiso, pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION:
                String message = Util.onRequestPermissionsResult(permissions, grantResults);
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }
}
