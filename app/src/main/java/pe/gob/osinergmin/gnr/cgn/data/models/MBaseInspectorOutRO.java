package pe.gob.osinergmin.gnr.cgn.data.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class MBaseInspectorOutRO {
    @Id
    private Long idInspector;
    private String nombreInspector;
    private String nombreDocumentoIdentificacionInspector;
    private String resultCode;
    private String message;
    private String errorCode;

    @Generated(hash = 240395344)
    public MBaseInspectorOutRO() {
    }

    public MBaseInspectorOutRO(Long idInspector) {
        this.idInspector = idInspector;
    }

    @Generated(hash = 2126615663)
    public MBaseInspectorOutRO(Long idInspector, String nombreInspector, String nombreDocumentoIdentificacionInspector, String resultCode, String message, String errorCode) {
        this.idInspector = idInspector;
        this.nombreInspector = nombreInspector;
        this.nombreDocumentoIdentificacionInspector = nombreDocumentoIdentificacionInspector;
        this.resultCode = resultCode;
        this.message = message;
        this.errorCode = errorCode;
    }

    public Long getIdInspector() {
        return idInspector;
    }

    public void setIdInspector(Long idInspector) {
        this.idInspector = idInspector;
    }

    public String getNombreInspector() {
        return nombreInspector;
    }

    public void setNombreInspector(String nombreInspector) {
        this.nombreInspector = nombreInspector;
    }

    public String getNombreDocumentoIdentificacionInspector() {
        return nombreDocumentoIdentificacionInspector;
    }

    public void setNombreDocumentoIdentificacionInspector(String nombreDocumentoIdentificacionInspector) {
        this.nombreDocumentoIdentificacionInspector = nombreDocumentoIdentificacionInspector;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
