package pe.gob.osinergmin.gnr.cgn.adaptador;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import pe.gob.osinergmin.gnr.cgn.R;

public class VacioAdaptador extends RecyclerView.Adapter<VacioAdaptador.ViewHolder> {

    private String text = "";

    public VacioAdaptador(String text) {
        this.text = text;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_vacio, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txtVacio.setText(text);
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtVacio;

        ViewHolder(View v) {
            super(v);
            txtVacio = v.findViewById(R.id.txtVacio);
        }
    }
}
