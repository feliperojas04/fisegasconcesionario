package pe.gob.osinergmin.gnr.cgn.task;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import gob.osinergmin.gnr.domain.dto.rest.in.filter.FilterGenericBeanInRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListMotivoRechazoHabilitacionOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.util.Urls;

public class TipoMotivoRechazo extends AsyncTask<Config, Void, ListMotivoRechazoHabilitacionOutRO> {

    private OnTipoMotivoRechazoCompleted listener = null;

    public TipoMotivoRechazo(OnTipoMotivoRechazoCompleted listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected ListMotivoRechazoHabilitacionOutRO doInBackground(Config... configs) {
        try {

            Config config = configs[0];

            String url = Urls.getBase() + Urls.getListaMotivoRechazo();

            FilterGenericBeanInRO filterGenericBeanInRO = new FilterGenericBeanInRO();
            filterGenericBeanInRO.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Authorization", "Bearer " + config.getToken());

            HttpEntity<FilterGenericBeanInRO> httpEntity = new HttpEntity<>(filterGenericBeanInRO, httpHeaders);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            return restTemplate.postForObject(url, httpEntity, ListMotivoRechazoHabilitacionOutRO.class);

        } catch (RestClientException e) {
            Log.e("LISTAMOTIVO", e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(ListMotivoRechazoHabilitacionOutRO listMotivoRechazoHabilitacionOutRO) {
        super.onPostExecute(listMotivoRechazoHabilitacionOutRO);
        listener.onTipoMotivoRechazoCompled(listMotivoRechazoHabilitacionOutRO);
    }

    public interface OnTipoMotivoRechazoCompleted {
        void onTipoMotivoRechazoCompled(ListMotivoRechazoHabilitacionOutRO listMotivoRechazoHabilitacionOutRO);
    }
}
