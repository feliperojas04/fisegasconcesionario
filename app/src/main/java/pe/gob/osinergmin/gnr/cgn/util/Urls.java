package pe.gob.osinergmin.gnr.cgn.util;

public class Urls {

    public static boolean getSSL() {
        return true; //false: si es http , true: si es https
    }

    public static String getBase() {
        if (getSSL()) {
            String base = "http://54.210.5.199:8075/gnr-api/rest";
            //String base = "https://masigas.osinergmin.gob.pe/gnr-api/rest";
            //base = "http://ec2-54-201-231-139.us-west-2.compute.amazonaws.com:59092/gnr-api/rest";

            return base;
        } else {
            String base = "http://ec2-54-201-231-139.us-west-2.compute.amazonaws.com:59092/gnr-api/rest";
            return base;
        }
    }

    public static String getRecuperarContrasena() {
        String recuperarContrasena = "/mobile/forgotPasswordInspector";
        return recuperarContrasena;
    }

    public static String getMapa() {
        String mapa = "/mobile/parametro/obtener/urlMapaOffline";
        return mapa;
    }

    public static String getLogin() {
        String login = "/mobile/loginInspector";
        return login;
    }

    public static String getListaMotivoRechazo() {
        String rechazo = "/mobile/motivoRechazoHabilitacion/listarResumen";
        return rechazo;
    }

    public static String getLogout() {
        String logout = "/mobile/logout";
        return logout;
    }

    public static String getListaHabilitacion() {
        String listaHabilitacion = "/mobile/habilitacion/listarResumen";
        return listaHabilitacion;
    }

    public static String getListaHabilitacionAll() {
        String listaHabilitacionAll = "/mobile/habilitacion/listar";
        return listaHabilitacionAll;
    }

    public static String getHabilitacion() {
        String habilitacion = "/mobile/habilitacion/obtener";
        return habilitacion;
    }

    public static String getParametroAll() {
        String parametroAll = "/mobile/parametro/listarByInspector";
        return parametroAll;
    }

    public static String getParametro1() {
        String parametro1 = "/mobile/parametro/obtener/validacionActivaDistanciaInspectorPredio";
        return parametro1;
    }

    public static String getParametro2() {
        String parametro2 = "/mobile/parametro/obtener/distanciaMaximaInspectorPredio";
        return parametro2;
    }

    public static String getParametro3() {
        String parametro3 = "/mobile/parametro/obtener/solicitudActivaCodigoEspecialAcceso";
        return parametro3;
    }

    public static String getParametro4() {
        String parametro4 = "/mobile/parametro/obtener/telefonoSolicitudCodigoEspecialAcceso";
        return parametro4;
    }

    public static String getParametro5() {
        String parametro5 = "/mobile/parametro/obtener/configuracionCompresionFotosAplicativoMovil";
        return parametro5;
    }

    public static String getAccesoCodigoEspecial() {
        String accesoCodigoEspecial = "/mobile/accesoByCodigo";
        return accesoCodigoEspecial;
    }

    public static String getListaTipoAcometida() {
        String listaTipoAcometida = "/mobile/tipoAcometida/listarResumen";
        return listaTipoAcometida;
    }

    public static String getListarTiposInstalacion() {
        String listarTiposInstalacion = "/mobile/habilitacion/listarTiposInstalacion";
        return listarTiposInstalacion;
    }

    public static String getListarTiposMarcaAccesorio() {
        String listarTiposMarcaAccesorio = "/mobile/marcaItemInstalacion/accesorio/listarResumen";
        return listarTiposMarcaAccesorio;
    }

    public static String getListarTiposMarcaTuberia() {
        String listarTiposMarcaTuberia = "/mobile/marcaItemInstalacion/tuberia/listarResumen";
        return listarTiposMarcaTuberia;
    }

    public static String getListaMaterialInstalacion() {
        String listaMaterialInstalacion = "/mobile/materialInstalacion/listarResumen";
        return listaMaterialInstalacion;
    }

    public static String getListaGasodomestico() {
        String listaGasodomestico = "/mobile/gasodomestico/listarResumen";
        return listaGasodomestico;
    }

    public static String getListaAmbientePredio() {
        String listaAmbientePredio = "/mobile/ambientePredio/listarResumen";
        return listaAmbientePredio;
    }

    public static String getRegistrarHabilitacion() {
        String registrarHabilitacion = "/mobile/habilitacion/registrar";
        return registrarHabilitacion;
    }

    public static String getMontanteListarResumen() {
        String s = "/mobile/habilitacionMontante/listarResumen";
        return s;
    }

    public static String getMontanteListar() {
        String s = "/mobile/habilitacionMontante/listar";
        return s;
    }

    public static String getMontanteTiposInstalacion() {
        String s = "/mobile/habilitacionMontante/listarTiposInstalacion";
        return s;
    }

    public static String getMontanteObtener() {
        String s = "/mobile/habilitacionMontante/obtener";
        return s;
    }

    public static String getMontanteRegistrar() {
        String s = "/mobile/habilitacionMontante/registrar";
        return s;
    }

    public static String getAcometidaListarResumen() {
        String s = "/mobile/instalacionAcometida/listarResumen";
        return s;
    }

    public static String getAcometidaListar() {
        String s = "/mobile/instalacionAcometida/listar";
        return s;
    }

    public static String getAcometidaObtener() {
        String s = "/mobile/instalacionAcometida/obtener";
        return s;
    }

    public static String getTuberiaRegistrar() {
        String s = "/mobile/instalacionAcometida/registrarInstalacionTuberia";
        return s;
    }

    public static String getAcometidaRegistrar() {
        String s = "/mobile/instalacionAcometida/registrar";
        return s;
    }

    public static String getListaResultado() {
        String s = "/mobile/instalacionAcometida/listarResultadosInstalacionAcometida";
        return s;
    }

    public static String getListaAcometidaPendiente() {
        String s = "/mobile/tipoObservacion/instalacionAcometidaPendiente/listarResumen";
        return s;
    }

    public static String getListaAcometidaRechazada() {
        String s = "/mobile/tipoObservacion/instalacionAcometidaRechazada/listarResumen";
        return s;
    }

    public static String getListaFusionista() {
        String s = "/mobile/instalacionAcometida/listarFusionistas";
        return s;
    }
    public static String getMonitoreoGPS() {
        return "/mobile/monitoreo/registrarMonitoreo";
    }

    /*PAPU 2011 - 0*/
    public static String getListaTipoGabinete() {
        String s = "/mobile/instalacionAcometida/listarTipoGabinete";
        return s;
    }
    /*PAPU 2011 - 1*/

    public static String getListaMotivoRechazoMontante() {
        String rechazo = "/mobile/motivoRechazoHabilitacionMontante/listarResumen";
        return rechazo;
    }

    public static String getListaTipoGabineteporIdProyectoInstalacion() {
        String s = "/mobile/instalacionAcometida/listarGabinetePorIdProyectoInstalacion";
        return s;
    }
}
