package pe.gob.osinergmin.gnr.cgn.actividad;

import android.Manifest;
import android.content.ClipData;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;

import java.io.File;

import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.Precision;
import pe.gob.osinergmin.gnr.cgn.data.models.PrecisionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Suministro;
import pe.gob.osinergmin.gnr.cgn.data.models.SuministroDao;
import pe.gob.osinergmin.gnr.cgn.util.DownTimer;
import pe.gob.osinergmin.gnr.cgn.util.MostrarFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.ReducirFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.Util;

public class ArtefactoActivity extends AppCompatActivity {

    private Suministro suministro;
    private Config config;
    private TableLayout tablaArtefacto;
    private TextView txtDet_DocumentoPropietarioView;
    private TextView txtDet_nombrePropietarioView;
    private TextView txtDet_PredioView;
    private TextView txtDet_numeroIntHabilitacionView;
    private TextView txtDet_numeroSolicitudView;
    private TextView txtDet_numeroContratoView;
    private TextView txtDet_fechaSolicitudView;
    private TextView txtDet_fechaAprobacionView;
    private TextView txtDet_tipoProyectoView;
    private TextView txtDet_tipoInstalacionView;
    private TextView txtDet_numeroPuntosInstaacionView;
    private ImageView icoArtef_medicionFoto;
    private ImageView imgArtef_medicionFoto;
    private ImageView icoArtef_presion;
    private TextInputLayout ediArtef_presion;
    private SwitchCompat swiArtef_aprueba;
    private Button btnArtef_rechazar;
    private Toolbar toolbar;
    private MenuItem menuMas;
    private MenuItem menuMenos;
    private String tmpRuta;
    private SuministroDao suministroDao;
    private DownTimer countDownTimer;
    private MParametroOutRODao mParametroOutRODao;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private PrecisionDao precisionDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artefacto);

        countDownTimer = DownTimer.getInstance();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        suministroDao = ((App) getApplication()).getDaoSession().getSuministroDao();
        mParametroOutRODao = ((App) getApplication()).getDaoSession().getMParametroOutRODao();
        precisionDao = ((App) getApplication()).getDaoSession().getPrecisionDao();

        toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_36dp);

        suministro = getIntent().getExtras().getParcelable("SUMINISTRO");
        config = getIntent().getExtras().getParcelable("CONFIG");

        tablaArtefacto = findViewById(R.id.tablaArtefacto);
        txtDet_DocumentoPropietarioView = findViewById(R.id.txtDet_DocumentoPropietarioView);
        txtDet_nombrePropietarioView = findViewById(R.id.txtDet_nombrePropietarioView);
        txtDet_PredioView = findViewById(R.id.txtDet_PredioView);
        txtDet_numeroIntHabilitacionView = findViewById(R.id.txtDet_numeroIntHabilitacionView);
        txtDet_numeroSolicitudView = findViewById(R.id.txtDet_numeroSolicitudView);
        txtDet_numeroContratoView = findViewById(R.id.txtDet_numeroContratoView);
        txtDet_fechaSolicitudView = findViewById(R.id.txtDet_fechaSolicitudView);
        txtDet_fechaAprobacionView = findViewById(R.id.txtDet_fechaAprobacionView);
        txtDet_tipoProyectoView = findViewById(R.id.txtDet_tipoProyectoView);
        txtDet_tipoInstalacionView = findViewById(R.id.txtDet_tipoInstalacionView);
        txtDet_numeroPuntosInstaacionView = findViewById(R.id.txtDet_numeroPuntosInstaacionView);
        icoArtef_medicionFoto = findViewById(R.id.icoArtef_medicionFoto);
        imgArtef_medicionFoto = findViewById(R.id.imgArtef_medicionFoto);
        icoArtef_presion = findViewById(R.id.icoArtef_presion);
        ediArtef_presion = findViewById(R.id.ediArtef_presion);
        swiArtef_aprueba = findViewById(R.id.swiArtef_aprueba);
        Button btnArtef_evaluando = findViewById(R.id.btnArtef_evaluando);
        btnArtef_rechazar = findViewById(R.id.btnArtef_rechazar);

        imgArtef_medicionFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(
                        ArtefactoActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ((App) getApplication()).showToast("No puede realizar la acción porque no se ha dado el permiso de Almacenamiento");
                    return;
                }
                if (Util.isExternalStorageWritable()) {
                    if (Util.getAlbumStorageDir(ArtefactoActivity.this)) {
                        File file = Util.getNuevaRutaFoto(suministro.getIdHabilitacion().toString(), ArtefactoActivity.this);
                        tmpRuta = file.getPath();
//                        suministro.setArtefactoMedicionFoto(file.getPath());
//                        suministroDao.insertOrReplace(suministro);
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        Uri outputUri = FileProvider.getUriForFile(ArtefactoActivity.this, getApplicationContext().getPackageName() + ".provider", file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            ClipData clip = ClipData.newUri(getContentResolver(), "Instalacion", outputUri);
                            intent.setClipData(clip);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        }
                        startActivityForResult(intent, 2);
                    } else {
                        ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                    }
                } else {
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                }
            }
        });

        ediArtef_presion.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    if (Util.validarScaleAndPrecision(s.toString(), 2, 8)) {
                        suministro.setArtefactoPresion(s.toString());
                    } else {
                        ((App) getApplication()).showToast(String.format(getResources().getString(R.string.app_error_numero), 8, 2));
                        ediArtef_presion.getEditText().setText(suministro.getArtefactoPresion());
                        ediArtef_presion.getEditText().setSelection(ediArtef_presion.getEditText().getText().length());
                    }
                } else {
                    suministro.setArtefactoPresion("");
                }
                validarCheck();
            }
        });

        swiArtef_aprueba.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    suministro.setArtefactoAprueba("1");
                    btnArtef_rechazar.setVisibility(View.GONE);
                } else {
                    suministro.setArtefactoAprueba("0");
                    btnArtef_rechazar.setVisibility(View.VISIBLE);
                }
            }
        });

        btnArtef_evaluando.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suministroDao.insertOrReplace(suministro);
                Intent intent = new Intent(ArtefactoActivity.this, MonoxidoActivity.class);
                intent.putExtra("SUMINISTRO", suministro);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
            }
        });

        btnArtef_rechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ArtefactoActivity.this, RechazoActivity.class);
                intent.putExtra("SUMINISTRO", suministro);
                intent.putExtra("CONFIG", config);
                intent.putExtra("TIPO", "");
                startActivity(intent);
            }
        });
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    Precision precision = new Precision();
                    precision.setIdSolicitud(suministro.getIdHabilitacion());
                    precision.setLatitud(String.valueOf(location.getLatitude()));
                    precision.setLongitud(String.valueOf(location.getLongitude()));
                    precision.setPrecision(String.valueOf(location.getAccuracy()));
                    precision.setEstado("1");
                    precisionDao.insertOrReplace(precision);
                }
            }
        };
        aceptarPermiso();
    }

    private void validarCheck() {
        if ("".equalsIgnoreCase(suministro.getArtefactoPresion())) {
            icoArtef_presion.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            icoArtef_presion.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        validarSelector();
    }

    private void validarSelector() {
        if ("".equalsIgnoreCase(suministro.getArtefactoMedicionFoto()) || "".equalsIgnoreCase(suministro.getArtefactoPresion())) {
            swiArtef_aprueba.setChecked(false);
            swiArtef_aprueba.setClickable(false);
        } else {
            swiArtef_aprueba.setClickable(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1458) {
            if (resultCode == RESULT_CANCELED) {
                Intent intent = new Intent(ArtefactoActivity.this, BuscarSuministroActivity.class);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
            }
        }
        if (requestCode == 2) {
            if (resultCode != RESULT_CANCELED) {
                suministro.setArtefactoMedicionFoto(tmpRuta);
                ReducirFotoTask reducirFotoTask = new ReducirFotoTask(new ReducirFotoTask.OnReducirFotoTaskCompleted() {
                    @Override
                    public void onReducirFotoTaskCompleted(String result) {
                        if (!("1".equalsIgnoreCase(result))) {
                            ((App) getApplication()).showToast(result);
                        }
                    }
                });
                suministroDao.insertOrReplace(suministro);
                reducirFotoTask.execute(suministro.getArtefactoMedicionFoto(), config.getParametro5());
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.createAndStart();
        createLocationRequest();
        cargarSuministro();
        cargaDatos();
        toolbar.setTitle(suministro.getSuministro() + " - Prueba de presión del artefacto");
        validarCheck();
        if (config.getOffline()) {
            MParametroOutRO mParametroOutRO = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_CONFIGURACION_COMPRESION_FOTOS_APLICATIVO_MOVIL))
                    .limit(1)
                    .unique();
            config.setParametro5(mParametroOutRO == null ? "800x600" : mParametroOutRO.getValorParametro());
            MParametroOutRO mParametroOutPrecision = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_FRECUENCIA_TOMA_COORDENADAS_AUDITORIA))
                    .limit(1).unique();
            config.setPrecision(mParametroOutPrecision == null ? "10000" : mParametroOutPrecision.getValorParametro());
        } else {
            if ("".equalsIgnoreCase(config.getParametro5())) {
                config.setParametro5("800x600");
            }
            if ("".equalsIgnoreCase(config.getPrecision())) {
                config.setPrecision("10000");
            }
        }
    }

    private void cargaDatos() {
        if (!("".equalsIgnoreCase(suministro.getArtefactoMedicionFoto()))) {
            loadBitmap(suministro.getArtefactoMedicionFoto(), imgArtef_medicionFoto);
            icoArtef_medicionFoto.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if (!("".equalsIgnoreCase(suministro.getArtefactoPresion()))) {
            ediArtef_presion.getEditText().setText(suministro.getArtefactoPresion());
            ediArtef_presion.getEditText().setSelection(ediArtef_presion.getEditText().getText().length());
        }
        if (!("0".equalsIgnoreCase(suministro.getArtefactoAprueba()))) {
            swiArtef_aprueba.setChecked(true);
        }
    }

    public void loadBitmap(String ruta, final ImageView imageView) {
        MostrarFotoTask mostrarFotoTask = new MostrarFotoTask(new MostrarFotoTask.OnMostrarFotoTaskCompleted() {
            @Override
            public void onMostrarFotoTaskCompleted(Bitmap bitmap) {
                if (bitmap != null) {
                    if (imageView != null) {
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(120, 120);
                        layoutParams.setMargins(10, 10, 10, 10);
                        imageView.setLayoutParams(layoutParams);
                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        imageView.setImageBitmap(bitmap);
                    }
                } else {
                    ((App) getApplication()).showToast("Error al cargar la foto");
                }
            }
        });
        mostrarFotoTask.execute(ruta);
    }

    private void cargarSuministro() {
        txtDet_DocumentoPropietarioView.setText(suministro.getDocumentoPropietario());
        txtDet_nombrePropietarioView.setText(suministro.getNombrePropietario());
        txtDet_PredioView.setText(suministro.getPredio());
        txtDet_numeroIntHabilitacionView.setText(suministro.getNumeroIntHabilitacion());
        txtDet_numeroSolicitudView.setText(suministro.getNumeroSolicitud());
        txtDet_numeroContratoView.setText(suministro.getNumeroContrato());
        txtDet_fechaSolicitudView.setText(suministro.getFechaSolicitud());
        txtDet_fechaAprobacionView.setText(suministro.getFechaAprobacion());
        txtDet_tipoProyectoView.setText(suministro.getTipoProyecto());
        txtDet_tipoInstalacionView.setText(suministro.getTipoInstalacion());
        txtDet_numeroPuntosInstaacionView.setText(suministro.getNumeroPuntosInstaacion());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_general, menu);
        menuMas = menu.findItem(R.id.menuMas);
        menuMas.setVisible(true);
        menuMenos = menu.findItem(R.id.menuMenos);
        menuMenos.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                suministroDao.insertOrReplace(suministro);
                Intent intent = new Intent(ArtefactoActivity.this, HabilitacionActivity.class);
                intent.putExtra("SUMINISTRO", suministro);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
                return true;
            case R.id.menuMas:
                tablaArtefacto.setVisibility(View.VISIBLE);
                menuMas.setVisible(false);
                menuMenos.setVisible(true);
                return true;

            case R.id.menuMenos:
                tablaArtefacto.setVisibility(View.GONE);
                menuMas.setVisible(true);
                menuMenos.setVisible(false);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        suministroDao.insertOrReplace(suministro);
        Intent intent = new Intent(ArtefactoActivity.this, HabilitacionActivity.class);
        intent.putExtra("SUMINISTRO", suministro);
        intent.putExtra("CONFIG", config);
        startActivity(intent);
        finish();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        countDownTimer.createAndStart();
    }

    private void createLocationRequest() {
        int value = 10000;
        try {
            value = Integer.valueOf(config.getPrecision());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(value);
        mLocationRequest.setFastestInterval(value);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(ArtefactoActivity.this, 1458);
                    } catch (IntentSender.SendIntentException sendEx) {
                    }
                }
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            aceptarPermiso();
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
    }

    private void aceptarPermiso() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permiso = Util.getPermisos(ArtefactoActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION);
            if (permiso != null) {
                requestPermissions(permiso, pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION:
                String message = Util.onRequestPermissionsResult(permissions, grantResults);
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }
}
