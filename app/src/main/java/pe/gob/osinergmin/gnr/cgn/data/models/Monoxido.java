package pe.gob.osinergmin.gnr.cgn.data.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class Monoxido {

    @Id
    private Long id;
    private Integer idHabilitacion;
    private String suministro;
    private String monoxidoMedicion;

    @Generated(hash = 1452969067)
    public Monoxido() {
    }

    public Monoxido(Long id) {
        this.id = id;
    }

    @Generated(hash = 172927017)
    public Monoxido(Long id, Integer idHabilitacion, String suministro, String monoxidoMedicion) {
        this.id = id;
        this.idHabilitacion = idHabilitacion;
        this.suministro = suministro;
        this.monoxidoMedicion = monoxidoMedicion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdHabilitacion() {
        return idHabilitacion;
    }

    public void setIdHabilitacion(Integer idHabilitacion) {
        this.idHabilitacion = idHabilitacion;
    }

    public String getSuministro() {
        return suministro;
    }

    public void setSuministro(String suministro) {
        this.suministro = suministro;
    }

    public String getMonoxidoMedicion() {
        return monoxidoMedicion;
    }

    public void setMonoxidoMedicion(String monoxidoMedicion) {
        this.monoxidoMedicion = monoxidoMedicion;
    }

}
