package pe.gob.osinergmin.gnr.cgn.data.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class MMotivoRechazoHabilitacionMontanteOutRO {

    @Id
    private Long idMotivoRechazoHabilitacionMontante;
    private Integer idMotivoRechazoHabilitacionMontantePadre;
    private String nombreMotivoRechazoHabilitacionMontante;

    @Generated(hash = 1471928302)
    public MMotivoRechazoHabilitacionMontanteOutRO() {
    }

    public MMotivoRechazoHabilitacionMontanteOutRO(Long idMotivoRechazoHabilitacionMontante) {
        this.idMotivoRechazoHabilitacionMontante = idMotivoRechazoHabilitacionMontante;
    }

    @Generated(hash = 1122443282)
    public MMotivoRechazoHabilitacionMontanteOutRO(Long idMotivoRechazoHabilitacionMontante, Integer idMotivoRechazoHabilitacionMontantePadre, String nombreMotivoRechazoHabilitacionMontante) {
        this.idMotivoRechazoHabilitacionMontante = idMotivoRechazoHabilitacionMontante;
        this.idMotivoRechazoHabilitacionMontantePadre = idMotivoRechazoHabilitacionMontantePadre;
        this.nombreMotivoRechazoHabilitacionMontante = nombreMotivoRechazoHabilitacionMontante;
    }

    public Long getIdMotivoRechazoHabilitacionMontante() {
        return idMotivoRechazoHabilitacionMontante;
    }

    public void setIdMotivoRechazoHabilitacionMontante(Long idMotivoRechazoHabilitacionMontante) {
        this.idMotivoRechazoHabilitacionMontante = idMotivoRechazoHabilitacionMontante;
    }

    public Integer getIdMotivoRechazoHabilitacionMontantePadre() {
        return idMotivoRechazoHabilitacionMontantePadre;
    }

    public void setIdMotivoRechazoHabilitacionMontantePadre(Integer idMotivoRechazoHabilitacionMontantePadre) {
        this.idMotivoRechazoHabilitacionMontantePadre = idMotivoRechazoHabilitacionMontantePadre;
    }

    public String getNombreMotivoRechazoHabilitacionMontante() {
        return nombreMotivoRechazoHabilitacionMontante;
    }

    public void setNombreMotivoRechazoHabilitacionMontante(String nombreMotivoRechazoHabilitacionMontante) {
        this.nombreMotivoRechazoHabilitacionMontante = nombreMotivoRechazoHabilitacionMontante;
    }
}
