package pe.gob.osinergmin.gnr.cgn.task;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import gob.osinergmin.gnr.domain.dto.rest.in.EspecialAccessRequestInRO;
import gob.osinergmin.gnr.domain.dto.rest.out.EspecialAccessResponseOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.util.Urls;

public class LoginEspecial extends AsyncTask<Config, Void, EspecialAccessResponseOutRO> {

    private OnLoginEspecialCompleted listener = null;

    public LoginEspecial(OnLoginEspecialCompleted listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected EspecialAccessResponseOutRO doInBackground(Config... configs) {
        try {

            Config config = configs[0];

            String url = Urls.getBase() + Urls.getAccesoCodigoEspecial();

            EspecialAccessRequestInRO especialAccessRequestInRO = new EspecialAccessRequestInRO();
            especialAccessRequestInRO.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);
            especialAccessRequestInRO.setCodigoAcceso(config.getTexto());

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Authorization", "Bearer " + config.getToken());

            HttpEntity<EspecialAccessRequestInRO> httpEntity = new HttpEntity<>(especialAccessRequestInRO, httpHeaders);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            return restTemplate.postForObject(url, httpEntity, EspecialAccessResponseOutRO.class);

        } catch (RestClientException e) {
            Log.e("LOGINESPECIAL", e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(EspecialAccessResponseOutRO especialAccessResponseOutRO) {
        super.onPostExecute(especialAccessResponseOutRO);
        listener.onLoginEspecialCompleted(especialAccessResponseOutRO);
    }

    public interface OnLoginEspecialCompleted {
        void onLoginEspecialCompleted(EspecialAccessResponseOutRO especialAccessResponseOutRO);
    }
}
