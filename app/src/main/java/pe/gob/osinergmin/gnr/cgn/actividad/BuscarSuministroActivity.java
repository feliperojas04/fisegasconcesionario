package pe.gob.osinergmin.gnr.cgn.actividad;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.BaseHabilitacionOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.BaseOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.HabilitacionOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.ParametroOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.SolicitudOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListBaseHabilitacionOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListParametroOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.adaptador.SuministroAdaptador;
import pe.gob.osinergmin.gnr.cgn.adaptador.VacioAdaptador;
import pe.gob.osinergmin.gnr.cgn.data.models.AcometidaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.AmbienteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.ConfigDao;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MSolicitudOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MSolicitudOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MontanteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.PuntoDao;
import pe.gob.osinergmin.gnr.cgn.data.models.RechazoDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Suministro;
import pe.gob.osinergmin.gnr.cgn.data.models.SuministroDao;
import pe.gob.osinergmin.gnr.cgn.task.Habilitacion;
import pe.gob.osinergmin.gnr.cgn.task.ListaBaseHabilitacion;
import pe.gob.osinergmin.gnr.cgn.task.Logout;
import pe.gob.osinergmin.gnr.cgn.task.ParametroAll;
import pe.gob.osinergmin.gnr.cgn.util.DownTimer;
import pe.gob.osinergmin.gnr.cgn.util.NetworkUtil;
import pe.gob.osinergmin.gnr.cgn.util.Parse;
import pe.gob.osinergmin.gnr.cgn.util.SimpleDividerItemDecoration;

public class BuscarSuministroActivity extends BaseActivity implements SuministroAdaptador.CallBack {

    private static final int PAGE_SIZE = 20;
    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;

    private RecyclerView listaSuministros;
    private List<MHabilitacionOutRO> mHabilitacionOutROs = new ArrayList<>();

    private Toolbar toolbar;
    private Config config;
    private DownTimer countDownTimer;
    private boolean abierto = true, isLoading = false, isLastPage = false;

    private ConfigDao configDao;
    private SuministroDao suministroDao;
    private AcometidaDao acometidaDao;
    private MontanteDao montanteDao;
    private MParametroOutRODao mParametroOutRODao;
    private MHabilitacionOutRODao mHabilitacionOutRODao;
    private MSolicitudOutRODao mSolicitudOutRODao;
    private PuntoDao puntoDao;
    private AmbienteDao ambienteDao;
    private RechazoDao rechazoDao;

    private TextView user, preVisitas, numPre, tv_acometida, numAco, tv_montante, numMon, sincronizacion, cerrar, configModo;
    private ImageView desConfig, configVacio, imageSync;
    private SwitchCompat offline;
    private SuministroAdaptador suministroAdaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_suministro_nav);

        countDownTimer = DownTimer.getInstance();

        suministroAdaptador = new SuministroAdaptador();

        configDao = ((App) getApplication()).getDaoSession().getConfigDao();
        suministroDao = ((App) getApplication()).getDaoSession().getSuministroDao();
        acometidaDao = ((App) getApplication()).getDaoSession().getAcometidaDao();
        montanteDao = ((App) getApplication()).getDaoSession().getMontanteDao();
        mParametroOutRODao = ((App) getApplication()).getDaoSession().getMParametroOutRODao();
        mHabilitacionOutRODao = ((App) getApplication()).getDaoSession().getMHabilitacionOutRODao();
        mSolicitudOutRODao = ((App) getApplication()).getDaoSession().getMSolicitudOutRODao();
        puntoDao = ((App) getApplication()).getDaoSession().getPuntoDao();
        ambienteDao = ((App) getApplication()).getDaoSession().getAmbienteDao();
        rechazoDao = ((App) getApplication()).getDaoSession().getRechazoDao();

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.nav_open, R.string.nav_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        user = header.findViewById(R.id.nav_usuario);
        preVisitas = header.findViewById(R.id.preVisitas);
        numPre = header.findViewById(R.id.numPre);
        tv_acometida = header.findViewById(R.id.tv_acometida);
        numAco = header.findViewById(R.id.numAco);
        tv_montante = header.findViewById(R.id.tv_montante);
        numMon = header.findViewById(R.id.numMon);
        sincronizacion = header.findViewById(R.id.sincronizacion);
        imageSync = header.findViewById(R.id.imageSync);
        desConfig = header.findViewById(R.id.desConfig);
        configVacio = header.findViewById(R.id.configVacio);
        configModo = header.findViewById(R.id.configModo);
        offline = header.findViewById(R.id.offline);
        cerrar = header.findViewById(R.id.cerrar);

        listaSuministros = findViewById(R.id.listaSuministros);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        listaSuministros.setLayoutManager(mLayoutManager);
        listaSuministros.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext(), R.drawable.line_divider_black));
        listaSuministros.setItemAnimator(new DefaultItemAnimator());
        ImageView recargaSuministro = findViewById(R.id.recargaSuministro);

        assert recargaSuministro != null;
        recargaSuministro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (config.getOffline()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(BuscarSuministroActivity.this);
                    builder.setTitle("Aviso");
                    builder.setMessage("Para obtener datos actualizados debe activar el modo ONLINE, lo cual puede hacer por medio de la opción \"Configuracion\".");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Entendido", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.show();
                } else {
                    if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        initRecycler();
                        CallListaBaseHabilitacion(currentPage);
                    } else {
                        ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                    }
                }
            }
        });

        listaSuministros.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!config.getOffline()) {
                    if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        int visibleItemCount = mLayoutManager.getChildCount();
                        int totalItemCount = mLayoutManager.getItemCount();
                        int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                        if (!isLoading && !isLastPage) {
                            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                                    && firstVisibleItemPosition >= 0
                                    && totalItemCount >= PAGE_SIZE) {
                                isLoading = true;
                                CallListaBaseHabilitacion(currentPage);
                            }
                        }
                    } else {
                        ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                    }
                }
            }
        });
    }

    private void cargarHabilitaciones(int posicion) {
        HabilitacionOutRO habilitacionOutRO = Parse.getHabilitacionOutRO(mHabilitacionOutROs.get(posicion));
        MSolicitudOutRO mSolicitudOutRO = mSolicitudOutRODao.queryBuilder().where(MSolicitudOutRODao.Properties.IdSolicitud.eq(habilitacionOutRO.getIdSolicitud())).limit(1).unique();
        SolicitudOutRO solicitudOutRO = Parse.getSolicitudOutRO(mSolicitudOutRO);
        habilitacionOutRO.setSolicitud(solicitudOutRO);
        if (Constantes.VALOR_SI.equalsIgnoreCase(config.getParametro1())) {
            if (config.getOffline()) {
                if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    Intent intent = new Intent(BuscarSuministroActivity.this, MapasActivity.class);
                    intent.putExtra("HABILITACION", habilitacionOutRO);
                    intent.putExtra("CONFIG", config);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(BuscarSuministroActivity.this, MapViewerActivity.class);
                    intent.putExtra("HABILITACION", habilitacionOutRO);
                    intent.putExtra("CONFIG", config);
                    startActivity(intent);
                    finish();
                }
            } else {
                Intent intent = new Intent(BuscarSuministroActivity.this, MapasActivity.class);
                intent.putExtra("HABILITACION", habilitacionOutRO);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
            }
        } else {
            List suministroList = suministroDao.queryBuilder()
                    .where(SuministroDao.Properties.IdHabilitacion.eq(habilitacionOutRO.getIdHabilitacion())).list();
            if (suministroList.size() > 0) {
                Suministro suministro = (Suministro) suministroList.get(0);
                Intent intent = new Intent(BuscarSuministroActivity.this, HabilitacionActivity.class);
                intent.putExtra("SUMINISTRO", suministro);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
            } else {
                Suministro suministro = new Suministro(null,
                        habilitacionOutRO.getIdHabilitacion() != null ? habilitacionOutRO.getIdHabilitacion() : 0,
                        habilitacionOutRO.getDocumentoIdentificacionSolicitante() != null ? habilitacionOutRO.getDocumentoIdentificacionSolicitante() : "",
                        habilitacionOutRO.getNombreSolicitante() != null ? habilitacionOutRO.getNombreSolicitante() : "",
                        habilitacionOutRO.getDireccionUbigeoPredio() != null ? habilitacionOutRO.getDireccionUbigeoPredio() : "",
                        habilitacionOutRO.getNumeroIntentoHabilitacion() != null ? habilitacionOutRO.getNumeroIntentoHabilitacion().toString() : "",
                        habilitacionOutRO.getCodigoSolicitud() != null ? habilitacionOutRO.getCodigoSolicitud().toString() : "",
                        habilitacionOutRO.getNumeroContratoConcesionariaPredio() != null ? habilitacionOutRO.getNumeroContratoConcesionariaPredio() : "",
                        habilitacionOutRO.getSolicitud().getFechaSolicitud() != null ? habilitacionOutRO.getSolicitud().getFechaSolicitud() : "",
                        habilitacionOutRO.getSolicitud().getFechaAprobacionSolicitud() != null ? habilitacionOutRO.getSolicitud().getFechaAprobacionSolicitud() : "",
                        "",
                        habilitacionOutRO.getSolicitud().getNombreTipoProyectoInstalacion() != null ? habilitacionOutRO.getSolicitud().getNombreTipoProyectoInstalacion() : "",
                        habilitacionOutRO.getSolicitud().getNombreTipoInstalacion() != null ? habilitacionOutRO.getSolicitud().getNombreTipoInstalacion() : "",
                        habilitacionOutRO.getSolicitud().getNumeroPuntosInstalacion() != null ? habilitacionOutRO.getSolicitud().getNumeroPuntosInstalacion().toString() : "",
                        "",
                        habilitacionOutRO.getNumeroSuministroPredio() != null ? habilitacionOutRO.getNumeroSuministroPredio() : "",
                        "",
                        "",
                        "0",
                        "",
                        "",
                        "0",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "0",
                        "0",
                        "0",
                        "0",
                        "",
                        "",
                        "0",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "0",
                        "",
                        "",
                        "0",
                        "",
                        false,
                        "",
                        "0",
                        "",
                        "",
                        "",
                        "");
                suministroDao.insertOrReplace(suministro);

                Intent intent = new Intent(BuscarSuministroActivity.this, HabilitacionActivity.class);
                intent.putExtra("SUMINISTRO", suministro);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.createAndStart();
        toolbar.setTitle("Habilitaciones");

        config = configDao.queryBuilder().limit(1).unique();
        config.setTexto("");
        user.setText(config.getUsername());
        setupMenu();
        setMenuCounter();
        initRecycler();
        if (config.getOffline()) {
            mHabilitacionOutROs = mHabilitacionOutRODao.queryBuilder().orderDesc(MHabilitacionOutRODao.Properties.ResultCode).list();
            listaHabiltacion(mHabilitacionOutROs);
        } else {
            if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                CallListaBaseHabilitacion(currentPage);
            } else {
                ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
            }
        }
    }

    public void setupMenu() {
        preVisitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        tv_acometida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuscarSuministroActivity.this, BuscarAcometidaActivity.class);
                startActivity(intent);
                finish();
            }
        });
        tv_montante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuscarSuministroActivity.this, BuscarMontanteActivity.class);
                startActivity(intent);
                finish();
            }
        });
        sincronizacion.setVisibility(config.getOffline() ? View.VISIBLE : View.GONE);
        imageSync.setVisibility(config.getOffline() ? View.VISIBLE : View.GONE);
        sincronizacion.setText(config.getSync() ? "Sincronizando..." : "Sincronización");
        sincronizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuscarSuministroActivity.this, SyncActivity.class);
                startActivity(intent);
            }
        });
        offline.setChecked(config.getOffline());
        desConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (abierto) {
                    configVacio.setVisibility(View.VISIBLE);
                    configModo.setVisibility(View.VISIBLE);
                    offline.setVisibility(View.VISIBLE);
                    abierto = false;
                } else {
                    configVacio.setVisibility(View.GONE);
                    configModo.setVisibility(View.GONE);
                    offline.setVisibility(View.GONE);
                    abierto = true;
                }
            }
        });
        offline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuscarSuministroActivity.this, ConfiguracionActivity.class);
                startActivity(intent);
                finish();
            }
        });
        cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cerrarSesion();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_busqueda_suministro, menu);
        MenuItem searchItem = menu.findItem(R.id.menuBusS_buscar);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        String buscar = getResources().getString(R.string.txtBus_filtro);
        searchView.setQueryHint(buscar);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                config.setTexto(query);
                if (config.getOffline()) {
                    suministroAdaptador.clear();
                    mHabilitacionOutROs = mHabilitacionOutRODao.queryBuilder().whereOr(
                            MHabilitacionOutRODao.Properties.CodigoSolicitud.like("%" + query + "%"),
                            MHabilitacionOutRODao.Properties.DireccionUbigeoPredio.like("%" + query + "%"),
                            MHabilitacionOutRODao.Properties.DocumentoIdentificacionSolicitante.like("%" + query + "%"),
                            MHabilitacionOutRODao.Properties.NombreSolicitante.like("%" + query + "%"),
                            MHabilitacionOutRODao.Properties.NumeroSuministroPredio.like("%" + query + "%"),
                            MHabilitacionOutRODao.Properties.NumeroContratoConcesionariaPredio.like("%" + query + "%")).list();
                    listaHabiltacion(mHabilitacionOutROs);
                } else {
                    if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        initRecycler();
                        CallListaBaseHabilitacion(currentPage);
                    } else {
                        ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                    }
                }
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                config.setTexto("");
                if (config.getOffline()) {
                    suministroAdaptador.clear();
                    mHabilitacionOutROs = mHabilitacionOutRODao.queryBuilder().orderDesc(MHabilitacionOutRODao.Properties.ResultCode).list();
                    listaHabiltacion(mHabilitacionOutROs);
                } else {
                    if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        initRecycler();
                        CallListaBaseHabilitacion(currentPage);
                    } else {
                        ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                    }
                }
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    public void listaHabiltacion(List<MHabilitacionOutRO> mHabilitacionOutROs) {
        initRecycler();
        List<BaseHabilitacionOutRO> baseHabilitacionOutROList = new ArrayList<>();
        for (MHabilitacionOutRO mHabilitacionOutRO : mHabilitacionOutROs) {
            baseHabilitacionOutROList.add(Parse.getBaseHabilitacionOutRO(mHabilitacionOutRO));
        }
        if (baseHabilitacionOutROList.size() > 0) {
            mostrarDatos(baseHabilitacionOutROList);
        } else {
            mostrarSin();
        }
    }

    public void initRecycler() {
        suministroAdaptador.clear();
        suministroAdaptador.setCallback(this);
        listaSuministros.setAdapter(suministroAdaptador);
        currentPage = PAGE_START;
        isLastPage = false;
    }

    public void mostrarDatos(List<BaseHabilitacionOutRO> baseHabilitacionOutROList) {
        suministroAdaptador.add(baseHabilitacionOutROList);
    }

    public void mostrarSin() {
        listaSuministros.setAdapter(new VacioAdaptador(getResources().getString(R.string.txtBus_sinSuministro)));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent intent = new Intent(BuscarSuministroActivity.this, PrincipalActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        countDownTimer.createAndStart();
    }

    private void setMenuCounter() {
        long previsita = suministroDao
                .queryBuilder()
                .where(SuministroDao.Properties.Grabar.eq(true)).count();
        long acometida = acometidaDao
                .queryBuilder()
                .where(AcometidaDao.Properties.Grabar.eq(true)).count();
        long montante = montanteDao
                .queryBuilder()
                .where(MontanteDao.Properties.Grabar.eq(true)).count();
        if (previsita > 0) {
            numPre.setText(String.valueOf(previsita));
        } else {
            numPre.setText("");
        }
        if (acometida > 0) {
            numAco.setText(String.valueOf(acometida));
        } else {
            numAco.setText("");
        }
        if (montante > 0) {
            numMon.setText(String.valueOf(montante));
        } else {
            numMon.setText("");
        }
    }

    private void cerrarSesion() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(BuscarSuministroActivity.this);
        builder.setTitle(R.string.app_msjTitulo);
        builder.setMessage(R.string.app_msjCuerpo);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.app_msjSi, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (config.getOffline()) {
                    config.setLogin(false);
                    configDao.insertOrReplace(config);
                    Intent intent = new Intent(BuscarSuministroActivity.this, IniciarActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        showProgressDialog(R.string.app_cargando);
                        Logout logout = new Logout(new Logout.OnLogoutCompleted() {
                            @Override
                            public void onLogoutCompled(BaseOutRO baseOutRO) {
                                hideProgressDialog();
                                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                                } else if (baseOutRO == null) {
                                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                                } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(baseOutRO.getResultCode())) {
                                    ((App) getApplication()).showToast(baseOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : baseOutRO.getMessage());
                                } else {
                                    countDownTimer.stop();
                                    config.setLogin(false);
                                    configDao.insertOrReplace(config);
                                    Intent intent = new Intent(BuscarSuministroActivity.this, IniciarActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        });
                        logout.execute(config);
                    } else {
                        ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                    }
                }
            }
        });
        builder.setNegativeButton(R.string.app_msjNo, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onInstalacionEliminar(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(BuscarSuministroActivity.this);
        builder.setTitle(R.string.app_msjTitulo);
        builder.setMessage("¿Está seguro que desea eliminar la Habilitación?");
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.app_msjSi, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                BaseHabilitacionOutRO baseHabilitacionOutRO = suministroAdaptador.getItem(position);
                //MSolicitudOutRO mSolicitudOutRO = mSolicitudOutRODao.queryBuilder().where(MSolicitudOutRODao.Properties.IdSolicitud.eq(mHabilitacionOutRO.getIdHabilitacion())).unique();
                Suministro suministro = suministroDao.queryBuilder().where(SuministroDao.Properties.IdHabilitacion.eq(baseHabilitacionOutRO.getIdHabilitacion())).limit(1).unique();
                suministro.setGrabar(false);
                suministroDao.insertOrReplace(suministro);
                MHabilitacionOutRO mHabilitacionOutRO = mHabilitacionOutRODao.queryBuilder().where(MHabilitacionOutRODao.Properties
                        .IdHabilitacion.eq(baseHabilitacionOutRO.getIdHabilitacion())).limit(1).unique();
                mHabilitacionOutRO.setResultCode("101");
                mHabilitacionOutRODao.insertOrReplace(mHabilitacionOutRO);
                suministroAdaptador.getItem(position).setResultCode("101");
                suministroAdaptador.notifyItemChanged(position);
                /*new BorrarFoto().execute(suministro.getIdHabilitacion().toString());
                List<Punto> puntos = puntoDao.queryBuilder()
                        .where(PuntoDao.Properties.IdHabilitacion.eq(suministro.getIdHabilitacion())).list();
                for (Punto punto : puntos) {
                    puntoDao.delete(punto);
                }
                List<Ambiente> ambientes = ambienteDao.queryBuilder()
                        .where(AmbienteDao.Properties.IdHabilitacion.eq(suministro.getIdHabilitacion())).list();
                for (Ambiente ambiente : ambientes) {
                    ambienteDao.delete(ambiente);
                }
                List<Rechazo> rechazos = rechazoDao.queryBuilder().where(RechazoDao.Properties.IdHabilitacion.eq(suministro.getIdHabilitacion())).list();
                for (Rechazo rechazo : rechazos) {
                    rechazoDao.delete(rechazo);
                }
                mHabilitacionOutRODao.deleteByKey(baseHabilitacionOutRO.getIdHabilitacion().longValue());
                // mSolicitudOutRODao.delete(mSolicitudOutRO);
                suministroDao.delete(suministro);
                suministroAdaptador.remove(baseHabilitacionOutRO);*/
                setMenuCounter();
            }
        });
        builder.setNegativeButton(R.string.app_msjNo, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    @Override
    public void onSelect(BaseHabilitacionOutRO baseHabilitacionOutRO, int position) {
        //   if (baseHabilitacionOutROList.size() > 0) {
        // BaseHabilitacionOutRO baseHabilitacionOutRO = (BaseHabilitacionOutRO) listaSuministros.getAdapter().getItem(position);
        config.setIdHabilitacion(baseHabilitacionOutRO.getIdHabilitacion().toString());
        if (config.getOffline()) {
            MParametroOutRO mParametroOutRO1 = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_VALIDACION_ACTIVA_DISTANCIA_INSPECTOR_PREDIO))
                    .limit(1)
                    .unique();
            config.setParametro1(mParametroOutRO1 == null ? "N" : mParametroOutRO1.getValorParametro());
            MParametroOutRO mParametroOutPrecision = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_FRECUENCIA_TOMA_COORDENADAS_AUDITORIA))
                    .limit(1).unique();
            config.setPrecision(mParametroOutPrecision == null ? "10000" : mParametroOutPrecision.getValorParametro());
            if (Constantes.VALOR_SI.equalsIgnoreCase(config.getParametro1())) {
                MParametroOutRO mParametroOutRO2 = mParametroOutRODao.
                        queryBuilder().
                        where(MParametroOutRODao.
                                Properties.NombreParametro.
                                eq(Constantes.NOMBRE_PARAMETRO_DISTANCIA_MAXIMA_INSPECTOR_PREDIO))
                        .limit(1)
                        .unique();
                config.setParametro2(mParametroOutRO2 == null ? "150" : mParametroOutRO2.getValorParametro());
                MParametroOutRO mParametroOutRO3 = mParametroOutRODao.
                        queryBuilder().
                        where(MParametroOutRODao.
                                Properties.NombreParametro.
                                eq(Constantes.NOMBRE_PARAMETRO_SOLICITUD_ACTIVA_CODIGO_ESPECIAL_ACCESO))
                        .limit(1)
                        .unique();
                config.setParametro3(mParametroOutRO3 == null ? "S" : mParametroOutRO3.getValorParametro());
                cargarHabilitaciones(position);
            } else {
                cargarHabilitaciones(position);
            }
        } else {
            if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                CallParametro();
            } else {
                ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
            }
        }
        // }
    }

    private void CallListaBaseHabilitacion(final int actualPage) {
        showProgressDialog(R.string.app_cargando);
        ListaBaseHabilitacion listaBaseHabilitacion = new ListaBaseHabilitacion(new ListaBaseHabilitacion.OnListaBaseHabilitacionCompleted() {
            @Override
            public void onListaBaseHabilitacionCompled(ListBaseHabilitacionOutRO listBaseHabilitacionOutRO) {
                hideProgressDialog();
                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                } else if (listBaseHabilitacionOutRO == null) {
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listBaseHabilitacionOutRO.getResultCode())) {
                    ((App) getApplication()).showToast(listBaseHabilitacionOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listBaseHabilitacionOutRO.getMessage());
                } else {
                    if (listBaseHabilitacionOutRO.getHabilitacion() != null && listBaseHabilitacionOutRO.getHabilitacion().size() > 0) {
                        mostrarDatos(listBaseHabilitacionOutRO.getHabilitacion());
                    } else {
                        isLastPage = true;
                        if (actualPage == PAGE_START) {
                            mostrarSin();
                        }
                    }
                    currentPage += 1;
                    isLoading = false;
                }
            }
        });
        Config config1 = config;
        config1.setErrorPrevisita(actualPage); // pagina
        config1.setErrorVisita(PAGE_SIZE); // elementos por pagina
        listaBaseHabilitacion.execute(config1);
    }

    private void CallParametro() {
        showProgressDialog(R.string.app_cargando);
        ParametroAll parametroAll = new ParametroAll(new ParametroAll.OnParametroAllCompleted() {
            @Override
            public void onParametroAllCompled(ListParametroOutRO listParametroOutRO) {
                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                } else if (listParametroOutRO == null) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listParametroOutRO.getResultCode())) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast(listParametroOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listParametroOutRO.getMessage());
                } else {
                    List<ParametroOutRO> parametroOutROList = listParametroOutRO.getParametro();
                    for (ParametroOutRO parametroOutRO : parametroOutROList) {
                        switch (parametroOutRO.getNombreParametro()) {
                            case Constantes.NOMBRE_PARAMETRO_VALIDACION_ACTIVA_DISTANCIA_INSPECTOR_PREDIO:
                                config.setParametro1(parametroOutRO.getValorParametro());
                                break;
                            case Constantes.NOMBRE_PARAMETRO_DISTANCIA_MAXIMA_INSPECTOR_PREDIO:
                                config.setParametro2(parametroOutRO.getValorParametro());
                                break;
                            case Constantes.NOMBRE_PARAMETRO_SOLICITUD_ACTIVA_CODIGO_ESPECIAL_ACCESO:
                                config.setParametro3(parametroOutRO.getValorParametro());
                                break;
                            case Constantes.NOMBRE_PARAMETRO_TELEFONO_SOLICITUD_CODIGO_ESPECIAL_ACCESO:
                                config.setParametro4(parametroOutRO.getValorParametro());
                                break;
                            case Constantes.NOMBRE_PARAMETRO_CONFIGURACION_COMPRESION_FOTOS_APLICATIVO_MOVIL:
                                config.setParametro5(parametroOutRO.getValorParametro());
                                break;
                            case Constantes.NOMBRE_PARAMETRO_FRECUENCIA_TOMA_COORDENADAS_AUDITORIA:
                                config.setPrecision(parametroOutRO.getValorParametro());
                                break;
                        }
                    }
                    CallHabilitacion();
                }
            }
        });
        parametroAll.execute(config);
    }

    private void CallHabilitacion() {
        Habilitacion habilitacion = new Habilitacion(new Habilitacion.OnHabilitacionCompleted() {
            @Override
            public void onHabilitacionCompled(HabilitacionOutRO habilitacionOutRO) {
                hideProgressDialog();
                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                } else if (habilitacionOutRO == null) {
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(habilitacionOutRO.getResultCode())) {
                    ((App) getApplication()).showToast(habilitacionOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : habilitacionOutRO.getMessage());
                } else {
                    if (Constantes.VALOR_SI.equalsIgnoreCase(config.getParametro1())) {
                        Intent intent = new Intent(BuscarSuministroActivity.this, MapasActivity.class);
                        intent.putExtra("HABILITACION", habilitacionOutRO);
                        intent.putExtra("CONFIG", config);
                        startActivity(intent);
                        finish();
                    } else {
                        List suministroList = suministroDao.queryBuilder()
                                .where(SuministroDao.Properties.IdHabilitacion.eq(habilitacionOutRO.getIdHabilitacion())).list();
                        if (suministroList.size() > 0) {
                            Suministro suministro = (Suministro) suministroList.get(0);
                            Intent intent = new Intent(BuscarSuministroActivity.this, HabilitacionActivity.class);
                            intent.putExtra("SUMINISTRO", suministro);
                            intent.putExtra("CONFIG", config);
                            startActivity(intent);
                            finish();
                        } else {
                            Suministro suministro = new Suministro(null,
                                    habilitacionOutRO.getIdHabilitacion() != null ? habilitacionOutRO.getIdHabilitacion() : 0,
                                    habilitacionOutRO.getDocumentoIdentificacionSolicitante() != null ? habilitacionOutRO.getDocumentoIdentificacionSolicitante() : "",
                                    habilitacionOutRO.getNombreSolicitante() != null ? habilitacionOutRO.getNombreSolicitante() : "",
                                    habilitacionOutRO.getDireccionUbigeoPredio() != null ? habilitacionOutRO.getDireccionUbigeoPredio() : "",
                                    habilitacionOutRO.getNumeroIntentoHabilitacion() != null ? habilitacionOutRO.getNumeroIntentoHabilitacion().toString() : "",
                                    habilitacionOutRO.getCodigoSolicitud() != null ? habilitacionOutRO.getCodigoSolicitud().toString() : "",
                                    habilitacionOutRO.getNumeroContratoConcesionariaPredio() != null ? habilitacionOutRO.getNumeroContratoConcesionariaPredio() : "",
                                    habilitacionOutRO.getSolicitud().getFechaSolicitud() != null ? habilitacionOutRO.getSolicitud().getFechaSolicitud() : "",
                                    habilitacionOutRO.getSolicitud().getFechaAprobacionSolicitud() != null ? habilitacionOutRO.getSolicitud().getFechaAprobacionSolicitud() : "",
                                    "",
                                    habilitacionOutRO.getSolicitud().getNombreTipoProyectoInstalacion() != null ? habilitacionOutRO.getSolicitud().getNombreTipoProyectoInstalacion() : "",
                                    habilitacionOutRO.getSolicitud().getNombreTipoInstalacion() != null ? habilitacionOutRO.getSolicitud().getNombreTipoInstalacion() : "",
                                    habilitacionOutRO.getSolicitud().getNumeroPuntosInstalacion() != null ? habilitacionOutRO.getSolicitud().getNumeroPuntosInstalacion().toString() : "",
                                    "",
                                    habilitacionOutRO.getNumeroSuministroPredio() != null ? habilitacionOutRO.getNumeroSuministroPredio() : "",
                                    "",
                                    "",
                                    "0",
                                    "",
                                    "",
                                    "0",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "0",
                                    "0",
                                    "0",
                                    "0",
                                    "",
                                    "",
                                    "0",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "0",
                                    "",
                                    "",
                                    "0",
                                    "",
                                    false,
                                    "",
                                    "0",
                                    "",
                                    "",
                                    "",
                                    "");
                            suministroDao.insertOrReplace(suministro);

                            Intent intent = new Intent(BuscarSuministroActivity.this, HabilitacionActivity.class);
                            intent.putExtra("SUMINISTRO", suministro);
                            intent.putExtra("CONFIG", config);
                            startActivity(intent);
                            finish();
                        }
                    }
                }
            }
        });
        habilitacion.execute(config);
    }
}
