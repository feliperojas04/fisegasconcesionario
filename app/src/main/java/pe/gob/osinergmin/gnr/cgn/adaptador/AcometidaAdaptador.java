package pe.gob.osinergmin.gnr.cgn.adaptador;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.BaseInstalacionAcometidaOutRO;
import pe.gob.osinergmin.gnr.cgn.R;

public class AcometidaAdaptador extends RecyclerView.Adapter<AcometidaAdaptador.ViewHolder> {

    private CallBack mCallback;
    private List<BaseInstalacionAcometidaOutRO> baseInstalacionAcometidaOutROs;

    public AcometidaAdaptador() {
        this.baseInstalacionAcometidaOutROs = new ArrayList<>();
    }

    public void add(List<BaseInstalacionAcometidaOutRO> mc) {
        baseInstalacionAcometidaOutROs.addAll(mc);
        notifyItemInserted(baseInstalacionAcometidaOutROs.size() - 1);
    }

    public void remove(BaseInstalacionAcometidaOutRO city) {
        int position = baseInstalacionAcometidaOutROs.indexOf(city);
        if (position > -1) {
            baseInstalacionAcometidaOutROs.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public void setCallback(CallBack callback) {
        mCallback = callback;
    }

    public List<BaseInstalacionAcometidaOutRO> getItems() {
        return baseInstalacionAcometidaOutROs;
    }

    public BaseInstalacionAcometidaOutRO getItem(int position) {
        return this.baseInstalacionAcometidaOutROs.get(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_acometida, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final BaseInstalacionAcometidaOutRO baseInstalacionAcometidaOutRO = this.baseInstalacionAcometidaOutROs.get(position);
        String suministro = "<b>Solicitante:</b> %1$s (%2$s)";
        String suministroF = String.format(suministro, baseInstalacionAcometidaOutRO.getNombreSolicitante(), baseInstalacionAcometidaOutRO.getDocumentoIdentificacionSolicitante());
        holder.txtBusI_propietario.setText(Html.fromHtml(suministroF));
        String visitado = "<b>Suministro:</b> %1$s";
        String visitadoF = String.format(visitado, baseInstalacionAcometidaOutRO.getNumeroSuministroPredio());
        holder.txtBusI_predio.setText(Html.fromHtml(visitadoF));
        String predio = "<b>Predio:</b> %1$s";
        String predioF = String.format(predio, baseInstalacionAcometidaOutRO.getDireccionUbigeoPredio());
        holder.txtBusI_suministros.setText(Html.fromHtml(predioF));
        String visitante = "<b>Contrato:</b> %1$s";
        String visitanteF = String.format(visitante, baseInstalacionAcometidaOutRO.getNumeroContratoConcesionariaPredio());
        holder.txtBusI_contrato.setText(Html.fromHtml(visitanteF));
        String tipoInstalacion = "<b>Tipo de instalación:</b> %1$s";
        String tipoInstalacionF = String.format(tipoInstalacion, baseInstalacionAcometidaOutRO.getNombreTipoInstalacionAcometida());
        holder.txtBusITipoInstalacion.setText(Html.fromHtml(tipoInstalacionF));

        if (baseInstalacionAcometidaOutRO.getErrorCode() == null || "".equalsIgnoreCase(baseInstalacionAcometidaOutRO.getErrorCode())) {
            holder.btnBusI_error.setVisibility(View.GONE);
            holder.btnBusI_eliminar.setVisibility(View.GONE);
            if (baseInstalacionAcometidaOutRO.getResultCode() == null || "".equalsIgnoreCase(baseInstalacionAcometidaOutRO.getResultCode())) {
                holder.contenedor.setBackgroundResource(R.color.blanco);
            } else {
                holder.contenedor.setBackgroundResource(R.color.mod);
            }
        } else {
            holder.btnBusI_error.setVisibility(View.VISIBLE);
            holder.btnBusI_eliminar.setVisibility(baseInstalacionAcometidaOutRO.getResultCode().equalsIgnoreCase("101") ? View.GONE : View.VISIBLE);
            holder.contenedor.setBackgroundResource(R.color.error);
            holder.btnBusI_error.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setTitle("Causa del error en la sincronización");
                    builder.setMessage(baseInstalacionAcometidaOutRO.getMessage());
                    builder.setCancelable(false);
                    builder.setPositiveButton("Entendido", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.show();
                }
            });
            holder.btnBusI_eliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onInstalacionEliminar(position);
                }
            });
        }
        holder.contenedor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onSelect(baseInstalacionAcometidaOutRO, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return baseInstalacionAcometidaOutROs.size();
    }

    public interface CallBack {
        void onInstalacionEliminar(int position);

        void onSelect(BaseInstalacionAcometidaOutRO baseInstalacionAcometidaOutRO, int position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtBusI_propietario, txtBusI_predio, txtBusI_suministros, txtBusI_contrato, txtBusITipoInstalacion;
        Button btnBusI_error, btnBusI_eliminar;
        RelativeLayout contenedor;

        ViewHolder(View v) {
            super(v);
            txtBusI_propietario = v.findViewById(R.id.txtBusI_propietario);
            txtBusI_predio = v.findViewById(R.id.txtBusI_predio);
            txtBusI_suministros = v.findViewById(R.id.txtBusI_suministros);
            txtBusI_contrato = v.findViewById(R.id.txtBusI_contrato);
            txtBusITipoInstalacion = v.findViewById(R.id.txtBusITipoInstalacion);
            btnBusI_error = v.findViewById(R.id.btnBusI_error);
            btnBusI_eliminar = v.findViewById(R.id.btnBusI_eliminar);
            contenedor = v.findViewById(R.id.contenedor);
        }
    }
}
