package pe.gob.osinergmin.gnr.cgn.task;

import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.in.BaseInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.HabilitacionMontanteInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.MotivoRechazoHabilitacionMontanteHabilitacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.filter.FilterHabilitacionMontanteInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.list.ListMotivoRechazoHabilitacionMontanteHabilitacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.out.HabilitacionMontanteOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListBaseHabilitacionMontanteOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListGenericBeanOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListHabilitacionMontanteOutRO;
import gob.osinergmin.gnr.util.Constantes;
import io.reactivex.Single;
import pe.gob.osinergmin.gnr.cgn.data.models.PrecisionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.Montante;
import pe.gob.osinergmin.gnr.cgn.data.models.Precision;
import pe.gob.osinergmin.gnr.cgn.data.models.RechazoMontante;
import pe.gob.osinergmin.gnr.cgn.data.models.RechazoMontanteDao;
import pe.gob.osinergmin.gnr.cgn.util.Urls;

public class MontanteRx {

    public static Single<ListBaseHabilitacionMontanteOutRO> getMontanteListarResumen(Config config) {
        return Single.create(emitter -> {
            try {

                String url = Urls.getBase() + Urls.getMontanteListarResumen();

                FilterHabilitacionMontanteInRO filter = new FilterHabilitacionMontanteInRO();
                filter.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);
                filter.setPage(config.getErrorPrevisita());
                filter.setRowsPerPage(config.getErrorVisita());
                if (!(config.getTexto().equalsIgnoreCase(""))) {
                    filter.setTexto(config.getTexto());
                }

                HttpHeaders httpHeaders = new HttpHeaders();
                httpHeaders.setContentType(MediaType.APPLICATION_JSON);
                httpHeaders.add("Authorization", "Bearer " + config.getToken());

                HttpEntity<FilterHabilitacionMontanteInRO> httpEntity = new HttpEntity<>(filter, httpHeaders);

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                ListBaseHabilitacionMontanteOutRO list = restTemplate.postForObject(url, httpEntity, ListBaseHabilitacionMontanteOutRO.class);
                emitter.onSuccess(list);
            } catch (RestClientException e) {
                emitter.onError(e);
            }
        });
    }

    public static Single<ListHabilitacionMontanteOutRO> getMontanteListar(String token) {
        return Single.create(emitter -> {
            try {

                String url = Urls.getBase() + Urls.getMontanteListar();

                FilterHabilitacionMontanteInRO filter = new FilterHabilitacionMontanteInRO();
                filter.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);

                HttpHeaders httpHeaders = new HttpHeaders();
                httpHeaders.setContentType(MediaType.APPLICATION_JSON);
                httpHeaders.add("Authorization", "Bearer " + token);

                HttpEntity<FilterHabilitacionMontanteInRO> httpEntity = new HttpEntity<>(filter, httpHeaders);

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                ListHabilitacionMontanteOutRO list = restTemplate.postForObject(url, httpEntity, ListHabilitacionMontanteOutRO.class);
                emitter.onSuccess(list);
            } catch (RestClientException e) {
                emitter.onError(e);
            }
        });
    }

    public static Single<ListGenericBeanOutRO> getTiposInstalacion(String token) {
        return Single.create(emitter -> {
            try {

                String url = Urls.getBase() + Urls.getMontanteTiposInstalacion();

                BaseInRO filter = new BaseInRO();
                filter.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);

                HttpHeaders httpHeaders = new HttpHeaders();
                httpHeaders.setContentType(MediaType.APPLICATION_JSON);
                httpHeaders.add("Authorization", "Bearer " + token);

                HttpEntity<BaseInRO> httpEntity = new HttpEntity<>(filter, httpHeaders);

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                ListGenericBeanOutRO list = restTemplate.postForObject(url, httpEntity, ListGenericBeanOutRO.class);
                emitter.onSuccess(list);
            } catch (RestClientException e) {
                emitter.onError(e);
            }
        });
    }

    public static Single<HabilitacionMontanteOutRO> getMontanteObtener(Integer idHabilitacionMontante, String token) {
        return Single.create(emitter -> {
            try {

                String url = Urls.getBase() + Urls.getMontanteObtener();

                FilterHabilitacionMontanteInRO filter = new FilterHabilitacionMontanteInRO();
                filter.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);
                filter.setIdHabilitacionMontante(idHabilitacionMontante);

                HttpHeaders httpHeaders = new HttpHeaders();
                httpHeaders.setContentType(MediaType.APPLICATION_JSON);
                httpHeaders.add("Authorization", "Bearer " + token);

                HttpEntity<FilterHabilitacionMontanteInRO> httpEntity = new HttpEntity<>(filter, httpHeaders);

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                HabilitacionMontanteOutRO convenio = restTemplate.postForObject(url, httpEntity, HabilitacionMontanteOutRO.class);
                emitter.onSuccess(convenio);
            } catch (RestClientException e) {
                emitter.onError(e);
            }
        });
    }

    public static Single<HabilitacionMontanteOutRO> getMontanteRegistrar(PrecisionDao precisionDao, Montante montante, String token, RechazoMontanteDao rechazoMontanteDao) {
        return Single.create(emitter -> {
            try {
                Precision precision = precisionDao.queryBuilder()
                        .where(PrecisionDao.Properties.Estado.eq("2"),
                                PrecisionDao.Properties.IdSolicitud.eq(montante.getIdHabilitacionMontante()))
                        .orderAsc(PrecisionDao.Properties.Precision).limit(1).unique();

                String url = Urls.getBase() + Urls.getMontanteRegistrar();

                HabilitacionMontanteInRO newObj = new HabilitacionMontanteInRO();
                newObj.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);
                newObj.setIdHabilitacionMontante(montante.getIdHabilitacionMontante());
                newObj.setAprobadoHabilitacionMontante("1".equalsIgnoreCase(montante.getApruebaHabilitacionMontante()) ? Constantes.VALOR_SI : Constantes.VALOR_NO);
                newObj.setAprobadaAcometida("1".equalsIgnoreCase(montante.getApruebaAcometida()) ? Constantes.VALOR_SI : Constantes.VALOR_NO);
                newObj.setAprobadoRecorridoTuberia("1".equalsIgnoreCase(montante.getApruebaTuberia()) ? Constantes.VALOR_SI : Constantes.VALOR_NO);
                newObj.setAprobadoHermeticidad("1".equalsIgnoreCase(montante.getApruebaHermeticidad()) ? Constantes.VALOR_SI : Constantes.VALOR_NO);
                newObj.setPresionOperacionHermeticidad("".equalsIgnoreCase(montante.getPresionOperacionHermeticidad()) ? null : BigDecimal.valueOf(Double.valueOf(montante.getPresionOperacionHermeticidad())));
                newObj.setPresionTuberiaHermeticidad("".equalsIgnoreCase(montante.getPresionTuberiaHermeticidad()) ? null : BigDecimal.valueOf(Double.valueOf(montante.getPresionTuberiaHermeticidad())));
                newObj.setAprobadaInstalacion("1".equalsIgnoreCase(montante.getApruebaInstalacion()) ? Constantes.VALOR_SI : Constantes.VALOR_NO);
                newObj.setTipoInstalacion("".equalsIgnoreCase(montante.getTipoInstalacion()) ? null : montante.getTipoInstalacion());
                newObj.setIdMaterialInstalacion("".equalsIgnoreCase(montante.getTipoMaterialInstalacion()) ? null : Integer.parseInt(montante.getTipoMaterialInstalacion()));
                newObj.setNombreMaterialInstalacion("");
                newObj.setLongitudTotalInstalacion("".equalsIgnoreCase(montante.getLongitudInstalacion()) ? null : BigDecimal.valueOf(Double.valueOf(montante.getLongitudInstalacion())));
                newObj.setDiametroTuberia("".equalsIgnoreCase(montante.getDiametroTuberiaInstalacion()) ? null : BigDecimal.valueOf(Double.valueOf(montante.getDiametroTuberiaInstalacion())));
                newObj.setFechaRegistroOfflineHabilitacionMontante(montante.getFechaRegistroOffline());
                newObj.setCoordenadasAuditoriaHabilitacionMontante(String.format("%s,%s", precision.getLatitud(), precision.getLongitud()));
                newObj.setPrecisionCoordenadasAuditoriaHabilitacionMontante(BigDecimal.valueOf(Double.valueOf(precision.getPrecision())));

                if("0".equalsIgnoreCase(montante.getApruebaHabilitacionMontante())){
                    List<RechazoMontante> rechazosMontantes= rechazoMontanteDao.queryBuilder()
                            .where(RechazoMontanteDao.Properties.IdHabilitacionMontante.eq(montante.getIdHabilitacionMontante())).list();
                    ListMotivoRechazoHabilitacionMontanteHabilitacionInRO  listMotivoRechazoHabilitacionMontanteHabilitacionInRO = new ListMotivoRechazoHabilitacionMontanteHabilitacionInRO();
                    List<MotivoRechazoHabilitacionMontanteHabilitacionInRO> motivoRechazoHabilitacionMontanteHabilitacion = new ArrayList<>();
                    int k=1;
                    for (RechazoMontante rechazoMontante : rechazosMontantes){
                        MotivoRechazoHabilitacionMontanteHabilitacionInRO motivoRechazoMontanteTMP = new MotivoRechazoHabilitacionMontanteHabilitacionInRO();
                        motivoRechazoMontanteTMP.setOrdenMotivoRechazoHabilitacionMontanteHabilitacion(k);
                        motivoRechazoMontanteTMP.setIdMotivoRechazoHabilitacionMontante(rechazoMontante.getIdMotivoSubRechazo());
                        motivoRechazoMontanteTMP.setObservacionMotivoRechazoHabilitacionMontanteHabilitacion(rechazoMontante.getObservacion());
                        motivoRechazoHabilitacionMontanteHabilitacion.add(motivoRechazoMontanteTMP);
                        k++;
                    }
                    listMotivoRechazoHabilitacionMontanteHabilitacionInRO.setMotivoRechazoHabilitacionMontanteHabilitacion(motivoRechazoHabilitacionMontanteHabilitacion);
                    newObj.setMotivosRechazoHabilitacionMontante(listMotivoRechazoHabilitacionMontanteHabilitacionInRO);
                }
                /*Fin*/

                HttpHeaders httpHeaders = new HttpHeaders();
                httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
                httpHeaders.add("Authorization", "Bearer " + token);

                MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
                parts.add(Constantes.PARAM_NAME_HABILITACION_MONTANTE, newObj);

                parts.add(Constantes.PARAM_NAME_FOTO_ACOMETIDA, "".equalsIgnoreCase(montante.getFotoAcometida()) ? null : new FileSystemResource(montante.getFotoAcometida()));
                parts.add(Constantes.PARAM_NAME_FOTO_1_RECORRIDO_TUBERIA, "".equalsIgnoreCase(montante.getFoto1Tuberia()) ? null : new FileSystemResource(montante.getFoto1Tuberia()));
                parts.add(Constantes.PARAM_NAME_FOTO_2_RECORRIDO_TUBERIA, "".equalsIgnoreCase(montante.getFoto2Tuberia()) ? null : new FileSystemResource(montante.getFoto2Tuberia()));
                parts.add(Constantes.PARAM_NAME_FOTO_3_RECORRIDO_TUBERIA, "".equalsIgnoreCase(montante.getFoto3Tuberia()) ? null : new FileSystemResource(montante.getFoto3Tuberia()));
                parts.add(Constantes.PARAM_NAME_FOTO_MEDICION_HERMETICIDAD_MANOMETRO, "".equalsIgnoreCase(montante.getFotoManoHermeticidad()) ? null : new FileSystemResource(montante.getFotoManoHermeticidad()));

                if("0".equalsIgnoreCase(montante.getApruebaHabilitacionMontante())){
                    parts.add(Constantes.PARAM_NAME_FOTO_ACTA_HABILITACION_MONTANTE,"".equalsIgnoreCase(montante.getMontanteHabilitacionFotoActaRechazo())? null : new FileSystemResource(montante.getMontanteHabilitacionFotoActaRechazo()));
                    parts.add(Constantes.PARAM_NAME_FOTO_PRUEBA_RECHAZO_HABILITACION_MONTANTE,"".equalsIgnoreCase(montante.getMontanteHabilitacionFotoPruebaRechazo())? null : new FileSystemResource(montante.getMontanteHabilitacionFotoPruebaRechazo()));
                }else{
                    parts.add(Constantes.PARAM_NAME_FOTO_ACTA_HABILITACION_MONTANTE,"".equalsIgnoreCase(montante.getMontanteHabilitacionFotoActa())? null : new FileSystemResource(montante.getMontanteHabilitacionFotoActa()));
                }

                HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(parts, httpHeaders);
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                HabilitacionMontanteOutRO convenio = restTemplate.postForObject(url, httpEntity, HabilitacionMontanteOutRO.class);
                emitter.onSuccess(convenio);
            } catch (RestClientException e) {
                emitter.onError(e);
            }
        });
    }
}
