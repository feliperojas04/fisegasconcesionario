package pe.gob.osinergmin.gnr.cgn.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import pe.gob.osinergmin.gnr.cgn.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.util.ResponseReceiver;

public class AlarmService {

    public AlarmService() {

    }

    public static void startActionAlarm(Context context, long fecha) {
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                context,
                1010,
                IntentHelper.buildDisplayIntentOn(context),
                PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        manager.set(AlarmManager.RTC_WAKEUP, fecha, pendingIntent);
    }

    public static void cancelActionAlarm(Context context) {
        SharedPreferences mPreferences = context.getSharedPreferences("gnrconcesionario", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putBoolean("pendientes", false);
        editor.putLong("fecha", 0);
        editor.apply();
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                context,
                1010,
                IntentHelper.buildDisplayIntentOn(context),
                PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
        clearNotificacion(context);
    }

    public static void clearNotificacion(Context context) {
        LocalBroadcastManager.getInstance(context)
                .sendBroadcast(IntentHelper.buildDisplayIntentOff(context));
    }

    public static class IntentHelper {

        public static Intent buildDisplayIntentOn(Context context) {
            Intent intent = new Intent(context, ResponseReceiver.class);
            intent.setAction(Constantes.ACTION_OFFLINE_ON);
            return intent;

        }

        public static Intent buildDisplayIntentOff(Context context) {
            Intent intent = new Intent(context, ResponseReceiver.class);
            intent.setAction(Constantes.ACTION_OFFLINE_OFF);
            return intent;

        }
    }
}
