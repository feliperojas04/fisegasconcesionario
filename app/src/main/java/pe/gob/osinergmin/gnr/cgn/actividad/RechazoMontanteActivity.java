package pe.gob.osinergmin.gnr.cgn.actividad;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import gob.osinergmin.gnr.domain.dto.rest.out.MotivoRechazoHabilitacionMontanteOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListMotivoRechazoHabilitacionMontanteOutRO;
import gob.osinergmin.gnr.util.Constantes;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.adaptador.RechazoMontanteAdaptador;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionMontanteOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionMontanteOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MMotivoRechazoHabilitacionMontanteOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MMotivoRechazoHabilitacionMontanteOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.Montante;
import pe.gob.osinergmin.gnr.cgn.data.models.MontanteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Precision;
import pe.gob.osinergmin.gnr.cgn.data.models.PrecisionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.RechazoMontante;
import pe.gob.osinergmin.gnr.cgn.data.models.RechazoMontanteDao;
import pe.gob.osinergmin.gnr.cgn.task.MontanteRx;
import pe.gob.osinergmin.gnr.cgn.task.TipoMotivoRechazoMontante;
import pe.gob.osinergmin.gnr.cgn.util.BorrarFoto;
import pe.gob.osinergmin.gnr.cgn.util.DownTimer;
import pe.gob.osinergmin.gnr.cgn.util.FormatoFecha;
import pe.gob.osinergmin.gnr.cgn.util.MostrarFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.NetworkUtil;
import pe.gob.osinergmin.gnr.cgn.util.Parse;
import pe.gob.osinergmin.gnr.cgn.util.ReducirFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.Util;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class RechazoMontanteActivity extends BaseActivity implements RechazoMontanteAdaptador.CallBack {

    private Montante montante;
    private Config config;
    private LinearLayout lnlRec_finales;
    private ImageView icoRec_acta;
    private ImageView imgRec_fotoActa;
    private ImageView icoRec_prueba;
    private ImageView imgRec_fotoPrueba;
    private ListView listaRechazos;
    private Button btnRec_rechazar;
    private Button btnRec_aceptar;
    private Button btnRec_cancelar;
    private List<RechazoMontante> rechazos;
    private Toolbar toolbar;
    private String tipo;
    private List<MotivoRechazoHabilitacionMontanteOutRO> totalMotivos = new ArrayList<>();
    private RechazoMontanteAdaptador rechazoAdaptador;
    private MontanteDao montanteDao;
    private RechazoMontanteDao rechazoMontanteDao;
    private DownTimer countDownTimer;
    private MParametroOutRODao mParametroOutRODao;
    private MMotivoRechazoHabilitacionMontanteOutRODao mMotivoRechazoHabilitacionMontanteOutRODao;
    private String tmpRuta;
    private List<Integer> select = new ArrayList<>();
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private PrecisionDao precisionDao;
    private MHabilitacionMontanteOutRODao mHabilitacionMontanteOutRODao;
    private final CompositeDisposable disposables = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rechazo_montante);

        countDownTimer = DownTimer.getInstance();

        montanteDao = ((App) getApplication()).getDaoSession().getMontanteDao();
        precisionDao = ((App) getApplication()).getDaoSession().getPrecisionDao();
        mParametroOutRODao = ((App) getApplication()).getDaoSession().getMParametroOutRODao();
        rechazoMontanteDao = ((App) getApplication()).getDaoSession().getRechazoMontanteDao();
        mHabilitacionMontanteOutRODao = ((App) getApplication()).getDaoSession().getMHabilitacionMontanteOutRODao();
        mMotivoRechazoHabilitacionMontanteOutRODao = ((App) getApplication()).getDaoSession().getMMotivoRechazoHabilitacionMontanteOutRODao();
        precisionDao = ((App) getApplication()).getDaoSession().getPrecisionDao();

        toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_36dp);

        montante = getIntent().getExtras().getParcelable("MONTANTE");
        config = getIntent().getExtras().getParcelable("CONFIG");
        tipo = getIntent().getExtras().getString("TIPO");

        lnlRec_finales = findViewById(R.id.lnlRec_finalesMontante);
        icoRec_acta = findViewById(R.id.icoRec_actaRechazo);
        imgRec_fotoActa = findViewById(R.id.imgRec_fotoActaRechazo);
        icoRec_prueba = findViewById(R.id.icoRec_pruebaMontante);
        imgRec_fotoPrueba = findViewById(R.id.imgRec_fotoPruebaRechazo);
        ImageView addRec_rechazos = findViewById(R.id.addRec_rechazos);
        listaRechazos = findViewById(R.id.listaRechazos);
        btnRec_rechazar = findViewById(R.id.btnRec_rechazar);
        btnRec_aceptar = findViewById(R.id.btnRec_aceptar);
        btnRec_cancelar = findViewById(R.id.btnRec_cancelar);

        imgRec_fotoActa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(
                        RechazoMontanteActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ((App) getApplication()).showToast("No puede realizar la acción porque no se ha dado el permiso de Almacenamiento");
                    return;
                }
                if (Util.isExternalStorageWritable()) {
                    if (Util.getAlbumStorageDir(RechazoMontanteActivity.this)) {
                        File file = Util.getNuevaRutaFoto(montante.getIdHabilitacionMontante().toString(), RechazoMontanteActivity.this);
                        tmpRuta = file.getPath();
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        Uri outputUri = FileProvider.getUriForFile(RechazoMontanteActivity.this, getApplicationContext().getPackageName() + ".provider", file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            ClipData clip = ClipData.newUri(getContentResolver(), "Instalacion", outputUri);
                            intent.setClipData(clip);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        }
                        guardarRechazos();
                        startActivityForResult(intent, 1);
                    } else {
                        ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                    }
                } else {
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                }
            }
        });

        imgRec_fotoPrueba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(
                        RechazoMontanteActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ((App) getApplication()).showToast("No puede realizar la acción porque no se ha dado el permiso de Almacenamiento");
                    return;
                }
                if (Util.isExternalStorageWritable()) {
                    if (Util.getAlbumStorageDir(RechazoMontanteActivity.this)) {
                        File file = Util.getNuevaRutaFoto(montante.getIdHabilitacionMontante().toString(), RechazoMontanteActivity.this);
                        tmpRuta = file.getPath();
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        Uri outputUri = FileProvider.getUriForFile(RechazoMontanteActivity.this, getApplicationContext().getPackageName() + ".provider", file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            ClipData clip = ClipData.newUri(getContentResolver(), "Instalacion", outputUri);
                            intent.setClipData(clip);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        }
                        guardarRechazos();
                        startActivityForResult(intent, 2);
                    } else {
                        ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                    }
                } else {
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                }
            }
        });

        addRec_rechazos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RechazoMontante rechazo = new RechazoMontante(null, montante.getIdHabilitacionMontante(), 0, 0, "");
                if (rechazos != null) {
                    rechazos.add(rechazo);
                }
                rechazoAdaptador.setRechazos(rechazos);
                Util.setListViewHeightBasedOnChildren(listaRechazos);
            }
        });

        btnRec_aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarRechazos();
                finish();
            }
        });

        btnRec_cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                ((App) getApplication()).getDaoSession().clear();
            }
        });

        btnRec_rechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(RechazoMontanteActivity.this);
                builder.setTitle(R.string.txtHab_mensajeConfirmacion);
                builder.setMessage("¿Está seguro de rechazar la Habilitación de Montante?");
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.btnHab_si, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        montante.setApruebaHabilitacionMontante("0");
                        if (config.getOffline()) {
                            MHabilitacionMontanteOutRO mHabilitacionMontanteOutRO = mHabilitacionMontanteOutRODao.queryBuilder().where(MHabilitacionMontanteOutRODao.Properties.IdHabilitacionMontante.eq(montante.getIdHabilitacionMontante())).limit(1).unique();
                            mHabilitacionMontanteOutRO.setResultCode("100");
                            montante.setFechaAprobacion(FormatoFecha.getfecha());
                            montante.setFechaRegistroOffline(FormatoFecha.getfecha());
                            montante.setGrabar(true);
                            for (RechazoMontante rechazo : rechazos) {
                                rechazoMontanteDao.insertOrReplace(rechazo);
                            }
                            mHabilitacionMontanteOutRODao.insertOrReplace(mHabilitacionMontanteOutRO);
                            montanteDao.insertOrReplace(montante);
                            AlertDialog.Builder builder = new AlertDialog.Builder(RechazoMontanteActivity.this);
                            builder.setTitle(R.string.txtHab_mensajeConfirmacion);
                            builder.setMessage("La Habilitación de Montante fue rechazada satisfactoriamente");
                            builder.setCancelable(false);
                            builder.setPositiveButton(R.string.btnHab_aceptar, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(RechazoMontanteActivity.this, BuscarMontanteActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                            builder.show();
                        } else {
                            if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                showProgressDialog(R.string.app_procesando);
                                guardarRechazos();
                                grabarMontante(precisionDao, montante, config.getToken());
                            } else {
                                ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                            }
                        }
                    }
                });
                builder.setNegativeButton(R.string.btnHab_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    Precision precision = new Precision();
                    precision.setIdSolicitud(montante.getIdHabilitacionMontante());
                    precision.setLatitud(String.valueOf(location.getLatitude()));
                    precision.setLongitud(String.valueOf(location.getLongitude()));
                    precision.setPrecision(String.valueOf(location.getAccuracy()));
                    precision.setEstado("2");
                    precisionDao.insertOrReplace(precision);
                }
            }
        };
        aceptarPermiso();
    }

    private void grabarMontante(PrecisionDao precisionDao, Montante montante, String token) {
        disposables.add(MontanteRx.getMontanteRegistrar(precisionDao, montante, token, rechazoMontanteDao)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(habilitacionMontanteOutRO -> {
                    if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(habilitacionMontanteOutRO.getResultCode())) {
                        hideProgressDialog();
                        ((App) getApplication()).showToast(habilitacionMontanteOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : habilitacionMontanteOutRO.getMessage());
                    } else {
                        new BorrarFoto().execute(montante.getIdHabilitacionMontante().toString());
                        final AlertDialog.Builder builder = new AlertDialog.Builder(RechazoMontanteActivity.this);
                        builder.setTitle(R.string.txtHab_mensajeConfirmacion);
                        if (montante.getApruebaHabilitacionMontante().equalsIgnoreCase("1")) {
                            builder.setMessage("La Habilitación de Montante fue aprobada satisfactoriamente");
                        } else {
                            builder.setMessage("La Habilitación de Montante fue rechazada satisfactoriamente");
                        }
                        builder.setCancelable(false);
                        builder.setPositiveButton(R.string.btnHab_aceptar, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                List<RechazoMontante> rechazos = rechazoMontanteDao
                                        .queryBuilder()
                                        .where(RechazoMontanteDao
                                                .Properties
                                                .IdHabilitacionMontante
                                                .eq(montante.getIdHabilitacionMontante()))
                                        .list();
                                rechazoMontanteDao.deleteInTx(rechazos);

                                List<Precision> precisiones = precisionDao.queryBuilder()
                                        .where(PrecisionDao.Properties.Estado.eq("2"),
                                                PrecisionDao.Properties.IdSolicitud.eq(montante.getIdHabilitacionMontante())).list();
                                precisionDao.deleteInTx(precisiones);

                                montanteDao.delete(montante);
                                Intent intent = new Intent(RechazoMontanteActivity.this, BuscarMontanteActivity.class);
                                intent.putExtra("CONFIG",config);
                                startActivity(intent);
                                finish();
                            }
                        });
                        builder.show();
                    }
                }, throwable -> {
                    hideProgressDialog();
                    if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                    } else {
                        ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                    }
                }));
    }

    private void guardarRechazos() {
        List<RechazoMontante> rechazosTMP = rechazoMontanteDao.queryBuilder()
                .where(RechazoMontanteDao.Properties.IdHabilitacionMontante.eq(montante.getIdHabilitacionMontante())).list();
        rechazoMontanteDao.deleteInTx(rechazosTMP);
        for (RechazoMontante rechazo : rechazos) {
            rechazoMontanteDao.insertOrReplace(rechazo);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.createAndStart();
        createLocationRequest();
        if ("general".equalsIgnoreCase(tipo)) {
            toolbar.setTitle(montante.getObjetoConexion() + " - Rechazo");
            btnRec_aceptar.setVisibility(View.GONE);
            btnRec_cancelar.setVisibility(View.GONE);
            btnRec_rechazar.setVisibility(View.VISIBLE);
            lnlRec_finales.setVisibility(View.VISIBLE);
        } else {
            toolbar.setTitle("Selección de motivo de rechazo");
            btnRec_aceptar.setVisibility(View.VISIBLE);
            btnRec_cancelar.setVisibility(View.VISIBLE);
            btnRec_rechazar.setVisibility(View.GONE);
            lnlRec_finales.setVisibility(View.GONE);
        }
        if (config.getOffline()) {
            totalMotivos.clear();
            List<MMotivoRechazoHabilitacionMontanteOutRO> mMotivoRechazoHabilitacionOutROS = mMotivoRechazoHabilitacionMontanteOutRODao.queryBuilder().list();
            for (MMotivoRechazoHabilitacionMontanteOutRO mMotivoRechazoHabilitacionOutRO : mMotivoRechazoHabilitacionOutROS) {
                totalMotivos.add(Parse.getMotivoRechazoHabilitacionMontanteOutRO(mMotivoRechazoHabilitacionOutRO));
            }
            MParametroOutRO mParametroOutRO = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_CONFIGURACION_COMPRESION_FOTOS_APLICATIVO_MOVIL))
                    .limit(1).unique();
            config.setParametro5(mParametroOutRO == null ? "800x600" : mParametroOutRO.getValorParametro());
            MParametroOutRO mParametroOutPrecision = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_FRECUENCIA_TOMA_COORDENADAS_AUDITORIA))
                    .limit(1).unique();
            config.setPrecision(mParametroOutPrecision == null ? "10000" : mParametroOutPrecision.getValorParametro());
            cargaDatos();
        } else {
            if (totalMotivos.size() > 0 && !"".equalsIgnoreCase(config.getParametro5())) {
                cargaDatos();
            } else {
                if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    showProgressDialog(R.string.app_cargando);
                    TipoMotivoRechazoMontante tipoMotivoRechazo = new TipoMotivoRechazoMontante(new TipoMotivoRechazoMontante.OnTipoMotivoRechazoCompleted() {
                        @Override
                        public void onTipoMotivoRechazoCompled(ListMotivoRechazoHabilitacionMontanteOutRO listMotivoRechazoHabilitacionOutRO) {
                            hideProgressDialog();
                            if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                            } else if (listMotivoRechazoHabilitacionOutRO == null) {
                                ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                            } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listMotivoRechazoHabilitacionOutRO.getResultCode())) {
                                ((App) getApplication()).showToast(listMotivoRechazoHabilitacionOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listMotivoRechazoHabilitacionOutRO.getMessage());
                            } else {
                                totalMotivos = listMotivoRechazoHabilitacionOutRO.getMotivoRechazoHabilitacionMontante();
                                if ("".equalsIgnoreCase(config.getParametro5())) {
                                    config.setParametro5("800x600");
                                }
                                if ("".equalsIgnoreCase(config.getPrecision())) {
                                    config.setPrecision("10000");
                                }
                                cargaDatos();
                            }
                        }
                    });
                    tipoMotivoRechazo.execute(config);
                } else {
                    ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                }
            }
        }
    }

    private void cargaDatos() {
        rechazos = rechazoMontanteDao.queryBuilder()
                .where(RechazoMontanteDao.Properties.IdHabilitacionMontante.eq(montante.getIdHabilitacionMontante())).list();
        if (rechazos.size() > 0) {
            rechazoAdaptador = new RechazoMontanteAdaptador(this);
            rechazoAdaptador.setRechazos(rechazos);
            rechazoAdaptador.setListaRechazos(listaRechazos);
            rechazoAdaptador.setCallback(this);
            rechazoAdaptador.setListaTotalMotivos(totalMotivos);
            listaRechazos.setAdapter(rechazoAdaptador);
            Util.setListViewHeightBasedOnChildren(listaRechazos);
        } else {
            rechazos = new ArrayList<>();
            RechazoMontante rechazo = new RechazoMontante(null, montante.getIdHabilitacionMontante(), 0, 0, "");
            rechazos.add(rechazo);
            rechazoAdaptador = new RechazoMontanteAdaptador(this);
            rechazoAdaptador.setRechazos(rechazos);
            rechazoAdaptador.setListaRechazos(listaRechazos);
            rechazoAdaptador.setCallback(this);
            rechazoAdaptador.setListaTotalMotivos(totalMotivos);
            listaRechazos.setAdapter(rechazoAdaptador);
            Util.setListViewHeightBasedOnChildren(listaRechazos);
        }
        if(!("".equalsIgnoreCase(montante.getMontanteHabilitacionFotoActaRechazo()))){
            loadBitmap(montante.getMontanteHabilitacionFotoActaRechazo(), imgRec_fotoActa);
            icoRec_acta.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if(!("".equalsIgnoreCase(montante.getMontanteHabilitacionFotoPruebaRechazo()))){
            loadBitmap(montante.getMontanteHabilitacionFotoPruebaRechazo(), imgRec_fotoPrueba);
            icoRec_prueba.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        onCheck();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_general, menu);
        MenuItem menuMas = menu.findItem(R.id.menuMas);
        menuMas.setVisible(false);
        MenuItem menuMenos = menu.findItem(R.id.menuMenos);
        menuMenos.setVisible(false);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1458) {
            if (resultCode == RESULT_CANCELED) {
                Intent intent = new Intent(RechazoMontanteActivity.this, BuscarMontanteActivity.class);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
            }
        }
        if (requestCode == 1) {
            if (resultCode != RESULT_CANCELED) {
                montante.setMontanteHabilitacionFotoActaRechazo(tmpRuta);
                ReducirFotoTask reducirFotoTask = new ReducirFotoTask(new ReducirFotoTask.OnReducirFotoTaskCompleted() {
                    @Override
                    public void onReducirFotoTaskCompleted(String result) {
                        if (!("1".equalsIgnoreCase(result))) {
                            ((App) getApplication()).showToast(result);
                        }
                    }
                });
                montanteDao.insertOrReplace(montante);
                reducirFotoTask.execute(montante.getMontanteHabilitacionFotoActaRechazo(), config.getParametro5());
            }
        }
        if (requestCode == 2) {
            if (resultCode != RESULT_CANCELED) {
                montante.setMontanteHabilitacionFotoPruebaRechazo(tmpRuta);
                ReducirFotoTask reducirFotoTask = new ReducirFotoTask(new ReducirFotoTask.OnReducirFotoTaskCompleted() {
                    @Override
                    public void onReducirFotoTaskCompleted(String result) {
                        if (!("1".equalsIgnoreCase(result))) {
                            ((App) getApplication()).showToast(result);
                        }
                    }
                });
                montanteDao.insertOrReplace(montante);
                reducirFotoTask.execute(montante.getMontanteHabilitacionFotoPruebaRechazo(), config.getParametro5());
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        ((App) getApplication()).getDaoSession().clear();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        countDownTimer.createAndStart();
    }

    private boolean addSelect(int id) {
        if (id != 0) {
            if (!select.contains(id)) {
                select.add(id);
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    @Override
    public void onCheck() {
        int i = 0;
        select.clear();
        boolean boton = true;
        while (i < rechazos.size()) {
            RechazoMontante rechazo = rechazos.get(i);
            if (addSelect(rechazo.getIdMotivoSubRechazo())) {
                ((App) getApplication()).showToast("Un submotivo de rechazo ya ha sido seleccionado más de una vez.");
                boton = false;
                i = rechazos.size();
            }
            if (0 == rechazo.getIdMotivoRechazo()) {
                boton = false;
                i = rechazos.size();
            }
            if (0 == rechazo.getIdMotivoSubRechazo()) {
                boton = false;
                i = rechazos.size();
            }
            i++;
        }
        if ("general".equalsIgnoreCase(tipo)) {
            if (!(rechazos.size() > 0)) {
                boton = false;
            }
            if ("".equalsIgnoreCase(montante.getMontanteHabilitacionFotoActaRechazo())) {
                boton = false;
            }
            btnRec_rechazar.setEnabled(boton);
        } else {
            btnRec_aceptar.setEnabled(boton);
        }
    }

    public void loadBitmap(String ruta, final ImageView imageView) {
        MostrarFotoTask mostrarFotoTask = new MostrarFotoTask(new MostrarFotoTask.OnMostrarFotoTaskCompleted() {
            @Override
            public void onMostrarFotoTaskCompleted(Bitmap bitmap) {
                if (bitmap != null) {
                    if (imageView != null) {
                        imageView.setImageBitmap(bitmap);
                    }
                } else {
                    ((App) getApplication()).showToast("Error al cargar la foto");
                }
            }
        });
        mostrarFotoTask.execute(ruta);
    }

    private void createLocationRequest() {
        int value = 10000;
        try {
            value = Integer.valueOf(config.getPrecision());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(value);
        mLocationRequest.setFastestInterval(value);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(RechazoMontanteActivity.this, 1458);
                    } catch (IntentSender.SendIntentException sendEx) {
                    }
                }
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            aceptarPermiso();
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
    }

    private void aceptarPermiso() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permiso = Util.getPermisos(RechazoMontanteActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION);
            if (permiso != null) {
                requestPermissions(permiso, pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION:
                String message = Util.onRequestPermissionsResult(permissions, grantResults);
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    @Override
    protected void onDestroy() {
        disposables.clear();
        super.onDestroy();
    }

}