package pe.gob.osinergmin.gnr.cgn.task;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import gob.osinergmin.gnr.domain.dto.rest.in.PasswordRequestInRO;
import gob.osinergmin.gnr.domain.dto.rest.out.BaseOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.util.Urls;

public class RecuperarContrasenia extends AsyncTask<String, Void, BaseOutRO> {

    private OnRecuperarContraseniaCompleted listener = null;

    public RecuperarContrasenia(OnRecuperarContraseniaCompleted listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected BaseOutRO doInBackground(String... params) {
        try {
            String url = Urls.getBase() + Urls.getRecuperarContrasena();

            PasswordRequestInRO passwordRequestInRO = new PasswordRequestInRO();
            passwordRequestInRO.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);
            passwordRequestInRO.setUsername(params[0]);

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<PasswordRequestInRO> httpEntity = new HttpEntity<>(passwordRequestInRO, httpHeaders);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            return restTemplate.postForObject(url, httpEntity, BaseOutRO.class);

        } catch (RestClientException e) {
            Log.e("RECUPERARCONTRASEÑA", e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(BaseOutRO baseOutRO) {
        super.onPostExecute(baseOutRO);
        listener.onRecuperarContraseniaCompleted(baseOutRO);
    }

    public interface OnRecuperarContraseniaCompleted {
        void onRecuperarContraseniaCompleted(BaseOutRO baseOutRO);
    }
}
