package pe.gob.osinergmin.gnr.cgn.data.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class Ambiente {
    @Id
    private Long id;
    private Integer idHabilitacion;
    private String suministro;
    private String ambienteTipo;
    private String ambienteConfinado;
    private String ambienteVentilacionSuperior;
    private String ambienteVentilacionInferior;
    private String ambienteVentilacionSuperiorFoto;
    private String ambienteVentilacionInferiorFoto;
    private String monoxidoMedicion;
    private String monoxidoFoto;
    private Integer posicion;
    private String ambienteUnoFoto;
    private String ambienteDosFoto;

    @Generated(hash = 2022271096)
    public Ambiente() {
    }

    public Ambiente(Long id) {
        this.id = id;
    }

    @Generated(hash = 470886650)
    public Ambiente(Long id, Integer idHabilitacion, String suministro, String ambienteTipo, String ambienteConfinado, String ambienteVentilacionSuperior, String ambienteVentilacionInferior, String ambienteVentilacionSuperiorFoto, String ambienteVentilacionInferiorFoto, String monoxidoMedicion, String monoxidoFoto, Integer posicion, String ambienteUnoFoto, String ambienteDosFoto) {
        this.id = id;
        this.idHabilitacion = idHabilitacion;
        this.suministro = suministro;
        this.ambienteTipo = ambienteTipo;
        this.ambienteConfinado = ambienteConfinado;
        this.ambienteVentilacionSuperior = ambienteVentilacionSuperior;
        this.ambienteVentilacionInferior = ambienteVentilacionInferior;
        this.ambienteVentilacionSuperiorFoto = ambienteVentilacionSuperiorFoto;
        this.ambienteVentilacionInferiorFoto = ambienteVentilacionInferiorFoto;
        this.monoxidoMedicion = monoxidoMedicion;
        this.monoxidoFoto = monoxidoFoto;
        this.posicion = posicion;
        this.ambienteUnoFoto = ambienteUnoFoto;
        this.ambienteDosFoto = ambienteDosFoto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdHabilitacion() {
        return idHabilitacion;
    }

    public void setIdHabilitacion(Integer idHabilitacion) {
        this.idHabilitacion = idHabilitacion;
    }

    public String getSuministro() {
        return suministro;
    }

    public void setSuministro(String suministro) {
        this.suministro = suministro;
    }

    public String getAmbienteTipo() {
        return ambienteTipo;
    }

    public void setAmbienteTipo(String ambienteTipo) {
        this.ambienteTipo = ambienteTipo;
    }

    public String getAmbienteConfinado() {
        return ambienteConfinado;
    }

    public void setAmbienteConfinado(String ambienteConfinado) {
        this.ambienteConfinado = ambienteConfinado;
    }

    public String getAmbienteVentilacionSuperior() {
        return ambienteVentilacionSuperior;
    }

    public void setAmbienteVentilacionSuperior(String ambienteVentilacionSuperior) {
        this.ambienteVentilacionSuperior = ambienteVentilacionSuperior;
    }

    public String getAmbienteVentilacionInferior() {
        return ambienteVentilacionInferior;
    }

    public void setAmbienteVentilacionInferior(String ambienteVentilacionInferior) {
        this.ambienteVentilacionInferior = ambienteVentilacionInferior;
    }

    public String getAmbienteVentilacionSuperiorFoto() {
        return ambienteVentilacionSuperiorFoto;
    }

    public void setAmbienteVentilacionSuperiorFoto(String ambienteVentilacionSuperiorFoto) {
        this.ambienteVentilacionSuperiorFoto = ambienteVentilacionSuperiorFoto;
    }

    public String getAmbienteVentilacionInferiorFoto() {
        return ambienteVentilacionInferiorFoto;
    }

    public void setAmbienteVentilacionInferiorFoto(String ambienteVentilacionInferiorFoto) {
        this.ambienteVentilacionInferiorFoto = ambienteVentilacionInferiorFoto;
    }

    public String getMonoxidoMedicion() {
        return monoxidoMedicion;
    }

    public void setMonoxidoMedicion(String monoxidoMedicion) {
        this.monoxidoMedicion = monoxidoMedicion;
    }

    public String getMonoxidoFoto() {
        return monoxidoFoto;
    }

    public void setMonoxidoFoto(String monoxidoFoto) {
        this.monoxidoFoto = monoxidoFoto;
    }

    public Integer getPosicion() {
        return posicion;
    }

    public void setPosicion(Integer posicion) {
        this.posicion = posicion;
    }

    public String getAmbienteUnoFoto() {
        return ambienteUnoFoto;
    }

    public void setAmbienteUnoFoto(String ambienteUnoFoto) {
        this.ambienteUnoFoto = ambienteUnoFoto;
    }

    public String getAmbienteDosFoto() {
        return ambienteDosFoto;
    }

    public void setAmbienteDosFoto(String ambienteDosFoto) {
        this.ambienteDosFoto = ambienteDosFoto;
    }
}
