package pe.gob.osinergmin.gnr.cgn.data.pojos;

public class Habilitacion {

    public int item;
    public String nombre;

    public Habilitacion(int item, String nombre) {
        this.item = item;
        this.nombre = nombre;
    }
}
