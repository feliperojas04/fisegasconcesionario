package pe.gob.osinergmin.gnr.cgn.service;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import gob.osinergmin.gnr.domain.dto.rest.out.InstalacionInternaOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.actividad.PrincipalActivity;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.ConfigDao;
import pe.gob.osinergmin.gnr.ign.data.models.PaqueteGPS;
import pe.gob.osinergmin.gnr.cgn.task.GrabarGPSTask;
import pe.gob.osinergmin.gnr.cgn.util.BorrarFoto;
import pe.gob.osinergmin.gnr.cgn.util.NetworkUtil;
import pe.gob.osinergmin.gnr.cgn.util.Urls;

public class GPSService extends Service {
    private static final String TAG = GPSService.class.getSimpleName();
    private final int tiempoEnSegundosGPS = 1000 * 25;// 25 Segundos
    private final int tiempoEnvioGPS = 1000 * 30;// 30 Segundos

    public static final String EDIT_TEXT_SHARED_PREFERENCES = "pe.gob.osinergmin.coords";
    public Handler handler = null;
    public static Runnable runnable = null;
    LocationManager locationManager;
    private Config config;

    public GPSService() {
    }
    @Override
    public void onCreate() {
        Log.d(TAG, "Servicio creado...");
        ConfigDao configDao = ((App) getApplicationContext()).getDaoSession().getConfigDao();
        config = configDao.queryBuilder().limit(1).unique();

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        handler = new Handler();
        runnable = new Runnable() {
            public void run() {
                if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    enviarGPS(false);
                } else{
                    Log.i("TEST_GPS","No puedo enviar GPS, será para después.");
                }
                /*
                if (MyApp.hayConexion(getApplicationContext())) {
                    subirData();
                } else {
                    //Log.i("TEST_GPS","No puedo enviar GPS");
                    //Toast.makeText(getApplicationContext(), "Sin CONEXIÓN...", Toast.LENGTH_SHORT).show();
                }*/
                handler.postDelayed(runnable, tiempoEnvioGPS);
            }
        };
        handler.postDelayed(runnable, tiempoEnvioGPS);
        obtenerUbicacion();
    }

    @Override
    public int onStartCommand(Intent intent, int flag, int idProcess) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        enviarGPS(true);
        handler.removeCallbacks(runnable);
        locationManager.removeUpdates(locationListenerBest);
        Log.d(TAG, "Servicio destruido...");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void enviarGPS(boolean esFinalizar){
        SharedPreferences mSharedPrefs = getApplicationContext().getSharedPreferences("GPSService", Context.MODE_PRIVATE);
        String  actualLatitudLongitud = mSharedPrefs.getString(EDIT_TEXT_SHARED_PREFERENCES, "");
        if (actualLatitudLongitud.equalsIgnoreCase("")){
            return;
        }
        String[] arregloCoords = actualLatitudLongitud.split(",");

        final String    idUsuario = config.getUsername(),
                        idDevice = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                                Settings.Secure.ANDROID_ID),
                        latitud = arregloCoords[1],
                        longitud = arregloCoords[0],
                        estado = esFinalizar ? "I"  : "A";

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url = Urls.getBase().concat(Urls.getMonitoreoGPS())
                            .concat("?idUsuario=").concat(idUsuario)
                            .concat("&idDevice=").concat(idDevice)
                            .concat("&latitud=").concat(latitud)
                            .concat("&longitud=").concat(longitud)
                            .concat("&estado=").concat(estado);
        // Request a string response from the provided URL.
        //Toast.makeText(getApplicationContext(), "Enviando "+url, Toast.LENGTH_SHORT).show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        //Toast.makeText(getApplicationContext(), response.substring(0,500), Toast.LENGTH_SHORT).show();
                        Log.i("ERIK ".concat(esFinalizar ? "CERRANDO ": "ENVIANDO").concat(estado), response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorStr = "";
                error.printStackTrace();

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    errorStr = "No se logró conectar al servidor.";//This indicates that the reuest has either time out or there is no connection

                } else if (error instanceof AuthFailureError) {
                    errorStr = "Hubo un error de autentificación en el envío.";
                    // Error indicating that there was an Authentication Failure while performing the request

                } else if (error instanceof ServerError) {
                    errorStr = "Error 500: ";
                    errorStr += error.getMessage();
                    //Indicates that the server responded with a error response

                } else if (error instanceof NetworkError) {
                    errorStr = "Hubo un error de red en el envío. ";
                    //Indicates that there was network error while performing the request

                } else if (error instanceof ParseError) {
                    errorStr = "La respuesta del servidor no puedo ser procesada. ";
                    // Indicates that the server response could not be parsed
                }

                Toast.makeText(getApplicationContext(), errorStr, Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer ".concat(config.getToken()));
                return headers;

            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void obtenerUbicacion() {
        if (!checkLocation())
            return;
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        String provider = locationManager.getBestProvider(criteria, true);
        if (provider != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            locationManager.requestLocationUpdates(provider, tiempoEnSegundosGPS, 0, locationListenerBest);
            obtenerInitUbicacion(locationManager.getLastKnownLocation(provider));
        } else{
            Toast.makeText(getApplicationContext(), "Proveedor GPS NULO.", Toast.LENGTH_SHORT).show();
        }
    }

    private final LocationListener locationListenerBest = new LocationListener() {
        public void onLocationChanged(Location location) {
            obtenerInitUbicacion(location);
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {
        }

        @Override
        public void onProviderDisabled(String s) {
        }
    };

    private boolean checkLocation() {
        if (!isLocationEnabled())
            Toast.makeText(this, "RECUERDA ACTIVAR GPS", Toast.LENGTH_LONG).show();
        return isLocationEnabled();
    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public void obtenerInitUbicacion(Location location){
        if (location != null) {
            SharedPreferences mSharedPrefs;
            String latitud = String.valueOf(location.getLatitude());
            String longitud = String.valueOf(location.getLongitude());
            if (!latitud.equalsIgnoreCase("0")) {
                mSharedPrefs = getApplicationContext().getSharedPreferences("GPSService", Context.MODE_PRIVATE);
                //String actualLatitudLongitud = mSharedPrefs.getString(EDIT_TEXT_SHARED_PREFERENCES, "");
                mSharedPrefs
                        .edit()
                        .putString(EDIT_TEXT_SHARED_PREFERENCES,latitud+","+longitud)
                        .apply();
            }
        }
    }

}
