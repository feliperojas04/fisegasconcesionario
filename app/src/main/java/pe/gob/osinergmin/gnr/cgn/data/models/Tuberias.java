package pe.gob.osinergmin.gnr.cgn.data.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class Tuberias {

    @Id
    private Long id;
    private int IdInstalacionAcometida;
    private String fusionista;
    private String resultado;
    private String diametro;
    private String longitud;
    private String material;
    private String fotoActa;
    private String fotoTC;
    private String fotoGabinete;
    private Integer posicion;

    @Generated(hash = 457725251)
    public Tuberias() {
    }

    @Generated(hash = 132571503)
    public Tuberias(Long id, int IdInstalacionAcometida, String fusionista, String resultado, String diametro, String longitud, String material, String fotoActa, String fotoTC, String fotoGabinete, Integer posicion) {
        this.id = id;
        this.IdInstalacionAcometida = IdInstalacionAcometida;
        this.fusionista = fusionista;
        this.resultado = resultado;
        this.diametro = diametro;
        this.longitud = longitud;
        this.material = material;
        this.fotoActa = fotoActa;
        this.fotoTC = fotoTC;
        this.fotoGabinete = fotoGabinete;
        this.posicion = posicion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getIdInstalacionAcometida() {
        return IdInstalacionAcometida;
    }

    public void setIdInstalacionAcometida(int idInstalacionAcometida) {
        IdInstalacionAcometida = idInstalacionAcometida;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getDiametro() {
        return diametro;
    }

    public void setDiametro(String diametro) {
        this.diametro = diametro;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getFotoActa() {
        return fotoActa;
    }

    public void setFotoActa(String fotoActa) {
        this.fotoActa = fotoActa;
    }

    public String getFotoTC() {
        return fotoTC;
    }

    public void setFotoTC(String fotoTC) {
        this.fotoTC = fotoTC;
    }

    public String getFotoGabinete() {
        return fotoGabinete;
    }

    public void setFotoGabinete(String fotoGabinete) {
        this.fotoGabinete = fotoGabinete;
    }

    public Integer getPosicion() {
        return posicion;
    }

    public void setPosicion(Integer posicion) {
        this.posicion = posicion;
    }

    public String getFusionista() {
        return fusionista;
    }

    public void setFusionista(String fusionista) {
        this.fusionista = fusionista;
    }
}
