package pe.gob.osinergmin.gnr.cgn.actividad;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;

import java.io.File;
import java.util.List;

import gob.osinergmin.gnr.util.Constantes;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionMontanteOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionMontanteOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.Montante;
import pe.gob.osinergmin.gnr.cgn.data.models.MontanteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Precision;
import pe.gob.osinergmin.gnr.cgn.data.models.PrecisionDao;
import pe.gob.osinergmin.gnr.cgn.task.MontanteRx;
import pe.gob.osinergmin.gnr.cgn.util.DownTimer;
import pe.gob.osinergmin.gnr.cgn.util.FormatoFecha;
import pe.gob.osinergmin.gnr.cgn.util.MostrarFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.NetworkUtil;
import pe.gob.osinergmin.gnr.cgn.util.ReducirFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.Util;

public class AcometidaActivity extends BaseActivity {

    private Montante montante;
    private Config config;
    private TableLayout tablaHabilitacion;
    private TextView txtDet_DocumentoPropietarioView;
    private TextView txtDet_nombrePropietarioView;
    private TextView txtDetProyectoView;
    private TextView txtDetMontanteView;
    private TextView txtDet_numeroIntHabilitacionView;
    private TextView txtDet_numeroSolicitudView;
    private TextView txtDet_numeroContratoView;
    private TextView txtDet_fechaSolicitudView;
    private TextView txtDet_fechaAprobacionView;
    private TextView txtDet_tipoProyectoView;
    private TextView txtDet_tipoInstalacionView;
    private TextView txtDet_numeroPuntosInstaacionView;
    private TextView txtDet_memoriaView;
    private ImageView icoMed_fotoUbicacion;
    private ImageView imgMed_fotoUbicacion;
    private SwitchCompat swiMed_aprueba;
    private Button btnMed_rechazar;
    private Toolbar toolbar;
    private MenuItem menuMas;
    private MenuItem menuMenos;
    private String tmpRuta;
    private MontanteDao montanteDao;
    private DownTimer countDownTimer;
    private MParametroOutRODao mParametroOutRODao;
    private FusedLocationProviderClient mFusedLocationClient;
    private MHabilitacionMontanteOutRODao mHabilitacionMontanteOutRODao;
    private LocationCallback mLocationCallback;
    private PrecisionDao precisionDao;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private TextView txtMed_fotoUbicacion;
    private TextView txtMed_medidor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acometida);

        countDownTimer = DownTimer.getInstance();

        montanteDao = ((App) getApplication()).getDaoSession().getMontanteDao();
        mParametroOutRODao = ((App) getApplication()).getDaoSession().getMParametroOutRODao();
        mHabilitacionMontanteOutRODao = ((App) getApplication()).getDaoSession().getMHabilitacionMontanteOutRODao();
        precisionDao = ((App) getApplication()).getDaoSession().getPrecisionDao();

        toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_36dp);

        montante = getIntent().getExtras().getParcelable("MONTANTE");
        config = getIntent().getExtras().getParcelable("CONFIG");

        tablaHabilitacion = findViewById(R.id.tablaHabilitacion);
        txtDet_DocumentoPropietarioView = findViewById(R.id.txtDet_DocumentoPropietarioView);
        txtDet_nombrePropietarioView = findViewById(R.id.txtDet_nombrePropietarioView);
        txtDetProyectoView = findViewById(R.id.txtDetProyectoView);
        txtDetMontanteView = findViewById(R.id.txtDetMontanteView);
        txtDet_numeroIntHabilitacionView = findViewById(R.id.txtDet_numeroIntHabilitacionView);
        txtDet_numeroSolicitudView = findViewById(R.id.txtDet_numeroSolicitudView);
        txtDet_numeroContratoView = findViewById(R.id.txtDet_numeroContratoView);
        txtDet_fechaSolicitudView = findViewById(R.id.txtDet_fechaSolicitudView);
        txtDet_fechaAprobacionView = findViewById(R.id.txtDet_fechaAprobacionView);
        txtDet_tipoProyectoView = findViewById(R.id.txtDet_tipoProyectoView);
        txtDet_tipoInstalacionView = findViewById(R.id.txtDet_tipoInstalacionView);
        txtDet_numeroPuntosInstaacionView = findViewById(R.id.txtDet_numeroPuntosInstaacionView);
        txtDet_memoriaView = findViewById(R.id.txtDet_memoriaView);
        icoMed_fotoUbicacion = findViewById(R.id.icoMed_fotoUbicacion);
        imgMed_fotoUbicacion = findViewById(R.id.imgMed_fotoUbicacion);
        swiMed_aprueba = findViewById(R.id.swiMed_aprueba);
        Button btnMed_evaluando = findViewById(R.id.btnMed_evaluando);
        btnMed_rechazar = findViewById(R.id.btnMed_rechazar);
        txtMed_fotoUbicacion=findViewById(R.id.txtMed_fotoUbicacion);
        txtMed_medidor=findViewById(R.id.txtMed_medidor);

        imgMed_fotoUbicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(
                        AcometidaActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ((App) getApplication()).showToast("No puede realizar la acción porque no se ha dado el permiso de Almacenamiento");
                    return;
                }
                if (Util.isExternalStorageWritable()) {
                    if (Util.getAlbumStorageDir(AcometidaActivity.this)) {
                        File file = Util.getNuevaRutaFoto(montante.getIdHabilitacionMontante().toString(), AcometidaActivity.this);
                        tmpRuta = file.getPath();
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        Uri outputUri = FileProvider.getUriForFile(AcometidaActivity.this, getApplicationContext().getPackageName() + ".provider", file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            ClipData clip = ClipData.newUri(getContentResolver(), "Instalacion", outputUri);
                            intent.setClipData(clip);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        }
                        startActivityForResult(intent, 2);
                    } else {
                        ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                    }
                } else {
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                }
            }
        });

        swiMed_aprueba.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    montante.setApruebaAcometida("1");
                    btnMed_rechazar.setVisibility(View.GONE);
                } else {
                    montante.setApruebaAcometida("0");
                    btnMed_rechazar.setVisibility(View.VISIBLE);
                }
            }
        });

        btnMed_evaluando.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                montanteDao.insertOrReplace(montante);
                Intent intent = new Intent(AcometidaActivity.this, TuberiaMontanteActivity.class);
                intent.putExtra("MONTANTE", montante);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
            }
        });

        btnMed_rechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AcometidaActivity.this, RechazoMontanteActivity.class);
                intent.putExtra("MONTANTE", montante);
                intent.putExtra("CONFIG", config);
                intent.putExtra("TIPO", "");
                startActivity(intent);
            }
        });
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    Precision precision = new Precision();
                    precision.setIdSolicitud(montante.getIdHabilitacionMontante());
                    precision.setLatitud(String.valueOf(location.getLatitude()));
                    precision.setLongitud(String.valueOf(location.getLongitude()));
                    precision.setPrecision(String.valueOf(location.getAccuracy()));
                    precision.setEstado("2");
                    precisionDao.insertOrReplace(precision);
                }
            }
        };
        aceptarPermiso();
    }

    private void validarSelector() {
        if ("".equalsIgnoreCase(montante.getFotoAcometida())) {
            swiMed_aprueba.setChecked(false);
            swiMed_aprueba.setClickable(false);
        } else {
            swiMed_aprueba.setClickable(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1458) {
            if (resultCode == RESULT_CANCELED) {
                Intent intent = new Intent(AcometidaActivity.this, BuscarSuministroActivity.class);
                startActivity(intent);
                finish();
            }
        }
        if (requestCode == 2) {
            if (resultCode != RESULT_CANCELED) {
                montante.setFotoAcometida(tmpRuta);
                ReducirFotoTask reducirFotoTask = new ReducirFotoTask(new ReducirFotoTask.OnReducirFotoTaskCompleted() {
                    @Override
                    public void onReducirFotoTaskCompleted(String result) {
                        if (!("1".equalsIgnoreCase(result))) {
                            ((App) getApplication()).showToast(result);
                        }
                    }
                });
                montanteDao.insertOrReplace(montante);
                reducirFotoTask.execute(montante.getFotoAcometida(), config.getParametro5());
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.createAndStart();
        createLocationRequest();
        cargarSuministro();
        cargaDatos();
        toolbar.setTitle(montante.getObjetoConexion() + " - "+config.getParametro6());
        txtMed_fotoUbicacion.setText(config.getParametro6());
        txtMed_medidor.setText(config.getParametro6());
        validarSelector();
        if (config.getOffline()) {
            MParametroOutRO mParametroOutRO = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_CONFIGURACION_COMPRESION_FOTOS_APLICATIVO_MOVIL))
                    .limit(1)
                    .unique();
            config.setParametro5(mParametroOutRO == null ? "800x600" : mParametroOutRO.getValorParametro());
            MParametroOutRO mParametroOutPrecision = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_FRECUENCIA_TOMA_COORDENADAS_AUDITORIA))
                    .limit(1).unique();
            config.setPrecision(mParametroOutPrecision == null ? "10000" : mParametroOutPrecision.getValorParametro());
        } else {
            if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                if ("".equalsIgnoreCase(config.getParametro5())) {
                    config.setParametro5("800x600");
                }
                if ("".equalsIgnoreCase(config.getPrecision())) {
                    config.setPrecision("10000");
                }
            } else {
                ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
            }
        }
    }

    private void cargaDatos() {
        if (!"".equalsIgnoreCase(montante.getFotoAcometida())) {
            loadBitmap(montante.getFotoAcometida(), imgMed_fotoUbicacion);
            icoMed_fotoUbicacion.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if (!("0".equalsIgnoreCase(montante.getApruebaAcometida()))) {
            swiMed_aprueba.setChecked(true);
        }
    }

    public void loadBitmap(String ruta, final ImageView imageView) {
        MostrarFotoTask mostrarFotoTask = new MostrarFotoTask(new MostrarFotoTask.OnMostrarFotoTaskCompleted() {
            @Override
            public void onMostrarFotoTaskCompleted(Bitmap bitmap) {
                if (bitmap != null) {
                    if (imageView != null) {
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(120, 120);
                        layoutParams.setMargins(10, 10, 10, 10);
                        imageView.setLayoutParams(layoutParams);
                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        imageView.setImageBitmap(bitmap);
                    }
                } else {
                    ((App) getApplication()).showToast("Error al cargar la foto");
                }
            }
        });
        mostrarFotoTask.execute(ruta);
    }

    private void cargarSuministro() {
        txtDet_DocumentoPropietarioView.setText(montante.getObjetoConexion());
        txtDet_nombrePropietarioView.setText(montante.getCup());
        txtDetProyectoView.setText(montante.getNombre());
        txtDetMontanteView.setText(montante.getNombreMontanteProyectoInstalacion());
        txtDet_numeroIntHabilitacionView.setText(montante.getDireccion());
        txtDet_numeroSolicitudView.setText(montante.getTipoProyecto());
        txtDet_numeroContratoView.setText(montante.getFechaRegistro());
        txtDet_fechaSolicitudView.setText(montante.getEsProyectoFise());
        txtDet_fechaAprobacionView.setText(montante.getTienePromocion());
        txtDet_tipoProyectoView.setText(montante.getEsZonaGasificada());
        txtDet_tipoInstalacionView.setText(montante.getTienePlanoInstalacion());
        txtDet_numeroPuntosInstaacionView.setText(montante.getTieneEspecificacion());
        txtDet_memoriaView.setText(montante.getTieneMemoria());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_general, menu);
        menuMas = menu.findItem(R.id.menuMas);
        menuMas.setVisible(true);
        menuMenos = menu.findItem(R.id.menuMenos);
        menuMenos.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menuMas:
                tablaHabilitacion.setVisibility(View.VISIBLE);
                menuMas.setVisible(false);
                menuMenos.setVisible(true);
                return true;

            case R.id.menuMenos:
                tablaHabilitacion.setVisibility(View.GONE);
                menuMas.setVisible(true);
                menuMenos.setVisible(false);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        montanteDao.insertOrReplace(montante);
        Intent intent = new Intent(AcometidaActivity.this, HabilitacionMontanteActivity.class);
        intent.putExtra("MONTANTE", montante);
        intent.putExtra("CONFIG", config);
        startActivity(intent);
        finish();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        countDownTimer.createAndStart();
    }

    private void createLocationRequest() {
        int value = 10000;
        try {
            value = Integer.valueOf(config.getPrecision());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(value);
        mLocationRequest.setFastestInterval(value);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(AcometidaActivity.this, 1458);
                    } catch (IntentSender.SendIntentException sendEx) {
                    }
                }
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            aceptarPermiso();
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
    }

    private void aceptarPermiso() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permiso = Util.getPermisos(AcometidaActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION);
            if (permiso != null) {
                requestPermissions(permiso, pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION:
                String message = Util.onRequestPermissionsResult(permissions, grantResults);
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    @Override
    protected void onDestroy() {
        disposables.clear();
        super.onDestroy();
    }
}
