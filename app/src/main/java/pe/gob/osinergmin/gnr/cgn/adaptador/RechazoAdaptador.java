package pe.gob.osinergmin.gnr.cgn.adaptador;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.MotivoRechazoHabilitacionOutRO;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.data.models.Rechazo;
import pe.gob.osinergmin.gnr.cgn.util.Util;

public class RechazoAdaptador extends BaseAdapter {

    private Context context;
    private CallBack mCallback;
    private List<Rechazo> rechazos;
    private ListView listaRechazos;
    private List<MotivoRechazoHabilitacionOutRO> totalMotivos = new ArrayList<>();
    private List<MotivoRechazoHabilitacionOutRO> motivos = new ArrayList<>();
    private List<MotivoRechazoHabilitacionOutRO> subMotivos = new ArrayList<>();

    public RechazoAdaptador(Context context) {
        this.context = context;
    }

    public void setRechazos(List<Rechazo> rechazos) {
        this.rechazos = new ArrayList<>();
        this.rechazos = rechazos;
        notifyDataSetChanged();
    }

    public void setListaRechazos(ListView listaRechazos) {
        this.listaRechazos = listaRechazos;
    }

    public void setCallback(CallBack callback) {
        mCallback = callback;
    }

    public void setListaTotalMotivos(List<MotivoRechazoHabilitacionOutRO> totalMotivos) {
        this.totalMotivos = totalMotivos;
    }

    @Override
    public int getCount() {
        return this.rechazos.size();
    }

    @Override
    public Object getItem(int position) {
        return this.rechazos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, final View convertView, final ViewGroup parent) {
        View rowView = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = layoutInflater.inflate(R.layout.item_rechazo, parent, false);

            holder.txtRec_rechazo = (TextView) rowView.findViewById(R.id.txtRec_rechazo);
            holder.icoRec_rechazo = (ImageView) rowView.findViewById(R.id.icoRec_rechazo);
            holder.remRec_rechazo = (ImageView) rowView.findViewById(R.id.remRec_rechazo);
            holder.spiRec_rechazo = (Spinner) rowView.findViewById(R.id.spiRec_rechazo);
            holder.spiRec_subRechazo = (Spinner) rowView.findViewById(R.id.spiRec_subRechazo);
            holder.ediRec_observacion = (EditText) rowView.findViewById(R.id.ediRec_observacion);

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        holder.ref = position;

        //rechazos.get(position).setPosicion(position + 1);

        String recurso = rowView.getResources().getString(R.string.txtRec_rechazo);
        String formateada = String.format(recurso, String.valueOf(position + 1));
        holder.txtRec_rechazo.setText(formateada);

        holder.remRec_rechazo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rechazos.remove(holder.ref);
                notifyDataSetChanged();
                Util.setListViewHeightBasedOnChildren(listaRechazos);
                mCallback.onCheck();
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<>(context,
                android.R.layout.simple_spinner_item, getListSpiner(0));
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        holder.spiRec_rechazo.setAdapter(adapter);

        holder.spiRec_rechazo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    rechazos.get(holder.ref).setIdMotivoRechazo(getIdRechazo(parent.getSelectedItem().toString()));
                } else {
                    rechazos.get(holder.ref).setIdMotivoRechazo(0);
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<>(context,
                        android.R.layout.simple_spinner_item,
                        getListSpiner(getIdRechazo(parent.getSelectedItem().toString())));
                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                holder.spiRec_subRechazo.setAdapter(adapter);
                if (0 != rechazos.get(holder.ref).getIdMotivoSubRechazo()) {
                    holder.spiRec_subRechazo.setSelection(getPosicionSubRechazo(rechazos.get(holder.ref).getIdMotivoSubRechazo()));
                }
                validar(holder);
                mCallback.onCheck();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        holder.spiRec_subRechazo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {

                    if (!verifySeleccion(holder, getIdRechazo(parent.getSelectedItem().toString()))) {
                        rechazos.get(holder.ref).setIdMotivoSubRechazo(getIdRechazo(parent.getSelectedItem().toString()));
                    }
                    //genListSelect();
                } else {
                    rechazos.get(holder.ref).setIdMotivoSubRechazo(0);
                }
                validar(holder);
                mCallback.onCheck();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        holder.ediRec_observacion.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    if (s.length() < 300) {
                        rechazos.get(holder.ref).setObservacion(s.toString());
                    } else {
                        ((App) context.getApplicationContext()).showToast(String.format(context.getResources().getString(R.string.app_error_campo), 300));
                    }
                } else {
                    rechazos.get(holder.ref).setObservacion("");
                }
                validar(holder);
                mCallback.onCheck();
            }
        });

        if (0 != rechazos.get(holder.ref).getIdMotivoRechazo()) {
            holder.spiRec_rechazo.setSelection(getPosicionRechazo(rechazos.get(holder.ref).getIdMotivoRechazo()));
        }
        holder.ediRec_observacion.setText(rechazos.get(holder.ref).getObservacion());
        validar(holder);

        return rowView;
    }

    private void validar(ViewHolder holder) {
        if (0 == rechazos.get(holder.ref).getIdMotivoRechazo() ||
                0 == rechazos.get(holder.ref).getIdMotivoSubRechazo()) {
            holder.icoRec_rechazo.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            holder.icoRec_rechazo.setImageResource(R.mipmap.ic_done_black_36dp);
        }
    }

    private List<String> getListSpiner(int id) {
        ArrayList<String> list = new ArrayList<>();
        if (id == 0) {
            list.add("Seleccione motivo");
            motivos.clear();
            MotivoRechazoHabilitacionOutRO motivo = new MotivoRechazoHabilitacionOutRO();
            motivo.setNombreMotivoRechazoHabilitacion("Seleccione motivo");
            motivo.setIdMotivoRechazoHabilitacionPadre(-1);
            motivo.setIdMotivoRechazoHabilitacion(-1);
            motivos.add(motivo);
            for (MotivoRechazoHabilitacionOutRO motivoRechazoHabilitacionOutRO : totalMotivos) {
                if (motivoRechazoHabilitacionOutRO.getIdMotivoRechazoHabilitacionPadre() == null) {
                    list.add(motivoRechazoHabilitacionOutRO.getNombreMotivoRechazoHabilitacion());
                    motivos.add(motivoRechazoHabilitacionOutRO);
                }
            }
        } else {
            list.add("Seleccione submotivo");
            subMotivos.clear();
            MotivoRechazoHabilitacionOutRO subMotivo = new MotivoRechazoHabilitacionOutRO();
            subMotivo.setNombreMotivoRechazoHabilitacion("Seleccione submotivo");
            subMotivo.setIdMotivoRechazoHabilitacionPadre(-1);
            subMotivo.setIdMotivoRechazoHabilitacion(-1);
            subMotivos.add(subMotivo);
            if (id != -1) {
                for (MotivoRechazoHabilitacionOutRO motivoRechazoHabilitacionOutRO : totalMotivos) {
                    if (motivoRechazoHabilitacionOutRO.getIdMotivoRechazoHabilitacionPadre() != null
                            && motivoRechazoHabilitacionOutRO.getIdMotivoRechazoHabilitacionPadre() == id) {
                        list.add(motivoRechazoHabilitacionOutRO.getNombreMotivoRechazoHabilitacion());
                        subMotivos.add(motivoRechazoHabilitacionOutRO);
                    }
                }
            }
        }
        return list;
    }

    private int getPosicionRechazo(int id) {
        int i = 0;
        for (MotivoRechazoHabilitacionOutRO motivoRechazoHabilitacionOutRO : motivos) {
            if (id == motivoRechazoHabilitacionOutRO.getIdMotivoRechazoHabilitacion()) {
                break;
            }
            i++;
        }
        return i;
    }

    private int getPosicionSubRechazo(int id) {
        boolean entro = false;
        int i = 0;
        for (MotivoRechazoHabilitacionOutRO motivoRechazoHabilitacionOutRO : subMotivos) {
            if (id == motivoRechazoHabilitacionOutRO.getIdMotivoRechazoHabilitacion()) {
                entro = true;
                break;
            }
            i++;
        }
        if (entro) {
            return i;
        } else {
            return 0;
        }
    }

    private int getIdRechazo(String rechazo) {
        int i = -1;
        for (MotivoRechazoHabilitacionOutRO motivoRechazoHabilitacionOutRO : totalMotivos) {
            if (rechazo.equalsIgnoreCase(motivoRechazoHabilitacionOutRO.getNombreMotivoRechazoHabilitacion())) {
                i = motivoRechazoHabilitacionOutRO.getIdMotivoRechazoHabilitacion();
                break;
            }
        }
        return i;
    }

    private boolean verifySeleccion(ViewHolder holder, int id) {
        boolean exite = false;
       /* if (select.contains(id)) {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(context,
                    android.R.layout.simple_spinner_item,
                    getListSpiner(rechazos.get(holder.ref).getIdMotivoRechazo()));
            adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            holder.spiRec_subRechazo.setAdapter(adapter);
            exite = true;
            ((App) context.getApplicationContext()).showToast(context, "El submotivo de rechazo ya ha sido seleccionado previamente);
        }*/
        return exite;
    }

    public interface CallBack {
        void onCheck();
    }

    private class ViewHolder {
        int ref;
        TextView txtRec_rechazo;
        ImageView icoRec_rechazo;
        ImageView remRec_rechazo;
        Spinner spiRec_rechazo;
        Spinner spiRec_subRechazo;
        EditText ediRec_observacion;
    }

}
