package pe.gob.osinergmin.gnr.cgn.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class Config implements Parcelable {

    @Id
    private Long id;
    private String username;
    private String recordarUsuario;
    private Boolean offline;
    private String token;
    private String texto;
    private String idHabilitacion;
    private String idHabilitacionMontante;
    private String codigoAcceso;
    private String parametro1;
    private String parametro2;
    private String parametro3;
    private String parametro4;
    private String parametro5;
    private Integer errorPrevisita;
    private Integer errorVisita;
    private Integer errorInstalacion;
    private Boolean desMaestras;
    private Boolean desPreVisitas;
    private Boolean desVisitas;//Montante
    private Boolean desInstalaciones;
    private String nombreUsuario;
    private Boolean sync;
    private Boolean desca;
    private Boolean login;
    private String precision; // en milisegundos
    private String idInstalacionAcometida;
    private Boolean desAcometidas;
    private String parametro6;
    private String idProyectoInstalacion;

    @Generated(hash = 589037648)
    public Config() {
    }

    public Config(Long id) {
        this.id = id;
    }

    @Generated(hash = 1563039615)
    public Config(Long id, String username, String recordarUsuario, Boolean offline, String token, String texto, String idHabilitacion, String idHabilitacionMontante, String codigoAcceso, String parametro1, String parametro2, String parametro3, String parametro4, String parametro5, Integer errorPrevisita, Integer errorVisita, Integer errorInstalacion, Boolean desMaestras, Boolean desPreVisitas, Boolean desVisitas, Boolean desInstalaciones, String nombreUsuario, Boolean sync, Boolean desca, Boolean login, String precision, String idInstalacionAcometida, Boolean desAcometidas, String parametro6, String idProyectoInstalacion) {
        this.id = id;
        this.username = username;
        this.recordarUsuario = recordarUsuario;
        this.offline = offline;
        this.token = token;
        this.texto = texto;
        this.idHabilitacion = idHabilitacion;
        this.idHabilitacionMontante = idHabilitacionMontante;
        this.codigoAcceso = codigoAcceso;
        this.parametro1 = parametro1;
        this.parametro2 = parametro2;
        this.parametro3 = parametro3;
        this.parametro4 = parametro4;
        this.parametro5 = parametro5;
        this.errorPrevisita = errorPrevisita;
        this.errorVisita = errorVisita;
        this.errorInstalacion = errorInstalacion;
        this.desMaestras = desMaestras;
        this.desPreVisitas = desPreVisitas;
        this.desVisitas = desVisitas;
        this.desInstalaciones = desInstalaciones;
        this.nombreUsuario = nombreUsuario;
        this.sync = sync;
        this.desca = desca;
        this.login = login;
        this.precision = precision;
        this.idInstalacionAcometida = idInstalacionAcometida;
        this.desAcometidas = desAcometidas;
        this.parametro6 = parametro6;
        this.idProyectoInstalacion = idProyectoInstalacion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRecordarUsuario() {
        return recordarUsuario;
    }

    public void setRecordarUsuario(String recordarUsuario) {
        this.recordarUsuario = recordarUsuario;
    }

    public Boolean getOffline() {
        return offline;
    }

    public void setOffline(Boolean offline) {
        this.offline = offline;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getIdHabilitacion() {
        return idHabilitacion;
    }

    public void setIdHabilitacion(String idHabilitacion) {
        this.idHabilitacion = idHabilitacion;
    }

    public String getIdHabilitacionMontante() {
        return idHabilitacionMontante;
    }

    public void setIdHabilitacionMontante(String idHabilitacionMontante) {
        this.idHabilitacionMontante = idHabilitacionMontante;
    }

    public String getCodigoAcceso() {
        return codigoAcceso;
    }

    public void setCodigoAcceso(String codigoAcceso) {
        this.codigoAcceso = codigoAcceso;
    }

    public String getParametro1() {
        return parametro1;
    }

    public void setParametro1(String parametro1) {
        this.parametro1 = parametro1;
    }

    public String getParametro2() {
        return parametro2;
    }

    public void setParametro2(String parametro2) {
        this.parametro2 = parametro2;
    }

    public String getParametro3() {
        return parametro3;
    }

    public void setParametro3(String parametro3) {
        this.parametro3 = parametro3;
    }

    public String getParametro4() {
        return parametro4;
    }

    public void setParametro4(String parametro4) {
        this.parametro4 = parametro4;
    }

    public String getParametro5() {
        return parametro5;
    }

    public void setParametro5(String parametro5) {
        this.parametro5 = parametro5;
    }

    public Integer getErrorPrevisita() {
        return errorPrevisita;
    }

    public void setErrorPrevisita(Integer errorPrevisita) {
        this.errorPrevisita = errorPrevisita;
    }

    public Integer getErrorVisita() {
        return errorVisita;
    }

    public void setErrorVisita(Integer errorVisita) {
        this.errorVisita = errorVisita;
    }

    public Integer getErrorInstalacion() {
        return errorInstalacion;
    }

    public void setErrorInstalacion(Integer errorInstalacion) {
        this.errorInstalacion = errorInstalacion;
    }

    public Boolean getDesMaestras() {
        return desMaestras;
    }

    public void setDesMaestras(Boolean desMaestras) {
        this.desMaestras = desMaestras;
    }

    public Boolean getDesPreVisitas() {
        return desPreVisitas;
    }

    public void setDesPreVisitas(Boolean desPreVisitas) {
        this.desPreVisitas = desPreVisitas;
    }

    public Boolean getDesVisitas() {
        return desVisitas;
    }

    public void setDesVisitas(Boolean desVisitas) {
        this.desVisitas = desVisitas;
    }

    public Boolean getDesInstalaciones() {
        return desInstalaciones;
    }

    public void setDesInstalaciones(Boolean desInstalaciones) {
        this.desInstalaciones = desInstalaciones;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public Boolean getSync() {
        return sync;
    }

    public void setSync(Boolean sync) {
        this.sync = sync;
    }

    public Boolean getDesca() {
        return desca;
    }

    public void setDesca(Boolean desca) {
        this.desca = desca;
    }

    public Boolean getLogin() {
        return login;
    }

    public void setLogin(Boolean login) {
        this.login = login;
    }

    public String getPrecision() {
        return precision;
    }

    public void setPrecision(String precision) {
        this.precision = precision;
    }

    public String getIdInstalacionAcometida() {
        return idInstalacionAcometida;
    }

    public void setIdInstalacionAcometida(String idInstalacionAcometida) {
        this.idInstalacionAcometida = idInstalacionAcometida;
    }

    public String getIdProyectoInstalacion() {
        return idProyectoInstalacion;
    }

    public void setIdProyectoInstalacion(String idProyectoInstalacion) {
        this.idProyectoInstalacion = idProyectoInstalacion;
    }

    public Boolean getDesAcometidas() {
        return desAcometidas;
    }

    public void setDesAcometidas(Boolean desAcometidas) {
        this.desAcometidas = desAcometidas;
    }

    public String getParametro6() {
        return parametro6;
    }

    public void setParametro6(String parametro6) {
        this.parametro6 = parametro6;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.username);
        dest.writeString(this.recordarUsuario);
        dest.writeValue(this.offline);
        dest.writeString(this.token);
        dest.writeString(this.texto);
        dest.writeString(this.idHabilitacion);
        dest.writeString(this.idHabilitacionMontante);
        dest.writeString(this.codigoAcceso);
        dest.writeString(this.parametro1);
        dest.writeString(this.parametro2);
        dest.writeString(this.parametro3);
        dest.writeString(this.parametro4);
        dest.writeString(this.parametro5);
        dest.writeValue(this.errorPrevisita);
        dest.writeValue(this.errorVisita);
        dest.writeValue(this.errorInstalacion);
        dest.writeValue(this.desMaestras);
        dest.writeValue(this.desPreVisitas);
        dest.writeValue(this.desVisitas);
        dest.writeValue(this.desInstalaciones);
        dest.writeString(this.nombreUsuario);
        dest.writeValue(this.sync);
        dest.writeValue(this.desca);
        dest.writeValue(this.login);
        dest.writeString(this.precision);
        dest.writeString(this.idInstalacionAcometida);
        dest.writeValue(this.desAcometidas);
        dest.writeString(this.parametro6);
        dest.writeValue(this.idProyectoInstalacion);
    }

    protected Config(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.username = in.readString();
        this.recordarUsuario = in.readString();
        this.offline = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.token = in.readString();
        this.texto = in.readString();
        this.idHabilitacion = in.readString();
        this.idHabilitacionMontante = in.readString();
        this.codigoAcceso = in.readString();
        this.parametro1 = in.readString();
        this.parametro2 = in.readString();
        this.parametro3 = in.readString();
        this.parametro4 = in.readString();
        this.parametro5 = in.readString();
        this.errorPrevisita = (Integer) in.readValue(Integer.class.getClassLoader());
        this.errorVisita = (Integer) in.readValue(Integer.class.getClassLoader());
        this.errorInstalacion = (Integer) in.readValue(Integer.class.getClassLoader());
        this.desMaestras = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.desPreVisitas = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.desVisitas = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.desInstalaciones = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.nombreUsuario = in.readString();
        this.sync = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.desca = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.login = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.precision = in.readString();
        this.idInstalacionAcometida = in.readString();
        this.desAcometidas = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.parametro6 = in.readString();
        this.idProyectoInstalacion = in.readString();
    }

    public static final Creator<Config> CREATOR = new Creator<Config>() {
        @Override
        public Config createFromParcel(Parcel source) {
            return new Config(source);
        }

        @Override
        public Config[] newArray(int size) {
            return new Config[size];
        }
    };
}
