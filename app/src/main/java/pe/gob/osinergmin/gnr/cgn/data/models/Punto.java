package pe.gob.osinergmin.gnr.cgn.data.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class Punto {

    @Id
    private Long id;
    private Integer idHabilitacion;
    private String suministro;
    private String puntoGasodomestico;
    private String puntoDiametroValvula;
    private String puntoFoto;
    private Integer posicion;

    @Generated(hash = 470330007)
    public Punto() {
    }

    public Punto(Long id) {
        this.id = id;
    }

    @Generated(hash = 495828109)
    public Punto(Long id, Integer idHabilitacion, String suministro, String puntoGasodomestico, String puntoDiametroValvula, String puntoFoto, Integer posicion) {
        this.id = id;
        this.idHabilitacion = idHabilitacion;
        this.suministro = suministro;
        this.puntoGasodomestico = puntoGasodomestico;
        this.puntoDiametroValvula = puntoDiametroValvula;
        this.puntoFoto = puntoFoto;
        this.posicion = posicion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdHabilitacion() {
        return idHabilitacion;
    }

    public void setIdHabilitacion(Integer idHabilitacion) {
        this.idHabilitacion = idHabilitacion;
    }

    public String getSuministro() {
        return suministro;
    }

    public void setSuministro(String suministro) {
        this.suministro = suministro;
    }

    public String getPuntoGasodomestico() {
        return puntoGasodomestico;
    }

    public void setPuntoGasodomestico(String puntoGasodomestico) {
        this.puntoGasodomestico = puntoGasodomestico;
    }

    public String getPuntoDiametroValvula() {
        return puntoDiametroValvula;
    }

    public void setPuntoDiametroValvula(String puntoDiametroValvula) {
        this.puntoDiametroValvula = puntoDiametroValvula;
    }

    public String getPuntoFoto() {
        return puntoFoto;
    }

    public void setPuntoFoto(String puntoFoto) {
        this.puntoFoto = puntoFoto;
    }

    public Integer getPosicion() {
        return posicion;
    }

    public void setPosicion(Integer posicion) {
        this.posicion = posicion;
    }
}
