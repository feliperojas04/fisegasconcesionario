package pe.gob.osinergmin.gnr.cgn.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class Acometida implements Parcelable {

    @Id
    private Long id;
    private Integer idInstalacionAcometida;
    private String DocumentoPropietario;
    private String nombrePropietario;
    private String Predio;
    private String numeroIntHabilitacion;
    private String numeroSolicitud;
    private String numeroContrato;
    private String fechaSolicitud;
    private String fechaAprobacion;
    private String usoInstalacion;
    private String tipoProyecto;
    private String tipoInstalacion;
    private String numeroPuntosInstaacion;
    private String tipoAcometida;
    private String suministro;
    private String fusionista;
    private String resultado;
    private String diametro;
    private String longitud;
    private String material;
    private String fotoActa;
    private String fotoTC;
    private String fotoGabinete;
    private String fechaRegistroOffline;
    private Boolean grabar;
    private String nombreTipoInstalacionAcometida;
    private String TipoInstalacionAcometida;
    private String tipoGabinete;/*PAPU 2011*/
    private boolean activo;


    @Generated(hash = 1698844285)
    public Acometida() {
    }

    public Acometida(Long id) {
        this.id = id;
    }

    @Generated(hash = 78125792)
    public Acometida(Long id, Integer idInstalacionAcometida, String DocumentoPropietario, String nombrePropietario, String Predio, String numeroIntHabilitacion, String numeroSolicitud, String numeroContrato, String fechaSolicitud, String fechaAprobacion, String usoInstalacion, String tipoProyecto, String tipoInstalacion, String numeroPuntosInstaacion, String tipoAcometida, String suministro, String fusionista, String resultado, String diametro, String longitud, String material, String fotoActa, String fotoTC, String fotoGabinete, String fechaRegistroOffline, Boolean grabar, String nombreTipoInstalacionAcometida, String TipoInstalacionAcometida,
            String tipoGabinete, boolean activo) {
        this.id = id;
        this.idInstalacionAcometida = idInstalacionAcometida;
        this.DocumentoPropietario = DocumentoPropietario;
        this.nombrePropietario = nombrePropietario;
        this.Predio = Predio;
        this.numeroIntHabilitacion = numeroIntHabilitacion;
        this.numeroSolicitud = numeroSolicitud;
        this.numeroContrato = numeroContrato;
        this.fechaSolicitud = fechaSolicitud;
        this.fechaAprobacion = fechaAprobacion;
        this.usoInstalacion = usoInstalacion;
        this.tipoProyecto = tipoProyecto;
        this.tipoInstalacion = tipoInstalacion;
        this.numeroPuntosInstaacion = numeroPuntosInstaacion;
        this.tipoAcometida = tipoAcometida;
        this.suministro = suministro;
        this.fusionista = fusionista;
        this.resultado = resultado;
        this.diametro = diametro;
        this.longitud = longitud;
        this.material = material;
        this.fotoActa = fotoActa;
        this.fotoTC = fotoTC;
        this.fotoGabinete = fotoGabinete;
        this.fechaRegistroOffline = fechaRegistroOffline;
        this.grabar = grabar;
        this.nombreTipoInstalacionAcometida = nombreTipoInstalacionAcometida;
        this.TipoInstalacionAcometida = TipoInstalacionAcometida;
        this.tipoGabinete = tipoGabinete;
        this.activo = activo;
    }

    /*PAPU 2011 - 0*/
    public String getTipoGabinete() {
        return tipoGabinete;
    }

    public void setTipoGabinete(String tipoGabinete) {
        this.tipoGabinete = tipoGabinete;
    }
    /*PAPU 2011 - 1*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdInstalacionAcometida() {
        return idInstalacionAcometida;
    }

    public void setIdInstalacionAcometida(Integer idInstalacionAcometida) {
        this.idInstalacionAcometida = idInstalacionAcometida;
    }

    public String getDocumentoPropietario() {
        return DocumentoPropietario;
    }

    public void setDocumentoPropietario(String documentoPropietario) {
        DocumentoPropietario = documentoPropietario;
    }

    public String getNombrePropietario() {
        return nombrePropietario;
    }

    public void setNombrePropietario(String nombrePropietario) {
        this.nombrePropietario = nombrePropietario;
    }

    public String getPredio() {
        return Predio;
    }

    public void setPredio(String predio) {
        Predio = predio;
    }

    public String getNumeroIntHabilitacion() {
        return numeroIntHabilitacion;
    }

    public void setNumeroIntHabilitacion(String numeroIntHabilitacion) {
        this.numeroIntHabilitacion = numeroIntHabilitacion;
    }

    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    public String getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(String fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getFechaAprobacion() {
        return fechaAprobacion;
    }

    public void setFechaAprobacion(String fechaAprobacion) {
        this.fechaAprobacion = fechaAprobacion;
    }

    public String getUsoInstalacion() {
        return usoInstalacion;
    }

    public void setUsoInstalacion(String usoInstalacion) {
        this.usoInstalacion = usoInstalacion;
    }

    public String getTipoProyecto() {
        return tipoProyecto;
    }

    public void setTipoProyecto(String tipoProyecto) {
        this.tipoProyecto = tipoProyecto;
    }

    public String getTipoInstalacion() {
        return tipoInstalacion;
    }

    public void setTipoInstalacion(String tipoInstalacion) {
        this.tipoInstalacion = tipoInstalacion;
    }

    public String getNumeroPuntosInstaacion() {
        return numeroPuntosInstaacion;
    }

    public void setNumeroPuntosInstaacion(String numeroPuntosInstaacion) {
        this.numeroPuntosInstaacion = numeroPuntosInstaacion;
    }

    public String getTipoAcometida() {
        return tipoAcometida;
    }

    public void setTipoAcometida(String tipoAcometida) {
        this.tipoAcometida = tipoAcometida;
    }

    public String getSuministro() {
        return suministro;
    }

    public void setSuministro(String suministro) {
        this.suministro = suministro;
    }

    public String getFusionista() {
        return fusionista;
    }

    public void setFusionista(String fusionista) {
        this.fusionista = fusionista;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getDiametro() {
        return diametro;
    }

    public void setDiametro(String diametro) {
        this.diametro = diametro;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getFotoActa() {
        return fotoActa;
    }

    public void setFotoActa(String fotoActa) {
        this.fotoActa = fotoActa;
    }

    public String getFotoTC() {
        return fotoTC;
    }

    public void setFotoTC(String fotoTC) {
        this.fotoTC = fotoTC;
    }

    public String getFotoGabinete() {
        return fotoGabinete;
    }

    public void setFotoGabinete(String fotoGabinete) {
        this.fotoGabinete = fotoGabinete;
    }

    public String getFechaRegistroOffline() {
        return fechaRegistroOffline;
    }

    public void setFechaRegistroOffline(String fechaRegistroOffline) {
        this.fechaRegistroOffline = fechaRegistroOffline;
    }

    public Boolean getGrabar() {
        return grabar;
    }

    public void setGrabar(Boolean grabar) {
        this.grabar = grabar;
    }

    public String getNombreTipoInstalacionAcometida() {
        return nombreTipoInstalacionAcometida;
    }

    public void setNombreTipoInstalacionAcometida(String nombreTipoInstalacionAcometida) {
        this.nombreTipoInstalacionAcometida = nombreTipoInstalacionAcometida;
    }

    public String getTipoInstalacionAcometida() {
        return TipoInstalacionAcometida;
    }

    public void setTipoInstalacionAcometida(String tipoInstalacionAcometida) {
        TipoInstalacionAcometida = tipoInstalacionAcometida;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.idInstalacionAcometida);
        dest.writeString(this.DocumentoPropietario);
        dest.writeString(this.nombrePropietario);
        dest.writeString(this.Predio);
        dest.writeString(this.numeroIntHabilitacion);
        dest.writeString(this.numeroSolicitud);
        dest.writeString(this.numeroContrato);
        dest.writeString(this.fechaSolicitud);
        dest.writeString(this.fechaAprobacion);
        dest.writeString(this.usoInstalacion);
        dest.writeString(this.tipoProyecto);
        dest.writeString(this.tipoInstalacion);
        dest.writeString(this.numeroPuntosInstaacion);
        dest.writeString(this.tipoAcometida);
        dest.writeString(this.suministro);
        dest.writeString(this.fusionista);
        dest.writeString(this.resultado);
        dest.writeString(this.diametro);
        dest.writeString(this.longitud);
        dest.writeString(this.material);
        dest.writeString(this.fotoActa);
        dest.writeString(this.fotoTC);
        dest.writeString(this.fotoGabinete);
        dest.writeString(this.fechaRegistroOffline);
        dest.writeValue(this.grabar);
        dest.writeString(this.nombreTipoInstalacionAcometida);
        dest.writeString(this.TipoInstalacionAcometida);

        /*PAPU 2011 - 0*/
        dest.writeString(this.tipoGabinete);
        /*PAPU 2011 - 1*/
    }

    public boolean getActivo() {
        return this.activo;
    }

    protected Acometida(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.idInstalacionAcometida = (Integer) in.readValue(Integer.class.getClassLoader());
        this.DocumentoPropietario = in.readString();
        this.nombrePropietario = in.readString();
        this.Predio = in.readString();
        this.numeroIntHabilitacion = in.readString();
        this.numeroSolicitud = in.readString();
        this.numeroContrato = in.readString();
        this.fechaSolicitud = in.readString();
        this.fechaAprobacion = in.readString();
        this.usoInstalacion = in.readString();
        this.tipoProyecto = in.readString();
        this.tipoInstalacion = in.readString();
        this.numeroPuntosInstaacion = in.readString();
        this.tipoAcometida = in.readString();
        this.suministro = in.readString();
        this.fusionista = in.readString();
        this.resultado = in.readString();
        this.diametro = in.readString();
        this.longitud = in.readString();
        this.material = in.readString();
        this.fotoActa = in.readString();
        this.fotoTC = in.readString();
        this.fotoGabinete = in.readString();
        this.fechaRegistroOffline = in.readString();
        this.grabar = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.nombreTipoInstalacionAcometida = in.readString();
        this.TipoInstalacionAcometida = in.readString();

        /*PAPU 2011 - 0*/
        this.tipoGabinete = in.readString();
        /*PAPU 2011 - 1*/
    }

    public static final Creator<Acometida> CREATOR = new Creator<Acometida>() {
        @Override
        public Acometida createFromParcel(Parcel source) {
            return new Acometida(source);
        }

        @Override
        public Acometida[] newArray(int size) {
            return new Acometida[size];
        }
    };
}
