package pe.gob.osinergmin.gnr.cgn.task;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import gob.osinergmin.gnr.domain.dto.rest.in.filter.FilterHabilitacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListBaseHabilitacionOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.util.Urls;

public class ListaBaseHabilitacion extends AsyncTask<Config, Void, ListBaseHabilitacionOutRO> {

    private OnListaBaseHabilitacionCompleted listener = null;

    public ListaBaseHabilitacion(OnListaBaseHabilitacionCompleted listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected ListBaseHabilitacionOutRO doInBackground(Config... configs) {
        try {

            Config config = configs[0];

            String url = Urls.getBase() + Urls.getListaHabilitacion();

            FilterHabilitacionInRO filterHabilitacionInRO = new FilterHabilitacionInRO();
            filterHabilitacionInRO.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);
            filterHabilitacionInRO.setPage(config.getErrorPrevisita());
            filterHabilitacionInRO.setRowsPerPage(config.getErrorVisita());
            if (!("".equalsIgnoreCase(config.getTexto()))) {
                filterHabilitacionInRO.setTexto(config.getTexto());
            }

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Authorization", "Bearer " + config.getToken());

            HttpEntity<FilterHabilitacionInRO> httpEntity = new HttpEntity<>(filterHabilitacionInRO, httpHeaders);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            return restTemplate.postForObject(url, httpEntity, ListBaseHabilitacionOutRO.class);

        } catch (RestClientException e) {
            Log.e("LISTATALLER", e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(ListBaseHabilitacionOutRO listBaseHabilitacionOutRO) {
        super.onPostExecute(listBaseHabilitacionOutRO);
        listener.onListaBaseHabilitacionCompled(listBaseHabilitacionOutRO);
    }

    public interface OnListaBaseHabilitacionCompleted {
        void onListaBaseHabilitacionCompled(ListBaseHabilitacionOutRO listBaseHabilitacionOutRO);
    }
}
