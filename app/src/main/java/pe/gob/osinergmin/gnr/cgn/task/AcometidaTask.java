package pe.gob.osinergmin.gnr.cgn.task;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import gob.osinergmin.gnr.domain.dto.rest.in.filter.FilterInstalacionAcometidaInRO;
import gob.osinergmin.gnr.domain.dto.rest.out.InstalacionAcometidaOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.util.Urls;

public class AcometidaTask extends AsyncTask<Config, Void, InstalacionAcometidaOutRO> {

    private OnInstalacionAcometidaCompleted listener = null;

    public AcometidaTask(OnInstalacionAcometidaCompleted listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected InstalacionAcometidaOutRO doInBackground(Config... configs) {
        try{
            Config config = configs[0];

            String url = Urls.getBase() + Urls.getAcometidaObtener();

            FilterInstalacionAcometidaInRO filter = new FilterInstalacionAcometidaInRO();
            filter.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);
            filter.setIdInstalacionAcometida(Integer.parseInt(config.getIdInstalacionAcometida()));

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Authorization", "Bearer " + config.getToken());

            HttpEntity<FilterInstalacionAcometidaInRO> httpEntity = new HttpEntity<>(filter, httpHeaders);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            return restTemplate.postForObject(url, httpEntity, InstalacionAcometidaOutRO.class);

        } catch (RestClientException e) {
            Log.e("ACOMETIDA", e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(InstalacionAcometidaOutRO instalacionAcometidaOutRO) {
        super.onPostExecute(instalacionAcometidaOutRO);
        listener.onInstalacionAcometidaCompled(instalacionAcometidaOutRO);
    }

    public interface OnInstalacionAcometidaCompleted {
        void onInstalacionAcometidaCompled(InstalacionAcometidaOutRO instalacionAcometidaOutRO);
    }

}
