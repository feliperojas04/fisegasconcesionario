package pe.gob.osinergmin.gnr.cgn.data.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class Usuario {

    @Id
    private Long id;
    private String username;
    private String password;
    private String tipo;
    private String dni;
    private String token;
    private String recordarUsuario;
    private String texto;
    private String idHabilitacion;
    private String codigoAcceso;
    private String parametro1;
    private String parametro2;
    private String parametro3;
    private String parametro4;
    private String parametro5;

    @Generated(hash = 562950751)
    public Usuario() {
    }

    public Usuario(Long id) {
        this.id = id;
    }

    @Generated(hash = 218313008)
    public Usuario(Long id, String username, String password, String tipo, String dni, String token, String recordarUsuario, String texto, String idHabilitacion, String codigoAcceso, String parametro1, String parametro2, String parametro3, String parametro4, String parametro5) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.tipo = tipo;
        this.dni = dni;
        this.token = token;
        this.recordarUsuario = recordarUsuario;
        this.texto = texto;
        this.idHabilitacion = idHabilitacion;
        this.codigoAcceso = codigoAcceso;
        this.parametro1 = parametro1;
        this.parametro2 = parametro2;
        this.parametro3 = parametro3;
        this.parametro4 = parametro4;
        this.parametro5 = parametro5;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRecordarUsuario() {
        return recordarUsuario;
    }

    public void setRecordarUsuario(String recordarUsuario) {
        this.recordarUsuario = recordarUsuario;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getIdHabilitacion() {
        return idHabilitacion;
    }

    public void setIdHabilitacion(String idHabilitacion) {
        this.idHabilitacion = idHabilitacion;
    }

    public String getCodigoAcceso() {
        return codigoAcceso;
    }

    public void setCodigoAcceso(String codigoAcceso) {
        this.codigoAcceso = codigoAcceso;
    }

    public String getParametro1() {
        return parametro1;
    }

    public void setParametro1(String parametro1) {
        this.parametro1 = parametro1;
    }

    public String getParametro2() {
        return parametro2;
    }

    public void setParametro2(String parametro2) {
        this.parametro2 = parametro2;
    }

    public String getParametro3() {
        return parametro3;
    }

    public void setParametro3(String parametro3) {
        this.parametro3 = parametro3;
    }

    public String getParametro4() {
        return parametro4;
    }

    public void setParametro4(String parametro4) {
        this.parametro4 = parametro4;
    }

    public String getParametro5() {
        return parametro5;
    }

    public void setParametro5(String parametro5) {
        this.parametro5 = parametro5;
    }

}
