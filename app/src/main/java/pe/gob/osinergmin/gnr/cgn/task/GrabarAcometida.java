package pe.gob.osinergmin.gnr.cgn.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.in.InstalacionAcometidaInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.InstalacionAcometidaProyectoInstalacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.InstalacionAcometidaTuberiaConexionProyectoInstalacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.ObservacionInstalacionAcometidaInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.list.ListObservacionInstalacionAcometidaInRO;
import gob.osinergmin.gnr.domain.dto.rest.out.InstalacionAcometidaOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.data.models.AcometidaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.ConfigDao;
import pe.gob.osinergmin.gnr.cgn.data.models.ObservacionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.PrecisionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Acometida;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.Observacion;
import pe.gob.osinergmin.gnr.cgn.data.models.Precision;
import pe.gob.osinergmin.gnr.cgn.data.models.Tuberias;
import pe.gob.osinergmin.gnr.cgn.util.Urls;

import static gob.osinergmin.gnr.util.Constantes.RESULTADO_INSTALACION_ACOMETIDA_CONCLUIDA;

public class GrabarAcometida extends AsyncTask<Acometida, Void, InstalacionAcometidaOutRO> {

    private final WeakReference<Context> context;
    private OnGrabarAcometidaAsyncTaskCompleted listener = null;
    private Config config;
    private ConfigDao configDao;
    private ObservacionDao observacionDao;
    private AcometidaDao acometidaDao;
    private PrecisionDao precisionDao;

    public GrabarAcometida(OnGrabarAcometidaAsyncTaskCompleted listener, Context context) {
        this.listener = listener;
        this.context = new WeakReference<>(context);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Context context = this.context.get();
        if (context != null) {
            observacionDao = ((App) context.getApplicationContext()).getDaoSession().getObservacionDao();
            configDao = ((App) context.getApplicationContext()).getDaoSession().getConfigDao();
            config = configDao.queryBuilder().limit(1).unique();
            precisionDao = ((App) context.getApplicationContext()).getDaoSession().getPrecisionDao();
        }
    }

    @Override
    protected InstalacionAcometidaOutRO doInBackground(Acometida... acometidas) {
        try {
            Acometida acometida = acometidas[0];
            Precision precision = precisionDao.queryBuilder()
                    .where(PrecisionDao.Properties.Estado.eq("3"),
                            PrecisionDao.Properties.IdSolicitud.eq(acometida.getIdInstalacionAcometida()))
                    .orderAsc(PrecisionDao.Properties.Precision).limit(1).unique();
            String url = Urls.getBase() + Urls.getAcometidaRegistrar();

            InstalacionAcometidaInRO instalacionAcometida = new InstalacionAcometidaInRO();
            instalacionAcometida.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);
            instalacionAcometida.setIdInstalacionAcometida(acometida.getIdInstalacionAcometida());

            instalacionAcometida.setIdFusionistaRegistro("".equalsIgnoreCase(acometida.getFusionista()) ? null : Integer.parseInt(acometida.getFusionista()));
            instalacionAcometida.setResultadoInstalacionAcometida("".equalsIgnoreCase(acometida.getResultado()) ? null : acometida.getResultado());
            instalacionAcometida.setTipoGabinete("".equalsIgnoreCase(acometida.getTipoGabinete()) ? null : acometida.getTipoGabinete());
            instalacionAcometida.setFechaRegistroOfflineInstalacionAcometida(acometida.getFechaRegistroOffline());
            instalacionAcometida.setCoordenadasAuditoriaInstalacionAcometida(String.format("%s,%s", precision.getLatitud(), precision.getLongitud()));
            instalacionAcometida.setPrecisionCoordenadasAuditoriaInstalacionAcometida(BigDecimal.valueOf(Double.valueOf(precision.getPrecision())));

            if (RESULTADO_INSTALACION_ACOMETIDA_CONCLUIDA.equalsIgnoreCase(acometida.getResultado())) {
                instalacionAcometida.setDiametroTuberiaConexionInstalacionAcometida("".equalsIgnoreCase(acometida.getDiametro()) ? null : BigDecimal.valueOf(Float.parseFloat(acometida.getDiametro())));
                instalacionAcometida.setLongitudTuberiaConexionInstalacionAcometida("".equalsIgnoreCase(acometida.getLongitud()) ? null : BigDecimal.valueOf(Float.parseFloat(acometida.getLongitud())));
                instalacionAcometida.setIdMaterialInstalacion("".equalsIgnoreCase(acometida.getMaterial()) ? null : Integer.parseInt(acometida.getMaterial()));
            } else {
                List<Observacion> observacions = observacionDao.queryBuilder().where(ObservacionDao.Properties.IdInstalacionMontante.eq(acometida.getIdInstalacionAcometida())).list();
                ListObservacionInstalacionAcometidaInRO listObservacionInstalacionAcometidaInRO = new ListObservacionInstalacionAcometidaInRO();
                List<ObservacionInstalacionAcometidaInRO> observacionInstalacionAcometidaInROS = new ArrayList<>();
                int k = 1;
                for (Observacion observacion : observacions) {
                    ObservacionInstalacionAcometidaInRO tmp = new ObservacionInstalacionAcometidaInRO();
                    tmp.setOrdenObservacionInstalacionAcometida(k);
                    tmp.setIdTipoObservacion(Integer.parseInt(observacion.getTipoObservacion()));
                    tmp.setDescripcionObservacionInstalacionAcometida(observacion.getDescripcionObservacion());
                    observacionInstalacionAcometidaInROS.add(tmp);
                    k++;
                }
                listObservacionInstalacionAcometidaInRO.setObservacionInstalacionAcometida(observacionInstalacionAcometidaInROS);
                instalacionAcometida.setObservaciones(listObservacionInstalacionAcometidaInRO);
            }

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
            httpHeaders.add("Authorization", "Bearer " + config.getToken());

            MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
            parts.add(Constantes.PARAM_NAME_INSTALACION_ACOMETIDA, instalacionAcometida);
            if (RESULTADO_INSTALACION_ACOMETIDA_CONCLUIDA.equalsIgnoreCase(acometida.getResultado())) {
                parts.add(Constantes.PARAM_NAME_FOTO_ACTA_INSTALACION_TUBERIA_CONEXION_ACOMETIDA, "".equalsIgnoreCase(acometida.getFotoActa()) ? null : new FileSystemResource(acometida.getFotoActa()));
                parts.add(Constantes.PARAM_NAME_FOTO_TUBERIA_CONEXION_TERMINADA, "".equalsIgnoreCase(acometida.getFotoTC()) ? null : new FileSystemResource(acometida.getFotoTC()));
                parts.add(Constantes.PARAM_NAME_FOTO_GABINETE_MANIFOLD_TERMINADO, "".equalsIgnoreCase(acometida.getFotoGabinete()) ? null : new FileSystemResource(acometida.getFotoGabinete()));
            }

            ObjectMapper mapper = new ObjectMapper();String jsonString = mapper.writeValueAsString(instalacionAcometida);
            System.out.println("JsonAcom: "+jsonString);

            HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(parts, httpHeaders);
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            return restTemplate.postForObject(url, httpEntity, InstalacionAcometidaOutRO.class);

        } catch (RestClientException | NullPointerException | IllegalArgumentException | JsonProcessingException e) {
            Log.e("GRABAR", e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(InstalacionAcometidaOutRO instalacionAcometidaOutRO) {
        super.onPostExecute(instalacionAcometidaOutRO);
        listener.onGrabarAcometidaAsyncTaskCompleted(instalacionAcometidaOutRO);
    }

    public interface OnGrabarAcometidaAsyncTaskCompleted {
        void onGrabarAcometidaAsyncTaskCompleted(InstalacionAcometidaOutRO instalacionAcometidaOutRO);
    }
}
