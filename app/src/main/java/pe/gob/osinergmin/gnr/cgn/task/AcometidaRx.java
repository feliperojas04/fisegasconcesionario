package pe.gob.osinergmin.gnr.cgn.task;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import gob.osinergmin.gnr.domain.dto.rest.in.filter.FilterInstalacionAcometidaInRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListInstalacionAcometidaOutRO;
import gob.osinergmin.gnr.util.Constantes;
import io.reactivex.Single;
import pe.gob.osinergmin.gnr.cgn.util.Urls;

public class AcometidaRx {
    public static Single<ListInstalacionAcometidaOutRO> getAcometidaListar(String token) {
        return Single.create(emitter -> {
            try {

                String url = Urls.getBase() + Urls.getAcometidaListar();

                FilterInstalacionAcometidaInRO filter = new FilterInstalacionAcometidaInRO();
                filter.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);

                HttpHeaders httpHeaders = new HttpHeaders();
                httpHeaders.setContentType(MediaType.APPLICATION_JSON);
                httpHeaders.add("Authorization", "Bearer " + token);

                HttpEntity<FilterInstalacionAcometidaInRO> httpEntity = new HttpEntity<>(filter, httpHeaders);

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                ListInstalacionAcometidaOutRO list = restTemplate.postForObject(url, httpEntity, ListInstalacionAcometidaOutRO.class);
                emitter.onSuccess(list);
            } catch (RestClientException e) {
                emitter.onError(e);
            }
        });
    }
}
