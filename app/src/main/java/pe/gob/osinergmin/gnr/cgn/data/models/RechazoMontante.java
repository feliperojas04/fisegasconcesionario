package pe.gob.osinergmin.gnr.cgn.data.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class RechazoMontante {

    @Id
    private Long id;
    private Integer idHabilitacionMontante;
    private Integer idMotivoRechazo;
    private Integer idMotivoSubRechazo;
    private String observacion;

    @Generated(hash = 2110160194)
    public RechazoMontante() {
    }

    public RechazoMontante(Long id) {
        this.id = id;
    }

    @Generated(hash = 1994903471)
    public RechazoMontante(Long id, Integer idHabilitacionMontante, Integer idMotivoRechazo, Integer idMotivoSubRechazo, String observacion) {
        this.id = id;
        this.idHabilitacionMontante = idHabilitacionMontante;
        this.idMotivoRechazo = idMotivoRechazo;
        this.idMotivoSubRechazo = idMotivoSubRechazo;
        this.observacion = observacion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdHabilitacionMontante() {
        return idHabilitacionMontante;
    }

    public void setIdHabilitacionMontante(Integer idHabilitacionMontante) {
        this.idHabilitacionMontante = idHabilitacionMontante;
    }

    public Integer getIdMotivoRechazo() {
        return idMotivoRechazo;
    }

    public void setIdMotivoRechazo(Integer idMotivoRechazo) {
        this.idMotivoRechazo = idMotivoRechazo;
    }

    public Integer getIdMotivoSubRechazo() {
        return idMotivoSubRechazo;
    }

    public void setIdMotivoSubRechazo(Integer idMotivoSubRechazo) {
        this.idMotivoSubRechazo = idMotivoSubRechazo;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

}
