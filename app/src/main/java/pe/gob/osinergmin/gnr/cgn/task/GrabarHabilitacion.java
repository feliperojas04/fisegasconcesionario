package pe.gob.osinergmin.gnr.cgn.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.commons.io.FileUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.in.AmbientePredioHabilitacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.HabilitacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.MotivoRechazoHabilitacionHabilitacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.PuntoInstalacionHabilitacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.list.ListAmbientePredioHabilitacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.list.ListMotivoRechazoHabilitacionHabilitacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.list.ListPuntoInstalacionHabilitacionInRO;
import gob.osinergmin.gnr.domain.dto.rest.out.HabilitacionOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.data.models.Ambiente;
import pe.gob.osinergmin.gnr.cgn.data.models.AmbienteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.ConfigDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Precision;
import pe.gob.osinergmin.gnr.cgn.data.models.PrecisionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Punto;
import pe.gob.osinergmin.gnr.cgn.data.models.PuntoDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Rechazo;
import pe.gob.osinergmin.gnr.cgn.data.models.RechazoDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Suministro;
import pe.gob.osinergmin.gnr.cgn.util.Urls;

public class GrabarHabilitacion extends AsyncTask<Suministro, Void, HabilitacionOutRO> {

    private final WeakReference<Context> context;
    private OnGrabarHabilitacionAsyncTaskCompleted listener = null;
    private Config config;
    private ConfigDao configDao;
    private PuntoDao puntoDao;
    private AmbienteDao ambienteDao;
    private RechazoDao rechazoDao;
    private PrecisionDao precisionDao;

    public GrabarHabilitacion(OnGrabarHabilitacionAsyncTaskCompleted listener, Context context) {
        this.listener = listener;
        this.context = new WeakReference<>(context);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Context context = this.context.get();
        if (context != null) {
            puntoDao = ((App) context.getApplicationContext()).getDaoSession().getPuntoDao();
            configDao = ((App) context.getApplicationContext()).getDaoSession().getConfigDao();
            ambienteDao = ((App) context.getApplicationContext()).getDaoSession().getAmbienteDao();
            rechazoDao = ((App) context.getApplicationContext()).getDaoSession().getRechazoDao();
            config = configDao.queryBuilder().limit(1).unique();
            precisionDao = ((App) context.getApplicationContext()).getDaoSession().getPrecisionDao();
        }
    }

    @Override
    protected HabilitacionOutRO doInBackground(Suministro... suministros) {
        try {

            Suministro suministro = suministros[0];
            Precision precision = precisionDao.queryBuilder()
                    .where(PrecisionDao.Properties.Estado.eq("1"),
                            PrecisionDao.Properties.IdSolicitud.eq(suministro.getIdHabilitacion()))
                    .orderAsc(PrecisionDao.Properties.Precision).limit(1).unique();
            String url = Urls.getBase() + Urls.getRegistrarHabilitacion();

            HabilitacionInRO habilitacionInRO = new HabilitacionInRO();
            habilitacionInRO.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);
            habilitacionInRO.setIdHabilitacion(suministro.getIdHabilitacion());
            habilitacionInRO.setAprobadoHabilitacion("1".equalsIgnoreCase(suministro.getSuministroAprueba()) ? Constantes.VALOR_SI : Constantes.VALOR_NO);
            habilitacionInRO.setAprobadoMedidor("1".equalsIgnoreCase(suministro.getMedidorAprueba()) ? Constantes.VALOR_SI : Constantes.VALOR_NO);
            habilitacionInRO.setIdTipoAcometida("".equalsIgnoreCase(suministro.getMedidorTipoAcomedida()) ? null : Integer.parseInt(suministro.getMedidorTipoAcomedida()));
            habilitacionInRO.setAprobadoValvulaCorteGeneral("1".equalsIgnoreCase(suministro.getValvulaAprueba()) ? Constantes.VALOR_SI : Constantes.VALOR_NO);
            habilitacionInRO.setDiametroValvulaCorteGeneral("".equalsIgnoreCase(suministro.getValvulaDiametro()) ? null : BigDecimal.valueOf(Float.parseFloat(suministro.getValvulaDiametro())));
            habilitacionInRO.setAprobadoTuberia("1".equalsIgnoreCase(suministro.getTuberiaAprueba()) ? Constantes.VALOR_SI : Constantes.VALOR_NO);
            habilitacionInRO.setTipoInstalacion("".equalsIgnoreCase(suministro.getTuberiaTipoInstalacion()) ? null : suministro.getTuberiaTipoInstalacion());
            habilitacionInRO.setIdMarcaAccesorio("".equalsIgnoreCase(suministro.getIdMarcaAccesorio()) ? null : Integer.parseInt(suministro.getIdMarcaAccesorio()));
            habilitacionInRO.setIdMarcaTuberia("".equalsIgnoreCase(suministro.getIdMarcaTuberia()) ? null : Integer.parseInt(suministro.getIdMarcaTuberia()));
            habilitacionInRO.setIdMaterialInstalacion("".equalsIgnoreCase(suministro.getTuberiaMaterialInstalacion()) ? null : Integer.parseInt(suministro.getTuberiaMaterialInstalacion()));
            habilitacionInRO.setLongitudTotalInstalacion("".equalsIgnoreCase(suministro.getTuberiaLongitudTotal()) ? null : BigDecimal.valueOf(Float.parseFloat(suministro.getTuberiaLongitudTotal())));
            habilitacionInRO.setAprobadoPuntosInstalacion("1".equalsIgnoreCase(suministro.getPuntosAprueba()) ? Constantes.VALOR_SI : Constantes.VALOR_NO);
            habilitacionInRO.setAprobadoAmbientes("1".equalsIgnoreCase(suministro.getAmbientesAprueba()) ? Constantes.VALOR_SI : Constantes.VALOR_NO);
            habilitacionInRO.setAprobadoPruebaMonoxido("1".equalsIgnoreCase(suministro.getMonoxidoAprueba()) ? Constantes.VALOR_SI : Constantes.VALOR_NO);
            habilitacionInRO.setAprobadoPresionArtefacto("1".equalsIgnoreCase(suministro.getArtefactoAprueba()) ? Constantes.VALOR_SI : Constantes.VALOR_NO);
            habilitacionInRO.setValorPresionArtefacto("".equalsIgnoreCase(suministro.getArtefactoPresion()) ? null : BigDecimal.valueOf(Float.parseFloat(suministro.getArtefactoPresion())));
            habilitacionInRO.setAprobadoHermeticidad("1".equalsIgnoreCase(suministro.getHermeticidadAprueba()) ? Constantes.VALOR_SI : Constantes.VALOR_NO);
            habilitacionInRO.setPresionInicialPruebaHermeticidad("".equalsIgnoreCase(suministro.getHermeticidadPruebaInicial()) ? null : BigDecimal.valueOf(Float.parseFloat(suministro.getHermeticidadPruebaInicial())));
            habilitacionInRO.setPresionFinalPruebaHermeticidad("".equalsIgnoreCase(suministro.getHermeticidadPruebaFinal()) ? null : BigDecimal.valueOf(Float.parseFloat(suministro.getHermeticidadPruebaFinal())));
            habilitacionInRO.setTiempoPruebaHermeticidad("".equalsIgnoreCase(suministro.getHermeticidadPruebaTiempo()) ? null : BigDecimal.valueOf(Float.parseFloat(suministro.getHermeticidadPruebaTiempo())));
            habilitacionInRO.setFechaRegistroOfflineHabilitacion(suministro.getFechaRegistroOffline());
            habilitacionInRO.setCoordenadasAuditoriaHabilitacion(String.format("%s,%s", precision.getLatitud(), precision.getLongitud()));
            habilitacionInRO.setPrecisionCoordenadasAuditoriaHabilitacion(BigDecimal.valueOf(Double.valueOf(precision.getPrecision())));
            habilitacionInRO.setAprobadoCapacitacionInstalador("1".equalsIgnoreCase(suministro.getAprobadoCapacitacionInstalador()) ? Constantes.VALOR_SI : Constantes.VALOR_NO);

            List<Punto> puntos = puntoDao.queryBuilder()
                    .where(PuntoDao.Properties.IdHabilitacion.eq(suministro.getIdHabilitacion())).list();
            if (puntos.size() > 0) {
                ListPuntoInstalacionHabilitacionInRO listPuntoInstalacionHabilitacionInRO = new ListPuntoInstalacionHabilitacionInRO();
                List<PuntoInstalacionHabilitacionInRO> puntosInstalacion = new ArrayList<>();
                int i = 1;
                for (Punto punto : puntos) {
                    PuntoInstalacionHabilitacionInRO puntoTMP = new PuntoInstalacionHabilitacionInRO();
                    puntoTMP.setOrdenPuntoInstalacionHabilitacion(i);
                    puntoTMP.setIdGasodomestico("".equalsIgnoreCase(punto.getPuntoGasodomestico()) ? null : Integer.parseInt(punto.getPuntoGasodomestico()));
                    puntoTMP.setDiametroValvulaPuntoInstalacionHabilitacion("".equalsIgnoreCase(punto.getPuntoDiametroValvula()) ? null : BigDecimal.valueOf(Float.parseFloat(punto.getPuntoDiametroValvula())));
                    puntosInstalacion.add(puntoTMP);
                    i++;
                }
                listPuntoInstalacionHabilitacionInRO.setPuntoInstalacionHabilitacion(puntosInstalacion);
                habilitacionInRO.setPuntosInstalacion(listPuntoInstalacionHabilitacionInRO);
            }

            List<Ambiente> ambientes = ambienteDao.queryBuilder()
                    .where(AmbienteDao.Properties.IdHabilitacion.eq(suministro.getIdHabilitacion())).list();
            if (ambientes.size() > 0) {
                ListAmbientePredioHabilitacionInRO listAmbientePredioHabilitacionInRO = new ListAmbientePredioHabilitacionInRO();
                List<AmbientePredioHabilitacionInRO> ambientesPredio = new ArrayList<>();
                int j = 1;
                for (Ambiente ambiente : ambientes) {
                    AmbientePredioHabilitacionInRO ambienteTMP = new AmbientePredioHabilitacionInRO();
                    ambienteTMP.setOrdenAmbientePredioHabilitacion(j);
                    ambienteTMP.setIdAmbientePredio("".equalsIgnoreCase(ambiente.getAmbienteTipo()) ? null : Integer.parseInt(ambiente.getAmbienteTipo()));
                    ambienteTMP.setConfinadoAmbientePredioHabilitacion("1".equalsIgnoreCase(ambiente.getAmbienteConfinado()) ? Constantes.VALOR_SI : Constantes.VALOR_NO);
                    ambienteTMP.setValorPruebaMonoxidoAmbientePredioHabilitacion("".equalsIgnoreCase(ambiente.getMonoxidoMedicion()) ? null : BigDecimal.valueOf(Float.parseFloat(ambiente.getMonoxidoMedicion())));//Este dato se obtiene del ítem "Prueba de monóxido" en donde sólo se ven aquellos ambientes que son confinados
                    ambientesPredio.add(ambienteTMP);
                    j++;
                }
                listAmbientePredioHabilitacionInRO.setAmbientePredioHabilitacion(ambientesPredio);
                habilitacionInRO.setAmbientesPredio(listAmbientePredioHabilitacionInRO);
            }
            if ("0".equalsIgnoreCase(suministro.getSuministroAprueba())) {
                List<Rechazo> rechazos = rechazoDao.queryBuilder()
                        .where(RechazoDao.Properties.IdHabilitacion.eq(suministro.getIdHabilitacion())).list();
                ListMotivoRechazoHabilitacionHabilitacionInRO listMotivoRechazoHabilitacionHabilitacionInRO = new ListMotivoRechazoHabilitacionHabilitacionInRO();
                List<MotivoRechazoHabilitacionHabilitacionInRO> motivoRechazoHabilitacionHabilitacion = new ArrayList<>();
                int k = 1;
                for (Rechazo rechazo : rechazos) {
                    MotivoRechazoHabilitacionHabilitacionInRO motivoRechazoTMP = new MotivoRechazoHabilitacionHabilitacionInRO();
                    motivoRechazoTMP.setOrdenMotivoRechazoHabilitacionHabilitacion(k);
                    motivoRechazoTMP.setIdMotivoRechazoHabilitacion(rechazo.getIdMotivoSubRechazo());
                    motivoRechazoTMP.setObservacionMotivoRechazoHabilitacionHabilitacion(rechazo.getObservacion());
                    motivoRechazoHabilitacionHabilitacion.add(motivoRechazoTMP);
                    k++;
                }
                listMotivoRechazoHabilitacionHabilitacionInRO.setMotivoRechazoHabilitacionHabilitacion(motivoRechazoHabilitacionHabilitacion);
                habilitacionInRO.setMotivosRechazoHabilitacion(listMotivoRechazoHabilitacionHabilitacionInRO);
            }

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
            httpHeaders.add("Authorization", "Bearer " + config.getToken());

            MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
            parts.add(Constantes.PARAM_NAME_HABILITACION, habilitacionInRO);
            parts.add(Constantes.PARAM_NAME_FOTO_UBICACION_MEDIDOR, "".equalsIgnoreCase(suministro.getMedidorFotoUbicacion()) ? null : new FileSystemResource(suministro.getMedidorFotoUbicacion()));
            parts.add(Constantes.PARAM_NAME_FOTO_UBICACION_VALVULA_CORTE_GENERAL, "".equalsIgnoreCase(suministro.getValvulaFotoUbicacion()) ? null : new FileSystemResource(suministro.getValvulaFotoUbicacion()));
            parts.add(Constantes.PARAM_NAME_FOTO_1_RECORRIDO_TUBERIA, "".equalsIgnoreCase(suministro.getTuberiaFotoRecorrido1()) ? null : new FileSystemResource(suministro.getTuberiaFotoRecorrido1()));
            parts.add(Constantes.PARAM_NAME_FOTO_2_RECORRIDO_TUBERIA, "".equalsIgnoreCase(suministro.getTuberiaFotoRecorrido2()) ? null : new FileSystemResource(suministro.getTuberiaFotoRecorrido2()));
            parts.add(Constantes.PARAM_NAME_FOTO_3_RECORRIDO_TUBERIA, "".equalsIgnoreCase(suministro.getTuberiaFotoRecorrido3()) ? null : new FileSystemResource(suministro.getTuberiaFotoRecorrido3()));
            parts.add(Constantes.PARAM_NAME_FOTO_4_RECORRIDO_TUBERIA, "".equalsIgnoreCase(suministro.getTuberiaFotoRecorrido4()) ? null : new FileSystemResource(suministro.getTuberiaFotoRecorrido4()));
            parts.add(Constantes.PARAM_NAME_FOTO_ISOMETRICO, "".equalsIgnoreCase(suministro.getIsometricoFoto()) ? null : new FileSystemResource(suministro.getIsometricoFoto()));
            parts.add(Constantes.PARAM_NAME_FOTO_MEDICION_PRESION_MANOMETRO, "".equalsIgnoreCase(suministro.getArtefactoMedicionFoto()) ? null : new FileSystemResource(suministro.getArtefactoMedicionFoto()));
            parts.add(Constantes.PARAM_NAME_FOTO_MEDICION_HERMETICIDAD_MANOMETRO, "".equalsIgnoreCase(suministro.getHermeticidadNanometroFoto()) ? null : new FileSystemResource(suministro.getHermeticidadNanometroFoto()));
            if (puntos.size() > 0) {
                for (Punto punto : puntos) {
                    if (!"".equalsIgnoreCase(punto.getPuntoFoto())) {
                        final String orden = punto.getPosicion().toString();
                        ByteArrayResource bytes = new ByteArrayResource(FileUtils.readFileToByteArray(new File(punto.getPuntoFoto()))) {
                            @Override
                            public String getFilename() {
                                return orden + ".JPEG";
                            }
                        };
                        parts.add(Constantes.PARAM_NAME_FOTOS_GASODOMESTICOS, bytes);
                    }
                }
            }
            if (ambientes.size() > 0) {
                for (Ambiente ambiente : ambientes) {
                    final String orden = ambiente.getPosicion().toString();
                    ByteArrayResource bytesAsResource1PruebaMonoxido = new ByteArrayResource(FileUtils.readFileToByteArray(new File(ambiente.getMonoxidoFoto()))) {
                        @Override
                        public String getFilename() {
                            return orden + ".JPEG";
                        }
                    };
                    parts.add(Constantes.PARAM_NAME_FOTOS_PRUEBA_MONOXIDO_AMBIENTES, bytesAsResource1PruebaMonoxido);

                    ByteArrayResource byteFotoUno = new ByteArrayResource(FileUtils.readFileToByteArray(new File(ambiente.getAmbienteUnoFoto()))) {
                        @Override
                        public String getFilename() {
                            return orden + ".JPEG";
                        }
                    };
                    parts.add(Constantes.PARAM_NAME_FOTOS_1_AMBIENTES, byteFotoUno);

                    ByteArrayResource byteFotoDos = new ByteArrayResource(FileUtils.readFileToByteArray(new File(ambiente.getAmbienteDosFoto()))) {
                        @Override
                        public String getFilename() {
                            return orden + ".JPEG";
                        }
                    };
                    parts.add(Constantes.PARAM_NAME_FOTOS_2_AMBIENTES, byteFotoDos);
                }
            }
            if ("0".equalsIgnoreCase(suministro.getSuministroAprueba())) {
                parts.add(Constantes.PARAM_NAME_FOTO_ACTA_HABILITACION, "".equalsIgnoreCase(suministro.getSuministroHabilitacionFoto()) ? null : new FileSystemResource(suministro.getSuministroHabilitacionFoto()));
                parts.add(Constantes.PARAM_NAME_FOTO_PRUEBA_RECHAZO_HABILITACION, "".equalsIgnoreCase(suministro.getSuministroPruebaFoto()) ? null : new FileSystemResource(suministro.getSuministroPruebaFoto()));
            } else {
                parts.add(Constantes.PARAM_NAME_FOTO_ACTA_HABILITACION, "".equalsIgnoreCase(suministro.getSuministroHabilitacionFotoAprovado()) ? null : new FileSystemResource(suministro.getSuministroHabilitacionFotoAprovado()));
                /*PAPU 1911 - 0*/
                //parts.add(Constantes.PARAM_NAME_FOTO_ACTA_FINALIZACION, "".equalsIgnoreCase(suministro.getSuministroHabilitacionFotoActaFinalizacion()) ? null : new FileSystemResource(suministro.getSuministroHabilitacionFotoActaFinalizacion()));
                parts.add(Constantes.PARAM_NAME_FOTO_ACTA_FINALIZACION_TC_ACOMETIDA, "".equalsIgnoreCase(suministro.getSuministroHabilitacionFotoActaFinalizacion()) ? null : new FileSystemResource(suministro.getSuministroHabilitacionFotoActaFinalizacion()));
                Log.i("TAG ADENTROOOOOOOOOO", suministro.getSuministroHabilitacionFotoActaFinalizacion()+" "+suministro.getSuministroHabilitacionFotoAprovado());
                /*PAPU 1911 - 1*/
            }
            Log.i("TAG AFUERAAAAAAAAAAA", suministro.getSuministroHabilitacionFotoActaFinalizacion()+" "+suministro.getSuministroHabilitacionFotoAprovado());

            HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(parts, httpHeaders);
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            return restTemplate.postForObject(url, httpEntity, HabilitacionOutRO.class);

        } catch (RestClientException | NullPointerException | IllegalArgumentException | IOException e) {
            Log.e("GRABAR", e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(HabilitacionOutRO habilitacionOutRO) {
        super.onPostExecute(habilitacionOutRO);
        listener.onGrabarHabilitacionAsyncTaskCompleted(habilitacionOutRO);
    }

    public interface OnGrabarHabilitacionAsyncTaskCompleted {
        void onGrabarHabilitacionAsyncTaskCompleted(HabilitacionOutRO habilitacionOutRO);
    }
}
