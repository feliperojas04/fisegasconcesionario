package pe.gob.osinergmin.gnr.cgn.task;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import gob.osinergmin.gnr.domain.dto.rest.in.BaseInRO;
import gob.osinergmin.gnr.domain.dto.rest.out.ParametroOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.util.Urls;

public class Mapa extends AsyncTask<String, Void, ParametroOutRO> {

    private OnMapaCompleted listener = null;

    public Mapa(OnMapaCompleted listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected ParametroOutRO doInBackground(String... configs) {
        try {

//            Config config = configs[0];

            String url = Urls.getBase() + Urls.getMapa();

            BaseInRO baseInRO = new BaseInRO();
            baseInRO.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<BaseInRO> httpEntity = new HttpEntity<>(baseInRO, httpHeaders);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            return restTemplate.postForObject(url, httpEntity, ParametroOutRO.class);

        } catch (RestClientException e) {
            Log.e("MAPA", e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(ParametroOutRO parametroOutRO) {
        super.onPostExecute(parametroOutRO);
        listener.onMapaCompleted(parametroOutRO);
    }

    public interface OnMapaCompleted {
        void onMapaCompleted(ParametroOutRO parametroOutRO);
    }
}
