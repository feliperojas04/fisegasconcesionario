package pe.gob.osinergmin.gnr.cgn.actividad;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.util.Constantes;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.adaptador.HabilitacionAdaptador;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionMontanteOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionMontanteOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.Montante;
import pe.gob.osinergmin.gnr.cgn.data.models.MontanteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Precision;
import pe.gob.osinergmin.gnr.cgn.data.models.PrecisionDao;
import pe.gob.osinergmin.gnr.cgn.data.pojos.Habilitacion;
import pe.gob.osinergmin.gnr.cgn.task.MontanteRx;
import pe.gob.osinergmin.gnr.cgn.util.BorrarFoto;
import pe.gob.osinergmin.gnr.cgn.util.DownTimer;
import pe.gob.osinergmin.gnr.cgn.util.FormatoFecha;
import pe.gob.osinergmin.gnr.cgn.util.MostrarFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.NetworkUtil;
import pe.gob.osinergmin.gnr.cgn.util.ReducirFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.Util;

public class HabilitacionMontanteActivity extends BaseActivity {

    private Montante montante;
    private Config config;
    private TableLayout tablaHabilitacion;
    private TextView txtDet_DocumentoPropietarioView;
    private TextView txtDet_nombrePropietarioView;
    private TextView txtDetProyectoView;
    private TextView txtDetMontanteView;
    private TextView txtDet_numeroIntHabilitacionView;
    private TextView txtDet_numeroSolicitudView;
    private TextView txtDet_numeroContratoView;
    private TextView txtDet_fechaSolicitudView;
    private TextView txtDet_fechaAprobacionView;
    private TextView txtDet_tipoProyectoView;
    private TextView txtDet_tipoInstalacionView;
    private TextView txtDet_numeroPuntosInstaacionView;
    private TextView txtDet_memoriaView;
    private ListView listHabilitacion;
    private Button btnHab_aprueba;
    private List<Habilitacion> habilitaciones;
    private Intent intent;
    private Toolbar toolbar;
    private MenuItem menuMas;
    private MenuItem menuMenos;
    private MontanteDao montanteDao;
    private DownTimer countDownTimer;
    private MHabilitacionMontanteOutRODao mHabilitacionMontanteOutRODao;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private PrecisionDao precisionDao;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private ImageView imgRec_fotoActa;
    private ImageView icoRec_acta;
    private String tmpRuta;
    private static int RESULT_CODE_ACTA_HABILITACION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_habitacion_montante);

        countDownTimer = DownTimer.getInstance();

        montanteDao = ((App) getApplication()).getDaoSession().getMontanteDao();
        mHabilitacionMontanteOutRODao = ((App) getApplication()).getDaoSession().getMHabilitacionMontanteOutRODao();
        precisionDao = ((App) getApplication()).getDaoSession().getPrecisionDao();

        toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_36dp);

        montante = getIntent().getExtras().getParcelable("MONTANTE");
        config = getIntent().getExtras().getParcelable("CONFIG");

        tablaHabilitacion = findViewById(R.id.tablaHabilitacion);
        txtDet_DocumentoPropietarioView = findViewById(R.id.txtDet_DocumentoPropietarioView);
        txtDet_nombrePropietarioView = findViewById(R.id.txtDet_nombrePropietarioView);
        txtDetProyectoView = findViewById(R.id.txtDetProyectoView);
        txtDetMontanteView = findViewById(R.id.txtDetMontanteView);
        txtDet_numeroIntHabilitacionView = findViewById(R.id.txtDet_numeroIntHabilitacionView);
        txtDet_numeroSolicitudView = findViewById(R.id.txtDet_numeroSolicitudView);
        txtDet_numeroContratoView = findViewById(R.id.txtDet_numeroContratoView);
        txtDet_fechaSolicitudView = findViewById(R.id.txtDet_fechaSolicitudView);
        txtDet_fechaAprobacionView = findViewById(R.id.txtDet_fechaAprobacionView);
        txtDet_tipoProyectoView = findViewById(R.id.txtDet_tipoProyectoView);
        txtDet_tipoInstalacionView = findViewById(R.id.txtDet_tipoInstalacionView);
        txtDet_numeroPuntosInstaacionView = findViewById(R.id.txtDet_numeroPuntosInstaacionView);
        txtDet_memoriaView = findViewById(R.id.txtDet_memoriaView);
        listHabilitacion = findViewById(R.id.listHabilitacion);
        btnHab_aprueba = findViewById(R.id.btnHab_aprueba);
        Button btnHab_rechazar = findViewById(R.id.btnHab_rechazar);
        imgRec_fotoActa = findViewById(R.id.imgRec_fotoActa);
        icoRec_acta = findViewById(R.id.icoRec_acta);

        listHabilitacion.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    intent = new Intent(HabilitacionMontanteActivity.this, AcometidaActivity.class);
                } else if (position == 1) {
                    intent = new Intent(HabilitacionMontanteActivity.this, TuberiaMontanteActivity.class);
                } else if (position == 2) {
                    intent = new Intent(HabilitacionMontanteActivity.this, HermeticidadMontanteActivity.class);
                } else if (position == 3) {
                    intent = new Intent(HabilitacionMontanteActivity.this, InstalacionActivity.class);
                }
                intent.putExtra("MONTANTE", montante);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
            }
        });

        btnHab_aprueba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(HabilitacionMontanteActivity.this);
                builder.setTitle(R.string.txtHab_mensajeConfirmacion);
                builder.setMessage("¿Está seguro de aprobar la Habilitación de Montante?");
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.btnHab_si, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        montante.setApruebaHabilitacionMontante("1");
                        if (config.getOffline()) {
                            MHabilitacionMontanteOutRO mHabilitacionMontanteOutRO = mHabilitacionMontanteOutRODao.queryBuilder().where(MHabilitacionMontanteOutRODao.Properties.IdHabilitacionMontante.eq(montante.getIdHabilitacionMontante())).limit(1).unique();
                            mHabilitacionMontanteOutRO.setResultCode("100");
                            montante.setFechaAprobacion(FormatoFecha.getfecha());
                            montante.setFechaRegistroOffline(FormatoFecha.getfecha());
                            montante.setGrabar(true);
                            mHabilitacionMontanteOutRODao.insertOrReplace(mHabilitacionMontanteOutRO);
                            montanteDao.insertOrReplace(montante);
                            AlertDialog.Builder builder = new AlertDialog.Builder(HabilitacionMontanteActivity.this);
                            builder.setTitle(R.string.txtHab_mensajeConfirmacion);
                            builder.setMessage("La Habilitación de Montante fue aprobada satisfactoriamente");
                            builder.setCancelable(false);
                            builder.setPositiveButton(R.string.btnHab_aceptar, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(HabilitacionMontanteActivity.this, BuscarMontanteActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                            builder.show();
                        } else {
                            if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                showProgressDialog(R.string.app_procesando);
                                grabarMontante(precisionDao, montante, config.getToken());
                            } else {
                                ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                            }
                        }
                    }
                });
                builder.setNegativeButton(R.string.btnHab_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });

        btnHab_rechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HabilitacionMontanteActivity.this, RechazoMontanteActivity.class);
                intent.putExtra("MONTANTE", montante);
                intent.putExtra("CONFIG", config);
                intent.putExtra("TIPO", "general");
                startActivity(intent);
            }
        });
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    Precision precision = new Precision();
                    precision.setIdSolicitud(montante.getIdHabilitacionMontante());
                    precision.setLatitud(String.valueOf(location.getLatitude()));
                    precision.setLongitud(String.valueOf(location.getLongitude()));
                    precision.setPrecision(String.valueOf(location.getAccuracy()));
                    precision.setEstado("2");
                    precisionDao.insertOrReplace(precision);
                }
            }
        };

        imgRec_fotoActa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(
                        HabilitacionMontanteActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ((App) getApplication()).showToast("No puede realizar la acción porque no se ha dado el permiso de Almacenamiento");
                    return;
                }
                if (Util.isExternalStorageWritable()) {
                    if (Util.getAlbumStorageDir(HabilitacionMontanteActivity.this)) {
                        File file = Util.getNuevaRutaFoto(montante.getIdHabilitacionMontante().toString(),HabilitacionMontanteActivity.this);
                        tmpRuta = file.getPath();
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        Uri outputUri = FileProvider.getUriForFile(HabilitacionMontanteActivity.this, getApplicationContext().getPackageName() + ".provider", file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            ClipData clip = ClipData.newUri(getContentResolver(), "Instalacion", outputUri);
                            intent.setClipData(clip);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        }
                        startActivityForResult(intent, 1);
                    } else {
                        ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                    }
                } else {
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                }
            }
        });
    }

    private void grabarMontante(PrecisionDao precisionDao, Montante montante, String token) {
        disposables.add(MontanteRx.getMontanteRegistrar(precisionDao, montante, token, null)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(habilitacionMontanteOutRO -> {
                    if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(habilitacionMontanteOutRO.getResultCode())) {
                        hideProgressDialog();
                        ((App) getApplication()).showToast(habilitacionMontanteOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : habilitacionMontanteOutRO.getMessage());
                    } else {
                        new BorrarFoto().execute(montante.getIdHabilitacionMontante().toString());
                        AlertDialog.Builder builder = new AlertDialog.Builder(HabilitacionMontanteActivity.this);
                        builder.setTitle(R.string.txtHab_mensajeConfirmacion);
                        if (montante.getApruebaHabilitacionMontante().equalsIgnoreCase("1")) {
                            builder.setMessage("La Habilitación de Montante fue aprobada satisfactoriamente");
                        } else {
                            builder.setMessage("La Habilitación de Montante fue rechazada satisfactoriamente");
                        }
                        builder.setCancelable(false);
                        builder.setPositiveButton(R.string.btnHab_aceptar, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                List<Precision> precisiones = precisionDao.queryBuilder()
                                        .where(PrecisionDao.Properties.Estado.eq("2"),
                                                PrecisionDao.Properties.IdSolicitud.eq(montante.getIdHabilitacionMontante())).list();
                                precisionDao.deleteInTx(precisiones);
                                montanteDao.delete(montante);
                                Intent intent = new Intent(HabilitacionMontanteActivity.this, BuscarMontanteActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        });
                        builder.show();
                    }
                }, throwable -> {
                    hideProgressDialog();
                    if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                    } else {
                        ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                    }
                }));
    }

    private void cargaHabilitaciones() {
        habilitaciones = new ArrayList();
        if ("0".equalsIgnoreCase(montante.getApruebaAcometida())) {
            habilitaciones.add(new Habilitacion(R.mipmap.ic_warning_black_36dp, config.getParametro6()));
        } else {
            habilitaciones.add(new Habilitacion(R.mipmap.ic_done_black_36dp, config.getParametro6()));
        }
        if ("0".equalsIgnoreCase(montante.getApruebaTuberia())) {
            habilitaciones.add(new Habilitacion(R.mipmap.ic_warning_black_36dp, "Recorrido de tubería"));
        } else {
            habilitaciones.add(new Habilitacion(R.mipmap.ic_done_black_36dp, "Recorrido de tubería"));
        }
        if ("0".equalsIgnoreCase(montante.getApruebaHermeticidad())) {
            habilitaciones.add(new Habilitacion(R.mipmap.ic_warning_black_36dp, "Prueba de hermeticidad"));
        } else {
            habilitaciones.add(new Habilitacion(R.mipmap.ic_done_black_36dp, "Prueba de hermeticidad"));
        }
        if ("0".equalsIgnoreCase(montante.getApruebaInstalacion())) {
            habilitaciones.add(new Habilitacion(R.mipmap.ic_warning_black_36dp, "Datos de la tubería instalada"));
        } else {
            habilitaciones.add(new Habilitacion(R.mipmap.ic_done_black_36dp, "Datos de la tubería instalada"));
        }

        if (!("".equalsIgnoreCase(montante.getMontanteHabilitacionFotoActa()))) {
            loadBitmap(montante.getMontanteHabilitacionFotoActa(), imgRec_fotoActa);
            icoRec_acta.setImageResource(R.mipmap.ic_done_black_36dp);
        }
    }

    private void validar() {
        boolean habilitar = true;
        if ("0".equalsIgnoreCase(montante.getApruebaAcometida()) ||
                "0".equalsIgnoreCase(montante.getApruebaTuberia()) ||
                "0".equalsIgnoreCase(montante.getApruebaHermeticidad()) ||
                "0".equalsIgnoreCase(montante.getApruebaInstalacion()) ||
                "".equalsIgnoreCase(montante.getMontanteHabilitacionFotoActa())) {
            habilitar = false;
        }
        btnHab_aprueba.setEnabled(habilitar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.createAndStart();
        createLocationRequest();
        cargarSuministro();
        cargaHabilitaciones();
        montante=montanteDao.queryBuilder()
                .where(MontanteDao.Properties.IdHabilitacionMontante.eq(montante.getIdHabilitacionMontante())).limit(1).unique();
        listHabilitacion.setAdapter(new HabilitacionAdaptador(this, habilitaciones));
        Util.setListViewHeightBasedOnChildren(listHabilitacion);
        toolbar.setTitle(montante.getObjetoConexion());
        validar();
    }

    private void cargarSuministro() {
        txtDet_DocumentoPropietarioView.setText(montante.getObjetoConexion());
        txtDet_nombrePropietarioView.setText(montante.getCup());
        txtDetProyectoView.setText(montante.getNombre());
        txtDetMontanteView.setText(montante.getNombreMontanteProyectoInstalacion());
        txtDet_numeroIntHabilitacionView.setText(montante.getDireccion());
        txtDet_numeroSolicitudView.setText(montante.getTipoProyecto());
        txtDet_numeroContratoView.setText(montante.getFechaRegistro());
        txtDet_fechaSolicitudView.setText(montante.getEsProyectoFise());
        txtDet_fechaAprobacionView.setText(montante.getTienePromocion());
        txtDet_tipoProyectoView.setText(montante.getEsZonaGasificada());
        txtDet_tipoInstalacionView.setText(montante.getTienePlanoInstalacion());
        txtDet_numeroPuntosInstaacionView.setText(montante.getTieneEspecificacion());
        txtDet_memoriaView.setText(montante.getTieneMemoria());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_general, menu);
        menuMas = menu.findItem(R.id.menuMas);
        menuMas.setVisible(true);
        menuMenos = menu.findItem(R.id.menuMenos);
        menuMenos.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menuMas:
                tablaHabilitacion.setVisibility(View.VISIBLE);
                menuMas.setVisible(false);
                menuMenos.setVisible(true);
                return true;
            case R.id.menuMenos:
                tablaHabilitacion.setVisibility(View.GONE);
                menuMas.setVisible(true);
                menuMenos.setVisible(false);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(HabilitacionMontanteActivity.this, BuscarMontanteActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        countDownTimer.createAndStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1458) {
            if (resultCode == RESULT_CANCELED) {
                Intent intent = new Intent(HabilitacionMontanteActivity.this, BuscarMontanteActivity.class);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
            }
        }

        if (requestCode == RESULT_CODE_ACTA_HABILITACION) {
            if (resultCode != RESULT_CANCELED) {
                montante.setMontanteHabilitacionFotoActa(tmpRuta);
                ReducirFotoTask reducirFotoTask = new ReducirFotoTask(new ReducirFotoTask.OnReducirFotoTaskCompleted() {
                    @Override
                    public void onReducirFotoTaskCompleted(String result) {
                        if (!("1".equalsIgnoreCase(result))) {
                            ((App) getApplication()).showToast(result);
                        }
                    }
                });
                montanteDao.insertOrReplace(montante);
                reducirFotoTask.execute(montante.getMontanteHabilitacionFotoActa(), config.getParametro5());
            }
        }
    }

    private void createLocationRequest() {
        int value = 10000;
        try {
            value = Integer.valueOf(config.getPrecision());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(value);
        mLocationRequest.setFastestInterval(value);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(HabilitacionMontanteActivity.this, 1458);
                    } catch (IntentSender.SendIntentException sendEx) {
                    }
                }
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION:
                String message = Util.onRequestPermissionsResult(permissions, grantResults);
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    @Override
    protected void onDestroy() {
        disposables.clear();
        super.onDestroy();
    }

    public void loadBitmap(String ruta, final ImageView imageView) {
        MostrarFotoTask mostrarFotoTask = new MostrarFotoTask(new MostrarFotoTask.OnMostrarFotoTaskCompleted() {
            @Override
            public void onMostrarFotoTaskCompleted(Bitmap bitmap) {
                if (bitmap != null) {
                    if (imageView != null) {
                        imageView.setImageBitmap(bitmap);
                    }
                } else {
                    ((App) getApplication()).showToast("Error al cargar la foto");
                }
            }
        });
        mostrarFotoTask.execute(ruta);
    }
}
