package pe.gob.osinergmin.gnr.cgn.actividad;

import android.Manifest;
import android.content.ClipData;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.GenericBeanOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListGenericBeanOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.adaptador.FotoAdaptador;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MaterialInstalacionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.PrecisionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.SuministroDao;
import pe.gob.osinergmin.gnr.cgn.data.models.TiposInstalacionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MaterialInstalacion;
import pe.gob.osinergmin.gnr.cgn.data.models.Precision;
import pe.gob.osinergmin.gnr.cgn.data.models.Suministro;
import pe.gob.osinergmin.gnr.cgn.data.models.TiposInstalacion;
import pe.gob.osinergmin.gnr.cgn.data.models.TiposMarcaAccesorio;
import pe.gob.osinergmin.gnr.cgn.data.models.TiposMarcaAccesorioDao;
import pe.gob.osinergmin.gnr.cgn.data.models.TiposMarcaTuberia;
import pe.gob.osinergmin.gnr.cgn.data.models.TiposMarcaTuberiaDao;
import pe.gob.osinergmin.gnr.cgn.task.TipoInstalacion;
import pe.gob.osinergmin.gnr.cgn.task.TipoMarcaAccesorio;
import pe.gob.osinergmin.gnr.cgn.task.TipoMarcaTuberia;
import pe.gob.osinergmin.gnr.cgn.task.TipoMaterialInstalacion;
import pe.gob.osinergmin.gnr.cgn.util.DownTimer;
import pe.gob.osinergmin.gnr.cgn.util.MostrarFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.NetworkUtil;
import pe.gob.osinergmin.gnr.cgn.util.Parse;
import pe.gob.osinergmin.gnr.cgn.util.ReducirFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.Util;

public class TuberiaActivity extends BaseActivity {

    private Suministro suministro;
    private Config config;
    private TableLayout tablaTuberia;
    private TextView txtDet_DocumentoPropietarioView;
    private TextView txtDet_nombrePropietarioView;
    private TextView txtDet_PredioView;
    private TextView txtDet_numeroIntHabilitacionView;
    private TextView txtDet_numeroSolicitudView;
    private TextView txtDet_numeroContratoView;
    private TextView txtDet_fechaSolicitudView;
    private TextView txtDet_fechaAprobacionView;
    private TextView txtDet_tipoProyectoView;
    private TextView txtDet_tipoInstalacionView;
    private TextView txtDet_numeroPuntosInstaacionView;
    private ImageView icoTub_recorrido;
    private ListView listaTub_recorrido;
    private List<String> listaTub_fotosRecorrido;
    private ImageView icoTub_tipoInstalacion;
    private Spinner spiTub_tipoInstalacion;
    private ImageView icoTub_materialInstalacion;
    private Spinner spiTub_materialInstalacion;
    private ImageView icoTub_marcaAccesorio;
    private Spinner spiTub_marcaAccesorio;
    private ImageView icoTub_marcaTuberia;
    private Spinner spiTub_marcaTuberia;
    private ImageView icoTub_fotoIsometrico;
    private ImageView imgTub_fotoIsometrico;
    private TextInputLayout ediTub_longitudTotal;
    private ImageView icoTub_longitudTotal;
    private SwitchCompat swiTub_aprueba;
    private Button btnTub_rechazar;
    private Toolbar toolbar;
    private MenuItem menuMas;
    private MenuItem menuMenos;
    private FotoAdaptador fotoAdaptador;
    private SuministroDao suministroDao;
    private List<GenericBeanOutRO> listaTipoInslacion = new ArrayList<>();
    private List<GenericBeanOutRO> listaTipoMaterial = new ArrayList<>();
    private List<GenericBeanOutRO> listaTipoMarcaAccesorio = new ArrayList<>();
    private List<GenericBeanOutRO> listaTipoMarcaTuberia = new ArrayList<>();
    private int check1 = 0, check2 = 0, check3 = 0, check4 = 0;
    private DownTimer countDownTimer;
    private TiposInstalacionDao tiposInstalacionDao;
    private MParametroOutRODao mParametroOutRODao;
    private MaterialInstalacionDao materialInstalacionDao;
    private TiposMarcaAccesorioDao tiposMarcaAccesorioDao;
    private TiposMarcaTuberiaDao tiposMarcaTuberiaDao;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private PrecisionDao precisionDao;
    private String tmpRuta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tuberia);

        countDownTimer = DownTimer.getInstance();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        suministroDao = ((App) getApplication()).getDaoSession().getSuministroDao();
        tiposInstalacionDao = ((App) getApplication()).getDaoSession().getTiposInstalacionDao();
        mParametroOutRODao = ((App) getApplication()).getDaoSession().getMParametroOutRODao();
        materialInstalacionDao = ((App) getApplication()).getDaoSession().getMaterialInstalacionDao();
        tiposMarcaAccesorioDao = ((App) getApplication()).getDaoSession().getTiposMarcaAccesorioDao();
        tiposMarcaTuberiaDao = ((App) getApplication()).getDaoSession().getTiposMarcaTuberiaDao();
        precisionDao = ((App) getApplication()).getDaoSession().getPrecisionDao();

        toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_36dp);

        suministro = getIntent().getExtras().getParcelable("SUMINISTRO");
        config = getIntent().getExtras().getParcelable("CONFIG");

        tablaTuberia = findViewById(R.id.tablaTuberia);
        txtDet_DocumentoPropietarioView = findViewById(R.id.txtDet_DocumentoPropietarioView);
        txtDet_nombrePropietarioView = findViewById(R.id.txtDet_nombrePropietarioView);
        txtDet_PredioView = findViewById(R.id.txtDet_PredioView);
        txtDet_numeroIntHabilitacionView = findViewById(R.id.txtDet_numeroIntHabilitacionView);
        txtDet_numeroSolicitudView = findViewById(R.id.txtDet_numeroSolicitudView);
        txtDet_numeroContratoView = findViewById(R.id.txtDet_numeroContratoView);
        txtDet_fechaSolicitudView = findViewById(R.id.txtDet_fechaSolicitudView);
        txtDet_fechaAprobacionView = findViewById(R.id.txtDet_fechaAprobacionView);
        txtDet_tipoProyectoView = findViewById(R.id.txtDet_tipoProyectoView);
        txtDet_tipoInstalacionView = findViewById(R.id.txtDet_tipoInstalacionView);
        txtDet_numeroPuntosInstaacionView = findViewById(R.id.txtDet_numeroPuntosInstaacionView);
        icoTub_recorrido = findViewById(R.id.icoTub_recorrido);
        ImageView imgTub_recorrido = findViewById(R.id.imgTub_recorrido);
        listaTub_recorrido = findViewById(R.id.listaTub_recorrido);
        icoTub_tipoInstalacion = findViewById(R.id.icoTub_tipoInstalacion);
        spiTub_tipoInstalacion = findViewById(R.id.spiTub_tipoInstalacion);
        icoTub_materialInstalacion = findViewById(R.id.icoTub_materialInstalacion);
        spiTub_materialInstalacion = findViewById(R.id.spiTub_materialInstalacion);
        icoTub_marcaAccesorio = findViewById(R.id.icoTub_marcaAccesorio);
        spiTub_marcaAccesorio = findViewById(R.id.spiTub_marcaAccesorio);
        icoTub_marcaTuberia = findViewById(R.id.icoTub_marcaTuberia);
        spiTub_marcaTuberia = findViewById(R.id.spiTub_marcaTuberia);
        icoTub_fotoIsometrico = findViewById(R.id.icoTub_fotoIsometrico);
        imgTub_fotoIsometrico = findViewById(R.id.imgTub_fotoIsometrico);
        icoTub_longitudTotal = findViewById(R.id.icoTub_longitudTotal);
        ediTub_longitudTotal = findViewById(R.id.ediTub_longitudTotal);
        swiTub_aprueba = findViewById(R.id.swiTub_aprueba);
        Button btnTub_evaluando = findViewById(R.id.btnTub_evaluando);
        btnTub_rechazar = findViewById(R.id.btnTub_rechazar);

        imgTub_recorrido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listaTub_fotosRecorrido.size() < 4) {
                    if (ActivityCompat.checkSelfPermission(
                            TuberiaActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ((App) getApplication()).showToast("No puede realizar la acción porque no se ha dado el permiso de Almacenamiento");
                        return;
                    }
                    if (Util.isExternalStorageWritable()) {
                        if (Util.getAlbumStorageDir(TuberiaActivity.this)) {
                            File file = Util.getNuevaRutaFoto(suministro.getIdHabilitacion().toString(), TuberiaActivity.this);
                            listaTub_fotosRecorrido.add(file.getPath());
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            Uri outputUri = FileProvider.getUriForFile(TuberiaActivity.this, getApplicationContext().getPackageName() + ".provider", file);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                ClipData clip = ClipData.newUri(getContentResolver(), "Concesionario", outputUri);
                                intent.setClipData(clip);
                                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            }
                            startActivityForResult(intent, 2);
                        } else {
                            ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                        }
                    } else {
                        ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                    }
                } else {
                    ((App) getApplication()).showToast("Solo se admite 4 fotos");
                }
            }
        });

        spiTub_tipoInstalacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                check1++;
                if (check1 > 1) {
                    if (position != 0) {
                        suministro.setTuberiaTipoInstalacion(setTipoInslacion(parent.getSelectedItem().toString()));
                    } else {
                        suministro.setTuberiaTipoInstalacion("");
                    }
                    validarCheck();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spiTub_materialInstalacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                check2++;
                if (check2 > 1) {
                    if (position != 0) {
                        suministro.setTuberiaMaterialInstalacion(setTipoMaterial(parent.getSelectedItem().toString()));
                    } else {
                        suministro.setTuberiaMaterialInstalacion("");
                    }
                    validarCheck();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spiTub_marcaAccesorio.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                check3++;
                if (check3 > 1) {
                    if (position != 0) {
                        suministro.setIdMarcaAccesorio(setTipoMarcaAccesorio(parent.getSelectedItem().toString()));
                    } else {
                        suministro.setIdMarcaAccesorio("");
                    }
                    validarCheck();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spiTub_marcaTuberia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                check4++;
                if (check4 > 1) {
                    if (position != 0) {
                        suministro.setIdMarcaTuberia(setTipoMarcaTuberia(parent.getSelectedItem().toString()));
                    } else {
                        suministro.setIdMarcaTuberia("");
                    }
                    validarCheck();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ediTub_longitudTotal.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    if (Util.validarScaleAndPrecision(s.toString(), 2, 5)) {
                        suministro.setTuberiaLongitudTotal(s.toString());
                    } else {
                        ((App) getApplication()).showToast(String.format(getResources().getString(R.string.app_error_numero), 5, 2));
                        ediTub_longitudTotal.getEditText().setText(suministro.getTuberiaLongitudTotal());
                        ediTub_longitudTotal.getEditText().setSelection(ediTub_longitudTotal.getEditText().getText().length());
                    }
                } else {
                    suministro.setTuberiaLongitudTotal("");
                }
                validarCheck();
            }
        });

        imgTub_fotoIsometrico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(
                        TuberiaActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ((App) getApplication()).showToast("No puede realizar la acción porque no se ha dado el permiso de Almacenamiento");
                    return;
                }
                if (Util.isExternalStorageWritable()) {
                    if (Util.getAlbumStorageDir(TuberiaActivity.this)) {
                        File file = Util.getNuevaRutaFoto(suministro.getIdHabilitacion().toString(), TuberiaActivity.this);
                        tmpRuta = file.getPath();
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        Uri outputUri = FileProvider.getUriForFile(TuberiaActivity.this, getApplicationContext().getPackageName() + ".provider", file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            ClipData clip = ClipData.newUri(getContentResolver(), "Concesionario", outputUri);
                            intent.setClipData(clip);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        }
                        startActivityForResult(intent, 3);
                    } else {
                        ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                    }
                } else {
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                }
            }
        });

        swiTub_aprueba.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    suministro.setTuberiaAprueba("1");
                    btnTub_rechazar.setVisibility(View.GONE);
                } else {
                    suministro.setTuberiaAprueba("0");
                    btnTub_rechazar.setVisibility(View.VISIBLE);
                }
            }
        });

        assert btnTub_evaluando != null;
        btnTub_evaluando.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suministroDao.insertOrReplace(suministro);
                Intent intent = new Intent(TuberiaActivity.this, PuntoActivity.class);
                intent.putExtra("SUMINISTRO", suministro);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
            }
        });

        btnTub_rechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TuberiaActivity.this, RechazoActivity.class);
                intent.putExtra("SUMINISTRO", suministro);
                intent.putExtra("CONFIG", config);
                intent.putExtra("TIPO", "");
                startActivity(intent);
            }
        });
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    Precision precision = new Precision();
                    precision.setIdSolicitud(suministro.getIdHabilitacion());
                    precision.setLatitud(String.valueOf(location.getLatitude()));
                    precision.setLongitud(String.valueOf(location.getLongitude()));
                    precision.setPrecision(String.valueOf(location.getAccuracy()));
                    precision.setEstado("1");
                    precisionDao.insertOrReplace(precision);
                }
            }
        };
        aceptarPermiso();
    }

    private void validarCheck() {
        if (listaTub_fotosRecorrido != null && listaTub_fotosRecorrido.size() < 4) {
            icoTub_recorrido.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            icoTub_recorrido.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if ("".equalsIgnoreCase(suministro.getTuberiaTipoInstalacion())) {
            icoTub_tipoInstalacion.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            icoTub_tipoInstalacion.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if ("".equalsIgnoreCase(suministro.getTuberiaMaterialInstalacion())) {
            icoTub_materialInstalacion.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            icoTub_materialInstalacion.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if ("".equalsIgnoreCase(suministro.getIdMarcaAccesorio())) {
            icoTub_marcaAccesorio.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            icoTub_marcaAccesorio.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if ("".equalsIgnoreCase(suministro.getIdMarcaTuberia())) {
            icoTub_marcaTuberia.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            icoTub_marcaTuberia.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if ("".equalsIgnoreCase(suministro.getTuberiaLongitudTotal())) {
            icoTub_longitudTotal.setImageResource(R.mipmap.ic_warning_black_36dp);
        } else {
            icoTub_longitudTotal.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        validarSelector();
    }

    private void validarSelector() {
        if (listaTub_fotosRecorrido != null) {
            if (listaTub_fotosRecorrido.size() < 4 ||
                    "".equalsIgnoreCase(suministro.getTuberiaTipoInstalacion()) ||
                    "".equalsIgnoreCase(suministro.getTuberiaMaterialInstalacion()) ||
                    "".equalsIgnoreCase(suministro.getIdMarcaAccesorio()) ||
                    "".equalsIgnoreCase(suministro.getIdMarcaTuberia()) ||
                    "".equalsIgnoreCase(suministro.getIsometricoFoto()) ||
                    "".equalsIgnoreCase(suministro.getTuberiaLongitudTotal())) {
                swiTub_aprueba.setClickable(false);
                swiTub_aprueba.setChecked(false);
            } else {
                swiTub_aprueba.setClickable(true);
            }
        } else {
            swiTub_aprueba.setClickable(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1458) {
            if (resultCode == RESULT_CANCELED) {
                Intent intent = new Intent(TuberiaActivity.this, BuscarSuministroActivity.class);
                startActivity(intent);
                finish();
            }
        }
        if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                if (listaTub_fotosRecorrido.size() > 0) {
                    ReducirFotoTask reducirFotoTask = new ReducirFotoTask(new ReducirFotoTask.OnReducirFotoTaskCompleted() {
                        @Override
                        public void onReducirFotoTaskCompleted(String result) {
                            if (!("1".equalsIgnoreCase(result))) {
                                ((App) getApplication()).showToast(result);
                            }
                        }
                    });
                    reducirFotoTask.execute(listaTub_fotosRecorrido.get(listaTub_fotosRecorrido.size() - 1), config.getParametro5());
                    switch (listaTub_fotosRecorrido.size()) {
                        case 1:
                            suministro.setTuberiaFotoRecorrido1(listaTub_fotosRecorrido.get(0));
                            suministro.setTuberiaFotoRecorrido2("");
                            suministro.setTuberiaFotoRecorrido3("");
                            suministro.setTuberiaFotoRecorrido4("");
                            break;
                        case 2:
                            suministro.setTuberiaFotoRecorrido1(listaTub_fotosRecorrido.get(0));
                            suministro.setTuberiaFotoRecorrido2(listaTub_fotosRecorrido.get(1));
                            suministro.setTuberiaFotoRecorrido3("");
                            suministro.setTuberiaFotoRecorrido4("");
                            break;
                        case 3:
                            suministro.setTuberiaFotoRecorrido1(listaTub_fotosRecorrido.get(0));
                            suministro.setTuberiaFotoRecorrido2(listaTub_fotosRecorrido.get(1));
                            suministro.setTuberiaFotoRecorrido3(listaTub_fotosRecorrido.get(2));
                            suministro.setTuberiaFotoRecorrido4("");
                            break;
                        case 4:
                            suministro.setTuberiaFotoRecorrido1(listaTub_fotosRecorrido.get(0));
                            suministro.setTuberiaFotoRecorrido2(listaTub_fotosRecorrido.get(1));
                            suministro.setTuberiaFotoRecorrido3(listaTub_fotosRecorrido.get(2));
                            suministro.setTuberiaFotoRecorrido4(listaTub_fotosRecorrido.get(3));
                            break;
                    }
                    fotoAdaptador.notifyDataSetChanged();
                    validarCheck();
                }
            } else {
                if (listaTub_fotosRecorrido.size() > 0) {
                    listaTub_fotosRecorrido.remove(listaTub_fotosRecorrido.size() - 1);
                }
            }
        }
        if (requestCode == 3) {
            if (resultCode == RESULT_OK) {
                suministro.setIsometricoFoto(tmpRuta);
                ReducirFotoTask reducirFotoTask = new ReducirFotoTask(new ReducirFotoTask.OnReducirFotoTaskCompleted() {
                    @Override
                    public void onReducirFotoTaskCompleted(String result) {
                        if (!("1".equalsIgnoreCase(result))) {
                            ((App) getApplication()).showToast(result);
                        }
                    }
                });
                suministroDao.insertOrReplace(suministro);
                reducirFotoTask.execute(suministro.getIsometricoFoto(), config.getParametro5());
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.createAndStart();
        createLocationRequest();
        toolbar.setTitle(suministro.getSuministro() + " - Recorrido y materiales");
        cargarSuministro();
        cargaDatos();
        validarCheck();
        if (config.getOffline()) {
            listaTipoInslacion.clear();
            List<TiposInstalacion> tiposInstalacions = tiposInstalacionDao.queryBuilder().list();
            for (TiposInstalacion tiposInstalacion : tiposInstalacions) {
                listaTipoInslacion.add(Parse.getGenericBeanOutRO(tiposInstalacion));
            }
            ArrayList<String> list = new ArrayList<>();
            list.add("Seleccione tipo de instalación");
            for (GenericBeanOutRO genericBeanOutRO : listaTipoInslacion) {
                list.add(genericBeanOutRO.getLabel());
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<>(TuberiaActivity.this, android.R.layout.simple_spinner_item, list);
            adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            spiTub_tipoInstalacion.setAdapter(adapter);
            if (!("".equalsIgnoreCase(suministro.getTuberiaTipoInstalacion()))) {
                spiTub_tipoInstalacion.setSelection(getTipoInslacion(suministro.getTuberiaTipoInstalacion()));
            } else {
                spiTub_tipoInstalacion.setSelection(0);
            }
            listaTipoMaterial.clear();
            List<MaterialInstalacion> materialInstalacions = materialInstalacionDao.queryBuilder().list();
            for (MaterialInstalacion materialInstalacion : materialInstalacions) {
                listaTipoMaterial.add(Parse.getGenericBeanOutRO(materialInstalacion));
            }
            ArrayList<String> list2 = new ArrayList<>();
            list2.add("Seleccione material de instalación");
            for (GenericBeanOutRO genericBeanOutRO : listaTipoMaterial) {
                list2.add(genericBeanOutRO.getLabel());
            }
            ArrayAdapter<String> adapter2 = new ArrayAdapter<>(TuberiaActivity.this, android.R.layout.simple_spinner_item, list2);
            adapter2.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            spiTub_materialInstalacion.setAdapter(adapter2);
            if (!(suministro.getTuberiaMaterialInstalacion().equalsIgnoreCase(""))) {
                spiTub_materialInstalacion.setSelection(getTipoMaterial(suministro.getTuberiaMaterialInstalacion()));
            } else {
                spiTub_materialInstalacion.setSelection(0);
            }

            listaTipoMarcaAccesorio.clear();
            List<TiposMarcaAccesorio> tiposMarcaAccesorios = tiposMarcaAccesorioDao.queryBuilder().list();
            for (TiposMarcaAccesorio tiposMarcaAccesorio : tiposMarcaAccesorios) {
                listaTipoMarcaAccesorio.add(Parse.getGenericBeanOutRO(tiposMarcaAccesorio));
            }
            ArrayList<String> list3 = new ArrayList<>();
            list3.add("Seleccione marca del accesorio");
            for (GenericBeanOutRO genericBeanOutRO : listaTipoMarcaAccesorio) {
                list3.add(genericBeanOutRO.getLabel());
            }
            ArrayAdapter<String> adapter3 = new ArrayAdapter<>(TuberiaActivity.this, android.R.layout.simple_spinner_item, list3);
            adapter3.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            spiTub_marcaAccesorio.setAdapter(adapter3);
            if (!(suministro.getIdMarcaAccesorio().equalsIgnoreCase(""))) {
                spiTub_marcaAccesorio.setSelection(getTipoMarcaAccesorio(suministro.getIdMarcaAccesorio()));
            } else {
                spiTub_marcaAccesorio.setSelection(0);
            }

            listaTipoMarcaTuberia.clear();
            List<TiposMarcaTuberia> tiposMarcaTuberias = tiposMarcaTuberiaDao.queryBuilder().list();
            for (TiposMarcaTuberia tiposMarcaTuberia : tiposMarcaTuberias) {
                listaTipoMarcaTuberia.add(Parse.getGenericBeanOutRO(tiposMarcaTuberia));
            }
            ArrayList<String> list4 = new ArrayList<>();
            list4.add("Seleccione marca de la tubería");
            for (GenericBeanOutRO genericBeanOutRO : listaTipoMarcaTuberia) {
                list4.add(genericBeanOutRO.getLabel());
            }
            ArrayAdapter<String> adapter4 = new ArrayAdapter<>(TuberiaActivity.this, android.R.layout.simple_spinner_item, list4);
            adapter4.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            spiTub_marcaTuberia.setAdapter(adapter4);
            if (!(suministro.getIdMarcaTuberia().equalsIgnoreCase(""))) {
                spiTub_marcaTuberia.setSelection(getTipoMarcaTuberia(suministro.getIdMarcaTuberia()));
            } else {
                spiTub_marcaTuberia.setSelection(0);
            }

            MParametroOutRO mParametroOutRO = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_CONFIGURACION_COMPRESION_FOTOS_APLICATIVO_MOVIL))
                    .limit(1)
                    .unique();
            config.setParametro5(mParametroOutRO == null ? "800x600" : mParametroOutRO.getValorParametro());
            MParametroOutRO mParametroOutPrecision = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_FRECUENCIA_TOMA_COORDENADAS_AUDITORIA))
                    .limit(1).unique();
            config.setPrecision(mParametroOutPrecision == null ? "10000" : mParametroOutPrecision.getValorParametro());
        } else {
            if (listaTipoInslacion.size() < 1) {
                if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    showProgressDialog(R.string.app_cargando);
                    TipoInstalacion tipoInstalacion = new TipoInstalacion(new TipoInstalacion.OnTipoInstalacionCompleted() {
                        @Override
                        public void onTipoInstalacionCompled(ListGenericBeanOutRO listGenericBeanOutRO) {
                            if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                hideProgressDialog();
                                ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                            } else if (listGenericBeanOutRO == null) {
                                hideProgressDialog();
                                ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                            } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                                hideProgressDialog();
                                ((App) getApplication()).showToast(listGenericBeanOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listGenericBeanOutRO.getMessage());
                            } else {
                                listaTipoInslacion = listGenericBeanOutRO.getGenericBean();
                                ArrayList<String> list = new ArrayList<>();
                                list.add("Seleccione tipo de instalación");
                                for (GenericBeanOutRO genericBeanOutRO : listaTipoInslacion) {
                                    list.add(genericBeanOutRO.getLabel());
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<>(TuberiaActivity.this, android.R.layout.simple_spinner_item, list);
                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                                spiTub_tipoInstalacion.setAdapter(adapter);
                                if (!("".equalsIgnoreCase(suministro.getTuberiaTipoInstalacion()))) {
                                    spiTub_tipoInstalacion.setSelection(getTipoInslacion(suministro.getTuberiaTipoInstalacion()));
                                } else {
                                    spiTub_tipoInstalacion.setSelection(0);
                                }
                                if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                    TipoMaterialInstalacion tipoMaterialInstalacion = new TipoMaterialInstalacion(new TipoMaterialInstalacion.OnTipoMaterialInstalacionCompleted() {
                                        @Override
                                        public void onTipoMaterialInstalacionCompled(ListGenericBeanOutRO listGenericBeanOutRO) {
                                            hideProgressDialog();
                                            if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                                ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                                            } else if (listGenericBeanOutRO == null) {
                                                ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                                            } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                                                ((App) getApplication()).showToast(listGenericBeanOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listGenericBeanOutRO.getMessage());
                                            } else {
                                                listaTipoMaterial = listGenericBeanOutRO.getGenericBean();
                                                ArrayList<String> list = new ArrayList<>();
                                                list.add("Seleccione material de instalación");
                                                for (GenericBeanOutRO genericBeanOutRO : listaTipoMaterial) {
                                                    list.add(genericBeanOutRO.getLabel());
                                                }
                                                ArrayAdapter<String> adapter = new ArrayAdapter<>(TuberiaActivity.this, android.R.layout.simple_spinner_item, list);
                                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                                                spiTub_materialInstalacion.setAdapter(adapter);
                                                if (!(suministro.getTuberiaMaterialInstalacion().equalsIgnoreCase(""))) {
                                                    spiTub_materialInstalacion.setSelection(getTipoMaterial(suministro.getTuberiaMaterialInstalacion()));
                                                } else {
                                                    spiTub_materialInstalacion.setSelection(0);
                                                }
                                                if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                                    TipoMarcaAccesorio tipoMarcaAccesorio = new TipoMarcaAccesorio(new TipoMarcaAccesorio.OnTipoMarcaAccesorioCompleted() {
                                                        @Override
                                                        public void onTipoMarcaAccesorioCompled(ListGenericBeanOutRO listGenericBeanOutRO) {
                                                            hideProgressDialog();
                                                            if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                                                ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                                                            } else if (listGenericBeanOutRO == null) {
                                                                ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                                                            } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                                                                ((App) getApplication()).showToast(listGenericBeanOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listGenericBeanOutRO.getMessage());
                                                            } else {
                                                                listaTipoMarcaAccesorio = listGenericBeanOutRO.getGenericBean();
                                                                ArrayList<String> list = new ArrayList<>();
                                                                list.add("Seleccione marca del accesorio");
                                                                for (GenericBeanOutRO genericBeanOutRO : listaTipoMarcaAccesorio) {
                                                                    list.add(genericBeanOutRO.getLabel());
                                                                }
                                                                ArrayAdapter<String> adapter = new ArrayAdapter<>(TuberiaActivity.this, android.R.layout.simple_spinner_item, list);
                                                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                                                                spiTub_marcaAccesorio.setAdapter(adapter);
                                                                if (!(suministro.getIdMarcaAccesorio().equalsIgnoreCase(""))) {
                                                                    spiTub_marcaAccesorio.setSelection(getTipoMarcaAccesorio(suministro.getIdMarcaAccesorio()));
                                                                } else {
                                                                    spiTub_marcaAccesorio.setSelection(0);
                                                                }
                                                                if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                                                    TipoMarcaTuberia tipoMarcaTuberia = new TipoMarcaTuberia(new TipoMarcaTuberia.OnTipoMarcaTuberiaCompleted() {
                                                                        @Override
                                                                        public void onTipoMarcaTuberiaCompled(ListGenericBeanOutRO listGenericBeanOutRO) {
                                                                            hideProgressDialog();
                                                                            if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                                                                ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                                                                            } else if (listGenericBeanOutRO == null) {
                                                                                ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                                                                            } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                                                                                ((App) getApplication()).showToast(listGenericBeanOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listGenericBeanOutRO.getMessage());
                                                                            } else {
                                                                                listaTipoMarcaTuberia = listGenericBeanOutRO.getGenericBean();
                                                                                ArrayList<String> list = new ArrayList<>();
                                                                                list.add("Seleccione marca de la tubería");
                                                                                for (GenericBeanOutRO genericBeanOutRO : listaTipoMarcaTuberia) {
                                                                                    list.add(genericBeanOutRO.getLabel());
                                                                                }
                                                                                ArrayAdapter<String> adapter = new ArrayAdapter<>(TuberiaActivity.this, android.R.layout.simple_spinner_item, list);
                                                                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                                                                                spiTub_marcaTuberia.setAdapter(adapter);
                                                                                if (!(suministro.getIdMarcaTuberia().equalsIgnoreCase(""))) {
                                                                                    spiTub_marcaTuberia.setSelection(getTipoMarcaTuberia(suministro.getIdMarcaTuberia()));
                                                                                } else {
                                                                                    spiTub_marcaTuberia.setSelection(0);
                                                                                }
                                                                                if ("".equalsIgnoreCase(config.getParametro5())) {
                                                                                    config.setParametro5("800x600");
                                                                                }
                                                                                if ("".equalsIgnoreCase(config.getPrecision())) {
                                                                                    config.setPrecision("10000");
                                                                                }
                                                                            }
                                                                        }
                                                                    });
                                                                    tipoMarcaTuberia.execute(config);
                                                                } else {
                                                                    ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                                                                }
                                                            }
                                                        }
                                                    });
                                                    tipoMarcaAccesorio.execute(config);
                                                } else {
                                                    ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                                                }
                                            }
                                        }
                                    });
                                    tipoMaterialInstalacion.execute(config);
                                } else {
                                    ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                                }
                            }
                        }
                    });
                    tipoInstalacion.execute(config);
                } else {
                    ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                }
            }
        }
    }

    private void cargaDatos() {
        listaTub_fotosRecorrido = new ArrayList<>();
        if (!suministro.getTuberiaFotoRecorrido1().equalsIgnoreCase("")) {
            listaTub_fotosRecorrido.add(suministro.getTuberiaFotoRecorrido1());
        }
        if (!suministro.getTuberiaFotoRecorrido2().equalsIgnoreCase("")) {
            listaTub_fotosRecorrido.add(suministro.getTuberiaFotoRecorrido2());
        }
        if (!suministro.getTuberiaFotoRecorrido3().equalsIgnoreCase("")) {
            listaTub_fotosRecorrido.add(suministro.getTuberiaFotoRecorrido3());
        }
        if (!suministro.getTuberiaFotoRecorrido4().equalsIgnoreCase("")) {
            listaTub_fotosRecorrido.add(suministro.getTuberiaFotoRecorrido4());
        }
        fotoAdaptador = new FotoAdaptador(this, listaTub_fotosRecorrido);
        fotoAdaptador.setFotos(listaTub_recorrido);
        fotoAdaptador.setTipo(10);
        fotoAdaptador.setIco(icoTub_recorrido);
        fotoAdaptador.setAprueba(swiTub_aprueba);
        listaTub_recorrido.setAdapter(fotoAdaptador);
        Util.setListViewHeightBasedOnChildren(listaTub_recorrido);
        if (!suministro.getTuberiaLongitudTotal().equalsIgnoreCase("")) {
            ediTub_longitudTotal.getEditText().setText(suministro.getTuberiaLongitudTotal());
            ediTub_longitudTotal.getEditText().setSelection(ediTub_longitudTotal.getEditText().getText().length());
            icoTub_longitudTotal.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if (!"".equalsIgnoreCase(suministro.getIsometricoFoto())) {
            loadBitmap(suministro.getIsometricoFoto(), imgTub_fotoIsometrico);
            icoTub_fotoIsometrico.setImageResource(R.mipmap.ic_done_black_36dp);
        }
        if (!(suministro.getTuberiaAprueba().equalsIgnoreCase("0"))) {
            swiTub_aprueba.setChecked(true);
        }
    }

    private void cargarSuministro() {
        txtDet_DocumentoPropietarioView.setText(suministro.getDocumentoPropietario());
        txtDet_nombrePropietarioView.setText(suministro.getNombrePropietario());
        txtDet_PredioView.setText(suministro.getPredio());
        txtDet_numeroIntHabilitacionView.setText(suministro.getNumeroIntHabilitacion());
        txtDet_numeroSolicitudView.setText(suministro.getNumeroSolicitud());
        txtDet_numeroContratoView.setText(suministro.getNumeroContrato());
        txtDet_fechaSolicitudView.setText(suministro.getFechaSolicitud());
        txtDet_fechaAprobacionView.setText(suministro.getFechaAprobacion());
        txtDet_tipoProyectoView.setText(suministro.getTipoProyecto());
        txtDet_tipoInstalacionView.setText(suministro.getTipoInstalacion());
        txtDet_numeroPuntosInstaacionView.setText(suministro.getNumeroPuntosInstaacion());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_general, menu);
        menuMas = menu.findItem(R.id.menuMas);
        menuMas.setVisible(true);
        menuMenos = menu.findItem(R.id.menuMenos);
        menuMenos.setVisible(false);
        return true;
    }

    private String setTipoInslacion(String tipoInslacion) {
        for (GenericBeanOutRO genericBeanOutRO : listaTipoInslacion) {
            if (tipoInslacion.equalsIgnoreCase(genericBeanOutRO.getLabel())) {
                return genericBeanOutRO.getValue();
            }
        }
        return null;
    }

    private int getTipoInslacion(String tipoInslacion) {
        int i = 0;
        boolean entro = false;
        for (GenericBeanOutRO genericBeanOutRO : listaTipoInslacion) {
            if (tipoInslacion.equalsIgnoreCase(genericBeanOutRO.getValue())) {
                entro = true;
                break;
            }
            i++;
        }
        if (entro) {
            return i + 1;
        } else {
            return 0;
        }
    }

    private String setTipoMaterial(String tipoMaterial) {
        for (GenericBeanOutRO genericBeanOutRO : listaTipoMaterial) {
            if (tipoMaterial.equalsIgnoreCase(genericBeanOutRO.getLabel())) {
                return genericBeanOutRO.getValue();
            }
        }
        return null;
    }

    private String setTipoMarcaAccesorio(String tipoMarcaAccesorio) {
        for (GenericBeanOutRO genericBeanOutRO : listaTipoMarcaAccesorio) {
            if (tipoMarcaAccesorio.equalsIgnoreCase(genericBeanOutRO.getLabel())) {
                return genericBeanOutRO.getValue();
            }
        }
        return null;
    }

    private String setTipoMarcaTuberia(String tipoMarcaTuberia) {
        for (GenericBeanOutRO genericBeanOutRO : listaTipoMarcaTuberia) {
            if (tipoMarcaTuberia.equalsIgnoreCase(genericBeanOutRO.getLabel())) {
                return genericBeanOutRO.getValue();
            }
        }
        return null;
    }

    private int getTipoMaterial(String tipoMaterial) {
        int i = 0;
        boolean entro = false;
        for (GenericBeanOutRO genericBeanOutRO : listaTipoMaterial) {
            if (tipoMaterial.equalsIgnoreCase(genericBeanOutRO.getValue())) {
                entro = true;
                break;
            }
            i++;
        }
        if (entro) {
            return i + 1;
        } else {
            return 0;
        }
    }

    private int getTipoMarcaAccesorio(String tipoMarcaAccesorio) {
        int i = 0;
        boolean entro = false;
        for (GenericBeanOutRO genericBeanOutRO : listaTipoMarcaAccesorio) {
            if (tipoMarcaAccesorio.equalsIgnoreCase(genericBeanOutRO.getValue())) {
                entro = true;
                break;
            }
            i++;
        }
        if (entro) {
            return i + 1;
        } else {
            return 0;
        }
    }

    private int getTipoMarcaTuberia(String tipoMarcaTuberia) {
        int i = 0;
        boolean entro = false;
        for (GenericBeanOutRO genericBeanOutRO : listaTipoMarcaTuberia) {
            if (tipoMarcaTuberia.equalsIgnoreCase(genericBeanOutRO.getValue())) {
                entro = true;
                break;
            }
            i++;
        }
        if (entro) {
            return i + 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                suministroDao.insertOrReplace(suministro);
                Intent intent = new Intent(TuberiaActivity.this, HabilitacionActivity.class);
                intent.putExtra("SUMINISTRO", suministro);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
                return true;
            case R.id.menuMas:
                tablaTuberia.setVisibility(View.VISIBLE);
                menuMas.setVisible(false);
                menuMenos.setVisible(true);
                return true;

            case R.id.menuMenos:
                tablaTuberia.setVisibility(View.GONE);
                menuMas.setVisible(true);
                menuMenos.setVisible(false);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        suministroDao.insertOrReplace(suministro);
        Intent intent = new Intent(TuberiaActivity.this, HabilitacionActivity.class);
        intent.putExtra("SUMINISTRO", suministro);
        intent.putExtra("CONFIG", config);
        startActivity(intent);
        finish();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        countDownTimer.createAndStart();
    }

    private void aceptarPermiso() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permiso = Util.getPermisos(TuberiaActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION);
            if (permiso != null) {
                requestPermissions(permiso, pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION);
            }
        }
    }

    private void createLocationRequest() {
        int value = 10000;
        try {
            value = Integer.valueOf(config.getPrecision());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(value);
        mLocationRequest.setFastestInterval(value);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(TuberiaActivity.this, 1458);
                    } catch (IntentSender.SendIntentException sendEx) {
                    }
                }
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            aceptarPermiso();
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION:
                String message = Util.onRequestPermissionsResult(permissions, grantResults);
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    public void loadBitmap(String ruta, final ImageView imageView) {
        MostrarFotoTask mostrarFotoTask = new MostrarFotoTask(new MostrarFotoTask.OnMostrarFotoTaskCompleted() {
            @Override
            public void onMostrarFotoTaskCompleted(Bitmap bitmap) {
                if (bitmap != null) {
                    if (imageView != null) {
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(120, 120);
                        layoutParams.setMargins(10, 10, 10, 10);
                        imageView.setLayoutParams(layoutParams);
                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        imageView.setImageBitmap(bitmap);
                    }
                } else {
                    ((App) getApplication()).showToast("Error al cargar la foto");
                }
            }
        });
        mostrarFotoTask.execute(ruta);
    }
}
