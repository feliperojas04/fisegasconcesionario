package pe.gob.osinergmin.gnr.cgn.data.models;

import java.io.Serializable;

public class BaseOutRO implements Serializable {
    private static final long serialVersionUID = 1L;
    private String resultCode;
    private String message;
    private String errorCode;

    public BaseOutRO() {
    }

    public String getResultCode() {
        return this.resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
