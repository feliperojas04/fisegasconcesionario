package pe.gob.osinergmin.gnr.cgn.data.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class MProyectoInstalacionOutRO {

    @Id
    private Long idProyectoInstalacion;
    private String codigoObjetoConexionProyectoInstalacion;
    private String codigoUnidadPredialProyectoInstalacion;
    private String nombreProyectoInstalacion;
    private String fechaRegistroPortalProyectoInstalacion;
    private String direccionUbigeoProyectoInstalacion;
    private Integer idEstadoProyectoInstalacion;
    private String nombreEstadoProyectoInstalacion;
    private Integer idEmpresaInstaladora;
    private String documentoIdentificacionEmpresaInstaladora;
    private String nombreCortoEmpresaInstaladora;
    private String nombreEmpresaInstaladora;
    private String nombreDocumentoIdentificacionEmpresaInstaladora;
    private String numeroRegistroGnEmpresaInstaladora;
    private String coordenadasProyectoInstalacion;
    private String nombreTipoProyectoInstalacion;
    private String nombreEsProyectoFiseProyectoInstalacion;
    private String nombreAplicaMecanismoPromocionProyectoInstalacion;
    private String nombreEsZonaGasificadaProyectoInstalacion;
    private String nombreTienePlanoInstalacionInternaProyectoInstalacion;
    private String nombreTieneEspecificacionTecnicaProyectoInstalacion;
    private String nombreTieneMemoriaCalculosProyectoInstalacion;
    private String resultCode;
    private String message;
    private String errorCode;

    @Generated(hash = 622346065)
    public MProyectoInstalacionOutRO() {
    }

    public MProyectoInstalacionOutRO(Long idProyectoInstalacion) {
        this.idProyectoInstalacion = idProyectoInstalacion;
    }

    @Generated(hash = 1211841170)
    public MProyectoInstalacionOutRO(Long idProyectoInstalacion, String codigoObjetoConexionProyectoInstalacion, String codigoUnidadPredialProyectoInstalacion, String nombreProyectoInstalacion, String fechaRegistroPortalProyectoInstalacion, String direccionUbigeoProyectoInstalacion, Integer idEstadoProyectoInstalacion, String nombreEstadoProyectoInstalacion, Integer idEmpresaInstaladora, String documentoIdentificacionEmpresaInstaladora, String nombreCortoEmpresaInstaladora, String nombreEmpresaInstaladora, String nombreDocumentoIdentificacionEmpresaInstaladora, String numeroRegistroGnEmpresaInstaladora, String coordenadasProyectoInstalacion, String nombreTipoProyectoInstalacion, String nombreEsProyectoFiseProyectoInstalacion, String nombreAplicaMecanismoPromocionProyectoInstalacion, String nombreEsZonaGasificadaProyectoInstalacion, String nombreTienePlanoInstalacionInternaProyectoInstalacion, String nombreTieneEspecificacionTecnicaProyectoInstalacion, String nombreTieneMemoriaCalculosProyectoInstalacion, String resultCode, String message, String errorCode) {
        this.idProyectoInstalacion = idProyectoInstalacion;
        this.codigoObjetoConexionProyectoInstalacion = codigoObjetoConexionProyectoInstalacion;
        this.codigoUnidadPredialProyectoInstalacion = codigoUnidadPredialProyectoInstalacion;
        this.nombreProyectoInstalacion = nombreProyectoInstalacion;
        this.fechaRegistroPortalProyectoInstalacion = fechaRegistroPortalProyectoInstalacion;
        this.direccionUbigeoProyectoInstalacion = direccionUbigeoProyectoInstalacion;
        this.idEstadoProyectoInstalacion = idEstadoProyectoInstalacion;
        this.nombreEstadoProyectoInstalacion = nombreEstadoProyectoInstalacion;
        this.idEmpresaInstaladora = idEmpresaInstaladora;
        this.documentoIdentificacionEmpresaInstaladora = documentoIdentificacionEmpresaInstaladora;
        this.nombreCortoEmpresaInstaladora = nombreCortoEmpresaInstaladora;
        this.nombreEmpresaInstaladora = nombreEmpresaInstaladora;
        this.nombreDocumentoIdentificacionEmpresaInstaladora = nombreDocumentoIdentificacionEmpresaInstaladora;
        this.numeroRegistroGnEmpresaInstaladora = numeroRegistroGnEmpresaInstaladora;
        this.coordenadasProyectoInstalacion = coordenadasProyectoInstalacion;
        this.nombreTipoProyectoInstalacion = nombreTipoProyectoInstalacion;
        this.nombreEsProyectoFiseProyectoInstalacion = nombreEsProyectoFiseProyectoInstalacion;
        this.nombreAplicaMecanismoPromocionProyectoInstalacion = nombreAplicaMecanismoPromocionProyectoInstalacion;
        this.nombreEsZonaGasificadaProyectoInstalacion = nombreEsZonaGasificadaProyectoInstalacion;
        this.nombreTienePlanoInstalacionInternaProyectoInstalacion = nombreTienePlanoInstalacionInternaProyectoInstalacion;
        this.nombreTieneEspecificacionTecnicaProyectoInstalacion = nombreTieneEspecificacionTecnicaProyectoInstalacion;
        this.nombreTieneMemoriaCalculosProyectoInstalacion = nombreTieneMemoriaCalculosProyectoInstalacion;
        this.resultCode = resultCode;
        this.message = message;
        this.errorCode = errorCode;
    }

    public Long getIdProyectoInstalacion() {
        return idProyectoInstalacion;
    }

    public void setIdProyectoInstalacion(Long idProyectoInstalacion) {
        this.idProyectoInstalacion = idProyectoInstalacion;
    }

    public String getCodigoObjetoConexionProyectoInstalacion() {
        return codigoObjetoConexionProyectoInstalacion;
    }

    public void setCodigoObjetoConexionProyectoInstalacion(String codigoObjetoConexionProyectoInstalacion) {
        this.codigoObjetoConexionProyectoInstalacion = codigoObjetoConexionProyectoInstalacion;
    }

    public String getCodigoUnidadPredialProyectoInstalacion() {
        return codigoUnidadPredialProyectoInstalacion;
    }

    public void setCodigoUnidadPredialProyectoInstalacion(String codigoUnidadPredialProyectoInstalacion) {
        this.codigoUnidadPredialProyectoInstalacion = codigoUnidadPredialProyectoInstalacion;
    }

    public String getNombreProyectoInstalacion() {
        return nombreProyectoInstalacion;
    }

    public void setNombreProyectoInstalacion(String nombreProyectoInstalacion) {
        this.nombreProyectoInstalacion = nombreProyectoInstalacion;
    }

    public String getFechaRegistroPortalProyectoInstalacion() {
        return fechaRegistroPortalProyectoInstalacion;
    }

    public void setFechaRegistroPortalProyectoInstalacion(String fechaRegistroPortalProyectoInstalacion) {
        this.fechaRegistroPortalProyectoInstalacion = fechaRegistroPortalProyectoInstalacion;
    }

    public String getDireccionUbigeoProyectoInstalacion() {
        return direccionUbigeoProyectoInstalacion;
    }

    public void setDireccionUbigeoProyectoInstalacion(String direccionUbigeoProyectoInstalacion) {
        this.direccionUbigeoProyectoInstalacion = direccionUbigeoProyectoInstalacion;
    }

    public Integer getIdEstadoProyectoInstalacion() {
        return idEstadoProyectoInstalacion;
    }

    public void setIdEstadoProyectoInstalacion(Integer idEstadoProyectoInstalacion) {
        this.idEstadoProyectoInstalacion = idEstadoProyectoInstalacion;
    }

    public String getNombreEstadoProyectoInstalacion() {
        return nombreEstadoProyectoInstalacion;
    }

    public void setNombreEstadoProyectoInstalacion(String nombreEstadoProyectoInstalacion) {
        this.nombreEstadoProyectoInstalacion = nombreEstadoProyectoInstalacion;
    }

    public Integer getIdEmpresaInstaladora() {
        return idEmpresaInstaladora;
    }

    public void setIdEmpresaInstaladora(Integer idEmpresaInstaladora) {
        this.idEmpresaInstaladora = idEmpresaInstaladora;
    }

    public String getDocumentoIdentificacionEmpresaInstaladora() {
        return documentoIdentificacionEmpresaInstaladora;
    }

    public void setDocumentoIdentificacionEmpresaInstaladora(String documentoIdentificacionEmpresaInstaladora) {
        this.documentoIdentificacionEmpresaInstaladora = documentoIdentificacionEmpresaInstaladora;
    }

    public String getNombreCortoEmpresaInstaladora() {
        return nombreCortoEmpresaInstaladora;
    }

    public void setNombreCortoEmpresaInstaladora(String nombreCortoEmpresaInstaladora) {
        this.nombreCortoEmpresaInstaladora = nombreCortoEmpresaInstaladora;
    }

    public String getNombreEmpresaInstaladora() {
        return nombreEmpresaInstaladora;
    }

    public void setNombreEmpresaInstaladora(String nombreEmpresaInstaladora) {
        this.nombreEmpresaInstaladora = nombreEmpresaInstaladora;
    }

    public String getNombreDocumentoIdentificacionEmpresaInstaladora() {
        return nombreDocumentoIdentificacionEmpresaInstaladora;
    }

    public void setNombreDocumentoIdentificacionEmpresaInstaladora(String nombreDocumentoIdentificacionEmpresaInstaladora) {
        this.nombreDocumentoIdentificacionEmpresaInstaladora = nombreDocumentoIdentificacionEmpresaInstaladora;
    }

    public String getNumeroRegistroGnEmpresaInstaladora() {
        return numeroRegistroGnEmpresaInstaladora;
    }

    public void setNumeroRegistroGnEmpresaInstaladora(String numeroRegistroGnEmpresaInstaladora) {
        this.numeroRegistroGnEmpresaInstaladora = numeroRegistroGnEmpresaInstaladora;
    }

    public String getCoordenadasProyectoInstalacion() {
        return coordenadasProyectoInstalacion;
    }

    public void setCoordenadasProyectoInstalacion(String coordenadasProyectoInstalacion) {
        this.coordenadasProyectoInstalacion = coordenadasProyectoInstalacion;
    }

    public String getNombreTipoProyectoInstalacion() {
        return nombreTipoProyectoInstalacion;
    }

    public void setNombreTipoProyectoInstalacion(String nombreTipoProyectoInstalacion) {
        this.nombreTipoProyectoInstalacion = nombreTipoProyectoInstalacion;
    }

    public String getNombreEsProyectoFiseProyectoInstalacion() {
        return nombreEsProyectoFiseProyectoInstalacion;
    }

    public void setNombreEsProyectoFiseProyectoInstalacion(String nombreEsProyectoFiseProyectoInstalacion) {
        this.nombreEsProyectoFiseProyectoInstalacion = nombreEsProyectoFiseProyectoInstalacion;
    }

    public String getNombreAplicaMecanismoPromocionProyectoInstalacion() {
        return nombreAplicaMecanismoPromocionProyectoInstalacion;
    }

    public void setNombreAplicaMecanismoPromocionProyectoInstalacion(String nombreAplicaMecanismoPromocionProyectoInstalacion) {
        this.nombreAplicaMecanismoPromocionProyectoInstalacion = nombreAplicaMecanismoPromocionProyectoInstalacion;
    }

    public String getNombreEsZonaGasificadaProyectoInstalacion() {
        return nombreEsZonaGasificadaProyectoInstalacion;
    }

    public void setNombreEsZonaGasificadaProyectoInstalacion(String nombreEsZonaGasificadaProyectoInstalacion) {
        this.nombreEsZonaGasificadaProyectoInstalacion = nombreEsZonaGasificadaProyectoInstalacion;
    }

    public String getNombreTienePlanoInstalacionInternaProyectoInstalacion() {
        return nombreTienePlanoInstalacionInternaProyectoInstalacion;
    }

    public void setNombreTienePlanoInstalacionInternaProyectoInstalacion(String nombreTienePlanoInstalacionInternaProyectoInstalacion) {
        this.nombreTienePlanoInstalacionInternaProyectoInstalacion = nombreTienePlanoInstalacionInternaProyectoInstalacion;
    }

    public String getNombreTieneEspecificacionTecnicaProyectoInstalacion() {
        return nombreTieneEspecificacionTecnicaProyectoInstalacion;
    }

    public void setNombreTieneEspecificacionTecnicaProyectoInstalacion(String nombreTieneEspecificacionTecnicaProyectoInstalacion) {
        this.nombreTieneEspecificacionTecnicaProyectoInstalacion = nombreTieneEspecificacionTecnicaProyectoInstalacion;
    }

    public String getNombreTieneMemoriaCalculosProyectoInstalacion() {
        return nombreTieneMemoriaCalculosProyectoInstalacion;
    }

    public void setNombreTieneMemoriaCalculosProyectoInstalacion(String nombreTieneMemoriaCalculosProyectoInstalacion) {
        this.nombreTieneMemoriaCalculosProyectoInstalacion = nombreTieneMemoriaCalculosProyectoInstalacion;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
