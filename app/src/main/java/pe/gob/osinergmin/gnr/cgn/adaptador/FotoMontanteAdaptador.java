package pe.gob.osinergmin.gnr.cgn.adaptador;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.appcompat.widget.SwitchCompat;

import java.util.List;

import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.util.MostrarFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.Util;

public class FotoMontanteAdaptador extends BaseAdapter {

    private Context context;
    private List<String> fotos;
    private ListView listaFotos;
    private ImageView ico;
    private SwitchCompat aprueba;
    private int tipo;

    public FotoMontanteAdaptador(Context context, List<String> fotos) {
        this.context = context;
        this.fotos = fotos;
    }

    public void setIco(ImageView ico) {
        this.ico = ico;
    }

    public void setFotos(ListView listaFotos) {
        this.listaFotos = listaFotos;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public void setAprueba(SwitchCompat aprueba) {
        this.aprueba = aprueba;
    }

    @Override
    public int getCount() {
        if (fotos.size() < 3) {
            ico.setImageResource(R.mipmap.ic_warning_black_36dp);
            aprueba.setChecked(false);
            aprueba.setClickable(false);
        }
        if (tipo == 10 & fotos.size() == 0) {
        }
        if (tipo == 20 & fotos.size() == 0) {
        }
        return this.fotos.size();
    }

    @Override
    public Object getItem(int position) {
        return this.fotos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = layoutInflater.inflate(R.layout.item_foto, parent, false);

            holder = new ViewHolder();
            holder.imgFoto = (ImageView) rowView.findViewById(R.id.imgFoto);
            holder.removeFoto = (ImageView) rowView.findViewById(R.id.removeFoto);

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        holder.ref = position;
        holder.removeFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fotos.remove(holder.ref);
                notifyDataSetChanged();
                Util.setListViewHeightBasedOnChildren(listaFotos);
            }
        });

        if (!"".equalsIgnoreCase(fotos.get(position))) {
            loadBitmap(fotos.get(position), holder.imgFoto);
        }
        //byte[] tmp = fotos.get(position);
        // holder.imgFoto.setImageBitmap(BitmapFactory.decodeByteArray(tmp, 0, fotos.get(position).length));

        holder.imgFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //((Activity) context).startActivityForResult(intent, tipo + holder.ref);
            }
        });

        return rowView;
    }

    private void loadBitmap(String ruta, final ImageView imageView) {
        MostrarFotoTask mostrarFotoTask = new MostrarFotoTask(new MostrarFotoTask.OnMostrarFotoTaskCompleted() {
            @Override
            public void onMostrarFotoTaskCompleted(Bitmap bitmap) {
                if (bitmap != null) {
                    if (imageView != null) {
                        imageView.setImageBitmap(bitmap);
                    }
                } else {
                    ((App) context.getApplicationContext()).showToast("Error al cargar la foto");
                }
            }
        });
        mostrarFotoTask.execute(ruta);
        //BitmapWorkerTask2 task = new BitmapWorkerTask2(imageView);
        //task.execute(ruta);
    }

    private class ViewHolder {
        int ref;
        ImageView imgFoto;
        ImageView removeFoto;
    }
}
