package pe.gob.osinergmin.gnr.cgn.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.os.AsyncTask;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ReducirFotoTask extends AsyncTask<String, Void, String> {

    private OnReducirFotoTaskCompleted listener;

    public ReducirFotoTask(OnReducirFotoTaskCompleted listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        int orientacion;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bitmap;
        FileOutputStream out;
        try {
            try {
                BitmapFactory.decodeStream(new FileInputStream(params[0]), null, options);
                String[] size = params[1].split("x");
                options.inSampleSize = Util.calculateInSampleSize(options, Integer.parseInt(size[0]), Integer.parseInt(size[1]));
                options.inJustDecodeBounds = false;
                ExifInterface exifInterface = new ExifInterface(params[0]);
                orientacion = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                bitmap = Util.rotateBitmap(BitmapFactory.decodeStream(new FileInputStream(params[0]), null, options), orientacion);
                out = new FileOutputStream(params[0]);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return "Error al reducir la foto. No se encontro la ruta.";
            } catch (NumberFormatException e) {
                e.printStackTrace();
                return "Error al reducir la foto. El tamaño de la foto es incorrecto.";
            }
            if (bitmap != null) {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            }
            out.flush();
            out.close();
            return "1";
        } catch (IOException e) {
            e.printStackTrace();
            return "Error al reducir la foto.";
        }
    }

    @Override
    protected void onPostExecute(String mensaje) {
        listener.onReducirFotoTaskCompleted(mensaje);
    }

    public interface OnReducirFotoTaskCompleted {
        void onReducirFotoTaskCompleted(String result);
    }
}
