package pe.gob.osinergmin.gnr.cgn.actividad;

import android.Manifest;
import android.content.ClipData;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.GenericBeanOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListGenericBeanOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.adaptador.AmbienteAdaptador;
import pe.gob.osinergmin.gnr.cgn.adaptador.MonoxidoAdaptador;
import pe.gob.osinergmin.gnr.cgn.data.models.Ambiente;
import pe.gob.osinergmin.gnr.cgn.data.models.AmbienteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.AmbientePredio;
import pe.gob.osinergmin.gnr.cgn.data.models.AmbientePredioDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.Precision;
import pe.gob.osinergmin.gnr.cgn.data.models.PrecisionDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Suministro;
import pe.gob.osinergmin.gnr.cgn.data.models.SuministroDao;
import pe.gob.osinergmin.gnr.cgn.task.TipoAmbientePredio;
import pe.gob.osinergmin.gnr.cgn.util.DownTimer;
import pe.gob.osinergmin.gnr.cgn.util.NetworkUtil;
import pe.gob.osinergmin.gnr.cgn.util.Parse;
import pe.gob.osinergmin.gnr.cgn.util.ReducirFotoTask;
import pe.gob.osinergmin.gnr.cgn.util.Util;

public class AmbienteActivity extends BaseActivity implements AmbienteAdaptador.Callback {

    private Suministro suministro;
    private Config config;
    private TableLayout tablaAmbiente;
    private TextView txtDet_DocumentoPropietarioView;
    private TextView txtDet_nombrePropietarioView;
    private TextView txtDet_PredioView;
    private TextView txtDet_numeroIntHabilitacionView;
    private TextView txtDet_numeroSolicitudView;
    private TextView txtDet_numeroContratoView;
    private TextView txtDet_fechaSolicitudView;
    private TextView txtDet_fechaAprobacionView;
    private TextView txtDet_tipoProyectoView;
    private TextView txtDet_tipoInstalacionView;
    private TextView txtDet_numeroPuntosInstaacionView;
    private ListView listaAmbientes;
    private List<Ambiente> ambientes;
    private SwitchCompat swiAmb_aprueba;
    private Button btnAmb_rechazar;
    private Toolbar toolbar;
    private MenuItem menuMas;
    private MenuItem menuMenos;
    private AmbienteAdaptador ambienteAdaptador;
    private SuministroDao suministroDao;
    private AmbienteDao ambienteDao;
    private List<GenericBeanOutRO> listaTipoAmbiente = new ArrayList<>();
    private DownTimer countDownTimer;
    private MParametroOutRODao mParametroOutRODao;
    private AmbientePredioDao ambientePredioDao;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private PrecisionDao precisionDao;
    private String tmpRuta;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ambiente);

        countDownTimer = DownTimer.getInstance();

        suministroDao = ((App) getApplication()).getDaoSession().getSuministroDao();
        ambienteDao = ((App) getApplication()).getDaoSession().getAmbienteDao();
        mParametroOutRODao = ((App) getApplication()).getDaoSession().getMParametroOutRODao();
        ambientePredioDao = ((App) getApplication()).getDaoSession().getAmbientePredioDao();
        precisionDao = ((App) getApplication()).getDaoSession().getPrecisionDao();

        toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_36dp);

        suministro = getIntent().getExtras().getParcelable("SUMINISTRO");
        config = getIntent().getExtras().getParcelable("CONFIG");

        tablaAmbiente = findViewById(R.id.tablaAmbiente);
        txtDet_DocumentoPropietarioView = findViewById(R.id.txtDet_DocumentoPropietarioView);
        txtDet_nombrePropietarioView = findViewById(R.id.txtDet_nombrePropietarioView);
        txtDet_PredioView = findViewById(R.id.txtDet_PredioView);
        txtDet_numeroIntHabilitacionView = findViewById(R.id.txtDet_numeroIntHabilitacionView);
        txtDet_numeroSolicitudView = findViewById(R.id.txtDet_numeroSolicitudView);
        txtDet_numeroContratoView = findViewById(R.id.txtDet_numeroContratoView);
        txtDet_fechaSolicitudView = findViewById(R.id.txtDet_fechaSolicitudView);
        txtDet_fechaAprobacionView = findViewById(R.id.txtDet_fechaAprobacionView);
        txtDet_tipoProyectoView = findViewById(R.id.txtDet_tipoProyectoView);
        txtDet_tipoInstalacionView = findViewById(R.id.txtDet_tipoInstalacionView);
        txtDet_numeroPuntosInstaacionView = findViewById(R.id.txtDet_numeroPuntosInstaacionView);
        ImageView addAmb_ambientes = findViewById(R.id.addAmb_ambientes);
        listaAmbientes = findViewById(R.id.listaAmbientes);
        swiAmb_aprueba = findViewById(R.id.swiAmb_aprueba);
        Button btnAmb_evaluando = findViewById(R.id.btnAmb_evaluando);
        btnAmb_rechazar = findViewById(R.id.btnAmb_rechazar);

        addAmb_ambientes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ambiente ambiente = new Ambiente(null, suministro.getIdHabilitacion(), "", "", "", "", "", "", "", "", "", -1, "", "");
                if (ambientes != null) {
                    ambientes.add(ambiente);
                }
                ambienteDao.insertOrReplace(ambiente);
                if (ambienteAdaptador != null) {
                    ambienteAdaptador.notifyDataSetChanged();
                }
                Util.setListViewHeightBasedOnChildren(listaAmbientes);
                suministro.setAmbientesAprueba("0");
                swiAmb_aprueba.setChecked(false);
                swiAmb_aprueba.setClickable(false);
            }
        });

        swiAmb_aprueba.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    suministro.setAmbientesAprueba("1");
                    btnAmb_rechazar.setVisibility(View.GONE);
                } else {
                    suministro.setAmbientesAprueba("0");
                    btnAmb_rechazar.setVisibility(View.VISIBLE);
                }
            }
        });

        btnAmb_evaluando.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkMonoxido();
                suministroDao.insertOrReplace(suministro);
                Intent intent = new Intent(AmbienteActivity.this, HermeticidadActivity.class);
                intent.putExtra("SUMINISTRO", suministro);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
            }
        });

        btnAmb_rechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AmbienteActivity.this, RechazoActivity.class);
                intent.putExtra("SUMINISTRO", suministro);
                intent.putExtra("CONFIG", config);
                intent.putExtra("TIPO", "");
                startActivity(intent);
            }
        });

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    Precision precision = new Precision();
                    precision.setIdSolicitud(suministro.getIdHabilitacion());
                    precision.setLatitud(String.valueOf(location.getLatitude()));
                    precision.setLongitud(String.valueOf(location.getLongitude()));
                    precision.setPrecision(String.valueOf(location.getAccuracy()));
                    precision.setEstado("1");
                    precisionDao.insertOrReplace(precision);
                }
            }
        };
        aceptarPermiso();
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.createAndStart();
        createLocationRequest();
        toolbar.setTitle(suministro.getSuministro() + " - Ambientes");
        cargarSuministro();
        if (config.getOffline()) {
            listaTipoAmbiente.clear();
            List<AmbientePredio> ambientePredios = ambientePredioDao.queryBuilder().list();
            for (AmbientePredio ambientePredio : ambientePredios) {
                listaTipoAmbiente.add(Parse.getGenericBeanOutRO(ambientePredio));
            }
            MParametroOutRO mParametroOutRO = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_CONFIGURACION_COMPRESION_FOTOS_APLICATIVO_MOVIL))
                    .limit(1).unique();
            config.setParametro5(mParametroOutRO == null ? "800x600" : mParametroOutRO.getValorParametro());
            MParametroOutRO mParametroOutPrecision = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_FRECUENCIA_TOMA_COORDENADAS_AUDITORIA))
                    .limit(1).unique();
            config.setPrecision(mParametroOutPrecision == null ? "10000" : mParametroOutPrecision.getValorParametro());
            cargaDatos();
        } else {
            if ("".equalsIgnoreCase(config.getParametro5())) {
                config.setParametro5("800x600");
            }
            if ("".equalsIgnoreCase(config.getPrecision())) {
                config.setPrecision("10000");
            }
            if (listaTipoAmbiente.size() > 0 && !"".equalsIgnoreCase(config.getParametro5())) {
                cargaDatos();
            } else {
                if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    showProgressDialog(R.string.app_cargando);
                    TipoAmbientePredio tipoAmbientePredio = new TipoAmbientePredio(new TipoAmbientePredio.OnAmbientePredioCompleted() {
                        @Override
                        public void onAmbientePredioCompled(ListGenericBeanOutRO listGenericBeanOutRO) {
                            hideProgressDialog();
                            if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                            } else if (listGenericBeanOutRO == null) {
                                ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                            } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listGenericBeanOutRO.getResultCode())) {
                                ((App) getApplication()).showToast(listGenericBeanOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listGenericBeanOutRO.getMessage());
                            } else {
                                listaTipoAmbiente = listGenericBeanOutRO.getGenericBean();
                                cargaDatos();
                            }
                        }
                    });
                    tipoAmbientePredio.execute(config);
                } else {
                    ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                }
            }
        }
    }

    @Override
    public void clicFoto(int tipo, int position) {
        if (ActivityCompat.checkSelfPermission(
                AmbienteActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ((App) getApplication()).showToast("No puede realizar la acción porque no se ha dado el permiso de Almacenamiento");
            return;
        }
        if (Util.isExternalStorageWritable()) {
            if (Util.getAlbumStorageDir(AmbienteActivity.this)) {
                File file = Util.getNuevaRutaFoto(suministro.getIdHabilitacion().toString(), AmbienteActivity.this);
                tmpRuta = file.getPath();
                this.position = position;
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                Uri outputUri = FileProvider.getUriForFile(AmbienteActivity.this, getApplicationContext().getPackageName() + ".provider", file);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    ClipData clip = ClipData.newUri(getContentResolver(), "Instalacion", outputUri);
                    intent.setClipData(clip);
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                }
                startActivityForResult(intent, tipo);
            } else {
                ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
            }
        } else {
            ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
        }
    }

    private void cargaDatos() {
        ambientes = ambienteDao.queryBuilder()
                .where(AmbienteDao.Properties.IdHabilitacion.eq(suministro.getIdHabilitacion())).list();
        if (ambientes.size() > 0) {
            ambienteAdaptador = new AmbienteAdaptador(this, ambientes);
            ambienteAdaptador.setCallback(this);
            ambienteAdaptador.setAmbientes(listaAmbientes);
            ambienteAdaptador.setAprueba(swiAmb_aprueba);
            ambienteAdaptador.setAmbienteDao(ambienteDao);
            ambienteAdaptador.setListatipoObse(listaTipoAmbiente);
            listaAmbientes.setAdapter(ambienteAdaptador);
            Util.setListViewHeightBasedOnChildren(listaAmbientes);
        } else {
            ambientes = new ArrayList<>();
            Ambiente ambiente = new Ambiente(null, suministro.getIdHabilitacion(), "", "", "", "", "", "", "", "", "", -1, "", "");
            ambienteDao.insertOrReplace(ambiente);
            ambientes.add(ambiente);
            ambienteAdaptador = new AmbienteAdaptador(this, ambientes);
            ambienteAdaptador.setCallback(this);
            ambienteAdaptador.setAmbientes(listaAmbientes);
            ambienteAdaptador.setAprueba(swiAmb_aprueba);
            ambienteAdaptador.setAmbienteDao(ambienteDao);
            ambienteAdaptador.setListatipoObse(listaTipoAmbiente);
            listaAmbientes.setAdapter(ambienteAdaptador);
            Util.setListViewHeightBasedOnChildren(listaAmbientes);
        }
        if (!("0".equalsIgnoreCase(suministro.getAmbientesAprueba()))) {
            swiAmb_aprueba.setChecked(true);
        } else {
            boolean entro = true;
            for (Ambiente ambiente : ambientes) {
                if ("".equalsIgnoreCase(ambiente.getAmbienteTipo()) ||
                        "".equalsIgnoreCase(ambiente.getAmbienteUnoFoto()) ||
                        "".equalsIgnoreCase(ambiente.getAmbienteDosFoto())) {
                    entro = false;
                    break;
                }
            }
            swiAmb_aprueba.setClickable(entro);
        }
    }

    private void cargarSuministro() {
        txtDet_DocumentoPropietarioView.setText(suministro.getDocumentoPropietario());
        txtDet_nombrePropietarioView.setText(suministro.getNombrePropietario());
        txtDet_PredioView.setText(suministro.getPredio());
        txtDet_numeroIntHabilitacionView.setText(suministro.getNumeroIntHabilitacion());
        txtDet_numeroSolicitudView.setText(suministro.getNumeroSolicitud());
        txtDet_numeroContratoView.setText(suministro.getNumeroContrato());
        txtDet_fechaSolicitudView.setText(suministro.getFechaSolicitud());
        txtDet_fechaAprobacionView.setText(suministro.getFechaAprobacion());
        txtDet_tipoProyectoView.setText(suministro.getTipoProyecto());
        txtDet_tipoInstalacionView.setText(suministro.getTipoInstalacion());
        txtDet_numeroPuntosInstaacionView.setText(suministro.getNumeroPuntosInstaacion());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_general, menu);
        menuMas = menu.findItem(R.id.menuMas);
        menuMas.setVisible(true);
        menuMenos = menu.findItem(R.id.menuMenos);
        menuMenos.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                checkMonoxido();
                suministroDao.insertOrReplace(suministro);
                Intent intent = new Intent(AmbienteActivity.this, HabilitacionActivity.class);
                intent.putExtra("SUMINISTRO", suministro);
                intent.putExtra("CONFIG", config);
                startActivity(intent);
                finish();
                return true;
            case R.id.menuMas:
                tablaAmbiente.setVisibility(View.VISIBLE);
                menuMas.setVisible(false);
                menuMenos.setVisible(true);
                return true;

            case R.id.menuMenos:
                tablaAmbiente.setVisibility(View.GONE);
                menuMas.setVisible(true);
                menuMenos.setVisible(false);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        checkMonoxido();
        suministroDao.insertOrReplace(suministro);
        Intent intent = new Intent(AmbienteActivity.this, HabilitacionActivity.class);
        intent.putExtra("SUMINISTRO", suministro);
        intent.putExtra("CONFIG", config);
        startActivity(intent);
        finish();
    }

    public void checkMonoxido() {
        if (ambientes != null) {
            int i = 0;
            while (i < ambientes.size()) {
                Ambiente ambiente = ambientes.get(i);
                if ("1".equalsIgnoreCase(ambiente.getAmbienteConfinado())) {
                    if ("".equalsIgnoreCase(ambiente.getMonoxidoMedicion())) {
                        suministro.setMonoxidoAprueba("0");
                        i = ambientes.size();
                    }
                }
                i++;
            }
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        countDownTimer.createAndStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1458) {
            if (resultCode == RESULT_CANCELED) {
                Intent intent = new Intent(AmbienteActivity.this, BuscarSuministroActivity.class);
                startActivity(intent);
                finish();
            }
        }
        if (requestCode == 1) {
            if (resultCode != RESULT_CANCELED) {
                ((AmbienteAdaptador) listaAmbientes.getAdapter()).getAmbiente(position).setAmbienteUnoFoto(tmpRuta);
                ReducirFotoTask reducirFotoTask = new ReducirFotoTask(new ReducirFotoTask.OnReducirFotoTaskCompleted() {
                    @Override
                    public void onReducirFotoTaskCompleted(String result) {
                        if (!("1".equalsIgnoreCase(result))) {
                            ((App) getApplication()).showToast(result);
                        }
                    }
                });
                ambienteDao.insertOrReplace(((AmbienteAdaptador) listaAmbientes.getAdapter()).getAmbiente(position));
                reducirFotoTask.execute(((AmbienteAdaptador) listaAmbientes.getAdapter()).getAmbiente(position).getAmbienteUnoFoto()
                        , config.getParametro5());
                ((AmbienteAdaptador) listaAmbientes.getAdapter()).notifyDataSetChanged();
            }
        }
        if (requestCode == 2) {
            if (resultCode != RESULT_CANCELED) {
                ((AmbienteAdaptador) listaAmbientes.getAdapter()).getAmbiente(position).setAmbienteDosFoto(tmpRuta);
                ReducirFotoTask reducirFotoTask = new ReducirFotoTask(new ReducirFotoTask.OnReducirFotoTaskCompleted() {
                    @Override
                    public void onReducirFotoTaskCompleted(String result) {
                        if (!("1".equalsIgnoreCase(result))) {
                            ((App) getApplication()).showToast(result);
                        }
                    }
                });
                ambienteDao.insertOrReplace(((AmbienteAdaptador) listaAmbientes.getAdapter()).getAmbiente(position));
                reducirFotoTask.execute(((AmbienteAdaptador) listaAmbientes.getAdapter()).getAmbiente(position).getAmbienteDosFoto()
                        , config.getParametro5());
                ((AmbienteAdaptador) listaAmbientes.getAdapter()).notifyDataSetChanged();
            }
        }
    }

    private void createLocationRequest() {
        int value = 10000;
        try {
            value = Integer.valueOf(config.getPrecision());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(value);
        mLocationRequest.setFastestInterval(value);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(AmbienteActivity.this, 1458);
                    } catch (IntentSender.SendIntentException sendEx) {
                    }
                }
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            aceptarPermiso();
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
    }

    private void aceptarPermiso() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permiso = Util.getPermisos(AmbienteActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION);
            if (permiso != null) {
                requestPermissions(permiso, pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case pe.gob.osinergmin.gnr.cgn.util.Constantes.INTENT_PERMISSION:
                String message = Util.onRequestPermissionsResult(permissions, grantResults);
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }
}
