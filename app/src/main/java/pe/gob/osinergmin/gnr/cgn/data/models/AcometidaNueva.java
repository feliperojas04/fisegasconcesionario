package pe.gob.osinergmin.gnr.cgn.data.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class AcometidaNueva {

    @Id
    private Long id;
    private int IdInstalacionAcometida;
    private int idClasificacionGabinete;
    private String nombreClasificacionGabinete;
    private int valorClasificacionGabinete;
    private int numerodeOrden;
    private int numerodeGabinete;
    private String fotoGabinete;
    private String tipoGabinete;
    private Integer posicion;
    private Integer idGabineteProyectoInstalacion;

    @Generated(hash = 801726548)
    public AcometidaNueva() {
    }

    @Generated(hash = 1786800111)
    public AcometidaNueva(Long id, int IdInstalacionAcometida, int idClasificacionGabinete, String nombreClasificacionGabinete, int valorClasificacionGabinete, int numerodeOrden, int numerodeGabinete, String fotoGabinete, String tipoGabinete, Integer posicion, Integer idGabineteProyectoInstalacion) {
        this.id = id;
        this.IdInstalacionAcometida = IdInstalacionAcometida;
        this.idClasificacionGabinete = idClasificacionGabinete;
        this.nombreClasificacionGabinete = nombreClasificacionGabinete;
        this.valorClasificacionGabinete = valorClasificacionGabinete;
        this.numerodeOrden = numerodeOrden;
        this.numerodeGabinete = numerodeGabinete;
        this.fotoGabinete = fotoGabinete;
        this.tipoGabinete = tipoGabinete;
        this.posicion = posicion;
        this.idGabineteProyectoInstalacion = idGabineteProyectoInstalacion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getIdInstalacionAcometida() {
        return IdInstalacionAcometida;
    }

    public void setIdInstalacionAcometida(int idInstalacionAcometida) {
        IdInstalacionAcometida = idInstalacionAcometida;
    }

    public int getIdClasificacionGabinete() {
        return idClasificacionGabinete;
    }

    public void setIdClasificacionGabinete(int idClasificacionGabinete) {
        this.idClasificacionGabinete = idClasificacionGabinete;
    }

    public String getNombreClasificacionGabinete() {
        return nombreClasificacionGabinete;
    }

    public void setNombreClasificacionGabinete(String nombreClasificacionGabinete) {
        this.nombreClasificacionGabinete = nombreClasificacionGabinete;
    }

    public int getValorClasificacionGabinete() {
        return valorClasificacionGabinete;
    }

    public void setValorClasificacionGabinete(int valorClasificacionGabinete) {
        this.valorClasificacionGabinete = valorClasificacionGabinete;
    }

    public int getNumerodeOrden() {
        return numerodeOrden;
    }

    public void setNumerodeOrden(int numerodeOrden) {
        this.numerodeOrden = numerodeOrden;
    }

    public int getNumerodeGabinete() {
        return numerodeGabinete;
    }

    public void setNumerodeGabinete(int numerodeGabinete) {
        this.numerodeGabinete = numerodeGabinete;
    }

    public String getFotoGabinete() {
        return fotoGabinete;
    }

    public void setFotoGabinete(String fotoGabinete) {
        this.fotoGabinete = fotoGabinete;
    }

    public String getTipoGabinete() {
        return tipoGabinete;
    }

    public void setTipoGabinete(String tipoGabinete) {
        this.tipoGabinete = tipoGabinete;
    }

    public Integer getPosicion() {
        return posicion;
    }

    public void setPosicion(Integer posicion) {
        this.posicion = posicion;
    }

    public Integer getIdGabineteProyectoInstalacion() {
        return idGabineteProyectoInstalacion;
    }

    public void setIdGabineteProyectoInstalacion(Integer idGabineteProyectoInstalacion) {
        this.idGabineteProyectoInstalacion = idGabineteProyectoInstalacion;
    }
}
