package pe.gob.osinergmin.gnr.cgn.actividad;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.material.navigation.NavigationView;

import gob.osinergmin.gnr.domain.dto.rest.out.BaseOutRO;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.BuildConfig;
import pe.gob.osinergmin.gnr.cgn.data.models.AcometidaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.ConfigDao;
import pe.gob.osinergmin.gnr.cgn.data.models.MontanteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.SuministroDao;
import pe.gob.osinergmin.gnr.cgn.task.Logout;
import pe.gob.osinergmin.gnr.cgn.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.service.LocationUpdatesService;
import pe.gob.osinergmin.gnr.cgn.util.DownTimer;
import pe.gob.osinergmin.gnr.cgn.util.NetworkUtil;
import pe.gob.osinergmin.gnr.cgn.util.Util;

import static pe.gob.osinergmin.gnr.cgn.util.Constantes.ACTION_SERVICE;


public class PrincipalActivity extends BaseActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    private Config config;
    private DownTimer countDownTimer;
    private Button btnPri_Sincronizacion, btnPri_PreVisitas, btnPri_montante, btnPri_acometida;
    private boolean abierto = true;
    private int cont;

    private ConfigDao configDao;
    private SuministroDao suministroDao;
    private AcometidaDao acometidaDao;
    private MontanteDao montanteDao;

    private TextView user, preVisitas, numPre, tv_acometida, numAco, tv_montante, numMon, sincronizacion, cerrar, configModo;
    private ImageView desConfig, configVacio, imageSync;
    private SwitchCompat offline;

    private static final String TAG = PrincipalActivity.class.getSimpleName();
    // Used in checking for runtime permissions.
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    // The BroadcastReceiver used to listen from broadcasts from the service.
    private MyReceiver myReceiver;
    // A reference to the service used to get location updates.
    private LocationUpdatesService mService = null;
    // Tracks the bound state of the service.
    private boolean mBound = false;
    // Monitors the state of the connection to the service.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            pe.gob.osinergmin.gnr.cgn.service.LocationUpdatesService.LocalBinder binder = (pe.gob.osinergmin.gnr.cgn.service.LocationUpdatesService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_principal_nav);

        countDownTimer = DownTimer.getInstance();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        configDao = ((App) getApplication()).getDaoSession().getConfigDao();
        suministroDao = ((App) getApplication()).getDaoSession().getSuministroDao();
        acometidaDao = ((App) getApplication()).getDaoSession().getAcometidaDao();
        montanteDao = ((App) getApplication()).getDaoSession().getMontanteDao();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.nav_open, R.string.nav_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        user = header.findViewById(R.id.nav_usuario);
        preVisitas = header.findViewById(R.id.preVisitas);
        numPre = header.findViewById(R.id.numPre);
        tv_acometida = header.findViewById(R.id.tv_acometida);
        numAco = header.findViewById(R.id.numAco);
        tv_montante = header.findViewById(R.id.tv_montante);
        numMon = header.findViewById(R.id.numMon);
        sincronizacion = header.findViewById(R.id.sincronizacion);
        imageSync = header.findViewById(R.id.imageSync);
        desConfig = header.findViewById(R.id.desConfig);
        configVacio = header.findViewById(R.id.configVacio);
        configModo = header.findViewById(R.id.configModo);
        offline = header.findViewById(R.id.offline);
        cerrar = header.findViewById(R.id.cerrar);

        btnPri_PreVisitas = findViewById(R.id.btnPri_PreVisitas);
        btnPri_acometida = findViewById(R.id.btnPri_acometida);
        btnPri_montante = findViewById(R.id.btnPri_montante);
        btnPri_Sincronizacion = findViewById(R.id.btnPri_Sincronizacion);
        Button btnPri_Configuracion = findViewById(R.id.btnPri_Configuracion);
        Button btnPri_CerrarSession = findViewById(R.id.btnPri_CerrarSession);

        btnPri_PreVisitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PrincipalActivity.this, BuscarSuministroActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnPri_acometida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PrincipalActivity.this, BuscarAcometidaActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnPri_montante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PrincipalActivity.this, BuscarMontanteActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnPri_Sincronizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PrincipalActivity.this, SyncActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnPri_Configuracion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PrincipalActivity.this, ConfiguracionActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnPri_CerrarSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cerrarSesion();
            }
        });
        syncDes();

        if (Util.requestingLocationUpdates(this)) {
            if (!checkPermissions()) {
                requestPermissions();
            }
        }

        Util.setRequestingCloseSesion(this, false);

        myReceiver = new MyReceiver();
        IntentFilter actionsReceiver = new IntentFilter(pe.gob.osinergmin.gnr.cgn.service.LocationUpdatesService.ACTION_BROADCAST_CIERRE);
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver, actionsReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.createAndStart();
        cont = 0;

        config = configDao.queryBuilder().limit(1).unique();
        if (!config.getLogin()) {
            Intent intent2 = new Intent(this, IniciarActivity.class);
            intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent2);
        }
        user.setText(config.getUsername());
        btnPri_PreVisitas.setEnabled(!config.getDesca() && !config.getSync());
        btnPri_acometida.setEnabled(!config.getDesca() && !config.getSync());
        btnPri_montante.setEnabled(!config.getDesca() && !config.getSync());
        setupMenu();
        setMenuCounter();
        btnPri_Sincronizacion.setVisibility(config.getOffline() ? View.VISIBLE : View.GONE);
    }

    public void setupMenu() {
        ColorStateList oldColors = preVisitas.getTextColors();
        if (!config.getDesca() && !config.getSync()) {
            preVisitas.setTextColor(oldColors);
            preVisitas.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(PrincipalActivity.this, BuscarSuministroActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            tv_acometida.setTextColor(oldColors);
            tv_acometida.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(PrincipalActivity.this, BuscarAcometidaActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            tv_montante.setTextColor(oldColors);
            tv_montante.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(PrincipalActivity.this, BuscarMontanteActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        } else {
            preVisitas.setTextColor(getResources().getColor(R.color.divider));
            tv_acometida.setTextColor(getResources().getColor(R.color.divider));
            tv_montante.setTextColor(getResources().getColor(R.color.divider));
        }
        sincronizacion.setVisibility(config.getOffline() ? View.VISIBLE : View.GONE);
        imageSync.setVisibility(config.getOffline() ? View.VISIBLE : View.GONE);
        sincronizacion.setText(config.getSync() ? "Sincronizando..." : "Sincronización");
        sincronizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PrincipalActivity.this, SyncActivity.class);
                startActivity(intent);
                finish();
            }
        });
        offline.setChecked(config.getOffline());
        desConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (abierto) {
                    configVacio.setVisibility(View.VISIBLE);
                    configModo.setVisibility(View.VISIBLE);
                    offline.setVisibility(View.VISIBLE);
                    abierto = false;
                } else {
                    configVacio.setVisibility(View.GONE);
                    configModo.setVisibility(View.GONE);
                    offline.setVisibility(View.GONE);
                    abierto = true;
                }
            }
        });
        offline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PrincipalActivity.this, ConfiguracionActivity.class);
                startActivity(intent);
                finish();
            }
        });
        cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cerrarSesion();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            cerrarSesion();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);

        bindService(new Intent(this, LocationUpdatesService.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);

        if (!checkPermissions()) {
            requestPermissions();
        } else {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    mService.requestLocationUpdates();
                }
            }, 1000);   //1 seconds
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        countDownTimer.createAndStart();
    }

    @Override
    protected void onStop() {
        Log.i(TAG, "onStop");
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection);
            mBound = false;
        }

        //Util.setRequestingLocationUpdates(this, false);
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "onPause");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
        super.onPause();
    }

    private void syncDes() {
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
        IntentFilter filter = new IntentFilter(ACTION_SERVICE);
        broadcastManager.registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        boolean sinWifi = intent.getBooleanExtra("sinWifi", false);
                        boolean sync = intent.getBooleanExtra("sync", false);
                        switch (intent.getAction()) {
                            case Constantes.ACTION_SERVICE:
                                if (!config.getDesca() && !config.getSync()) {
                                    preVisitas.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (!config.getDesca() && !config.getSync()) {
                                                Intent intent = new Intent(PrincipalActivity.this, BuscarSuministroActivity.class);
                                                startActivity(intent);
                                                finish();
                                            }
                                        }
                                    });
                                    tv_acometida.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (!config.getDesca() && !config.getSync()) {
                                                Intent intent = new Intent(PrincipalActivity.this, BuscarAcometidaActivity.class);
                                                startActivity(intent);
                                                finish();
                                            }
                                        }
                                    });
                                    tv_montante.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (!config.getDesca() && !config.getSync()) {
                                                Intent intent = new Intent(PrincipalActivity.this, BuscarMontanteActivity.class);
                                                startActivity(intent);
                                                finish();
                                            }
                                        }
                                    });
                                    btnPri_PreVisitas.setEnabled(!config.getDesca() && !config.getSync());
                                    btnPri_acometida.setEnabled(!config.getDesca() && !config.getSync());
                                    btnPri_montante.setEnabled(!config.getDesca() && !config.getSync());
                                    preVisitas.setTextColor(cerrar.getTextColors());
                                    tv_acometida.setTextColor(cerrar.getTextColors());
                                    tv_montante.setTextColor(cerrar.getTextColors());
                                    setMenuCounter();
                                }
                                if (sinWifi && cont == 0) {
                                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                                    if (sync) {
                                        //todo aqui no se como va a saber
                                        btnPri_Sincronizacion.setVisibility(View.VISIBLE);
                                        btnPri_montante.setVisibility(View.VISIBLE);
                                    } else {
                                        btnPri_Sincronizacion.setVisibility(View.GONE);
                                        btnPri_montante.setVisibility(View.GONE);
                                    }
                                    cont = cont + 1;
                                }
                                break;
                        }
                    }
                },
                filter
        );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void setMenuCounter() {
        long previsita = suministroDao
                .queryBuilder()
                .where(SuministroDao.Properties.Grabar.eq(true)).count();
        long acometida = acometidaDao
                .queryBuilder()
                .where(AcometidaDao.Properties.Grabar.eq(true)).count();
        long montante = montanteDao
                .queryBuilder()
                .where(MontanteDao.Properties.Grabar.eq(true)).count();
        if (previsita > 0) {
            numPre.setText(String.valueOf(previsita));
        } else {
            numPre.setText("");
        }
        if (acometida > 0) {
            numAco.setText(String.valueOf(acometida));
        } else {
            numAco.setText("");
        }
        if (montante > 0) {
            numMon.setText(String.valueOf(montante));
        } else {
            numMon.setText("");
        }
    }

    private void cerrarSesion() {
        AlertDialog.Builder builder = new AlertDialog.Builder(PrincipalActivity.this);
        builder.setTitle(R.string.app_msjTitulo);
        builder.setMessage(R.string.app_msjCuerpo);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.app_msjSi, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (config.getOffline()) {
                    config.setLogin(false);
                    configDao.insertOrReplace(config);
                    Intent intent = new Intent(PrincipalActivity.this, IniciarActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        finalizarEventoGPS();
                        CallLogout();
                    } else {
                        ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                    }
                }
            }
        });
        builder.setNegativeButton(R.string.app_msjNo, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    private void CallLogout() {
        showProgressDialog(R.string.app_cargando);
        Logout logout = new Logout(new Logout.OnLogoutCompleted() {
            @Override
            public void onLogoutCompled(BaseOutRO baseOutRO) {
                hideProgressDialog();
                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                } else if (baseOutRO == null) {
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                } else if (!gob.osinergmin.gnr.util.Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(baseOutRO.getResultCode())) {
                    ((App) getApplication()).showToast(baseOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : baseOutRO.getMessage());
                } else {
                    config.setLogin(false);
                    configDao.insertOrReplace(config);
                    Intent intent = new Intent(PrincipalActivity.this, IniciarActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            }
        });
        logout.execute(config);
    }

    private boolean checkLocationPermission() {
        String permission = "android.permission.ACCESS_FINE_LOCATION";
        int res = this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
        /**
         * Receiver for broadcasts sent by {@link LocationUpdatesService}.
         */
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(pe.gob.osinergmin.gnr.cgn.service.LocationUpdatesService.EXTRA_LOCATION);
            if (location != null) {
                Toast.makeText(PrincipalActivity.this, Util.getLocationText(location),
                        Toast.LENGTH_SHORT).show();
            }
            /*
            Log.i(TAG, intent.getAction());
            if (intent.getAction().equalsIgnoreCase(LocationUpdatesService.ACTION_BROADCAST_CIERRE)){
                desactivarProveedorGPS();
                return;
            }
             */
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        // Update the buttons state depending on whether location updates are being requested.
        if (s.equals(Util.KEY_REQUESTING_LOCATION_UPDATES)) {
            setButtonsState(sharedPreferences.getBoolean(Util.KEY_REQUESTING_LOCATION_UPDATES,
                    false));
        }
    }

    private void setButtonsState(boolean requestingLocationUpdates) {
        if (requestingLocationUpdates) {
            Log.i("TEST", "LOG OFF");
        } else {
            Log.i("TEST", "LOG ON");
        }
    }/**
     * Callback received when a permissions request has been completed.
     */

    /**
     * Returns the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        return PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            /*
            Snackbar.make(
                    findViewById(R.id.mainlayout),
                    R.string.txtMap_desactivada,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.app_msjSi, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(PrincipalActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    })
                    .show();
            */
            ActivityCompat.requestPermissions(PrincipalActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(PrincipalActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted.
                mService.requestLocationUpdates();
            } else {
                // Permission denied.
                setButtonsState(false);
                Intent intent = new Intent();
                intent.setAction(
                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package",
                        BuildConfig.APPLICATION_ID, null);
                intent.setData(uri);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }
    }

    public void finalizarEventoGPS() {
        Util.setRequestingCloseSesion(this, true);
        Intent myService = new Intent(this, pe.gob.osinergmin.gnr.cgn.service.LocationUpdatesService.class);
        stopService(myService);
    }

    public void desactivarProveedorGPS() {
        Util.setRequestingCloseSesion(this, true);
        Intent myService = new Intent(this, pe.gob.osinergmin.gnr.cgn.service.LocationUpdatesService.class);
        stopService(myService);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAndRemoveTask();
        } else {
            finishAffinity();
        }
    }
}
