package pe.gob.osinergmin.gnr.cgn.util;

import android.os.AsyncTask;
import android.os.Environment;

import java.io.File;

public class BorrarFoto extends AsyncTask<String, Void, Void> {

    @Override
    protected Void doInBackground(String... params) {
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), Util.getRuta());
        if (file.isDirectory()) {
            String[] children = file.list();
            if (children != null) {
                for (int i = 0; i < children.length; i++) {
                    String tmp[] = children[i].split("_");
                    if (params[0].equalsIgnoreCase(tmp[0])) {
                        new File(file, children[i]).delete();
                    }
                }
            }
        }
        return null;
    }
}
