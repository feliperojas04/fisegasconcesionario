package pe.gob.osinergmin.gnr.cgn.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class Montante implements Parcelable {

    @Id
    private Long id;
    private Integer idHabilitacionMontante;
    private String objetoConexion;
    private String cup;
    private String nombre;
    private String direccion;
    private String tipoProyecto;
    private String fechaRegistro;
    private String esProyectoFise;
    private String tienePromocion;
    private String esZonaGasificada;
    private String tienePlanoInstalacion;
    private String tieneEspecificacion;
    private String tieneMemoria;
    private String nombreMontanteProyectoInstalacion;
    private String apruebaAcometida;
    private String fotoAcometida;
    private String apruebaTuberia;
    private String foto1Tuberia;
    private String foto2Tuberia;
    private String foto3Tuberia;
    private String apruebaHermeticidad;
    private String fotoManoHermeticidad;
    private String presionOperacionHermeticidad;
    private String presionTuberiaHermeticidad;
    private String apruebaInstalacion;
    private String tipoInstalacion;
    private String tipoMaterialInstalacion;
    private String nombreMaterialInstalacion;
    private String longitudInstalacion;
    private String diametroTuberiaInstalacion;
    private String apruebaHabilitacionMontante;
    private String fechaRegistroOffline;
    private String fechaAprobacion;
    private Boolean grabar;
    private String montanteHabilitacionFotoActa;
    private String montanteHabilitacionFotoActaRechazo;
    private String montanteHabilitacionFotoPruebaRechazo;

    @Generated(hash = 1341881842)
    public Montante() {
    }

    public Montante(Long id) {
        this.id = id;
    }

    @Generated(hash = 1105591078)
    public Montante(Long id, Integer idHabilitacionMontante, String objetoConexion, String cup, String nombre, String direccion, String tipoProyecto, String fechaRegistro, String esProyectoFise, String tienePromocion, String esZonaGasificada, String tienePlanoInstalacion, String tieneEspecificacion, String tieneMemoria, String nombreMontanteProyectoInstalacion, String apruebaAcometida, String fotoAcometida, String apruebaTuberia, String foto1Tuberia, String foto2Tuberia, String foto3Tuberia, String apruebaHermeticidad, String fotoManoHermeticidad, String presionOperacionHermeticidad, String presionTuberiaHermeticidad, String apruebaInstalacion, String tipoInstalacion, String tipoMaterialInstalacion, String nombreMaterialInstalacion, String longitudInstalacion, String diametroTuberiaInstalacion, String apruebaHabilitacionMontante, String fechaRegistroOffline, String fechaAprobacion, Boolean grabar,
            String montanteHabilitacionFotoActa, String montanteHabilitacionFotoActaRechazo, String montanteHabilitacionFotoPruebaRechazo) {
        this.id = id;
        this.idHabilitacionMontante = idHabilitacionMontante;
        this.objetoConexion = objetoConexion;
        this.cup = cup;
        this.nombre = nombre;
        this.direccion = direccion;
        this.tipoProyecto = tipoProyecto;
        this.fechaRegistro = fechaRegistro;
        this.esProyectoFise = esProyectoFise;
        this.tienePromocion = tienePromocion;
        this.esZonaGasificada = esZonaGasificada;
        this.tienePlanoInstalacion = tienePlanoInstalacion;
        this.tieneEspecificacion = tieneEspecificacion;
        this.tieneMemoria = tieneMemoria;
        this.nombreMontanteProyectoInstalacion = nombreMontanteProyectoInstalacion;
        this.apruebaAcometida = apruebaAcometida;
        this.fotoAcometida = fotoAcometida;
        this.apruebaTuberia = apruebaTuberia;
        this.foto1Tuberia = foto1Tuberia;
        this.foto2Tuberia = foto2Tuberia;
        this.foto3Tuberia = foto3Tuberia;
        this.apruebaHermeticidad = apruebaHermeticidad;
        this.fotoManoHermeticidad = fotoManoHermeticidad;
        this.presionOperacionHermeticidad = presionOperacionHermeticidad;
        this.presionTuberiaHermeticidad = presionTuberiaHermeticidad;
        this.apruebaInstalacion = apruebaInstalacion;
        this.tipoInstalacion = tipoInstalacion;
        this.tipoMaterialInstalacion = tipoMaterialInstalacion;
        this.nombreMaterialInstalacion = nombreMaterialInstalacion;
        this.longitudInstalacion = longitudInstalacion;
        this.diametroTuberiaInstalacion = diametroTuberiaInstalacion;
        this.apruebaHabilitacionMontante = apruebaHabilitacionMontante;
        this.fechaRegistroOffline = fechaRegistroOffline;
        this.fechaAprobacion = fechaAprobacion;
        this.grabar = grabar;
        this.montanteHabilitacionFotoActa = montanteHabilitacionFotoActa;
        this.montanteHabilitacionFotoActaRechazo = montanteHabilitacionFotoActaRechazo;
        this.montanteHabilitacionFotoPruebaRechazo = montanteHabilitacionFotoPruebaRechazo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdHabilitacionMontante() {
        return idHabilitacionMontante;
    }

    public void setIdHabilitacionMontante(Integer idHabilitacionMontante) {
        this.idHabilitacionMontante = idHabilitacionMontante;
    }

    public String getObjetoConexion() {
        return objetoConexion;
    }

    public void setObjetoConexion(String objetoConexion) {
        this.objetoConexion = objetoConexion;
    }

    public String getCup() {
        return cup;
    }

    public void setCup(String cup) {
        this.cup = cup;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTipoProyecto() {
        return tipoProyecto;
    }

    public void setTipoProyecto(String tipoProyecto) {
        this.tipoProyecto = tipoProyecto;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getEsProyectoFise() {
        return esProyectoFise;
    }

    public void setEsProyectoFise(String esProyectoFise) {
        this.esProyectoFise = esProyectoFise;
    }

    public String getTienePromocion() {
        return tienePromocion;
    }

    public void setTienePromocion(String tienePromocion) {
        this.tienePromocion = tienePromocion;
    }

    public String getEsZonaGasificada() {
        return esZonaGasificada;
    }

    public void setEsZonaGasificada(String esZonaGasificada) {
        this.esZonaGasificada = esZonaGasificada;
    }

    public String getTienePlanoInstalacion() {
        return tienePlanoInstalacion;
    }

    public void setTienePlanoInstalacion(String tienePlanoInstalacion) {
        this.tienePlanoInstalacion = tienePlanoInstalacion;
    }

    public String getTieneEspecificacion() {
        return tieneEspecificacion;
    }

    public void setTieneEspecificacion(String tieneEspecificacion) {
        this.tieneEspecificacion = tieneEspecificacion;
    }

    public String getTieneMemoria() {
        return tieneMemoria;
    }

    public void setTieneMemoria(String tieneMemoria) {
        this.tieneMemoria = tieneMemoria;
    }

    public String getNombreMontanteProyectoInstalacion() {
        return nombreMontanteProyectoInstalacion;
    }

    public void setNombreMontanteProyectoInstalacion(String nombreMontanteProyectoInstalacion) {
        this.nombreMontanteProyectoInstalacion = nombreMontanteProyectoInstalacion;
    }

    public String getApruebaAcometida() {
        return apruebaAcometida;
    }

    public void setApruebaAcometida(String apruebaAcometida) {
        this.apruebaAcometida = apruebaAcometida;
    }

    public String getFotoAcometida() {
        return fotoAcometida;
    }

    public void setFotoAcometida(String fotoAcometida) {
        this.fotoAcometida = fotoAcometida;
    }

    public String getApruebaTuberia() {
        return apruebaTuberia;
    }

    public void setApruebaTuberia(String apruebaTuberia) {
        this.apruebaTuberia = apruebaTuberia;
    }

    public String getFoto1Tuberia() {
        return foto1Tuberia;
    }

    public void setFoto1Tuberia(String foto1Tuberia) {
        this.foto1Tuberia = foto1Tuberia;
    }

    public String getFoto2Tuberia() {
        return foto2Tuberia;
    }

    public void setFoto2Tuberia(String foto2Tuberia) {
        this.foto2Tuberia = foto2Tuberia;
    }

    public String getFoto3Tuberia() {
        return foto3Tuberia;
    }

    public void setFoto3Tuberia(String foto3Tuberia) {
        this.foto3Tuberia = foto3Tuberia;
    }

    public String getApruebaHermeticidad() {
        return apruebaHermeticidad;
    }

    public void setApruebaHermeticidad(String apruebaHermeticidad) {
        this.apruebaHermeticidad = apruebaHermeticidad;
    }

    public String getFotoManoHermeticidad() {
        return fotoManoHermeticidad;
    }

    public void setFotoManoHermeticidad(String fotoManoHermeticidad) {
        this.fotoManoHermeticidad = fotoManoHermeticidad;
    }

    public String getPresionOperacionHermeticidad() {
        return presionOperacionHermeticidad;
    }

    public void setPresionOperacionHermeticidad(String presionOperacionHermeticidad) {
        this.presionOperacionHermeticidad = presionOperacionHermeticidad;
    }

    public String getPresionTuberiaHermeticidad() {
        return presionTuberiaHermeticidad;
    }

    public void setPresionTuberiaHermeticidad(String presionTuberiaHermeticidad) {
        this.presionTuberiaHermeticidad = presionTuberiaHermeticidad;
    }

    public String getApruebaInstalacion() {
        return apruebaInstalacion;
    }

    public void setApruebaInstalacion(String apruebaInstalacion) {
        this.apruebaInstalacion = apruebaInstalacion;
    }

    public String getTipoInstalacion() {
        return tipoInstalacion;
    }

    public void setTipoInstalacion(String tipoInstalacion) {
        this.tipoInstalacion = tipoInstalacion;
    }

    public String getTipoMaterialInstalacion() {
        return tipoMaterialInstalacion;
    }

    public void setTipoMaterialInstalacion(String tipoMaterialInstalacion) {
        this.tipoMaterialInstalacion = tipoMaterialInstalacion;
    }

    public String getNombreMaterialInstalacion() {
        return nombreMaterialInstalacion;
    }

    public void setNombreMaterialInstalacion(String nombreMaterialInstalacion) {
        this.nombreMaterialInstalacion = nombreMaterialInstalacion;
    }

    public String getLongitudInstalacion() {
        return longitudInstalacion;
    }

    public void setLongitudInstalacion(String longitudInstalacion) {
        this.longitudInstalacion = longitudInstalacion;
    }

    public String getDiametroTuberiaInstalacion() {
        return diametroTuberiaInstalacion;
    }

    public void setDiametroTuberiaInstalacion(String diametroTuberiaInstalacion) {
        this.diametroTuberiaInstalacion = diametroTuberiaInstalacion;
    }

    public String getApruebaHabilitacionMontante() {
        return apruebaHabilitacionMontante;
    }

    public void setApruebaHabilitacionMontante(String apruebaHabilitacionMontante) {
        this.apruebaHabilitacionMontante = apruebaHabilitacionMontante;
    }

    public String getFechaRegistroOffline() {
        return fechaRegistroOffline;
    }

    public void setFechaRegistroOffline(String fechaRegistroOffline) {
        this.fechaRegistroOffline = fechaRegistroOffline;
    }

    public String getFechaAprobacion() {
        return fechaAprobacion;
    }

    public void setFechaAprobacion(String fechaAprobacion) {
        this.fechaAprobacion = fechaAprobacion;
    }

    public Boolean getGrabar() {
        return grabar;
    }

    public void setGrabar(Boolean grabar) {
        this.grabar = grabar;
    }

    public String getMontanteHabilitacionFotoActa() {
        return montanteHabilitacionFotoActa;
    }

    public void setMontanteHabilitacionFotoActa(String montanteHabilitacionFotoActa) {
        this.montanteHabilitacionFotoActa = montanteHabilitacionFotoActa;
    }

    public String getMontanteHabilitacionFotoActaRechazo() {
        return montanteHabilitacionFotoActaRechazo;
    }

    public void setMontanteHabilitacionFotoActaRechazo(String montanteHabilitacionFotoActaRechazo) {
        this.montanteHabilitacionFotoActaRechazo = montanteHabilitacionFotoActaRechazo;
    }

    public String getMontanteHabilitacionFotoPruebaRechazo() {
        return montanteHabilitacionFotoPruebaRechazo;
    }

    public void setMontanteHabilitacionFotoPruebaRechazo(String montanteHabilitacionFotoPruebaRechazo) {
        this.montanteHabilitacionFotoPruebaRechazo = montanteHabilitacionFotoPruebaRechazo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.idHabilitacionMontante);
        dest.writeString(this.objetoConexion);
        dest.writeString(this.cup);
        dest.writeString(this.nombre);
        dest.writeString(this.direccion);
        dest.writeString(this.tipoProyecto);
        dest.writeString(this.fechaRegistro);
        dest.writeString(this.esProyectoFise);
        dest.writeString(this.tienePromocion);
        dest.writeString(this.esZonaGasificada);
        dest.writeString(this.tienePlanoInstalacion);
        dest.writeString(this.tieneEspecificacion);
        dest.writeString(this.tieneMemoria);
        dest.writeString(this.nombreMontanteProyectoInstalacion);
        dest.writeString(this.apruebaAcometida);
        dest.writeString(this.fotoAcometida);
        dest.writeString(this.apruebaTuberia);
        dest.writeString(this.foto1Tuberia);
        dest.writeString(this.foto2Tuberia);
        dest.writeString(this.foto3Tuberia);
        dest.writeString(this.apruebaHermeticidad);
        dest.writeString(this.fotoManoHermeticidad);
        dest.writeString(this.presionOperacionHermeticidad);
        dest.writeString(this.presionTuberiaHermeticidad);
        dest.writeString(this.apruebaInstalacion);
        dest.writeString(this.tipoInstalacion);
        dest.writeString(this.tipoMaterialInstalacion);
        dest.writeString(this.nombreMaterialInstalacion);
        dest.writeString(this.longitudInstalacion);
        dest.writeString(this.diametroTuberiaInstalacion);
        dest.writeString(this.apruebaHabilitacionMontante);
        dest.writeString(this.fechaRegistroOffline);
        dest.writeString(this.fechaAprobacion);
        dest.writeValue(this.grabar);
        dest.writeString(this.montanteHabilitacionFotoActa);
        dest.writeString(this.montanteHabilitacionFotoActaRechazo);
        dest.writeString(this.montanteHabilitacionFotoPruebaRechazo);
    }

    protected Montante(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.idHabilitacionMontante = (Integer) in.readValue(Integer.class.getClassLoader());
        this.objetoConexion = in.readString();
        this.cup = in.readString();
        this.nombre = in.readString();
        this.direccion = in.readString();
        this.tipoProyecto = in.readString();
        this.fechaRegistro = in.readString();
        this.esProyectoFise = in.readString();
        this.tienePromocion = in.readString();
        this.esZonaGasificada = in.readString();
        this.tienePlanoInstalacion = in.readString();
        this.tieneEspecificacion = in.readString();
        this.tieneMemoria = in.readString();
        this.nombreMontanteProyectoInstalacion = in.readString();
        this.apruebaAcometida = in.readString();
        this.fotoAcometida = in.readString();
        this.apruebaTuberia = in.readString();
        this.foto1Tuberia = in.readString();
        this.foto2Tuberia = in.readString();
        this.foto3Tuberia = in.readString();
        this.apruebaHermeticidad = in.readString();
        this.fotoManoHermeticidad = in.readString();
        this.presionOperacionHermeticidad = in.readString();
        this.presionTuberiaHermeticidad = in.readString();
        this.apruebaInstalacion = in.readString();
        this.tipoInstalacion = in.readString();
        this.tipoMaterialInstalacion = in.readString();
        this.nombreMaterialInstalacion = in.readString();
        this.longitudInstalacion = in.readString();
        this.diametroTuberiaInstalacion = in.readString();
        this.apruebaHabilitacionMontante = in.readString();
        this.fechaRegistroOffline = in.readString();
        this.fechaAprobacion = in.readString();
        this.grabar = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.montanteHabilitacionFotoActa = in.readString();
        this.montanteHabilitacionFotoActaRechazo = in.readString();
        this.montanteHabilitacionFotoPruebaRechazo = in.readString();
    }

    public static final Creator<Montante> CREATOR = new Creator<Montante>() {
        @Override
        public Montante createFromParcel(Parcel source) {
            return new Montante(source);
        }

        @Override
        public Montante[] newArray(int size) {
            return new Montante[size];
        }
    };
}
