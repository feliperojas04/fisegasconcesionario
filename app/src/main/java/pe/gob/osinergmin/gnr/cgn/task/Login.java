package pe.gob.osinergmin.gnr.cgn.task;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import gob.osinergmin.gnr.domain.dto.rest.in.AccessRequestInRO;
import gob.osinergmin.gnr.domain.dto.rest.out.AccessResponseOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.data.models.Usuario;
import pe.gob.osinergmin.gnr.cgn.util.Urls;

public class Login extends AsyncTask<Usuario, Void, AccessResponseOutRO> {

    private OnLoginCompleted listener = null;

    public Login(OnLoginCompleted listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected AccessResponseOutRO doInBackground(Usuario... usuarios) {
        try {

            Usuario usuario = usuarios[0];

            String url = Urls.getBase() + Urls.getLogin();

            AccessRequestInRO accessRequestInRO = new AccessRequestInRO();
            accessRequestInRO.setAppInvoker(Constantes.SIGLA_GNR_MOBILE);
            accessRequestInRO.setUsername(usuario.getUsername());
            accessRequestInRO.setPassword(usuario.getPassword());

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<AccessRequestInRO> httpEntity = new HttpEntity<>(accessRequestInRO, httpHeaders);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            return restTemplate.postForObject(url, httpEntity, AccessResponseOutRO.class);

        } catch (RestClientException e) {
            Log.e("LOGIN", e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(AccessResponseOutRO accessResponseOutRO) {
        super.onPostExecute(accessResponseOutRO);
        listener.onLoginCompled(accessResponseOutRO);
    }

    public interface OnLoginCompleted {
        void onLoginCompled(AccessResponseOutRO accessResponseOutRO);
    }
}
