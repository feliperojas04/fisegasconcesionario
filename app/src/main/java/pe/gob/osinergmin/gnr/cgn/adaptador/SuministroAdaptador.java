package pe.gob.osinergmin.gnr.cgn.adaptador;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.BaseHabilitacionOutRO;
import pe.gob.osinergmin.gnr.cgn.R;

public class SuministroAdaptador extends RecyclerView.Adapter<SuministroAdaptador.ViewHolder> {

    private CallBack mCallback;
    private List<BaseHabilitacionOutRO> baseHabilitacionOutROs;

    public SuministroAdaptador() {
        this.baseHabilitacionOutROs = new ArrayList<>();
    }

    public void add(List<BaseHabilitacionOutRO> mc) {
        baseHabilitacionOutROs.addAll(mc);
        notifyItemInserted(baseHabilitacionOutROs.size() - 1);
    }

    public void remove(BaseHabilitacionOutRO city) {
        int position = baseHabilitacionOutROs.indexOf(city);
        if (position > -1) {
            baseHabilitacionOutROs.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public void setCallback(CallBack callback) {
        mCallback = callback;
    }

    public List<BaseHabilitacionOutRO> getItems() {
        return baseHabilitacionOutROs;
    }

    public BaseHabilitacionOutRO getItem(int position) {
        return this.baseHabilitacionOutROs.get(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_suministro, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final BaseHabilitacionOutRO baseHabilitacionOutRO = this.baseHabilitacionOutROs.get(position);
        String suministro = "<b>Solicitante:</b> %1$s (%2$s)";
        String suministroF = String.format(suministro, baseHabilitacionOutRO.getNombreSolicitante(), baseHabilitacionOutRO.getDocumentoIdentificacionSolicitante());
        holder.txtBusI_propietario.setText(Html.fromHtml(suministroF));
        String visitado = "<b>Suministro:</b> %1$s";
        String visitadoF = String.format(visitado, baseHabilitacionOutRO.getNumeroSuministroPredio());
        holder.txtBusI_predio.setText(Html.fromHtml(visitadoF));
        String predio = "<b>Predio:</b> %1$s";
        String predioF = String.format(predio, baseHabilitacionOutRO.getDireccionUbigeoPredio());
        holder.txtBusI_suministros.setText(Html.fromHtml(predioF));
        String visitante = "<b>Contrato:</b> %1$s";
        String visitanteF = String.format(visitante, baseHabilitacionOutRO.getNumeroContratoConcesionariaPredio());
        holder.txtBusI_contrato.setText(Html.fromHtml(visitanteF));

        if (baseHabilitacionOutRO.getErrorCode() == null || "".equalsIgnoreCase(baseHabilitacionOutRO.getErrorCode())) {
            holder.btnBusI_error.setVisibility(View.GONE);
            holder.btnBusI_eliminar.setVisibility(View.GONE);
            if (baseHabilitacionOutRO.getResultCode() == null || "".equalsIgnoreCase(baseHabilitacionOutRO.getResultCode())) {
                holder.contenedor.setBackgroundResource(R.color.blanco);
            } else {
                holder.contenedor.setBackgroundResource(R.color.mod);
            }
        } else {
            holder.btnBusI_error.setVisibility(View.VISIBLE);
            holder.btnBusI_eliminar.setVisibility(baseHabilitacionOutRO.getResultCode().equalsIgnoreCase("101") ? View.GONE : View.VISIBLE);
            holder.contenedor.setBackgroundResource(R.color.error);
            holder.btnBusI_error.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setTitle("Causa del error en la sincronización");
                    builder.setMessage(baseHabilitacionOutRO.getMessage());
                    builder.setCancelable(false);
                    builder.setPositiveButton("Entendido", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.show();
                }
            });
            holder.btnBusI_eliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onInstalacionEliminar(position);
                }
            });
        }
        holder.contenedor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onSelect(baseHabilitacionOutRO, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return baseHabilitacionOutROs.size();
    }

    public interface CallBack {
        void onInstalacionEliminar(int position);

        void onSelect(BaseHabilitacionOutRO baseHabilitacionOutRO, int position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtBusI_propietario, txtBusI_predio, txtBusI_suministros, txtBusI_contrato;
        Button btnBusI_error, btnBusI_eliminar;
        RelativeLayout contenedor;

        ViewHolder(View v) {
            super(v);
            txtBusI_propietario = v.findViewById(R.id.txtBusI_propietario);
            txtBusI_predio = v.findViewById(R.id.txtBusI_predio);
            txtBusI_suministros = v.findViewById(R.id.txtBusI_suministros);
            txtBusI_contrato = v.findViewById(R.id.txtBusI_contrato);
            btnBusI_error = v.findViewById(R.id.btnBusI_error);
            btnBusI_eliminar = v.findViewById(R.id.btnBusI_eliminar);
            contenedor = v.findViewById(R.id.contenedor);
        }
    }
}
