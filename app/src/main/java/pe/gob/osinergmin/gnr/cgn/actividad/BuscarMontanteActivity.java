package pe.gob.osinergmin.gnr.cgn.actividad;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.out.BaseHabilitacionMontanteOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.BaseOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.HabilitacionMontanteOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.ParametroOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.ProyectoInstalacionOutRO;
import gob.osinergmin.gnr.domain.dto.rest.out.list.ListParametroOutRO;
import gob.osinergmin.gnr.util.Constantes;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.adaptador.MontanteAdaptador;
import pe.gob.osinergmin.gnr.cgn.adaptador.VacioAdaptador;
import pe.gob.osinergmin.gnr.cgn.data.models.AcometidaDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.ConfigDao;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionMontanteOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MHabilitacionMontanteOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MParametroOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.MProyectoInstalacionOutRO;
import pe.gob.osinergmin.gnr.cgn.data.models.MProyectoInstalacionOutRODao;
import pe.gob.osinergmin.gnr.cgn.data.models.Montante;
import pe.gob.osinergmin.gnr.cgn.data.models.MontanteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.RechazoMontanteDao;
import pe.gob.osinergmin.gnr.cgn.data.models.SuministroDao;
import pe.gob.osinergmin.gnr.cgn.task.Logout;
import pe.gob.osinergmin.gnr.cgn.task.MontanteRx;
import pe.gob.osinergmin.gnr.cgn.task.ParametroAll;
import pe.gob.osinergmin.gnr.cgn.util.DownTimer;
import pe.gob.osinergmin.gnr.cgn.util.NetworkUtil;
import pe.gob.osinergmin.gnr.cgn.util.Parse;
import pe.gob.osinergmin.gnr.cgn.util.SimpleDividerItemDecoration;

public class BuscarMontanteActivity extends BaseActivity implements MontanteAdaptador.CallBack {

    private static final int PAGE_SIZE = 20;
    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;

    private RecyclerView listaSuministros;
    private List<MHabilitacionMontanteOutRO> mHabilitacionMontanteOutROS = new ArrayList<>();

    private Toolbar toolbar;
    private Config config;
    private DownTimer countDownTimer;
    private boolean abierto = true, isLoading = false, isLastPage = false;

    private ConfigDao configDao;
    private MontanteDao montanteDao;
    private SuministroDao suministroDao;
    private AcometidaDao acometidaDao;
    private MParametroOutRODao mParametroOutRODao;
    private MHabilitacionMontanteOutRODao mHabilitacionMontanteOutRODao;
    private MProyectoInstalacionOutRODao mProyectoInstalacionOutRODao;
    private RechazoMontanteDao rechazoMontanteDao;

    private TextView user, preVisitas, numPre, tv_acometida, numAco, tv_montante, numMon, sincronizacion, cerrar, configModo;
    private ImageView desConfig, configVacio, imageSync;
    private SwitchCompat offline;
    private MontanteAdaptador montanteAdaptador;

    private final CompositeDisposable disposables = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_montante_nav);

        countDownTimer = DownTimer.getInstance();

        montanteAdaptador = new MontanteAdaptador();

        configDao = ((App) getApplication()).getDaoSession().getConfigDao();
        montanteDao = ((App) getApplication()).getDaoSession().getMontanteDao();
        suministroDao = ((App) getApplication()).getDaoSession().getSuministroDao();
        acometidaDao = ((App) getApplication()).getDaoSession().getAcometidaDao();
        mParametroOutRODao = ((App) getApplication()).getDaoSession().getMParametroOutRODao();
        mHabilitacionMontanteOutRODao = ((App) getApplication()).getDaoSession().getMHabilitacionMontanteOutRODao();
        mProyectoInstalacionOutRODao = ((App) getApplication()).getDaoSession().getMProyectoInstalacionOutRODao();
        rechazoMontanteDao = ((App) getApplication()).getDaoSession().getRechazoMontanteDao();

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.nav_open, R.string.nav_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        user = header.findViewById(R.id.nav_usuario);
        preVisitas = header.findViewById(R.id.preVisitas);
        numPre = header.findViewById(R.id.numPre);
        tv_acometida = header.findViewById(R.id.tv_acometida);
        numAco = header.findViewById(R.id.numAco);
        tv_montante = header.findViewById(R.id.tv_montante);
        numMon = header.findViewById(R.id.numMon);
        sincronizacion = header.findViewById(R.id.sincronizacion);
        imageSync = header.findViewById(R.id.imageSync);
        desConfig = header.findViewById(R.id.desConfig);
        configVacio = header.findViewById(R.id.configVacio);
        configModo = header.findViewById(R.id.configModo);
        offline = header.findViewById(R.id.offline);
        cerrar = header.findViewById(R.id.cerrar);

        listaSuministros = findViewById(R.id.listaSuministros);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        listaSuministros.setLayoutManager(mLayoutManager);
        listaSuministros.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext(), R.drawable.line_divider_black));
        listaSuministros.setItemAnimator(new DefaultItemAnimator());
        ImageView recargaSuministro = findViewById(R.id.recargaSuministro);

        assert recargaSuministro != null;
        recargaSuministro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (config.getOffline()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(BuscarMontanteActivity.this);
                    builder.setTitle("Aviso");
                    builder.setMessage("Para obtener datos actualizados debe activar el modo ONLINE, lo cual puede hacer por medio de la opción \"Configuracion\".");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Entendido", (dialog, which) -> {
                    });
                    builder.show();
                } else {
                    if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        initRecycler();
                        CallListaBaseHabilitacionMontante(currentPage);
                    } else {
                        ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                    }
                }
            }
        });

        listaSuministros.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!config.getOffline()) {
                    if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        int visibleItemCount = mLayoutManager.getChildCount();
                        int totalItemCount = mLayoutManager.getItemCount();
                        int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                        if (!isLoading && !isLastPage) {
                            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                                    && firstVisibleItemPosition >= 0
                                    && totalItemCount >= PAGE_SIZE) {
                                isLoading = true;
                                CallListaBaseHabilitacionMontante(currentPage);
                            }
                        }
                    } else {
                        ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                    }
                }
            }
        });
    }

    private void cargarHabilitaciones(int posicion) {
        HabilitacionMontanteOutRO habilitacionMontanteOutRO = Parse.getHabilitacionMontanteOutRO(mHabilitacionMontanteOutROS.get(posicion));
        MProyectoInstalacionOutRO mProyectoInstalacionOutRO = mProyectoInstalacionOutRODao.queryBuilder()
                .where(MProyectoInstalacionOutRODao.Properties.IdProyectoInstalacion.eq(habilitacionMontanteOutRO.getIdProyectoInstalacion())).limit(1).unique();
        ProyectoInstalacionOutRO proyectoInstalacionOutRO = Parse.getProyectoInstalacionOutRO(mProyectoInstalacionOutRO);
        habilitacionMontanteOutRO.setProyectoInstalacion(proyectoInstalacionOutRO);
        List montanteList = montanteDao.queryBuilder()
                .where(MontanteDao.Properties.IdHabilitacionMontante.eq(habilitacionMontanteOutRO.getIdHabilitacionMontante())).list();
        if (montanteList.size() > 0) {
            Montante montante = (Montante) montanteList.get(0);
            Intent intent = new Intent(BuscarMontanteActivity.this, HabilitacionMontanteActivity.class);
            intent.putExtra("MONTANTE", montante);
            intent.putExtra("CONFIG", config);
            startActivity(intent);
            finish();
        } else {
            Montante montante = new Montante(null,
                    habilitacionMontanteOutRO.getIdHabilitacionMontante() != null ? habilitacionMontanteOutRO.getIdHabilitacionMontante() : 0,
                    proyectoInstalacionOutRO.getCodigoObjetoConexionProyectoInstalacion() != null ? proyectoInstalacionOutRO.getCodigoObjetoConexionProyectoInstalacion() : "",
                    proyectoInstalacionOutRO.getCodigoUnidadPredialProyectoInstalacion() != null ? proyectoInstalacionOutRO.getCodigoUnidadPredialProyectoInstalacion() : "",
                    proyectoInstalacionOutRO.getNombreProyectoInstalacion() != null ? proyectoInstalacionOutRO.getNombreProyectoInstalacion() : "",
                    proyectoInstalacionOutRO.getDireccionUbigeoProyectoInstalacion() != null ? proyectoInstalacionOutRO.getDireccionUbigeoProyectoInstalacion() : "",
                    proyectoInstalacionOutRO.getNombreTipoProyectoInstalacion() != null ? proyectoInstalacionOutRO.getNombreTipoProyectoInstalacion() : "",
                    proyectoInstalacionOutRO.getFechaRegistroPortalProyectoInstalacion() != null ? proyectoInstalacionOutRO.getFechaRegistroPortalProyectoInstalacion() : "",
                    proyectoInstalacionOutRO.getNombreEsProyectoFiseProyectoInstalacion() != null ? proyectoInstalacionOutRO.getNombreEsProyectoFiseProyectoInstalacion() : "",
                    proyectoInstalacionOutRO.getNombreAplicaMecanismoPromocionProyectoInstalacion() != null ? proyectoInstalacionOutRO.getNombreAplicaMecanismoPromocionProyectoInstalacion() : "",
                    proyectoInstalacionOutRO.getNombreEsZonaGasificadaProyectoInstalacion() != null ? proyectoInstalacionOutRO.getNombreEsZonaGasificadaProyectoInstalacion() : "",
                    proyectoInstalacionOutRO.getNombreTienePlanoInstalacionInternaProyectoInstalacion() != null ? proyectoInstalacionOutRO.getNombreTienePlanoInstalacionInternaProyectoInstalacion() : "",
                    proyectoInstalacionOutRO.getNombreTieneEspecificacionTecnicaProyectoInstalacion() != null ? proyectoInstalacionOutRO.getNombreTieneEspecificacionTecnicaProyectoInstalacion() : "",
                    proyectoInstalacionOutRO.getNombreTieneMemoriaCalculosProyectoInstalacion() != null ? proyectoInstalacionOutRO.getNombreTieneMemoriaCalculosProyectoInstalacion() : "",
                    habilitacionMontanteOutRO.getNombreMontanteProyectoInstalacion() != null ? habilitacionMontanteOutRO.getNombreMontanteProyectoInstalacion() : "",
                    "0", "", "0",
                    "", "", "", "0",
                    "", "", "",
                    "0", "", "", "",
                    "", "", "0", "",
                    "", false,"","","");
            montanteDao.insertOrReplace(montante);
            Intent intent = new Intent(BuscarMontanteActivity.this, HabilitacionMontanteActivity.class);
            intent.putExtra("MONTANTE", montante);
            intent.putExtra("CONFIG", config);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.createAndStart();
        toolbar.setTitle("Habilitaciones de Montante");

        config = configDao.queryBuilder().limit(1).unique();
        config.setTexto("");
        user.setText(config.getUsername());
        setupMenu();
        setMenuCounter();
        initRecycler();
        if (config.getOffline()) {
            mHabilitacionMontanteOutROS = mHabilitacionMontanteOutRODao.queryBuilder()
                    .orderDesc(MHabilitacionMontanteOutRODao.Properties.ResultCode).list();
            listaHabilitacion(mHabilitacionMontanteOutROS);
        } else {
            if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                CallListaBaseHabilitacionMontante(currentPage);
            } else {
                ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
            }
        }
    }

    public void setupMenu() {
        preVisitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuscarMontanteActivity.this, BuscarSuministroActivity.class);
                startActivity(intent);
                finish();
            }
        });
        tv_acometida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuscarMontanteActivity.this, BuscarAcometidaActivity.class);
                startActivity(intent);
                finish();
            }
        });
        tv_montante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        sincronizacion.setVisibility(config.getOffline() ? View.VISIBLE : View.GONE);
        imageSync.setVisibility(config.getOffline() ? View.VISIBLE : View.GONE);
        sincronizacion.setText(config.getSync() ? "Sincronizando..." : "Sincronización");
        sincronizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuscarMontanteActivity.this, SyncActivity.class);
                startActivity(intent);
            }
        });
        offline.setChecked(config.getOffline());
        desConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (abierto) {
                    configVacio.setVisibility(View.VISIBLE);
                    configModo.setVisibility(View.VISIBLE);
                    offline.setVisibility(View.VISIBLE);
                    abierto = false;
                } else {
                    configVacio.setVisibility(View.GONE);
                    configModo.setVisibility(View.GONE);
                    offline.setVisibility(View.GONE);
                    abierto = true;
                }
            }
        });
        offline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuscarMontanteActivity.this, ConfiguracionActivity.class);
                startActivity(intent);
                finish();
            }
        });
        cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cerrarSesion();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_busqueda_suministro, menu);
        MenuItem searchItem = menu.findItem(R.id.menuBusS_buscar);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        String buscar = getResources().getString(R.string.txtBus_filtro);
        searchView.setQueryHint(buscar);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                config.setTexto(query);
                if (config.getOffline()) {
                    montanteAdaptador.clear();
                    mHabilitacionMontanteOutROS = mHabilitacionMontanteOutRODao.queryBuilder().whereOr(
                            MHabilitacionMontanteOutRODao.Properties.CodigoObjetoConexionProyectoInstalacion.like("%" + query + "%"),
                            MHabilitacionMontanteOutRODao.Properties.CodigoUnidadPredialProyectoInstalacion.like("%" + query + "%")).list();
                    listaHabilitacion(mHabilitacionMontanteOutROS);
                } else {
                    if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        initRecycler();
                        CallListaBaseHabilitacionMontante(currentPage);
                    } else {
                        ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                    }
                }
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                config.setTexto("");
                if (config.getOffline()) {
                    montanteAdaptador.clear();
                    mHabilitacionMontanteOutROS = mHabilitacionMontanteOutRODao.queryBuilder()
                            .orderDesc(MHabilitacionMontanteOutRODao.Properties.ResultCode).list();
                    listaHabilitacion(mHabilitacionMontanteOutROS);
                } else {
                    if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        initRecycler();
                        CallListaBaseHabilitacionMontante(currentPage);
                    } else {
                        ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                    }
                }
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    public void listaHabilitacion(List<MHabilitacionMontanteOutRO> mHabilitacionMontanteOutROS) {
        initRecycler();
        List<BaseHabilitacionMontanteOutRO> baseHabilitacionMontanteOutROS = new ArrayList<>();
        for (MHabilitacionMontanteOutRO mHabilitacionMontanteOutRO : mHabilitacionMontanteOutROS) {
            baseHabilitacionMontanteOutROS.add(Parse.getBaseHabilitacionMontanteOutRO(mHabilitacionMontanteOutRO));
        }
        if (baseHabilitacionMontanteOutROS.size() > 0) {
            mostrarDatos(baseHabilitacionMontanteOutROS);
        } else {
            mostrarSin();
        }
    }

    public void initRecycler() {
        montanteAdaptador.clear();
        montanteAdaptador.setCallback(this);
        listaSuministros.setAdapter(montanteAdaptador);
        currentPage = PAGE_START;
        isLastPage = false;
    }

    public void mostrarDatos(List<BaseHabilitacionMontanteOutRO> baseHabilitacionMontanteOutROS) {
        montanteAdaptador.add(baseHabilitacionMontanteOutROS);
    }

    public void mostrarSin() {
        listaSuministros.setAdapter(new VacioAdaptador("No se encontraron habilitaciones de montante pendientes"));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent intent = new Intent(BuscarMontanteActivity.this, PrincipalActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        countDownTimer.createAndStart();
    }

    private void setMenuCounter() {
        long previsita = suministroDao
                .queryBuilder()
                .where(SuministroDao.Properties.Grabar.eq(true)).count();
        long acometida = acometidaDao
                .queryBuilder()
                .where(AcometidaDao.Properties.Grabar.eq(true)).count();
        long montante = montanteDao
                .queryBuilder()
                .where(MontanteDao.Properties.Grabar.eq(true)).count();
        if (previsita > 0) {
            numPre.setText(String.valueOf(previsita));
        } else {
            numPre.setText("");
        }
        if (acometida > 0) {
            numAco.setText(String.valueOf(acometida));
        } else {
            numAco.setText("");
        }
        if (montante > 0) {
            numMon.setText(String.valueOf(montante));
        } else {
            numMon.setText("");
        }
        if (previsita > 0 || acometida > 0 || montante > 0) {
            SharedPreferences mPreferences = getSharedPreferences("gnrconcesionario", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = mPreferences.edit();
            editor.putBoolean("pendientes", true);
            editor.apply();
        } else {
            SharedPreferences mPreferences = getSharedPreferences("gnrconcesionario", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = mPreferences.edit();
            editor.putBoolean("pendientes", false);
            editor.apply();
        }
    }

    private void cerrarSesion() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(BuscarMontanteActivity.this);
        builder.setTitle(R.string.app_msjTitulo);
        builder.setMessage(R.string.app_msjCuerpo);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.app_msjSi, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (config.getOffline()) {
                    config.setLogin(false);
                    configDao.insertOrReplace(config);
                    Intent intent = new Intent(BuscarMontanteActivity.this, IniciarActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        showProgressDialog(R.string.app_cargando);
                        Logout logout = new Logout(new Logout.OnLogoutCompleted() {
                            @Override
                            public void onLogoutCompled(BaseOutRO baseOutRO) {
                                hideProgressDialog();
                                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                                } else if (baseOutRO == null) {
                                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                                } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(baseOutRO.getResultCode())) {
                                    ((App) getApplication()).showToast(baseOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : baseOutRO.getMessage());
                                } else {
                                    countDownTimer.stop();
                                    config.setLogin(false);
                                    configDao.insertOrReplace(config);
                                    Intent intent = new Intent(BuscarMontanteActivity.this, IniciarActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        });
                        logout.execute(config);
                    } else {
                        ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
                    }
                }
            }
        });
        builder.setNegativeButton(R.string.app_msjNo, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    @Override
    protected void onDestroy() {
        disposables.clear();
        super.onDestroy();
    }

    @Override
    public void onInstalacionEliminar(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(BuscarMontanteActivity.this);
        builder.setTitle(R.string.app_msjTitulo);
        builder.setMessage("¿Está seguro que desea eliminar la Habilitación de Montante?");
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.app_msjSi, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                BaseHabilitacionMontanteOutRO baseHabilitacionMontanteOutRO = montanteAdaptador.getItem(position);
                Montante montante = montanteDao.queryBuilder().where(MontanteDao.Properties.IdHabilitacionMontante.eq(baseHabilitacionMontanteOutRO.getIdHabilitacionMontante())).limit(1).unique();
                montante.setGrabar(false);
                montanteDao.insertOrReplace(montante);
                MHabilitacionMontanteOutRO mHabilitacionMontanteOutRO = mHabilitacionMontanteOutRODao
                        .queryBuilder().where(MHabilitacionMontanteOutRODao.Properties
                                .IdHabilitacionMontante.eq(baseHabilitacionMontanteOutRO.getIdHabilitacionMontante())).limit(1).unique();
                mHabilitacionMontanteOutRO.setResultCode("101");
                mHabilitacionMontanteOutRODao.insertOrReplace(mHabilitacionMontanteOutRO);
                montanteAdaptador.getItem(position).setResultCode("101");
                montanteAdaptador.notifyItemChanged(position);
                setMenuCounter();
            }
        });
        builder.setNegativeButton(R.string.app_msjNo, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    @Override
    public void onSelect(BaseHabilitacionMontanteOutRO baseHabilitacionMontanteOutRO, int position) {
        config.setIdHabilitacionMontante(baseHabilitacionMontanteOutRO.getIdHabilitacionMontante().toString());
        if (config.getOffline()) {
            MParametroOutRO mParametroOutRO1 = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_VALIDACION_ACTIVA_DISTANCIA_INSPECTOR_PREDIO))
                    .limit(1)
                    .unique();
            config.setParametro1(mParametroOutRO1 == null ? "N" : mParametroOutRO1.getValorParametro());
            MParametroOutRO mParametroOutPrecision = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_FRECUENCIA_TOMA_COORDENADAS_AUDITORIA))
                    .limit(1).unique();
            config.setPrecision(mParametroOutPrecision == null ? "10000" : mParametroOutPrecision.getValorParametro());
            MParametroOutRO mParametroOutRO3 = mParametroOutRODao.
                    queryBuilder().
                    where(MParametroOutRODao.
                            Properties.NombreParametro.
                            eq(Constantes.NOMBRE_PARAMETRO_REGULADOR_PRIMERA_ETAPA))
                    .limit(1)
                    .unique();
            config.setParametro6(mParametroOutRO3 == null ? "Regulador de primera etapa" : mParametroOutRO3.getValorParametro());
            cargarHabilitaciones(position);
        } else {
            if (NetworkUtil.isNetworkConnected(getApplicationContext())) {
                CallParametro();
            } else {
                ((App) getApplication()).showToast("No hay conexión a internet, asegúrese de contar con una y vuelva a intentarlo.");
            }
        }
    }

    private void CallListaBaseHabilitacionMontante(final int actualPage) {
        showProgressDialog(R.string.app_cargando);
        Config config1 = config;
        config1.setErrorPrevisita(actualPage); // pagina
        config1.setErrorVisita(PAGE_SIZE); // elementos por pagina
        disposables.add(MontanteRx.getMontanteListarResumen(config1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(listBaseHabilitacionMontanteOutRO -> {
                    hideProgressDialog();
                    if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listBaseHabilitacionMontanteOutRO.getResultCode())) {
                        ((App) getApplication()).showToast(listBaseHabilitacionMontanteOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listBaseHabilitacionMontanteOutRO.getMessage());
                    } else {
                        if (listBaseHabilitacionMontanteOutRO.getHabilitacionMontante() != null && listBaseHabilitacionMontanteOutRO.getHabilitacionMontante().size() > 0) {
                            mostrarDatos(listBaseHabilitacionMontanteOutRO.getHabilitacionMontante());
                        } else {
                            isLastPage = true;
                            if (actualPage == PAGE_START) {
                                mostrarSin();
                            }
                        }
                        currentPage += 1;
                        isLoading = false;
                    }
                }, throwable -> {
                    hideProgressDialog();
                    if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                    } else {
                        ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                    }
                }));
    }

    private void CallParametro() {
        showProgressDialog(R.string.app_cargando);
        ParametroAll parametroAll = new ParametroAll(new ParametroAll.OnParametroAllCompleted() {
            @Override
            public void onParametroAllCompled(ListParametroOutRO listParametroOutRO) {
                if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                } else if (listParametroOutRO == null) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(listParametroOutRO.getResultCode())) {
                    hideProgressDialog();
                    ((App) getApplication()).showToast(listParametroOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : listParametroOutRO.getMessage());
                } else {
                    List<ParametroOutRO> parametroOutROList = listParametroOutRO.getParametro();
                    for (ParametroOutRO parametroOutRO : parametroOutROList) {
                        switch (parametroOutRO.getNombreParametro()) {
                            case Constantes.NOMBRE_PARAMETRO_VALIDACION_ACTIVA_DISTANCIA_INSPECTOR_PREDIO:
                                config.setParametro1(parametroOutRO.getValorParametro());
                                break;
                            case Constantes.NOMBRE_PARAMETRO_DISTANCIA_MAXIMA_INSPECTOR_PREDIO:
                                config.setParametro2(parametroOutRO.getValorParametro());
                                break;
                            case Constantes.NOMBRE_PARAMETRO_SOLICITUD_ACTIVA_CODIGO_ESPECIAL_ACCESO:
                                config.setParametro3(parametroOutRO.getValorParametro());
                                break;
                            case Constantes.NOMBRE_PARAMETRO_TELEFONO_SOLICITUD_CODIGO_ESPECIAL_ACCESO:
                                config.setParametro4(parametroOutRO.getValorParametro());
                                break;
                            case Constantes.NOMBRE_PARAMETRO_CONFIGURACION_COMPRESION_FOTOS_APLICATIVO_MOVIL:
                                config.setParametro5(parametroOutRO.getValorParametro());
                                break;
                            case Constantes.NOMBRE_PARAMETRO_FRECUENCIA_TOMA_COORDENADAS_AUDITORIA:
                                config.setPrecision(parametroOutRO.getValorParametro());
                                break;
                            case Constantes.NOMBRE_PARAMETRO_REGULADOR_PRIMERA_ETAPA:
                                config.setParametro6(parametroOutRO.getValorParametro());
                                break;
                        }
                    }
                    CallHabilitacionMontante();
                }
            }
        });
        parametroAll.execute(config);
    }

    private void CallHabilitacionMontante() {
        disposables.add(MontanteRx.getMontanteObtener(Integer.parseInt(config.getIdHabilitacionMontante()), config.getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(habilitacionMontanteOutRO -> {
                    hideProgressDialog();
                    if (habilitacionMontanteOutRO == null) {
                        ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                    } else if (!Constantes.RESULTADO_SUCCESS.equalsIgnoreCase(habilitacionMontanteOutRO.getResultCode())) {
                        ((App) getApplication()).showToast(habilitacionMontanteOutRO.getMessage() == null ? getResources().getString(R.string.app_resultCode) : habilitacionMontanteOutRO.getMessage());
                    } else {
                        List montanteList = montanteDao.queryBuilder()
                                .where(MontanteDao.Properties.IdHabilitacionMontante.eq(habilitacionMontanteOutRO.getIdHabilitacionMontante())).list();
                        if (montanteList.size() > 0) {
                            Montante montante = (Montante) montanteList.get(0);
                            Intent intent = new Intent(BuscarMontanteActivity.this, HabilitacionMontanteActivity.class);
                            intent.putExtra("MONTANTE", montante);
                            intent.putExtra("CONFIG", config);
                            startActivity(intent);
                            finish();
                        } else {
                            ProyectoInstalacionOutRO proyectoInstalacionOutRO = habilitacionMontanteOutRO.getProyectoInstalacion();
                            Montante montante = new Montante(null,
                                    habilitacionMontanteOutRO.getIdHabilitacionMontante() != null ? habilitacionMontanteOutRO.getIdHabilitacionMontante() : 0,
                                    proyectoInstalacionOutRO.getCodigoObjetoConexionProyectoInstalacion() != null ? proyectoInstalacionOutRO.getCodigoObjetoConexionProyectoInstalacion() : "",
                                    proyectoInstalacionOutRO.getCodigoUnidadPredialProyectoInstalacion() != null ? proyectoInstalacionOutRO.getCodigoUnidadPredialProyectoInstalacion() : "",
                                    proyectoInstalacionOutRO.getNombreProyectoInstalacion() != null ? proyectoInstalacionOutRO.getNombreProyectoInstalacion() : "",
                                    proyectoInstalacionOutRO.getDireccionUbigeoProyectoInstalacion() != null ? proyectoInstalacionOutRO.getDireccionUbigeoProyectoInstalacion() : "",
                                    proyectoInstalacionOutRO.getNombreTipoProyectoInstalacion() != null ? proyectoInstalacionOutRO.getNombreTipoProyectoInstalacion() : "",
                                    proyectoInstalacionOutRO.getFechaRegistroPortalProyectoInstalacion() != null ? proyectoInstalacionOutRO.getFechaRegistroPortalProyectoInstalacion() : "",
                                    proyectoInstalacionOutRO.getNombreEsProyectoFiseProyectoInstalacion() != null ? proyectoInstalacionOutRO.getNombreEsProyectoFiseProyectoInstalacion() : "",
                                    proyectoInstalacionOutRO.getNombreAplicaMecanismoPromocionProyectoInstalacion() != null ? proyectoInstalacionOutRO.getNombreAplicaMecanismoPromocionProyectoInstalacion() : "",
                                    proyectoInstalacionOutRO.getNombreEsZonaGasificadaProyectoInstalacion() != null ? proyectoInstalacionOutRO.getNombreEsZonaGasificadaProyectoInstalacion() : "",
                                    proyectoInstalacionOutRO.getNombreTienePlanoInstalacionInternaProyectoInstalacion() != null ? proyectoInstalacionOutRO.getNombreTienePlanoInstalacionInternaProyectoInstalacion() : "",
                                    proyectoInstalacionOutRO.getNombreTieneEspecificacionTecnicaProyectoInstalacion() != null ? proyectoInstalacionOutRO.getNombreTieneEspecificacionTecnicaProyectoInstalacion() : "",
                                    proyectoInstalacionOutRO.getNombreTieneMemoriaCalculosProyectoInstalacion() != null ? proyectoInstalacionOutRO.getNombreTieneMemoriaCalculosProyectoInstalacion() : "",
                                    habilitacionMontanteOutRO.getNombreMontanteProyectoInstalacion()!= null ? habilitacionMontanteOutRO.getNombreMontanteProyectoInstalacion() : "",
                                    "0", "", "0",
                                    "", "", "", "0",
                                    "", "", "",
                                    "0", "", "", "",
                                    "", "", "0", "",
                                    "", false,"","","");
                            montanteDao.insertOrReplace(montante);

                            Intent intent = new Intent(BuscarMontanteActivity.this, HabilitacionMontanteActivity.class);
                            intent.putExtra("MONTANTE", montante);
                            intent.putExtra("CONFIG", config);
                            startActivity(intent);
                            finish();
                        }
                    }
                }, throwable -> {
                    hideProgressDialog();
                    if (!NetworkUtil.isNetworkConnected(getApplicationContext())) {
                        ((App) getApplication()).showToast("Se interrumpió la conexión a internet. Por favor vuelva a intentarlo.");
                    } else {
                        ((App) getApplication()).showToast(getResources().getString(R.string.app_resultCode));
                    }
                }));
    }
}
