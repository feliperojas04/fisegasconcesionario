package pe.gob.osinergmin.gnr.cgn.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import gob.osinergmin.gnr.domain.dto.rest.in.InstalacionInternaInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.ObservacionInstalacionInternaInRO;
import gob.osinergmin.gnr.domain.dto.rest.in.list.ListObservacionInstalacionInternaInRO;
import gob.osinergmin.gnr.domain.dto.rest.out.InstalacionInternaOutRO;
import gob.osinergmin.gnr.util.Constantes;
import pe.gob.osinergmin.gnr.cgn.App;
import pe.gob.osinergmin.gnr.cgn.data.models.Config;
import pe.gob.osinergmin.gnr.cgn.data.models.ConfigDao;
import pe.gob.osinergmin.gnr.cgn.data.models.Observacion;
import pe.gob.osinergmin.gnr.cgn.data.models.ObservacionDao;
import pe.gob.osinergmin.gnr.ign.data.models.PaqueteGPS;
import pe.gob.osinergmin.gnr.cgn.util.Urls;

public class GrabarGPSTask  extends AsyncTask<PaqueteGPS, Void, Boolean> {

    private GrabarGPSTask.OnGrabarGPSCompleted listener = null;
    private Config config;
    private ConfigDao configDao;
    private final WeakReference<Context> context;

    public GrabarGPSTask(GrabarGPSTask.OnGrabarGPSCompleted listener, Context context) {
        this.listener = listener;
        this.context = new WeakReference<>(context);
    }

    protected void onPreExecute() {
        super.onPreExecute();
        Context context = this.context.get();
        if (context != null) {
            ConfigDao configDao = ((App) context.getApplicationContext()).getDaoSession().getConfigDao();
            config = configDao.queryBuilder().limit(1).unique();
        }
    }

    @Override
    protected Boolean doInBackground(PaqueteGPS... params) {
        try {

            PaqueteGPS paqueteGPS = params[0];
            String url = Urls.getBase() + Urls.getMonitoreoGPS();

            paqueteGPS.setIdUsuario(config.getUsername());
            /*
            InstalacionInternaInRO instalacionInternaInRO = new InstalacionInternaInRO();
            instalacionInternaInRO.setIdInstalacionInterna(instalacion.getIdHabilitacion());
            instalacionInternaInRO.setCoordenadasPredio(instalacion.getCoordenadasPredio());
            instalacionInternaInRO.setFechaRegistroInicioOfflineInstalacionInterna(instalacion.getFechaRegistroOffline());
            instalacionInternaInRO.setSePudoRealizarInstalacionInterna(instalacion.getSeInstala().equalsIgnoreCase("1") ? Constantes.VALOR_SI : Constantes.VALOR_NO);
            instalacionInternaInRO.setCoordenadasAuditoriaInstalacionInterna(String.format("%s,%s", precision == null ? 0.0 : precision.getLatitud(), precision == null ? 0.0 : precision.getLongitud()));
            instalacionInternaInRO.setPrecisionCoordenadasAuditoriaInstalacionInterna(BigDecimal.valueOf(Double.valueOf(precision == null ? "0.0" : precision.getPrecision())));
            if (instalacion.getSeInstala().equalsIgnoreCase("0")) {
                ListObservacionInstalacionInternaInRO listObservacionInstalacionInternaInRO = new ListObservacionInstalacionInternaInRO();
                List<ObservacionInstalacionInternaInRO> observacionesTmp = new ArrayList<>();
                int i = 1;
                for (Observacion observacion : observaciones) {
                    ObservacionInstalacionInternaInRO observacion1 = new ObservacionInstalacionInternaInRO();
                    observacion1.setIdTipoObservacion(Integer.parseInt(observacion.getTipoObservacion()));
                    observacion1.setOrdenObservacionInstalacionInterna(i);
                    observacion1.setDescripcionObservacionInstalacionInterna(observacion.getDescripcionObservacion());
                    observacionesTmp.add(observacion1);
                    i++;
                }
                listObservacionInstalacionInternaInRO.setObservacionInstalacionInterna(observacionesTmp);
                instalacionInternaInRO.setObservaciones(listObservacionInstalacionInternaInRO);
            }
            */
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Authorization", "Bearer " + config.getToken());

            HttpEntity<PaqueteGPS> httpEntity = new HttpEntity<>(paqueteGPS, httpHeaders);
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            return restTemplate.postForObject(url, httpEntity, Boolean.class);
        } catch (RestClientException | NumberFormatException | NullPointerException e) {
            Log.e("ERIK", e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Boolean resultado) {
        super.onPostExecute(resultado);
        listener.OnGrabarGPSCompleted(resultado);
    }

    public interface OnGrabarGPSCompleted {
        void OnGrabarGPSCompleted(Boolean resultado);
    }
}
