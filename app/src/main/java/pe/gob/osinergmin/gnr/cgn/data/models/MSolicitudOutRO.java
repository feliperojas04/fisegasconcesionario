package pe.gob.osinergmin.gnr.cgn.data.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class MSolicitudOutRO {

    @Id
    private Long idSolicitud;
    private Integer codigoSolicitud;
    private String fechaSolicitud;
    private String documentoIdentificacion;
    private String nombreSolicitante;
    private String direccionUbigeoPredio;
    private String numeroSuministroPredio;
    private String numeroContratoConcesion;
    private Integer idEstadoSolicitud;
    private String nombreEstadoSolicitud;
    private Integer idEmpresaInstaladora;
    private String documentoIdentificacionEmpresaInstaladora;
    private String nombreCortoEmpresaInstaladora;
    private String nombreEmpresaInstaladora;
    private String nombreDocumentoIdentificacionEmpresaInstaladora;
    private String numeroRegistroGnEmpresaInstaladora;
    private String telefonoSolicitante;
    private String celularSolicitante;
    private String emailSolicitante;
    private String coordenadasPredio;
    private String codigoUnidadPredialPredio;
    private String codigoManzanaPredio;
    private String nombreEsUsuarioFiseSolicitud;
    private String nombreAplicaMecanismoPromocionSolicitud;
    private String nombreTipoProyectoInstalacion;
    private String nombreArchivoDocumentacionProyectoNoTipicoSolici;
    private String nombreTipoInstalacion;
    private String nombreMaterialInstalacion;
    private Integer numeroPuntosInstalacion;
    private String nombreEstratoPersona;
    private String fechaAprobacionSolicitud;
    private String fechaContratoConcesionariaPredio;
    private String codigoInternoContratoConcesionaria;
    private String numeroInstalacionConcesionaria;
    private String nombreZonaAdjudicacion;
    private String nombreEstadoTuberiaConexionAcometida;
    private String nombreTipoGabinete;
    private String nombreTipoAcometida;
    private String nombreInstalacionAcometidaConcluidaPredio;

    @Generated(hash = 1309554380)
    public MSolicitudOutRO() {
    }

    public MSolicitudOutRO(Long idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    @Generated(hash = 131140070)
    public MSolicitudOutRO(Long idSolicitud, Integer codigoSolicitud, String fechaSolicitud, String documentoIdentificacion, String nombreSolicitante, String direccionUbigeoPredio, String numeroSuministroPredio, String numeroContratoConcesion, Integer idEstadoSolicitud, String nombreEstadoSolicitud, Integer idEmpresaInstaladora, String documentoIdentificacionEmpresaInstaladora, String nombreCortoEmpresaInstaladora, String nombreEmpresaInstaladora, String nombreDocumentoIdentificacionEmpresaInstaladora, String numeroRegistroGnEmpresaInstaladora, String telefonoSolicitante, String celularSolicitante, String emailSolicitante, String coordenadasPredio, String codigoUnidadPredialPredio, String codigoManzanaPredio, String nombreEsUsuarioFiseSolicitud, String nombreAplicaMecanismoPromocionSolicitud, String nombreTipoProyectoInstalacion, String nombreArchivoDocumentacionProyectoNoTipicoSolici, String nombreTipoInstalacion, String nombreMaterialInstalacion, Integer numeroPuntosInstalacion, String nombreEstratoPersona, String fechaAprobacionSolicitud, String fechaContratoConcesionariaPredio, String codigoInternoContratoConcesionaria, String numeroInstalacionConcesionaria, String nombreZonaAdjudicacion, String nombreEstadoTuberiaConexionAcometida, String nombreTipoGabinete, String nombreTipoAcometida, String nombreInstalacionAcometidaConcluidaPredio) {
        this.idSolicitud = idSolicitud;
        this.codigoSolicitud = codigoSolicitud;
        this.fechaSolicitud = fechaSolicitud;
        this.documentoIdentificacion = documentoIdentificacion;
        this.nombreSolicitante = nombreSolicitante;
        this.direccionUbigeoPredio = direccionUbigeoPredio;
        this.numeroSuministroPredio = numeroSuministroPredio;
        this.numeroContratoConcesion = numeroContratoConcesion;
        this.idEstadoSolicitud = idEstadoSolicitud;
        this.nombreEstadoSolicitud = nombreEstadoSolicitud;
        this.idEmpresaInstaladora = idEmpresaInstaladora;
        this.documentoIdentificacionEmpresaInstaladora = documentoIdentificacionEmpresaInstaladora;
        this.nombreCortoEmpresaInstaladora = nombreCortoEmpresaInstaladora;
        this.nombreEmpresaInstaladora = nombreEmpresaInstaladora;
        this.nombreDocumentoIdentificacionEmpresaInstaladora = nombreDocumentoIdentificacionEmpresaInstaladora;
        this.numeroRegistroGnEmpresaInstaladora = numeroRegistroGnEmpresaInstaladora;
        this.telefonoSolicitante = telefonoSolicitante;
        this.celularSolicitante = celularSolicitante;
        this.emailSolicitante = emailSolicitante;
        this.coordenadasPredio = coordenadasPredio;
        this.codigoUnidadPredialPredio = codigoUnidadPredialPredio;
        this.codigoManzanaPredio = codigoManzanaPredio;
        this.nombreEsUsuarioFiseSolicitud = nombreEsUsuarioFiseSolicitud;
        this.nombreAplicaMecanismoPromocionSolicitud = nombreAplicaMecanismoPromocionSolicitud;
        this.nombreTipoProyectoInstalacion = nombreTipoProyectoInstalacion;
        this.nombreArchivoDocumentacionProyectoNoTipicoSolici = nombreArchivoDocumentacionProyectoNoTipicoSolici;
        this.nombreTipoInstalacion = nombreTipoInstalacion;
        this.nombreMaterialInstalacion = nombreMaterialInstalacion;
        this.numeroPuntosInstalacion = numeroPuntosInstalacion;
        this.nombreEstratoPersona = nombreEstratoPersona;
        this.fechaAprobacionSolicitud = fechaAprobacionSolicitud;
        this.fechaContratoConcesionariaPredio = fechaContratoConcesionariaPredio;
        this.codigoInternoContratoConcesionaria = codigoInternoContratoConcesionaria;
        this.numeroInstalacionConcesionaria = numeroInstalacionConcesionaria;
        this.nombreZonaAdjudicacion = nombreZonaAdjudicacion;
        this.nombreEstadoTuberiaConexionAcometida = nombreEstadoTuberiaConexionAcometida;
        this.nombreTipoGabinete = nombreTipoGabinete;
        this.nombreTipoAcometida = nombreTipoAcometida;
        this.nombreInstalacionAcometidaConcluidaPredio = nombreInstalacionAcometidaConcluidaPredio;
    }

    public Long getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(Long idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public Integer getCodigoSolicitud() {
        return codigoSolicitud;
    }

    public void setCodigoSolicitud(Integer codigoSolicitud) {
        this.codigoSolicitud = codigoSolicitud;
    }

    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(String fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getDocumentoIdentificacion() {
        return documentoIdentificacion;
    }

    public void setDocumentoIdentificacion(String documentoIdentificacion) {
        this.documentoIdentificacion = documentoIdentificacion;
    }

    public String getNombreSolicitante() {
        return nombreSolicitante;
    }

    public void setNombreSolicitante(String nombreSolicitante) {
        this.nombreSolicitante = nombreSolicitante;
    }

    public String getDireccionUbigeoPredio() {
        return direccionUbigeoPredio;
    }

    public void setDireccionUbigeoPredio(String direccionUbigeoPredio) {
        this.direccionUbigeoPredio = direccionUbigeoPredio;
    }

    public String getNumeroSuministroPredio() {
        return numeroSuministroPredio;
    }

    public void setNumeroSuministroPredio(String numeroSuministroPredio) {
        this.numeroSuministroPredio = numeroSuministroPredio;
    }

    public String getNumeroContratoConcesion() {
        return numeroContratoConcesion;
    }

    public void setNumeroContratoConcesion(String numeroContratoConcesion) {
        this.numeroContratoConcesion = numeroContratoConcesion;
    }

    public Integer getIdEstadoSolicitud() {
        return idEstadoSolicitud;
    }

    public void setIdEstadoSolicitud(Integer idEstadoSolicitud) {
        this.idEstadoSolicitud = idEstadoSolicitud;
    }

    public String getNombreEstadoSolicitud() {
        return nombreEstadoSolicitud;
    }

    public void setNombreEstadoSolicitud(String nombreEstadoSolicitud) {
        this.nombreEstadoSolicitud = nombreEstadoSolicitud;
    }

    public Integer getIdEmpresaInstaladora() {
        return idEmpresaInstaladora;
    }

    public void setIdEmpresaInstaladora(Integer idEmpresaInstaladora) {
        this.idEmpresaInstaladora = idEmpresaInstaladora;
    }

    public String getDocumentoIdentificacionEmpresaInstaladora() {
        return documentoIdentificacionEmpresaInstaladora;
    }

    public void setDocumentoIdentificacionEmpresaInstaladora(String documentoIdentificacionEmpresaInstaladora) {
        this.documentoIdentificacionEmpresaInstaladora = documentoIdentificacionEmpresaInstaladora;
    }

    public String getNombreCortoEmpresaInstaladora() {
        return nombreCortoEmpresaInstaladora;
    }

    public void setNombreCortoEmpresaInstaladora(String nombreCortoEmpresaInstaladora) {
        this.nombreCortoEmpresaInstaladora = nombreCortoEmpresaInstaladora;
    }

    public String getNombreEmpresaInstaladora() {
        return nombreEmpresaInstaladora;
    }

    public void setNombreEmpresaInstaladora(String nombreEmpresaInstaladora) {
        this.nombreEmpresaInstaladora = nombreEmpresaInstaladora;
    }

    public String getNombreDocumentoIdentificacionEmpresaInstaladora() {
        return nombreDocumentoIdentificacionEmpresaInstaladora;
    }

    public void setNombreDocumentoIdentificacionEmpresaInstaladora(String nombreDocumentoIdentificacionEmpresaInstaladora) {
        this.nombreDocumentoIdentificacionEmpresaInstaladora = nombreDocumentoIdentificacionEmpresaInstaladora;
    }

    public String getNumeroRegistroGnEmpresaInstaladora() {
        return numeroRegistroGnEmpresaInstaladora;
    }

    public void setNumeroRegistroGnEmpresaInstaladora(String numeroRegistroGnEmpresaInstaladora) {
        this.numeroRegistroGnEmpresaInstaladora = numeroRegistroGnEmpresaInstaladora;
    }

    public String getTelefonoSolicitante() {
        return telefonoSolicitante;
    }

    public void setTelefonoSolicitante(String telefonoSolicitante) {
        this.telefonoSolicitante = telefonoSolicitante;
    }

    public String getCelularSolicitante() {
        return celularSolicitante;
    }

    public void setCelularSolicitante(String celularSolicitante) {
        this.celularSolicitante = celularSolicitante;
    }

    public String getEmailSolicitante() {
        return emailSolicitante;
    }

    public void setEmailSolicitante(String emailSolicitante) {
        this.emailSolicitante = emailSolicitante;
    }

    public String getCoordenadasPredio() {
        return coordenadasPredio;
    }

    public void setCoordenadasPredio(String coordenadasPredio) {
        this.coordenadasPredio = coordenadasPredio;
    }

    public String getCodigoUnidadPredialPredio() {
        return codigoUnidadPredialPredio;
    }

    public void setCodigoUnidadPredialPredio(String codigoUnidadPredialPredio) {
        this.codigoUnidadPredialPredio = codigoUnidadPredialPredio;
    }

    public String getCodigoManzanaPredio() {
        return codigoManzanaPredio;
    }

    public void setCodigoManzanaPredio(String codigoManzanaPredio) {
        this.codigoManzanaPredio = codigoManzanaPredio;
    }

    public String getNombreEsUsuarioFiseSolicitud() {
        return nombreEsUsuarioFiseSolicitud;
    }

    public void setNombreEsUsuarioFiseSolicitud(String nombreEsUsuarioFiseSolicitud) {
        this.nombreEsUsuarioFiseSolicitud = nombreEsUsuarioFiseSolicitud;
    }

    public String getNombreAplicaMecanismoPromocionSolicitud() {
        return nombreAplicaMecanismoPromocionSolicitud;
    }

    public void setNombreAplicaMecanismoPromocionSolicitud(String nombreAplicaMecanismoPromocionSolicitud) {
        this.nombreAplicaMecanismoPromocionSolicitud = nombreAplicaMecanismoPromocionSolicitud;
    }

    public String getNombreTipoProyectoInstalacion() {
        return nombreTipoProyectoInstalacion;
    }

    public void setNombreTipoProyectoInstalacion(String nombreTipoProyectoInstalacion) {
        this.nombreTipoProyectoInstalacion = nombreTipoProyectoInstalacion;
    }

    public String getNombreArchivoDocumentacionProyectoNoTipicoSolici() {
        return nombreArchivoDocumentacionProyectoNoTipicoSolici;
    }

    public void setNombreArchivoDocumentacionProyectoNoTipicoSolici(String nombreArchivoDocumentacionProyectoNoTipicoSolici) {
        this.nombreArchivoDocumentacionProyectoNoTipicoSolici = nombreArchivoDocumentacionProyectoNoTipicoSolici;
    }

    public String getNombreTipoInstalacion() {
        return nombreTipoInstalacion;
    }

    public void setNombreTipoInstalacion(String nombreTipoInstalacion) {
        this.nombreTipoInstalacion = nombreTipoInstalacion;
    }

    public String getNombreMaterialInstalacion() {
        return nombreMaterialInstalacion;
    }

    public void setNombreMaterialInstalacion(String nombreMaterialInstalacion) {
        this.nombreMaterialInstalacion = nombreMaterialInstalacion;
    }

    public Integer getNumeroPuntosInstalacion() {
        return numeroPuntosInstalacion;
    }

    public void setNumeroPuntosInstalacion(Integer numeroPuntosInstalacion) {
        this.numeroPuntosInstalacion = numeroPuntosInstalacion;
    }

    public String getNombreEstratoPersona() {
        return nombreEstratoPersona;
    }

    public void setNombreEstratoPersona(String nombreEstratoPersona) {
        this.nombreEstratoPersona = nombreEstratoPersona;
    }

    public String getFechaAprobacionSolicitud() {
        return fechaAprobacionSolicitud;
    }

    public void setFechaAprobacionSolicitud(String fechaAprobacionSolicitud) {
        this.fechaAprobacionSolicitud = fechaAprobacionSolicitud;
    }

    public String getFechaContratoConcesionariaPredio() {
        return fechaContratoConcesionariaPredio;
    }

    public void setFechaContratoConcesionariaPredio(String fechaContratoConcesionariaPredio) {
        this.fechaContratoConcesionariaPredio = fechaContratoConcesionariaPredio;
    }

    public String getCodigoInternoContratoConcesionaria() {
        return codigoInternoContratoConcesionaria;
    }

    public void setCodigoInternoContratoConcesionaria(String codigoInternoContratoConcesionaria) {
        this.codigoInternoContratoConcesionaria = codigoInternoContratoConcesionaria;
    }

    public String getNumeroInstalacionConcesionaria() {
        return numeroInstalacionConcesionaria;
    }

    public void setNumeroInstalacionConcesionaria(String numeroInstalacionConcesionaria) {
        this.numeroInstalacionConcesionaria = numeroInstalacionConcesionaria;
    }

    public String getNombreZonaAdjudicacion() {
        return nombreZonaAdjudicacion;
    }

    public void setNombreZonaAdjudicacion(String nombreZonaAdjudicacion) {
        this.nombreZonaAdjudicacion = nombreZonaAdjudicacion;
    }

    public String getNombreEstadoTuberiaConexionAcometida() {
        return nombreEstadoTuberiaConexionAcometida;
    }

    public void setNombreEstadoTuberiaConexionAcometida(String nombreEstadoTuberiaConexionAcometida) {
        this.nombreEstadoTuberiaConexionAcometida = nombreEstadoTuberiaConexionAcometida;
    }

    public String getNombreTipoGabinete() {
        return nombreTipoGabinete;
    }

    public void setNombreTipoGabinete(String nombreTipoGabinete) {
        this.nombreTipoGabinete = nombreTipoGabinete;
    }

    public String getNombreTipoAcometida() {
        return nombreTipoAcometida;
    }

    public void setNombreTipoAcometida(String nombreTipoAcometida) {
        this.nombreTipoAcometida = nombreTipoAcometida;
    }

    public String getNombreInstalacionAcometidaConcluidaPredio() {
        return nombreInstalacionAcometidaConcluidaPredio;
    }

    public void setNombreInstalacionAcometidaConcluidaPredio(String nombreInstalacionAcometidaConcluidaPredio) {
        this.nombreInstalacionAcometidaConcluidaPredio = nombreInstalacionAcometidaConcluidaPredio;
    }
}
