package pe.gob.osinergmin.gnr.cgn.adaptador;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import pe.gob.osinergmin.gnr.cgn.R;
import pe.gob.osinergmin.gnr.cgn.data.pojos.Habilitacion;

public class HabilitacionAdaptador extends BaseAdapter {

    private Context context;
    private List<Habilitacion> habilitaciones;

    public HabilitacionAdaptador(Context context, List<Habilitacion> habilitaciones) {
        this.context = context;
        this.habilitaciones = habilitaciones;
    }

    @Override
    public int getCount() {
        return this.habilitaciones.size();
    }

    @Override
    public Object getItem(int position) {
        return this.habilitaciones.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = layoutInflater.inflate(R.layout.item_habilitacion, parent, false);

            holder = new ViewHolder();
            holder.txtitemHabilitacion = rowView.findViewById(R.id.txtitemHabilitacion);
            holder.icoitemHabilitacion = rowView.findViewById(R.id.icoitemHabilitacion);

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        holder.txtitemHabilitacion.setText(this.habilitaciones.get(position).nombre);
        holder.icoitemHabilitacion.setImageResource(this.habilitaciones.get(position).item);

        return rowView;
    }

    static class ViewHolder {
        TextView txtitemHabilitacion;
        ImageView icoitemHabilitacion;
    }
}
